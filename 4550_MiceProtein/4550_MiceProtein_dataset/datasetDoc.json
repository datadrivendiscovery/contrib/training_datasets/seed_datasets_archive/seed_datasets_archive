{
  "about": {
    "datasetID": "4550_MiceProtein_dataset",
    "datasetName": "MiceProtein",
    "description": "The data set consists of the expression levels of 77 proteins/protein modifications that produced detectable signals in the nuclear fraction of cortex. There are 38 control mice and 34 trisomic mice (Down syndrome), for a total of 72 mice. In the experiments, 15 measurements were registered of each protein per sample/mouse. Therefore, for control mice, there are 38x15, or 570 measurements, and for trisomic mice, there are 34x15, or 510 measurements. The dataset contains a total of 1080 measurements per protein. Each measurement can be considered as an independent sample/mouse. \n\nThe eight classes of mice are described based on features such as genotype, behavior and treatment. According to genotype, mice can be control or trisomic. According to behavior, some mice have been stimulated to learn (context-shock) and others have not (shock-context) and in order to assess the effect of the drug memantine in recovering the ability to learn in trisomic mice, some mice have been injected with the drug and others have not. \n\nClasses: \n```\n* c-CS-s: control mice, stimulated to learn, injected with saline (9 mice) \n* c-CS-m: control mice, stimulated to learn, injected with memantine (10 mice) \n* c-SC-s: control mice, not stimulated to learn, injected with saline (9 mice) \n* c-SC-m: control mice, not stimulated to learn, injected with memantine (10 mice) \n* t-CS-s: trisomy mice, stimulated to learn, injected with saline (7 mice) \n* t-CS-m: trisomy mice, stimulated to learn, injected with memantine (9 mice) \n* t-SC-s: trisomy mice, not stimulated to learn, injected with saline (9 mice) \n* t-SC-m: trisomy mice, not stimulated to learn, injected with memantine (9 mice) \n```\n\nThe aim is to identify subsets of proteins that are discriminant between the classes.",
    "citation": " @article{ author = {Higuera C, Gardiner KJ, Cios KJ}, title = {Self-Organizing Feature Maps Identify Proteins Critical to Learning in a Mouse Model of Down Syndrome}, journal = {PLoS One}, volume = {10}, number = {6}, year = {2015}, publisher = {PLoS One}, } ",
    "license": " CC Public Domain Mark 1.0 ",
    "source": "OpenML",
    "sourceURI": "http://www.openml.org/d/4550",
    "approximateSize": "",
    "datasetSchemaVersion": "4.0.0",
    "redacted": false,
    "datasetVersion": "4.0.0",
    "digest": "600b54a610df9bbeee687f8ec06a5b162d307afa9d3715024daa08a45b692182"
  },
  "dataResources": [
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colType": "integer",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "MouseID",
          "colType": "categorical",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 2,
          "colName": "DYRK1A_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 3,
          "colName": "ITSN1_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 4,
          "colName": "BDNF_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 5,
          "colName": "NR1_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 6,
          "colName": "NR2A_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 7,
          "colName": "pAKT_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 8,
          "colName": "pBRAF_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 9,
          "colName": "pCAMKII_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 10,
          "colName": "pCREB_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 11,
          "colName": "pELK_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 12,
          "colName": "pERK_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 13,
          "colName": "pJNK_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 14,
          "colName": "PKCA_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 15,
          "colName": "pMEK_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 16,
          "colName": "pNR1_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 17,
          "colName": "pNR2A_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 18,
          "colName": "pNR2B_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 19,
          "colName": "pPKCAB_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 20,
          "colName": "pRSK_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 21,
          "colName": "AKT_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 22,
          "colName": "BRAF_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 23,
          "colName": "CAMKII_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 24,
          "colName": "CREB_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 25,
          "colName": "ELK_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 26,
          "colName": "ERK_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 27,
          "colName": "GSK3B_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 28,
          "colName": "JNK_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 29,
          "colName": "MEK_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 30,
          "colName": "TRKA_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 31,
          "colName": "RSK_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 32,
          "colName": "APP_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 33,
          "colName": "Bcatenin_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 34,
          "colName": "SOD1_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 35,
          "colName": "MTOR_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 36,
          "colName": "P38_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 37,
          "colName": "pMTOR_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 38,
          "colName": "DSCR1_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 39,
          "colName": "AMPKA_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 40,
          "colName": "NR2B_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 41,
          "colName": "pNUMB_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 42,
          "colName": "RAPTOR_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 43,
          "colName": "TIAM1_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 44,
          "colName": "pP70S6_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 45,
          "colName": "NUMB_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 46,
          "colName": "P70S6_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 47,
          "colName": "pGSK3B_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 48,
          "colName": "pPKCG_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 49,
          "colName": "CDK5_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 50,
          "colName": "S6_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 51,
          "colName": "ADARB1_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 52,
          "colName": "AcetylH3K9_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 53,
          "colName": "RRP1_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 54,
          "colName": "BAX_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 55,
          "colName": "ARC_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 56,
          "colName": "ERBB4_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 57,
          "colName": "nNOS_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 58,
          "colName": "Tau_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 59,
          "colName": "GFAP_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 60,
          "colName": "GluR3_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 61,
          "colName": "GluR4_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 62,
          "colName": "IL1B_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 63,
          "colName": "P3525_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 64,
          "colName": "pCASP9_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 65,
          "colName": "PSD95_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 66,
          "colName": "SNCA_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 67,
          "colName": "Ubiquitin_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 68,
          "colName": "pGSK3B_Tyr216_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 69,
          "colName": "SHH_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 70,
          "colName": "BAD_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 71,
          "colName": "BCL2_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 72,
          "colName": "pS6_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 73,
          "colName": "pCFOS_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 74,
          "colName": "SYP_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 75,
          "colName": "H3AcK18_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 76,
          "colName": "EGR1_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 77,
          "colName": "H3MeK4_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 78,
          "colName": "CaNA_N",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 79,
          "colName": "Genotype",
          "colType": "categorical",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 80,
          "colName": "Treatment",
          "colType": "categorical",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 81,
          "colName": "Behavior",
          "colType": "categorical",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 82,
          "colName": "class",
          "colType": "categorical",
          "role": [
            "suggestedTarget"
          ]
        }
      ],
      "columnsCount": 83
    }
  ]
}