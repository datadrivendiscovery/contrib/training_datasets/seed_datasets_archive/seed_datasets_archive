import os, sys
import pandas as pd 
from datetime import datetime

# ldfpath = sys.argv[1] # path to learningData.csv
# dsfpath = sys.argv[2] # path to dataSplits.csv
# assert os.path.exists(ldfpath)
# assert os.path.exists(dsfpath)

here = os.path.dirname(os.path.abspath(__file__)) # _ignore
ldfpath = os.path.join(here, '..','LL1_736_stock_market_dataset','tables','learningData.csv')
dsfpath = os.path.join(here, 'dataSplits.csv')

ldf = pd.read_csv(ldfpath, index_col='d3mIndex', parse_dates=['Date'])
print(ldf.shape)
ldf['type']=['TRAIN']*len(ldf)
test_idx=(ldf[ldf['Date']>=datetime.strptime('10/01/2017','%m/%d/%Y')].index)
print(len(test_idx))

ldf.loc[test_idx, 'type'] = 'TEST'
print(ldf[ldf['type']=='TRAIN'].shape, ldf[ldf['type']=='TEST'].shape)
ldf = ldf.drop(['Company', 'Date', 'Close'], axis=1)
ldf['fold']=[0]*len(ldf)
ldf['repeat']=[0]*len(ldf)
print(ldf.head())
print(ldf.tail())
ldf.to_csv(dsfpath)
