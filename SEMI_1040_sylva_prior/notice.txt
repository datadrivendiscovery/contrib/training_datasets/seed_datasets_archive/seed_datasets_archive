 ID: SEMI_1040_sylva_prior_dataset
 Name: SEMI 1040 sylva prior dataset
 Description: SYLVA is the ecology database
The task of SYLVA is to classify forest cover types. The forest cover type for 30 x 30 meter cells is obtained from US Forest Service (USFS) Region 2 Resource Information System (RIS) data. We brought it back to a two-class classification problem (classifying Ponderosa pine vs. everything else). The agnostic learning track data consists in 216 input variables. Each pattern is composed of 4 records: 2 true records matching the target and 2 records picked at random. Thus 1/2 of the features are distracters. The  prior knowledge track data is identical to the agnostic learning track data, except that the distracters are removed and the identity of the features is revealed.
 License: CC-BY License
 License Link: http://creativecommons.org/licenses/by/4.0/
 Source: OpenML
 Source Link: http://www.openml.org/d/1040
 Citation: @article{OpenML2013,
    author = {Vanschoren, Joaquin and van Rijn, Jan N. and Bischl, Bernd and Torgo, Luis},
    title = {OpenML: Networked Science in Machine Learning},
    journal = {SIGKDD Explorations},
    volume = {15},
    number = {2},
    year = {2013},
    pages = {49--60},
    url = {http://doi.acm.org/10.1145/2641190.2641198},
    doi = {10.1145/2641190.2641198},
    publisher = {ACM},
    address = {New York, NY, USA},
    }
