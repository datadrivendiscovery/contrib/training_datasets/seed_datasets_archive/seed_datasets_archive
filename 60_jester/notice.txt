 ID: 60_jester_dataset
 Name: Ratings from the Jester Online Joke Recommender System
 Description: This dataset consists of over 1.7 million instances of (user_id, item_id, rating) triples, which is split 50-50 into train and test data
 License: public
 License Link: None
 Source: University of California Berkeley, CA
 Source Link: http://eigentaste.berkeley.edu/dataset/
 Citation: 
