 ID: uu7_pima_diabetes_dataset
 Name: Pima Diabetes Data Set
 Description: This is a a two-class classification problem to distinguish between absence and presence of diabetes.
 For LUPI processing, the features are split into two groups:

 - standard features (columns 2-6, 8) are physically observable properties during a routine doctor visit
 - privileged features (columns 1,7 ) are private information (number of pregnancies and diabetes pedigree function, which is the presence of diabetes among patient's relatives), which may not be available due to lack of recordkeeping.
 License: CC-BY License
 License Link: http://creativecommons.org/licenses/by/4.0/
 Source: OpenML
 Source Link: https://www.openml.org/d/37
 Citation: @article{OpenML2013,
    author = {Vanschoren, Joaquin and van Rijn, Jan N. and Bischl, Bernd and Torgo, Luis},
    title = {OpenML: Networked Science in Machine Learning},
    journal = {SIGKDD Explorations},
    volume = {15},
    number = {2},
    year = {2013},
    pages = {49--60},
    url = {http://doi.acm.org/10.1145/2641190.2641198},
    doi = {10.1145/2641190.2641198},
    publisher = {ACM},
    address = {New York, NY, USA},
    }
