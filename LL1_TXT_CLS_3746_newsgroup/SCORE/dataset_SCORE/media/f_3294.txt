[<text>
<title>sallie mae prices short-term floating rate notes</title>
<dateline>    washington, march 9 - </dateline>the student loan marketing
association said it priced its 300 mln dlr short-term floating
rate note offer at par to yield 25 basis points above the
bond-equivalent yield of the 91-day treasury bill.
    sallie mae said previously interest on the notes, which are
for settlement march 12 and are due sept 10, 1987, will be
reset the day following each weekly t-bill auction.
    when-issued trading is expected to begin at 0930 hrs est
tomorrow, it said.
    sallie mae said it generally offers short-term floating
rate notes each month.
 reuter
</text>]