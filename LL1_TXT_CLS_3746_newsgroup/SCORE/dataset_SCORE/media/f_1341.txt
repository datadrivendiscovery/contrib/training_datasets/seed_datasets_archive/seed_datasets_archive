[<text>
<title>pacific lighting copr &lt;plt&gt; quarterly dividend</title>
<dateline>    los angeles, march 3 -
    </dateline>qtly div 87 cts vs 87 cts
    pay feb 17
    record jan 20
 reuter
</text>]