[<text>
<title>hercules &lt;hpc&gt;, collagen &lt;cgen&gt; form venture</title>
<dateline>    palo alto, calif., march 31 - </dateline>hercules inc and collagen
corp said they formed an equally-owned joint venture to
develop, make and market products for the treatment of dermal
wounds caused by surgery, trauma or disease.
    the joint venture will focus on development of products to
heal dermal wounds that are resistant to currently available
treatment, the companies said.
    they said clinical studies will focus initially on treating
wounds such as decubitus ulcers or bedsores caused by prolonged
pressure and underlying diseases such as diabetes, which
compromises the blood supply.
 reuter
</text>]