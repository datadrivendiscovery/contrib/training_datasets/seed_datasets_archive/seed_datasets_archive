[<text>
<title>ferruzzi paris unit seen absorbing cpc purchase</title>
<dateline>    milan, march 26 - </dateline>sources close to italy's &lt;gruppo
ferruzzi&gt; said &lt;european sugar (france)&gt;, a french company
owned by ferruzzi, would take over control of &lt;cpc industrial
division&gt;, the corn wet milling business acquired by the
italian group earlier this week from cpc international inc
&lt;cpc&gt;.
    the sources told reuters that european sugar, owned by
ferruzzi subsidiary eridania zuccherifici nazionali spa
&lt;erdi.mi&gt;, planned to seek a listing on the paris bourse and
make a share offering there.
    cpc international announced tuesday it had agreed in
principle to sell its european corn wet millng business to
ferruzzi. the deal is worth 630 mln dlrs.
 reuter
</text>]