[<text>
<title>u.s. healthcare &lt;ushc.o&gt; to buy back shares</title>
<dateline>    blue bell, pa., oct 20 - </dateline>u.s. healthcare inc said it has
authorized the repurchase of up to 10 mln shares of stock.
    the company repurchased five mln shares under a program
that ended earlier this year.
 reuter
</text>]