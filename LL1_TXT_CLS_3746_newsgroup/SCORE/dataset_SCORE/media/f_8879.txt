[<text>
<title>general nutrition inc &lt;gnc&gt; 4th qtr net</title>
<dateline>    pittsburgh, march 24 - </dateline>qtr ends jan 31
    shr profit eight cts vs loss 38 cts
    net profit 2,466,000, vs loss 12,691,000
    revs 111.1 mln vs 106.8 mln
    12 mths
    shr profit 20 cts vs loss 47 cts
    net profit 6,591,000 vs loss 15.5 mln
    revs 342.6 mln vs 370.4 mln
    note: includes provision for store closings of foreign
operations of 3,897,000 for 1986 qtr, and 1,403,000 for qtr
prior.
    includes provision for store closing costs and unproductive
inventory of 1,000,000 for 1986 qtr, and 25.1 mln for qtr
prior.
 reuter
</text>]