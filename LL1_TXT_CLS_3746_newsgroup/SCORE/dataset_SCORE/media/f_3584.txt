[<text>
<title>u.k. money market given 106 mln stg assistance</title>
<dateline>    london, march 11 - </dateline>the bank of england said it gave the
money market assistance worth 106 mln stg this afternoon,
buying bank bills at the rates established on monday.
    the bank bought 11 mln stg of band one bills at 10-3/8 pct
and 95 mln stg of band two paper at 10-5/16 pct. this is the
first time that it has intervened today.
    the bank has revised its estimate of the liquidity shortage
in the market down to 250 mln stg from 300 mln initially.
 reuter
</text>]