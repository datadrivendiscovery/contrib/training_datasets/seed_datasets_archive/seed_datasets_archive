[<text>
<title>healthvest &lt;hvt&gt; sells shares</title>
<dateline>    austin, texas, march 4 - </dateline>healthvest, a maryland real estate
investment trust, said it began selling five mln shares of
common stock at 21 dlrs a share.
    the company said it is also selling 543,237 shares to
healthcare international inc &lt;hii&gt;, giving the company a 9.8
pct stake in healthvest.
 reuter
</text>]