[<text>
<title>kincaid furniture &lt;kncd.o&gt; repurchases shares</title>
<dateline>    hudson, n.c., june 1 - </dateline>kincaid furniture co inc said its
board has authorized the repurchase of up to 100,000 of its
common shares, or a 2.9 pct interest, in the open market or
privately from time to time.
 reuter
</text>]