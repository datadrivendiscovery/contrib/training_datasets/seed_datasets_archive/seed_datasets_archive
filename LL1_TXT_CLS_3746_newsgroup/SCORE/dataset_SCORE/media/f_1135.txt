[<text>
<title>american vanguard corp &lt;amgd&gt; year net</title>
<dateline>    los angeles, march 3 -
    </dateline>shr 57 cts vs 27 cts
    net 1,002,000 vs 470,000
    sales 15.9 mln vs 12.0 mln
    note: 4th qtr data not available
 reuter
</text>]