[<text>
<title>mid-state &lt;mssl&gt;, first federal in deal</title>
<dateline>    ocala, fla, april 3 - </dateline>mid-state federal savings and loan
association said it and first federal savings and loan
association of brooksville &lt;ffbv&gt; reached a definitive merger
agreement.
    as previously announced, brooksville shareholders will get
cash and stock in exchange for their shares. the transaction is
expected to be completed during the summer 1987.
 reuter
</text>]