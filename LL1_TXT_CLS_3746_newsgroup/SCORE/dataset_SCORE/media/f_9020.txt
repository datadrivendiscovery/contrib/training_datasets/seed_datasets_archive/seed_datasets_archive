[<text>
<title>midivest acquires assets of business aviation</title>
<dateline>    roanoke, va., march 24 - </dateline>&lt;midivest inc&gt; said it acquired
all the assets of &lt;business aviation inc&gt; of sioux falls, s.d.,
for an undisclosed amount of stock.
    midivest said it expects to sell 10 to 20 of the renovated
beechcraft planes next year. it said management will also lease
these airborne intensive care units to hospitals and government
subdivisions through metropolitan leasing, a wholly-owned
subsidiary of midivest.
 reuter
</text>]