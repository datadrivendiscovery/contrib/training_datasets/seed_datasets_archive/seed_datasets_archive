[<text>
<title>regency electronics inc &lt;rgcy.o&gt; 1st qtr net</title>
<dateline>    indianapolis, oct 20 - </dateline>period ended september 30
    shr profit one ct vs loss three cts
    net profit 65,000 vs 292,000
    sales 18.1 mln vs 16.7 mln
 reuter
</text>]