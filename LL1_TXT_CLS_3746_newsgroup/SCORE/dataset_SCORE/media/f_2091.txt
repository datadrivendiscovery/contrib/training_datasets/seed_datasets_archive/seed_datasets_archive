[<text>
<title>consolidated stores &lt;cns&gt; buys warehouse space</title>
<dateline>    columbus, ohio, march 5 - </dateline>consolidated store corp said it
purchased two mln square feet of warehouse space on 146 acres
of land adjacent to its present columbus distribution center of
430,000 square feet on 121 acres.
    the company said the building and lands were acquired from
white consolidated industries inc, an &lt;electrolux ab&gt;
subsidiary, for 27 mln dlrs through a sale and leaseback.
 reuter
</text>]