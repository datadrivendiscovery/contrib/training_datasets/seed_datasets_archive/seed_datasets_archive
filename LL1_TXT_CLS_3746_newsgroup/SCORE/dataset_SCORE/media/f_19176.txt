[<text>
<title>gte &lt;gte&gt; unit gets contract for products</title>
<dateline>    new york, june 19 - </dateline>gte corp said its fiber optic products
division was awarded a 250,000 dlr minimum contract by nynex
corp's &lt;nyn&gt; enterprise co.
    the bulk of the agreement applies to gte's elastomeric
glass tube splice products, the company said, as well as other
fiber optic components, including gte's recently-introduced
cleaving tool and installation kit.
 reuter
</text>]