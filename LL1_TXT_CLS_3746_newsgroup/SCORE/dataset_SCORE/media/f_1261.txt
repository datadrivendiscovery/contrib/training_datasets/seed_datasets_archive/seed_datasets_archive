[<text>
<title>mobil &lt;mob&gt; files for one billion dlr debt offer</title>
<dateline>    washington, march 3 - </dateline>mobil corp filed with the securities
and exchange commission for a shelf offering of up to one
billion dlrs of debt securities on terms to be determined at
the time of the sale.
    proceeds from the sale will be used for general corporate
purposes, the company said.
    mobil did not name any underwriters for the offering.
 reuter
</text>]