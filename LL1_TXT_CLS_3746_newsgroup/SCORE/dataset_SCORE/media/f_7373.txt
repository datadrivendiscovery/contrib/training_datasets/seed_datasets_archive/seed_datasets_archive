[<text>
<title>mead &lt;mea&gt; sells debentures at nine pct</title>
<dateline>    new york, march 19 - </dateline>a 150 mln dlr issue of mead corp
30-year debentures was given a nine pct coupon and priced at
par, smith barney, harris upham and co inc said as lead
manager.
    the securities, which are not callable for 10 years, carry
a rating of a-3 from moody's investors service inc and one of
bbb-plus from standard and poor's corp.
    mead said it will use the proceeds to retire outstanding
short-term debt, a portion of which was incurred to finance
part of recent acquisitions.
 reuter
</text>]