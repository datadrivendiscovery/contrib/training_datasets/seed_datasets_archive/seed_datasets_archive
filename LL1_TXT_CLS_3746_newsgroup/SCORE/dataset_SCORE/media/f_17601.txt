[<text>
<title>sun &lt;sun&gt; to acquire more of wyoming field</title>
<dateline>    fort worth, texas, - </dateline>&lt;wolverine exploration co&gt; said
substantially all the material aspects of the agreement to sell
its 8.95 pct working interest in the luckey ditch unit in unita
county, wyo., to sun co inc have been satisfied.
    closing of the transaction is scheduled for june eight,
wolverine said. the company agreed to sell its interest for
7,250,000 dlrs, subject to downward adjustment for certain
title and state requirements. sun already owns a 44 pct working
interest in the unit.
 reuter
</text>]