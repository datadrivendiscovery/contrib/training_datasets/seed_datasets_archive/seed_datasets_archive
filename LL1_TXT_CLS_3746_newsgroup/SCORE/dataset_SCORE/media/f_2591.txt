[<text>
<title>&lt;far west industries inc&gt; raises dividend</title>
<dateline>    vernon, british columbia, march 6 -
    </dateline>annual div four cts vs 1.76 cts
    pay march 20
    record march 15
 reuter
</text>]