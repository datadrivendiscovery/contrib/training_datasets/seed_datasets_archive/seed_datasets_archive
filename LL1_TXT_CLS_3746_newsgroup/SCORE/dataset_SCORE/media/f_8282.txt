[<text>
<title>florida federal &lt;flfe&gt; sees no need for action</title>
<dateline>    st. petersburg, fla., march 23 - </dateline>florida federal savings
and loan association a special review committee has determined
has determined there is no justification for legal action
against the persons responsible for the association's loan an
investment losses in fiscal 1986, ended june 30.
    in august 1986, a shareholder demand florida federal's
board take such legal action. in response, the board formed the
special review committee comprised of four outside directors.
    the company said the committee also found legal action
would not be in the best interest of first federal and its
stockholders, adding the group's findings were ratified by the
remaining disinterested directors.
 reuter
</text>]