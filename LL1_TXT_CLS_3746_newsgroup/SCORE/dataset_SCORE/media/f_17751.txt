[<text>
<title>merrill corp &lt;mrll.o&gt; 1st qtr april 30 net</title>
<dateline>    st. paul, minn., june 1 -
    </dateline>shr 21 cts vs 20 cts
    net 965,000 vs 726,000
    revs 13.4 mln vs 11.8 mln
    avg shrs 4,606,242 vs 3,624,528
 reuter
</text>]