[<text>
<title>time management systems &lt;tms&gt; to get infusion</title>
<dateline>    stillwater, okla., march 5 - </dateline>time management systems
software inc said it will receive 2,400,000 dlrs from
management technologies inc &lt;mti&gt; in exchange for an equity
position.
    the company said the money will be used to service existing
and pending contracts as well as the overall anticipated growth
of the company.
    in a separate action, management technologies  will acquire
options to purchase enough shares from the personal holdings of
j.w. mckellip, chairman of time, to become a controlling
shareholder of time.

 reuter
</text>]