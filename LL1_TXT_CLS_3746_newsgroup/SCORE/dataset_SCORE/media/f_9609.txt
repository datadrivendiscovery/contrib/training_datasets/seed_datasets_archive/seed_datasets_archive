[<text>
<title>kansas power and light &lt;kan&gt; chairman leaving</title>
<dateline>    topeka, kan., march 25 - </dateline>kansas power and light co said it
will name a successor on or before may 5 to its outgoing
chairman and chief executive officer william wall.
    wall, who will assume the position of chairman and chief
executive officer at asbestos claims facility, will fill the
positions which have been vacant for several months, the
company said.
 reuter
</text>]