[<text>
<title>titan &lt;ttn&gt; extends odd lot tender offer</title>
<dateline>    san diego, march 12 - </dateline>titan corp said it extended its
odd-lot tender offer program to purchase shares of its
preferred stock to june 10, 1987.
    the offer is made to holders of 99 or fewer preferred
shares.
    the offer was originally scheduled to expire march 10.
 reuter
</text>]