[<text>
<title>foreign brokers may get more access to japan bonds</title>
<dateline>    tokyo, march 9 - </dateline>japanese securities houses are thinking of
allowing foreign brokerages to underwrite more japanese
government bonds from april, securities managers said.
    the average foreign brokerage now underwrites only around
0.07 pct of each such issue's volume, they told reuters.
    the proposal is a response to overseas calls for japan to
liberalise its markets in the trend towards global trading.
    japanese brokerages are consistently heavy buyers of u.s.
treasury issues. &lt;nomura securities international inc&gt; and
&lt;daiwa securities america inc&gt; were named primary dealers in
u.s. government securities in december, they noted.
    brokerages within the government bond-writing syndicate are
now negotiating by how much foreign house subscriptions to
government bonds should be raised, the managers said.
    the syndicate members agreed in april, 1982 that 26 pct of
10-year government bonds should be underwritten by 93
securities firms, 17 of them foreign, and 74 pct by banks.
    the finance ministry later approved the arrangement.
    foreign brokers want to underwrite more government bonds
due to their end-investor appeal. &lt;salomon brothers asia ltd&gt;
caught market attention by buying 45 billion yen of the 100
billion of government two-year notes auctioned on february 3.
    salomon's operation was an attempt to demonstrate its
commitment to the japanese market in the hope of expanding its
underwriting share of 10-year bonds, securities sources said.
    they said that to expand participation by foreigners, the
syndicate must either expand the securities industry's 26 pct
share, cut local brokerages' share, or introduce auctions to
the government bond primary market, the sources said.
    bankers are likely to oppose losing any share and the
finance ministry is unwilling to introduce auctions on the
ground that it would slow smooth absorption of bonds in the
secondary market, securities manager said.
 reuter
</text>]