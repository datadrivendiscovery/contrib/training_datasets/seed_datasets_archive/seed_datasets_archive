[<text>
<title>u.s. selling 17 billion dlrs of 9-, 20-day bills</title>
<dateline>    washington, march 31 - </dateline>the u.s. treasury said it will
auction 17 billion dlrs of 9-day and 20-day cash management
bills on april 2.
    the auction will consist of 11 billion dlrs in 9-day bills
and six billion dlrs in 20-day bills.
    bids for the bills will be received at all federal reserve
banks and branches. tenders must be for a minimum of one mln
dlrs, and non-competitive tenders from the public will not be
accepted. bids will not be received at the treasury department.
the 9-day bills will be issued april 7 and mature april 16,
while the 20-day bills will be issued april 3 and mature april
23.
 reuter
</text>]