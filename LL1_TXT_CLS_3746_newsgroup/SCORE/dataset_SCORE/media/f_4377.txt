[<text>
<title>varian &lt;var&gt; gets 19 mln dlr subcontract</title>
<dateline>    palo alto, calif., march 12 - </dateline>varian associates inc said it
received a 19 mln dlr subcontract from general electric co &lt;ge&gt;
for 36 100 kilowatt transmitters to be used in the air force's
advanced an/fps-118 over-the-horizon backscatter radar systems.
    nearly 15 mln dlrs was funded immediately, varian said.
 reuter
</text>]