[<text>
<title>internatio-mueller acquires canadian company</title>
<dateline>    rotterdam, april 1- </dateline>internatio-mueller nv &lt;intn.as&gt; said it
will acquire &lt;promac controls inc&gt; of canada but declined to
comment on the amount of the payment, which will be in cash.
    promac, which produces measurement and regulating
equipment, has a work force of 50 and had 1986 turnover of five
mln guilders, an internatio spokesman said.
    he said the takeover fits into the company's drive for
expansion in the u.s and canada and further acquisitions are
possible.
    promac controls will be part of internatio's
electrotechnical sector.
 reuter
</text>]