[<text type="unproc">
smith international inc &lt;sii&gt; 4th qtr loss
    newport beach, calif.
    shr loss 1.95 dlrs vs loss 9.10 dlrs
    net loss 44.4 mln vs 206.8 mln
    revs 84.1 mln vs 172.4 mln
    year
    shr loss 6.56 dlrs vs loss 11.66 dlrs
    net loss 149.0 mln vs loss 265.1 mln
    revs 415.2 mln vs 697.3 mln
    note: current qtr net includes pretax charges of 10.7 mln
dlrs against inventory adjustments and currency costs related
to fluctuations of italian lira.
    current year net includes pretax charges of 46.6 mln dlrs
for provision to reduce carrying value of excess capital assets
and to writedown certain inventories and other assets. also
includes charges of 18 mln dlrs covering costs of workforce
reductions, relocation and chapter 11.
     year-ago net includes pretax charges related to judgment
against company in litigation with hughes tool co of 189.1 mln
for qtr and 216.9 mln dlrs for 12 mths.
 reuter


</text>]