[<text>
<title>&lt;turbo resources ltd&gt; year net</title>
<dateline>    calgary, alberta, march 13 -
    </dateline>oper shr nine cts vs three cts
    oper shr diluted eight cts vs three cts
    oper net 15 mln vs five mln
    revs 518 mln vs 622 mln
    note: oper net excludes extraordinary income of seven mln
dlrs vs four mln on tax loss carryforward, offset by writedowns
of three mln dlrs vs eight mln on u.s. oil and gas properties
and other assets.

 reuter
</text>]