[<text>
<title>cnw &lt;cnw&gt; makes private bond placement</title>
<dateline>    chicago, march 13 - </dateline>cnw corp said its subsidiary, chicago
and north western transportation co, sold in a private
placement 25 mln dlrs of consolidated mortgage 9.9 pct bonds,
series c, due 1992.
    it said morgan stanley and co inc acted as placement agent.
proceeds will be used for general corporate purposes.
 reuter
</text>]