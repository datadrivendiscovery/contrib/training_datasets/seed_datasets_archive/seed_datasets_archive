[<text>
<title>sun microsystems &lt;sunw&gt; contract extended</title>
<dateline>    mountain view, calif., march 23 - </dateline>sun microsystems inc said
it has received a one-year 15 mln dlr renewal of its agreement
to provide interleaf inc &lt;leaf&gt; with sun-3 technical
workstations on an original equipment basis.
 reuter
</text>]