[<text>
<title>handleman co &lt;hdl&gt; sets quarterly</title>
<dateline>    troy, mich., march 11 -
    </dateline>qtly div 14 cts vs 14 cts prior
    pay april 10
    record march 27
 reuter
</text>]