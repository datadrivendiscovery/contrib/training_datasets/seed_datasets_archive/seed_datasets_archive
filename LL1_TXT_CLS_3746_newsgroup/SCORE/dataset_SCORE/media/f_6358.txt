[<text>
<title>henderson land development co ltd &lt;hndh.hk&gt;</title>
<dateline>    hong kong, march 18 - </dateline>six months to dec 31.
    shr 16 h.k. cents vs 11
    interim div seven cents vs five
    net 211.03 mln dlrs vs 138.69 mln
    turnover 583.83 mln dlrs vs 441.04 mln
    note - dividend payable may 4, books close april 21 to 27.
 reuter
</text>]