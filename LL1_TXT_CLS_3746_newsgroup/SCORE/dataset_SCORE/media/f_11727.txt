[<text>
<title>international lease finance &lt;ilfc&gt; 1st qtr net</title>
<dateline>    beverly hills, calif., march 31 -
    </dateline>shr 22 cts vs 13 cts
    net 7,121,000 vs 4,481,000
    revs 37.4 mln vs 22.8 mln
    avg shrs primary 30,067,000 vs 29,735,000
 reuter
</text>]