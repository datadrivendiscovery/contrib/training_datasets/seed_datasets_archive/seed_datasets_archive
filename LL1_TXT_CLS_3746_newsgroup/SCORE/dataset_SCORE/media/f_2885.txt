[<text>
<title>black/decker &lt;bdk&gt; charged with violating patent</title>
<dateline>    aurora, ill., march 6 - </dateline>pittway corp &lt;pry&gt; charged black
and decker inc with infringing on pittway's switch mechanism
patent for two models of pittway's first alert rechargeable
flashlights.
    pittway said it filed suit in u.s. district court, northern
district of illinois, eastern division, alleging black and
decker also violated the copyright and trademark on the
flashlights. the charge also covered packaging and design
details of the lights.
 reuter
</text>]