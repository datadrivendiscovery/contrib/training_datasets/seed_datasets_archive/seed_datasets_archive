[<text>
<title>entex energy development ltd &lt;eed&gt; qtly div</title>
<dateline>    houston, june 19 -
    </dateline>qtly div 15 cts vs 15 cts prior
    payable august 31
    record june 30
 reuter
</text>]