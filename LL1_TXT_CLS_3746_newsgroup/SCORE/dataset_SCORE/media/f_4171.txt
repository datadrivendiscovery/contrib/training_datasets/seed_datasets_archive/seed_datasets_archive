[<text>
<title>saudi supertanker reported hit by iran off uae</title>
<dateline>    london, march 12 - </dateline>iran attacked the saudi arabian
supertanker arabian sea off the united arab emirates last night
but the vessel was able to proceed after the incident, lloyds
shipping intelligence reported.
    the 315,695-dwt arabian sea had set sail on tuesday after
loading oil at the saudi port of ras tannurah. lloyds said the
attack occurred at about 2200 hrs local time (1800 gmt).
 reuter
</text>]