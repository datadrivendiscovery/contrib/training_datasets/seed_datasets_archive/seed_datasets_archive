[<text>
<title>amoskeag bank shares inc &lt;amkg&gt; 1st qtr net</title>
<dateline>    manchester, n.h., april 9 -
    </dateline>shr 70 cts vs 67 cts
    net 6,416,000 vs 6,057,000
    note: net includes pretax securities sales gains of
5,900,000 dlrs vs 5,900,000 dlrs.
 reuter
</text>]