[<text>
<title>franklin pennsylvania tax-free in initial payout</title>
<dateline>    san mateo, calif., april 13 - </dateline>franklin pennsylvania
tax-free income fund said its board declared an initial monthly
dividend of six cts per share, payable april 30 to holders of
record april 15.
 reuter
</text>]