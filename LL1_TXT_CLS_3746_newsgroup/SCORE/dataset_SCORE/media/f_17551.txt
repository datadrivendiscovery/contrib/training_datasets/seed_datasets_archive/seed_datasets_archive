[<text>
<title>video &lt;juke.o&gt; to buy president's shares</title>
<dateline>    miami, june 1 - </dateline>video jukebox network inc said it signed a
letter of intent to purchase up to 3.5 mln shares of the four
mln shares of the company's common stock from its founder and
president, steven peters.
    video said the shares are to be purchased by louis wolfson
iii, senior vice president of &lt;venture w inc&gt;, &lt;national brands
inc&gt;, j. patrick michaels jr and &lt;cea investors partnership
ii&gt;.
    video said it currently has 7,525,000 shares of common
stock outstanding. the company said it went public earlier this
year and its current ask price was 1-7/8.
   
    cea investors partnership ii has planned the partnership to
be operated by michaels, who is chairman and president of
&lt;communications equity associates inc&gt;, a media brokerage firm,
video said.
    the terms of the proposed transaction were not disclosed.
    video said peters will continue as chairman and president
of the company.
    it said the parties have until june 29 to agree to all
terms of the letter of intent.
 reuter
</text>]