[<text>
<title>pico products inc &lt;ppi&gt; 2nd qtr jan 31</title>
<dateline>    liverpool, n.y., march 4 -
    </dateline>shr profit three cts vs loss nine cts
    net profit 103,664 vs loss 326,675
    revs 7.6 mln vs 6.9 mln
    six months
    shr loss two cts vs loss 15 cts
    net loss 78,246 vs loss 522,868
    revs 14.7 mln vs 12.9 mln
    note:1986 net includes gain of 43,185 or one cts in 2nd qtr
and six months for discount on early long-term debt repayment.
 reuter
</text>]