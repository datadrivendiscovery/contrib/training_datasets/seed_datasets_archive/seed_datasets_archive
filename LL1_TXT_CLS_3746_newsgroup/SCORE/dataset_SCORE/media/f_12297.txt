[<text>
<title>northern trust &lt;ntrs&gt; to increase prime rate</title>
<dateline>    chicago, april 1 - </dateline>northern trust co said it is raising its
prime rate to 7-3/4 pct from 7-1/2 pct effective april 2.
 reuter
</text>]