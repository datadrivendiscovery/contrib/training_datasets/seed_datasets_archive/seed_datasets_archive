[<text>
<title>security pacific &lt;spc&gt; registers debt</title>
<dateline>    los angeles, march 23 - </dateline>security pacific corp said it filed
a registration statement with the securities and exchange
commission covering 500 mln dlrs of subordinated debt
securities.
    terms of the debt securities will be determined by market
conditions at the time of sale, the company said.
 reuter
</text>]