[<text>
<title>&lt;grand metropolitan plc&gt; unit to sell business</title>
<dateline>    montvale, n.j., march 9 - </dateline>grand metropolitan plc said its
grandmet usa inc unit decided to sell its physical fitness and
exercise equipment business.
    the company said morgan stanely and co inc is advising it
on the sale of the business.
 reuter
</text>]