[<text>
<title>lotus development corp &lt;lots.o&gt; 3rd qtr net</title>
<dateline>    cambridge, mass., oct 19 -
    </dateline>shr 42 cts vs 21 cts
    net 19.1 mln vs 9,528,000
    sales 101.2 mln vs 65.6 mln
    avg shrs 46.0 mln vs 46.0 mln
    nine mths
    shr 1.08 dlrs vs 69 cts
    net 49.1 mln vs 32.7 mln
    sales 280.0 mln vs 201.0 mln
    avg shrs 45.6 mln vs 47.4 mln
    note: share adjusted for february 1987 two-for-one split.
 reuter
</text>]