[<text>
<title>french farmers say u.s. subsidises more than ec</title>
<dateline>    paris, march 12 - </dateline>the united states subsidises its
agricultural industry much more than the european community,
according to an internal usda report, marcel cazale, president
of the french maize producers' association (agpm), said.
    he told reporters that according to what he described as a
"confidential" report prepared by the usda which he had seen, the
us took the leading place regarding agricultural subsidies,
ahead of the ec.
    cazale said according to the report, argentina and
australia were the only countries not to subsidise their
agriculture at all, or only by a small amount.
    cazale said in argentina subsidies were granted for
exports, but for a very small amount, while in australia
subsidies were also only for exports, amounting to 15 to 30
pct.
    but cazale said australian agriculture received disguised
aid to the extent that fuel and fertilisers benefited from tax
reductions, and transport, electricity and telephones in the
sector had special tariffs. he did not elaborate.
 reuter
</text>]