[<text>
<title>society/savings &lt;socs.o&gt; forms holding company</title>
<dateline>    hartford, conn., june 1 - </dateline>society for savings said it has
completed a merger into newly formed holding company society
for savings bancorp inc on a share-for-share basis.
 reuter
</text>]