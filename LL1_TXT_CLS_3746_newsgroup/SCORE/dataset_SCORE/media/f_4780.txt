[<text>
<title>nortek inc &lt;ntk&gt; sets quarterlies</title>
<dateline>    providence, march 13 -
    </dateline>qtly div common 2-1/2 cts vs 2-1/2 cts prior
    qtly div special common one ct vs one ct
    pay may eight
    record april three
 reuter
</text>]