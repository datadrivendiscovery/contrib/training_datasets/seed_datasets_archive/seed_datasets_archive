[<text>
<title>stop and shop cos &lt;shp&gt; in two-for-one split</title>
<dateline>    boston, april 9 - </dateline>the stop and shop cos inc said its board
voted a two-for-one stock split payable july one, to
stockholders of record may 29.
    it also said it was raising its quarterly cash dividend 16
pct to 32 cts per share from 27.5 cts per share prior.
    as a result of the split, the number of outstanding shares
will increase to 28 mln from 14 mln, the company said.
    the dividend is payable july one to shareholders of record
may 29, it said.
 reuter
</text>]