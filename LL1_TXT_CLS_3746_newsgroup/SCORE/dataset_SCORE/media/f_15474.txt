[<text>
<title>independent air &lt;iair&gt; extends warrants</title>
<dateline>    smyrna, tenn., april 9 - </dateline>independent air inc said its board
has extended the expiration of its class a warrants to june 30
from april 11 and left the exercise price unchanged at 18 cts
per common share.
 reuter
</text>]