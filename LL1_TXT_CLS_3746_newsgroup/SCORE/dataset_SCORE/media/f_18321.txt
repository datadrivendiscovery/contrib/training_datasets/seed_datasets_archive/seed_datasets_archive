[<text>
<title>treasury to publish u.s. reserve assets monthly</title>
<dateline>    washington, june 2 - </dateline>the treasury department said it would
release data on u.s. reserve assets on a monthly basis from now
on instead of quarterly.
    reserve assets are held in gold, special drawing rights
with the internatinal monetary fund, foreign currencies and in
a u.s. reserve position in the imf.
    assets totaled 46.59 billion dlrs at the end of april,
compared with 48.82 billion dlrs at the end of march, the
treasury said.
 reuter
</text>]