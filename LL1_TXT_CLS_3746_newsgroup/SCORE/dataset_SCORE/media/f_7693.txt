[<text>
<title>vertex &lt;vetx&gt; to buy computer transceiver stake</title>
<dateline>    clifton, n.j., march 20 - </dateline>vertex industries inc and
&lt;computer transceiver systems inc&gt; jointly announced an
agreement for vertex to acquire a 60 pct interest in computer
after it completes a proposed reorganization.
    computer has been in reorganization proceedings under
chapter 11 since september 1986.
    the companies said the agreement would allow computer's
unsecured creditors and debenture holders to receive new stock
in exchange for exsiting debt, and for shareholders to receive
one new share of computer's stock for each four shares
previously held.
   
    the companies said the united states bankruptcy court for
the southern district of new york has given preliminary
approval for the proposal, which is subject to formal approval
by computer's creditors and the court.
    under the agreement, vertex also said it would supply
computer with 250,000 dlrs of operating funds, and arrange
renegotiation of its secured bank debt, among other things.
 reuter
</text>]