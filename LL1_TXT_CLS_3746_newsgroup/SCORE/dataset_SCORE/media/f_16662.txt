[<text>
<title>rhnb corp &lt;rhnb&gt; 1st qtr net</title>
<dateline>    rock hill, s.c., april 13 -
    </dateline>shr 61 cts vs 55 cts
    net 695,252 vs 633,329
    loans 125.2 mln vs 89.9 mln
    deposits 209.2 mln vs 172.9 mln
    assets 245.5 mln vs 207.5 mln
 reuter
</text>]