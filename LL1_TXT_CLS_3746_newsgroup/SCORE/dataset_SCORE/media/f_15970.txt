[<text>
<title>mcdonnell douglas &lt;md&gt; buys computer firm</title>
<dateline>    st. louis, april 9 - </dateline>mcdonnell douglas corp said it
acquired frampton computer services ltd, a british software
company that is also known as isis.
    terms of the acquisition were not disclosed.
    based in bristol, england, isis employs 65 workers and has
annual revenues of about five mln dlrs, mcdonnell douglas said.
    the company added that isis will operate as part of
mcdonnell douglas information systems international.
 reuter
</text>]