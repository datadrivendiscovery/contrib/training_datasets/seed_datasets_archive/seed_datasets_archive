[<text>
<title>environmental power &lt;powr&gt; unit gets contract</title>
<dateline>    boston, march 4 - </dateline>environmental power corp said its
subsidiary, environmental protection resources inc, of houston,
has been hired to build a 20 mln dlr resource recovery plant
for lubbock, texas.
    environmental said its subsidiary also will own and operate
the plant, which will convert solid waste into electricity.
    the plant is scheduled for operation by early 1989, the
company said.
 reuter
</text>]