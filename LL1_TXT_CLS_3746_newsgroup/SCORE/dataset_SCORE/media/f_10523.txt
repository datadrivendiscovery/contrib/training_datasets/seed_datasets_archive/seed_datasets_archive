[<text>
<title>north hills &lt;nohl&gt; gets construction funding</title>
<dateline>    glen cove, n.y., march 27 - </dateline>north hills electronics inc
said its north hills israel ltd unit received a commitment for
1,500,000 dlrs in financing from the &lt;overseas private
placement corp&gt;.
    the company said the money will be used to construct and
equip a 14,000-square-foot manufacturing plant in israel.
    north hills said construction of the plant, which will make
electronic power supplies and interconnect devices for computer
local area networks, will be completed by the second quarter of
its current fiscal year.
 reuter
</text>]