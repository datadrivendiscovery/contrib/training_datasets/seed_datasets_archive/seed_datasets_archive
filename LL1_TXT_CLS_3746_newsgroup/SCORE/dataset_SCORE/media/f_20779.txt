[<text>
<title>zero corp &lt;zro&gt; 2nd quarter net</title>
<dateline>    los angeles, oct 20 -
    </dateline>shr 27 cts vs 20 cts
    net 3,411,000 vs 2,487,000
    revs 34.7 mln vs 31.9 mln
    six mths
    shr 51 cts vs 41 cts
    net 6,372,000 vs 5,090,000
    revs 68.1 mln vs 63.7 mln
    avg shrs 12.6 mln vs 12.5 mln
 reuter
</text>]