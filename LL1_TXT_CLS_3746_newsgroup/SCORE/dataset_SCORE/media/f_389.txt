[<text>
<title>senior engineering makes 12.5 mln dlr us purchase</title>
<dateline>    london, march 2 - </dateline>&lt;senior engineering group plc&gt; said it
reached agreement with &lt;cronus industries inc&gt; to acquire the
whole share capital of &lt;south western engineering co&gt; for 12.5
mln dlrs cash. this sum is being financed by a term loan.
    south western is one of the u.s.'s leading manufacturers of
heat transfer equipment, with a turnover of 54.86 mln dlrs and
pre-tax profits of 1.72 mln in 1986.
    completion of the deal is conditional on approval under
u.s. hart-scott-rodino regulations which is expected within 30
days. some 350,000 dlrs is payable immediately, 12 mln dlrs
payable on completion with the balance due by june 30, 1987.
 reuter
</text>]