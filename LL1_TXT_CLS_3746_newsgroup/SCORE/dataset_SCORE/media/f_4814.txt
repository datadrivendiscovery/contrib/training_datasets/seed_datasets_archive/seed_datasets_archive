[<text>
<title>manufactured homes inc &lt;mnh&gt; year net</title>
<dateline>    winston-salem, n.c., march 13 -
    </dateline>shr 53 cts vs 85 cts
    net 2,033,425 vs 3,718,325
    revs 120.6 mln vs 79.5 mln
    note: 1986 net includes 3,300,000 dlr provision for credit
loss.
    1985 net includes charge 504,571 dlrs from cumulative
effect of accounting change.
 reuter
</text>]