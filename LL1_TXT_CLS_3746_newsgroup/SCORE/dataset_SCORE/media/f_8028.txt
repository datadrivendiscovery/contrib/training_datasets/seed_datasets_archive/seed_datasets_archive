[<text>
<title>eastpark realty trust &lt;ert&gt; qtly div</title>
<dateline>    jackson, miss, march 20 -
    </dateline>qlty div 25 cts vs 25 cts prior
    payable april 22
    record april 10
 reuter
</text>]