[<text>
<title>pechiney to sell part of stake in canada plant</title>
<dateline>    paris, april 29 - </dateline>french state-owned aluminium and special
metals group pechiney &lt;pukg.pa&gt; is planning to sell 25 pct of a
canadian aluminium plant at becancour to the u.s.'s reynolds
metals co &lt;rlm.n&gt; over the coming weeks, the company said.
     pechiney currently holds a majority 50.1 pct stake in the
plant. the company also said it planned an agreement soon with
italy's &lt;la metalli industriale&gt;, a 57 pct owned subsidiary of
&lt;societa metallurgica italiana&gt; related to cupreous products.
no further details were available.
 reuter
</text>]