[<text>
<title>japan central bank actively buys dollars in tokyo</title>
<dateline>    tokyo, april 13 - </dateline>the bank of japan actively bought dollars
here in early afternoon trade at around 142.20 yen, dealers
said.
    the central bank had placed buy orders at that level and
prevented the dollar from falling when it came under heavy
selling pressure from investment trusts and trading houses,
they said.
    however, the intervention failed to boost the u.s. currency
significantly from the 142.20 yen level, they added.
    the dollar was trading around its midday rate of 142.30
yen. it had opened here at 141.85 yen.
 reuter
</text>]