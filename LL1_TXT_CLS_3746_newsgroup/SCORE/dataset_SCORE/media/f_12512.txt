[<text>
<title>american barrick &lt;abx&gt; sells ohio assets</title>
<dateline>    toronto, april 2 - </dateline>american barrick resources corp said it
sold two coal supply agreements and substantially all assets at
its two coal mines in ohio to &lt;peabody coal co&gt; of henderson,
kentucky, for an undisclosed price.
    the company said proceeds from this sale, together with the
sale of the remaining coal assets, should allow it to fully
recover its investment in the operations.
    it said the transactions will complete its previously
announced plan to sell all non-gold assets.
 reuter
</text>]