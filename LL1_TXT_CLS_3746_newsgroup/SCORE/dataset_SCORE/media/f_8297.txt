[<text>
<title>hemodynamics inc &lt;hmdy&gt; year loss</title>
<dateline>    boca raton, fla., march 23 -
    </dateline>shr loss 24 cts vs loss 41 cts
    net loss 148,070 vs loss 251,225
    sales 1,298,257 vs 319,588
 reuter
</text>]