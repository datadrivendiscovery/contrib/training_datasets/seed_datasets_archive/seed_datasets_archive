[<text>
<title>hadson corp &lt;hads.o&gt; completes acquisition</title>
<dateline>    oklahoma city, okla., april 13 - </dateline>hadson corp said it
completed the acquisition of 85 pct of the seaxe energy corp's
&lt;seax.o&gt; common stock for 182,415 shares of hadson's stock.
    seaxe is engaged in international oil and gas exploration
and development primarily in the paris basin of france.
 reuter
</text>]