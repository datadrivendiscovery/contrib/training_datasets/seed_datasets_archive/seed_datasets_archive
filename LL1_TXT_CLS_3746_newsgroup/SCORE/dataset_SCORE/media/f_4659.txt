[<text>
<title>county increases tesco stake in hillards</title>
<dateline>    london, march 13 - </dateline>&lt;county ltd&gt;, which is acting in concert
with tesco plc &lt;tsco.l&gt; in its 151.4 mln stg for supermarket
chain operator &lt;hillards plc&gt;, has purchased 300,000 hillards
ordinary shares at 316p per share, a statement said.
    these purchases, together with those made by county on 10
march, represent about 4.8 pct of hillards issued ordinary
share capital, it said.
    tesco's offer, made on march 10, values each hillards
ordinary share at 305.5p, a 37.6 pct premium over the previous
day's  closing price. a cash alternative of 290.55p will be
made available.
 reuter
</text>]