[<text>
<title>american republic bancorp &lt;arbc.o&gt; 3rd qtr net</title>
<dateline>    torrance, calif., oct 19 -
    </dateline>shr profit 32 cts vs profit nine cts
    net profit 413,000 vs profit 63,000
    avg shrs 1,278,360 vs 728,476
    nine mths
    shr profit 68 cts vs loss 57 cts
    net profit 708,000 vs loss 415,000
    avg shrs 1,041,697 vs 728,476
 reuter
</text>]