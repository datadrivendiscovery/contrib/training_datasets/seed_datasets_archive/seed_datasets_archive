[<text>
<title>becor western &lt;bcw&gt; gets offer to be acquired</title>
<dateline>    milwaukee, march 16 - </dateline>becor western inc said &lt;investment
limited partnership&gt; of greenwich, conn., and randolph w. lenz
are offering to acquire becor for 15.50 dlrs per share, subject
to becor's receipt of at least 110 mln dlrs from the proposed
sale of its western gear corp subsidiary.
    becor said it has also received expressions of interest
from other parties seeking information about becor.
    becor had previously agreed to sell western gear for at
least 110 mln dlrs and to be acquired by bcw acquisition inc
for 10.45 dlrs in cash and four dlrs in debentures per becor
share.  bcw was formed by becor executives and &lt;goldman, sachs
and co&gt;.  both deals are subject to shareholder approval.
 reuter
</text>]