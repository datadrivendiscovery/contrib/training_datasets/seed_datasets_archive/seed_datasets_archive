[<text>
<title>getty petroleum &lt;gty&gt; year net</title>
<dateline>    plainview, n.y., march 31 -
    </dateline>shr 1.54 dlrs vs 1.09 dlrs
    qtly div four cts vs four cts prior
    net 17.1 mln vs 11.5 mln
    revs 953.2 mln vs 1.33 billion
    avg shrs 11.1 mln vs 10.5 mln
    note: cash dividend payable april 21 to holders of record
april 10. shr figures adjusted for five pct stock dividend
declared march 31.
 reuter
</text>]