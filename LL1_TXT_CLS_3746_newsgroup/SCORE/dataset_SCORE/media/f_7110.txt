[<text>
<title>cyclops &lt;cyl&gt;board restructured to include dixon</title>
<dateline>    pittsburgh, pa., march 19 - </dateline>cyclops corp said its board has
been restructured under the terms of the company's merger
agreement with &lt;dixons group plc&gt; following the british
company's acquisition of 54 pct of cyclops' stock.
    the company said its board is now composed of three cyclops
executives -- chairman w.h. knoell, president james f. will and
senior vice president william d. dickey -- and three dixons
executives -- vice-chairman and financial director egon von
greyerz, corporate finance director gerald m.n. corbett, and
secretary jeoffrey budd.
 reuter
</text>]