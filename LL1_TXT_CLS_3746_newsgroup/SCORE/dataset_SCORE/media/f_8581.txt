[<text>
<title>ambrit inc &lt;abi&gt; 4th qtr jan 31 net</title>
<dateline>    clearwater, fla., march 23 - 
    </dateline>shr 28 cts vs nil
    net 4,568,000 vs 7,000
    revs 37.5 mln vs 7,835,000
    year
    shr 13 cts vs nil
    net 5,011,000 vs 30,000
    revs 145.5 mln vs 51.0 mln
    note: current year results includes revs of 87.2 mln dlrs
from chocolate co inc, which was acquired in march 1986.
    note: shr results after preferred dividend payments of
954,000 dlrs for current qtr and 3,410,000 dlrs for current
year.
    net includes gains from sale of investment in sheraton
securities international of 5,807,000 dlrs vs 928,000 dlrs for
qtr and 8,705,000 dlrs vs 928,000 dlrs for year.
    net also includes extraordinary loss from early retirement
of debt of 303,000 dlrs for year-ago 12 mths.
 reuter
</text>]