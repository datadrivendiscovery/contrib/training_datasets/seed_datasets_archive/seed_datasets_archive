[<text>
<title>&lt;mesa airlines inc&gt; initial offering underway</title>
<dateline>    new york, march 11 - </dateline>lead underwriters josephthal and co
inc, emmett a. larkin co inc and hays securities corp said an
initial public offering of 865,000 common shares of mesa
airlines inc is underway at 7.50 dlrs per share.
    underwriters have been granted an overallotment option to
purchase up to another 129,750 shares.
 reuter
</text>]