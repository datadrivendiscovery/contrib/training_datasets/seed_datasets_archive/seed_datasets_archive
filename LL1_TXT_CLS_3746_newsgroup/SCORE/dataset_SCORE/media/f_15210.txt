[<text>
<title>corrected - datron &lt;datr&gt; agrees to buyout</title>
<dateline>    minneapolis, minn., april 8 - </dateline>datron corp said it agreed to
merge with gghf inc, a florida-based company formed by the four
top officers of the company.
    according to terms of the proposed transaction, each share
of datron common stock, excluding those shares owned by the
four officers, will be converted into six dlrs a share, it
said.
    datron's officers hold about 73 pct of the total 896,000
datron common shares outstanding, it said.
(corrects company name, gghf, in first paragraph)
 reuter
</text>]