[<text>
<title>american cyanamid &lt;acy&gt; changes record date</title>
<dateline>    wayne, n.j., march 24 - </dateline>american cyanamid co said subject
to approval by its board it has changed the record date for the
quarterly dividend it will pay on june 26 to may 8 from may 29
to coincide with the record date for a two-for-one stock split
that was declared at the same time.
    the dividend on a post-split basis is 26-1/4 cts per share.
 reuter
</text>]