[<text>
<title>&lt;franklin gold fund&gt; cuts dividend</title>
<dateline>    san mateo, calif., march 2 -
    </dateline>semi div 13 cts vs 18 cts prior
    pay march 13
    record march two
 reuter
</text>]