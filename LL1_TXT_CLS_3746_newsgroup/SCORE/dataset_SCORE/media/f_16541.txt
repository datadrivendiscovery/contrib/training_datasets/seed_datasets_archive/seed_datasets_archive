[<text>
<title>cbt t-note options set volume record</title>
<dateline>    chicago, april 13 - </dateline>the chicago board of trade, cbt,
achieved record volume in trading of 10-year u.s. treasury note
futures-options april 10.
    volume reached 38,402 contracts, up from the previous
record of 16,349 contracts set april 9.
 reuter
</text>]