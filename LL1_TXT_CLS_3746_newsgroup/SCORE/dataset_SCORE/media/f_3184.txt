[<text>
<title>bankamerica &lt;bac&gt; completes mortgage unit sale</title>
<dateline>    san francisco, march 9 - </dateline>bankamerica corp's bank of america
unit said it completed the previously announced sale of
bankamerica finance ltd to &lt;bank of ireland&gt; for an expected
pre-tax gain of 23 mln dlrs.
    bankamerica finance provides residential mortgages in south
east england.
    its total assets at the end of 1986 were about 1.2 mln
dlrs.
 reuter
</text>]