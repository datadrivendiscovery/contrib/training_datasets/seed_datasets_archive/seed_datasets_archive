[<text>
<title>north american &lt;nahl&gt; sees record fourth qtr</title>
<dateline>    east hartford, conn, march 12 - </dateline>north american holding corp
said it expects to have record fourth quarter earnings and for
sales to exceed 10 mln dlrs.
    for the fourth quarter ended march 30, 1986 the company
reported net income of 631,720 dlrs on sales of 5.2 mln dlrs.
    it also said it expects revenues for the year to exceed 30
mln dlrs. for fiscal 1986, north american reported a net loss
of 126,900 dlrs on sales of 12.8 mln dlrs.
 reuter
</text>]