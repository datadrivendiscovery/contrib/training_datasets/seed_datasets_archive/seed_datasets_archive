[<text>
<title>owens and minor inc &lt;obod&gt; raises qtly dividend</title>
<dateline>    richmond, va. feb 26 -
    </dateline>qtly div eights cts vs 7.5 cts prior
    pay march 31
    record march 13
 reuter
</text>]