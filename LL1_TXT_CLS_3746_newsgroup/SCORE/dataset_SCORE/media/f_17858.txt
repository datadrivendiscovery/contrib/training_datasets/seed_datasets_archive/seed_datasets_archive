[<text>
<title>elsinore &lt;els&gt; makes deposit to cover bonds</title>
<dateline>    new york, june 1 - </dateline>elsinore corp said it deposited five mln
dlrs to cover accrued and unpaid interest through june 30 of
the 15-1/2 pct senior mortgage bonds of 1999 of its unit
elsinore finance corp.
    elsinore has guaranteed the payment of its unit's bonds.
    the parent deposited the five mln dlrs with elsinore
finance's trustee, manufacturers hanover trust co, it said.
 reuter
</text>]