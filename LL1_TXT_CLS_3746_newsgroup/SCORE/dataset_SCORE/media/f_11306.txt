[<text>
<title>terra mines ltd &lt;tmexf&gt; year loss</title>
<dateline>    edmonton, alberta, march 31 -
    </dateline>shr loss 89 cts vs loss 17 cts
    net loss 13.9 mln vs loss 1,996,000
    revs 204,000 vs 2,087,000
    note: 1986 includes writedown of 12.5 mln dlrs for the
costs of mineral properties and deferred exploration and
development bullmoose lake in the northwest territories.
 reuter
</text>]