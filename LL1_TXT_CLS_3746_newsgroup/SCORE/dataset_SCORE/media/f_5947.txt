[<text>
<title>canadian foremost continues macedon sale talks</title>
<dateline>    calgary, alberta, march 17 - </dateline>&lt;canadian foremost ltd&gt;,
earlier reporting lower 1986 net profit, said negotiations are
continuing concerning the previously announced sale of the
company's 49 pct interest in &lt;macedon resources ltd&gt;.
    if concluded, the sale would be refelected in the company's
1987 results, foremost said without elaborating.
    it also said lower revenues from the last half of 1986 are
expected to continue during 1987, but a strong cash and working
capital position will enable foremost to go on developing
traditional and new markets. it earlier said 1986 earnings fell
to 1,042,000 dlrs from year-ago 2,510,000 dlrs.
 reuter
</text>]