[<text>
<title>vernitron &lt;vrn&gt; sets record date for merger vote</title>
<dateline>    deer park, n.y., june 29 - </dateline>vernitron corp said it has set a
new record date for shareholders entitled to vote on the
proposed merger of vernitron corp with sb holding corp for july
10.
    vernitron, which manufactures electromechanical components
and related products and services, said it expects that a
special meeting of shareholders will be held in august.
    vernitron said that sb holding holds 55.2 pct in vernitron,
resulting from a tender offer in november 1986.
 reuter
</text>]