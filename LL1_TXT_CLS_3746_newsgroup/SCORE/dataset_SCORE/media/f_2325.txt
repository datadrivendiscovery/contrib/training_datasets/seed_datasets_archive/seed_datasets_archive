[<text>
<title>bishop graphics &lt;bgph&gt; completes store sale</title>
<dateline>    westlake village, calif., march 5 - </dateline>bishop graphics inc
said it completed the sale of its newport beach art supply
center to standard brands paint co's &lt;sbp&gt; art store unit.
    terms were not disclosed.
    bishop also said it has opened a new sales and service
office in irvine, calif.
   
 reuter
</text>]