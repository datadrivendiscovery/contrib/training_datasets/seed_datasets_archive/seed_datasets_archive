[<text>
<title>wisconsin power and light co &lt;wpl&gt;votes payout</title>
<dateline>    madison, wis., march 19 -
    </dateline>qtly div 76 cts vs 76 cts prior qtr
    pay 15 may
    record 30 april
 reuter
</text>]