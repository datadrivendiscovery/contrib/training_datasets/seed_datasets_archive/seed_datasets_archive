[<text>
<title>usp real estate investment trust &lt;uspts.o&gt; 3rd</title>
<dateline>    cedar rapids, iowa, oct 20 -
    </dateline>shr eight cts vs 10 cts
    qtly div 30 cts vs 30 cts prior
    net 204,064 vs 245,931
    nine mths
    shr 1.14 dlrs vs 52 cts
    net 2,850,042 vs 1,291,047
    note: 1987 and 1986 nine mths includes a net gain on sale
of assets of 2,258,206 dlrs or 90 cts a share and 459,503 dlrs,
respectively. dividend payable november 13 to shareholders or
record october 30.
 reuter
</text>]