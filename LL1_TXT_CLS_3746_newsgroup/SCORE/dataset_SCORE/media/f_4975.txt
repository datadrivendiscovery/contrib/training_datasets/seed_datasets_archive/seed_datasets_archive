[<text>
<title>j.p. morgan &lt;jpm&gt; official dies</title>
<dateline>    new york, march 13 - </dateline>j.p. morgan and co said peter f.
culver, 42, senior vice president of its morgan guaranty trust
co subsidiary and general manager of the euro-clear operations
centre in brussels, has died of an apparent heart attack at a
business meeting in frankfurt.
 reuter
</text>]