[<text>
<title>&lt;summit petroleum corp&gt; sells shares</title>
<dateline>    abilene, texas, march 12 - </dateline>summit petroleum corp said it
sold 11.3 mln shares, or 29.4 pct, of its common stock to
&lt;halbert and associates inc&gt;.
    the company said the shares were previously held by
&lt;consolidated energy corp&gt; and harken oil and gas inc &lt;hogi&gt;.
    in addition, david d. halbert, president and chief
executive officer of halbert, an abilene investment firm, was
named chairman and chief executive of summit, the company said.
   halbert, charles m. bruce and james o. burke were also named
directors, expanding the board to five, summit added.
    the company said burke is president and chief executive of
&lt;allied comprehensive health inc&gt;, abilene, while bruce is a
partner in the washington law firm of butler and binion.
    summit said it intends to actively seek acquisitions to
increase its asset base.
 reuter
</text>]