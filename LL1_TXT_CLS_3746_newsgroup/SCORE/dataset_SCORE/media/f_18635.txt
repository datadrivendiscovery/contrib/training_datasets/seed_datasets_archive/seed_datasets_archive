[<text>
<title>american biomaterials &lt;abci.o&gt; in rights offer</title>
<dateline>    princeton, n.j., june 18 - </dateline>american biomaterials corp said
it will issue one transferable right to each shareholder of
record yesterday for each three shares held.
    it said each of the 2,920,280 rights allows the holder to
buy one common share for three dlrs until july 15 expiration.
    the company said hallwood group inc &lt;hwg&gt; has agreed to buy
all shares not purchased in the rights offering, except those
connected with rights issued to current and former directors
owning a 30.2 pct interest.  persons exercising rights will be
able to oversubscribe, the company said.
 reuter
</text>]