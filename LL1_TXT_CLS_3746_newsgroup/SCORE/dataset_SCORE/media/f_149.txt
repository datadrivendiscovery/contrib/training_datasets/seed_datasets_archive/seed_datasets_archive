[<text>
<title>r.p. scherer &lt;schc&gt; sets preferred stock offer</title>
<dateline>    troy, mich., feb 26 - </dateline>r.p. scherer inc said it registered
with the securities and exchange commission a proposed public
offering of 1.2 mln shares of convertible exchangeable
preferred stock at 25 dlrs a share.
    in addition, the company said it is offering 200,000
preferred shares to richard manoogian, a scherer director at 25
dlrs a share. manoogian said he will buy the 200,000 shares.
    proceeds from the offering will be used to repay debt and
to fund research and development, it said.
    underwriters are led by goldman, sachs and co.
 reuter
</text>]