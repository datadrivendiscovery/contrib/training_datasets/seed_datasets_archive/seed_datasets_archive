[<text>
<title>pepsi &lt;pep&gt; names new ad agency for black market</title>
<dateline>    new york, june 29 - </dateline>pepsico inc's pepsi-cola usa said it
named privately owned lockhart and pettus inc of new york to
handle its advertising and promotion to black consumers.
    pepsi-cola usa said it is the second major new account for
lochart and pettus in as many weeks. earlier this month,
chrysler motors corp named the agency to handle its minority
marketing.
 reuter
</text>]