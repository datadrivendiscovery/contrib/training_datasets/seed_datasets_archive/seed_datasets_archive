[<text>
<title>two ford &lt;f&gt; plants to work saturday</title>
<dateline>    detroit, march 19 - </dateline>ford motor co said one of its u.s. car
and one of its u.s. truck assembly plants will work overtime on
saturday, march 21.
    the number two automaker said all of its u.s. car and truck
plants will work overtime during the week.
    the two plants scheduled to work saturday are ford's
assembly plant in dearborn, mich., which makes mustang cars,
and its michigan truck plant in wayne, mich., which makes light
trucks.
    no layoffs are scheduled, a spokesman said.
 reuter
</text>]