[<text>
<title>alex brown inc &lt;absb&gt; 1st qtr march 27 net</title>
<dateline>    baltimore, april 8 -
    </dateline>shr primary 78 cts vs 68 cts
    shr diluted 75 cts vs 68 cts
    qtrly div six cts vs five cts
    net 7,929,000 vs 6,569,000
    revs 78.7 mln vs 61.9 mln
    note: pay date for the qtrly div is april 28 for
shareholders of record april 20.
 reuter
</text>]