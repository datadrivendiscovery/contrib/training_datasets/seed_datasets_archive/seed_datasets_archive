[<text>
<title>wal-mart stores &lt;wmt&gt; march sales rise</title>
<dateline>    bentonville, ark., april 9 - </dateline>wal-mart stores inc said march
sales were up 32 pct to 1.13 billion dlrs from 855 mln dlrs a
year earlier, with same-store sales up eight pct.
    the company said sales for the first two months of its
fiscal year were up 37 pct to 2.01 billion dlrs from 1.47
billion dlrs a year before, with same-store sales up 11 pct.
 reuter
</text>]