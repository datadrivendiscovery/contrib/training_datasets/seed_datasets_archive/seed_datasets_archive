[<text>
<title>usda offers eep barley malt to colombia</title>
<dateline>    washington, april 3 - </dateline>the u.s. agriculture department
announced colombia has been made eligible for sales of up to
15,000 tonnes of barley malt under the department's export
enhancement program, eep.
    as with the previous 64 eep initiatives, sales of u.s.
barley malt would be made to buyers in colombia at competitive
world prices, usda said.
    the export sales would be subsidized with commodities from
the inventory of the commodity credit corp and enable u.s.
exporters to compete at commercial prices in the colombian
market, usda said.
 reuter
</text>]