[<text>
<title>new dutch advances total 4.1 billion guilders</title>
<dateline>    amsterdam, april 3 - </dateline>the duth central bank said it accepted
bids totalling 4.086 billion guilders at tender for the new
twelve-day special advances at an unchanged 5.3 pct.
    bids up to 30 mln guilders were met in full, amounts above
at 40 pct.
    the new advances, covering the period april 3 to 15,
replace the current 4.2 billion guilder nine-day facility at
5.3 pct, which expires today.
    money market dealers said the total amount allocated was in
line with expectations and would be sufficient to cover the
money market shortage for the duration of the facility.
 reuter
</text>]