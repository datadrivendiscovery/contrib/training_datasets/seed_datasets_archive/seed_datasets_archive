[<text>
<title>evans/southerland &lt;escc&gt; sells convertible debt</title>
<dateline>    new york, march 13 - </dateline>evans and southerland computer corp is
raising 50 mln dlrs through an offering of convertible
subordinated debentures due 2012 with a 6-1/2 pct coupon and
par pricing, said sole manager hambrecht and quist inc.
    the debentures are convertible into the company's common
stock at 48.50 dlrs per share, representing a premium of 24.36
pct over the stock price when terms on the debt were set.
    non-callable for two years, the issue is rated b-plus by
standard and poor's corp.
 reuter
</text>]