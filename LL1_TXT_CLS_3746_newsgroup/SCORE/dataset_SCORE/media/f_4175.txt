[<text>
<title>possis &lt;poss&gt; votes 100 pct stock dividend</title>
<dateline>    minneapolis, march 12 - </dateline>possis corp said its board approved
a 100 pct stock dividend payable may one, record march 27.
    at the company's annual meeting wednesday, possis said
shareholders approved a proposal to increase the authorized
common shares to 20 mln from eight mln. the company currently
has about 3.9 mln shares outstanding.
 reuter
</text>]