[<text>
<title>baker says u.s. not seeking iadb veto</title>
<dateline>    washington, march 17 - </dateline>treasury secretary james baker said
that the united states was not seeking loan veto in its
negotiation for voting power at the inter-american development
bank.
    discussing the ongoing negotiation, baker said the u.s.
position is that the countries that pay in the most should have
the greatest say over the loan decisions.
    the u.s. is seeking the power to block loan votes with one
other country, and the issue will be discussed in miami later
this month at the annual meeting of the latin development bank.
 reuter
</text>]