[<text>
<title>congress video group inc &lt;cvgi&gt; 3rd qtr net</title>
<dateline>    new york, march 2 - </dateline>qtr ends dec 31
    shr profit three cts vs loss three cts
    net profit 129,000 vs loss 85,000
    revs 4,001,000 vs 4,347,000
    avg shrs 3,994,347 vs 3,769,347
    nine mths
    shr loss 75 cts vs profit 39 cts
    net loss 2,900,000 vs profit 1,753,000
    revs 7,472,000 vs 15.3 mln
    avg shrs 3,845,438 vs 4,470,275
    note: net 1986 includes tax gain carryforward from
discontinued operations of master's merchandise group in year
prior.
 reuter
</text>]