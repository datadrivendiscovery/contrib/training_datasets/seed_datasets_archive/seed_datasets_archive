[<text>
<title>erly industries &lt;erly&gt; makes loan to hanson</title>
<dateline>    los angeles, april 3 - </dateline>erly industries inc said it provided
privately-owned hanson foods inc with a 23 mln dlr credit line
needed to carry out its voluntary reorganization plan.
    the plan calls for about 20 mln dlrs of the credit facility
to be used to retire outstanding bank debt and to pay trade
creditors, with the balance to be used for working capital,
erly said.
 reuter
</text>]