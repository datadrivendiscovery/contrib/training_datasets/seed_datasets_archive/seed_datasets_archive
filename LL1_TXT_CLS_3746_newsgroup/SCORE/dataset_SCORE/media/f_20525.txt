[<text>
<title>stanadyne &lt;stna.o&gt; purchases ambac stake</title>
<dateline>    windsor, conn., oct 20 - </dateline>stanadyne inc said it has acquired
a substnatial majority of ambac s.p.a.'s outstanding stock from
&lt;ail corp&gt;, the successor to united technologies corp's &lt;utx&gt;
diesel systems division for undisclosed terms.
    in addition to purchase a majority of the brescia, italy,
based company, stanadyne said, it acquired a minority interest
in ambac's u.s. operation headquartered in columbia, s.c.
 reuter
</text>]