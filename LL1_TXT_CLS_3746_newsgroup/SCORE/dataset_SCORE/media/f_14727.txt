[<text>
<title>baker, volcker meet west german officials</title>
<dateline>    washington, april 7 - </dateline>treasury secretary james baker and
federal reserve board chairman paul volcker held a meeting with
senior west german monetary officials, including finance
minister gerhard stoltenberg, volcker said.
    volcker emerged after two-hours in the treasury building
but declined to comment on the subjects they discussed.
    stoltenberg, accompanied by other senior west german
officials attending the spring meeting of the international
monetary fund, left the treasury without speaking to reporters.
 reuter
</text>]