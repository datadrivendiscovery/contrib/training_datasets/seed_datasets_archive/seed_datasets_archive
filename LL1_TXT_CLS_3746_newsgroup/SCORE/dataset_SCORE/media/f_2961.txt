[<text>
<title>&lt;consolidated plantations bhd&gt;</title>
<dateline>    kuala lumpur, march 7 - </dateline>six months to december 31
    shr 2.6 cents vs 5.2 cents
    interim dividend four cents vs same
    group net 12.4 mln ringgit vs 24.3 mln
    pre-tax 26.6 mln vs 53.5 mln
    turnover 235.3 mln vs 333.9 mln
    note - dividend pay april 30, register april 3.
 reuter
</text>]