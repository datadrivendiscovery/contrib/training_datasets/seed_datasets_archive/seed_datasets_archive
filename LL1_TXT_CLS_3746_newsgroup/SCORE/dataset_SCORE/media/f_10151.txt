[<text>
<title>california micro &lt;camd&gt;, grumman &lt;gq&gt; set pact</title>
<dateline>    milpitas, calif., march 26 - </dateline>california micro devices corp
said it has signed an agreement with grumman corp's tachonics
corp unit to develop and product gallium arsenide
seminconductor chips.
    under the pact, california micro devices will design the
chips and tachonics will manufacture them.
    initial products to be developed will be gate arrays with
500 to 2,500 gate complexity and radiation hardening
capabilities, the company said.
 reuter
</text>]