[<text>
<title>ionics inc &lt;ion&gt; 4th qtr net</title>
<dateline>    watertown, mass., march 17 -
    </dateline>shr 11 cts vs 24 cts
    net 419,000 vs 938,000
    revs 16.2 mln vs 16.9 mln
    year
    shr 25 cts vs 95 cts
    net 952,000 vs 3,001,000
    revs 64.6 mln vs 68.8 mln
    backlog 30.3 mln vs 31.9 mln
    note: 1986 net includes nonrecurring gain 383,000 dlrs in
quarter and charge 175,000 dlrs in year.
 reuter
</text>]