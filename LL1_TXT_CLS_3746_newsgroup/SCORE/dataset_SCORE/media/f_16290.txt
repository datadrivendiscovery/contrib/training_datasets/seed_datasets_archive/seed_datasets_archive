[<text>
<title>enel issues 15 billion yen eurobond</title>
<dateline>    london, april 13 -  </dateline>italy's state-owned ente nazionale per
l'energia elettrica (enel) is issuing a 15 billion yen eurobond
due may 27, 1994 paying 4-3/4 pct and priced at 101-7/8 pct,
joint-lead bookrunner ibj international ltd said. morgan
stanley international is the other joint-lead bookrunner and
appears on the left in documentation.
    the non-callable bond is available in denominations of one
mln yen and will be listed in luxembourg. the selling
concession is 1-1/4 pct while management and underwriting
combined pays 5/8 pct. the payment date is may 27.
 reuter
</text>]