[<text>
<title>morgan, olmstead, kennedy &lt;mokg&gt; names pres</title>
<dateline>    los angeles, april 9 - </dateline>morgan, olmstead, kennedy and
gardner capital corp said michael fusco has been named
president of its securities brokerage and investment banking
firm, morgan, olmstead, kennedy and gardner inc.
    fusco formerly was executive vice president of painewebber
inc.
    he replaces g. bryan herrmann, who will retain his title of
president of the parent group, and assume the title of vice
chairman of the parent, the company said.
 reuter
</text>]