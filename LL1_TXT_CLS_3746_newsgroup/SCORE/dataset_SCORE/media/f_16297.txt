[<text>
<title>o'sullivan corp &lt;osl&gt; 1st qtr net</title>
<dateline>    winchester, va., april 13 -
    </dateline>shr 28 cts vs 32 cts
    net 2,823,000 vs 3,216,000
    rev 47.9 mln vs 42.9 mln
    note: the 1986 earnings per share adjusted for a four for
three stock distribution paid may 1986.
 reuter
</text>]