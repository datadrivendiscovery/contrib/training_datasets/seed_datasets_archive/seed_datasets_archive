[<text>
<title>tandy corp &lt;tan&gt; 1st qtr sept 30 net</title>
<dateline>    fort worth, texas, oct 19 -
    </dateline>shr 71 cts vs 49 cts
    net 64.3 mln vs 43.7 mln
    revs 838.2 mln vs 742.6 mln
    avg shrs 89.9 mln vs 89.9 mln
 reuter
</text>]