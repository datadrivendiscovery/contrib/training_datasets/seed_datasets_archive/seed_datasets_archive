[<text>
<title>gac liquidating trust &lt;gactz&gt; sets cash payout</title>
<dateline>    coral gables, fla., april 1 - 
    </dateline>unit distribution one dlr vs 1.75 dlrs prior
    pay june one
    record may one
    note: prior distribution declared in april 1986.
 reuter
</text>]