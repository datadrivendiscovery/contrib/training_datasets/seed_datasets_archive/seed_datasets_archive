[<text>
<title>stanley works &lt;swk&gt; makes acquisitions</title>
<dateline>    new york, march 5 - </dateline>stanley works said it has acquired acme
holding corp, a maker of sliding and folding door hardware, and
the designs, patents and other righs of plan-a-flex designer
co, which provides kits for home design and remodeling
projects.
    it said acme had 1986 sales of over 50 mln dlrs.
    terms were not disclosed.
 reuter
</text>]