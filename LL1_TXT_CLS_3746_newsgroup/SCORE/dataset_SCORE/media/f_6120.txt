[<text>
<title>vtx elecronics &lt;vtx&gt; sets sdtock split</title>
<dateline>    farmingdale, n.y., march 17 - </dateline>vtx electronics corp said its
board declared a five-for-four stock split, payable april nine
to holders of record march 27.
 reuter
</text>]