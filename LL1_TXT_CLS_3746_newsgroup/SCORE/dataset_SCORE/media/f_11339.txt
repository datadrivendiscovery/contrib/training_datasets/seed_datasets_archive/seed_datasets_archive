[<text>
<title>old dominion systems &lt;odsi&gt; gets contract</title>
<dateline>    germantown, md., march 31 - </dateline>old dominion systems inc said
it has received a contract to provide multi-line voice
messaging systems to the u.s. immigration and naturalization
service.
    it said the contract is to provide 280,000 dlrs of
equipment immediately and contains options for the purchase of
another 800,000 dlrs worth through may 30.
 reuter
</text>]