[<text>
<title>kentucky central to declare stock dividend</title>
<dateline>    lexington, ky, april 3 - </dateline>kentucky central life insurance co
said the company said it will declare a 200 pct stock dividend
on monday.
    accordingly, it said it filed a registration statement with
the securities and exchange commission for a proposed offering
of three mln shares of its class a non-voting common stock,
which reflects the anticipated dividend.
    proceeds will be used for general purposes.
 reuter
</text>]