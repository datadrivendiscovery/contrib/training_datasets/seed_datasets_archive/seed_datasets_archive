[<text>
<title>lsi lighting systems inc &lt;lyts.o&gt; 1st qtr net</title>
<dateline>    cincinnati, oct 19 - </dateline>qtr ends september 30
    shr 25 cts vs 13 cts
    net 759,000 vs 383,000
    revs 9,052,000 vs 6,829,000
 reuter
</text>]