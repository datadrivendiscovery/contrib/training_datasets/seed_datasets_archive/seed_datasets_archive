[<text>
<title>university &lt;upt&gt; to supply intraocular lenses</title>
<dateline>    westport, conn., march 24 - </dateline>university patents inc said its
university optical products co subsidiary signed an agreement
to supply intraocular lenses to a "major international"
manufacturer and distributor based in the u.s.
    details of the agreement were not disclosed but university
said its optical unit will now produce about 3,000 intraocular
lenses each month for sale outside the u.s.
    intraocular lenses are used as a replacement for the eye's
natural lenses which must be removed during cataract surgery.
university said over a million cataract removals occur each
year in the u.s.
 reuter
</text>]