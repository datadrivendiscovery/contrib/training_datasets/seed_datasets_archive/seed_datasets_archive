[<text>
<title> fdic announces 44th u.s. bank failure of year</title>
<dateline>    washington, march 18 - </dateline>the federal deposit insurance corp.
(fdic) announced the failure of an oklahoma city bank, the 44th
u.s. bank failure this year.
    the fdic said it had approved the assumption of the united
oklahoma bank by united bank of oklahoma, a newly chartered
subsidiary of united bank shares, inc., of oklahoma city.
    it said the failed bank had 148.9 million dlrs in assets,
including about 94.1 million dlrs in 13,000 accounts.
 reuter
</text>]