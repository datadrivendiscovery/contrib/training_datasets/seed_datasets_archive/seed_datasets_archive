[<text>
<title>beneficial &lt;bnl&gt; unit sale approved</title>
<dateline>    new york, april 3 - </dateline>beneficial corp said the sale of its
american centennial insurance co subsidiary to &lt;first delaware
holdings inc&gt; was approved by the delaware insurance
department.
    under the transaction, american centennial will receive a
cash infusion of 200 mln dlrs, including the settlement of tax
sharing agreements with beneficial corp, beneficial said.
    it will also receive 25 mln dlrs from beneficial
international insurance co, another beneficial subsidiary being
purchased by the management-led group of first delaware, the
company said.
 reuter
</text>]