[<text>
<title>borden &lt;bn&gt; says plans to issue debt</title>
<dateline>    new york, oct 20 - </dateline>borden inc said it plans to issue up to
250 mln dlrs of long-term debt securities in order to
capitalize on sharply lower interest rates.
    proceeds will be used primarily to refinance existing
commercial paper.
    the company said the offering of long-term debt will be in
addition to its previously announced offering of a master
limited partnership interest relating to borden's basic
chemicals and plastics operations.
 reuter
</text>]