[<text>
<title>american &lt;amr&gt; sets schedule for aircal merger</title>
<dateline>    dallas, march 31 - </dateline>amr corp's american airlines unit said
it plans to complete the integration of aircal into its
operations within four to five months.
    american's merger with aircal, announced last november,
received final approval from the department of transportation
yesterday.
    american said richard d. pearson, coordinator of the
airline's merger activities, will become chairman and chief
executive officer of aircal during the period up to completion
of the merger.
    american said pearson succeeds william lyon, who has been
elected to amr's board.
    it added that david a. banmiller will continue as aircal's
president and chief operating officer.
 reuter
</text>]