[<text>
<title>bethlehem steel &lt;bs&gt; debt downgraded by s/p</title>
<dateline>    new york, march 19 - </dateline>standard and poor's corp said it
downgraded bethlehem steel corp's debt securities.
    bethlehem has one billion dlrs of debt and preferred stock
outstanding. cut were its senior debt to ccc-plus from b-minus
and subordinated debt to ccc-minus from ccc. s and p affirmed
the c-rated preferred, since preferred dividends were
discontinued last year. 
    s and p cited concerns over bethlehem's viability over the
intermediate term, rather than any immediate threat to
solvency. its cash is strong relative to 1987 requirements, but
was accumulated through such means as asset sales.
 reuter
</text>]