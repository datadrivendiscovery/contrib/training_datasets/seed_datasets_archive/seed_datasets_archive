[<text>
<title>american healthcare &lt;ahi&gt; downgraded by moody's</title>
<dateline>    new york, june 29 - </dateline>moody's investors service said it
downgraded american healthcare management's outstanding 80 mln
dlrs of 15 pct subordinated notes to caa from b-1.
    moody's said that noteholders now have the option to
accelerate payment of the issue as early as july 15, 1987. the
rating had been under review since april.
    the agency noted the company earned three mln dlrs in the
first quarter after taxes. however, moody's said the firm 
violated covenants in its in its bank credit agreement as a
result of substantial asset writedowns and an operating
earnings decline, both of which were announced in april.
 reuter
</text>]