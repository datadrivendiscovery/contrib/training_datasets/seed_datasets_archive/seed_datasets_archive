[<text>
<title>u.s. says state regulators close oklahoma bank</title>
<dateline>    washington, march 20 - </dateline>the federal deposit insurance corp 
(fdic) said it transfered the federally insured deposits of
madill bank and trust co, madill, okla, to the healthy first
american national bank, tishomingo, okla., after madill was
closed by oklahoma authorities.
    the failed bank had 37.0 mln in assets and two offices.
    madill was the 49th federally insured bank to fail so far
this year, the fdic said.
 reuter
</text>]