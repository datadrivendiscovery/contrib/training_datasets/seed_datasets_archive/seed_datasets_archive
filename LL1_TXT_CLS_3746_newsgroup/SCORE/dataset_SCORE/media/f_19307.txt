[<text>
<title>altus bank &lt;alts.o&gt; fails to reach agreement</title>
<dateline>    mobile, ala., june 19 - </dateline>altus bank said it has not been
able to reach a definitive agreement to buy horizon financial
corp, horizon funding and 2.8 billion dlrs in loan servicing
from victor savings and loan association.
    altus, formerly known as first southern federal savings and
loan association, earlier announced the signing of a letter of
intent to make the acquisitions.
    horizon financial and horizon funding both are units of
victor savings and loan association.

 reuter
</text>]