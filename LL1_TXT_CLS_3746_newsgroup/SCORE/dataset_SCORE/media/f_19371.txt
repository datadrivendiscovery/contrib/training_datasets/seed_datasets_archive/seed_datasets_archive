[<text>
<title>world bank loans uganda 13 mln dlrs</title>
<dateline>    washington, june 19 - </dateline>the world bank said it has loaned
uganda 13 mln dlrs through the international development
association (ida), the bank's concessionary lending affiliate.
    the ida loan will support a project that hopes to preserve
the country's natural forests and meet its demand for wood
products by rehabilitating uganda's forestry management agency,
the bank said.
    it also said the project plans to increase the area and
management of protected forests, establish pilot wood farms and
nurseries, and rehabilitate soft wood plantations.
 reuter
</text>]