[<text>
<title>united jersey banks inc &lt;ujb&gt; sets quarterly</title>
<dateline>    princeton, n.j., june 18 -
    </dateline>qtly div 21-1/2 cts vs 21-1/2 cts prior
    pay aug three
    reord july seven
 reuter
</text>]