[<text>
<title>ameron inc &lt;amn&gt; sets qtly div</title>
<dateline>    monterey park, calif., june 29 -
    </dateline>qtly div 24 cts vs 24 cts
    pay august 14
    record july 24
 reuter
</text>]