[<text>
<title>gencorp &lt;gy&gt; unit completes wor-tv sale</title>
<dateline>    akron, ohio, april 3 - </dateline>gencorp said its rko general inc
subsidiary completed the sale of wor-tv to mca inc &lt;mca&gt; for
387 mln dlrs.
    the federal communications commission approved the sale
last december, gencorp said. the closing was delayed because
that decision was appealed by four parties to the u.s. court of
appeals, gencorp explained.
    wor-tv is based in  secaucus, n.j., gencorp said.
    earlier today, &lt;general partners&gt; said it was prepared to
raise its bid to 110 dlrs per share, or even more, in its bid
for gencorp.
 reuter
</text>]