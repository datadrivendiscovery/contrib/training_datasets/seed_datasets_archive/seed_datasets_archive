[<text>
<title>ponce federal bank fsb &lt;pfbs&gt; raises dividend</title>
<dateline>    ponce, p.r., march 26 -
    </dateline>qtly div nine cts vs 7.5 cts in prior qtr
    payable april 15
    record march 31
 reuter
</text>]