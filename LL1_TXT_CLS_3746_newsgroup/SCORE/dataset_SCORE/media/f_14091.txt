[<text>
<title>photronics corp &lt;phot&gt; year feb 28 net</title>
<dateline>    hauppauge, n.y., april 7 -
    </dateline>shr 64 cts vs 38 cts
    net 1,062,000 vs 585,000
    sales 13.6 mln vs 9,262,000
 reuter
</text>]