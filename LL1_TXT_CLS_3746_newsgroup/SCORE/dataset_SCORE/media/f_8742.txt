[<text>
<title>griffin technology inc &lt;grif&gt; 4th qtr net</title>
<dateline>    victor, n.y., march 24 - </dateline>ended jan 31
    shr loss one ct vs loss eight cts
    net loss 25,800 vs loss 157,100
    revs 2,323,500 vs 1,930,400
    year
    shr profit 19 cts vs profit four cts
    net profit 401,100 vs profit 93,100
    revs 10.3 mln vs 8,807,000
 reuter
</text>]