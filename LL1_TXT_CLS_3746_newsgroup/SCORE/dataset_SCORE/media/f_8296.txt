[<text>
<title>raven industries inc &lt;rav&gt; 4th qtr jan 31 net</title>
<dateline>    sioux falls, s.d., march 23 -
    </dateline>shr profit six cts vs loss nine cts
    net profit 101,000 vs loss 142,000
    sales 12.6 mln vs 8,736,000
    year
    shr profit 1.34 dlrs vs profit 1.02 dlrs
    net profit 2,122,000 vs profit 1,611,000
    sales 52.3 mln vs 41.9 mln
 reuter
</text>]