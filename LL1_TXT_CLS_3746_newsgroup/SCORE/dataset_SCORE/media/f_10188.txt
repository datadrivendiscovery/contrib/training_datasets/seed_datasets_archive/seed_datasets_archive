[<text>
<title>hawaiian airlines &lt;ha&gt; to expand services</title>
<dateline>    honolulu, march 26 - </dateline>hawaiian airlines inc said it will
continue to expand services to pacific islands this year.
    the company said it will increase its south pacific
services to daily service, from the current four flights per
week, and beginning may 21 it will fly twice a week to the
western pacific to guam.
    hawaiian airlines also said it has applied with the
department of transportation for an emergency exemption to
provide service to tahiti, with connecting services to american
samoa, new zealand and cooke islands.
 reuter
</text>]