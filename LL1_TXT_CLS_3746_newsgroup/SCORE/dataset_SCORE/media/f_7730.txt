[<text>
<title>virateck inc &lt;vira&gt; 1st qtr feb 28 oper loss</title>
<dateline>    costa mesa, calif., march 20 -
    </dateline>oper shr loss 23 cts vs profit 16 cts
    oper loss 1,868,000 vs profit 1,293,000
    revs 183,000 vs 3,400,000
    note: oper data does not include year ago extraordinary
gain of 750,000 dlrs, or nine cts per shr.
 reuter
</text>]