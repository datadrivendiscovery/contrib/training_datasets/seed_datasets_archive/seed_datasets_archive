[<text>
<title>echlin inc &lt;ech&gt; dividend increased 12 pct</title>
<dateline>    branford, conn., march 12 -
    </dateline>qtly div 14 cts vs 12.5 cts in prior qtr
    payable april 18
    record april two
 reuter
</text>]