[<text>
<title>mason best forms energy holding company</title>
<dateline>    dallas, april 8 - </dateline>&lt;mason best co&gt; said it has formed
&lt;meridian energy corp&gt;, a privately-owned holding company
structured to acquire operating companies ans assets in the oil
and gas industry.
    mason best, a texas-based merchant banking firm, said it is
sponsor and largest shareholder of meridian. the new company's
chairman and chief executive officer is  ralph e. bailey, who
retired as vice-chairman of du pont co &lt;dd&gt; and chairman of its
conoco inc subsidiary on march 31.
    meridian is headquartered in dallas with an administrative
office in stamford, conn.
 reuter
</text>]