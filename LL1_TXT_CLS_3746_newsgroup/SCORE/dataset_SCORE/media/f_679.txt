[<text>
<title>&lt;pantry inc&gt; inb talks on being acquired</title>
<dateline>    sanford, n.c., march 2 - </dateline>privately-held pantry inc, which
operates 477 convenience stores in five southeastern states,
said it has engaged alex. brown and sons inc &lt;absb&gt; to explore
a possbile sale of the company.
    it said it expects to start talks with prospective
acquirers shortly. the company said it has been approached by a
number of parties in recent months.
 reuter
</text>]