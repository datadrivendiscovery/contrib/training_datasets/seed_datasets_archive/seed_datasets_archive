[<text>
<title>american motors &lt;amo&gt; weighs takeover proposal</title>
<dateline>    southfield, mich., march 11 - </dateline>american motors corp said its
directors met wednesday to review the takeover proposal which
the corporation received monday from chrysler corp &lt;c&gt;.
    amc said its board has retained independent investment and
legal advisers and expects to meet periodically over the next
several weeks on the proposal.
 reuter
</text>]