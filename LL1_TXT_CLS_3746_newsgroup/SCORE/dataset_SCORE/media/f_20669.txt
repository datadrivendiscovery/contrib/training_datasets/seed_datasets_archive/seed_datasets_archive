[<text>
<title>petrolane partners l.p. &lt;lpg&gt; 3rd qtr loss</title>
<dateline>    long beach, calif, oct 20 -
    </dateline>shr loss five cts vs profit six cts
    net loss 1,200,000 vs profit 1,400,000
    nine mths
    shr profit 1.00 dlrs vs profit 84 cts
    net profit 23.7 mln vs profit 19.9 mln
    note: year ago results are pro forma since the company was
created in march by the transfer to a master limited
partnership of all domestic assets of petrolane inc's liquefied
petroleum gas division.
 reuter
</text>]