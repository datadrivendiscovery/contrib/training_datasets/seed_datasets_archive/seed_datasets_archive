[<text>
<title>xerox &lt;xrx&gt; sells connecticut property</title>
<dateline>    stamford, conn., march 18 - </dateline>xerox corp said it sold a
102-acre site in greenwich, conn., to a limited partnership for
10.5 mln dlrs.
    other details were not disclosed.
    the purchaser was identified as greenwich king street
associates ii limited partnership.
 reuter
</text>]