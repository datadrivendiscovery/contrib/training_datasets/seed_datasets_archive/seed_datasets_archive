[<text>
<title>national patent development corp &lt;npd&gt; in payout</title>
<dateline>    new york, march 20 -
    </dateline>qtly div 2-1/2 cts vs 2-1/2 cts prior
    pay may one
    record april one
 reuter
</text>]