[<text>
<title>ceasars world &lt;caw&gt; studying sosnoff offer</title>
<dateline>    los angeles, march 9 - </dateline>ceasars wold inc said its board is
studying the unsolicited and conditional tender offer for all
its common shares at 28 dlrs per share from martin t. sosnoff.
    a company spokesman said the board expects to make a
recommendation "shortly", but could not specify a time period.
    ceasars world chairman henry gluck in a statement urged
shareholders not to take any action with respect to the offer
prior to the board's recommendation.
    sosnoff made the offer directly to shareholders in a
newspaper advertisement through a company he formed, called mts
acquisition corp. it expires april 3.
 reuter
</text>]