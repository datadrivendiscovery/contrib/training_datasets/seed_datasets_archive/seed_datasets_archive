[<text>
<title>uslico corp &lt;usvc&gt; increases dividend</title>
<dateline>    arlington, va, march 6 -
    </dateline>qtly div 22 cts vs 20 cts prior
    payable march 27
    record march 18
 reuter
</text>]