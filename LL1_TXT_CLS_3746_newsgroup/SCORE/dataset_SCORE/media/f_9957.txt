[<text>
<title>mulford says germany, japan should do more</title>
<dateline>    washington, march 26 - </dateline>treasury assistant secretary david
mulford said he did not believe that west germany and japan
have yet carried out their international responsibilities.
    "i do not believe they have up to this time," mulford told
a senate banking subcommittee.
    he said that for the u.s. trade deficit to continue
improving in the next two years, "we need more policy actions"
across the entire front of u.s. trade relations, including
canada and the newly-industrialized countries (nics).
    in particular, he said, efforts by south korea and taiwan
to strengthen their currencies were still disappointing.
    mulford also said that oecd nations need to grow an average
three pct to help resolve the international debt crisis.
    he noted that japanese and european imports from latin
nations were significantly smaller than imports into the u.s.
    he stressed both germany and japan must continue to take
economic and structural measures to ensure stronger sustained
economic growth.
 reuter
</text>]