[<text type="unproc">
e and b marine &lt;ebmi&gt; 4th qtr net
    edison, n.j., april 3
    shr loss 1.65 dlrs vs loss 24 cts
    net loss 3,259,000 vs loss 470,000
    revs 16.0 mln vs 9,510,000
    12 mths
    shr loss 84 cts vs gain 63 cts
    net loss 1,661,000 vs gain 1,301,000
    revs 80.5 mln vs 56.4 mln
 reuter


</text>]