[<text>
<title>newhall resources &lt;nr&gt; qtly distribution</title>
<dateline>    valencia, calif., april 9 -
    </dateline>shr 15 cts vs 15 cts prior qtr
    pay june one
    record april 24

 reuter
</text>]