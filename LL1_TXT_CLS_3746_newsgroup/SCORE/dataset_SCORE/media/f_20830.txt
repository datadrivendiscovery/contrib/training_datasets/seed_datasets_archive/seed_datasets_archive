[<text>
<title>s/p reviews ua communications &lt;uaci&gt; for cut</title>
<dateline>    new york, oct 20 - </dateline>standard and poor's corp said it is
reviewing for downgrade united artists communications inc's
single-b rating on 490 mln dlrs of outstanding and
shelf-registered subordinated debt.
    placed under review with positive implications were united
cable television corp's b-minus rating on 250 mln dlrs of
outstanding subordinated debt and preliminary b-plus senior and
b-minus subordinated ratings on 200 mln dlrs of
shelf-registered debt.
    s and p said the reviews reflect its perception of the
implications of a proposed merger involving the two firms.
 reuter
</text>]