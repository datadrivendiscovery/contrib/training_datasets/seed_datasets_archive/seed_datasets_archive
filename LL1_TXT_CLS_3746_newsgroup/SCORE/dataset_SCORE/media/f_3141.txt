[<text>
<title>golden tech resources &lt;gts.v&gt; unit notes sales</title>
<dateline>    vancouver, british columbia, march 9 - </dateline>international
geosystems corp, a subsidiary of golden tech resources, said
recent orders of its tianma chinese language word processing
system have brought year-to-date sales to more than 2,900,000
canadian dlrs.
 reuter
</text>]