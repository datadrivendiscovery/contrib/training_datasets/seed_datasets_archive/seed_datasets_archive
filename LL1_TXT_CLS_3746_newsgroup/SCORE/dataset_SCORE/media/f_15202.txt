[<text>
<title>digilog &lt;dilo&gt; says 2nd qtr revenues rose</title>
<dateline>    montgomeryville, pa., april 8 - </dateline>digilog inc said revenues
for the second quarter ended march 31 were about 4,300,000
dlrs, up from 3,065,000 dlrs a year earlier.
 reuter
</text>]