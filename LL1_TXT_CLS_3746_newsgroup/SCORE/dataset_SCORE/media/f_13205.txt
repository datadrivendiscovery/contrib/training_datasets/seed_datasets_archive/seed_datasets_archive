[<text>
<title>publishers equipment &lt;pecn&gt;, trentonian in deal</title>
<dateline>    dallas, april 3 - </dateline>publishers equipment corp said it reached
an agreement to incorporate flexo printing technology into the
tentonian, an ingersoll newspaper in trenton, n.j.
    it said that mid-1988 will be the target date for start-up
for the new equipment which will double the size of its present
press.
 reuter
</text>]