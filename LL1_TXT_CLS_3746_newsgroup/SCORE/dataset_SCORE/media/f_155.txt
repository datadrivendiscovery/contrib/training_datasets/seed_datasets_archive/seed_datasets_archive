[<text>
<title>varian &lt;var&gt;, siemens form joint venture</title>
<dateline>    palo alto, calif., feb 26 - </dateline>varian associates inc and
&lt;siemens a.g.&gt; said they signed a letter of intent to form and
jointly operate a nuclear magnetic resonance imaging
spectroscopy business in fremont, calif.
    the systems are smaller than magnetic resonance imaging
equipment currently used in clinical examinations, the
companies said.
    they also said the products resulting from the venture are
intended for use in small animal research, certain limited
medical research and materials testing.
 reuter
</text>]