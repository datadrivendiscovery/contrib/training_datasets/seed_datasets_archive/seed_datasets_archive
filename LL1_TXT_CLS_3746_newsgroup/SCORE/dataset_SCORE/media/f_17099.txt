[<text>
<title>texas commerce &lt;tcb&gt; holders approve merger</title>
<dateline>    houston, april 24 - </dateline>texas commerce bancshares inc said its
shareholders approved the merger of the bank with chemical new
york corp &lt;chl&gt;, moving a step closer towards creating the
nation's fourth largest bank.
    the company said each holder will receive 31.19 dlrs a
share in cash and securities, somewhat less than the 35 dlrs to
36 dlrs a share estimated when the deal was announced on
december 15, 1986. the deal is now worth 1.16 billion dlrs.
    the merger is still subject to approval by chemical's
shareholders, who will vote on the deal at the company's annual
meeting on april 28.
    the company said 98.7 pct of the shareholders voting on the
merger cast an affirmative vote.
    chairman and chief executive officer ben love said all
regulatory hurdles to the merger have been cleared, including
last week's final approval of the transaction by the u.s.
comptroller of the currency. pending approval of chemical's
shareholders, the merger should be closed by may one, he added.
    the bank said the combined company will have assets of
about 80 billion dlrs.
 reuter
</text>]