[<text>
<title>newport electronics inc &lt;newe&gt; 4th qtr net</title>
<dateline>    santa ana, calif., march 11 -
    </dateline>oper shr loss nine cts vs loss four cts
    oper net loss 108,000 vs loss 55,000
    sales 3,029,000 vs 2,694,000
    year
    oper shr profit 42 cts vs profit 15 cts
    oper net profit 511,000 vs profit 177,000
    sales 13.3 mln vs 11.2 mln
    avg shrs 1,233,136 vs 1,217,981
    note: current qtr and year figures exclude operating loss
carryforward gain of 26,000 dlrs vs gain of 88,000 dlrs in
prior year periods.
 reuter
</text>]