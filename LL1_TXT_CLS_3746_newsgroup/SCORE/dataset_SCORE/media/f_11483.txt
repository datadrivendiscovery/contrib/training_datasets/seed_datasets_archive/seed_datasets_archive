[<text>
<title>gull &lt;gll&gt; in private sale of notes</title>
<dateline>    smithtown, n.y., march 31 - </dateline>gull inc said it sold 20 mln
dlrs of senior notes due march 31, 1999, to two institutional
investors through oppenheimer and co.
    proceeds will be used to reduce long term debt and to
convert other debt to fixed from floating rates, the maker of
high technology instruments and other products said.
 reuter
</text>]