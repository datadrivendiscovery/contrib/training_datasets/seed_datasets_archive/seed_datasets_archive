[<text>
<title>shamrock completes central soya sale to ferruzzi</title>
<dateline>    burbank, calif., oct 19 - </dateline>shamrock capital lp said it has
completed the sale of cental soya co inc to ferruzzi agricola
finanziaria the holding company for the ferruzzi group of
ravenna, italy.
    shamrock capital is a limited partnership led by shamrock
holdings inc, the roy e. disney family company.
    under the agreement announced last month, ferruzzi acquired
all the equity and assumed subordinated term debt of about 195
mln dlrs in a transaction valued at about 370 mln dlrs.
 reuter
</text>]