[<text>
<title>constellation bancorp &lt;cstl.o&gt; 3rd qtr net</title>
<dateline>    elizabeth, n.j., oct 20 -
    </dateline>shr 78 cts vs 61 cts
    net 4,774,000 vs 3,683,000
    nine mths
    shr 2.14 dlrs vs 1.77 dlrs
    net 13.1 mln vs 10.7 mln
 reuter
</text>]