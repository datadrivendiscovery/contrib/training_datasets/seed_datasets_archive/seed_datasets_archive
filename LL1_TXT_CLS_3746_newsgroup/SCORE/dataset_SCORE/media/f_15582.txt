[<text>
<title>yugoslavia to tender for 100,000 tonnes wheat</title>
<dateline>    washington, april 9 - </dateline>yugoslavia will tender april 14 for
100,000 tonnes of wheat, the u.s. agriculture department's
counselor in belgrade said in a field report.
    the report, dated april 7, said the wheat must be from 1986
and 1987 harvest, and imports of soft wheat from europe and
from other suppliers will not be considered.
    it said the imports will be used to rebuild the federal
reserves and as a result will not be subject to import
surcharges.
 reuter
</text>]