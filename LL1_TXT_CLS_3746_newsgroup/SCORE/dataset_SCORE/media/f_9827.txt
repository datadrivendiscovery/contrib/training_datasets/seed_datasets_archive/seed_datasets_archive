[<text>
<title>boston edison co &lt;bse&gt; regular dividend</title>
<dateline>    boston, march 26 -
    </dateline>qtly div 44.5 cts vs 44.5 cts in prior qtr
    payable may one
    record april 10
 reuter
</text>]