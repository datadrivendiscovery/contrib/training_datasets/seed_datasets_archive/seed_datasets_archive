[<text>
<title>&lt;strathfield oil and gas ltd&gt; year net</title>
<dateline>    calgary, alberta, april 9 -
    </dateline>shr 46 cts vs 48 cts
    net 1,196,331 vs 1,341,314
    revs 5,153,109 vs 7,680,350
 reuter
</text>]