[<text>
<title>american motors&lt;amo&gt; extends incentive program</title>
<dateline>    southfield, mich., april 1 - </dateline>american motors corp said it
is extending and enhancing its buyer-incentive programs on
renault alliance, encore and gta models and jeep comanche,
cherokee and wagoneer to april 10. the incentives were to have
expired march 31.
    new to the april program is the combination of a
low-interest annual percentage financing program and 1986 and
1987 renault vehicles and a 500 dlr rebate.
    it said buyers of 1986 and 1987 jeep cherokee, wagoneer and
comanche vehicles have a choice of the financing program or a
rebate.

 reuter
</text>]