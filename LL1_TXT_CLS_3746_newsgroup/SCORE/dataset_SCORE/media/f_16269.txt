[<text>
<title>ncr corp &lt;ncr&gt; 1st qtr net</title>
<dateline>    dayton, ohio, april 13 -
    </dateline>shr 65 cts vs 51 cts
    net 61.5 mln vs 50.2 mln
    revs 1.12 billion vs 960.8 mln
    avg shrs 95.3 mln vs 99.4 mln
 reuter
</text>]