[<text>
<title>pansophic systems &lt;pns&gt; splits stock 2-for-1</title>
<dateline>    oak brook, ill., march 3 - </dateline>pansophic systems inc said it
will split its stock two-for-one effective april two to
shareholders of record march 13.
    it also said it will pay a six cts per share dividend on
the pre-split shares, a regular quarterly dividend, on april
two to shareholders of record march 13.
 reuter
</text>]