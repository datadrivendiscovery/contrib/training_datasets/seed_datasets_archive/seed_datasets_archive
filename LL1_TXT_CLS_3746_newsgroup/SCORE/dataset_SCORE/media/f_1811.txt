[<text>
<title>yeutter seeks stronger taiwan, s.korea currencies</title>
<dateline>    washington, march 4 - </dateline>u.s. trade representative clayton
yeutter said he hoped the u.s. dollar would continue to decline
in relation to the currencies of taiwan and south korea as a
way to improve the u.s. trade picture.
    testifying before the house appropriations subcommittee
which must approve his agency's 1988 budget, he said, "in my
judgment economic factors justify a continued decline."
    asked by a committee member if he expected a further
decline, and how much, yeutter said the taiwan and south korean
currencies should be adjusted to reflect "positive factors" in
their economies.
 reuter
</text>]