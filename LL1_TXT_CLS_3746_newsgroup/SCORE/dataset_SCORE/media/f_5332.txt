[<text>
<title>clark equipment &lt;ckl&gt; stake acquired</title>
<dateline>    south bend, ind., march 16 - </dateline>clark equipment co said it was
informed by arthur m. goldberg acting on behalf of a group of
investors that the group had accumulated 1,262,200 shares, or
about 6.7 pct of clark's outstanding common stock.
    it said goldberg recently approached clark to repurchase
the shares. however, negotiations for the block repurchase were
unsuccessful and have been terminated.
 reuter
</text>]