[<text>
<title>fidelity federal savings and loan &lt;ffed.o&gt; 3rd</title>
<dateline>    philadelphia, oct 19 -
    </dateline>shr 42 cts vs 59 cts
    net 734,659 vs 1,033,309
    nine mths
    shr 92 cts vs 1.69 dlrs
    net 1,629,719 vs 2,971,144
    note: 1987 nine mths net includes a loss of 290,000 dlrs on
sale of securities and nine-recurring charge of 32,713 dlrs for
write-off of fslic secondary reserve.
 reuter
</text>]