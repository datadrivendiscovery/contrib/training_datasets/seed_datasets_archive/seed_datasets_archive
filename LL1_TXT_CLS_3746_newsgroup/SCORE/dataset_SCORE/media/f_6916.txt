[<text>
<title>rockwell &lt;rok&gt; unit, japan firm form venture</title>
<dateline>    milwaukee, march 18 - </dateline>allen-bradley co, a unit of rockwell
international corp, said it has formed a joint venture with tdk
corp of tokyo to produce, market and sell ferrite magnets.
    allen-bradley will be responsible for production and tdk
will handle sales and marketing. products will be marketed
through tdk corp of america, a chicago-based tdk unit that
markets tdk's electronic components.
    the venture, to be known as allen-bradley/tdk magnetics,
will be based in shawnee, okla, in the existing allen-bradley
magnetics production facility.
 reuter
</text>]