[<text>
<title>security pacific &lt;spc&gt; unit's shelf effective</title>
<dateline>    new york, june 18 - </dateline>security pacific national bank, a unit
of security pacific corp, said the securities and exchange
commission declared effective its shelf registration statement
covering 1.2 billion dlrs of mortgage related securities.
    the bank said it filed the registration statement in april.
this is the bank's second shelf registration filed in
connection with security pacific merchant bank's efforts to
securitize mortgages originated by security pacific national,
it said.
 reuter
</text>]