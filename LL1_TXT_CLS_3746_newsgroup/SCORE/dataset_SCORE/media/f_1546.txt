[<text>
<title>instron corp &lt;isn&gt; sets quarterly</title>
<dateline>    canton, mass., march 4 -
    </dateline>qtly div three cts vs three cts prior
    pay april 2
    record march 16
 reuter
</text>]