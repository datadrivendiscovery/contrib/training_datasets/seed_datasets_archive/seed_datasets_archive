[<text>
<title>cullen/frost &lt;cfbi&gt; to omit dividend</title>
<dateline>    san antonio, texas, april 3 - </dateline>cullen/frost bankers inc said
it will defer paying a cash dividend for the next 12 months,
due to the economic slump in the texas economy. it previously
paid a five cents a share dividend in recent quarters.
    it also said its first quarter earnings ended march 31,
which it said it will release later this month, will be similar
to its fourth quarter earnings last year.
    in 1986 the company reported a loss of 6,565,000 dlrs or 91
cts a share.
 reuter
</text>]