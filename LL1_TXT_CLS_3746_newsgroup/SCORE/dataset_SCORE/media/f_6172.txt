[<text>
<title>hospital staffing services inc &lt;hssi&gt; 1st qtr</title>
<dateline>    fort lauderdale, fla., march 17 - </dateline>feb 28
    oper shr six cts vs two cts
    oper net 189,683 vs 47,499
    revs 2,874,930 vs 2,594,574
    note: prior year net excludes 33,000 dlr tax credit.
 reuter
</text>]