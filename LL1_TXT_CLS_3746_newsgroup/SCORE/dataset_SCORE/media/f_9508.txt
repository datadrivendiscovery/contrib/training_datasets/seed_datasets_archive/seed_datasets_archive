[<text>
<title>sas to upgrade cabin service in scandinavia</title>
<dateline>    new york, march 25 - </dateline>scandinavian airlines systems, sas,
said it will improve cabin service for business class
passengers on intra-scandinavian routes starting next week.
    the company also said it will simplify timetables on routes
in scandinavia, with many flights departing hourly.
    sas said it will upgrade meals and between meal
refreshments and relegate the sale of tax-free candy and
cosmetics to airport shops to give cabin attendants more time
to devote to the enhanced service.
 reuter
</text>]