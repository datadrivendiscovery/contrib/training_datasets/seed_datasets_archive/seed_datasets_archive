[<text>
<title>mexican cattle imports to be branded - usda</title>
<dateline>    washington, april 8 - </dateline>all steers imported into the united
states from mexico must now be branded with the letter m on the
right jaw, the u.s. agriculture department said.
    in its world production and trade report, the department
said the branding is necessary to improve surveillance for
bovine tuberculosis because it provides a permanent way to
identify mexican steers.
    the requirement is not expected to affect the number of
mexican steers imported into the united states and the brand
will be applied before the animals arrive at u.s. ports of
entry.
    last november, the mexican government authorized an export
quota of nearly 1.1 mln head of live cattle for the 1986/87
season (aug-july), most of which goes to the united states.
 reuter
</text>]