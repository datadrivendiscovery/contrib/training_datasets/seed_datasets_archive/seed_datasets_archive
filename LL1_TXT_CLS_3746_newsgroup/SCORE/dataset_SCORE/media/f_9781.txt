[<text>
<title>u.k. money market forecast revised to deficit</title>
<dateline>    london, march 26 - </dateline>the bank of england said it revised its
estimate of today's money market shortfall to around 350 mln
stg from a flat position.
 reuter
</text>]