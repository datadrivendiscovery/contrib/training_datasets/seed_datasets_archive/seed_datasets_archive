[<text>
<title>baker, fentress and co &lt;bkfr&gt; votes dividend</title>
<dateline>    chicago, march 19 -
    </dateline>qtly div 25 cts vs 25 cts prior qtr
    payable 10 june
    record 15 may
 reuter
</text>]