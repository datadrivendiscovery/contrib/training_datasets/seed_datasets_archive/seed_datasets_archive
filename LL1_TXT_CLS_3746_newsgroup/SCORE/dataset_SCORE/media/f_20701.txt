[<text>
<title>centerre bancorp &lt;ctbc.o&gt; sets qtly payout</title>
<dateline>    st. louis, oct. 20 -
    </dateline>qtly div 45 cts vs 45 cts prior qtr
    pay nov 30
    record nov 3
 reuter
</text>]