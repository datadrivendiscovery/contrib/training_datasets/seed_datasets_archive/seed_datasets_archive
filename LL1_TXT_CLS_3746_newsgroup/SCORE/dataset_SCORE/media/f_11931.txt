[<text>
<title>amex begins trading harcourt brace &lt;hbj&gt; options</title>
<dateline>    new york, april 1 - </dateline>the american stock exchange said it
today opened put and call options trading in the common stock
of harcourt brace jovanovice inc.
    the exchange said harcourt brace is a replacement selection
for ltv corp.
    the amex said the initial expiration months for hbj options
are april, may, july and october.
 reuter
</text>]