[<text>
<title>world bank loans hungary 70 mln dlrs</title>
<dateline>    washington, june 19 - </dateline>the world bank said it approved a 70
mln dlr loan to hungary to help finance a 833.1 mln dlr
telecommunications project.
    the bank noted that hungary, which until recently had given
telecommunications low priority, recognized that inadequate
telecommunications are a serious constraint to economic
development and reform.
    the project extends through 1991 and will help magyar
posta, the state enterprise overseeing the telecommunications
sector, meet its long-term development goals.
 reuter
</text>]