[<text>
<title>riverside group inc &lt;rsgi&gt; 4th qtr</title>
<dateline>    jacksonville, fla, march 19 -
    </dateline>shr 55 cts vs 93 cts
    net 1.6 mln vs 2.6 mln
    revs 5.3 mln vs 1.2 mln
    year
    shr 73 cts vs 1.36 dlrs
    net 2.0 mln vs 3.0 mln
    revs 9.1 mln vs 11.0 mln
    note:1985 net includes 2.7 mln dlrs gain on disposal of
unit. 1986 includes operations of dependable insurance gruop
inc.
 reuter
</text>]