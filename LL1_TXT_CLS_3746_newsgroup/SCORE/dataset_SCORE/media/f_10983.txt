[<text>
<title>ameron inc &lt;amn&gt; 1st qtr feb 28 loss</title>
<dateline>    monterey park, calif., march 30 -
    </dateline>shr loss 11 cts vs profit six cts
    net loss 515,000 vs profit 294,000
    sales 62.8 mln vs 65.4 mln
 reuter
</text>]