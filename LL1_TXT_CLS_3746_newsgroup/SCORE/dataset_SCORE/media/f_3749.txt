[<text>
<title>schering-plough &lt;sgp&gt; obtains injunction</title>
<dateline>    kenilworth, n.j., march 11 - </dateline>schering-plough corp said it
obtained an injunction against nature's blend products inc of
due to trademark infringement on its fibre trim label.
    under terms of the injunction, nature's blend agreed to
refrain from making and selling food supplement products under
the trademark fiber slim or using any any trademark that
simulates the trade dress of fibre trim.
 reuter
</text>]