[<text type="unproc">
federal reserve weekly report 1 - feb 26
    two weeks ended feb 25      daily avgs-mlns
 net free reserves.............644 vs.....1,337
 bank borrowings...............680 vs.......425
 including seasonal loans.......81 vs........56
 including extended loans......299 vs.......265
 excess reserves.............1,025 vs.....1,497
 required reserves (adj)....55,250 vs....55,366
 required reserves..........55,513 vs....56,208
 total reserves.............56,538 vs....57,705
 non-borrowed reserves......55,859 vs....57,281
 monetary base.............244,199 vs...244,925
                         
 reuter


</text>]