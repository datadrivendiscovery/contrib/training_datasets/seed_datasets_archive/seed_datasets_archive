[<text>
<title>worthen banking &lt;wor&gt; unit head quits</title>
<dateline>    hot springs, ark., march 16 - </dateline>worthen banking corp said g.
michael sigman has resigned as president and chief executive
officer of its first national bank of hot springs subsidiary to
pursue other business interests.
    it said james c. patridge, executive vice president of its
worthen bank and trust co subsidiary, will act as sigman's
replacement on an interim basis until a successor is found.
 reuter
</text>]