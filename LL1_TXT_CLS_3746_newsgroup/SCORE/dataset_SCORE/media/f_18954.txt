[<text>
<title>china adds corn to commitments - usda</title>
<dateline>    washington, june 18 - </dateline>china has added 30,000 tonnes of u.s.
corn to its previous commitments, according to the u.s.
agriculture department's latest export sales report.
    the report, covering transactions in the week june 11, the
additional corn resulted from changes in destinations.
    total corn commitments for delivery in the 1986/87 season
amount to 1,083,400 tonnes.
 reuter
</text>]