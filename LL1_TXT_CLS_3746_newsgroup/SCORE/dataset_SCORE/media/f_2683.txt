[<text>
<title>fed sets one billion dlr customer repo</title>
<dateline>    new york, march 6 - </dateline>the federal reserve entered the
government securities market to arrange one billion dlrs of
customer repurchase agreements, a spokesman for the new york
fed said.
    fed funds were trading at 5-15/16 pct at the time of the
indirect injection of temporary reserves, dealers said.
 reuter
</text>]