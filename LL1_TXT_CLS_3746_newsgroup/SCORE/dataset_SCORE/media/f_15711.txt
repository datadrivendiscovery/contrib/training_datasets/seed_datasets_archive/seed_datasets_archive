[<text>
<title>centel &lt;cnt&gt; sells debentures at 9.233 pct</title>
<dateline>    new york, april 9 - </dateline>centel corp is raising 50 mln dlrs
through an offering of debentures due 2017 yielding 9.233 pct,
said lead manager smith barney, harris upham and co inc.
    the debentures have a 9-1/8 pct coupon and were priced at
98.90 to yield 117 basis points over comparable treasury
securities.
    non-callable for five years, the issue is rated a-3 by
moody's investors service inc and a by standard and poor's
corp.
    e.f. hutton and co inc and ubs securities inc co-managed
the deal.
 reuter
</text>]