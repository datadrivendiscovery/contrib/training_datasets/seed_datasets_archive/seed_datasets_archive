[<text>
<title>mobil &lt;mob&gt; to upgrade refinery unit</title>
<dateline>    new york, march 26 - </dateline>mobil corp said it will spend over 30
mln dlrs to upgrade a gasoline-producing unit at its beaumont,
texas, refinery.
    it said the unit is a catalytic reformer, which converts
low-octane components of gasoline into high-octane components
for use in super unleaded gasoline.
    the company said the modernization will allow the unit to
regenerate catalysts on a continuous basis without shutdown.
currently, it must be shut twice a year.  the unit produces
46,000 barrels of gasoline components a year.  construction
will start late this year, with completion set for mid-1989.
 reuter
</text>]