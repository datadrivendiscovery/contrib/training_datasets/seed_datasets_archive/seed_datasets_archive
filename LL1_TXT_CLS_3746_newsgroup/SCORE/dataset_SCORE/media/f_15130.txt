[<text>
<title>telecrafter corp &lt;swhi&gt; 2nd qtr feb 28 net</title>
<dateline>    lakewood, colo, april 8 -
    </dateline>shr profit 12 cts vs loss 14 cts
    net profit 183,000 vs loss 234,000
    revs 2.4 mln vs 1.5 mln
    six months
    shr profit 22 cts vs loss 22 cts
    net profit 345,000 vs loss 358,000
    revs 5.2 mln vs 2.9 mln
    note:1987 2nd qtr and six months include gains of 78,000
dlrs and 154,000 dlrs for tax loss carryforward.
 reuter
</text>]