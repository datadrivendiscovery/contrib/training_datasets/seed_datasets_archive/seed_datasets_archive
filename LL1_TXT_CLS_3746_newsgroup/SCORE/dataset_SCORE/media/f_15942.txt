[<text>
<title>grain ships loading at portland</title>
<dateline>    portland, april 9 - </dateline>there were six grain ships loading and
eight ships were waiting to load at portland, according to the
portland merchants exchange.
  
 reuter
</text>]