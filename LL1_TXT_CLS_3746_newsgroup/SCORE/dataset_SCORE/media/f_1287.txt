[<text>
<title>continental general &lt;cgic&gt; sets stock dividend</title>
<dateline>    omaha, neb., march 3 - </dateline>continental general insurance co
said its board of directors declared a 10 pct stock dividend on
common shares, payable april one to shareholders of record
march 16.
    the company yesterday paid a quarterly cash dividend of
2-1/2 cts a share, unchanged from the previous quarter, to
shareholders of record february 20.
 reuter
</text>]