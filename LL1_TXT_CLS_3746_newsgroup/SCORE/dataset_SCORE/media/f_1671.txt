[<text>
<title>toyota motor u.s.a. february car sales down</title>
<dateline>    torrance, calif., march 4 - </dateline>toyota motor sales, u.s.a. inc
said its february car sales totaled 36,811, down from 40,012
last february.
    the company said february truck sales totaled 20,437, down
from 28,519 a year earlier.
    toyota said sales of its domestically produced corolla
models totaled 2,098 units during the month.
   
 reuter
</text>]