[<text>
<title>merrill &lt;mrll&gt; buys frayn financial printing</title>
<dateline>    st. paul, march 11 - </dateline>merrill corp said it acquired frayn
financial printing, the financial printing division of frayn
printing co, seattle. terms were not disclosed.
    it said the acquired division, now known as
merrill/seattle, becomes the company's 10th financial printing
service center.
 reuter
</text>]