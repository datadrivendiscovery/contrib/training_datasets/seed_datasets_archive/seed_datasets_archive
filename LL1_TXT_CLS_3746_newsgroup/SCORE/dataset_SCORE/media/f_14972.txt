[<text>
<title>&lt;north star uranium inc&gt; changes name</title>
<dateline>    los angeles, april 8 - </dateline>north star uranium inc said it has
changed its name to west coast traders inc to reflect its
business, the bulk importing of olive oil.
    the company said it plans to start distributing its italia
olive oil brand, now sold on the west coast, nationally.
    north star uranium had been inactive until it acquired west
coast traders in june 1986.
 reuter
</text>]