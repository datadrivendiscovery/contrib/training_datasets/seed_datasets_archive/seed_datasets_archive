[<text>
<title>yankee &lt;ynk&gt; unit not to sell subsidiary</title>
<dateline>    cohasset, mass., april 8 - </dateline>yankee cos inc said its eskey
inc &lt;esk&gt; subsidiary has decided not to sell its yale e. key
unit.
    further details were not disclosed.
 reuter
</text>]