[<text>
<title>energy conversion &lt;ener.o&gt; in superconductivity</title>
<dateline>    troy, mich., june 18 - </dateline>energy conversion devices inc said
its scientists have achieved superconductivity at  a
temperature of 90 degrees fahrenheit.
    the company said the general composition of the fluorinated
multi-phase superconducting material involved is yttrium,
barium, copper, fluorine and oxygen.
                         
 reuter
</text>]