[<text>
<title>kms industries inc &lt;kmsi.o&gt; gets contract</title>
<dateline>    ann arbor, mich., june 1 - </dateline>kms industries inc said its
covalent technology corp subsidiary won a one-year 60,000 dlr
contract for develpoing a rapid and simple method for detecting
heaptitis b antigen and diarrheal disease toxins.
    it said the award was from the program for appropriate
technology in health, in collaboration with the united states
agency for international development.
 reuter
</text>]