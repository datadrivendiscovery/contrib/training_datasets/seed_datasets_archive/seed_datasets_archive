[<text>
<title>preferred healthcare ltd &lt;phcc&gt; 4th qtr net</title>
<dateline>    wilton, conn, march 2 - 
    </dateline>shr six cts vs four cts
    net 383,189 vs 241,857
    revs 1,506,756 vs 793,459
    12 mths
    shr 24 cts vs 15 cts
    net 1,520,797 vs 929,017
    revs 5,268,486 vs 2,617,995
 reuter
</text>]