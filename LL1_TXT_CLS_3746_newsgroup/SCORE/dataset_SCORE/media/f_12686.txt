[<text>
<title>sun &lt;sun&gt; in north dakota oil find</title>
<dateline>    dallas, april 2 - </dateline>sun co inc said the o.m. steel federal
number one well in williams county, n.d., flowed 660 barrels of
oil and 581,000 cubic feet of natural gas per day through a
13/64 inch choke from depths of 13,188 to 13,204 feet.
    sun said it has a 50 pct interest and comdisco resources
inc the remainder.  an offset well is under consideration, it said.
 reuter
</text>]