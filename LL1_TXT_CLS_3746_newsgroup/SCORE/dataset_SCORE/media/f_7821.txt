[<text>
<title>monfort of colorado inc &lt;mnft&gt; 2nd qtr net</title>
<dateline>    greeley, colo., march 20 - </dateline>qtr ended feb 28
    shr 1.03 dlrs vs 1.34 dlrs
    net 4,385,000 vs 5,792,000
    revs 474.4 mln vs 381.4 mln
    six mths
    shr 2.46 dlrs vs 2.71 dlrs
    net 10.5 mln vs 11.7 mln
    revs 906.0 mln vs 757.6 mln
 reuter
</text>]