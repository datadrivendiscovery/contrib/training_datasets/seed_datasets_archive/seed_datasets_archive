[<text>
<title>fed will buy bills for customer after auction</title>
<dateline>    new york, march 30 - </dateline>the federal reserve said it will enter
the u.s. government securities market after the 1300 est weekly
bill auction to purchase around 900 mln dlrs of treasury bills
for customers, a spokesman said.
    he said the fed will purchase bills with maturities from
may through september 10.
    dealers said federal funds were trading at 6-3/8 pct when
the fed announced the operation.
 reuter
</text>]