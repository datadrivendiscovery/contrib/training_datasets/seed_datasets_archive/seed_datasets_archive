[<text>
<title>winchell's donut &lt;wdh&gt; sets initial quarterly</title>
<dateline>    la mirada, calif., march 23 - </dateline>winchell's donut houses lp
said it has declared an initial quarterly dividend of 45 cts
per unit on class a and class b limited partnership units,
payable may 29 to holders of record march 31.
 reuter
</text>]