[<text>
<title>soviet corn cargo rejected, reloading at chicago</title>
<dateline>    chicago, april 24 - </dateline>a cargo of u.s. corn for the soviet
union was rejected and forced to be unloaded at a chicago
export elevator earlier this week after it failed to make
grade, with the british vessel broompark now being reloaded, an
elevator spokesman said.
    the first attempt to load the ship failed when the
percentage of broken kernels proved to be higher than contract
specifications, he said.
    the soviets traditionally refuse to take sub-grade grain at
a price discount, as is the practice with many other importing
nations, he added.
    the official estimated the reloading of the vessel with
about 700,000 tonnes of corn may be completed by tuesday, april
28.
 reuter
</text>]