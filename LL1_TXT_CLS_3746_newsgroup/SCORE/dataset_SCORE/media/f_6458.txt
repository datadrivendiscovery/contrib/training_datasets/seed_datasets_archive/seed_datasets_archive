[<text>
<title>publicker industries inc &lt;pul&gt; 4th qtr loss</title>
<dateline>    greenwich, conn., march 18 -
    </dateline>shr loss five cts vs loss 15 cts
    net loss 619,000 vs loss 1,730,000
    sales 3,138,000 vs 5,667,000
    avg shrs 12.5 mln vs 11.5 mln
    year
    shr loss four cts vs loss 40 cts
    net loss 343,000 vs loss 3,963,000
    sales 13.4 mln vs 35.3 mln
    avg shrs 12.5 mln vs 10.3 mln
 reuter
</text>]