[<text>
<title>citadel &lt;cdl&gt; settles with great western &lt;gwf&gt;</title>
<dateline>    glendale, calif., april 1 - </dateline>citadel holding corp said it
has settled its litigation with great western financial corp.
    the company said under the terms, great western has agreed
not to acquire or seek to acquire any voting securities of
citadel or propose a merger with citadel for five years, and
citadel has paid great western six mln dlrs.
    citadel said it is continuing to pursue its claims against
salomon inc &lt;sb&gt;, which represented it in connection with the
disputed proposed merger with great western that was the
subject of the litigation.
 reuter
</text>]