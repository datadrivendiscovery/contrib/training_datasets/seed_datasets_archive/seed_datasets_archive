[<text type="unproc">
unilever plc and nv 1986 4th qtr to dec 31
    london, march 3
    unilever plc share 49.57p vs 44.19p, making 177.55p vs
137.96p for full year.
    unilever nv share 10.69 guilders vs 11.82 guilders, making
38.22 guilders vs 36.79 guilders.
    unilever plc final div 35.18p, making 50.17p vs 38.62p.
    unilever nv final div 10.67 guilders, making 15.33 guilders
vs 14.82 guilders.
    combined pre-tax profit 276 mln stg vs same, making 1.14
billion stg vs 953 mln.
    the two companies proposed a five for one share split.
    combined fourth quarter pre-tax profit 1.10 billion
guilders vs 1.11 billion, making 3.69 billion guilders vs 3.81
billion.
    operating profit 259 mln stg vs 265 mln, making 1.12
billion stg vs 949 mln.
    tax 109 mln stg vs 105 mln, making 468 mln vs 394 mln. tax
adjustments 19 mln stg credit vs nil, making 26 mln stg credit
vs three mln debit.
    attributable profit 185 mln stg vs 165 mln, making 664 mln
vs 516 mln.
    full year 1986 turnover 17.14 billion stg vs 16.69 billion.
 reuter


</text>]