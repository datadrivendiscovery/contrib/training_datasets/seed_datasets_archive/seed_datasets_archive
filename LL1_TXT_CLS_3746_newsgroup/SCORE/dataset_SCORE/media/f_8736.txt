[<text>
<title>charming shoppes inc &lt;chrs&gt; raises quarterly</title>
<dateline>    bensalem, pa., march 24 -
    </dateline>qtly div three cts vs 2-1/2 cts prior
    pay april 15
    record april six
 reuter
</text>]