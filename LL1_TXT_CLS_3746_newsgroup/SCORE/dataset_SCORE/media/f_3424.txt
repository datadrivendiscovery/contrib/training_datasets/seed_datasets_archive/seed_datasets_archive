[<text>
<title>barnett banks &lt;bbf&gt; proposes name change</title>
<dateline>    jacksonville, fla., march 9 - </dateline>barnett banks of florida inc
proposed changing its name to barnett banks inc, the company
said.
    the company said shareholders will vote on the proposal,
made in its 1987 proxy statement, at the annual meeting on
april 22.
 reuter
</text>]