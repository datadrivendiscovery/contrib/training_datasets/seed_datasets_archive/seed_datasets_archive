[<text>
<title>&lt;american hoechst corp&gt; year net</title>
<dateline>    somerville, n.j., march 16 -
    </dateline>net 38 mln vs 5.7 mln
    revs 1.71 billion vs 1.69 billion
    note: fully owned subsidiary of hoechst ag.
 reuter
</text>]