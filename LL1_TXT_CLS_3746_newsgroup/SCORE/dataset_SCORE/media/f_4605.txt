[<text>
<title>cannon &lt;can&gt; sells library to weintraub</title>
<dateline>    los angeles, march 12 - </dateline>weintraub entertainment group inc
said it agreed to acquire cannon group's screen entertainment
film library.
    the library was purchased in may 1986 as part of cannon's
acquisition of screen entertainment ltd from bond corp holdings
ltd. the library has over 2,000 theatrical motion pictures.
    terms call for the price to be established through an
appraisal process beginning immediately and not to exceed 175
mln dlrs or be below 125 mln dlrs.
   
 reuter
</text>]