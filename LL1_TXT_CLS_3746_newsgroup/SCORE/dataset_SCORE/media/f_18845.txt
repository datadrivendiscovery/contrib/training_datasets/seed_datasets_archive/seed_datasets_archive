[<text>
<title>braintree savings bank &lt;btsb.o&gt; sets payout</title>
<dateline>    braintree, mass., june 18 -
    </dateline>qtly div five cts vs five cts prior
    pay july 15
    record june 26
 reuter
</text>]