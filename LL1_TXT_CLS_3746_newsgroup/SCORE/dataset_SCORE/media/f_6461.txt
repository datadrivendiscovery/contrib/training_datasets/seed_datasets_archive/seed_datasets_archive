[<text>
<title>rhodes &lt;rhds&gt; sees sharply lower 4th qtr net</title>
<dateline>    atlanta, march 18 - </dateline>rhodes inc said it now estimates fourth
quarter, ended february 28, earnings were eight cts a share,
down from the 25 cts earned in the final quarter of fiscal
1986.
    the company said the major causes for the reduction were an
unfavorable lifo adjustment and softer than projected sales in
the quarter, due primarily to unfavorable weather.
    despite the 4th qtr results, rhodes said, "net income for
the year just ended should come close to the record level of
the previous year" -- 1.61 dlrs dlrs a share.
 reuter
</text>]