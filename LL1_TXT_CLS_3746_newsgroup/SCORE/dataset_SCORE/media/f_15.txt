[<text>
<title>national intergroup&lt;nii&gt; to offer permian units</title>
<dateline>    pittsburgh, feb 26 - </dateline>national intergroup inc said it plans
to file a registration statement with the securities and
exchange commission for an offering of cumulative convertible
preferred partnership units in permian partners l.p.
    the permian partners l.p. was recently formed by national
intergroup to continue to business of permian corp, acquired by
the company in 1985.
    the company said permian will continue to manage the
business as a general partner, retaining a 35 pct stake in the
partnership in the form of common and general partnership
units.
    it did not say how many units would be offered or what the
price would be.
 reuter
</text>]