[<text>
<title>white house opposes stock transfer tax</title>
<dateline>    washington, march 11 - </dateline>the white house said that a proposal
by house speaker jim wright for a tax on stock transfers "would
clearly be a tax increase" and that president reagan opposed it.
    spokesman marlin fitzwater noted that some 47 mln americans
owned securities, shares in mutual funds or had pensions
invested in the stock market.
    he said wright's proposal differed from user fees, which
the administration has proposed in its budget and "clearly is a
tax increase."
 reuter
</text>]