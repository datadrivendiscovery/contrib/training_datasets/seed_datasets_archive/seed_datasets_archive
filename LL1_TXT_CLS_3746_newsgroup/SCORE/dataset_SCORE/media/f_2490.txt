[<text>
<title>&lt;spar aerospace ltd&gt; year net</title>
<dateline>    toronto, march 5 -
    </dateline>shr basic 42 cts vs 1.41 dlrs
    shr diluted 42 cts vs 1.33 dlrs
    net 4,394,000 vs 13,070,000
    revs 191.0 mln vs 223.3 mln
 reuter
</text>]