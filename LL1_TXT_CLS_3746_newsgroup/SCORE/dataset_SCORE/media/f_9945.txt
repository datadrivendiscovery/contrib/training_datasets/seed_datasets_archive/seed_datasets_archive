[<text>
<title>ge &lt;ge&gt; credit sues phoenix financial &lt;phfc&gt;</title>
<dateline>    medford, n.j., march 26 - </dateline>phoenix financial corp said it
was sued by general electric credit corp, a unit of general
electric co, for breach of warranties and representations.
    the warranties are connected with leases the company said
it sold to ge credit in june 1985. ge is asking the company to
buy back leases totaling 829,000 dlrs.
    the company said there were no breach of warranties, but it
added it is in the process of reviewing the complaint and
preparing an answer.
 reuter
</text>]