[<text>
<title>sumitomo bank &lt;sumi&gt; declares stock dividend</title>
<dateline>    san francisco, march 11 - </dateline>sumitomo bank of california said
it declared a 7-1/2 pct stock dividend in addition to its
regular quarterly cash dividend of 29 cts per share.
    both dividends are payable april 27 to shareholders of
record march 31, the company said.
 reuter
</text>]