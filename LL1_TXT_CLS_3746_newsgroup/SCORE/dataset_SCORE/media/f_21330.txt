[<text>
<title>fpl group inc &lt;fpl&gt; 3rd qtr net</title>
<dateline>    north palm beach, fla., oct 19 -
    </dateline>shr 1.16 dlrs vs 1.19 dlrs
    net 151.4 mln vs 152.4 mln
    revs 1.31 billion vs 1.17 billion
    avg shrs 130.0 mln vs 127.6 mln
    12 mths
    shr 3.03 dlrs vs 2.85 dlrs
    net 392.7 mln vs 353.0 mln
    revs 4.32 billion vs 4.14 billion
    avg shrs 129.8 mln vs 123.9 mln
 reuter
</text>]