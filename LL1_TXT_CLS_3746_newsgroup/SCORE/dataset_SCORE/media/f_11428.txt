[<text>
<title>watso inc &lt;wsoa&gt; sets regular payouts</title>
<dateline>    coconut grove, fla., march 31 -
    </dateline>qtrly class a div five cts vs five cts prior
    qtrly class b div four cts vs four cts prior
    pay may 29
    record april 30
    note: company changed date for its annual shareholders'
meeting to june 24 from june 15 due to a scheduling conflict.
 reuter
</text>]