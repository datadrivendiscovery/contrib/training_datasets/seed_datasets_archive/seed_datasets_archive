[<text>
<title>co-operative &lt;cobk.o&gt; completes acquisition</title>
<dateline>    acton, mass., june 1 - </dateline>co-operative bancorp said it
completed the acquisition of all the issued and outstanding
stock of the quincy co-operative bank &lt;qbck.o&gt;.
    under the agreement, quincy stockholders will receive 30
dlrs cash for each share owned of the quincy co-operative bank,
for a total transaction of approximately 50 mln dlrs,
co-operative said.
 reuter
</text>]