[<text>
<title> pension insurance group &lt;pgai&gt; 4th qtr net</title>
<dateline>    valley forge, pa., march 19 -
    </dateline>shr profit two cts vs profit two cts
    net profit 216,000 vs profit 265,000
    revs 1,661,000 vs 1,376,000
    12 mths
    shr profit four cts vs loss two cts
    net profit 528,000 vs loss 290,000
    revs 5,881,000 vs 5,541,000
    note: full name of company is pension insurance group of
america inc.
 reuter
</text>]