[<text>
<title>egypt tenders thursday for optional origin corn</title>
<dateline>    kansas city, march 31 - </dateline>egypt will tender thursday for
200,000 tonnes of optional origin corn, u.s. number two or
equivalent, 14.5 pct moisture, for late april shipment, private
export sources said.
    shipment will be from the gulf or great lakes if u.s.
origin, they said.
 reuter
</text>]