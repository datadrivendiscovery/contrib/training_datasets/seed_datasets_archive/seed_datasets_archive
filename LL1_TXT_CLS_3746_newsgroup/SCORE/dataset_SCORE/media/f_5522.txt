[<text>
<title>franklin resources inc &lt;ben&gt; sets quarterly</title>
<dateline>    san mateo, calif., march 16 -
    </dateline>qtly div six cts vs six cts prior
    pay april 10
    record march 27
 reuter
</text>]