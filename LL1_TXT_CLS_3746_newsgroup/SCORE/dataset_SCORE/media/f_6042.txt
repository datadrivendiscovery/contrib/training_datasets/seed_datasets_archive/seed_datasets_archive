[<text>
<title>applied circuit technology &lt;acrt&gt; 1st qtr loss</title>
<dateline>    anaheim, calif., march 17 - </dateline>period ended january 31.
    shr loss two cts vs loss 12 cts
    net loss 192,370 vs loss 1,494,146
    revs 6,751,830 vs 2,278,842
    note: full name applied circuit technology inc.
 reuter
</text>]