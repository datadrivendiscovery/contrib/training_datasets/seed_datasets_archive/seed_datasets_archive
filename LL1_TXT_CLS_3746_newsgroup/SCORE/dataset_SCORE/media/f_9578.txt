[<text>
<title>redkin laboratories inc &lt;rdkn&gt; qtly dividend</title>
<dateline>    canoga park, calif., march 25 -
    </dateline>shr five cts vs five cts prior qtr
    pay april 17
    record april 3.
 reuter
</text>]