[<text>
<title>chicago pacific &lt;cpa&gt; to repurchase notes</title>
<dateline>    new york, june 2 - </dateline>chicago pacific corp chairman harvey
kapnick told analysts that he expects whithin the next year to
repurchase about 25 mln dlrs of the company's 200 mln dlr issue
of 14 pct subordinated notes.
    to date the company has repurchased about 60 mln of these
notes.
    "this will have a beneficial effect on our net income but
the effects in 1987 have already been anticipated in estimates
for 1987," kapnik said.
 reuter
</text>]