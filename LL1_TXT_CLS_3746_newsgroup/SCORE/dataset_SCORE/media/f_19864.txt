[<text>
<title>rte corp &lt;rte&gt; buys emhart corp unit</title>
<dateline>     brookfield, wis., june 29 - </dateline>rte corp said it completed the
purchase of emhart corp's united states aluminum electrolytic
capacitor business for an undisclosed sum.
    the company said the capacitor business is part of emhart's
electrical and electronic group marketed under the mallory
brand name.
    the company said the product lines it acquired had sales of
25 mln dlrs last year.
 reuter
</text>]