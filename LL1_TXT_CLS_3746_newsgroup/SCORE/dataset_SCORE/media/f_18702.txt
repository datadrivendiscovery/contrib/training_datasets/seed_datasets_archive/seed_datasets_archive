[<text>
<title>nabisco brands ltd &lt;nab.to&gt; names new ceo</title>
<dateline>    toronto, june 18 - </dateline>nabisco brands ltd, 80 pct-owned by rjr
nabisco inc &lt;rjr&gt;, said it named president r. edward glover as
chief executive, replacing j.r. macdonald, who remains as
vice-chairman and chairman of the executive committee,
effective immediately.
    glover has been president and chief operating officer since
april, 1986, nabisco said.
 reuter
</text>]