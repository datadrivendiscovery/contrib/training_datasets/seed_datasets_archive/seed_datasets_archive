[<text>
<title>highland superstores &lt;high&gt; march sales gain</title>
<dateline>    taylor, mich., april 8 - </dateline>highland superstores inc said its
march sales rose to 50.6 mln dlrs from 47.4 mln dlrs a year
ago.
    on a comparable store basis, it said the sales decreased
nine pct.
 reuter
</text>]