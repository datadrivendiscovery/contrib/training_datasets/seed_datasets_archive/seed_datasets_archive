[<text>
<title>unitil corp &lt;utl&gt; 4th qtr net</title>
<dateline>    bedford, n.h., march 5 -
    </dateline>shr 79 cts vs 72 cts
    net 581,915 vs 536,040
    revs 13.9 mln vs 13.3 mln
    year
    shr 3.28 dlrs vs 3.21 dlrs
    net 2,413,407 vs 2,360,048
    revs 54.9 mln vs 54.2 mln
 reuter
</text>]