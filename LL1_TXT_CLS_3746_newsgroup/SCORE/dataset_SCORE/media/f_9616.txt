[<text>
<title>el paso electric &lt;elpa&gt; to buy back bonds</title>
<dateline>    new york, march 25 - </dateline>el paso electric co said it plans to
redeem its 40 mln dlrs of 16.35 pct first mortgage bonds of
1991 and 60 mln dlrs of 16.20 pct first mortgage bonds of 2012.
    it will buy back at par plus accrued interest the 16.35 pct
issue in may and the 16.20 pct bonds after august one.
    el paso electric will use proceeds of about 684.4 mln dlrs
from sales and leasebacks, completed in 1986, of its entire
ownership interest in palo verde nuclear generating station
unit 2 to redeem the double-digit debt.
 reuter
</text>]