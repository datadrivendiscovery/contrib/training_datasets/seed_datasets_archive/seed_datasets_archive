[<text>
<title>united financial banking &lt;ufbc.o&gt; 1st qtr net</title>
<dateline>    vienna, va., june 2 -
    </dateline>shr four cts vs 21 cts
    net 29,862 vs 152,826
    note: full name is united financial banking cos inc. net
includes loan loss provision nil vs 40,000 dlrs.
 reuter
</text>]