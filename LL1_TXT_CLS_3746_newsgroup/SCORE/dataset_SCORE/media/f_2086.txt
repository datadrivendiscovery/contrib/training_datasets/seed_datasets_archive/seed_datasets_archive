[<text>
<title>carson pirie scott &lt;crn&gt; february sales up</title>
<dateline>    chicago, march 5 - </dateline>carson pirie scott and co said its
february sales increased 18.2 pct to 104.3 mln dlrs from 88.2
mln dlrs a year ago.
    it said each of its business groups - retail foodservice
and lodging and distribution - contributed to the sales gain.
    the food service and lodging group's sales were up 8.9 pct
after eliminating sales of the steak 'n egg kitchen restaurant
chain sold aug 15, 1986.
 reuter
</text>]