[<text>
<title>motorola &lt;mot&gt; selects system compilers</title>
<dateline>    tempe, ariz., april 7 - </dateline>motorola inc said it has selected
&lt;oregon software's&gt; pascal-2 compilers as the compiler for its
mc68000 family of 16-bit microprocessor chips under the
versados and system v/68 operating systems.
    also, motorola said that its microcomputer division and
oregon software will be the sole supplier of the pascal
suppliers.
    motorola said the pascal-2 compiler for system v/68,
supporting the mc68020 and mc68881, is priced at 2,800 dlrs for
june delivery.
    it added that the pascal-2 compiler for the versados system
is priced at 2,800 dlrs for june delivery.
 reuter
</text>]