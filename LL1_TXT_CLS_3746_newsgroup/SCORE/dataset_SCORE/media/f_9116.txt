[<text>
<title>first new hampshire banks inc div</title>
<dateline>    manchester, n.h, march 24 -
    </dateline>qtly div 15 cts vs 15 cts prior
    payable may one
    record april 10
 reuter
</text>]