[<text>
<title>tracor &lt;trr&gt; gets navy contract</title>
<dateline>    austin, texas, march 19 - </dateline>tracor inc's applied sciences inc
unit said it received an 8.4 mln dlrs contract from the u.s.
navy
    the contract is for one year and includes four one-year
options.
 reuter
</text>]