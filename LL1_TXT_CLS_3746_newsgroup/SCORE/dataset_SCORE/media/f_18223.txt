[<text>
<title>group lifts stake in scandinavia fund &lt;scf&gt;</title>
<dateline>    washington, june 2 - </dateline>a foreign investment group told the
securities and exchange commission it raised its stake in
scandinavia fund inc to 3,040,600 shares, or 46.6 pct of the
total outstanding, from 2,829,300 shares, or 43.2 pct.
    the group, which includes ingemar rydin industritillbehor
ab, a swedish firm, vbi corp, a british west indies investment
firm, and norwegian investor erik vik, said it bought 211,300
scandinavia common shares since april 30 at prices ranging from
10.000 to 10.625 dlrs a share.

 reuter
</text>]