[<text>
<title>norway offers 11th licence round offshore blocks</title>
<dateline>    oslo, march 25 - </dateline>norway has offered 10 new offshore blocks
to foreign and domestic applicants in the first phase of the
country's eleventh concession round, government officials said.
    company shares in each of the licences proposed by the oil
and energy ministry are not final. the ministry has given the
companies 10 days to accept or decline the proposed shares.
    french companies ste nationale elf aquitaine &lt;elfp.pa&gt; and
total cie francaise des petroles &lt;tpn.pa&gt;, which were expected
to receive operatorships following france's agreement last
autumn to purchase gas from norway's troll field, were not
offered operatorships in this round, industry sources said.
    three eleventh round blocks were awarded in the
haltenbanken exploration tract off central norway, including
the smoerbukk west field where den norske stats oljeselskap a/s
&lt;stat.ol&gt; (statoil) was appointed operator.
    statoil will share the licence with subsidiaries of u.s.
oil companies tenneco inc &lt;tgt.n&gt; and texas eastern corp
&lt;tet.n&gt; and the italian oil company &lt;agip spa&gt;'s norwegian
subsidiary.
    e.i. du pont de nemours &lt;dd.n&gt; subsidiary conoco norway inc
was named operator on haltenbanken block 6406/8 and will share
the licence with statoil.
    norsk hydro a/s &lt;nhy.ol&gt; will operate nearby block 6407/10
with partners statoil, norsk agip a/s, royal dutch/shell
group's &lt;rd.as&gt; a/s norske shell and &lt;deminex&gt; unit deminex
(norge) a/s.
    statoil has been offered the operatorship on a new block in
the relatively unexplored moere south exploration area south of
haltenbanken, with a/s norske shell, texas eastern and
&lt;petroleo brasileiro sa&gt; (petrobras) also offered stakes in the
block.
    norwegian companies landed operatorships on all six blocks
opened in the barents sea area off northern norway. the blocks
were awarded in three licenses, each covering two blocks.
    statoil will head exploration on blocks 7224/7 and 7224/8,
sharing the licence with exxon corp's &lt;xon.n&gt; norwegian
subsidiary esso norge a/s, the british petroleum co plc's
&lt;bp.l&gt; bp petroleum development (norway) ltd, shell, norsk
hydro and saga petroleum a/s &lt;sagp.ol&gt;.
    blocks 7219/9 and 7220/7 were awarded to norsk hydro, the
operator, statoil, mobil corp's &lt;mob.n&gt; mobil exploration
norway, petrofina sa's &lt;petb.br&gt; norske fina a/s and bp.
    the third barents sea licence, covering blocks 7124/3 and
7125/1, went to saga petroleum a/s, the operator, statoil,
atlantic richfield co's &lt;arc.n&gt; arco norge a/s, total marine
norge a/s and amerada hess corp &lt;ahc.n&gt;.
    the oil ministry withheld awards on four strategic blocks
included in the eleventh round's second phase.
    the ministry is accepting applications for phase two blocks
until early april and the awards will likely be announced this
summer, officials said.
 reuter
</text>]