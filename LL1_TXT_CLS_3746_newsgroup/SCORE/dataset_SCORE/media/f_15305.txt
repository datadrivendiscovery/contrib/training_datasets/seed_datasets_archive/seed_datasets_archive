[<text>
<title>mcf financial plans initial public offering</title>
<dateline>    encino, calif., april 8 - </dateline>mcf financial corp said it filed
a registration statement with the securities and exchange
commission covering a proposed initial public offering of
1,075,000 common shares.
    mcf said proceeds from the offering will be used to repay
debt, to purchase loan participation interests and for working
capital.
    mcf is engaged in the commercial finance business.
 reuter
</text>]