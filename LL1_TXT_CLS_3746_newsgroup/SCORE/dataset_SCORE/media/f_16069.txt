[<text>
<title>dollar falls below 143 yen in tokyo</title>
<dateline>    tokyo, april 10 - </dateline>the dollar fell below 143 yen in hectic
early tokyo trading despite aggressive bank of japan
intervention, dealers said.
    after opening at a tokyo low of 143.75 yen, the dollar fell
as low as 142.90 yen on heavy selling led by securities firms
and institutional investors, they said.
 reuter
</text>]