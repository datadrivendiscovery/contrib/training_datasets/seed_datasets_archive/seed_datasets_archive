[<text>
<title>china soybean output down slightly - usda report</title>
<dateline>    washington, april 7 - </dateline>china's soybean crop this year is
forecast at 11.5 mln tonnes, down slightly from 11.55 mln
estiamted for last year, the u.s. agriculture department's
officer in peking said in a field report.
    the report, dated april 2, said chinese imports this year
are projected at 300,000 tonnes unchanged from last year's
level.
    exports are forecast to increase to 1.0 mln tonnes from
800,000 tonnes exported last year, the report said.
    imports of soybean oil are estimated at 200,000 tonnes,
also unchanged from last year.
 reuter
</text>]