[<text>
<title>socal edison &lt;sce&gt; signs hoover dam power pact</title>
<dateline>    rosemead, calif., april 8 - </dateline>southern california edison said
it signed a 30-year contract with the federal government for
hydroelectric power from hoover dam.
    southern california edison said it would receive some 277.5
megawatts of electrical power under the contract, enough to
serve about 160,000 homes.
    terms of the contract were not disclosed.
    the original 50-year contracts for hoover dam hydroelectric
power are scheduled to expire at the end of may, the company
said.
 reuter
</text>]