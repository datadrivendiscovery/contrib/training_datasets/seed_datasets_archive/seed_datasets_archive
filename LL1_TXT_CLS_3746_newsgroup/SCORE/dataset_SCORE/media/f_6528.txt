[<text>
<title>brad ragan inc &lt;brd&gt; sets quarterly</title>
<dateline>    charlotte, n.c., march 18 -
    </dateline>qtly div three cts vs three cts prior
    pay may 4
    record april 3
 reuter
</text>]