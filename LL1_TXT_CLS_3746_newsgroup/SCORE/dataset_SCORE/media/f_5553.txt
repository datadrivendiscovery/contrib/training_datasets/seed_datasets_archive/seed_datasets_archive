[<text>
<title>gotaas-larsen &lt;gotlf&gt; to build fifth carrier</title>
<dateline>    hamilton, bermuda, march 16 - </dateline>gotaas-larsen shipping corp
said it had exercised an option to build a fifth in a series of
crude oil carriers to be constructed by &lt;daewoo shipbuilding
and heavy machinery ltd&gt; in south korea.
 reuter
</text>]