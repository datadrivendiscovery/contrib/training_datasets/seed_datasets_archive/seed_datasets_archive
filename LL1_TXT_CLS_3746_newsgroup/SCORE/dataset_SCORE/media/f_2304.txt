[<text>
<title>amrep corp &lt;axr&gt; 3rd qtr jan 31 net</title>
<dateline>    new york, march 5 -
    </dateline>shr 12 cts vs 34 cts
    net 787,000 vs 2,250,000
    revs 23.6 mln vs 23.6 mln
    nine mths
    shr 70 cts vs 1.06 dlrs
    net 4,598,000 vs 6,974,000
    revs 73.1 mln vs 73.6 mln
    note: share adjusted for three-for-two stock split in
december 1986.
 reuter
</text>]