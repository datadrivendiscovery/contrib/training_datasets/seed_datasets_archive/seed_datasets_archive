[<text>
<title>thompson medical co inc &lt;tm&gt; sets quarterly</title>
<dateline>    new york, march 20 -
    </dateline>qtly div 10 cts vs 10 cts prior
    pay april 15
    record march 30
 reuter
</text>]