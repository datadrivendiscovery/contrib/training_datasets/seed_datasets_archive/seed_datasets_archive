[<text>
<title>firstier &lt;frst&gt; names new chief executive</title>
<dateline>    omaha, march 5 - </dateline>firstier inc said president william c.
smith has been named chief executive officer, succeeding
charles w. durham, who remains chairman.
    the company said durham had been chief executive on an
interim basis only.
 reuter
</text>]