[<text>
<title>american security &lt;asec&gt; to release information</title>
<dateline>    washington, march 16 - </dateline>american security corp said a
release will be forthcoming shortly regarding its pending
merger with maryland national corp &lt;mdnt&gt;, approved by its
stockholders october 10.
    american was halted on nasdaq pending a news announcement.
 reuter
</text>]