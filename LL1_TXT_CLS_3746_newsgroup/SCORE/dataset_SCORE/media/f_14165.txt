[<text>
<title>university &lt;upt&gt; reports march lens sale</title>
<dateline>    westport, conn., april 7 - </dateline>university patents inc said its
optical products subsidiary sold 5,652 alges soft bifocal
contact lenses in march, after allowances for exchanges and
returns.
    the lenses were approved for marketing in april 1986 by the
food and drug administration.
 reuter
</text>]