[<text>
<title>mitsui and co buys stake in imatron</title>
<dateline>   tokyo, march 19 - </dateline>a mitsui and co &lt;mits.t&gt; spokesman said
its subsidiary &lt;mitsui and co usa inc&gt; bought two mln dlrs of
newly-issued shares in &lt;imatron inc&gt;, an unlisted
california-based medical equipment manufacturer.
    mitsui and co usa is now imatron's fifth-largest
shareholder with 3.1 pct of the firm's outstanding shares.
imatron is capitalised at 32.76 mln dlrs, the spokesman said.
    mitsui and co intends to import imatron's computerised
diagnostic equipment into japan, he said.
 reuter
</text>]