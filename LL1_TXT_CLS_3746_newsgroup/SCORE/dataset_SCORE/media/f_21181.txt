[<text>
<title>aspen ribbons inc &lt;arib.o&gt; 1st qtr net</title>
<dateline>    lafayette, colo., oct 19 - </dateline>sept 30
    shr seven cts vs five cts
    net 234,504 vs 157,862
    revs 4,096,000 vs 3,007,383
 reuter
</text>]