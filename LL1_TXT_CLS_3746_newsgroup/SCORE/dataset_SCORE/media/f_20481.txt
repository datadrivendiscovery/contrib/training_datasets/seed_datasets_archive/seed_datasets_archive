[<text>
<title>empire of carolina &lt;emp&gt; in restructuring</title>
<dateline>    tarboro, n.c., oct 20 - </dateline>empire of carolina inc said it
plans to streamline operations, which will result in an
approximate 20 pct reduction of manufacturing management and
support personnel.
    the company said it expects to increase production
personnel by a like amount of people.
 reuter
</text>]