[<text>
<title>biocraft &lt;bcl&gt;, american cyanamid &lt;acy&gt; in pact</title>
<dateline>    elmwood park, n.j., march 23 - </dateline>biocraft laboratories inc
said it signed a contract with american cyanamid co to make
cefixime, a third generation oral cephalosporin.
    it said american cyanamid's lederle laboratories filed a
new drug application with the food and drug administration for
the product.
    according to the agreement, biocraft will manufacture the
product for american cyanamid for the first three years of
commercial production.
 reuter
</text>]