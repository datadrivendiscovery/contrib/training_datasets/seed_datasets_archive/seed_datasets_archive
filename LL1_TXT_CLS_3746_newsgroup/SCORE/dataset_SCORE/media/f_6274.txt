[<text>
<title>marcus corp &lt;mrcs&gt; 3rd qtr feb five net</title>
<dateline>    milwaukee, wis., march 17 -
    </dateline>shr 14 cts vs 15 cts
    net 733,000 vs 788,000
    revs 31.9 mln vs 28.9 mln
    nine mths
    shr 1.08 dlrs vs 1.20 dlrs
    net 5,560,000 vs 6,162,000
    revs 104.5 mln vs 97.2 mln
    note: 1987 net includes tax credits of 25,000 dlrs in the
third quarter and 100,000 dlrs in the nine months compared with
370,000 dlrs and 910,000 dlrs in the 1986 periods.
 reuter
</text>]