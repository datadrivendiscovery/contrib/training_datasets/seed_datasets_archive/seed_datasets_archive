[<text>
<title>petrolite corp &lt;plit&gt; sets payout</title>
<dateline>    st. louis, march 2 -
    </dateline>qtly dividend 28 cts vs 28 cts
    pay april 24
    record april 10
 reuter
</text>]