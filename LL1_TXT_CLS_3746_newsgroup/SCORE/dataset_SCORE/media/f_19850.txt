[<text>
<title>laidlaw transportation ltd (ldmf.o) third qtr</title>
<dateline>    toronto, june 29 - 
    </dateline>shr 20 cts vs 12 cts
    net 34,088,000 vs 18,727,000
    revs 340.4 mln vs 200.3 mln
    nine mths
    shr 53 cts vs 31 cts
    net 88,661,000 vs 49,059,000
    revs 926.5 mln vs 560.1 mln
    avg shrs 151.1 mln vs 137.0 mln
 reuter
</text>]