[<text>
<title>advanced systems &lt;asy&gt; to distribute courses</title>
<dateline>    chicago, march 2 - </dateline>advanced systems inc said it acquired
distribution rights to three interactive video courses
developed by ncr corp &lt;ncr&gt;.
    the three courses, for data processing professionals and
bankers, are delivered through interactive video, which
combines video discs, personal computers and touch-screen
monitors.
 reuter
</text>]