[<text>
<title>nucor corp &lt;nue&gt; raises quarterly</title>
<dateline>    charlotte, n.c., march 11 -
    </dateline>qtly div nine cts vs eight cts prior
    pay may 12
    record march 31
 reuter
</text>]