[<text>
<title>british aerospace raises system designers stake</title>
<dateline>    london, march 11 - </dateline>british aerospace plc &lt;bael.l&gt; said it
has increased its stake in &lt;systems designers plc&gt; to 22.1 pct
or 25.46 mln ordinary shares following the purchase of 10.45
mln ordinary shares.
    the british aerospace pension fund holds 2.15 mln ordinary
shares in systems, representing a stake of 1.9 pct.
    a spokesman for british aerospace said it has no present or
future intention of making a full bid for systems designers.
system designers shares were nine pence higher at 100 prior to
the share stake announcement, and have showed little movement
since.
 reuter
</text>]