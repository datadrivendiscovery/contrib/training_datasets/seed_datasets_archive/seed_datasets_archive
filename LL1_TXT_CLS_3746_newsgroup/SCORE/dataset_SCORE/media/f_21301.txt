[<text>
<title>shamrock capital completes central soya sale</title>
<dateline>    burbank, calif., oct 19 - </dateline>shamrock capital l.p, a limited
partnership led by shamrock holdings inc, said it completed its
sale of central soya co inc to ferruzzi agricola finanziaria of
italy.
    under terms of the sale agreement, which was announced on
september 15, ferruzzi acquired all the equity in central soya
and assumed subordinated term debt of about 195 mln dlrs in a
transaction valued at about 370 mln dlrs.
 reuter
</text>]