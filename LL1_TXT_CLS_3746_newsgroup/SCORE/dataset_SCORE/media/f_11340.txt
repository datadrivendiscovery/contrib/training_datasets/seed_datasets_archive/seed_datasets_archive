[<text>
<title>colorocs corp &lt;clrx&gt; year loss</title>
<dateline>    norcross, ga., march 31 -
    </dateline>shr loss 28 cts vs loss 29 cts
    net loss 2,086,477 vs loss 1,466,907
    revs 218,864 vs 60,000
    avg shrs 7,510,781 vs 4,990,168
 reuter
</text>]