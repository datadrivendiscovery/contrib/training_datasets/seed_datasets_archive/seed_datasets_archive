[<text>
<title>safecard services &lt;sfcd&gt; sets split, ups payout</title>
<dateline>    fort lauderdale, fla., march 5 - </dateline>safecard services inc said
its board declared a three-for-two stock split and is
maintaining the quarterly dividend on post-split shares at the
same six cts it now pays for an effective 50 pct increase.
    both the split and the dividend are payable april 30 to
holders of record march 31.
 reuter
</text>]