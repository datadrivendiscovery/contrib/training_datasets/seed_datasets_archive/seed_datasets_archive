[<text>
<title>bell south &lt;bls&gt; unit gets digital customers</title>
<dateline>    atlanta, june 18 - </dateline>bell south corp's southern bell said
american telephone and telegraph co's &lt;t&gt; network systems and
&lt;hayes microcomputer products&gt; will use its integrated services
digital network services when the product is launched in march
1988.
 reuter
</text>]