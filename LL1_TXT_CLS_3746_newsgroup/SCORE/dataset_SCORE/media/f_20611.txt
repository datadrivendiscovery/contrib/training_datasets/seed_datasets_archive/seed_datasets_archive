[<text>
<title>ccc preparing wheat catalogue</title>
<dateline>    kansas city, oct 20 - </dateline>the kansas city commodity credit corp
office is preparing a wheat catalogue containing roughly 300
mln bushels, scheduled to be released in the next two to three
weeks, a ccc spokesman said.
    the catalogue should include all ccc stocks stored at
terminals and about 50 pct of the stocks stored at country
elevators, the spokesman said. hard red winter wheat should
comprise the bulk of the stocks, followed by spring wheat, he
said.
    the release date is tentative in case there are snags in
the catalogue's preparation, the spokesman said.
 reuter
</text>]