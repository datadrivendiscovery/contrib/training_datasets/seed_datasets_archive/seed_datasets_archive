[<text>
<title>microdyne corp &lt;mcdy.o&gt; 2nd qtr may three net</title>
<dateline>    oscala, fla., june 18 -
    </dateline>oper shr nil vs profit one ct
    oper net profit 14,000 vs profit 51,000
    revs 5,547,000 vs 6,021,000
    six mths
    oper shr loss nine cts vs profit seven cts
    oper net loss 383,000 vs profit 314,000 dlrs
    revs 9,31,000 dlrs vs 12.5 mln
    note: 1986 qtr and six mths excludes loss 171,000 dlrs and
358,000 dlrs, respectively, for loss from discontinued
operations.
 reuter
</text>]