[<text>
<title>butler national corp &lt;butl&gt; 3rd qtr jan 31</title>
<dateline>    kansas city, mo., march 19 -
    </dateline>shr profit nil vs loss one ct
    net profit 1,136 vs loss 42,840
    revs 1,490,718 vs 1,151,176
    nine mths
    shr profit three cts vs profit five cts
    net profit 89,900 vs profit 150,523
    revs 4,520,393 vs 4,078,441
 reuter
</text>]