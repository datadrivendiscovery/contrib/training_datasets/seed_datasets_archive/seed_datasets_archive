[<text>
<title>&lt;sun hung kai co ltd&gt; year 1986</title>
<dateline>    hong kong, march 18 -
    </dateline>shr 21.6 h.k. cents vs 12
    final div six cents vs three, making nine cents vs 5.5
    net 121 mln dlrs vs 67.42 mln
    note - extraordinary gains 72 mln dlrs vs 2.7 mln. special
bonus four cents vs nil. dividend payable may 25, books close
april 28 to may 6.
 reuter
</text>]