[<text>
<title>mickelberry corp &lt;mbc&gt; sets qtly payout</title>
<dateline>    new york, march 4 -
    </dateline>qtly div 1-1/2 cts vs 1-1/2 cts prior
    pay march 31
    record march 13
 reuter
</text>]