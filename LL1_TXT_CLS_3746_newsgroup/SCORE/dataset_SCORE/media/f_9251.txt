[<text>
<title>metro cable &lt;meto&gt; to sell partnership</title>
<dateline>    englewood, colo., march 25 - </dateline>metro cable corp said it has
entered into a letter of intent for the sale of limited
partnership intermountain cable associates for about 3,250,000
dlrs.
    metro and dmn cable investors are co-general partners for
intermountain.  the name of the buyer was not disclosed.
    metro said it has also transfered its northwest iowa and
eastern colorado cablevision systems into a new wholly-owned
subsidiary called mcc cablevision. 
    metro said the new unit received 2,700,000 dlrs in
financing from bank of boston corp &lt;bkb&gt; which was used to
retire metro cable's outstanding debt to &lt;national bank of
canada&gt; and for working capital.
 reuter
</text>]