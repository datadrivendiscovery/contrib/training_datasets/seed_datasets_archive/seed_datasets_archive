[<text>
<title>rossignol unit buys canadian ski boot maker</title>
<dateline>    paris, march 4 - </dateline>french ski and tennis equipment maker
&lt;skis rossignol&gt; said its 97.7-pct owned subsidiary &lt;skis
dynastar sa&gt; agreed to buy canadian ski stick and boot
manufacturer &lt;cfas&gt; from &lt;warrington inc&gt;.
    a rossignol spokesman declined to give financial details
but said turnover of cfas was about 100 mln french francs,
doubling the rossignol group's activities in the boot and stick
sectors.
 reuter
</text>]