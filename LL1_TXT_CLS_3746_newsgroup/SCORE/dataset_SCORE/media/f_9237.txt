[<text>
<title>enterra corp &lt;en&gt; 4th qtr loss</title>
<dateline>    houston, march 25 -
    </dateline>shr loss 4.14 dlrs vs loss 19 cts
    net loss 37.1 mln vs loss 1,712,000
    revs 27.3 mln vs 33.4 mln
    year
    shr loss 5.51 dlrs vs loss 73 cts
    net loss 49.3 mln vs loss 6,544,000
    revs 109.0 mln vs 141.9 mln
    note: 1986 net both periods includes 34.8 mln dlr
weritedown of assets of services segment and southeast asian
joint venture.
 reuter
</text>]