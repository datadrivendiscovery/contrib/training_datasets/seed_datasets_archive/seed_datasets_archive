[<text>
<title>u.s. to ask japan to drop beef restrictions</title>
<dateline>    stillwater, okla., march 23 - </dateline>u.s. agriculture secretary
richard lyng will ask the japanese government to remove all
beef import restrictions when he visits there next month.
    lyng's remarks came in a speech at oklahoma state
university today.
    "we think japanese consumers should have the same freedom
of choice as our consumers. look at all the japanese cameras
and tape recorders in this room. we know they'd buy more beef
if they had the opportunity," lyng said.
 reuter
</text>]