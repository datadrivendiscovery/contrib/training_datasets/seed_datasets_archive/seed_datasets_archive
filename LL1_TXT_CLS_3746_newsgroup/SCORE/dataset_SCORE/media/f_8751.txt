[<text>
<title>control data &lt;cda&gt; announces new computer</title>
<dateline>    new york, march 24 - </dateline>control data corp said it is offering
its first departmental computer, the cyber 1809 model 930.
    it also announced enhanced versions of its cdcnet
communications network, nos/ve virtual operating system and
im/ve information management system.
    control data also said all systems operate with the cyber
930 system.
    the company said the cyber 930 is available in two models.
both are said to deliver two to three times the input-output
capability. the 930-11 model is priced at 59,900 dlrs, and the
930-31, is priced at 125,900 dlrs.
 reuter
</text>]