[<text>
<title>consolidated oil &lt;cgs&gt; to issue news shortly</title>
<dateline>    denver, march 17 - </dateline>consolidated oil and gas inc and
consolidated energy partners lp &lt;cps&gt;, whose stocks are halted
on the american stock exchange, said they will issue news
releases shortly.
    a spokesman for the companies declined to comment on why
the stocks had been halted.
 reuter
</text>]