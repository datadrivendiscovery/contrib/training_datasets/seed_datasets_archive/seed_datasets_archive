[<text>
<title>chrysler canada imports thai-made mitsubishi cars</title>
<dateline>    bangkok, april 7 - </dateline>&lt;chrysler canada ltd&gt; said it signed an
agreement to import 100,000 mitsubishi small cars from &lt;mmc
sittipol co ltd&gt; of thailand between 1988 and 1993.
    chrysler canada, a subsidiary of chrysler motors corp &lt;c&gt;
of the u.s., will market the two and four-door cars in canada
under the dodge-colt brand.
    the cars will be thailand's first car exports.
    mmc sittipol is a thai-japanese joint venture in which
mitsubishi motors corp &lt;mimt.t&gt; of japan holds an estimated 47
pct stake. its thai partners are &lt;sittipol motor co&gt; and
&lt;united development motor industries co ltd&gt;.
    officials of mmc sittipol said chrysler canada currently
buys the same dodge-colt models from japan where the strong yen
has sharply increased their production costs.
    they said chrysler will from next year import the same cars
from japan and thailand which has a lower labour cost
advantage.
    the officials said mitsubishi's decision to move its
production to thailand also stemmed from the fact that japan is
facing auto export quota restrictions to the canadian market.
 reuter
</text>]