[<text>
<title>vms strategic land trust &lt;vlans&gt; sets payout</title>
<dateline>    chicago, april 1 - </dateline>vms strategic land trust declared an
initial dividend of 30 cts a share payable may 15 to
shareholders of record april 20.
    the dividend represents a 12 pct annual return based on the
company's original offering price in december of 10 dlrs a
share. the return is guaranteed through december 31, 1988, the
company said.
    the trust invests in short term junior preconstruction
mortgage loans and has total principal amount of investments of
approximately 105.7 mln dlrs.
 reuter
</text>]