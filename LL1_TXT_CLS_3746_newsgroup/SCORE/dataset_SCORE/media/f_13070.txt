[<text>
<title>u.s. grain carloadings fall in week</title>
<dateline>    washington, april 3 - </dateline>u.s. grain carloadings totaled 25,744
cars in the week ended march 28, down 4.3 pct from the previous
week but 41.6 pct above the corresponding week a year ago, the
association of american railroads reported.
    grain mill product loadings in the week totalled 10,920
cars, up 0.1 pct from the previous week and 12.7 pct above the
same week a year earlier, the association said.
 reuter
</text>]