[<text>
<title>york international &lt;yrk&gt; to make acquisitions</title>
<dateline>    york, pa., march 30 - </dateline>york international corp said it has
agreed to acquire frick co and frigid coil/frick inc for
undisclosed terms.
    the company said frick makes refrigeration equipment and
compressors and frigid coil also makes refrigeration equipment.
 together the two had revenues of about 50 mln dlrs in 1986.
    the company said it hopes to complete the acquisition in
may, subject to its review of frick and frigid and regulatory
approvals.
 reuter
</text>]