[<text>
<title>medical research fund says assets rose</title>
<dateline>    palm beach, fla., march 17 - </dateline>medical research investment
fund inc said net assets as of february 28 were 2,359,722 dlrs,
up 189.5 pct from a year earlier.
    it said net asset value per share rose to 14.64 dlrs from
12.14 dlrs, based on 161,187 shares outstanding compared with
67,136 shares.                
 reuter
</text>]