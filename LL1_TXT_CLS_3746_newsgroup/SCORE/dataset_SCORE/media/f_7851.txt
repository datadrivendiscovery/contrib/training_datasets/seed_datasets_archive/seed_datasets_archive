[<text>
<title>southeast banking corp &lt;stb&gt; declares qtly div</title>
<dateline>    miami, fla., march 20 -
    </dateline>qtly div 22 cts vs 22 cts prior
    pay april 10
    record march 30
 reuter
</text>]