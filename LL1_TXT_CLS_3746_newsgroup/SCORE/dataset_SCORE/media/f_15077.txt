[<text>
<title>diebold inc &lt;dbd&gt; declares dividend</title>
<dateline>    canton, ohio, april  8 -
    </dateline>qtly div 30 cts vs 30 cts prior
    pay june 8
    record may 18
 reuter
</text>]