[<text>
<title>plessey &lt;plyl.l&gt; wins 100 mln dlr u.s. phone order</title>
<dateline>    london, april 5 - </dateline>the plessey co plc announced it had won a
100 mln dlr digital telephone exchange order in the united
states, which it said was the first major u.s. telephone
exchange order not to go to established u.s. suppliers.
    &lt;south central bell telephone co&gt;, a unit of bellsouth corp
&lt;bls.n&gt;, placed the order with plessey's u.s. subsidiary
&lt;stromberg-carlson&gt;.
    most of the equipment for the 100 exchanges will be built
in stromberg-carlson's florida plant over the next three years.
    "it is the most important thing that has happened in the
plessey telecommunications business," said plessey
telecommunications managing director david dey.
    the british firm failed to win a four billion dlr deal to
supply an advanced communications system to the u.s. army in
november 1985 despite personal lobbying by prime minister
margaret thatcher. a rival french system won the contract.
 reuter
</text>]