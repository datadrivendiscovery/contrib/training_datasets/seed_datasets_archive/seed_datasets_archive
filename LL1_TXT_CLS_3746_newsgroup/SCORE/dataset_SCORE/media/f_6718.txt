[<text>
<title>pacific eastern adds three trucking operations</title>
<dateline>    houston, march 18 - </dateline>&lt;pacific eastern corp&gt; said it added
three trucking operations to its affiliate program totaling
about 6.7 mln dlrs in annual revenue.
    the company said the addition brings to 10 to 12 mln dlrs
its annualized revenues, and predicts that figure will be more
than 25 mln dlrs by the end of the year.
    pacific eastern said its sales were about six mln dls in
1986.
 reuter
</text>]