[<text>
<title>metrobanc &lt;mtbc&gt; shareholders approve merger</title>
<dateline>    grand rapids, mich., march 25 - </dateline>metrobanc, a federal
savings bank, said its shareholders approved the previously
announced merger with comerica inc &lt;cmca&gt;, a bank holding
company.
    metrobanc said the merger is still subject to regulatory
approval.
 reuter
</text>]