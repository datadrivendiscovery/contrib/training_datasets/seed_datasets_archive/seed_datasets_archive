[<text>
<title>ccr video &lt;cccr.o&gt; in talks on being acquired</title>
<dateline>    los angeles, oct 19 - </dateline>ccr video corp said it has received
an offer to enter into negotiations for &lt;intercep investment
corp&gt; to acquire a controlling interest through a tender offer.
    the company said "the negotiations would determine the
terms under which the ccr board of directors could support the
intercep tender offer."
    it gave no further details.
 reuter
</text>]