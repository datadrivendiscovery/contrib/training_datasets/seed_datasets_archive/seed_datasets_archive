[<text>
<title>holly sugar corp &lt;hly&gt; sets regular dividend</title>
<dateline>    colorado springs, colo., march 6 - 
    </dateline>qtly div 25 cts vs 25 cts prior
    pay march 31
    record march 18
 reuter
</text>]