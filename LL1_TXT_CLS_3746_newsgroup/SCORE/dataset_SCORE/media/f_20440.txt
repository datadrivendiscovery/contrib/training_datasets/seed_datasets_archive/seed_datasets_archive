[<text>
<title>contrans corp &lt;css.to&gt; 4th qtr august 31 net</title>
<dateline>    toronto, oct 20 - 
    </dateline>shr 52 cts vs 47 cts
    net 1,935,000 vs 1,495,000
    revs 52.7 mln vs 43.1 mln
    year
    shr 83 cts vs 1.01 dlrs
    net 3,775,000 vs 3,221,000
    revs 172.7 mln vs 105.9 mln
    note: share figures for year are after payment of preferred
share dividend and include unspecified extraordinary items.
 reuter
</text>]