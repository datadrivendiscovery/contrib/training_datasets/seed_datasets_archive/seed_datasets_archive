[<text>
<title>hcc industries &lt;hcci&gt; quarterly dividend</title>
<dateline>    encino, calif., march 4 -
    </dateline>qtly div three cts vs three cts
    pay march 27
    record march 16
 reuter
</text>]