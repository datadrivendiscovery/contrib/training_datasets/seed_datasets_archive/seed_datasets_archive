[<text>
<title>woodward's ltd &lt;wdsa.to&gt; 1st qtr may 2 loss</title>
<dateline>    vancouver, british columbia, june 2 -
    </dateline>shr loss 32 cts vs loss 37 cts
    net loss 5,374,000 vs loss 6,159,000
    revs 241.3 mln vs 253.2 mln
 reuter
</text>]