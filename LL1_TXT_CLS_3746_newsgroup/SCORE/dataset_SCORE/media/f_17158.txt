[<text>
<title>boeing &lt;ba&gt; gets orders totaling 543 mln dlrs</title>
<dateline>    seattle, april 24 - </dateline>boeing co said it confirmed orders
totaling 543 mln dlrs for 14 jet aircraft from four customers.
    the company said delta air lines &lt;dal&gt; has increased its
orders for the 767-300 jetliner by six, bringing its total
commitment for the longer-fuselage plane to 15, with a total
value of more than 300 mln dlrs.
   
    boeing said &lt;lufthansa german airlines&gt; will take delivery
of five more 737-300 jetliners worth about 130 mln dlrs,
bringing its orders for the twinjets to 15.
    &lt;quantas&gt; of australia has ordered its first 767-300 with
extended range capability for about 75 mln dlrs, boeing said.
    boeing said &lt;air france&gt; has placed firm orders for two
additional 737-200 twinjets valued at 38 mln dlrs.
 reuter
</text>]