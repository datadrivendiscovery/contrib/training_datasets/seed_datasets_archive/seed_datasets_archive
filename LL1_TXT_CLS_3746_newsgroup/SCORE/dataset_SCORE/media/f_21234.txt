[<text>
<title>interstate securities inc &lt;is&gt; 4th qtr sept 30</title>
<dateline>    charlotte, n.c., oct 19 -
    </dateline>shr 11 cts vs 22 cts
    qtly div 10 cts vs 10 cts prior
    net 548,254 vs 1,138,978
    revs 28 mln vs 31.1 mln
    year
    shr 50 cts vs 1.34 dlrs
    net 2,527,846 vs 6,822,293
    revs 111.7 mln vs 118.9 mln
    note: qtly div payable december 4 to shareholders of record
november 13.
 reuter
</text>]