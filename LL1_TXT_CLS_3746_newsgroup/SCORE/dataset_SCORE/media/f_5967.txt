[<text>
<title>sterling inc &lt;strl&gt; 4th qtr jan 31 net</title>
<dateline>    akron, ohio, march 17 -
    </dateline>shr 1.27 dlrs vs not given
    net 5,097,000 vs 3,164,000
    sales 48.1 mln vs 31.7 mln
    year
    shr 1.42 dlrs vs not given
    net 5,194,000 vs 3,457,000
    sales 100.4 mln vs 70.1 mln
    note: company went public in may 1986.
 reuter
</text>]