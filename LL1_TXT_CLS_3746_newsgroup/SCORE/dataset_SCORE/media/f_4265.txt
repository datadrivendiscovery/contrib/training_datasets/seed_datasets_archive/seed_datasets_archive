[<text>
<title>vitronics &lt;vitx&gt; plans british plant</title>
<dateline>    newmarket, n.h., march 12 - </dateline>vitronics corp said it plans to
establish a plant in plymouth, england, to serve european
customers.
 reuter
</text>]