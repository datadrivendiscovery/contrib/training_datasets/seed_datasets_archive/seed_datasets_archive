[<text>
<title>mitsui buys five pct stake in u.s. chip maker</title>
<dateline>    tokyo, june 19 - </dateline>mitsui and co ltd &lt;mits.t&gt; paid 1.5 mln
dlrs in early may for a five pct stake in &lt;zoran corp&gt;, a
california-based maker of large scale integrated circuits (lsi)
with computer graphic, communications and medical applications,
a mitsui spokesman told reuters.
    he said the two firms will form a marketing company in
japan as early as next year, although details of the joint
venture are not yet fixed. mitsui expects last year's 10
billion yen japanese lsi market to grow quickly.
    zoran was founded in 1981 and now has about 100 employees,
he said.
 reuter
</text>]