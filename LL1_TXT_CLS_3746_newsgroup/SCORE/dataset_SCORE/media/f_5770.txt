[<text>
<title>mitsubishi bank &lt;mitb.t&gt; plans 40 mln n.z. dlr cd</title>
<dateline>    hong kong, march 17 - </dateline>mitsubishi bank ltd's hong kong
branch is planning a 40 mln new zealand dlr certificate of
deposit, cd, issue, banking sources said.
    the three-year cds are denominated in 100,000 dlr units and
will carry interest at 37.5 basis points below the three-month
new zealand bank bill rate.
    payment date is expected to be around april 1.
    anz securities asia ltd is lead manager for the issue.
 reuter
</text>]