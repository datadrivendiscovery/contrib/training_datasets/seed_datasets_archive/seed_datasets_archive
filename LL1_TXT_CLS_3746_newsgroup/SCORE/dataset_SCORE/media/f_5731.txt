[<text>
<title>astro-med inc &lt;alot&gt; 4th qtr net</title>
<dateline>    west warwick, r.i., march 16 -
    </dateline>shr 10 cts vs nine cts
    net 217,000 vs 192,000
    revs 3,325,000 vs 2,506,000
    year
    shr 31 cts vs 43 cts
    net 660,000 vs 932,000
    revs 12.2 mln vs 10.7 mln
 reuter
</text>]