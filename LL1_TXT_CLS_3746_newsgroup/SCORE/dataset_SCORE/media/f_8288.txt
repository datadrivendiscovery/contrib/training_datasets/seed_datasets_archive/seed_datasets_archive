[<text>
<title>patrick petroleum &lt;ppc&gt; to buy bayou</title>
<dateline>    jackson, mich., march 23 - </dateline>patrick petroleum co said it
signed a definitive agreement to buy bayou resources inc.
    as previously announced, the transaction is valued at about
8.8 mln dlrs, including 2.8 mln dlrs in debt.
    under the agreement, patrick will pay six dlrs per share
for each bayou share, with additional value being given for
bayou's preferred and options. bayou has 827,000 shares out.
    depending upon the results of the re-evaluation of a
significant bayou well as of jan. 1, 1988, bayou stockholders
may receive up to an additional two mln dlrs in stock and cash,
which has not been included in the 8.8 mln dlrs.
 reuter
</text>]