[<text>
<title>feds okay western carolina savings conversion</title>
<dateline>    valdese, n.c., april 1 - </dateline>western carolina savings and loan
association said the federal home loan bank board approved the
completion of its conversion to a state chartered stock savings
and loan from a state chartered mutual bank.
    carolina said 575,000 shares were subscribed for at 10 dlrs
per share during its initial offering.
    it said trading will begin in its stock april eight on
nasdaq under the sumbol wcar.
 reuter
</text>]