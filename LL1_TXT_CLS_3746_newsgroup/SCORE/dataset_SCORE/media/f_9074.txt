[<text>
<title>h.f. ahmanson and co &lt;ahm&gt; qtly dividend</title>
<dateline>    los angeles, march 24 -
    </dateline>shr 22 cts vs 22 cts prior qtr
    pay june one
    record may 12
 reuter
</text>]