[<text>
<title>del laboratories inc &lt;dli&gt; 4th qtr net</title>
<dateline>    farmingdale, n.y., march 6 -
    </dateline>shr 16 cts vs 55 cts
    net 232,000 vs 814,000
    revs 22.4 mln vs 22 mln
    year
    shr 2.07 dlrs vs 2.43 dlrs
    net 3,108,000 vs 3,670,000
    revs 106.7 mln vs 101.1 mln
    note: per share figures adjusted to reflect four-for-three
stock split paid march 26, 1986.
 reuter
</text>]