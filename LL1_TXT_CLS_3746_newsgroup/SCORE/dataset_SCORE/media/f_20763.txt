[<text>
<title>canada panel advises cutting u.s. corn duty</title>
<dateline>    ottawa, oct 20 - </dateline>the canadian import tribunal said the
countervailing duty on u.s. corn imports should be cut to 30
canadian cts a bushel from 1.10 dlrs a bushel.
    in a report to the canadian finance department, the
tribunal said the duty is hurting canadian farmers and food
processors. the duty was imposed last year after the revenue
department found u.s. corn production was subsidized.
    it is now up to the government to decide whether to change
the duty.
 reuter
</text>]