[<text>
<title>reynolds and reynolds &lt;reyna&gt; to fight suit</title>
<dateline>    dayton, ohio, april 1 - </dateline>the reynolds and reynolds co said
it will fight a suit filed against it by &lt;advanced voice
technologies&gt; alleging misappropriation of trade secrets.
    the company reiterated its denial of the charges, stating
there was no merit for the suit. the company said it will file
a response to the suit with federal court in detroit by april
7.
 reuter
</text>]