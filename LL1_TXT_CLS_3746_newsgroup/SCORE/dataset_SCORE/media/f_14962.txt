[<text>
<title>rubbermaid inc &lt;rbd&gt; 1st qtr net</title>
<dateline>    wooster, ohio, april 8 -
    </dateline>shr 28 cts vs 22 cts
    net 20.6 mln vs 16.1 mln
    sales 238.0 mln vs 188.8 mln
 reuter
</text>]