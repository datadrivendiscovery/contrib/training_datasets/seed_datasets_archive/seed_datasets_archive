[<text>
<title>&lt;asta group inc&gt; unit in loan purchase</title>
<dateline>    yonkers, n.y., march 25 - </dateline>asta group inc said its 50 pct
owned liberty service corp affiliate has purchased about 50 mln
dlrs face value of credit card installment receivables from a
major financial institution it did not name for a significant
discount from face value.
    it said the portfolio consists mostly of charged off loans.
    the company also said it expects to realize a profit of
about 300,000 dlrs on its 25 pct interest in the briarcliff
manor condominium project in new york, with about 140,000 dlrs
of the profit being reflected in the year ending september 30.
 reuter
</text>]