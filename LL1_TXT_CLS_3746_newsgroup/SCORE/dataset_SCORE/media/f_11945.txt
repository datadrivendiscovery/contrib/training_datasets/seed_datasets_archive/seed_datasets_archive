[<text>
<title>asarco lowers copper price 1.50 cts to 67 cts</title>
<dateline>    new york, april 1 - </dateline>asarco inc said it is decreasing its
domestic delivered copper cathode price by 1.50 cents to 67.0
cents a lb, effective immediately.
 reuter
</text>]