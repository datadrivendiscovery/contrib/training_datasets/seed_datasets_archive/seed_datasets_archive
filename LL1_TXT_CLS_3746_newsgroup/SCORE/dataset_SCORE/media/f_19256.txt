[<text>
<title>boeing &lt;ba&gt; merger waiting period expires</title>
<dateline>    seattle, june 19 - </dateline>boeing co said the hart-scott-rodino
waiting period required in connection with its pending tender
offer for argosystems inc &lt;argi.o&gt; expired at midnight june 18.
    boeing began its 37 dlr per share cash tender offer for the
defense electronics firm on june two.
 reuter
</text>]