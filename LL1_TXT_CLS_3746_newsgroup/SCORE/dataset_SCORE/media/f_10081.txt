[<text>
<title>fed's heller says dollar's level is appropriate</title>
<dateline>    new york, march 26 - </dateline>federal reserve board governor robert
heller said that the dollar's current level is appropriate but
declined to comment on widespread market reports of concerted
central bank intervention this week.
    "the dollar is stable again... the current level is the
appropriate level," heller told reporters after a speech to a
meeting of financial services analysts.
    he said last month's six-nation currency accord in paris
did not include target ranges for the dollar in an "academic
way."
    heller also said that it was too early to determine whether
the parties to the six-nation accord were taking appropriate
steps to carry out the longer-term economic adjustments agreed
to in paris.
    "clearly, they've not been implemented yet... no one
expects implementation within a week or two," he said.
    earlier today, u.s. treasury assistant secretary told a
senate banking subcommittee that he did not believe that west
germany and japan have yet carried out their international
responsibilities.
 reuter
</text>]