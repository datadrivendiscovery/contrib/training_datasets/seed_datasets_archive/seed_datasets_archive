[<text>
<title>world bank to send economic mission to brazil</title>
<dateline>    washington, april 9 - </dateline>world bank president barber conable
said his agency plans to send a mission to brazil next week to
explore economic conditions.
    speaking to reporters at the international monetary fund
and world bank annual meeting, conable said that he felt
positive and optimistic that something between brazil and its
creditors will be worked out.
    conable met last night with brazilian officials and told
reporters, "we agreed to send a mission to brazil next week to
explore further the state of progress and to get
clarifications."
 reuter
</text>]