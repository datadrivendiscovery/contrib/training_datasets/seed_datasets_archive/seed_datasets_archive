[<text>
<title>blasius industries inc &lt;blas&gt; 3rd qtr loss</title>
<dateline>    erie, pa., march 26 - </dateline>qtr ended feb 28
    oper shr loss one ct vs profit 12 cts
    oper net profit 3,000 vs profit 218,000
    revs 12.0 mln vs 10.6 mln
    avg shrs 2,421,000 vs 1,602,000
    nine mths
    oper shr profit 28 cts vs profit 24 cts
    oper net profit 639,000 vs profit 500,000
    revs 34.6 mln vs 31.2 mln
    avg shrs 1,928,000 vs 1,620,000
    note: oper excludes tax credits of 180,000 and 415,000 for
year-ago qtr and nine mths.
    oper includes writeoff related to subordinated note
exchange of 185,000 for current qtr and nine mths.
 reuter
</text>]