[<text>
<title>att &lt;t&gt; launches systems for small businesses</title>
<dateline>    new york, april 21 - </dateline>american telephone and telegraph co
introduced two communications systems, spirit and merlin, and
other products, in a bid to strengthen its position with small
businesses, the company said.
    the spirit system, with a basic price tag of 1,500 dlrs,
can handle up to six lines and 16 telephones and a more
advanced line which can handle up to 24 lines and 48
tlelphones.
    att said the merlin line, which starts at 2,500 dlrs, can
handle up to 32 lines and 72 telephones.
    att said the new products will eventually replace the
current merlin product family. some of the systems will be
available in may and others in the third quarter.
    att also introduced software enhancements for the system
25, for business that require pbx voice and data communications
and need up to 150 phones. these and other enhancements will be
available in the third quarter, the company said.
 reuter
</text>]