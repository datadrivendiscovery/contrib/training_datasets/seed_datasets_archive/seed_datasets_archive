[<text>
<title>ferranti unit lands multi mln dlr conrtact</title>
<dateline>    london, march 24 - </dateline>ferranti plc &lt;fnti.l&gt; said its edinburgh
based electro-optics department of ferranti defence systems ltd
has secured a multi mln dlr contract to supply laser
rangefinders for the canadian forces low level air defense
system.
    the first production order for 38 of the type 629g lasers
has been placed by &lt;martin marietta&gt; of the u.s., a
subcontractor to &lt;orelikon aerospace incorporated&gt; of canada,
the prime contractor for the system. some 300 further orders
are anticipated as the programme proceeds.
    ferranti shares were 2p firmer at 134 after the news.
 reuter
</text>]