[<text>
<title>sec acts on exchange-listed options trading</title>
<dateline>    washington, june 18 - </dateline>the securities and exchange
commission began proceedings in which it will consider allowing
options trading on exchange-listed securities on more than one
options market.
    in a three-nil decision, the sec voted to seek public
comment on a proposed rule removing restrictions that prevent
the options from being traded on more than one market.
    exclusive trading rights in options on exchange-listed
stocks are now determined by a lottery system among options
exchanges.
    the sec also voted to ask the new york, american, chicago
board options, pacific and philadelphia exchanges to
voluntarily postpone an options allocation lottery scheduled
for june 22 while the matter is under consideration.
    richard ketchum, head of the sec's division of market
regulation, said the exchanges are not required to adhere to
the request.
 reuter
</text>]