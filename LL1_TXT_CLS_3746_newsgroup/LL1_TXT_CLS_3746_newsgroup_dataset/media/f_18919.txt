[<text>
<title>bic corp &lt;bic&gt; raises quarterly</title>
<dateline>    milford, conn., june 18 -
    </dateline>qtly div 18 cts vs 15 cts prior
    pay july 30
    record july 10
 reuter
</text>]