[<text>
<title>united merchants &lt;umm&gt; to buy its own stock</title>
<dateline>    new york, march 2 - </dateline>united merchants and manufactuerers inc
said its board has authorized the repurchase of up to one mln
shares of the company's common stock.
    the company now has about 9.1 mln shares outstanding.
    it said the stock will be acquired from time to time on the
open market, depending on market conditions and other factors.
the number of shares purchased and the timing of the purchases
are also subject to restrictions under certain of the company's
debt instruments, it added.
 reuter
</text>]