[<text>
<title>atlantic city electric co &lt;ate&gt; sets payout</title>
<dateline>    pleasantville, n.j., march 5 -
    </dateline>qtly div 65-1/2 cts vs 65-1/2 cts prior
    pay april 15
    record march 19
 reuter
</text>]