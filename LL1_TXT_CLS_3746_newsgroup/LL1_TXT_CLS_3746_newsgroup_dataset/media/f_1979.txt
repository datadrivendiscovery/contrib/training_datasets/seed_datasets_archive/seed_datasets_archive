[<text>
<title>gm &lt;gm&gt;  laying off 5,500 at two plants</title>
<dateline>    detroit, march 5 - </dateline>general motors corp said it ordered
temporary layoffs of 5,500 hourly workers to cut production and
thereby reduce inventories of cars built at two plants later
this month.
    a spokesman said 2,000 workers would be laid off one week
beginning march 9 at gm's detroit-hamtramck luxury car plant.
    another 3,500 will be laid off a week effective march 23 at
gm's lansing, mich, plant which builds the company's "n-body"
compact cars.
 reuter
</text>]