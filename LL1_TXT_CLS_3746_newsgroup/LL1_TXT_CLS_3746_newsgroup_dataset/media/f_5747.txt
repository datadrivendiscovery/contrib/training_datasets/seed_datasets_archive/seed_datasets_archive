[<text>
<title>fnma &lt;fnm&gt; will not buy home-equity loans</title>
<dateline>    washington, march 16 - </dateline>the federal national mortgage
association said it will not purchase home-equity loans as part
of its mortgage-purchase and swap activities in the secondary
market.
    "we don't believe that our provision of liquidity is needed
in this segment of the market," david maxwell, fannie mae's
chairman, said.
 reuter
</text>]