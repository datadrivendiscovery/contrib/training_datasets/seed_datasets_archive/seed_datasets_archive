[<text>
<title>phh group &lt;phh&gt; regular qtly dividend</title>
<dateline>    hunt valley, md., march 16 -
    </dateline>qtly div 26 cts vs 26 cts prior
    pay april 30
    record april 10
 reuter
</text>]