[<text>
<title>american realty &lt;arb&gt; sets record date for offer</title>
<dateline>    dallas, april 2 - </dateline>american realty trust said its board has
set april 3 as the record date for its previously announced
rights offering, and the rights will be issued on april 6 and
expire may 22.
    shareholders will be able to subscribe for 1.25 shares for
each share held, at a price of 3.75 dlrs per share.
 reuter
</text>]