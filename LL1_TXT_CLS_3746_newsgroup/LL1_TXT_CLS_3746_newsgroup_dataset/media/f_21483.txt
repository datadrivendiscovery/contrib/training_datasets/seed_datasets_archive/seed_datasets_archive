[<text>
<title>quantech &lt;qant.o&gt; gets nasdaq exception</title>
<dateline>    levittown, n.y., oct 19 - </dateline>quantech electronics corp said
its common stock will continue to be quoted on the nasdaq
system due to an exception from national association of
securities dealers capital and surplus requirements, which it
failed to meet as of august 31.
    the company said it believes it can meet conditions imposed
by the nasd for the exception, but there can be no assurance
that it will do so.
 reuter
</text>]