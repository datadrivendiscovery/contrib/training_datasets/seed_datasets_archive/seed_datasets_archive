[<text>
<title>corrected - ibm &lt;ibm&gt; newly named secretary dies</title>
<dateline>    armonk, n.y., april 3 - </dateline>international business machines
corp said recently elected secretary, john manningham, and his
wife, patricia, died early this morning in a fire at their
ridgefield, conn home.
    manningham, 53, began his ibm career in 1959 as a marketing
representative. his election would have been effective july 1.
    tom irwin is the current secretary.
    ibm chairman john akers said "this is a great loss to ibm
and to the manningham's family, friends and community.
(corrects spelling of town in line five)
 reuter
</text>]