[<text>
<title>lebanese pound falls sharply against dollar</title>
<dateline>    beirut, april 9 - </dateline>the lebanese pound fell sharply against
the u.s. dollar again today with dealers attributing the
decline to continued political uncertainty.
    the pound closed at 118.25/118.75 against the dollar
compared to yesterday's close of 115.60/115.80.
    "political deadlock is reflected in the pound's position.
there was more demand and less on offer in the market," one
dealer told reuters.
    the pound, which was at 18 to the dollar in january, 1986,
has lost more than 30 pct of its international value over the
past three months.
 reuter
</text>]