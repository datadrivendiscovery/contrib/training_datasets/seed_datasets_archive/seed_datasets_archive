[<text>
<title>pop radio &lt;popx&gt; gets rite aid &lt;rad&gt;</title>
<dateline>    new york, march 19 - </dateline>pop radio corp said it has signed a
seven-year agreement to provide rite aid corp with in-store
customized disc jocky-hosted radio programs, resulting in an
increase of more than 50 pct in the total number of stores pop
now has under contract.
    value was not disclosed.
 reuter
</text>]