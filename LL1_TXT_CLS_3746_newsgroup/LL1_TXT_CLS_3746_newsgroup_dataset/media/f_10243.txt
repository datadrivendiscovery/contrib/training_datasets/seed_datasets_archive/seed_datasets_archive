[<text>
<title>tektronix inc &lt;tek&gt; quarterly dividend</title>
<dateline>    beaverton, wash., march 26 -
    </dateline>qtly div 15 cts vs 15 cts
    pay may 4
    record april 10
    note: previous dividend restated to reflect january 26
two-for-one stock split.
 reuter
</text>]