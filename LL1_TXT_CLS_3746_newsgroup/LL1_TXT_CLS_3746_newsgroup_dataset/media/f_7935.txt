[<text>
<title>integrated generics &lt;ign&gt; may sell 10 pct of unit</title>
<dateline>    bellport, n.y., march 20 - </dateline>integrated generics inc said it
is discussing with an unnamed pharmaceutical distributor the
sale of 10 pct of its a.n.d.a. development corp subsidiary for
200,000 dlrs.
    integrated said its subsidiary, biopharmaceutics, is
negotiating with the same unnamed distributor to sell it five
prescription drugs.
    the company said it can release no other details at this
time.
 reuter
</text>]