[<text>
<title>american federal savings &lt;afsl.o&gt; sees pressure</title>
<dateline>    denver, june 19 - </dateline>american federal savings and loans
association of colorado said its earnings will continue to be
under pressure this year due to the weakness of the colorado
economy.
    the company, which today declared a regular quarterly
dividend, said it was also under pressure from the level of
non-earning assets it holds.
    the company said this pressure will be offset to a certain
extent by increased revenues from out-of-state operations and
its strong capital position.
 reuter
</text>]