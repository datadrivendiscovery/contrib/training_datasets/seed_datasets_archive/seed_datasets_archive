[<text>
<title>xerox credit &lt;xrx&gt; notes yield 8.061 pct</title>
<dateline>    new york, march 4 - </dateline>a 100 mln dlr offering of xerox credit
corp 12-year notes was given an eight pct coupon and priced at
99.534 to yield 8.061 pct, salomon brothers inc said as lead
manager.
    the securities of the xerox corp unit are not callable for
seven years. the yield reflected a premium of 91 basis points
over that of comparable u.s. treasury issues.
    moody's investors service inc rates the notes at a-2,
compared with a-plus from standard and poor's corp.
 reuter
</text>]