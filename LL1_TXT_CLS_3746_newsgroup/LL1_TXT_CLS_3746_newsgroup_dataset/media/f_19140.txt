[<text>
<title>xiox &lt;xiox.o&gt; reincorporates in delaware</title>
<dateline>    burlingame, calif., june 19 - </dateline>xiox corp said it has changed
its state of incorporation to delaware from california.
 reuter
</text>]