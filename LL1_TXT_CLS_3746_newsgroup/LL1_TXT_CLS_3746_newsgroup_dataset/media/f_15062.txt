[<text>
<title>data translation inc &lt;datx&gt; 1st qtr feb 28 net</title>
<dateline>    marlboro, mass., april 8 -
    </dateline>shr 18 cts vs 13 cts
    net 575,000 vs 379,000
    sales 6,625,000 vs 4,537,000
    avg shrs 3,173,000 vs 2,977,000
 reuter
</text>]