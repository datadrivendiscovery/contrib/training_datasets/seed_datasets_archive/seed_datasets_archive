[<text>
<title>quaker chemical &lt;qchm&gt; to repurchase shares</title>
<dateline>    conshohocken, pa., march 25 - </dateline>quaker chemical corp said its
board has authorized the repurchase from time to time of up to
300,000 common shares.
    quaker now has about 6.6 mln shares outstanding.
 reuter
</text>]