[<text>
<title>ross stores inc &lt;rost&gt; 4th qtr jan 31 loss</title>
<dateline>    newark, calif., march 25 -
    </dateline>shr loss 1.30 dlrs vs profit 29 cts
    net loss 33.4 mln vs profit 7,386,000
    sales 168.2 mln vs 128.4 mln
    year
    shr loss 1.61 dlrs vs profit 30 cts
    net loss 41.4 mln vs profit 7,055,000
    sales 527.5 mln vs 366.7 mln
    note: latest year net both periods includes 39.4 mln dlr
provision for closing 25 underperforming stores.
 reuter
</text>]