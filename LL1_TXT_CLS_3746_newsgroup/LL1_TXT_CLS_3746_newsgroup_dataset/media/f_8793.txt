[<text>
<title>hughes supply inc &lt;hug&gt; 4th qtr net</title>
<dateline>    orlando, fla., march 24 -
    </dateline>shr 52 cts vs 49 cts
    shr diluted 1.95 dlrs vs 1.99 dlrs
    net 1,751,609 vs 1,622,503
    sales 85.9 mln vs 85.1 mln
    year
    shr 2.10 dlrs vs 1.99 dlrs
    shr diluted 1.95 dlrs vs 1.99 dlrs
    net 6,822,493 vs 6,601,717
    sales 347.8 mln vs 324.6 mln
 reuter
</text>]