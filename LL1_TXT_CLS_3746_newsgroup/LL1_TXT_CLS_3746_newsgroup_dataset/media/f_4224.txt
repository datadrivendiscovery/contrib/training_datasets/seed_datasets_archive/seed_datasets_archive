[<text>
<title>sonex research &lt;sonx&gt; sells stock privately</title>
<dateline>    annapolis, md., march 12 - </dateline>sonex research inc said it has
completed a private sale of common shares and warrants for two
mln dlrs.
    it said if all warrants are exercised it will receive
another 4,600,000 dlrs.
    proceeds will be used for working capital, it said.
 reuter
</text>]