[<text>
<title>cineplex odeon to offer stock in the u.s.</title>
<dateline>    toronto, march 26 - </dateline>&lt;cineplex odeon corp&gt; said it proposed
to sell 3,650,000 shares of common stock in the united states
in late april, its first u.s. public stock offering.
    in a registration statement filed with the securities and
exchange commission, cineplex said it plans to offer the shares
through an underwriting group managed by merrill lynch capital
markets and allen and co.
    cineplex also said that mca inc &lt;mca&gt;, which holds a 50 pct
stake in company, will buy one mln subordinated restricted
voting shares. these shares, will be bought at the closing of
the public offering with existing purchase rights, it said.
  
 reuter
</text>]