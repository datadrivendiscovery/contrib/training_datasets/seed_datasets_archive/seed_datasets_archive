[<text>
<title>pittsburgh and west virginia railroad &lt;pw&gt; net</title>
<dateline>    pittsburgh, april 1 - </dateline>4th qtr
    shr 14 cts vs 14 cts
    net 210,000 vs 211,000
    revs 230,000 vs 229,000
    year
    shr 56 cts vs 56 cts
    net 838,000 vs 841,000
    revs 919,000 vs 919,000
 reuter
</text>]