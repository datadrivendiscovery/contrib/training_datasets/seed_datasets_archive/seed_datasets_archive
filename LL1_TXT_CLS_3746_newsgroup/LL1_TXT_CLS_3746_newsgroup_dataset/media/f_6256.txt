[<text>
<title>mor-flo &lt;morf&gt; loses infringement appeal</title>
<dateline>    cleveland, march 17 - </dateline>mo-flo industries inc said the
federal appeals court has affairmed a trial court ruling that
mor-flo infringed on patents held by &lt;state industries inc&gt; for
insulating gas water heaters with polyurethane.
    mor-flo said it intends to seek a rehearing of the
decision. if the rehearing is denied, further proceedings
before the trial court will be held to determine damages.
    the company said it made provision for a potential award
and related litigation expenses by taking an after-tax charge
of 384,000 dlrs, or 15 cts a share, against 1986 earnings.
 reuter
</text>]