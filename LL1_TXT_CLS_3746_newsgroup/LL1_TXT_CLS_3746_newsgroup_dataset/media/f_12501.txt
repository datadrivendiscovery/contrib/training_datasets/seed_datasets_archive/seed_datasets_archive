[<text>
<title>bank of france leaves intervention rate unchanged</title>
<dateline>    paris, april 2 - </dateline>the bank of france said it left its
intervention rate unchanged at 7-3/4 pct when it injected funds
to the market against first category paper in todays money
market intervention tender.
    money market dealers had earlier expressed mixed views on a
possible quarter point cut.
    the rate was last adjusted on march 9, when it was reduced
to 7-3/4 pct from the eight pct rate set in january.
 reuter
</text>]