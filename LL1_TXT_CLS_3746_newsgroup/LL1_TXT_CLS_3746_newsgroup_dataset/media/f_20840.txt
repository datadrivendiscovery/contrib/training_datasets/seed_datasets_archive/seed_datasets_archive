[<text>
<title>southmark &lt;sm&gt; to purchase &lt;national self&gt;</title>
<dateline>    west palm beach, fla., oct 20 - </dateline>national self storage said
has sold nine storage facilities to southmark corp for 37.1 mln
dlrs.
    national self, a developer of storage space for business
records, said the purchase includes nine storage facilties.
national self also said it and southmark plan a two-year 100
facility expansion program in south florida and across the
country.
 reuter
</text>]