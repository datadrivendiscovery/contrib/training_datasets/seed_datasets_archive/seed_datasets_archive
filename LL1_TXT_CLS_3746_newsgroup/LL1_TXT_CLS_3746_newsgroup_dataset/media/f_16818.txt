[<text>
<title>dynamics research corp &lt;drco.o&gt; 1st qtr march 21</title>
<dateline>    wilmington, mass., april 17 -
    </dateline>shr 17 cts vs 13 cts
    net 673,000 vs 514,000
    revs 18.4 mln vs 17.2 mln
    note: share adjusted for five-for-four stock split in
january 1987.
 reuter
</text>]