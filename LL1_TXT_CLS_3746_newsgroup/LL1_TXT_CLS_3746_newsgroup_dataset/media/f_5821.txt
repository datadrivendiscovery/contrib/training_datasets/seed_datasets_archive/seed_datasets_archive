[<text>
<title>japan line floats stock to help restore solvency</title>
<dateline>    tokyo, march 17 - </dateline>major tanker operator japan line ltd
&lt;jlit.t&gt; said it will raise seven billion yen by issuing 63.66
mln shares at 110 yen each to help reduce debt and restore
profitability within two years.
    half of the sum raised will be returned to creditor banks
which in december were asked to temporarily waive part of their
loan repayments, it said.
    the other half will be used to bolster operations hit by
the yen's steep appreciation and the global shipping slump, the
company added.
 more
</text>]