[<text>
<title>cdc life sciences &lt;lsi.to&gt; stake sold</title>
<dateline>    toronto, june 29 - </dateline>canada development corp &lt;cdc.to&gt; said it
agreed to sell its 25.2 pct interest in cdc life sciences inc
to caisse de depot et placement du quebec, the provincial
pension fund manager, and institut merieux, a french biological
laboratory company, for 169.2 mln dlrs.
    it said the caisse and institut merieux will each buy 2.75
mln common shares of the company for 30.76 dlrs a share.
    it said following the transaction the caisse will hold
about 19.3 pct of cdc life sciences. canada development said
the purchasers do not plan to acquire the remaining
publicly-held shares.
 reuter
</text>]