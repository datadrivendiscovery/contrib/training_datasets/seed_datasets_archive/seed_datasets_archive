[<text>
<title>genetics &lt;geni&gt;, wellcome name site for venture</title>
<dateline>    cambridge, mass., april 1 - </dateline>genetics institute inc and
wellcome plc, a british-based pharmaceutical company, jointly
announced the selection of a site for their joint venture,
welgen manufacturing inc, formed last september.
    the companies said they choose a 77-acre site in west
greenwich r.i. for the venture's manufacturing facility, which
will make biotechnology-based pharmaceuticals. the plant should
be completed in 1989, and will employ about 200 people.
 reuter
</text>]