[<text>
<title>kenya devalues shilling by 0.6 pct against sdr</title>
<dateline>    nairobi, april 13 - </dateline>kenya devalued the shilling by 0.6 pct
against the special drawing right (sdr) in response to the
decline of the dollar last last week, bankers said.
    the central bank of kenya set the shilling at 20.7449 to
the sdr compared with the 20.6226 rate in force since the last
devaluation on march 31.
    the kenyan shilling has lost 5.6 pct of its value against
the sdr this year in a series of devaluations designed to keep
the value of the dollar above 16 shillings.
 reuter
</text>]