[<text>
<title>fedders &lt;fjq&gt; offers preferred stock</title>
<dateline>    peapack, n.j., march 20 - </dateline>fedders corp said an offering of
1,500,000 shares of 1.75 dlr convertible exchangeable preferred
stock is under way at 25 dlrs per share through underwriters
led by bear stearns cos inc &lt;bsc&gt;.
    the preferred is convertible at any time into common stock
at 9.50 dlrs a share and is exchangeable after two years at
fedders' option for seven pct convertible subordinated
debentures due 2012.
 reuter
</text>]