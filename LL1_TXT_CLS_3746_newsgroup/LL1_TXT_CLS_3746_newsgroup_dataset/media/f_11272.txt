[<text>
<title>jewelmasters &lt;jem&gt; sees net below estimates</title>
<dateline>    new york, march 31 - </dateline>jewelmasters inc said it expects to
report net income for the year ended january 31 20 to 25 pct
below analysts' estimates of 1,750,000 dlrs or 95 cts per share.
    jewelmasters sales sales for the year just ended were about
52.5 mln dlrs. in the prior year it earned 1,650,000 dlrs on
sales of 45.1 mln dlrs.
    jewelmasters said net income for the year was hurt by
disappointing sales in december and january, a high level of
advertising spending in the fourth quarter, higher than
expected opening expenses for 34 additional units and an
adjustment to inventory associated with a shift to a more
comprehensive inventory system.
    jewelmasters said it expects to report audited results for
the fourth quarter and year in about three weeks.
 reuter
</text>]