[<text>
<title>coca-cola enterprises &lt;cce&gt; to sell debt</title>
<dateline>    new york, march 18 - </dateline>coca-cola enterprises inc said it
filed with the securities and exchange commission a
registration statement covering 500 mln dlrs of debt.
    the company said it plans to sell 250 mln dlrs of notes due
1997 and an equal amount of debentures due 2017.
    proceeds will be used to refinance outstanding
indebtedness, coca-cola enterprises said.
    the company named allen and co inc as lead manager and
merrill lynch capital markets and salomon brothers inc as
co-managers of the offerings.
 reuter
</text>]