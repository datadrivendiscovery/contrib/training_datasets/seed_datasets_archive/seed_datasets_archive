[<text>
<title>volkswagen dividend unchanged on 1986</title>
<dateline>    wolfsburg, west germany, april 9 -
    </dateline>dividend on 1986 business unchanged at 10 marks per
ordinary share. company also set dividend of 11 marks for new
preference shares, which were issued last year.
    (note: company has said profit will match 1985 level,
despite provisions of 480 mln marks connected with alleged
currency fraud. group net profit in 1985 was 596 mln marks,
parent company net was 477 mln marks. company's full name is
volkswagen ag &lt;vowg.f&gt;).
 reuter
</text>]