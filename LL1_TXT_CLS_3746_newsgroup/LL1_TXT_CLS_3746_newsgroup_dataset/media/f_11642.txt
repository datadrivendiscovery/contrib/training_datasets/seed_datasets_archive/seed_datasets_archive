[<text>
<title>lucky &lt;lks&gt; spinoff gets favorable tax ruling</title>
<dateline>    dublin, calif., march 31 - </dateline>lucky stores inc said its
planned distribution of hancock fabrics shares to lucky
stockholders will qualify as a tax-free spinoff under the
internal revenue code.
    the company also said it still must wait for completion of
a pending securities and exchange commission review of an
information statement related to the spinoff before it can
carry out the transaction.
    subject to completion of the review, it expects to make the
distribution in early may, lucky stores also said.
 reuter
</text>]