[<text>
<title>gencorp &lt;gy&gt; 1st qtr operating earnings rose</title>
<dateline>    akron, ohio, march 16 - </dateline>gencorp said its first quarter
earnings from operations rose four pct as sales increased six
pct to 650 mln dlrs from 614 mln a year earlier.
    however, the company reported net income declined to 17 mln
dlrs, or 77 cts a share, in the quarter ended february 28 from
19 mln dlrs, or 84 cts a year earlier. this year's net included
700,000 dlrs from the sale of assets while last years was
increased 3.0 mln dlrs by such sales.
    gencorp said lower operating profits for the tire and
plastics and industrial products segments were essentially
offset by higher wallcovering results.
 reuter
</text>]