[<text>
<title>coradian &lt;cdin.o&gt; stake acquired by sage</title>
<dateline>    albany, n.y., june 29 - </dateline>coradian corp said a group led by
privately held sage equities group agreed to buy a 7.6 pct
interest in coradian.
    in connection with the agreement, the company said it sold
666,667 shares at 75 cts a share.
    it said that in addition to common stock, sage equities
will receive 667,667 warrants exercisable at 1.50 dlrs a share.
 reuter
</text>]