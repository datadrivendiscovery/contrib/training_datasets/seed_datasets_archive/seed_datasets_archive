[<text>
<title>utilicorp &lt;ucu&gt; completes acquisition</title>
<dateline>    kansas city, march 2 - </dateline>utilicorp united inc said it
completed the acquisition of west virginia power from dominion
resources for about 21 mln dlrs.
    the sale was approved by the west virginia public service
commission in january and became effective march one. west
virginia's management will continue to be responsible for
operating the utility, it said.
 reuter
</text>]