[<text>
<title>pacific nuclear systems &lt;pacn.o&gt; wins contract</title>
<dateline>    federal way, wash., april 13 - </dateline>pacific nuclear systems inc
said its nuclear packaging inc unit won a contract with the
u.s. department of energy worth up to 2.3 mln dlrs.
    the company said the initial phase of the contract was
awarded for 1.2 mln dlrs, with an optional second phase for 1.1
mln dlrs.
    under the pact, pacific nuclear will design, license and
fabricate two truck casks and trailers for defense program
wastes.
    the contract wil begin immediately, with the first cask
delivery planned for may, 1989.
 reuter
</text>]