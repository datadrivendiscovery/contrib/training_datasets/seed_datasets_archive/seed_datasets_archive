[<text>
<title>textron &lt;txt&gt; gets 420.2 mln dlr contract</title>
<dateline>    washington, march 31 - </dateline>avco lycoming, a division of textron
inc, has been awarded a 420.2 mln dlr contract for work on 810
engines and spare parts for the m-1a1 tank, the army said.
 reuter
</text>]