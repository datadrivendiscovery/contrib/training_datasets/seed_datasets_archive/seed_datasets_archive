[<text>
<title>country wide transport offering 2.2 mln shares</title>
<dateline>    atlanta, march 27 - </dateline>&lt;country wide transport services inc&gt;
said it is holding an initial offering of 2.2 mln shares of
common stock at 15 dlrs per share.
    robinson-humphrey co inc is the manager of the underwriting
group, which has been granted an option to purchase up to an
additional 330,000 shares to cover over-allotments, the company
said.
    of the total shares to be sold, 1,500,000 will be sold by
the company and 700,000 will be sold by stockholders, the
company said. proceeds will be used to reduce indebtedness and
to purchase equipment, the company added.
 reuter
</text>]