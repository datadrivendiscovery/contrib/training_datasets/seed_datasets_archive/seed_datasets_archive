[<text>
<title>varity &lt;vat&gt; unit has new industrial diesels</title>
<dateline>    toronto, march 9 - </dateline>varity corp said perkins engines,
varity's british-based diesel engine subsidiary, introduced
three families of new industrial diesels, ranging from 37 to
400 horsepower.
    the new models include four and six cylinder units, two to
12 liters in displacement, varity said.
 reuter
</text>]