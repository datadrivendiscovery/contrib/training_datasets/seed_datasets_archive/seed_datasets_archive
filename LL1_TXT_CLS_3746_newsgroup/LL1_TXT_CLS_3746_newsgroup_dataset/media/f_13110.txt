[<text>
<title>enex resources corp &lt;enex&gt; 4th qtr loss</title>
<dateline>    kingwood, texas, april 3 -
    </dateline>shr not given
    net loss 330,613 vs profit
    revs 2,170,628 vs 614,511
    year
    shr loss one ct vs profit nine cts
    net loss 212,289 vs profit 829,747
    revs 5,397,167 vs 3,785,688
 reuter
</text>]