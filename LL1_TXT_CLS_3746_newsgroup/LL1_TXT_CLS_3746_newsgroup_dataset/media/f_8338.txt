[<text>
<title>sanford corp &lt;sanf&gt; 1st qtr feb 28 net</title>
<dateline>    bellwood, ill., march 23 -
    </dateline>shr 28 cts vs 13 cts
    net 1,898,000 vs 892,000
    sales 16.8 mln vs 15.3 mln
 reuter
</text>]