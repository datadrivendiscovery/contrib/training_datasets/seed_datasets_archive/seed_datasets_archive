[<text>
<title>bull and bear group a &lt;bnbga&gt; cuts fund payouts</title>
<dateline>    new york, march 24 - </dateline>bull and bear group a said it lowered
its monthly dividends on three of its funds.
    it said it lowered its tax free income fund &lt;bltfx&gt; to 10.3
cts from 10.6 cts; its u.s. government guaranteed securities
fund &lt;bbusx&gt; to 11.5 cts from 11.8 cts; and its high yield fund
&lt;bulhx&gt; to 14 cts from 14.2 cts.
    all dividends are payable march 31 to shareholders of
record march 25, the company said.
 reuter
</text>]