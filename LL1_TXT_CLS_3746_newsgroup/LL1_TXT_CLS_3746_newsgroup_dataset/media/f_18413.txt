[<text>
<title>more eep wheat for west african countries-usda</title>
<dateline>    washington, june 15 - </dateline>u.s. exporters will have the
opportunity to sell 170,000 tonnes of wheat to west african
countries under the export enhancement program, the u.s.
agriculture department said.
    it said in addition to benin, cameroon, ivory coast, ghana
and togo, four more countries -- burkina faso, gabon, liberia
and niger -- have been declared eligible under the initiative.
    the export sales will be subsidized with commodities from
the inventory of the commodity credit corporation (ccc), it
said.
    west african countries have already purchased 129,500
tonnes of wheat under previously announced export bonus
programs, it noted.
 reuter
</text>]