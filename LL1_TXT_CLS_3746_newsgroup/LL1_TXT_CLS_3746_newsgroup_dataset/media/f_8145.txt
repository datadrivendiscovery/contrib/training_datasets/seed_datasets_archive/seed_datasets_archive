[<text>
<title>bundesbank sets new repurchase tender</title>
<dateline>    frankfurt, march 23 - </dateline>the bundesbank set a new tender for a
28-day securities repurchase agreement, offering banks
liquidity aid at a fixed rate of 3.80 pct, a central bank
spokesman said.
    banks must make their bids by 1000 gmt tomorrow, and funds
allocated will be credited to accounts on wednesday. banks must
repurchase securities pledged on april 22.
 reuter
</text>]