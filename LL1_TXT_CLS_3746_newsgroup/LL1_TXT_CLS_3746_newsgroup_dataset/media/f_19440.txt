[<text>
<title>dwi corp &lt;dwic.o&gt; 3rd qtr march 31 net</title>
<dateline>    mission viejo, calif., june 19 -
    </dateline>shr profit nil vs loss two cts
    net profit 39,617 vs loss 200,740
    revs 619,076 vs 491,085
    nine mths
    shr loss nil vs loss five cts
    net loss 68,293 vs loss 434,087
    revs 1,614,960 vs 1,791,148
 reuter
</text>]