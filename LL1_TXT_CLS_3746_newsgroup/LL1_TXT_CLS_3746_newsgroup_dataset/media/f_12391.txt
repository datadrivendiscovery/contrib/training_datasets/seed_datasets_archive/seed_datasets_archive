[<text>
<title>&lt;ocelot industries ltd&gt; year loss</title>
<dateline>    calgary, alberta, april 1 -
    </dateline>shr loss 15.29 dlrs vs loss 2.80 dlrs
    net loss 221.3 mln vs loss 40.4 mln
    revs 146.3 mln vs 260.7 mln
    note: 1986 loss includes extraordinary loss of 171.6 mln
dlrs or 11.85 dlrs shr related to writedown of certain
petrochemical assets and reduced by tax gain of 4.2 mln dlrs or
28 cts shr.
    1985 results restated.
 reuter
</text>]