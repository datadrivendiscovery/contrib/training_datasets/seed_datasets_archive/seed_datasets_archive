[<text>
<title>harman international &lt;hiii.o&gt; 1st qtr sept 30</title>
<dateline>     washington, oct 19 -
    </dateline>shr 30 cts vs 26 cts
    net 2,534,000 vs 1,695,000
    revs 98.8 mln vs 67.1 mln
    avg shrs 8,447,000 vs 6,563,000
    note: full name of company is harman international
industries inc.
 reuter
</text>]