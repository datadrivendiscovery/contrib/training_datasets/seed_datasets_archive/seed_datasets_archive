[<text>
<title>a.h. belo corp &lt;blc&gt; 3rd qtr net</title>
<dateline>    dallas, oct 19 -
    </dateline>shr 59 cts vs 27 cts
    net 6,398,000 vs 2,979,000
    revs 91.0 mln vs 94.1 mln
    avg shrs 10.8 mln vs 11.2 mln
    nine mths
    shr 1.58 dlrs vs 1.06 dlrs
    net 17.2 mln vs 12.2 mln
    revs 279.7 mln vs 289.1 mln
    avg shrs 10.9 mln vs 11.4 mln
 reuter
</text>]