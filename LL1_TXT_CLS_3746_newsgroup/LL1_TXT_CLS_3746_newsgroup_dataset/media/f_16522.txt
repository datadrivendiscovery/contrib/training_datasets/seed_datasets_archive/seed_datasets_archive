[<text>
<title>&lt;c and r clothiers inc&gt; jan 31 year net</title>
<dateline>    culver city, calif., april 13 -
    </dateline>shr 1.46 dlrs vs 66 cts
    net 1,514,312 vs 714,670
    sales 62.1 mln vs 57.2 mln
 reuter
</text>]