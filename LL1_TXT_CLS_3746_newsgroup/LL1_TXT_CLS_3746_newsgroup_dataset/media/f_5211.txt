[<text>
<title>eksportfinans launches eurobond</title>
<dateline>    london, march 16 - </dateline>eksportfinans a/s of norway is issuing
15 billion yen in eurobonds due april 23, 1992 priced at 103
with a five pct coupon, mitsubishi finance international ltd
said as lead manager.
    fees are 1-1/4 pct for selling and 5/8 pct for management
and underwriting. payment date is april 23. listing will be in
london and the bonds will be sold in denominations of one mln
yen.
 reuter
</text>]