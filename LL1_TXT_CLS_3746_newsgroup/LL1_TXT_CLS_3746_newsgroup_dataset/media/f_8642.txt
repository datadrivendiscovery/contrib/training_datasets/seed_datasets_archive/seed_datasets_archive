[<text>
<title>skandinaviska enskilda issues yen bonds</title>
<dateline>    london, march 24 - </dateline>skandinaviska enskilda banken is issuing
a 10 billion yen bond due october 23, 1992 carrying a coupon of
4-1/2 pct and priced at 101-3/8, lead manager daiwa securities
international (europe) ltd said.
    the securities wil be listed on the london stock exchange
and will be available in denominations of one mln yen each.
payment date is april 23, 1987.
    there is a 1-1/4 pct selling concession and a 5/8 pct
combined management and underwriting fee.
 reuter
</text>]