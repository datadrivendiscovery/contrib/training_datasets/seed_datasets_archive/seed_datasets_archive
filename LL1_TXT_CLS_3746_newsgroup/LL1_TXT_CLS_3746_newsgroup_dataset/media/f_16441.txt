[<text>
<title>&lt;coast r.v. inc&gt; 1st qtr net</title>
<dateline>    san jose, calif., april 13 -
    </dateline>shr profit one ct vs loss 28 cts
    net profit 23,000 vs loss 725,000
    sales 20.6 mln vs 18.5 mln
    avg shrs 3,959,011 vs 2,608,571
 reuter
</text>]