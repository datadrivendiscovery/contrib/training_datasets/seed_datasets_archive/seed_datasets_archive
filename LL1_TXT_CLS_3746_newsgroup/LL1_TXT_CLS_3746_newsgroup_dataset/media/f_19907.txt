[<text>
<title>international broadcasting &lt;ibca.o&gt; in offering</title>
<dateline>    minneapolis, june 29 - </dateline>international broadcasting corp said
it plans to make a secondary public offering of between 700,000
and 800,000 shares of common stock.
    the offering will be made through underwriters in
mid-august. international broadcasting did not identify the
underwriters.
    the company said it plans to file a registration statement
on the offering with the securities and exchange commission.
 reuter
</text>]