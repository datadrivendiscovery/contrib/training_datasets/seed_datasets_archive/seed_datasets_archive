[<text>
<title>campbell resources inc &lt;cch&gt; 2nd qtr dec 31 net</title>
<dateline>    toronto, march 17 -
    </dateline>shr profit three cts vs loss 10 cts
    net profit 765,000 vs loss 2,600,000
    revs 9,259,000 vs 14,479,000
    six mths
    shr profit three cts vs loss 11 cts
    net profit 875,000 vs loss 2,303,000
    revs 17.7 mln vs 29.2 mln
    note: campbell changing its yr-end to dec 31 from june 30.
 reuter
</text>]