[<text>
<title>barnes &lt;b&gt; unit to construct new plant</title>
<dateline>    bristol, conn., june 2 - </dateline>barnes group inc said it will
build a new 3.7 mn dlrs manufacturing plant in saline, mich.,
to house its associated spriung unit's high-tech valve and
torque converter clutch spring operations.
 reuter
</text>]