[<text>
<title>swedish export credit issues new zealand dlr bond</title>
<dateline>    london, march 25 - </dateline>swedish export credit is issuing 75 mln
new zealand dlr eurobonds due april 28, 1989, paying 19 pct and
priced at 101-1/4 pct, lead manager credit suisse first boston
said.
    the bond is available in denominations of 1,000 and 5,000
dlrs and will be listed in luxembourg. fees comprise 7/8 pct
selling concession and 3/8 pct management and underwriting.
 reuter
</text>]