[<text>
<title>house of fabrics inc &lt;hf&gt; 1st qtr net april 30</title>
<dateline>    sherman oaks, calif., june 1 - 
    </dateline>shr 27 cts vs 18 cts
    net 1,757,000 vs 1,201,000
    revs 73.5 mln vs 71.2 mln
 reuter
</text>]