[<text>
<title>oncogene &lt;oncs&gt; president resigns</title>
<dateline>    mineola, n.y., march 18 - </dateline>oncogene science inc said robert
ivy is resigning as president, chief executive officer and
director, effective april 12
    no immediate successor was named.
 reuter
</text>]