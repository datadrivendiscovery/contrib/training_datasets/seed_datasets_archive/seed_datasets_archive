[<text>
<title>uaw confirms talks at gm &lt;gm&gt; plant to resume</title>
<dateline>    detroit, march 26 - </dateline>the united auto workers confirmed an
earlier company statement that contract talks between striking
workers at a general motors corp's michigan plant and the union
will resume at 1000 est tomorrow.
    a uaw spokesman, in confirming a gm statement that talks
will restart, said it "remains to be seen whether the talks
will be successful."
    the union, in a statement, said it was unable to reach a
comprehensive settlement with gm on the issue of the company's
subcontracting work to non-union labor, although tentative
agreement was reached "on many matters." a uaw spokesman would
not discuss the issues involved.
    the union's 9,000 workers walked off the job at gm's
pontiac, mich., truck and bus works earlier today.
 reuter
</text>]