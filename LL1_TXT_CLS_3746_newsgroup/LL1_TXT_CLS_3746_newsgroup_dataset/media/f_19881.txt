[<text>
<title>american century &lt;act&gt; unit sells stock</title>
<dateline>    san antonio, texas, june 29 - </dateline>american century corp said
its commerce savings unit will sell 997,756 common shares in
first american financial corp &lt;famr.o&gt;, representing 18 pct of
first american.
    the company said the shares are being repurchased by first
american for 29.4 mln dlrs and two real estate assets in
california.
    it said commerce savings acquired the shares in 1982 and
1983.
 reuter
</text>]