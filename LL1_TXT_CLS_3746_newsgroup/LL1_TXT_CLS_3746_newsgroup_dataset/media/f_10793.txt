[<text>
<title>amvestors &lt;avfc&gt; files for share offering</title>
<dateline>    topeka, kan., march 30 - </dateline>amvestors financial corp said it
has filed to offer 2,500,000 common shares through underwriters
&lt;smith barney, harris upham and co inc&gt; and morgan keegan inc
&lt;mor&gt;.
    it said proceeds will be used for general corporate
purposes.
 reuter
</text>]