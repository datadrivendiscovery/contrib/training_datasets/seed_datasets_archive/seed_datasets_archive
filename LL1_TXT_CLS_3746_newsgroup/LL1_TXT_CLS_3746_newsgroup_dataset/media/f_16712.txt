[<text>
<title>heileman &lt;ghb&gt; acquiring c. schmidt trademarks</title>
<dateline>    la crosse, wis., april 13 - </dateline>g. heileman brewing co inc said
it agreed to acquire the trademarks of philadelphia-based c.
schmidt which produces a line of malt beverage products,
including schmidt, rheingold, duquesne and ortlieb.
    in 1986, schmidt sold about 1.6 mln barrels.
    under terms of the proposed agreement, heileman said it
will enter a trademark transfer agreement which provides for
royalties to be paid to schmidt over a specified period of time
on beverage products produced under the c. schmidt labels.

 reuter
</text>]