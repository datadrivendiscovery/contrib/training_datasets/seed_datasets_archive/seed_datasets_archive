[<text>
<title>canada corn decision unjustified - yeutter</title>
<dateline>    washington, march 6 - </dateline>u.s. trade representative clayton
yeutter said canada's finding announced today that u.s. corn
imports injure canadian farmers is "totally unjustified."
    "u.s. corn exports to canada are so small that it is
inconceivable that they injure canadian corn farmers by any
reasonable measure," yeutter said in a statement.
    he said if other countries follow canada's lead it could
result in "a rash of protectionist actions throughout the
world." french corn growers have recently indicated they will
challenge u.s. corn gluten feed shipments to europe.
    yeutter said the u.s. will examine the canadian decision
closely and if the u.s. believes the decision was not based on
facts, "will carefully evaluate appropriate responses." yeutter
did not say what steps the u.s. may take in response.
 reuter
</text>]