[<text>
<title>investors file proxy for new hbo &lt;hboc&gt; board</title>
<dateline>    great falls, va., march 30 - </dateline>andover group said it filed
preliminary proxy materials with the securities and exchange
commission to elect a slate of directors to hbo and co's board
at its april 30 annual meeting.
    andover group said it owns seven pct of hbo, a hospital
information systems company.
</text>]