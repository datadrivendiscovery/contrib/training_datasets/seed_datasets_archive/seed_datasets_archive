[<text type="unproc">
federal reserve weekly report 1 - feb 26
    two weeks ended feb 25      daily avgs-mlns
 net free reserves.............644 vs.....1,337
 bank borrowings...............680 vs.......425
 including seasonal loans.......81 vs........56
 including extended loans......299 vs.......265
 excess reserves.............1,025 vs.....1,497
 required reserves (adj)....55,250 vs....55,366
 required reserves............n.a. vs......n.a.
 total reserves...............n.a. vs......n.a.
 non-borrowed reserves........n.a. vs......n.a.
 monetary base................n.a. vs......n.a.
                         
     two weeks ended feb 25                   
 total vault cash.............n.a. vs......n.a.
 inc cash equal to req res....n.a. vs......n.a.
     one week ended feb 25      daily avgs-mlns
 bank borrowings...............614 down.....131
 including seasonal loans.......88 up........14
 including extended loans......304 up........10
 float.........................511 down.....320
 balances/adjustments........2,101 down......67
 currency..................206,490 down.....519
 treasury deposits...........4,208 down......63

 reuter


</text>]