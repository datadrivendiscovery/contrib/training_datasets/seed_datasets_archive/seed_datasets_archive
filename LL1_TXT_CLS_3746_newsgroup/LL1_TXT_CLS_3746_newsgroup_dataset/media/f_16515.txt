[<text>
<title>foothill group &lt;fgi&gt; completes debt placement</title>
<dateline>    los angeles, april 13 - </dateline>the foothill group inc said its
foothill captial corp unit completed the placement of 23 mln
dlrs in senior debt and 27 mln dlrs in senior subordinated
debt.
    goldman, sachs and co privately placed the debt with a
group of institutional lenders, the company said.
    it said the senior debt will bear a 9.4 pct interest rate
and the senior subordinated debt will bear 10.15 pct.
   
 reuter
</text>]