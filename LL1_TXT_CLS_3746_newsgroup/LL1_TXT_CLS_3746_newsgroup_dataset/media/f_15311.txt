[<text>
<title>raytech &lt;ray&gt; buys west german company</title>
<dateline>    new york, april 8 - </dateline>raytech corp said it acquired
&lt;raybestos industrie-produkte gmbh&gt; for 7.5 mln dlrs.
    raybestos, with manufacturing facilities in radevormwald,
west germany, produces friction materials for use in clutch and
braking applications.
 reuter
</text>]