[<text>
<title>manhattan national corp &lt;mlc&gt; 4th qtr loss</title>
<dateline>    new york, march 23 -
    </dateline>oper shr loss 20 cts vs loss 81 cts
    oper net loss 1,042,000 vs loss 4,077,000
    revs 38.5 mln vs 50.3 mln
    12 mths
    oper shr loss six cts vs loss 43 cts
    oper net loss 336,000 vs loss 2,176,000
    revs 137.8 mln vs 209.1 mln
    note: qtrs 1986 and prior exclude net realized investment
gains of 74,000 dlrs and 644,000 dlrs, respectively, and years
1986 and prior exclude realized investment gains of 642,000
dlrs and 1,979,000 dlrs, respectively.
 reuter
</text>]