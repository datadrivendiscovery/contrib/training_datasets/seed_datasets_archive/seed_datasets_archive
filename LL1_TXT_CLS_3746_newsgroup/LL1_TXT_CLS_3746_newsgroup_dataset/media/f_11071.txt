[<text>
<title>florida weekly crop report</title>
<dateline>    orlando, fla, march 30 - </dateline>florida's citrus groves continue
in very good condition, acccording to the latest report by the
u.s. department of agriculture's florida agricultural
statistics service. late-week rains and thunderstorms came at
an opportune time. warm daytime temperatures and good soil
moisture have produced an abundance of new growth and bloom.
    most trees are in some stage of bloom development with
petal drop already taking place in many south florida groves.
    harvest of late-type valencia oranges is increasing rapidly
with the near completion of the early and midseason varieties.
rain during the week caused some delay in picking.
    for the week ended march 29, there were an estimated 77,000
boxes of early and midseason and 945,000 boxes of late season
oranges harvested, the usda said.
 reuter
</text>]