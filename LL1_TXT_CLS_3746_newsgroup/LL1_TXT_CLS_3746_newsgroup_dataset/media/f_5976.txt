[<text>
<title>codenoll technology corp &lt;codn&gt; 4th qtr loss</title>
<dateline>    yonkers, n.y., march 17 -
    </dateline>shr loss 12 cts vs loss 17 cts
    net loss 484,556 vs loss 620,607
    sales 2,167,631 vs 1,062,837
    avg shrs 3,985,924 vs 3,935,969
    year
    shr loss 62 cts vs loss 52 cts
    net loss 2,468,605 vs loss 1,788,406
    sales 6,603,285 vs 4,650,585
    avg shrs 3,983,692 vs 3,446,348
 reuter
</text>]