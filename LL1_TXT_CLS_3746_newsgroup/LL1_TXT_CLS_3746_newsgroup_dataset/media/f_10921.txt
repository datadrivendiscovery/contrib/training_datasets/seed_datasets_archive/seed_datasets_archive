[<text>
<title>computer microfilm corp &lt;comi&gt; year net</title>
<dateline>    atlanta, march 30 -
    </dateline>shr 23 cts vs 14 cts
    net 439,100 vs 259,948
    revs 9,918,413 vs 9,683,392
    note: share adjusted for five pct stock dividend.
 reuter
</text>]