[<text>
<title>schering-plough &lt;sgp&gt; trademark dispute settled</title>
<dateline>    kenilworth, n.j., march 11 - </dateline>schering corp, a unit of
schering-plough corp, said &lt;nature's blend products inc&gt; agreed
to an injunction against its fiber slim product.
    schering, the u.s. distributor of fibre trim made by &lt;farma
food a/s&gt; of denmark, said it and farma had sued nature's
blend, claiming nature's product infringed their trademark on
fibre trim.
    schering said nature's blend agreed to stop making and
selling food supplement products under the fiber slim
trademark, but it did not admit liability.
 reuter
</text>]