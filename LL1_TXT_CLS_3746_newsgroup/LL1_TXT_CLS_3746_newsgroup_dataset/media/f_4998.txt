[<text>
<title>general cinema corp &lt;gcn&gt; common stock dividend</title>
<dateline>    chestnut hill, mass., march 12 - 
    </dateline>qtly div 15 cts vs 15 cts prior
    pay april 30
    recrod april nine
 reuter
</text>]