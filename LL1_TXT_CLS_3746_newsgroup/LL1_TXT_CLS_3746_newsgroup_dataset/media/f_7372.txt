[<text>
<title>north american philips &lt;nph&gt; unit names executive</title>
<dateline>    new york, march 19 - </dateline>north american philips corp said lyman
beggs has been named president of its consumer products
divisions.
    the company said beggs, formerly executive vice president
of tambrands inc &lt;tmb&gt;, succeeds richard kress, who retired.
    the company said approximately 58 pct of the common stock
of north american philips is owned by n.v. philips of the
netherlands.
 reuter
</text>]