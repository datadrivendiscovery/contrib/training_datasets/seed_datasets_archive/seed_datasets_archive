[<text>
<title>canada approves u.s. gas exports by progas</title>
<dateline>    ottawa, april 13 - </dateline>progas ltd was issued an export licence
to sell 10.3 billion cubic meters of natural gas to ocean state
power co of burrillville, rhode island, the federal energy
department said.
    the sale, covering a 20 year period beginning may 1, 1989,
was previously recommended by the national energy board.
    contract terms were not released.
 reuter
</text>]