[<text>
<title>cra sold forrest gold for 76 mln dlrs - whim creek</title>
<dateline>    sydney, april 8 - </dateline>&lt;whim creek consolidated nl&gt; said the
consortium it is leading will pay 76.55 mln dlrs for the
acquisition of cra ltd's &lt;craa.s&gt; &lt;forrest gold pty ltd&gt; unit,
reported yesterday.
    cra and whim creek did not disclose the price yesterday.
    whim creek will hold 44 pct of the consortium, while
&lt;austwhim resources nl&gt; will hold 27 pct and &lt;croesus mining
nl&gt; 29 pct, it said in a statement.
    as reported, forrest gold owns two mines in western
australia producing a combined 37,000 ounces of gold a year. it
also owns an undeveloped gold project.
 reuter
</text>]