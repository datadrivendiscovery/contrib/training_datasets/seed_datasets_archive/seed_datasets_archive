[<text>
<title>rolls-royce signs new engine deal with boeing</title>
<dateline>    london, march 25 - </dateline>&lt;rolls-royce ltd&gt; said it signed an
agreement with the boeing company &lt;ba.n&gt; to install its rb
211-524 d4d engine on the b767 family of aircraft.
    the engine, currently under development, will be ready for
service by early 1990, the company said in a statement.
    boeing 747s are also powered with the engine, and its use
in b767 craft too could bring significant operating benefits,
rolls-royce said.
 reuter
</text>]