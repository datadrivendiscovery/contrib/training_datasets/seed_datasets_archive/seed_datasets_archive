[<text>
<title>flight dynamics &lt;flty&gt; chairman steps down</title>
<dateline>    portland, ore., march 26 - </dateline>flight dynamics inc said robert
l. carter has stepped down as chairman but will continue as a
director.
    the company said carter has been succeeded by john h.
geiger, who had been president and chief executive officer
since 1985.
    geiger's post, in turn, has been filled by john p. desmond,
previously executive vice president and chief operating
officer, the company said.
 reuter
</text>]