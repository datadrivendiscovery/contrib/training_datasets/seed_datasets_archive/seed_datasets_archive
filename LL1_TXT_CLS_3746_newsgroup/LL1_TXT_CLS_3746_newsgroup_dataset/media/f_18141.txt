[<text>
<title>integrated &lt;ign&gt; unit in initial offering</title>
<dateline>    bellport, n.y., june 2 - </dateline>integrated generics inc said its
anda development corp subsidiary filed with the securities and
exchange commission for an initial offering of 600,000 units,
with each consisting of one share of common stock and one
common purchase warrant.
    the company said the units will be offered on a "best
efforts" all or none basis by s.j. bayern and co inc and r.f.
lafferty and co inc.
    it said each unit is expected to be offered at five dlrs.
   
 reuter
</text>]