[<text>
<title>pulitzer&lt;pltz.o&gt; raises price of post-dispatch</title>
<dateline>    st. louis, june 1 - </dateline>pulitzer publishing co said it
increased the retail price of the sunday st. louis
post-dispatch to 1.00 dlrs a copy from 75 cts, effective june
14.
    the post-dispatch, which last increased the price of its
sunday paper in 1980, said it will receive 19 cts as its share
of the 25 cts increase.
    for the quarter ended march 31, the post had an average
sunday circulation of 556,620.
 reuter
</text>]