[<text>
<title>miyazawa, baker talked recently over telephone</title>
<dateline>    tokyo, april 3 - </dateline>finance minister kiichi miyazawa said he
recently talked with u.s. treasury james baker over the
telephone, but declined to say what was discussed.
    miyazawa told reporters he talks with the u.s. treasury
secretary from time to time on economic issues.
    the yomiuri shimbun daily newspaper reported baker called
miyazawa early this week to warn him next week's group of seven
talks would not be a success unless japan expands domestic
demand. the newspaper quoted ruling liberal democratic party
(ldp) sources.
    miyazawa told reporters the ldp will come up with a draft
economic package to stimulate the economy before he leaves next
week for washington.
    the finance minister leaves on april 7 to attend the imf
and world bank meetings.
    miyazawa did not say what would be in the ldp package.
 reuter
</text>]