[<text>
<title>soviet negotiator reports arms talks breakthrough</title>
<dateline>    paris, march 6 - </dateline>chief soviet arms negotiator yuli
vorontsov said soviet-american talks on cutting medium-range
nuclear missiles had made a breakthrough.
    vorontsov said he expected a treaty would be ready for
signing within three to four months following the latest talks
in geneva.
    he told a news conference here "all the elements point to
optimism" and only technical work on treaty language remained
outstanding.
 reuter
</text>]