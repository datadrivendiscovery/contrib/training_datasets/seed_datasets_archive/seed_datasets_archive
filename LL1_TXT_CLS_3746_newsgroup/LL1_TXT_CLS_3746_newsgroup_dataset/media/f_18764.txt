[<text>
<title>omi &lt;omic.o&gt; plans to reduce debt to 30 mln dlrs</title>
<dateline>    new york, june 18 - </dateline>omi corp said it expects to reduce the
debt guaranteed by former parent ogden corp &lt;og&gt; to 30 mln dlrs
from about 50 mln dlrs by the end of 1987 from financing
measures including an employee stock ownership plan.
    the bulk shipping company said it expects to have the esop
in place before year end, subject to stockholder approval.
    the company also said it anticipates later this year to
refinance its high cost debt related to a u.s. flag chemical
tanker through a lease swap arrangement.
    omi said this move would further enhance cash flow.
   
    in another matter, the company said through a joint venture
with &lt;anders wilhelmsen and co&gt;, three large crude carriers
have been chartered-in for minimum periods of six months with
various extension options, and, in one case, a purchase option.
    the company said it anticipates that the purchase option
will be exercised by the joint venture this summer.
 reuter
</text>]