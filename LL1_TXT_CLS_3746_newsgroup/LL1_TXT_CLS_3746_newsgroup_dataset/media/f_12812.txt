[<text>
<title>u.k. money market deficit further revised upward</title>
<dateline>    london, april 3 - </dateline>the bank of england said it has revised
its estimate of today's shortfall to 800 mln stg from 750 mln,
before taking account of 170 mln stg morning assistance.
 reuter
</text>]