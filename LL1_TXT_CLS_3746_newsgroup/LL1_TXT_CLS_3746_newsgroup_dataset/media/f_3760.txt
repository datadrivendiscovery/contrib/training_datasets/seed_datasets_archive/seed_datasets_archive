[<text>
<title>gevaert nv &lt;gevn.br&gt; 1986 year</title>
<dateline>    brussels, march 11 -
    </dateline>consolidated group net profit 1.88 billion francs vs 1.29
billion.
    net financial and operating profit 1.11 billion vs 825.1
mln.
    extraordinary earnings 774.7 mln vs 462.8 mln.
    net dividend 165 francs vs 150.
 reuter
</text>]