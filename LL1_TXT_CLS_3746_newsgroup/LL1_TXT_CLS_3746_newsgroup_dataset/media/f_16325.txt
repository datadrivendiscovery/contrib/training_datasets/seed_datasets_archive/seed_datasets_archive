[<text>
<title>du pont &lt;dd&gt; unit to redeem notes</title>
<dateline>    new york, april 13 - </dateline>du pont overseas capital n.v., a unit
of du pont co, said it will redeem on may 15 all 189 mln dlrs
of its outstanding 13-3/4 pct guaranteed retractable notes due
1997.
    it will buy back the notes at par value plus accrued
interest, du pont overseas said.
 reuter
</text>]