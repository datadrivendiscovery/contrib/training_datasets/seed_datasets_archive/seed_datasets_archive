[<text>
<title>wickes &lt;wix&gt; plans reverse split, calls debt</title>
<dateline>    santa monica, calif., march 23 - </dateline>wickes cos inc said its
board authorized a one-for-five reverse stock split and plans
to call the company's its 12 pct senior subordianted debentures
due 1994.
    the company said it will seek shareholder approval of the
reverse stock split at the annual shareholders meeting
scheduled for june 18.
    at january 31 wickes had 239 mln shares outstanding, the
company also said.
    wickes also said it will call the debentures on dec 1,
1987, assuming market conditions remain essentially the same.
 reuter
</text>]