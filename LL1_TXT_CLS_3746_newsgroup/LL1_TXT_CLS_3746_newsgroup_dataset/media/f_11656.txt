[<text>
<title>treasury balances at fed rose on march 30</title>
<dateline>    washington, march 31 - </dateline>treasury balances at the federal
reserve rose on march 30 to 3.254 billion dlrs from 2.424
billion dlrs on the previous business day, the treasury said in
its latest budget statement.
    balances in tax and loan note accounts fell to 7.291
billion dlrs from 9.706 billion dlrs on the same respective
days.
    the treasury's operating cash balance totaled 10.544
billion dlrs on march 30 compared with 12.131 billion dlrs on
march 27.
 reuter
</text>]