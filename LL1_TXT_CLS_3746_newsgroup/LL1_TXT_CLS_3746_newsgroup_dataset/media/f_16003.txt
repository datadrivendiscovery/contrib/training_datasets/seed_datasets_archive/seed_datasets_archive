[<text>
<title>beverly enterprises &lt;bev&gt; sets regular dividend</title>
<dateline>    pasedena, calif., april 9 - 
    </dateline>qtly div five cts vs five cts prior
    pay july 13
    record june 30
 reuter
</text>]