[<text>
<title>hog and cattle slaughter guesstimates</title>
<dateline>    chicago, april 9 - </dateline>chicago mercantile exchange floor
traders and commission house representatives are guesstimating
today's hog slaughter at about 287,000 to 295,000 head versus
292,000 week ago and 322,000 a year ago.
    cattle slaughter is guesstimated at about 124,000 to
128,000 head versus 129,000 week ago and 134,000 a year ago.
 reuter
</text>]