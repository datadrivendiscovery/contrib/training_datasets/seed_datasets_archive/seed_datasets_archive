[<text>
<title>bristol-myers trust &lt;byu&gt; extends expiration</title>
<dateline>    new york, march 31 - </dateline>&lt;americus shareowner service corp&gt;
said the americus trust for bristol-myers will continue to
accept tendered bristol-meyers co &lt;bmy&gt; shares until december
one, extending the original expiration date of april one for
eight months.
    if the common stock price exceeds the trust's termination
price of 110 dlrs, americus shareowner service said, the trust
will be temporarily closed until the price of the underlying
stock falls below the termination price.
 reuter
</text>]