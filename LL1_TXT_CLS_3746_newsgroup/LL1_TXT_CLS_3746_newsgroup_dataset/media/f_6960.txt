[<text>
<title>australian wheat board renews japan supply pact</title>
<dateline>    melbourne, march 19 - </dateline>the australian wheat board (awb)
expects to sell about 900,000 tonnes of wheat to the japanese
food agency this year after renewing its annual supply
agreement, awb general manager ron paice said.
    under the agreement, the awb makes the wheat available and
sells into the food agency's regular tenders, he said in a
statement.
    he noted that the board has sold more than three mln tonnes
to japan in the past three years.
 reuter
</text>]