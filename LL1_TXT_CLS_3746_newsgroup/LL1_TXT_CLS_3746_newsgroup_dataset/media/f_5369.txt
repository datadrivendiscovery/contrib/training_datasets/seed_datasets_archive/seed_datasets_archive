[<text>
<title>cme, cboe set joint press conference for today</title>
<dateline>    chicago, march 16 - </dateline>the chicago mercantile exchange, cme,
and the chicago board options exchange, cme, will hold a joint
news conference at 1200 cst (1800 gmt) today.
    neither exchange would elaborate on the topic of the press
conference, although a cboe spokesperson said it will deal a
new financial product.
 reuter
</text>]