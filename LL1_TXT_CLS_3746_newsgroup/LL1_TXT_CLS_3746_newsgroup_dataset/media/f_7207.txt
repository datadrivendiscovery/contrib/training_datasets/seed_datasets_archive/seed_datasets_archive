[<text>
<title>fed adds reserves via customer repurchases</title>
<dateline>    new york, march 19 - </dateline>the federal reserve entered the u.s.
government securities market to arrange 1.5 billion dlrs of
customer repurchase agreements, a fed spokesman said.
    dealers said federal funds were trading at six pct when the
fed began its temporary and indirect supply of reserves to the
banking system.
 reuter
</text>]