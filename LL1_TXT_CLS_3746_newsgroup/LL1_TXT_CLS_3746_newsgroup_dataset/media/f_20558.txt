[<text>
<title>consolidated fibres inc &lt;cfib.o&gt; 1st qtr net</title>
<dateline>    richmond, calif., oct 20 - </dateline>qtr ended sept 30
    shr 40 cts vs 20 cts
    net 797,000 vs 403,000
    sales 30.8 mln vs 27.3 mln
 reuter
</text>]