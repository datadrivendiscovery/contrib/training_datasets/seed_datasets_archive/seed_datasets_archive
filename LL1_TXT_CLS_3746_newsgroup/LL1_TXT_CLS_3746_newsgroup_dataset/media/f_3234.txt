[<text>
<title>university patents &lt;upt&gt; lens sales rise</title>
<dateline>    westport, conn., march 9 - </dateline>university patents inc said
4,344 of its aleges soft bifocal contract lenses were sold in
february, after allowances for exchanges and returns, up from
3,011 in january.
    it said 1,800 opthalmologists and optometrists were fitting
the product last month, up from 1,600 a month before. sales of
the lens started in may 1986.
 reuter
</text>]