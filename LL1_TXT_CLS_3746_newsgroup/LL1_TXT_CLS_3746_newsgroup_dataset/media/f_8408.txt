[<text>
<title>unc &lt;unc&gt; gets 16 mln dlr contract</title>
<dateline>    annapolis, march 23 - </dateline>unc inc said it has received a 16 mln
dlr contract from the u.s. department of energy for production
of naval propulsion system components.
 reuter
</text>]