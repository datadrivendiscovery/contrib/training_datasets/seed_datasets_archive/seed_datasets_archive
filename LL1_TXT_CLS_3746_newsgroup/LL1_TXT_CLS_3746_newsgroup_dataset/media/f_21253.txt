[<text>
<title>system software &lt;ssax.o&gt; completes acquisition</title>
<dateline>    chicago, oct 19 - </dateline>system software associates inc said it
completed its previously-announced acquisition of admin edp pty
ltd for cash and a small amount of stock.
    admin edp, of sydney, australia, is a full-service software
sales and services firm.
 reuter
</text>]