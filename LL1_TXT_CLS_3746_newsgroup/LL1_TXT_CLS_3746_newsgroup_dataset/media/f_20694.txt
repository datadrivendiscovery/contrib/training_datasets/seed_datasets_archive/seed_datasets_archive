[<text>
<title>burlington northern &lt;bni&gt; to repurchase stock</title>
<dateline>    seattle, oct 20 - </dateline>burlington northern inc said it plans to
repurchase up to five mln shares, or about seven pct of its
common stock.
    the company, joining a number of others buying back their
shares in the wake of yesterday's maket decline, said it will
make the purchases from time to time, based on market
conditions.
    burlington said its operating businesses are performing
well and are expected to continue generating substantial free
cash flow.
 reuter
</text>]