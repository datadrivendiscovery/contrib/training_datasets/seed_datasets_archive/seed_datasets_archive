[<text>
<title>s. african trade surplus rises sharply in february</title>
<dateline>    johannesburg, march 27 - </dateline>south africa's trade surplus rose
to 1.62 billion rand in february after falling to 906.2 mln in
january, customs and excise figures show.
    this compares with a year earlier surplus of 958.9 mln
rand.
    exports rose slightly to 3.36 billion rand in february from
3.31 billion in january but imports fell to 1.74 billion from
2.41 billion.
    this brought total exports for the first two months of 1987
to 6.67 billion rand and imports to 4.15 billion for a total
surplus of 2.52 billion rand against 1.71 billion a year
earlier.
 reuter
</text>]