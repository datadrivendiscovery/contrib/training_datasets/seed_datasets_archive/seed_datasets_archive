[<text>
<title>axlon &lt;axln&gt; names co-chief executive officer</title>
<dateline>    sunnyvale, calif., march 11 - </dateline>axlon inc said austin c.
marshall has been named president and co-chief executive
officer, sharing power in the latter post with founder and
chairman nolan k. bushnell.
    the company said marshall, who had been executive vice
president of new york toy company playtime, will have
responsibility for the distribution of toys and bushnell will
concentrate on research and development and licensing.
 reuter
</text>]