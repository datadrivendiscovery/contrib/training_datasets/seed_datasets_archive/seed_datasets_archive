[<text>
<title>hanover insurance co &lt;hins&gt; gets new executive</title>
<dateline>    boston, march 12 - </dateline>hanover insurance co said joseph henry
has been elected financial vice president of the company and
its subsidiary, the massachusetts bay insurance co.
    henry is a former partner of peat marwick mitchell, hanover
said.
 reuter
</text>]