[<text>
<title>electro-sensors inc &lt;else&gt; votes extra payout</title>
<dateline>    minneapolis, march 26 - </dateline>electro-sensors inc said its board
voted an extraordinary cash dividend on its common stock of 10
cts a share payable may 15, record april 30.
    the company paid an extraordinary dividend of 10 cts in may
1986.

 reuter
</text>]