[<text>
<title>aircoa &lt;airc&gt; forms limited partnership</title>
<dateline>    denver, march 6 - </dateline>aircoa said it formed aircoa hotel
partners lp, a limited partnership formed to acquire and
operate hotel and resort properties.
    aircoa said it holds a one pct general partner interest in
the partnership. the remaining 99 pct interest is held by
aircoa affiliates, it said.
    aircoa said it plans to file a registration statement for
the public offering of limited partnership interests. net
proceeds would be used to refinance the partnerships unsecured
debt and for capital improvements to hotels, it said.
    aircoa said the partnership bought seven hotel and resort
properties from its affiliates.
    it said financing for the acquisitions was provided by a 90
mln dlr mortgage loan from bankers trust co and cassa di
risparmio di torino of new york.
    the partnership also received 30 mln dlrs of unsecured
loans from a group of institutional lenders and 10 mln dlrs in
subordinated debt from aircoa, the company said.
 reuter
</text>]