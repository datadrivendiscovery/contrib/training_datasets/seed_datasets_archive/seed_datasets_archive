[<text>
<title>nationwide cellular service inc &lt;ncel&gt; 4th qtr</title>
<dateline>    valley stream, n.y., march 26 -
    </dateline>shr loss six cts vs loss 18 cts
    net loss 89,478 vs loss 178,507
    revs 3,894,844 vs 1,964,141
    avg shrs 1,582,790 vs one mln
    year
    shr loss 43 cts vs loss 81 cts
    net loss 534,099 vs loss 811,836
    revs 12.2 mln vs 5,167,573
    avg shrs 1,251,337 vs one mln
 reuter
</text>]