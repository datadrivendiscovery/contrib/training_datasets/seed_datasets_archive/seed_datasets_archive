[<text>
<title>pep boys - manny, moe and jack inc &lt;pby&gt; 4th qtr</title>
<dateline>    philadelphia, march 25 - </dateline>jan 31 end
    shr 45 cts vs 37 cts
    net 8,349,000 vs 6,187,000
    sales 126.7 mln vs 103.7 mln
    year
    shr 1.55 dlrs vs 1.29 dlrs
    net 28.1 mln vs 21.1 mln
    sales 485.9 mln vs 388.9 mln
    note: latest year net includes three cts shr gain from sale
of assets.
 reuter
</text>]