[<text>
<title>northern telecom &lt;nt&gt; in pact with u.s. firm</title>
<dateline>    toronto, march 24 - </dateline>northern telecom ltd said it signed a
two-year supply agreement with u s west materiel resources inc
that could lead to up to 300 mln u.s. dlrs of business.
    it said the agreement provided terms under which it can
supply its dms digital switching systems and related equipment
to u s west's three information distribution companies
consisting of mountain bell, pacific northwest bell and
northwestern bell.
 reuter
</text>]