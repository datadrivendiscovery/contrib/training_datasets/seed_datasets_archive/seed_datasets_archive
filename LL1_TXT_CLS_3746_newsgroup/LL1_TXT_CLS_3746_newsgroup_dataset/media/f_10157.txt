[<text>
<title>ual &lt;ual&gt; unit's eight-year notes yield 8.06 pct</title>
<dateline>    new york, march 26 - </dateline>hertz corp, a unit of ual inc, is
raising 100 mln dlrs through an offering of notes due 1995
yielding 8.06 pct, said lead manager shearson lehman brothers
inc.
    the notes have an eight pct coupon and were priced at 99.65
to yield 102 basis points more than comparable treasury
securities.
    non-callable to maturity, the issue is rated a-3 by moody's
investors and a-minus by standard and poor's. merrill lynch and
morgan stanley co-managed the deal.
 reuter
</text>]