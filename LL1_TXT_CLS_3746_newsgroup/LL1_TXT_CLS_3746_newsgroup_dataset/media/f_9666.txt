[<text>
<title>sdc sydney replaces chairman</title>
<dateline>    vancouver, b.c., march 25 - </dateline>&lt;sdc sydney development corp&gt;
said it terminated its service agreement with chairman and
chief executive walter steel on march 24, and appointed donald
michelin as acting chief executive.
    "the performance of the company since mr. steel's
appointment in june, 1985 has not fulfilled either mr. steel's
plans or the objectives of alexis nihon investments inc, the
principal creditor and largest shareholder, and a change in
senior management was viewed as essential to redirecting the
business of the company," sdc sydney said.
    michelin is an alexis nihon officer and director.
 reuter
</text>]