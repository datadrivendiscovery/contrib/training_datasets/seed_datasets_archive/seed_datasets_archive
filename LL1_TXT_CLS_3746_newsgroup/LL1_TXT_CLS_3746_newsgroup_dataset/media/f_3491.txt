[<text>
<title>queensland press board recommends murdoch offer</title>
<dateline>    brisbane, march 11 - </dateline>the &lt;queensland press ltd&gt; (qpl) board
said it unanimously recommended the one billion dlr takeover
bid by &lt;cruden investments pty ltd&gt;, a family company of news
corp ltd &lt;ncpa.s&gt; chief executive rupert murdoch.
    the 23 dlrs a share cash-only offer is nearly double the
market price before news announced its now-completed bid for
the herald and weekly times ltd &lt;hwta.s&gt; in early december and
no other offer is likely, it said in a statement.
    independent adviser, &lt;wardley australia ltd&gt;, had also
concluded the offer was fair and reasonable, it added.
    qpl is already owned 48.3 pct by hwt.
 reuter
</text>]