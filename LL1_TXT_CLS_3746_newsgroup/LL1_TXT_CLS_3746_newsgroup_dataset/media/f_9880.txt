[<text>
<title>u.k. money market gets 25 mln stg late help</title>
<dateline>    london, march 26 - </dateline>the bank of england said it provided
about 25 mln stg in late help to the money market, bringing the
total assistance today to 266 mln stg.
    this compares with the bank's revised estimate of a 350 mln
stg money market shortfall.
 reuter
</text>]