[<text>
<title>media general &lt;mega&gt; family wont sell shares</title>
<dateline>    richmond, va., oct 19 - </dateline>media general inc's chairman, d.
tennant bryant, said his family would not sell its controlling
share block, so it would be impossible for an investor group
led by &lt;giant group ltd&gt; to gain control of the company.
    the investor group, which includes barris industries
&lt;brrs.o&gt;, recently reported that it acquired a 9.8 pct stake of
media general's class a shares and might seek control.
    bryant said the company's class a stock elects only 30 pct
of the board, with the remaining 70 pct being elected by class
b shares, two-thirds of which are controlled by the bryant
family trust, which has no intention of selling its shares.
 reuter
</text>]