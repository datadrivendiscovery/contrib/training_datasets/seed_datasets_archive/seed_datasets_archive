[<text>
<title>fed adds reserves via two-day repurchases</title>
<dateline>    new york, april 21 - </dateline>the federal reserve entered the u.s.
government securities market to arrange two-day system
repurchase agreements, a fed spokesman said.
    dealers said that federal funds were trading at 6-7/16 pct
when the fed began its temporary and direct supply of reserves
to the banking system.
 reuter
</text>]