[<text>
<title>usda rejects sri lanka's 80 u.s. dlr wheat price</title>
<dateline>    colombo, march 27 - </dateline>sri lankan food department officials
said the u.s. department of agriculture rejected a u.s. firm's
offer of 80 u.s. dlrs per tonne caf to supply 52,500 tonnes of
soft wheat to colombo from the pacific northwest.
    they said sri lanka's food department subsequently made a
counter-offer to five u.s. firms to buy wheat at 85 u.s. dlrs
caf for april 8-16 delivery.
    the company which obtains usda approval for the proposed
price must inform the department before 1330 gmt, they said.
 reuter
</text>]