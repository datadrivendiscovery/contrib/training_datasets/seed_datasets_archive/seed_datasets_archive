[<text>
<title>u.k. money market given 120 mln stg late help</title>
<dateline>    london, march 17 - </dateline>the bank of england said it provided the
money market with late assistance of around 120 mln stg.
    this brings the bank's total help today to some 136 mln stg
and compares with its forecast of a 400 mln stg shortage in the
system.
 reuter
</text>]