[<text>
<title>first of america bank inc &lt;fabk.o&gt; 1st qtr net</title>
<dateline>    kalamazoo, mich., april 13 -
    </dateline>shr 1.12 dlrs vs 1.27 dlrs
    net 15,000,000 vs 11,900,000
    avg shrs 9,642,403 vs 8,322,245
    loans 4.57 billion vs 3.29 billion
    deposits 6.80 billion vs 4.75 billion
    assets 7.75 billion vs 5.37 billion
 reuter
</text>]