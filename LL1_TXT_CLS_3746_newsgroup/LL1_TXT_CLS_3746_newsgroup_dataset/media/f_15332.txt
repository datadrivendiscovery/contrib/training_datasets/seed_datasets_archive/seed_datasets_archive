[<text>
<title>phoenix financial &lt;phfc&gt; board resigns</title>
<dateline>    medford, n.j., april 8 - </dateline>phoenix financial corp said that,
in connection with its previously announced reorganization, 
its board of directors resigned.
    resigning as directors, officers and employees of the
company were mary anne cossa, thomas c. amendola and james j.
o'maller, the company said. it said martin s. ackerman and joel
yonover, both attorneys, were named directors and constitute
the new board.
    in addition, the company said ackerman was named chief
executive officer.
    phoenix said it acquired a controlling interest in &lt;data
access systems&gt;, which sells and leases computer equipment.
    it said ackerman, yonover and diane ackerman were named
directors of data access.
    moreover, the company that it and data access are both in
the process of reorganizing and the securities of both
companies represent speculative and risk-related investments.
 reuter
</text>]