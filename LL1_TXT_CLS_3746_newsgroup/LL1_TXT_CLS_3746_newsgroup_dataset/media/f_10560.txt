[<text>
<title>instinet &lt;inet&gt; changes special meeting date</title>
<dateline>    new york, march 27 - </dateline>instinet corp said the special meeting
at which shareholders will vote on its proposed merger with
reuters holdings plc &lt;rtrsy&gt; will be held may 12.
    instinet said it had previously scheduled the meeting for
may 21.
    it said the record date for the meeting remains april 10.
 reuter
</text>]