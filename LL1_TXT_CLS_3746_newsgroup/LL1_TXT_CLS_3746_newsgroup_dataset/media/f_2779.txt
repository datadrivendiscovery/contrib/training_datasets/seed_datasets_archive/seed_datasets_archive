[<text>
<title>first federal brooksville &lt;ffbv&gt; sets quarterly</title>
<dateline>    brooksville, fla., march 6 -
    </dateline>qtly div four cts vs four cts prior
    pay march 31
    record march 20
    note: first federal savings and loan association of
brooksville.
 reuter
</text>]