[<text>
<title>ameritrust &lt;amtr&gt; introduces ira fund</title>
<dateline>    cleveland, ohio, april 7 - </dateline>ameritrust corp said it is
introducing a pooled investment fund for ira's and other
retirement accounts, called the collective investment
retirement fund.
    the fund will operate much like a mutual fund and will
allow initial investments as low as 250 dlrs, it said.
 reuter
</text>]