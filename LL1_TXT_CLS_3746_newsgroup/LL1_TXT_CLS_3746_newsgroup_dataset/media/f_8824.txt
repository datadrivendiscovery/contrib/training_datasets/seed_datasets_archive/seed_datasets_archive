[<text>
<title>s/p upgrades iowa-illinois gas &lt;iwg&gt; preferred</title>
<dateline>    new york, march 24 - </dateline>standard and poor's said it upgraded
31 mln dlrs of preferred and preference stock of iowa-illinois
gas and electric co.
    it raised the preferred stock to aa-minus from a-plus and
preference stock to a-plus from a and affirmed iowa-illinois'
aa senior debt, aa-minus subordinated debt and a-1-plus
commercial paper. the utility has 350 mln dlrs of debt
securities outstanding.
    s and p said the upgrade reflected the firm's overall
strong profile, high quality earnings and cash flow, and
prospects for modest preferred and preference stock usage.
 reuter
</text>]