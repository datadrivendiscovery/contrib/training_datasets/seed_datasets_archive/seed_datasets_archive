[<text>
<title>summit tax exempt bond fund &lt;sua&gt; sets payout</title>
<dateline>    new york, march 25 -
    </dateline>qtrly div 40 cts vs 40 cts prior
    pay  aug  14, 1987
    record april one, 1987
 reuter
</text>]