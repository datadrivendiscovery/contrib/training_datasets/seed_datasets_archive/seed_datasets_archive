[<text>
<title>lanesborough's subordinated notes lowered by s/p</title>
<dateline>    new york, april 8 - </dateline>standard and poor's corp said it cut to
ccc-plus from b-minus lanesborough corp's 50 mln dlrs of senior
subordinated notes.
    s and p said the downgrade reflected lanesborough's planned
purchase of ampex corp from allied-signal inc &lt;ald&gt; for 479 mln
dlrs.
    "the acquisition represents a dramatic departure from
management's stated financial policies," s and p said. it also
said that lanesborough is making a major investment in a
venture with extremely high financial risk.
    the company's implied senior debt rating is b.
 reuter
</text>]