[<text>
<title>additional ccc credit guarantees for hungary</title>
<dateline>    washington, march 9 - </dateline>the commodity credit corporation,
ccc, has authorized an additional 8.0 mln dlrs in credit
guarantees for sales of vegetable protein meals to hungary for
fiscal year 1987, the u.s. agriculture department said.
    the additional guarantees increase the vegetable protein
meal credit line to 16.0 mln dlrs and increases the cumulative
fiscal year 1987 program for agricultural products to 23.0 mln
dlrs from 15.0 mln, it said.
    the department also announced an extension of the export
period from september 30, 1987, to december 31 for sales of
vegetable protein meals.
    to be eligible for the credit guarantees all sales must be
registered before export but not later than september 30.
 reuter
</text>]