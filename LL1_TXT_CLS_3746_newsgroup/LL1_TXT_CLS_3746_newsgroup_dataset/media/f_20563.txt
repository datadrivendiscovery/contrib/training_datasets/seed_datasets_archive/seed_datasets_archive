[<text>
<title>communications systems &lt;csii.o&gt; to buy shares</title>
<dateline>    hector, minn., oct 20 - </dateline>communications systems inc said its
directors authorized the repurchase of up to 200,000 of its
total 5.2 mln common shares.
 reuter
</text>]