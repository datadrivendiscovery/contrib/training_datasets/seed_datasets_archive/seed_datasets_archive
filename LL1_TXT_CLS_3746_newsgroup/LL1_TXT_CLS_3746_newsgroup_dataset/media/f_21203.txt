[<text>
<title>tons of toys inc &lt;tons.o&gt; 1st qtr aug 31 loss</title>
<dateline>    wareham, mass., oct 19 -
    </dateline>shr loss five cts vs loss eight cts
    net loss 118,000 vs loss 87,000
    sales 1,765,000 vs 1,345,000
    avg shrs 2,370,000 vs 1,070,000
 reuter
</text>]