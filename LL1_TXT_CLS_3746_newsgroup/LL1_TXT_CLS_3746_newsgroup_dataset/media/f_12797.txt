[<text>
<title>pioneer sugar says csr takeover offer too low</title>
<dateline>    brisbane, april 3 - </dateline>&lt;pioneer sugar mills ltd&gt; said it
considered the proposed 2.20 dlrs a share cash takeover offer
announced by csr ltd &lt;csra.s&gt; on march 31 to be too low in view
of the group*hk!uiie and prospects.
    csr's bid for the 68.26 pct of pioneer's 99.80 mln issued
shares it does not already hold values the entire grop_j9culd
make an alternative share offer but has not yet announced
terms.
    pioneer recommended in a statement that shareholders retain
their stock, pending the board's response once it receives full
details of the csr offer.
 reuter
</text>]