[<text>
<title>commerzbank to acquire and float linotype</title>
<dateline>    frankfurt, march 25 - </dateline>commerzbank ag &lt;cbkg.f&gt; said it will
acquire &lt;linotype gmbh&gt;, europe's largest manufacturer of
type-setting and printing communications technology from allied
signal inc &lt;ald.n&gt; and float off the shares in the company.
    commerzbank declined to say how much it had paid for
linotype.
    linotype's group turnover in 1986 rose 15 pct to more than
500 mln marks, the bank said. the group's net return on capital
was seven pct.
 reuter
</text>]