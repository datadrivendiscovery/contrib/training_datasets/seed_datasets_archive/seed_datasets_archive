[<text>
<title>dean foods &lt;df&gt; picks chief executive</title>
<dateline>    franklin park, ill., june 1 - </dateline>dean foods co said its board
elected howard m. dean, the company's president, to succeed
kenneth j. douglas as chief executive officer of the firm.
    kenneth will replace douglas, who is also chairman, on
october one, douglas' 65th birthday. douglas will remain
chairman.
 reuter
</text>]