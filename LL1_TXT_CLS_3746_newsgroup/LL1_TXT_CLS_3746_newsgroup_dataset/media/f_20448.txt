[<text>
<title>atlantic research corp &lt;atrc.o&gt; 3rd qtr net</title>
<dateline>    alexandria, va., oct 20 -
    </dateline>shr primary 60 cts vs 42 cts
    shr diluted 57 cts vs 41 cts
    net 5,590,000 vs 3,721,000
    revs 103.5 mln vs 91.8 mln
    nine mths
    shr primary 1.55 dlrs vs 1.41 dlrs
    shr diluted 1.48 dlrs vs 1.34 dlrs
    net 14.3 mln vs 12.7 mln
    revs 300.5 mln vs 269.3 mln
 reuter
</text>]