[<text>
<title>dahlberg &lt;dahl.o&gt; completes sears&lt;s&gt; agreement</title>
<dateline>    minneapolis, oct 19 - </dateline>dahlberg inc said it completed a
previously announced agreement with sears, roebuck and co to
operate hearing aid centers in sears stores on a national
basis.
    dahlberg currently operates some 142 hearing aid centers on
a concession basis in sears stores.
    in connection with the program, the company said it granted
sears an option to buy up to 300,000 shares of dahlberg stock
at 12 dlrs a share.
 reuter
</text>]