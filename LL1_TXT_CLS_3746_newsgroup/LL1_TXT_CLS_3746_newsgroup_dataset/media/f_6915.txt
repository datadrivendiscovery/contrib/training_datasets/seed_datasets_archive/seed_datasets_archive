[<text>
<title>reagan urges revision of high tech export limits</title>
<dateline>    washington, march 18 - </dateline>president reagan has sent congress
proposals to streamline high-tech export control procedures,
the white house said.
    "these proposals are part of the president's energetic
program to enhance america's competitiveness in the world
economy," it said in a written statement.
    it said the proposals included exempting from license
requirements a number of low technology items and allowing
foreign manufacturers to re-export u.s. parts and components up
to a certain level without u.s. government re-export
authorization.
 reuter
</text>]