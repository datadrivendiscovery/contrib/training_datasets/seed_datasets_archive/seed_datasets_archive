[<text>
<title>zenith electronics &lt;ze&gt; introduces computer</title>
<dateline>    glenview, ill., june 1 - </dateline>zenith electronics corp said it
introduced a one-piece computer with 512 kilobytes of memory
priced starting at 999 dlrs.
    zenith said the new computer, called the "eazy pc", is
compatible with the pc/xt system and uses 3-1/2-inch disk
drives.
    the computer, zenith's lowest priced unit, uses an intel
8088-compatible microprocessor. its memory will be expandable
to 640k through two options expected to be available in august.
the system uses an ms-dos operating system developed by zenith
and microsoft corp &lt;msft.o&gt;.
 reuter
</text>]