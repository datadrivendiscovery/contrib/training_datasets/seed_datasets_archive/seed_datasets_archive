[<text>
<title>lone star &lt;lce&gt; to add five cement terminals</title>
<dateline>    greenwhich, conn., march 11 - </dateline>lone star industries inc said
it signed an agreement to buy or lease five import cement
terminals from hanson industries' &lt;han&gt; kaiser cement corp.
    terms of the agreement were not disclosed.
    lone star said three of the terminals are located in
alaska, one in seattle and the other in portland, ore.
 reuter
</text>]