[<text>
<title>corrected - c.o.m.b &lt;cmco&gt; to have options on amex</title>
<dateline>    new york, march 27 - </dateline>c.o.m.b inc commenced trading put and
call options on the american stock exchange, according to the
exchange.
    the company's options will trade with a ticker symbol of
&lt;coq&gt; with initial expiration months of april, may, july and
october, with position and exercise limits at 5,500 contracts
on the same side of the market, according to the exchange.
(corrects ticker symbol in headline, which is symbol of
company's options)
   
 reuter
</text>]