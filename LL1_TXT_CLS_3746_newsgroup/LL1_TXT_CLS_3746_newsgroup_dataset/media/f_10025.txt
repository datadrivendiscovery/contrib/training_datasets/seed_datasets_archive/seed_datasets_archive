[<text>
<title>first wisconsin &lt;fwb&gt; to buy minnesota bank</title>
<dateline>    milwaukee, wis., march 26 - </dateline>first wisconsin corp said it
plans to acquire shelard bancshares inc for about 25 mln dlrs
in cash, its first acquisition of a minnesota-based bank.
    first wisconsin said shelard is the holding company for two
banks with total assets of 168 mln dlrs.
    first wisconsin, which had assets at yearend of 7.1 billion
dlrs, said the shelard purchase price is about 12 times the
1986 earnings of the bank.
    it said the two shelard banks have a total of five offices
in the minneapolis-st. paul area.
 reuter
</text>]