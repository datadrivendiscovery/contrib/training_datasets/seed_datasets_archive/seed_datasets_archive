[<text>
<title>ors &lt;orsc&gt; chairman resigns after conviction</title>
<dateline>    tulsa, okla., april 8 - </dateline>robert a. alexander resigned
yesterday as chairman, president and director of ors corp
following his conviction in the united states district court in
tulsa of seven counts of mail and wire fraud, the company said.
    the company, which will appoint an interim president, said
alexander plans to appeal the conviction.
    the company said it was not charged in alexander's
indictment.
 reuter
</text>]