[<text>
<title>integrated resources sets up branches in hong kong</title>
<dateline>    hong kong, april 27 - </dateline>u.s.-based insurance and investment
broker &lt;integrated resources inc&gt; has set up two subsidiaries
in hong kong, a company statement said.
    &lt;integrated resources insurance and investment services
ltd&gt; will focus on insurance marketing and &lt;integrated
resources (hk) ltd&gt; on stock broking.
    the statement gave no financial details for the local
firms.
 reuter
</text>]