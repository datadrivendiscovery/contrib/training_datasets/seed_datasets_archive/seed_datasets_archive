[<text>
<title>comtrex systems corp &lt;comx.o&gt; year loss</title>
<dateline>    mt. laurel, n.j., june 29 - </dateline>year ended march 31
    shr loss three cts vs profit 10 cts
    net loss 58,285 vs profit 182,039
    sales 3,857,122 vs 3,188,555
    avg shrs 2,108,080 vs 1,891,250
 reuter
</text>]