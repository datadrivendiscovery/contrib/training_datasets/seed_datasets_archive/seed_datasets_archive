[<text>
<title>cyprus lowers copper price 1.25 cts to 67 cts</title>
<dateline>    denver, april 1 - </dateline>cyprus minerals company said it is
decreasing its electrolytic copper cathode price by 1.25 cents
to 67.0 cents a pound, effective immediately.
 reuter
</text>]