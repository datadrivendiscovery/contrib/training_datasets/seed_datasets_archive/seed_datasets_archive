[<text>
<title>grain ships loading at portland</title>
<dateline>    portland, april 3 - </dateline>there were five grain ships loading and
three ships were waiting to load at portland, according to the
portland merchants exchange.
 reuter
</text>]