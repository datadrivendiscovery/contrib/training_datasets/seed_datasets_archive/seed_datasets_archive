[<text>
<title>shell oil gets 104.3 mln dlr contract</title>
<dateline>    washington, march 25 - </dateline>shell oil co of houston has been
awarded a 104.3 mln dlr contract for jet fuel, the defense
logistics agency said.
 reuter
</text>]