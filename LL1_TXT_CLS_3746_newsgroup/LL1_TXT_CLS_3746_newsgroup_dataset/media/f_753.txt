[<text>
<title>westinghouse &lt;wx&gt;, matsushita &lt;mc&gt; set venture</title>
<dateline>    pittsburgh, march 2 - </dateline>westinghouse electric corp said it
agreed in principle to form a joint venture in factory
automation with matsushita electric industrial co ltd of japan.
    the company said the venture aims to combine matsushita's
experience in high-volume electronics manufacturing and
westinghouse's knowledge of computer integrated manufacturing.
    the venture will design, sell and service automated
manufacturing systems, and is expected to begin operations by
the summer, it added.
 reuter
</text>]