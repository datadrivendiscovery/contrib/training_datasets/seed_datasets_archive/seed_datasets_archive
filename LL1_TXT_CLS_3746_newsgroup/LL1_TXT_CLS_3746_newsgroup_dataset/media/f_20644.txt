[<text>
<title>best &lt;bes&gt; to buy back two mln shares</title>
<dateline>    richmond, va., oct 20 - </dateline>best products co inc said 
planned to repurchase up to two mln shares of its common for
employee benefit plans and general corporate purposes.
    best has about 27.1 mln shares outstanding.
 reuter
</text>]