[<text>
<title>energy ventures cinc &lt;engy&gt; 4th qtr net</title>
<dateline>    new york, march 31 - 
    </dateline>net profit 510,000 vs loss 5,700,000
    revs 875,000 vs 4,100,000
    year
    net profit 871,000 vs loss 4,100,000
    revs 4,700,000 vs 14.1 mln
 reuter
</text>]