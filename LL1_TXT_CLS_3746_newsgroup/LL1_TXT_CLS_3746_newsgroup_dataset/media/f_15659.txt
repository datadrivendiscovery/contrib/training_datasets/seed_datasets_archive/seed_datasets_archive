[<text>
<title>conseco &lt;cnc&gt; files to offer shares</title>
<dateline>    carmel, ind., april 9 - </dateline>conseco inc said it has filed to
offer two mln common shares through underwriters led by sears,
roebuck and co inc's &lt;s&gt; dean witter reynolds unit.
    it said the offering is expected to be made in early may,
with proceeds to be used to partly finance the proposed 275 mln
dlr acquisition of western national life insurance co or to
repay bank debt if the acquisition is not completed.
 reuter
</text>]