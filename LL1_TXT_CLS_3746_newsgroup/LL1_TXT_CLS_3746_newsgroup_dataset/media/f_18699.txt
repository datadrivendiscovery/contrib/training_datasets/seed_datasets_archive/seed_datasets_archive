[<text>
<title>tultex corp &lt;ttx&gt; 2nd qtr may 30 net</title>
<dateline>    martinsville, va., june 18 -
    </dateline>shr 17 cts vs 20 cts
    net 3,121,000 vs 3,624,000
    revs 60.2 mln vs 59.0 mln
    avg shrs 18.3 mln vs 18.2 mln
    six mths
    shr 40 cts vs 48 cts
    net 7,429,000 vs 8,743,000
    revs 124.7 mln vs 126.1 mln
    avg shrs 18.3 mln vs 18.1 mln
 reuter
</text>]