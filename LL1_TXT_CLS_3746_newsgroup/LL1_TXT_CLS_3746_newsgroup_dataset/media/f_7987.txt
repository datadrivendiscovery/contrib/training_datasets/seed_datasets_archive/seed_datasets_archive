[<text>
<title>southern home savings bank &lt;shsb&gt; year net</title>
<dateline>    pensacola, fla., march 20 -
    </dateline>shr 1.86 dlrs vs 1.85 dlrs
    net 1,923,304 vs 1,897,998
 reuter
</text>]