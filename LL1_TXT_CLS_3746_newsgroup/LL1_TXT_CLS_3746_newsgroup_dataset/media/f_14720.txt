[<text>
<title>red lion inns l.p. begins offering</title>
<dateline>    new york, april 7 - </dateline>red lion inns l.p. said it began an
initial public offering of 4.9 mln units priced at 20 dlrs a
unit.
    proceeds and a 105.87 mln dlrs mortgage loan will be used
to finance the acquisition of 10 red lion hotels, it said.
    
 reuter
</text>]