[<text>
<title>infotron &lt;infn&gt; names new chairman</title>
<dateline>    cherry hill, n.j., march 9 - </dateline>infotron systems corp said it
elected robert e. purcell chairman of the board, a position
held vacant for several years.
 reuter
</text>]