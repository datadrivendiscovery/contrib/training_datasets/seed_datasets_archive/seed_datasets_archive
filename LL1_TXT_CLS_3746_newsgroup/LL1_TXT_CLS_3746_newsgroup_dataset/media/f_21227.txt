[<text>
<title>stratus computer inc &lt;stra.o&gt; 3rd qtr net</title>
<dateline>    marlboro, mass., oct 19 -
    </dateline>shr 26 cts vs 18 cts
    net 5,281,000 vs 3,496,000
    revs 48.8 mln vs 32.1 mln
    nine mths
    shr 64 cts vs 51 cts
    net 12.9 mln vs 9,822,000
    revs 129.0 mln vs 89.2 mln
 reuter
</text>]