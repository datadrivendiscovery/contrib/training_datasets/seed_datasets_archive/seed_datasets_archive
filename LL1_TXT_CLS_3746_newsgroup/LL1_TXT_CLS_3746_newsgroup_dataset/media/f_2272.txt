[<text>
<title>gecc issues 75 mln australian dlr eurobond</title>
<dateline>    london, march 5 - </dateline>general electric credit corp is issuing a
75 mln australian dlr eurobond due april 16, 1990 paying 15 pct
and priced at 101-3/8 pct, lead manager hambros bank ltd said.
    the non-callable bond is available in denominations of
1,000 australian dlrs and will be listed in luxembourg. the
selling concession is one pct while management and underwriting
combined pays 1/2 pct.
    the payment date is april 16.
 reuter
</text>]