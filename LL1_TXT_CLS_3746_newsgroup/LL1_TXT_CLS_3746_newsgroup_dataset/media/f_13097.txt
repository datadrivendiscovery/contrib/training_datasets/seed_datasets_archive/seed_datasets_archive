[<text>
<title>u.s. durum wheat bonus bid to algeria accepted</title>
<dateline>    washington, april 3 - </dateline>the u.s. agriculture department said
it has accepted a bid for an export bonus to cover the sale of
18,000 tonnes of u.s. durum wheat to algeria.
    a bonus of 43.25 dlrs per tonne was awarded to cam usa inc
on the shipment scheduled for june 20-30, melvin sims, usda
general sales manager, said.
    an additional 228,000 tonnes of durum wheat are available
to algeria under the department's export enhancement program,
sims said.
 reuter
</text>]