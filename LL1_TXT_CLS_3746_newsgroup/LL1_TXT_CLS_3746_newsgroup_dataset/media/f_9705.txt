[<text>
<title>vietnam's army ordered to grow more food</title>
<dateline>    bangkok, march 26 - </dateline>vietnam has ordered its army to grow
more food to ease shortages and meet economic recovery goals
set for 1990.
    the army newspaper quan doi nhan dan, monitored here, said
soldiers must work harder to care for rice, vegetables and
other crops endangered by the present unusually hot weather.
    the paper said the 1.6-mln strong regular army contributed
less than one pct to the nation's 18.2 mln tonne food output.
    north vietnam has set a 1990 food target of 23 to 24 mln
tonnes.
 reuter
</text>]