[<text>
<title>ccc accepts wheat bid for w africa countries</title>
<dateline>    washington, march 16 - </dateline>the commodity credit corportion,
ccc, has accepted a bid for an export bonus to cover the sale
of 15,000 tonnes of u.s. wheat to west african countries, the
u.s. agriculture department said.
    the dark northern spring wheat is for shipment may 15-june
15, 1987.
    the bonus of 40.05 dlrs per tonne was made to peavey
company and will be paid in the form of commodities from the
ccc inventory, it said.
    an additional 315,500 tonnes of wheat are still available
to west african countries under the export enhancement program
initiative announced october 30, 1986, it said.
 reuter
</text>]