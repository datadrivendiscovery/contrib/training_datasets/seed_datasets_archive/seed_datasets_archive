[<text>
<title>sec continues market manipulation investigation</title>
<dateline>    new york, march 19 - </dateline>the securities and exchange commission
is continuing an investigation of the practice of "parking"
stocks, said richard murphy, a lawyer with the sec enforcement
division.
    earlier, the sec settled civil charges against boyd
jefferies and jefferies group inc, which allege market
manipulation and the parking of stocks, a practice where stock
is held by one person or firm while it is actually under the
control of someone else.
    murphy told reporters others are being investigated for
parking stocks.
 reuter
</text>]