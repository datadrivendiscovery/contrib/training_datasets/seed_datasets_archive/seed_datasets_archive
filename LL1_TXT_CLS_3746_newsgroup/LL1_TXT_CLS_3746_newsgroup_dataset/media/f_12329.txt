[<text>
<title>moore &lt;mcl&gt; to amend rights of preference shares</title>
<dateline>    toronto, april 1 - </dateline>moore corp ltd said it proposed
amendments to a special resolution seeking shareholder approval
for authority to issue preference shares, so that the shares
will be non-voting except in the event of dividend non-payment.
    it said it proposed the amendments in response to
shareholder concerns that under the existing resolution, voting
rights could be attached to the preference shares that are
disproportionate to common share voting rights.
    moore corp said it has no immediate plans to issue
preference shares under the proposed resolution.
    the resolutions will be considered at the april 9 annual
and special meeting, the company said.
 reuter
</text>]