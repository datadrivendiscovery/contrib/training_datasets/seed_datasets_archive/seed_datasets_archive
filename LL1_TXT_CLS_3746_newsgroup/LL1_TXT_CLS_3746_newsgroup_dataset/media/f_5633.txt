[<text>
<title>bellsouth &lt;bls&gt; expands mobile phone service</title>
<dateline>    jacksonville, fla., march 16 - </dateline>bellsouth corp's bellsouth
mobility inc unit said its mobile phone customers can now
choose their long-distance carrier.
    previously, all mobile calls were routed through american
telephone and telegraph co's long distance service.
 reuter
</text>]