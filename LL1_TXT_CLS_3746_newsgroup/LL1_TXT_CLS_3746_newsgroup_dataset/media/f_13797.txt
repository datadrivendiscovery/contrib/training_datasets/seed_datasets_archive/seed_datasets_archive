[<text>
<title>twistee treat &lt;twst&gt; sets store development</title>
<dateline>    north fort myers, fla., april 7 - </dateline>twistee treat corp said
it has signed letters of intent for four investor group to
develop a total of 585 additional twistee treat ice cream
stores in western new york, california, north carolina, south
carolina and texas.
    the company said the agreements call for the opening of at
least 83 locations in the next 12 months and completion of
remaining stores by 1989.
    it said it expects area development fee income of over two
mln dlrs in cash over the next three years.
 reuter
</text>]