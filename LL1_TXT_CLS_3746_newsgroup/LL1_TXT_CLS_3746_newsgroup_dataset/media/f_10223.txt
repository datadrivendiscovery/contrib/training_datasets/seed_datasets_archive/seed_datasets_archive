[<text>
<title>fdic says oklahoma bank become 51st to fail</title>
<dateline>    washington, march 26 - </dateline>the federal deposit insurance corp
said the first state bank in billings, okla, was closed by
banking regulators, bringing the total number of bank failures
this year to 51.
    fdic said 9.4 mln dlrs of first state's deposits were
assumed by first national trust co of perry, in perry oklahoma.
    first national also will assume eight mln dlrs in assets.
the fdic said it will advance 1.5 mln dlrs to facilitate the
transaction and retain 2.1 mln dlrs in assets.
 reuter
</text>]