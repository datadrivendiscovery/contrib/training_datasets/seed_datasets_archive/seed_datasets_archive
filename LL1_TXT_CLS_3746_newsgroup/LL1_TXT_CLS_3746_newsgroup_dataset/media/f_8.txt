[<text>
<title>usx &lt;x&gt; debt dowgraded by moody's</title>
<dateline>    new york, feb 26 - </dateline>moody's investors service inc said it
lowered the debt and preferred stock ratings of usx corp and
its units. about seven billion dlrs of securities is affected.
    moody's said marathon oil co's recent establishment of up
to one billion dlrs in production payment facilities on its
prolific yates field has significant negative implications for
usx's unsecured creditors.
    the company appears to have positioned its steel segment
for a return to profit by late 1987, moody's added.
    ratings lowered include those on usx's senior debt to ba-1
from baa-3.
 reuter
</text>]