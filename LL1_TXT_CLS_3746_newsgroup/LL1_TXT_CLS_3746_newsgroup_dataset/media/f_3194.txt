[<text>
<title>servicemaster &lt;svm&gt; sets quarter distribution</title>
<dateline>    downers grove, ill., march 9 - </dateline>servicemaster l.p. approved
a second quarter cash distribution of 58 cts, payable april 10,
record march 23.
    previously, servicemaster said it would pay in 1987 an
indicated cash distribution of 1.50 dlrs a share, including  95
cts a share which would be paid before april 15, 1987.
    in other action, the company set may eight as the date of
its annual shareholders' meeting.
 reuter
</text>]