[<text>
<title>carolina power and light co &lt;cpl&gt; sets payout</title>
<dateline>    raleigh, n.c., march 18 -
    </dateline>qtly div 69 cts vs 69 cts prior
    pay may one
    record april 10
 reuter
</text>]