[<text>
<title>macgregor sporting goods inc &lt;mgs&gt;2nd qtr jan 31</title>
<dateline>    east rutherford, n.j., march 17 -
    </dateline>shr one cent vs nine cts
    net 51,057 vs 554,878
    sales 24.5 mln vs 21.3 mln
    first half
    shr 15 cts vs five cts
    net 1,026,479 vs 313,676
    sales 48.4 mln vs 37.4 mln
    note: net includes gains of 28,000 dlrs, or one cent a
share, vs 61,000 dlrs, or seven cts a share, in quarter and
453,000 dlrs, or seven cts a share, vs 61,000 dlrs, or one cent
a share, in half from tax loss carryforward
 reuter
</text>]