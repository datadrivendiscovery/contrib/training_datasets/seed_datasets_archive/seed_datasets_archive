[<text>
<title>termiflex corp &lt;tflx.o&gt; 1st qtr sept 30 net</title>
<dateline>    merrimack, n.h., oct 19 -
    </dateline>shr five cts vs seven cts
    net 64,652 vs 96,157
    sales 1,205,321 vs 1,499,591
    note: backlog three mln dlrs vs 2,600,000 as of june 30,
1987.
 reuter
</text>]