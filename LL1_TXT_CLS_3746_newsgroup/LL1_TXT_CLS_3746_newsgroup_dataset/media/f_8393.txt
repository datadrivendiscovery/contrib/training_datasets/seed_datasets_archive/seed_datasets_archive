[<text>
<title>pre-paid legal &lt;pd&gt; enters washington market</title>
<dateline>    ada, okla., march 23 - </dateline>pre-paid legal services inc said it
will start marketing its pre-paid legal services contract in
the state of washington today.
    it said it is now doing business in 23 states.
 reuter
</text>]