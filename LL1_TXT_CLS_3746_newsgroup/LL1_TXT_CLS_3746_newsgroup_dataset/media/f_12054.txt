[<text>
<title>wall street stocks/purolator courier &lt;pcc&gt;</title>
<dateline>    new york, april 1 - </dateline>purolator courier corp stock jumped
5-3/8 on a 40 dlr per share takeover offer from emery air
freight corp &lt;eaf&gt;, traders said.
    purolator was trading at 40-1/4, 1/4 above the offer price.
the emery offer tops a 35 dlr per share buyout agreement e.f.
hutton lbo inc reached with purolator february 27.
    that offer was to have expired today. neither hutton nor
purolator had any immediate comment.
    "there's probably some speculation out there that there
might be another offer," said one analyst.
 reuter
</text>]