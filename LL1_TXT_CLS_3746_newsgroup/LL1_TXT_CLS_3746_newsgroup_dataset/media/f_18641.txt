[<text>
<title>c.r.i. insured mortgage investments &lt;crm&gt; payout</title>
<dateline>    rockville, md., june 18 -
    </dateline>mthly div 16-1/2 cts vs 16-1/2 cts prior
    pay aug 15
    record june 30
    note: c.r.i. insured mortgage investments lp.
 reuter
</text>]