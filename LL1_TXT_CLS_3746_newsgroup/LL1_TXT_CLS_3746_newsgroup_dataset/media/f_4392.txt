[<text>
<title>u.s. soybean marketing loan not needed - lyng</title>
<dateline>    washington, march 12 - </dateline>u.s. agriculture secretary richard
lyng said a marketing loan for soybeans would serve no present
purpose because the u.s. price is not above the world price.
    asked in an interview if it was time to consider a
marketing loan for soybeans, lyng said, "i don't think so. i
don't think the world price is lower than our price anyway."
    however, the usda secretary said that if current conditions
of surplus production persisted, it might be appropriate to
consider a marketing loan.
    "i suppose that under that condition there is a danger our
exports will continue to drop and that the government will
continue to accumulate large stocks of soybeans," he said. "it
might be (worth contemplating a marketing loan), if there were
a world market that was lower than our market."
 reuter
</text>]