[<text>
<title>gateway communications inc &lt;gway&gt; 1st qtr net</title>
<dateline>    irvine, calif., april 9 -
    </dateline>shr 14 cts vs six cts
    net 653,561 vs 251,955
    revs 4,143,056 vs 2,199,238
 reuter
</text>]