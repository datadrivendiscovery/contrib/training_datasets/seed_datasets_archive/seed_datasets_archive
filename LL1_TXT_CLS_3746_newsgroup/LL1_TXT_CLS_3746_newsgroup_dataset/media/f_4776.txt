[<text>
<title>koninklijke nederlandse papierfabrieken &lt;knpn.as&gt;</title>
<dateline>    maastricht, netherlands, march 13 -
    </dateline>net 1986 profit 132.6 mln guilders vs 117.3 mln
    turnover 1.6 billion guilders vs same
    earnings per share 16.00 guilders vs 15.80 on capital
expanded by 11 pct to 8.21 mln outstanding shares.
    proposed dividend per share 5.50 guilders vs 5.00
    note - full company name is koninklijke nederlandse
papierfabrieken nv.
 reuter
</text>]