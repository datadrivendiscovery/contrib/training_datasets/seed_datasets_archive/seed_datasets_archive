[<text>
<title>usda seeking comments on 1988 farm programs</title>
<dateline>    washington, april 24 - </dateline>the u.s. agriculture department is
seeking comments on common provisions of the 1988 wheat,
feedgrains, cotton and rice programs.
    it said many program provisions are common to all the
commodity programs and decisions made in regard to one will
likely apply to other program crops.
    it asked for specific comments on the percentage reduction
for acreage limitation requirements under the wheat program,
the loan and purchase level, and whether a marketing loan, the
inventory reduction program and related provisions
should be implemented.
    the percentage acreage reduction of between 20 and 30 pct
must be announced no later than june 1, 1987 for wheat, it
said.
 reuter
</text>]