[<text>
<title>presidential airways &lt;pair&gt; pact approved</title>
<dateline>    washington, feb 26 - </dateline>presidential airways inc said its
joint marketing and services agreement with texas air corp's
&lt;txn&gt; continental airlines unit was approved by the u.s.
department of justice.
    according to the agreement, presidential airways will
operate scheduled service under the name "continental express."
the company, however, will remain independent.
   
 reuter
</text>]