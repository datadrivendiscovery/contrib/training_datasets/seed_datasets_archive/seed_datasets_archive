[<text>
<title>adsteam to acquire 49 pct of u.k. property company</title>
<dateline>    adelaide, march 31 - </dateline>the adelaide steamship co ltd &lt;adsa.s&gt;
(adsteam) said it will subscribe to 30 mln shares in listed
british property developer, &lt;markheath securities plc&gt;, at 60p
each, subject to shareholder approval.
    the subscription, expected to take place in may, will give
adsteam 49 pct of markheath, adsteam said in a statement.
    adsteam's managing director john spalvins will become
chairman of markheath and two other adsteam nominees will join
its board. "we hope that in time markheath will become a
significant property and industrial company in the same style
as adsteam," spalvins said in the statement.
 reuter
</text>]