[<text type="unproc">
eastgroup properties &lt;egp&gt; dividend
    jackson, miss., may 20
    qtly div 65 cts vs 65 cts prior
    payable april 22
    record april 10
    note:company paid 30 cts special dividend along with prior
quarter's dividend
 reuter


</text>]