[<text>
<title>treasury's baker - g-7 oppose further dlr drop</title>
<dateline>    washington, april 24 - </dateline>treasury secretary james baker said
the group of seven countries, in seeking to foster stability in
exchange markets, believe a further decline in the value of the
dollar would be counterproductive.
     in answer to questions by a business group, baker said
that for one thing further reductions could make it
economically difficult for the surplus countries to grow,
thereby making it difficult for them to purchase overseas
goods. in addition, baker said he was opposed to the u.s.
selling a yen denominated bond arguing that such a move might
send the wrong signal to markets.
 reuter
</text>]