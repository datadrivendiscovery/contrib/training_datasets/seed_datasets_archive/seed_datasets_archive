[<text>
<title>medical &lt;mdci&gt; gets humana &lt;hum&gt; contract</title>
<dateline>    farmingdale, n.y., march 24 - </dateline>medical action industries inc
said it got a contract from humana inc &lt;hum&gt; to supply humana
with lap sponges, used mainly during surgery to cover exposed
internal organs.
    medical said the contract could provide reveneus of more
than one mln dlrs over a two-year period. for the fiscal year
ended march 31, medical reported revenues of 14.8 mln dlrs.
 reuter
</text>]