[<text>
<title>wal-mart stores inc &lt;wmt&gt; 4th qtr jan 31 net</title>
<dateline>    bentonville, ark., march 4 -
    </dateline>shr 65 cts vs 47 cts
    net 184.3 mln vs 133.1 mln
    sales 3.85 billion vs 2.77 billion
    year
    shr 1.59 dlrs vs 1.16 dlrs
    net 450.1 mln vs 327.5 mln
    sales 11.91 billion vs 8.45 billion
 reuter
</text>]