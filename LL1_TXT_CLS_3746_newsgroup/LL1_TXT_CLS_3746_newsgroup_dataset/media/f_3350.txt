[<text>
<title>oshkosh truck gets 64.0 mln dlr army deal</title>
<dateline>    washington, march 9 - </dateline>oshkosh truck corp (otrkb) has
received a 64.0 mln dlr contract for work on 432 diesel engine
drive mk-48 series vehicles, the army said.
 reuter
</text>]