[<text>
<title>dataflex corp &lt;dflx.o&gt; 4th qtr march 31 net</title>
<dateline>    edison, n.j., june 1 -
    </dateline>shr 11 cts vs eight cts
    net 248,000 vs 155,000
    revs 4,385,000 vs 2,487,000
    year
    shr 36 cts vs 12 cts
    net 720,000 vs 220,000
    revs 15.2 mln vs 9,253,000
    note: share adjusted for 10 pct stock dividend in april
1987.
 reuter
</text>]