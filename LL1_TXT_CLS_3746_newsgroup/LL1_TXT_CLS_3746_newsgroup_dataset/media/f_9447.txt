[<text>
<title>&lt;atlantis group inc&gt; to offer shares, notes</title>
<dateline>    miami, march 25 - </dateline>atlantis group inc said it has filed for
an initial public offering of 1,500,000 common shares and a 50
mln dlr offering of subordinated notes due 1997.
    the company said robinson-humphrey co inc and sutro and co
inc will manage the share offering and american express co's
&lt;axp&gt; shearson lehman brothers inc and robinson-humphrey the
debt offering.  proceeds will be used to reduce debt and seek
acquisitions.
    atlantis is involved in plastics and furniture
manufacturing and in property-casualty insurance.
 reuter
</text>]