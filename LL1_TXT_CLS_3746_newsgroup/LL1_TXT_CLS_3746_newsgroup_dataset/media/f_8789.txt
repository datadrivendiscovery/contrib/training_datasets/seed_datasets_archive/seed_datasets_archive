[<text>
<title>lyphomed &lt;lmed&gt; to market wound dressing</title>
<dateline>    rosemont, ill., march 24 - </dateline>lyphomed inc said it agreed to
market an antiseptic transparent wound dressing made by hercon
laboratories, a subsidiary of health-chem corp &lt;hch&gt;.
    the dressing has been approved by the food and drug
administration for use as an intravenous catheter securement
device and as a protective dressing for wounds, it said.
 reuter
</text>]