[<text>
<title>biogen &lt;bgnf&gt; gets patent from european office</title>
<dateline>    cambridge, mass., march 26 - </dateline>biogen inc said the european
patent office granted it a patent covering certain proteins
used to produce a hepatitis b vaccine through genetic
engineering techniques.
    robert gottlieb, biogen spokesman, said the company has
licensed the vaccine on a nonexclusive basis to &lt;wellcome plc&gt;,
the british pharmaceutical firm, and is discussing licensing
with other companies.
    biogen said the patent gives it the right to exclude others
from marketing hepatitis b vaccine in the 11 member countries
of the european patent convention.
    gottlieb said the company has also filed a patent in other
markets, including the u.s. the vaccine is in clinical tests.
    patents in the biotechnology field are particularly
important as the company with an exclusive patent can reap
large rewards. recently many of the products of genetic
engineering have become the target of patent lawsuits.
    merck and co inc &lt;mrk&gt; already sells a genetically
engineered hepatitis b vaccine in the u.s. called recombivax
hb. a subsidiary of &lt;smithkline beckman corp&gt;, smithkline
biologicals, based in belgium, is selling a hepatitis b
vaccine, called engerix-b, in belgium.
    a smithkline spokesman said the vaccine has also been
formally approved in switzerland and luxembourg and has been
authorized for market in a number of far east countries.
    hepatitis b is a serious liver infection common in many
parts of africa and southeast asia where about five pct to 15
pct of the population carry the virus. in the u.s. about
200,000 new cases occur each year.
    last december the european patent ofice rejected biogen's
patent for alpha-interferon, which biogen said it will appeal
once it receives a formal written opinion from the office.
   
 reuter
</text>]