[<text>
<title>nasd names new board member</title>
<dateline>    washington, march 12 - </dateline>the &lt;national association of
securities dealers&gt; said it has named jaguar cars inc president
graham w. whitehead as one of its three governors-at-large
representing companies listed on the nasdaq system.
    jaguar cars is the u.s. marketing arm of jaguar plc
&lt;jagry&gt;, which was nasdaq's third-most-actively traded issue in
1986.
 reuter
</text>]