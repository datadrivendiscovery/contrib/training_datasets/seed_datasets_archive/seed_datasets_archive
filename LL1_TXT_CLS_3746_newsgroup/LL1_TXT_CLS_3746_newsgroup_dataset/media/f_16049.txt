[<text>
<title>s/p downgrades zenith electronic &lt;ze&gt;</title>
<dateline>    new york, april 9 - </dateline>standard and poor's said it lowered its
rating on 190 mln dlrs of zenith electronic corp debt because
of continued poor earnings results and increased debt leverage.
    among the rating changes, the company's senior debt was
lowered to bb-plus from bbb-plus.
 reuter
</text>]