[<text>
<title>the chubb corp &lt;cb&gt; sets qtrly payout</title>
<dateline>    warren, n.j., march 6 -
    </dateline>qtrly 42 cts vs 42 cts prior
    pay april 7
    record march 20
 reuter
</text>]