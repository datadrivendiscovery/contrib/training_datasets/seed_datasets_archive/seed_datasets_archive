[<text>
<title>newmont gold &lt;ngc&gt; offers shares</title>
<dateline>    new york, april 3 - </dateline>newmont gold co is offering five mln
common shares at 31 dlrs apiece, co-managing underwriters
kidder, peabody and co, lazard freres and co and salomon
brothers inc said.
    all shares are being issued and offered by the company. the
underwriters have been granted an option to buy up to 500,000
more shares to cover over-allotments.
    proceeds will be used to repay long-term debt owed to
newmont mining corp &lt;nem&gt;, which will own over 90 pct of
newmont gold after the offer. proceeds will also be used for
general purposes, the gold production company said.
 reuter
</text>]