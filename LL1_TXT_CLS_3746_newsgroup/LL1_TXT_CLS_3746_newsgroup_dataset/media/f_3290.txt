[<text>
<title>financial corp &lt;fin&gt; unit completes purchase</title>
<dateline>    irvine, calif., march 9 - </dateline>financial corp of america's
american savings and loan association said it completed the
previously announced purchase of 16 retail savings branches
from great western financial corp's &lt;gwf&gt; great western
savings.
    american savings said the purchases boost its deposits by
about 550 mln dlrs, but do not affect its asset base.
 reuter
</text>]