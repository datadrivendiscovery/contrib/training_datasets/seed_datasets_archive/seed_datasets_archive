[<text>
<title>bangladesh authorized to buy pl 480 cotton-usda</title>
<dateline>    washington, april 29 - </dateline>bangladesh has been authorized to
purchase about 32,000 bales (480 lbs) of u.s. cotton under an
existing pl 480 agreement, the u.s. agriculture department
said.
    it may buy the cotton, valued at 10.0 mln dlrs, between may
6 and august 15, 1987 and ship it by september 15, the
department said.
 reuter
</text>]