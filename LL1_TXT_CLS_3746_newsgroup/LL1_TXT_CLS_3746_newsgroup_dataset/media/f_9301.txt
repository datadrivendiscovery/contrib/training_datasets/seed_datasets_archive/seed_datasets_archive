[<text>
<title>vicorp restaurants inc &lt;vres&gt; 1st qtr feb 15 net</title>
<dateline>    denver, march 25 -
    </dateline>shr profit 1.17 dlrs vs loss 12 cts
    net profit 11.3 mln vs loss 1,038,000
    revs 104.6 mln vs 128.7 mln
    note: current year net includes gain 9,500,000 dlrs from
the sale of its specialty restaurants unit, charge 1,200,000
dlrs from addition to insurance reserves, 4,600,000 dlr tax
credit and 660,000 dlr charge from debt repayment.
 reuter
</text>]