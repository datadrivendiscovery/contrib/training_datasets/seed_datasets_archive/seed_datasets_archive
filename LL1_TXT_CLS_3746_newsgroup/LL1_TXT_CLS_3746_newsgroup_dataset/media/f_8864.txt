[<text>
<title>resorts int'l &lt;rt.a&gt; to make statement</title>
<dateline>    new york, march 24 - </dateline>a resorts international inc spokesman
said the company will make a statement later today regarding
its class a and class b &lt;rt.b&gt; common stock.
    the american stock exchange halted trading in the shares
this morning because of the pending announcement. the a shares
last traded at 58-3/4 and the b shares at 130-1/2, the exchange
said.
 reuter
</text>]