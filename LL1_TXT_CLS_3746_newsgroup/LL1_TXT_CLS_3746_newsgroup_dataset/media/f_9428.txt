[<text>
<title>rcm technologies inc &lt;rcmt&gt; 1st qtr jan 31 loss</title>
<dateline>    camden, n.j., march 25 -
    </dateline>shr loss one ct vs loss one ct
    net loss 89,844 vs loss 85,731
    revs 3,384,726 vs 4,646,285
 reuter
</text>]