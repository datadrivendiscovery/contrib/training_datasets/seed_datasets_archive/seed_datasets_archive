[<text>
<title>cftc studying transfering surveillance costs</title>
<dateline>    washington, march 23 - </dateline>the commodity futures trading
commission, cftc, and the white house budget office are
exploring the possibility of transfering the three mln dlr
annual cost of the commission's surveillance functions to the
private sector, cftc chairman susan phillips said.
    phillips told the house appropriations agriculture
subcommittee that the office of management and budget, omb, had
asked cftc to study whether the market surveillance
responsibilities could be paid for with user fees.
    cftc officials said the two agencies were studying whether
commodity exchanges, futures commission merchants or market
users should bear the cost of the surveillance program.
    phillips told reuters that cftc and omb were expected to
complete the study by mid-april.
 reuter
</text>]