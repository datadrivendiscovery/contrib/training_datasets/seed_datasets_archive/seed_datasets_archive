[<text>
<title>grumman &lt;gq&gt; unit wins mcdonnell douglas order</title>
<dateline>    long beach, calif., april 1 - </dateline>mcdonnell douglas corp &lt;md&gt;
said it awarded a contract valued at 28 mln dlrs to a unit of
grumman corp to develop flight control surfaces for the new
u.s. air force c-17 transport.
    grumman aerostructures of bethpage, n.y., will design,
develop and build the rudders, elevators and other parts for
the transports.
 reuter
</text>]