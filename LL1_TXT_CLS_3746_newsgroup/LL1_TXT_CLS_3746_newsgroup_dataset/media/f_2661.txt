[<text>
<title>equimark &lt;eqk&gt; partners to be dissolved</title>
<dateline>    pittsburgh, march 6 - </dateline>equimark corp said &lt;equimark
purchasing partners&gt; will be dissolved, effective february 24.
    equimark, a bank holding company with is the limited
partership's general partner, said the partnership's
dissolution will proceed in accordance with the partnership
agreement and the assets are being distributed to partners on
a pro rata basis.
    the assets primarily consist of 27.6 pct of equimark's
stock, it added, saying no individual partner will own in
excess of 10 pct of the stock after the distribution.
    equimark said the partnership was formed in october 1984 as
one of the principal components in the corporation's
recapitalization plan.
    on june 28, 1985, the partnership purchased 9,328,358
equimark common shares from chase manhattan corp for 25 mln
dlrs. the purchase was funded with the proceeds of cash capital
contributions made by the partners.
 reuter
</text>]