[<text>
<title>india reported buying two white sugar cargoes</title>
<dateline>    london, march 25 - </dateline>india is reported to have bought two
white sugar cargoes for april/may shipment at its tender today
near 227 dlrs a tonne cost and freight and could be seeking a
third cargo, traders said.
    a british operator is believed to have sold one of the
cargoes, while an austrian concern is thought to have featured
in the second cargo sale, they said.
 reuter
</text>]