[<text>
<title>shearson lehman names new managing director</title>
<dateline>    new york, feb 26 - </dateline>shearson lehman brothers, a unit of
american express co &lt;axp&gt;, said robert stearns has joined the
company as managing director of its merger and acquisition
department.
    shearson said stearns formerly was part of merrill lynch
pierce, fenner and smith inc's &lt;mer&gt; merger and acquisitions
department.
 reuter
</text>]