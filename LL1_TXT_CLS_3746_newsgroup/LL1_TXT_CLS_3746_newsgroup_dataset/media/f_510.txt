[<text>
<title>r.j.r. nabisco unit forms overseeing committee</title>
<dateline>    winston-salem, n.c., march 2 - </dateline>r.j. reynolds tobacco co, a
unit of r.j.r. nabisco inc &lt;rjr&gt;, said it has formed an
executive management committee to oversee the company's
worldwide tobacco operations.
    reynolds tobacco said the committee's members will be
senior managers of reynolds tobacco co, r.j. reynolds tobacco
usa, and r.j. reynolds international inc.
    r.j.r. nabisco is reportedly attempting to form a master
limited partnership out of its tobacco unit, part of which will
be sold to the public.
 reuter
</text>]