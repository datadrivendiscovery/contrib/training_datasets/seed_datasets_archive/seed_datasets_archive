[<text>
<title>international proteins &lt;pro&gt; replaces credit</title>
<dateline>    fairfield, n.j., march 31 - </dateline>international proteins corp
said it replaced its existing credit line with a new 10 mln dlr
facility consisting of a three mln dlr term loan and a
revolving credit line provided by citicorp &lt;cci&gt;.
    the company said the new credit line significantly improves
the company's working capital position and provides the company
with additional funds to finance growth of its domestic
operations.
 reuter
</text>]