[<text>
<title>capital bancorp &lt;capb&gt; sees gain on unit sale</title>
<dateline>    boston, march 31 - </dateline>capitol bancorp said it has sold its 80
pct interest in cap mortgage co inc for 3.1 mln dlrs, adding
this is expected to result in an after tax gain of about
900,000 dlrs to be reported in the first quarter.
    capitol bancorp said the cap mortgage interest was sold to
michael m. bronstein, president of cap mortgage, and robert
fox, president of fox properties inc. bronstein already held
the other 20 pct of cap mortgage's stock.
 reuter
</text>]