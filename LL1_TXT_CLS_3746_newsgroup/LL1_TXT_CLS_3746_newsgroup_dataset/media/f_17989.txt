[<text>
<title>comp-u-card &lt;cucd.o&gt; plans name change</title>
<dateline>    stamford, conn., june 2 - </dateline>comp-u-card international inc
said it intends to change its name to cuc international inc.
    it said shareholders will be asked to approve the new name
at the annual meeting on june 16
 reuter
</text>]