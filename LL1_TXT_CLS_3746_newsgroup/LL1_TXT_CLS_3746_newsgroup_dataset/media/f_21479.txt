[<text>
<title>emulex corp &lt;emlx.o&gt; 1st qtr sept 27 net</title>
<dateline>    costa mesa, calif., oct 19 -
    </dateline>shr 13 cts vs 12 cts
    net 1,612,000 vs 1,571,000
    revs 28.8 mln vs 25.0 mln
    avg shrs 12.8 mln vs 13.3 mln
 reuter
</text>]