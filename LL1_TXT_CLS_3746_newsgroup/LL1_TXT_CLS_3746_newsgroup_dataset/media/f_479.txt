[<text>
<title>quaker oats &lt;oat&gt; files shelf registration</title>
<dateline>    chicago, march 2 - </dateline>quaker oats co said it filed a shelf
registration with the securities and exchange commission
covering up to 250 mln dlrs in debt securities.
    the company said it may offer the securites in one or more
issues, from time to time, over the next two years.
    proceeds will be used to repay short term debt issued in
connection with quaker oats' recent acquisitions and for other
corporate purposes, it said.
    underwriters may include salomon brothers inc and goldman,
sachs and co.
 reuter
</text>]