[<text>
<title>elxsi ltd &lt;elxsf&gt; 4th qtr loss</title>
<dateline>    san jose, calif., march 5 -
    </dateline>shr loss four cts vs loss 34 cts
    net loss 2,922,000 vs loss 19.9 mln
    revs 4,071,000 vs 8,012,000
    year
    shr loss 23 cts vs loss 79 cts
    net loss 17.3 mln vs loss 46.2 mln
    revs 22.4 mln vs 28.6 mln
 reuter
</text>]