[<text>
<title>ford &lt;f&gt; awards 2.3 mln dlr contract</title>
<dateline>    dearborn, mich., march 31 - </dateline>ford motor co said it has
awarded &lt;cimarron express inc&gt;, an ohio-based minority owned
and operated trucking company, a 2.3 mln dlr regional
transportation contract.
    ford said that under the contract cimarron will transport
automotive parts from more than 50 supplier firms in eight
southern states to ford's wixom assembly plant in michigan.
 reuter
</text>]