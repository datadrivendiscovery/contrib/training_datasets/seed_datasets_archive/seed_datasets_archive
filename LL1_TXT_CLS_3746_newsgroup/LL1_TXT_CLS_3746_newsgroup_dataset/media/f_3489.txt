[<text>
<title>u.s. banks to get approval for securities business</title>
<dateline>    tokyo, march 11 - </dateline>u.s. commercial banks are expected to
receive finance ministry approval in late april to operate
securities subsidiaries in japan, u.s. bank officials said.
    a senior official at one of four prospective banks said the
ministry told his bank it would give approval as long as the
parent firm holds no more than 50 pct of the capital.
    "we expect the ministry to give us permission by the end of
april," he said.
    &lt;j.p. morgan and co&gt;, bankers trust new york corp &lt;bt.n&gt;,
manufacturers hanover corp &lt;mhc.n&gt; and chemical new york corp
&lt;fnbf.n&gt; have asked for securities business licenses.
    ministry officials declined to say when they would give
formal approval, but said they were working on the issue.
    approval would pave the way for u.s. commercial banks to
underwrite and trade equities in japan under their own names.
    citicorp &lt;cci.n&gt; and chase manhattan corp &lt;cmb.n&gt; have
already entered the japanese securities market by acquiring
u.k. securities houses already operating in japan. citicorp
took over &lt;vickers de costa ltd&gt; and chase bought &lt;laurie
milbank and co&gt;.
    bankers did not know if all the banks would get licenses,
but said j.p. morgan probably would as it was first to ask.
 reuter
</text>]