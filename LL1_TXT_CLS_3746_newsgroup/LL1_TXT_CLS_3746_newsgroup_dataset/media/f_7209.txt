[<text>
<title>public service co of n.h.&lt;pnh&gt; omits dividend</title>
<dateline>    manchester, n.h., march 19 - </dateline>public service co of new
hampshire said its board voted to omit the quarterly dividend
which would have been paid on may 15.
    the company said the omission is the thirteenth consecutive
dividend omission.
 reuter
</text>]