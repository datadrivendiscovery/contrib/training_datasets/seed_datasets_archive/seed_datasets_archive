[<text>
<title>donegal group inc &lt;dgic&gt; year net</title>
<dateline>    marietta, pa., march 6 -
    </dateline>shr six cts
    net 155,764
    revs 6,506,792
    note: company formed in august 1986.
 reuter
</text>]