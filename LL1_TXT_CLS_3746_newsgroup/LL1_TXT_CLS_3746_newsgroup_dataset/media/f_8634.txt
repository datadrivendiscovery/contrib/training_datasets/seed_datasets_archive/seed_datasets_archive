[<text type="unproc">
kyushu electric power issues yen bonds london, march 24 -
kyushu electric power co inc is issuing a 20 billion yen bond
due april 22, 1994 carrying a coupon of 4-3/4 pct and priced at
101-5/8 pct, said nomura securities co ltd as lead manager.
    the securities are available in denominations of one mln
yen each and will be listed on the luxembourg stock exchange.
payment date is april 22, 1987.
    there is a 1-1/4 pct selling concession and combined
management and underwriting fees total 5/8 pct.
 reuter


</text>]