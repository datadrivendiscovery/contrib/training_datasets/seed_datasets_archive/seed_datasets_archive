[<text>
<title>bsd medical &lt;bsdm.o&gt; sells securities privately</title>
<dateline>    salt lake city, june 18 - </dateline>bsd medical corp said it has
received 1,400,000 dlrs of equity capital from the private sale
of securities to two investment partnerships and director alan
l. himber.
 reuter
</text>]