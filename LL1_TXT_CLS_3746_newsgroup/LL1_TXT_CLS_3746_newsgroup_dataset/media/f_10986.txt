[<text>
<title>san/bar &lt;sbar&gt; appoints new president</title>
<dateline>    irvine, calif., march 30 - </dateline>san/bar corp said it appointed
charles von urff as president, chief operating officer and a
director of the company to succeed robert johnson, who retired
last year.
    prior to joining san/bar von urff had been president and
chief executive of microelectric packaging inc, the company
also said.
 reuter
</text>]