[<text>
<title>coca-cola &lt;ko&gt; executive resigns</title>
<dateline>    atlanta, march 24 - </dateline>coca-cola co said eugene v. amoroso has
resigned as president and chief executive officer of its
coca-cola foods division to pursue other business interests.
    the company said harry e. teasley jr., who had headed its
bottling subsidiary serving southern england, will succeed
amoroso.
 reuter
</text>]