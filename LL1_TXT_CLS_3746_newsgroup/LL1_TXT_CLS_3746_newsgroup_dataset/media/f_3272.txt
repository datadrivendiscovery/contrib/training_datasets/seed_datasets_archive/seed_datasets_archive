[<text>
<title>italy shows interest in u.k. new crop wheat</title>
<dateline>    london, march 9 - </dateline>italy has shown interest in british new
crop wheat recently but the actual volume booked so far by
italian buyers has not been large, traders said.
    they put purchases at around 50,000 tonnes for sept/dec
shipments but said some of the business was transacted at the
start of the year.
    italian interior home markets have been active in recent
weeks and traders said around 200,000 tonnes have traded
between dealers and home consumers. some of this has been
covered in the market here and more possibly will be, traders
said.
 reuter
</text>]