[<text>
<title>vanzetti &lt;vanz&gt; increase of shares approved</title>
<dateline>    stoughton, mass, march 11 - </dateline>vanzetti systems inc said its
shareholders approved increasing the number of authorized
shares to five mln from three mln.
    shareholders also approved increasing the number of shares
reserved for options to employees to 300,000 from 150,000
 reuter
</text>]