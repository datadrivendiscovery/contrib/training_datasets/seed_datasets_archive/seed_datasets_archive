[<text>
<title>dexter &lt;dex&gt; units sets license with toyota unit</title>
<dateline>    pittsburg, calif., march 26 - </dateline>dexter corp's hysol aerospace
and industrial products division said it agreed to license its
engineering adhesives to toyota motor co's toyoda gosei unit.
    the two units will jointly develop a line of structural
adhesive and application techniques for the automotive and
certain other industries.
 reuter
</text>]