[<text>
<title>ge &lt;ge&gt; circuits certified by military</title>
<dateline>    research triangle park, n.c., march 31 - </dateline>general electric
co said the u.s. defense electronics supply center has awarded
its research triangle park, n.c., facility a generic
certification for the production of two-micro gate arrays for
military use.
    the company said after it qualifies three generic test
circuits using the newly-certified two-micro process -- a
standard evaluation chip, micro test chip and jedec benchmark
chip -- the facility will require greatly simplified testing to
comply with military standards.  that qualification is expected
by the fourth quarter, the company said.
 reuter
</text>]