[<text>
<title>environmental &lt;power.o&gt; boardmember resigns</title>
<dateline>    boston, june 19 - </dateline>environmental power corp said that robert
w. baldridge has resigned from its board of directors,
effective june 17.
    environmental develops, owns and operates small power
production, hydropower, and waste-energy plants.
 reuter
</text>]