[<text>
<title>safeguard scientific &lt;sfe&gt; in equity deal</title>
<dateline>    king of prussia, pa., march 12 - </dateline>safeguard scientifics inc
said it made a 2.5 mln dlr equity investment in &lt;sanchez
computer associates inc&gt;, a private computer software firm
based in malvern, pa.
   safeguard said the investment gives it a "major ownership
position" in sanchez, which specializes in software products
for financial institutions.
 reuter
</text>]