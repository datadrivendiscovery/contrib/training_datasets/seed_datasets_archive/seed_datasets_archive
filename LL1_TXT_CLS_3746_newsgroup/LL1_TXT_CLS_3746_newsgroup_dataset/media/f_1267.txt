[<text>
<title>moody's downgrades loral &lt;lor&gt; debt</title>
<dateline>    new york, march 3 - </dateline>moody's investors service inc said it
downgraded to baa-2 from a-3 the rating on loral corp's 7-1/4
pct convertible subordinated debentures of 2010 and on 10-3/4
pct subordinated sinking fund debentures due 1997.
    moody's said it took the action, which affects
approximately 117 mln dlrs of debt, after examining loral's
acquisition of goodyear aerospace corp.
    it said the downgrade reflects the substantial increase in
the company's debt in order to finance the acquisition and the
risks of assimilating such a relatively large acquisition.
 reuter
</text>]