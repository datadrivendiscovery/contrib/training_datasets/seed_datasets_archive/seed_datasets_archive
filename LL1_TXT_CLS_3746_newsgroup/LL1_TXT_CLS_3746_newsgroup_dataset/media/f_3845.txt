[<text>
<title>neco &lt;npt&gt; postpones annual meeting</title>
<dateline>    middletown, r.i., march 11 - </dateline>neco enterprises inc said it
has postponed its annual meeting to may 21 from the previously
scheduled april 28 and the record date for the meeting to march
25 from march 11.
    a spokesman said the changes were made because of the
pending tender offer for the company's stock.
 reuter
</text>]