[<text>
<title>convenient food mart inc &lt;cfmi&gt; 4th qtr net</title>
<dateline>    rosemont, ill., march 19 - </dateline>period ended dec 28
    shr 42 cts vs 35 cts
    net 941,000 vs 786,000
    revs 12,798,000 vs 2,269,000
    year
    shr 97 cts vs 83 cts
    net 2,211,000 vs 1,841,000
    revs 19,027,000 vs 6,474,000
    note: 1985 period ended december 29
    share results adjusted for five-for-four stock split on
april 28, 1986 and 10 pct stock dividend issued dec 10, 1986
 reuter
</text>]