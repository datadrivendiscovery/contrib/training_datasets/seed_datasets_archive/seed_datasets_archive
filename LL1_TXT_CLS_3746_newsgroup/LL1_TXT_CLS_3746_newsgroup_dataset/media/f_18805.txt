[<text>
<title>acton &lt;atn&gt; sets one for five reverse split</title>
<dateline>    acton, mass, june 18 - </dateline>acton corp said shareholders at the
annaul meeting approved a one-for-five reverse split that takes
effect june 25.
    it said holders also approved a limitation of directors'
liability and indemnity agreements between the company and its
officers and directors.
 reuter
</text>]