[<text type="unproc">
federal reserve weekly report 2 - feb 26
     two weeks ended feb 25                    
 total vault cash...........25,237 vs....27,327
 inc cash equal to req res..22,834 vs....24,680
     one week ended feb 25      daily avgs-mlns
 bank borrowings...............614 down.....131
 including seasonal loans.......88 up........14
 including extended loans......304 up........10
 float.........................511 down.....320
 balances/adjustments........2,101 down......67
 currency..................206,490 down.....519
 treasury deposits...........4,208 down......63

 reuter


</text>]