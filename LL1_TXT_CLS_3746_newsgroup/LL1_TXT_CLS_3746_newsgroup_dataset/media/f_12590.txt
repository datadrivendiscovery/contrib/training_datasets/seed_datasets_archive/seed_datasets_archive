[<text>
<title>lone star &lt;lce&gt; forms joint venture</title>
<dateline>    greenwich, conn., april 2 - </dateline>lone star industries inc said
it formed a joint venture with monier ltd of australia to make
and sell concrete railroad ties in north america.
    the equally owned venture, lone star monier concrete tie
co, initially will operate a new denver, colo., plant that will
make 1.75 mln ties ordered by burlington northern inc &lt;bni&gt;.
    in addition, the venture will market ties made at lone
star's existing massachusetts facilities.
    based in chatswood, new south wales, monier is a
manufacturer of building and construction products and concrete
ties.
 reuter
</text>]