[<text>
<title>japan to curb vehicle exports to ec</title>
<dateline>    tokyo, march 11 - </dateline>japanese car makers will curb their
exports to the european community (ec) following an unofficial
directive from the ministry of international trade and industry
(miti), automobile industry sources said.
    some sources said exports to the ec this year are likely to
be at most unchanged from last year's 1.10 mln units and may
even fall due to an anticipated slackening of ec economic
growth and increasing trade friction.
    last week, miti vice minister for international affairs
makoto kuroda said the ministry had asked car makers to
exercise prudence in exporting to europe.
 
 reuter
</text>]