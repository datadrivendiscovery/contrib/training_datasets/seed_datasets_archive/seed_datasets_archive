[<text>
<title>hre properties &lt;hre&gt; 1st qtr ends jan 31 net</title>
<dateline>    new york, march 17 -
    </dateline>shr 38 cts vs 47 cts
    net 2,253,664 vs 2,806,820
    revs 5,173,318 vs 5,873,904
    note: 1987 qtr includes 126,117 dlrs, or two cts per share,
from gains on sale of property, vs gain 29,812, or less than
one cent per share, for prior qtr.
 reuter
</text>]