[<text>
<title>western federal savings bank &lt;wfpr&gt; sets payout</title>
<dateline>    mayaguez, puerto rico, june 18 -
    </dateline>qtly div 15 cts vs 15 cts prior
    pay july 15
    reord june 30    
 reuter
</text>]