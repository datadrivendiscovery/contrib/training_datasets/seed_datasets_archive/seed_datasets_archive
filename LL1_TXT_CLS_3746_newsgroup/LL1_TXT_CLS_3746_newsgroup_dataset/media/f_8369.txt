[<text>
<title>doe makes acid rain cleanup solicitation</title>
<dateline>    washington, march 23 - </dateline>the department of energy said it is
soliciting companies for innovative clean coal technologies as
part of president reagan's acid rain initiative.
    energy secretary john herrington said the 850 mln dlr
solicitation is tailored to attract industry proposals for
advanced pollution control devices that can be installed on
existing coal-fired power plants.
    companies submitting the new technologies would need to at
least match the federal funding share if their concept is
selected, the doe said. herrington said he will appoint a
senior panel to advise on the technologies.
 reuter
</text>]