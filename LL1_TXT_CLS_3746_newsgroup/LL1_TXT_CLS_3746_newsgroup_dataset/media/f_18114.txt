[<text>
<title>security bancorp &lt;secb.o&gt; merges two units</title>
<dateline>    southgate, mich., june 2 - </dateline>security bancorp inc said it has
merged its security bank of almont unit into its security bank
northeast unit.
 reuter
</text>]