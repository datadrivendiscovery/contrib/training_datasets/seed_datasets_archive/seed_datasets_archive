[<text>
<title>value line inc &lt;valu&gt; 3rd qtr jan 31</title>
<dateline>    new york, march 16 -
    </dateline>shr 52 cts vs 25 cts
    net 5,154,000 vs 2,496,000
    revs 17.7 mln vs 14.4 mln
    nine months
    shr 1.16 dlrs vs 70 cts
    net 11.5 mln seven mln
    revs 50.3 mln vs 41.2 mln
    note: 1987 periods include pretax investment income
of 2.9 mln dlrs in capital gains distributions from mutual fund
investment.
 reuter
</text>]