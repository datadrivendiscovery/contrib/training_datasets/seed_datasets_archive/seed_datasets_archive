[<text>
<title>aileen inc &lt;aee&gt; 1sr qtr jan 31 loss</title>
<dateline>    new york, march 13 - 
    </dateline>shr loss 30 cts vs loss 20 cts
    net loss 1,553,000 vs loss 1,031,000
    revs 10.0 mln vs 8,696,000
 reuter
</text>]