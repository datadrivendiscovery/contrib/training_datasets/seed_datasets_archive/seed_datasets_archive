[<text>
<title>international technology &lt;itx&gt; makes acquisition</title>
<dateline>    torrance, calif., march 3 - </dateline>international technology corp
said it has purchased &lt;western emergency services inc&gt; in a
pooling-of-interests transaction.
    international technology, a hazardous materials management
company, said it purchased western emergency, an environmental
services firm, to offer a broader range of environmental
services to the gulf coast area.
 reuter
</text>]