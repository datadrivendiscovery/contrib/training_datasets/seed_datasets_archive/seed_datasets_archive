[<text>
<title>&lt;microtel inc&gt; makes acquisition</title>
<dateline>    boca raton, fla., march 23 - </dateline>microtel inc said it has
completed the acquisition of &lt;american teledata corp&gt; and its
us dial subsidiary, which provide long distance telephone
service in northeast florida. terms were not disclosed.
    microtel's shareholders include norfolk southern corp
&lt;nsc&gt;, m/a-com inc &lt;mai&gt;, centel corp &lt;cnt&gt;, alltel corp &lt;at&gt;
and e.f. hutton group inc &lt;efh&gt;.
 reuter
</text>]