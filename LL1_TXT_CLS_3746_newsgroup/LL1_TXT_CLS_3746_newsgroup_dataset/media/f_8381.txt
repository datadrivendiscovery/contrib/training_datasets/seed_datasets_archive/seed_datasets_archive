[<text>
<title>hartford fire &lt;hig&gt; to offer preferred</title>
<dateline>    hartford, conn, march 23 - </dateline>hartford fire insurance co said
it filed a registration statement with the securities and
exchange commission relating to up to 210 mln dlrs aggregate of
class c and class d preferred stock.
    proceeds from any of the stock will be used towards the
redemption of its class a preferred stock, it said.
 reuter
</text>]