[<text>
<title>iraq ccc credit guarantees switched - usda</title>
<dateline>    washington, april 7 - </dateline>the commodity credit corporation
(ccc) has transferred 21.0 mln dlrs in credit guarantees
previously earmarked for sales of u.s. corn and 5.0 mln dlrs
for sales of oilseeds to increase available coverage on sales
of u.s. poultry meat to iraq, the u.s. agriculture department
said.
    the department said the action was taken at the request of
iraq's state trade organization for grains and foodstuffs.
    the guarantee line for sales of corn has been reduced from
78.0 mln dlrs to 57.0 mln and the line for oilseeds from 5.0
mln dlrs to zero.
    the guarantee line for sales of frozen poultry has been
increased from 30.0 mln dlrs to 56.0 mln dlrs, usda said.
 reuter
</text>]