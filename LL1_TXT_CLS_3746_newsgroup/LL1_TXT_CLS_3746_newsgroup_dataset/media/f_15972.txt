[<text>
<title>liberty financial group &lt;lfg&gt; president resigns</title>
<dateline>    horsham, pa., april 9 - </dateline>liberty financial group charles d.
cheleden, chairman and chief executive officer, said harold
kline has resigned his position as president of the group and
its subsidiary, liberty savings bank, effective may one.
    cheleden said he will assume the offices previously held by
kline, who will continue to serve on the board of the liberty
financal group and liberty savings bank.
 reuter
</text>]