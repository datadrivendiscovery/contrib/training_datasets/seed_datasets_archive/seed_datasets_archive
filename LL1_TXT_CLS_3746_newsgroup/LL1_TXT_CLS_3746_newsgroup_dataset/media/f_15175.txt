[<text>
<title>&lt;hayes-dana inc&gt; 1st qtr net</title>
<dateline>    toronto, april 8 -
    </dateline>shr 30 cts vs 28 cts
    net 5,000,000 vs 4,600,000
    revs 125.2 mln vs 123.9 mln
    note: 52 pct-owned by dana corp &lt;dcn&gt;.
 reuter
</text>]