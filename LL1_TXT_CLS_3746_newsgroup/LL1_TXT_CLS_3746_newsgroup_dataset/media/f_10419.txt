[<text>
<title>hanson trust &lt;han&gt; u.s. arm sells chemical unit</title>
<dateline>    new york, march 27 - </dateline>hanson trust plc &lt;han&gt; said its u.s.
subsidiary, hanson industries, sold pcr inc, a specialty
chemicals unit, for 6.25 mln dlrs in cash to &lt;chemical partners
inc&gt;.
    hanson industries said it acquired pcr inc in 1986 as part
of its purchase of &lt;scm corp&gt;.
    pcr inc posted an operating loss in 1986 of 381,000 dlrs on
sales of 13.2 mln dlrs, the company said.
 reuter
</text>]