[<text>
<title>bdm international &lt;bdm&gt; gets air force contract</title>
<dateline>    mclean, va., march 12 - </dateline>bdm international inc said it has
received a 26.7 mln dlr contract to provide operations,
maintenance and test support to the u.s. air force weapons
laboratory nuclear simulator test facilities at kirtland air
force base in albuquerque.
    the company said the contract covers a four-year peiod and
includes an option for another year.
 reuter
</text>]