[<text>
<title>orion broadcast &lt;obgi.o&gt; buys ford &lt;f&gt; unit</title>
<dateline>    denver, june 2 - </dateline>orion broadcast group inc said its
majority-owned orion financial services corp subsidiary has
agreed to purchase fn realty services inc from ford motor co
for 1,200,000 to 1,500,000 dlrs in cash and notes.
    it said closing is expected within 45 days after receipt of
regulatory approvals.
    fn provides loan collection, accounting, data processing
and administrative services to the real estate industry.
 reuter
</text>]