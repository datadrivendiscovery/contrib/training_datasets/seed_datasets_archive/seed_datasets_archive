[<text>
<title>sea galley stores inc &lt;seag&gt; 1st qtr net</title>
<dateline>    mountlake terrace, wash., april 9 -
    </dateline>oper shr four cts vs one ct
    oper net 108,000 vs 30,000
    revs 12.8 mln vs 14.8 mln
    note: current qtr figures exclude operating loss
carryforward gain of 57,000 dlrs, or two cts per share vs gain
of 21,000 dlrs, or one ct per share.
 reuter
</text>]