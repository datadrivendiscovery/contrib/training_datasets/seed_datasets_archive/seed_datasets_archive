[<text>
<title>jackpot enterprises &lt;jack&gt; to buyback shares</title>
<dateline>    las vegas, march 27 - </dateline>jackpot enterprises inc said it will
buy up to 50,000 of its own common shares from time to time in
market transactions.
    the amusement and recreation services company recently
completed buying 107,000 of its shares.
 reuter
</text>]