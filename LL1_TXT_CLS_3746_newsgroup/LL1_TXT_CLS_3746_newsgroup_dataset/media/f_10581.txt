[<text>
<title>henley group &lt;heng&gt; unit wins taiwan contract</title>
<dateline>    houston, march 27 - </dateline>henley group inc's m.w. kellogg co unit
said it was selected by &lt;chinese petroleum corp&gt; to design,
engineer and build a ethylene plant at chinese
petroleum's kaohsiung refinery in taiwan.
    terms of the contract were not disclosed, but kellogg said
the total cost of the plant will be 300 mln dlrs.
    kellogg said engineering of the plant, which will have a
capacity of 400,000 tonnes a year, is already underway and
construction will begin in 1988.
    the plant will go into operation in 1990, kellogg said.
 reuter
</text>]