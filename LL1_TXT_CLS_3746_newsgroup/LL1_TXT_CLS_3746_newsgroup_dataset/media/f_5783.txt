[<text>
<title>japan to release gnp figures later today</title>
<dateline>    tokyo, march 17 - </dateline>the economic planning agency will
announce gross national product (gnp) figures for the
october/december quarter today at 1700 hrs local time (0800
gmt), agency officials told reuters.
    in the july/september quarter, gnp rose 0.6 pct from the
previous three months.
 reuter
</text>]