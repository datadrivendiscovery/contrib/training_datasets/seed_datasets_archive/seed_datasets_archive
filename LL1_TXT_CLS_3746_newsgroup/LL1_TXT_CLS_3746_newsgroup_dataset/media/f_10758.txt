[<text>
<title>algeria sets tender for rape/sunflowerseed oil</title>
<dateline>    london, march 30 - </dateline>algeria will tender on april 3 for
20,000 tonnes of optional origin sunflowerseed oil/rapeseed oil
for apr/may loading, traders said.
    meanwhile, the market is awaiting results of an algerian
import tender which took place over the weekend for about
10,000 tonnes of refined vegetable oils in drums, traders
added.
 reuter
</text>]