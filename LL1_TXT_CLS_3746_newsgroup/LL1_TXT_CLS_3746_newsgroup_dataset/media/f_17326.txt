[<text>
<title>itt corp &lt;itt&gt; confident about fiscal 1987</title>
<dateline>    new york, april 27 - </dateline>itt corp said first quarter results,
reported earlier, were substantially ahead of budget and added
it is confident of a continued improvement by all divisions in
the remainder of the year.
    the company said the natural resources division more than
doubled its contribution from the year ago quarter while the
diversified services business reported strong operating results
due to continued improvement in the domestic casualty business
at the hartford.
    it said the operationg performance of the industrial and
defense technology business was above expectations but below
the year ago result.
 reuter
</text>]