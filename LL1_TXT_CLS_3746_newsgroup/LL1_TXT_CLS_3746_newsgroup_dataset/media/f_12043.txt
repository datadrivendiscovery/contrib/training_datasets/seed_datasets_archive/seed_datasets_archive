[<text>
<title>quantum diagnostics &lt;qtmcu&gt; gets patent</title>
<dateline>    hauppauge, n.y., april 1 - </dateline>quantum diagnostics ltd said it
has been granted a patent for a new imaging technology it has
developed that uses electrmagnetic radiation.
    it said it sees applications in airport security screening
devices, medical diagnostic imaging systems and quality control
devices.
 reuter
</text>]