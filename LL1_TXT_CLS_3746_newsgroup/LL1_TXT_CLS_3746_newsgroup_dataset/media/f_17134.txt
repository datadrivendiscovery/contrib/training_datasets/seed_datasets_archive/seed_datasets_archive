[<text>
<title>continental &lt;cont.o&gt; board considers suit</title>
<dateline>    mechanicsburg, pa, april 24 - </dateline>continental medical systems
inc said its board met to consider the allegations in the
derivative action filed by continental care group and that
officers named as defendants believe it to be totally without
merit is likely to be dismissed in maryland federal court.
    continental medical has sued continental care group for
over 5 mln dlrs for breach of a contract calling for it to buy
12 nursing homes from continental care.
    yesterday, continental care filed a derivative action
against continental medical charging it with fraud and
misrepresentation of fiduciary duties.

 reuter
</text>]