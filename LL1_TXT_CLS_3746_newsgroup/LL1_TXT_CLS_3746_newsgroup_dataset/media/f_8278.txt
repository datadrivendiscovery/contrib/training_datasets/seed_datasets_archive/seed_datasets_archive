[<text>
<title>waite kidnapped as spy - tehran radio</title>
<dateline>    london, march 23 - </dateline>tehran radio said british church envoy
terry waite, missing in beirut since january 20, had been
kidnapped as a spy by a lebanese group.
    "terry waite was taken hostage by an armed lebanese group,
calling itself the revolutionary justice organisation. he has
been accused of espionage activities," said the radio, monitored
by the british broadcasting corporation.
 reuter
</text>]