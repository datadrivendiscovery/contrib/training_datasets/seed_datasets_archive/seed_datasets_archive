[<text>
<title>lifestyle restaurants &lt;lif&gt; adjusts revenues</title>
<dateline>    new york, march 13 - </dateline>lifestyle restaurants inc said
revenues for the first quarter ended january 24, 1985, were
17.5 mln dlrs and not the 17.8 mln dlrs it had reported
earlier.
    the company also said a note attached to its earnings
concerning a gain in 1986 on certain sales was incorrect and
should be disregarded.
 reuter
</text>]