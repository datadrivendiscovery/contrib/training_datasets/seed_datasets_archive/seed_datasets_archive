[<text>
<title>valero natural gas &lt;vlp&gt; sets initial payout</title>
<dateline>    san antonio, texas, june 18 - </dateline>valero natural gas partners
lp said its board has declared an initial quarterly dividend of
67.3611 cents per unit, payable august 14, record july 1.
    the partnership said the distribution covers its first full
quarter of operations ending june 30 plus the period from the
inception of its operations on march 25.
    future dividends will be 62.5 cts quarterly, it said.
 reuter
</text>]