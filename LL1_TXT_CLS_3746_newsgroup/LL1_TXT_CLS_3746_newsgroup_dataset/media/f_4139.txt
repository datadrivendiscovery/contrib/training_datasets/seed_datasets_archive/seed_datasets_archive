[<text>
<title>treasury secretary baker declines comment on g-6</title>
<dateline>    new york, march 12 - </dateline>u.s. treasury secretary james baker
declined comment on the february 22 paris accord between the
six major industrial nations under which they agreed to foster
exchange rate stability.
    asked by reporters after a speech before the national
fitness foundation banquet what, if any, currency intervention
levels had been set in paris, baker replied: "we never talk
about intervention."
    baker also declined to comment on his views about the
foreign exchange markets' reaction to the accord.
 reuter
</text>]