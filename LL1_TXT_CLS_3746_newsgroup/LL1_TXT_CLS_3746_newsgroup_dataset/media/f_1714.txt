[<text>
<title>unicorp american corp &lt;uac&gt; sets quarterly</title>
<dateline>    new york, march 4 -
    </dateline>qtly div 15 cts vs 15 cts prior
    pay march 31
    record march 13
 reuter
</text>]