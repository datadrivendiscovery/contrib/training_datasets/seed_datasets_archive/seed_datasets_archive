[<text>
<title>britton lee &lt;blii&gt; sees first quarter loss</title>
<dateline>    los gatos, calif., april 2 - </dateline>britton lee inc said it
expects to report a loss on lower sales than it had anticipated
for the first quarter.
    the company earned 119,000 dlrs before a 70,000 dlr tax
credit on sales of 7,227,000 dlrs in the year-ago period.
</text>]