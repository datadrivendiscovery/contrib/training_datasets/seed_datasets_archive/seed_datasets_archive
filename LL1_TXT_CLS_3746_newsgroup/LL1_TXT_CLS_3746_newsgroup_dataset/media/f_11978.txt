[<text>
<title>cosmo communications corp &lt;csmo&gt; year loss</title>
<dateline>    miami, april 1 -
    </dateline>shr loss one ct vs loss 2.16 dlrs
    net loss 30,000 vs loss 12.4 mln
    revs 27.4 mln vs 38.3 mln
 reuter
</text>]