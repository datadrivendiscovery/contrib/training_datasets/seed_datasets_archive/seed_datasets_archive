[<text>
<title>petroleum and resources corp &lt;peo&gt; march 31</title>
<dateline>    new york, april 9 -
    </dateline>shr 31.36 dlrs vs 25.23 dlrs
    assets 286.5 mln vs 253.0 mln
    shrs out 9,138,526 vs 8,839,695
    note: latest assets after capital gain distributions of 50
cts a share in february 1987 and 83 cts a share in december
1986, and with 29,955,000 stated value 1.676 dlr convertible
preferred stock outstanding.
 reuter
</text>]