[<text>
<title>bzw makes markets in japan dollar convertibles</title>
<dateline>    london, march 16 - </dateline>barclays de zoete wedd ltd said it today
began market-making in japanese convertible eurobonds
denominated in dollars.
    this venture will be followed within a few months by the
establishment of a trading operation for japanese u.s.
dollar-denominated equity warrant issues.
    director in charge of the japanese convertible operation is
kelvin saunders, who said the desk was currently staffed by
seven traders and salespeople. this number would be doubled
with the addition of the equity warrant operation, "an even more
important element in the current market environment," he said.
 reuter
</text>]