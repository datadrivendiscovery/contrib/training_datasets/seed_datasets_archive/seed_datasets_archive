[<text>
<title>central bancshares &lt;cbss&gt; 1st qtr net</title>
<dateline>    birmingham, ala., april 7 -
    </dateline>shr 52 cts vs 45 cts
    net 10.9 mln vs 9,498,000
    assets 3.60 billion vs 3.25 billion
    deposits 2.45 billion vs 2.23 billion
    loans 2.16 billion vs 1.67 billion
    note: full name of company is central bancshares of the
south inc.
 reuter
</text>]