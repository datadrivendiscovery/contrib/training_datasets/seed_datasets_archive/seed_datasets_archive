[<text>
<title>devcon international &lt;devc.o&gt; gets contract</title>
<dateline>    pompano beach, fla., april 29 - </dateline>devcon international corp
said it has received a 13.5 mln dlr contract from the
government of antigua for harbor dredging and construction of a
deepwater pier at st. john's, with completion expected within
two years.
    it said work will start immediately.
 reuter
</text>]