[<text>
<title>&lt;agra industries ltd&gt; six mths jan 31 net</title>
<dateline>    saskatoon, saskatchewan, march 13 -
    </dateline>oper shr 35 cts vs 34 cts
    oper net 2,313,000 vs 1,646,000
    revs 100.1 mln vs 77.3 mln
    note: 1986 net excludes extraordinary loss of 294,000 dlrs
or four cts vs shr vs yr-ago loss of 579,000 dlrs or 12 cts
shr. 1986 net includes non-cash loss of 1,436,000 dlrs or 22
cts shr vs yr-ago loss of 1,922,000 dlrs or 39 cts shr from
depreciation and amortization allowances on u.s. cable tv
operation.
    fewer shrs outstanding.
 reuter
</text>]