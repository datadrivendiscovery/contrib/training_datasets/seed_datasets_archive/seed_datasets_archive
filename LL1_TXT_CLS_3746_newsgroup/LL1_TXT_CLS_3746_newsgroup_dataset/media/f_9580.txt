[<text>
<title>coopervision inc &lt;eye&gt; qtly dividend</title>
<dateline>    palo alto, calif., march 25 -
    </dateline>shr 10 cts vs 10 cts prior qtr
    pay april 17
    record april 9.

 reuter
</text>]