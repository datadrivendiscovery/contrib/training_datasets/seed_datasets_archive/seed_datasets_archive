[<text>
<title>first medical &lt;fmdc&gt; gets order</title>
<dateline>    bellevue, wash., march 26 - </dateline>first medical devices corp said
it has received a contract to sell emt-defibrillation equipment
to lane county, ore., for undisclosed terms.
 reuter
</text>]