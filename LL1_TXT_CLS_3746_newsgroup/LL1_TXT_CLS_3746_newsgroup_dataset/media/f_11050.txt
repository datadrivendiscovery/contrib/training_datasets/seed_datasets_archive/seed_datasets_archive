[<text>
<title>apple &lt;aapl&gt; expands network capabilities</title>
<dateline>    cupertino, calif., march 30 - </dateline>apple computer inc said it
has extended the ability of the apple macintosh personal
computer family to commmunicate over &lt;northern telecom ltd's&gt;
meridian sl-1 integrated services network.
    the new capabilities include linking separate appletalk
networks, dial-up remote use of laserwriter printers, one step
file transfer and conversion and memorybank 485, a new 485
megabyte storage subsystem for use with macintosh personal
computers, apple said.
 reuter
</text>]