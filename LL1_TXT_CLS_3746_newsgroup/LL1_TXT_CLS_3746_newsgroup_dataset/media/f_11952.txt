[<text>
<title>finland orders mcdonnell douglas md-11s</title>
<dateline>    helsinki, april 1 - </dateline>the finnish national airline &lt;finnair&gt;
said in a statement today it ordered two mcdonnell douglas corp
&lt;md.n&gt; md-11 passenger planes for delivery in october 1990 and
may 1991 and had taken an option to buy on two more.
    finnair chief gunnar korhonen would not discuss the price
further than telling reporters the planes were "inexpensive,"
though market sources said the airline could expect a price cut
as an earlier customer.
   korhonen said finnair had not opted for europe's a-340 plane
because of uncertainty about the manufacturing of its engines,
which may have delayed production of the jet.
 reuter
</text>]