[<text>
<title>&lt;widcom inc&gt; cuts teleconference system price</title>
<dateline>    los gatos, calif., march 16 - </dateline>widcom inc said effective
immediately it has cut the u.s. price for its video
teleconferencing system by 32 pct to 49,950 dlrs.
    it said it will consider volume discounts on orders of more
than 10 systems.
 reuter
</text>]