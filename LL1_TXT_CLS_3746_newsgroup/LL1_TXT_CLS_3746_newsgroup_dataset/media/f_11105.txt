[<text>
<title>inland vacuum &lt;ivac&gt; sets stock split</title>
<dateline>    upper saddle river, march 30 - </dateline>inland vacuum inc said is
board proposed a two-for-one stock split payable to
shareholders of record april 30.
    the board also elected phillip frost chairman, succeeding
john durkin, who remains president and chief executive officer.
frost in early february bought 49 pct of the company, durkin
said.
    stockholders at the annual meeting approved a measure to
change the company's name to ivaco industries inc. five new
directors were also elected to the company's board. durkin was
re-elected to the board, the company said.
 reuter
</text>]