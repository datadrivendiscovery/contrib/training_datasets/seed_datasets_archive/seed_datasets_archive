[<text>
<title>first union &lt;fur&gt; leaves dividend unchanged</title>
<dateline>    cleveland, march 4 - </dateline>first union real estate investments
said its board left the quarterly dividend unchanged at 37-1/2
cts per share, payable april 30, record march 31.
    the trust, which has raised its quarterly dividend
frequently in the past two years and in the first quarter in
both years, said the tax reform act of 1986 has limited its
flexibility on dividends, and trustees will now consider the
appropriateness of any dividend increases only during the later
quarters of the year.

 reuter
</text>]