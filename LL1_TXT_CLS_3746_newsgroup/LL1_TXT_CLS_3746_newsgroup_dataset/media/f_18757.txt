[<text>
<title>nucor corp &lt;nue&gt; sets payout</title>
<dateline>    charlotte, n.c., june 18 -
    </dateline>qtly div nine cts vs nine cts prior
    pay aug 11
    record june 30
 reuter
</text>]