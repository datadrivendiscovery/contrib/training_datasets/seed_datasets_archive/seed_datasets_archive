[<text>
<title>iranian premier reiterates warning of retaliation</title>
<dateline>    damascus, oct 20 - </dateline>iranian premier mir-hossein mousavi
reiterated his country would retaliate for u.s. navy attacks on
gulf oil platforms.
    "the u.s. attack on iran's oil platforms jeopardises our
national sovereignty ... and we will retaliate properly for
this perfidious american aggression," mousavi told a news
conference in damascus.  on monday u.s. navy warships blasted
the rostam platform, and navy personnel stormed a second
platform a few miles away. washington said the operation was
aimed at destroying positions used by iran to track and assault
neutral gulf shipping.
 reuter
</text>]