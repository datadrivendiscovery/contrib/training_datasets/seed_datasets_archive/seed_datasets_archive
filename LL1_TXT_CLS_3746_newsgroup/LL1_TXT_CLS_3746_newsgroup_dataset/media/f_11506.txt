[<text>
<title>pennzoil &lt;pzl&gt; sees lower first qtr</title>
<dateline>    new york, march 31 - </dateline>pennzoil co chairman j. hugh liedtke
told a meeting of analysts that the company expects its first
quarter net to be down considerably, before an unusual item,
from the loss of 49 cts a share reported a year ago.
    liedtke did not say how large the unusual item would be.
"we're still closing our books on the first quarter, so i don't
have final numbers for you," he said.
 reuter
</text>]