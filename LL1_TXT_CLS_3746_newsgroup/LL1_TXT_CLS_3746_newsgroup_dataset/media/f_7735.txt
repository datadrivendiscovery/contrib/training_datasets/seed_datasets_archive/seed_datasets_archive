[<text>
<title>spi pharmaceuticals inc &lt;spip&gt; 1st qtr oper net</title>
<dateline>    costa mesa, calif., march 20 - </dateline>qtr ended feb 28
    oper shr 31 cts vs 14 cts
    oper net 3,203,000 vs 1,357,000
    revs 13.0 mln vs 15.6 mln
    note: oper data does not include year ago extraordinary
gain of 821,000 dlrs, or eight cts per shr.
 reuter
</text>]