[<text>
<title>club med inc &lt;cmi&gt; 1st qtr jan 31 net</title>
<dateline>    new york, march 23 -
    </dateline>shr 41 cts vs 38 cts
    net 5,630,000 vs 5,152,000
    revs 97.1 mln vs 85.4 mln
    
 reuter
</text>]