[<text>
<title>mazda motor corp &lt;mazt.t&gt;</title>
<dateline>    tokyo, june 18 - </dateline>six months ended april 30
    parent shr 1.66 yen vs 7.28
    div 3.50 yen vs same
    net 1.59 billion vs 6.88 billion
    current 5.03 billion vs 16.03 billion
    sales 804.02 billion vs 839.20 billion
    oustanding shrs 955.00 mln vs 944.15 mln
 reuter
</text>]