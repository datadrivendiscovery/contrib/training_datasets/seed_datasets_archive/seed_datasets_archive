[<text>
<title>insituform of north america inc &lt;insua&gt; 4th qtr</title>
<dateline>    memphis, tenn., march 11 -
    </dateline>shr nine cts vs four cts
    net 658,159 vs 299,930
    revs 3,770,341 vs 2,614,224
    avg shrs 7,382,802 vs 6,747,442
    year
    oper shr 33 cts vs 18 cts
    oper net 2,287,179 vs 1,045,799
    revs 13.1 mln vs 8,577,853
    avg shrs 6,874,505 vs 5,951,612
    note: 1985 year net includes 13,000 dlr tax credit.
 reuter
</text>]