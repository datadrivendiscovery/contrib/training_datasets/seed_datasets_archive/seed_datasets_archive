[<text>
<title>bsn corp &lt;bsn&gt; 4th qtr net</title>
<dateline>    dallas, march 16 -
    </dateline>shr two cts vs eight cts
    net 73,000 vs 233,000
    revs 21.0 mln vs 9,510,000
    avg shrs 3,620,000 vs 2,886,000
    year
    shr 66 cts vs 37 cts
    net 2,246,000 vs 1,064,000
    revs 68.3 mln vs 40.8 mln
    avg shrs 3,392,000 vs 2,886,000
    note: 1985 year net includes extraordinary gain one ct shr
and gain two cts from cumulative effect of accounting change.
 reuter
</text>]