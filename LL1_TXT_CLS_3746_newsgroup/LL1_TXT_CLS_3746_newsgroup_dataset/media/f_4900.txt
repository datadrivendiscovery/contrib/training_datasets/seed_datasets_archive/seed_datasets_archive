[<text>
<title>helmerich and payne inc &lt;hp&gt; increases div</title>
<dateline>    tulsa, okla, march 13 -
    </dateline>qtly div 10 cts vs nine cts
    payable june one
    record may 15
 reuter
</text>]