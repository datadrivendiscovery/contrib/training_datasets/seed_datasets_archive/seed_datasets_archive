[<text>
<title>printronix inc &lt;ptnx.o&gt; 2nd qtr sept 25 net</title>
<dateline>    irvine, calif., oct 20 -
    </dateline>shr profit 11 cts vs loss 28 cts
    net profit 515,000 vs loss 1,328,000
    sales 31.0 mln vs 32.1 mln
    avg shrs 4,600,199 vs 4,815,062
    1st half
    shr loss 23 cts vs profit 10 cts
    net loss 1,033,000 vs profit 482,000
    sales 58.5 mln vs 62.1 mln
    avg shrs 4,565,752 vs 4,883,711
    note: 1986 half net includes pretax gain 4,150,000 dlrs
from sale of option to buy facility.
    backlog 28.1 mln dlrs vs 22.5 mln dlrs at end of previous
quarter and 21.0 mln dlrs at end of prior year's second quarter.
 reuter
</text>]