[<text>
<title>amoco &lt;an&gt; says dome &lt;dmp&gt; buy good for canada</title>
<dateline>    edmonton, alberta, june 2 - </dateline>amoco corp's wholly owned amoco
canada petroleum co ltd said its proposed 5.22-billion-
canadian-dlr acquisition of dome petroleum ltd will benefit
canada just like the foreign investment that made possible
commercial development of alberta's oilsands.
    amoco canada president t. don stacy told an oilsands
conference that "amoco canada has presented the solution to the
dome problem, and we're investing our confidence, dollars and
determination to make that solution work."
    the amoco buyout of debt-burdened dome has angered canadian
nationalists, who want a canadian buyer for dome.
    stacy described amoco canada's previously reported share
offer proposal as a chance to increase canadian ownership of
the country's oil and gas industry, now at about 50 pct.
    he reiterated that amoco planned virtually no layoffs of
dome employees. he also reaffirmed that amoco would reinvest in
amoco canada-dome properties all available cash flow for five
years after the acquisition.
 reuter
</text>]