[<text>
<title>canamax to acquire krezmar gold property stake</title>
<dateline>    toronto, march 18 - </dateline>&lt;canamax resources inc&gt; said it agreed
to acquire the 50 pct interest it does not already own in the
krezmar gold property, near wawa, ontario, by paying nine mln
dlrs to &lt;algoma steel corp ltd&gt; and granting algoma a four pct
net smelter return royalty, which is payable after payback.
    the property's drill indicated reserves to a depth of 1,200
feet are estimated at over one mln tons averaging 0.25 ounces
of gold a ton, canamax said.
 reuter
</text>]