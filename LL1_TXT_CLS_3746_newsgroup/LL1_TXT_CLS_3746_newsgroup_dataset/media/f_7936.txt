[<text>
<title>morgan stanley &lt;ms&gt; has market-on-close orders</title>
<dateline>    new york, march 20 - </dateline>morgan stanley and co &lt;ms&gt; said it
entered market-on-close orders totaling 1.1 billion dlrs for
stocks in the major market index and standard and poor's 500
stock index.
    morgan stanley said its orders were entered in accordance
with securities and exchange commission and new york stock
exchange requirements. the firm did not specify the nature of
its orders. participants in the index futures markets said
investors have been betting all day that the stock market will
finish sharply higher but few were willing to make a firm
prediction.
 reuter
</text>]