[<text>
<title>cistron biotechnology &lt;cist&gt; sets dividend</title>
<dateline>    pine brook, n.j., march 27 - </dateline>cistron biotechnology inc said
it will pay a stock dividend declared prior to the initial
public offering of its common stock in august 1986 to
stockholders of record prior to the common offering.
    payment of the dividend was contingent on the closing bid
price of the common stock averaging two dlrs or more per shares
for the trading days within any consecutive ten day period
ending before february 29, 1988. the company said that the
contingency has been fulfilled.
    payment of the stock dividend increases cistron's
outstanding common stock to 21,390,190 shares from 16,185,354
shares.
 reuter
</text>]