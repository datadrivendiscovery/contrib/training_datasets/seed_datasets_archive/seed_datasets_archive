[<text>
<title>general host &lt;gh&gt; to buy back shares</title>
<dateline>    stamford, conn., oct 20 - </dateline>general host corp said its board
authorized the repurchase of up to one mln additional shares of
its common.
    it said previous buy back authorizations of 6.5 mln shares
were approved in january 1875 and september 1987.
    to date, the company said it has repurchased about six mln
shares under those authorizations in open market transactions.
    general host has about 22.5 mln shares outstanding.
 reuter
</text>]