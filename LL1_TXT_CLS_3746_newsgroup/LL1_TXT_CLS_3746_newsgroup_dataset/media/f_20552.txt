[<text>
<title>science accessories &lt;seas.o&gt; ends purchase talks</title>
<dateline>    southport, conn., oct 20 - </dateline>science accessories corp said it
has ended talks on acquiring privately-held owl electronics
laborarories inc because it could not reach satisfactory terms.
 reuter
</text>]