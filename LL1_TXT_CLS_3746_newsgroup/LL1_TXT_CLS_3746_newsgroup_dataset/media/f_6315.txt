[<text>
<title>lennar &lt;len&gt; completes acquisition</title>
<dateline>    miami, march 17 - </dateline>lennar corp said it completed the
previously announced acquisition of development corp of america
&lt;dca&gt;.
    consequently, it said the american stock exchange suspended
trading of development corp's common stock, 10 pct subordinated
debentures due 1993 and 12 pct subordinated debentures due
1994.
    lennar said the debentures will continue to be traded
over-the-counter.
 reuter
</text>]