[<text>
<title>hitk &lt;hitk&gt; adds nipon, murdoch to clients</title>
<dateline>    stamford, conn., march 5 - </dateline>hitk corp said its worldwide 800
services telemarketing unit has added nipon electric co and
murdoch publishing to its list of clients.
    terms were not disclosed.
    hitk said nipon will use the service for promotion and
sales to hotels and individuals. murdoch will use the service
to sell subscriptions.
 reuter
</text>]