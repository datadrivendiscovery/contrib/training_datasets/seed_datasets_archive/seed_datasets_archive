[<text>
<title>kenner parker toys inc &lt;kpt&gt; 3rd qtr oper</title>
<dateline>    beverly, mass., oct 20 -
    </dateline>oper shr 1.22 dlrs vs 88 cts
    oper net 13.5 mln vs 11.7 mln
    revs 139.1 mln vs 160.5 mln
    nine mths
    oper shr 2.00 dlrs vs 1.15 dlrs
    oper net 22.8 mln vs 15.4 mln
    revs 348.8 mln vs 385.9 mln
    note: 1987 3rd qtr and nine mths oper net excludes gains
from tax loss carryforwards of 3,067,000 dlrs and 8,548,000
dlrs respectively. 1986 3rd qtr and nine mths oper net excludes
tax carryforward gains of 7,446,000 dlrs and 9,814,000 dlrs,
respectively.
 reuter
</text>]