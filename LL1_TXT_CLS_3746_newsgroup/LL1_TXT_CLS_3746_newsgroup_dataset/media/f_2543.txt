[<text>
<title>shearson has 5.4 pct of chicago milwaukee &lt;chg&gt;</title>
<dateline>    washington, march 5 - </dateline>shearson lehman brothers inc, the
brokerage subsidiary of american express co &lt;axp&gt;, said it has
acquired 131,300 shares of chicago milwaukee corp, or 5.4 pct
of its total outstanding common stock.
    in a filing with the securities and exchange commission,
shearson said it bought the stake for 18.8 mln dlrs for
investment purposes and has no intention of seeking control of
the company.
 reuter
</text>]