[<text>
<title>general binding &lt;gbnd&gt; in marketing agreement</title>
<dateline>    minneapolis, feb 26 - </dateline>general binding corp said it reached
a marketing agreement with varitronic systems inc, a
manufacturer and marketer of electronic lettering systems.
    under terms of the agreement, general binding will carry
varitronics' merlin express presentation lettering system, a
portable, battery-operated lettering system which produces type
on adhesive-backed tape.
 reuter
</text>]