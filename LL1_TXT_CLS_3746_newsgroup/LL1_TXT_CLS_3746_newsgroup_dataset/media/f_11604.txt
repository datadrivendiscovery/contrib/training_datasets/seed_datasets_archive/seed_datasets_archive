[<text>
<title>house votes to override reagan's bill veto</title>
<dateline>    washington, march 31 - </dateline>the house of representatives voted
350 to 73 to override president reagan's veto of an 88 billion
dlr highway and mass transit funding bill.
    the bill now goes to the senate, which must also pass the
bill by a two-thirds majority to override the veto.
    reagan vetoed the bill friday, saying it was a budget
buster and loaded with special interest projects.
 reuter
</text>]