[<text>
<title>natwest, rabo unit reports 8.8 pct profit growth</title>
<dateline>    den bosch, netherlands, march 11 - </dateline>dutch bank f. van
lanschot bankiers n.v., co-owned by national westminster plc
&lt;nwbl.l&gt; and rabobank b.a. &lt;rabn.a&gt; , said 1986 net profit rose
8.8 pct to 24.1 mln guilders on a 4.8-pct higher balance sheet
total of 6.2 billion.
    van lanschot bankiers is a subsidiary of van lanschot
beleggingscompagnie b.v. in which britain's national
westminster bank plc and dutch cooperative bank rabobank
nederland b.a. each have a 40-pct stake. commercial union's
dutch insurance unit &lt;delta lloyd verzekeringsgroep n.v.&gt; has a
5.4-pct stake in van lanschot, which lowered risk provisions to
22.5 mln guilders.
</text>]