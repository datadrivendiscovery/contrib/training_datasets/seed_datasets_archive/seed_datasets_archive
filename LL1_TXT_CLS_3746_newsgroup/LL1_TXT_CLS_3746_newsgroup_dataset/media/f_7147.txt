[<text>
<title>knowledge data systems &lt;kdsi&gt; names chairman</title>
<dateline>    salt lake city, march 19 - </dateline>knowledge data systems inc said
it named john kerr chairman of the company, and named its
former chairman, jon jacoby, vice chairman.
    the company said kerr was formerly president and co-founder
of &lt;mediflex systems corp&gt;, and jacoby is also its chief
financial officer.
 reuter
</text>]