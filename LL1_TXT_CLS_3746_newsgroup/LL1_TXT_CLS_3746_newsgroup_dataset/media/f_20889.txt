[<text>
<title>nat'l processing &lt;fkyn.o&gt;, chrysler &lt;c&gt; in pact</title>
<dateline>    louisville, ky., oct 19 - </dateline>national processing co inc said
it signed a contract with chrylser corp's chrysler financial
corp unit to process its loan payment trasactions beginning
february 1988.
    terms weren't disclosed.
    national said that more than four mln transactions are
expected to be processed for chrysler each year under the
contract.
 reuter
</text>]