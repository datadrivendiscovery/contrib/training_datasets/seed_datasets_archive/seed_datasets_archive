[<text>
<title>ibm &lt;ibm&gt; down on lower morgan stanley opinion</title>
<dateline>    new york, april 2 - </dateline>the stock of international business
machine corp declined slightly today and shares of digital
equipment corp &lt;dec&gt; rose after brokerage house morgan stanley
and co reiterated a negative opinion on ibm and a buy on
digital, traders said.
    ibm, which this morning introduced four new personal
computers, the first major overhaul of its pc line since it
entered the business in 1981, fell 1-3/4 to 149-3/8.
    digital equipment rose 1-3/8 to 163-5/8.
    analyst carol muratore of morgan stanley was unavailable
for comment.
</text>]