[<text>
<title>alleghany corp &lt;y&gt; declares 1987 dividend</title>
<dateline>     new york, march 18 - </dateline>alleghany corp said its board 
declared a stock dividend of one share of its common for every
50 shares outstanding, as the company's dividend on its company
for 1987.
    it said the dividend will be distributed on april 30, to
holders of record on march 30.
    alleghany said cash will be paid in lieu of any fractional
shares of its stock.
 reuter
</text>]