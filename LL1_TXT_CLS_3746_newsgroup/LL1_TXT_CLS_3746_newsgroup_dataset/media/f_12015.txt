[<text>
<title>allied supermarkets &lt;asu&gt; files for offering </title>
<dateline>    new york, april 1 - </dateline>allied supermarkets inc said it filed
with the securities and exchange commission a registration
statement covering 240 mln dlrs of debt securities.
    the company plans to offer 140 mln dlrs of senior
subordinated discount debentures and 100 mln dlrs of
subordinated debentures.
    proceeds will be used to finance allied's acquisition of
vons cos inc.
    allied named drexel burnham lambert inc and donaldson,
lufkin and jenrette securities corp as co-underwriters of both
offerings.
 reuter
</text>]