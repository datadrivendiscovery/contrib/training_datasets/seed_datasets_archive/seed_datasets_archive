[<text>
<title>carling o'keefe&lt;ckb&gt; sells oil unit, takes gain</title>
<dateline>    toronto, march 6 - </dateline>carling o'keefe ltd said it sold its
star oil and gas ltd unit to united coal (canada) ltd for about
57 mln dlrs cash.
    carling said it will record an extraordinary gain of about
two mln dlrs after tax, or nine cts a common share resulting
from the sale.
    the company did not elaborate further on financial terms.
    a carling official later said in reply to an inquiry that
carling would record the extraordinary gain in its fourth
quarter ending march 31.
    the move came after carling's 50 pct-owner &lt;rothmans inc&gt;
agreed last week to sell its carling stake to &lt;elders ixl ltd&gt;,
of australia, for 196.2 mln canadian dlrs.
 reuter
</text>]