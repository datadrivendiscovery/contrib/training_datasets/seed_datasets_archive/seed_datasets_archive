[<text>
<title>parlex &lt;prlx&gt; appoints chief operating officer</title>
<dateline>    methuen, mass., march 19 - </dateline>parlex corp said it named robert
cyr to its newly-created post of chief operating officer.
    cyr, 44, has been the company's senior vice president, the
company said.
 reuter
</text>]