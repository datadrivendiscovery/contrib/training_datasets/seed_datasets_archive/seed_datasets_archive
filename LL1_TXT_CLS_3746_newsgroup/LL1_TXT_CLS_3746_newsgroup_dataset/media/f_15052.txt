[<text>
<title>reader's digest association sells unit</title>
<dateline>    pleasantville, n.y., april 8 - </dateline>&lt;the reader's digest
association inc&gt; said it sold its subsidiary, source
telecomputing corp, to the venture capital firm of &lt;welsh,
carson, anderson and stowe&gt;.
    the purchase price was not disclosed, reader's digest said.
    it said it purchased an 80 pct stake in source in 1980 and
earned an unspecified profit on 14 mln dlrs in revenues in
1986.
 reuter
</text>]