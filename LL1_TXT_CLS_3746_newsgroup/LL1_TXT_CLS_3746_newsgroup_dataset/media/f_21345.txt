[<text>
<title>inland &lt;iad&gt; plans to proceed with plant</title>
<dateline>    chicago, oct 19 - </dateline>inland steel co said it and the uss
division of usx corp &lt;x&gt; are continuing to pursue plans for
construction of a state-of-the-art line for continuous
prepainting of steel coils.
    it said the proposed line will primarily serve the
appliance market.
    it said a location for the new plant has yet to be
selected.
 reuter
</text>]