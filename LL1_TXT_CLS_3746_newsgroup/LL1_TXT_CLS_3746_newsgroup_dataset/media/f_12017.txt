[<text>
<title>towle manufacturing co &lt;qtow&gt; year loss</title>
<dateline>    boston, april 1 -
    </dateline>oper shr loss 4.71 dlrs vs loss 14.09 dlrs
    oper loss 22 mln vs loss 67.2 mln
    note: 1986 loss excludes gain on the sale of gold lance
corp of 12.1 mln dlrs. company is operating under chapter 11.
 reuter
</text>]