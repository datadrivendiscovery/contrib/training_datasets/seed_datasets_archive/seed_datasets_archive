[<text>
<title>witco &lt;wit&gt; unit opens fast-oil change center</title>
<dateline>    irving, texas, june 1 - </dateline>kendall refining co, a unit of
witco corp, and the &lt;knox truck stop&gt; chain, said they have
developed one of the nation's first fast-oil-change centers
for trucks outside of dallas.
    the service, called fast lube, offers a 60-minute oil
change and inspection service for trucks and also involves a
used-oil analysis program that relays engine information to the
driver's fleet headquarters, the companies said.
    according to the companies, fast-oil-change centers are
expected to grow from the current three pct of the oil
lubrication market to 15 pct by 1990.
   
    in addition, kendall said it recently launched a national
fast lube program with &lt;avis inc&gt; that provides participating
avis lube franchises with lubricants, equipment and marketing
support.
 reuter
</text>]