[<text>
<title>suntrust banks inc &lt;sti&gt; 1st qtr net</title>
<dateline>    atlanta, april 9 -
    </dateline>shr 54 cts vs 49 cts
    net 70.2 mln vs 64.0 mln
    note: share adjusted for two-for-one split in july 1986.
    results restated for pooled acquisition of third national
corp in december 1986.
    net chargeoffs 15.0 mln dlrs vs 14.2 mln dlrs.
    assets 25.8 billion dlrs, up 7.2 pct from a year earlier,
deposits 21.1 billion, up 9.4 pct, and loans 17.1 billion dlrs,
up 17.2 pct.
 reuter
</text>]