[<text>
<title>network security &lt;ntwk.o&gt; holders to sell stock</title>
<dateline>    dallas, june 19 - </dateline>network security corp said three of its
major shareholders have executed definitive agreements to sell
their shares to inspectorate international &lt;insz.z&gt; sa.
    the company said its previously-announced merger into
inspectorate is still subject to approval by network
shareholders.
 reuter
</text>]