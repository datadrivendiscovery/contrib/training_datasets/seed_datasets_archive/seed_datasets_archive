[<text>
<title>first mississippi corp &lt;frm&gt; sets quarterly</title>
<dateline>    jackson, miss., june 1 -
    </dateline>qtly div five cts vs five cts prior
    pay july 28
    record june 30
 reuter
</text>]