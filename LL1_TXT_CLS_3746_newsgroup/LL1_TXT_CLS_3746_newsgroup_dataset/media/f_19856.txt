[<text>
<title>wal-mart &lt;wmt&gt; completes acquisition</title>
<dateline>    bentonville, ark, june 29 - </dateline>wal-mart stores inc said it has
completed the acquisition of &lt;super saver warehouse inc&gt; and
super saver is now a wholly owned unit of wal-mart.
 reuter
</text>]