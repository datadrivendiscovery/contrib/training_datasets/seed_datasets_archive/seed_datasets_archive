[<text>
<title>tseng laboratories inc &lt;tsng&gt; 4th qtr net</title>
<dateline>    newtown, pa., march 25 -
    </dateline>shr profit one ct vs loss nil
    net profit 200,052 vs loss 56,782
    revs 2,394,198 vs 706,393
    avg shrs 17.8 mln vs 19.8 mln
    year
    shr profit one ct vs profit one ct
    net profit 258,125 vs profit 164,553
    revs 4,225,731 vs 3,027,892
    avg shrs 17.9 mln vs 19.5 mln
 reuter
</text>]