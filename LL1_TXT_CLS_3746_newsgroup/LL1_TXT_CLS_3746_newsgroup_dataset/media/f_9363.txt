[<text>
<title>novamin in proposed buyout by breawater &lt;bwrlf&gt;</title>
<dateline>    toronto, march 25 - </dateline>&lt;novamin inc&gt; said it received a
proposed takeover offer from breakwater resources ltd involving
a swap of one breakwater share for two novamin common shares.
    it said the proposal also called for conversion of
outstanding novamin warrants into breakwater common shares on
the same basis, provided the exercise price was paid by the
warrant holders.
    novamin, a mineral exploration company, said directors
would meet next tuesday to deal with the proposal, which, it
said, was subject to approval by breakwater directors.
 reuter
</text>]