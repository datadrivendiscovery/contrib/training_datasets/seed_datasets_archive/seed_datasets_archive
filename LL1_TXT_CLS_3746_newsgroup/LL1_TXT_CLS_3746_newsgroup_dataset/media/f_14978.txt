[<text>
<title>i.u. international &lt;iu&gt; to sell insurance units</title>
<dateline>    philadelphia, april 8 - </dateline>i.u. international co said it
reached a preliminary agreement to sell the hawaiian insurance
cos to hawaiian electric industries inc. &lt;he&gt;.
    terms of the transaction were not disclosed, the company
said.
    the transaction is subject to the execution of definitive
agreements, certain governmental approvals and approvals by the
boards of directors involved, i.u. international said.
    hawaiian electric said the planned purchase was part of its
strategy to increase the company's investment in selected
service industries in hawaii, including financial services.
 reuter
</text>]