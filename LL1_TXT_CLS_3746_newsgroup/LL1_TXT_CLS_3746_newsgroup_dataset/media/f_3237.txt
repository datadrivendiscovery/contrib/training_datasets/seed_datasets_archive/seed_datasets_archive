[<text>
<title>angio-medical in initial offering</title>
<dateline>    new york, march 9 - </dateline>&lt;angio-medical corp&gt; said it filed with
the securities and exchange commission for an initial public
offering of 1,666,667 units of one share of common and one
redeemable warrant to buy one share of common at six dlrs per
unit.
    it said keane securities co inc &lt;kean&gt; is the underwriter.
    the company said its securities are proposed for trading on
nasdaq under the symbol &lt;angmu&gt;.
 reuter
</text>]