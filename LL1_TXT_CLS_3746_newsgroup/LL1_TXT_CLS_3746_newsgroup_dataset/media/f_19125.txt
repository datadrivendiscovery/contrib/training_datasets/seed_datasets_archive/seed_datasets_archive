[<text>
<title>vms mortgage investors lp &lt;vmlpz.o&gt; in payout</title>
<dateline>    chicago, june 19 -
    </dateline>mthly div nine cts vs nine cts prior
    pay aug 14
    record july one
 reuter
</text>]