[<text>
<title>&lt;burke-parsons-bowlby corp&gt; sets quarterly</title>
<dateline>    ripley, w.va., june 18 -
    </dateline>qtly div three cts vs three cts prior
    pay aug 14
    record july 24
 reuter
</text>]