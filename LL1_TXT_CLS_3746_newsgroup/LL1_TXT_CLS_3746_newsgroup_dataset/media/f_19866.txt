[<text>
<title>united medical corp &lt;um&gt; sells unit</title>
<dateline>    haddonfield, n.j., june 29 - </dateline>united medical corp said it
will sell its trotter treadmills inc unit.
    the company said the sale is in line with its strategy of
refocusing on its health care service business. it said it had
received interest from several parties, but no agreement has
yet been reached.
    trotter treadmills makes motorized treadmills for the
exercise enthusiast and fitness club market. it said its sales
for this year are projected to be over 10 mln dlrs.

 reuter
</text>]