[<text>
<title>gulf and western inc &lt;gw&gt; sets quarterly payout</title>
<dateline>    new york, june 2 -
    </dateline>qtly div 30 cts vs 30 cts
    pay july one
    record june 12
 reuter
</text>]