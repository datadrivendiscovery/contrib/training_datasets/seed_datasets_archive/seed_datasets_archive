[<text>
<title>dune resources &lt;dnlaf&gt; reserves rose in 1986</title>
<dateline>    oklahoma city, march 11 - </dateline>dune resources ltd said its oil
reserves increased 225 pct during 1986 while its natural gas
reserves were up six pct.
    the company said its proven oil reserves were estimated at
605,682 barrels on december 31, up from 186,655 barrels a year
earlier boosted by discoveries during the year put at 423,659
barrels.
    it said gas reserves rose to 8.8 mln cubic feet from 8.3
mln on dec 31, 1985, as discoveries of nearly 1.3 mln cubic
feet were partialy offset by production of 287,391 cubic feet
and downward revisions of previous estimates totaling 491,694
cubic feet.
 reuter
</text>]