[<text>
<title>new plan realty trust &lt;npr&gt; 2nd qtr net</title>
<dateline>    new york, march 16 - </dateline>qtr ends jan 31
    shr 22 cts vs 19 cts
    net 4,549,000 vs 3,666,000
    revs 8,903,000 vs 7,791,000
    avg shrs 20.9 mln vs 19.9 mln
    six mths
    shr 42 cts vs 41 cts
    net 8,641,000 vs 7,928,000
    revs 17.5 mln vs 14.6 mln
    avg shrs 20.8 mln vs 19.4 mln
    note: earnings were restated to reflect the three-for-two
stock split on april one, 1986.
 reuter
</text>]