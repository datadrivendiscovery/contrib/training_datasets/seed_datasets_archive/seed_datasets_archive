[<text>
<title>frank b. hall &lt;fbh&gt; earnings report qualified</title>
<dateline>    briarcliff manor, n.y., march 19 - </dateline>frank b. hall and co inc
said its independent accountants will issue a qualified report
on its financial statements.
    the company said the opinion is the result of ongoing
litigation over its discontinued operations. earlier it said it
lost 2.1 mln dlrs in the fourth quarter against a loss of 2.3
mln dlrs a year ago before discontinued operations.
    it also said it decided to sell its claims adjusting and
admininistrative operations and its automobile dealer insurance
unit, creating a reserve of about nine mln dlrs in the fourth
quarter 1986 for the sale.
    the company said it will vigorously defend the litigation
arising from the discontinued units.
    it also said it will concentrate on its direct brokerage
and service business.
 reuter
</text>]