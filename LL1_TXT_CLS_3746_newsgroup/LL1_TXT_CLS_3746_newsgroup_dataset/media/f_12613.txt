[<text>
<title>hitech engineering co &lt;thex&gt; year net</title>
<dateline>    mclean, va., april 2 -
    </dateline>shr profit nil vs loss 10 cts
    net profit 19,000 vs loss 1,825,000
    revs 2,611,000 vs 610,000
    avg shrs 19.3 mln vs 18.0 mln
    note: 1986 net includes 4,000 dlr tax credit.
 reuter
</text>]