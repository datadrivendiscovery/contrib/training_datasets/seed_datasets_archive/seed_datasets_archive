[<text>
<title>weisfield's inc &lt;weis&gt; 4th qtr net</title>
<dateline>    seattle, march 19 -
    </dateline>shr 1.09 dlrs vs 1.20 dlrs
    net 1,193,000 vs 1,361,000
    revs 18.7 mln vs 17.3 mln
    year
    shr 1.19 dlrs vs 1.52 dlrs
    net 1,300,000 vs 1,831,000
    revs 50.6 mln vs 48.8 mln
 reuter
</text>]