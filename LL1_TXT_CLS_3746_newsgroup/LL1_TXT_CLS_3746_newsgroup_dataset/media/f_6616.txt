[<text>
<title>financial security &lt;fssla&gt; to sell branch</title>
<dateline>    delray beach, fla., march 18 - </dateline>financial security savings
and loan association said it has agreed to sell its sunrise,
fla., branch to fortune financial group inc &lt;forf&gt; of
clearwater, fla., for a "substantial profit," subject to
regulatory approval.
    terms were not disclosed.
 reuter
</text>]