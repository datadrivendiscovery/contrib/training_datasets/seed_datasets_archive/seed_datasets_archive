[<text>
<title>packaging systems &lt;paks&gt; to buy label firm</title>
<dateline>    pearl river, n.y., march 13 - </dateline>packaging systems corp said
it agreed to acquire &lt;walter-richter labels inc&gt;, a privately
held maker of woven labels based in paterson, n.j.
    terms of the acquisition, which is expected to be completed
within 60 days, were not disclosed.
 reuter
</text>]