[<text>
<title>bond corp completes consolidated press purchase</title>
<dateline>    perth, april 1 - </dateline>bond corp holdings ltd &lt;bona.s&gt; said it
has completed the 1.05 billion dlr purchase of the electronic
media interests of unlisted &lt;consolidated press holdings ltd&gt;.
    the new company &lt;bond media ltd&gt; now holds the television,
broadcasting and associated businesses previously held by kerry
packer's consolidated, bond corp said in a statement.
    packer, who made the sale in january, will be a director of
bond media. as previously reported, bond media will be publicly
floated with a rights issue to bond corp shareholders. bond
media will be 50 pct owned by bond corp and is expected to be
listed by the end of may, it said.
 reuter
</text>]