[<text>
<title>rexnord &lt;rex&gt; sees may merger with banner &lt;bnr&gt;</title>
<dateline>    milwaukee, wis., march 19 - </dateline>rexnord inc said it expects to
merge with a wholly-owned subsidiary of banner industries inc
in early may.
    late last month, banner said it completed a tender offer
for and held 96 pct of rexnord's common shares.
 reuter
</text>]