[<text>
<title>presidential airways &lt;pair.o&gt; unit adds service</title>
<dateline>    washington, april 13 - </dateline>presidential airways said its
continental jet express will begin daily non-stop service
between dulles international airport here and akron/canton,
ohio, and the lexington/frankfort area of kentucky.
    presidential reiterated its marketing agreement with texas
air corp's &lt;tex&gt; continental airlines on jan 12 which calls for
presidential to do business as continental jet express.
 reuter
</text>]