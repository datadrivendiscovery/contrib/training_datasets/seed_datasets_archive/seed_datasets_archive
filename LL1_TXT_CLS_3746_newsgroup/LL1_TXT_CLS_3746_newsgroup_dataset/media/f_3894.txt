[<text>
<title>invacare corp &lt;ivcr&gt; 4th qtr net</title>
<dateline>    elyria, ohio, march 11 -
    </dateline>shr profit 26 cts vs profit 19 cts
    net profit 1,458,000 dlrs vs profit 1,070,000 dlrs
    rev 30.9 mln dlrs vs 27.5 mln dlrs
    12 mths
    shr profit 60 cts vs loss 19 cts
    net profit 3,367,000 dlrs vs loss 1,061,000 dlrs
    revs 111.5 mln dlrs vs 94.3 mln dlrs
 reuter
</text>]