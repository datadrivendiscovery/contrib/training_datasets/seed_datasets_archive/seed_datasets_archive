[<text>
<title>japan to ratify 1986 international cocoa agreement</title>
<dateline>    tokyo, june 29 - </dateline>japan will ratify the 1986 international
cocoa agreement (icca), with effect from july 1, and will renew
its membership in the international cocoa organisation (icco),
foreign affairs ministry officials said.
    they said japan would participate in an icco meeting on
july 13 in london to revise icco buffer stock policy.
    the 1986 icca has been in effect since january 1987.
    government approval to participate in the icco was delayed
by parliamentary debate over the sales tax issue, the officials
said.
 reuter
</text>]