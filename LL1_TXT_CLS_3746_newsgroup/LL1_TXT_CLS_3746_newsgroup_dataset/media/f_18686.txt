[<text>
<title>freedom federal &lt;frfe.o&gt; seeks buyer</title>
<dateline>    oak brook, ill., june 18 - </dateline>freedom federal savings bank
said it hired salomon brothers inc to solicit offers for the
purchase of the bank as part of an ongoing review of methods to
enhance shareholder value.
    freedom federal operates 15 retail branch banks and had
1986 year end assets of about 733 mln dlrs.
    "there is no assurance the bank will receive acceptable
offers or be sold, but we feel this is a prudent step to take
at this time," the bank said.
 reuter
</text>]