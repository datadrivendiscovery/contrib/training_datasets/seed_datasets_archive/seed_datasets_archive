[<text>
<title>mobil france to take 10 pct stake in primagaz</title>
<dateline>    paris, march 30 - </dateline>mobil corp's &lt;mob&gt; mobil oil francaise
unit said it will take a stake of about 10 pct in the french
butane and propane gas distribution company &lt;primagaz&gt; in
exchange for the transfer to primagaz of mobil's small and
medium bulk propane activity.
    small and medium bulk propane sales totalled 55,000 tonnes
in 1986 and the transfer will increase total business of
primagaz by about 12 pct, equal to 32,000 extra customers.
    a primagas spokesman said mobil will take the stake by
means of a capital increase, terms of which have not yet been
established.
</text>]