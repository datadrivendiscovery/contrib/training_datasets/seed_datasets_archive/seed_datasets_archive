[<text>
<title>aar &lt;air&gt; eleasing unit sells five aircraft</title>
<dateline>    elk grove village, ill., june 2 - </dateline>aar corp said its aar
financial services corp nonconsolidated leasing subsidiary sold
its beneficial interest in five used boeing 737-200 aircraft.
    in march, 1985 the aircraft were acquired from and leased
back to piedmont aviation inc &lt;pie&gt;.
    aar said the beneficial interest was sold to cis corp of
san francisco.
 reuter
</text>]