[<text>
<title>mexico veg oil tax not aimed at sunflower--usda</title>
<dateline>    washington, march 11 - </dateline>the mexican secretariat of commerce
has told the u.s. that recent implementation of a 10 pct ad
valorem tariff for fixed vegetable oils, fluid or solid, crude,
refined or purified, was not targeted at sunflower oil, the
u.s. agriculture department said.
    in its world production and trade report, the department
said the increase in tariffs on this category which includes
sunflower, corn and rapeseed oils, was a reaction to importers
using basket categories to avoid paying tariffs on specific
high-tariff products.
 reuter
</text>]