[<text>
<title>prime computer &lt;prm&gt; to buy back stock</title>
<dateline>    natick, mass., oct 20 - </dateline>prime computer inc said it will
periodically repurchase an unspecified number of its common
stock on the open market.
    the company has 49.1 mln shares outstanding.
    it said it stock will be bought in periods of price
weakness and that the stock will be used for the company's
restricted stock and employee stock purchase plans and for
other corporate purposes.
 reuter
</text>]