[<text>
<title>great country bank &lt;gcbk&gt; executive resigns</title>
<dateline>    ansonia, conn., march 30 - </dateline>great country bank said william
mcdougall has resigned as executive vice president and
secretary, and his reponsibilities have been assumed by other
officers.
 reuter
</text>]