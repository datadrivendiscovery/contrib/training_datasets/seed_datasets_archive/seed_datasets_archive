[<text>
<title>commonwealth edison co &lt;cwe&gt; 12 months net</title>
<dateline>    chicago, march 20 - </dateline>period ended feb 28
    shr 4.66 dlrs vs 4.40 dlrs
    net 1,048,884,000 vs 959,626,000
    revs 5.43 billion vs 5.04 billion
    avg shrs 200,242,000 vs 191,840,000
    note: per-share earnings reflect payment of preferred
dividend requirements
 reuter
</text>]