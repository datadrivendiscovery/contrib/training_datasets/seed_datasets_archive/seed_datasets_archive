[<text>
<title>toyota, vw consider upping joint procurement rate</title>
<dateline>    tokyo, oct 19 - </dateline>toyota motor corp &lt;toyo.t&gt; and volkswagen
ag &lt;vowg.f&gt; are considering raising the local procurement rate
at their west german joint venture to at least 60 pct from an
earlier planned 50 pct after one year's output, a toyota
spokesman said.
    the two carmakers agreed last june to start jointly
producing a toyota-designed light truck at vw's hanover plant
from early 1989 at a rate of 7,000 to 8,000 the first year,
rising to 15,000 from 1990.
    they may equip the light truck with a volkswagen-made
engine to reach a local procurement rate of 60 pct, he said.

    the two companies need to raise the value of locally
procured parts to 60 pct to avoid tariffs when they export to
other european markets, industry sources said.

 reuter
</text>]