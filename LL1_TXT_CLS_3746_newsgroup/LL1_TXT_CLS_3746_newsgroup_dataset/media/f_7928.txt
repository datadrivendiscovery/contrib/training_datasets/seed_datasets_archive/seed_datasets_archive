[<text>
<title>commercial credit &lt;ccc&gt; unit sells division</title>
<dateline>    baltimore, md., march 20 - </dateline>commercial credit co said its
american health and life insurance co sold its ordinary life
insurance business to american national insurance co &lt;anat&gt;.
    american national will assume the business no later than
august 31, 1987, commercial credit said.
    the sale is part of a restructuring program begun by
commercial credit's subsidiary in late 1986, the company said.
 reuter
</text>]