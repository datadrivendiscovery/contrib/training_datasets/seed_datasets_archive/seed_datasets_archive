[<text>
<title>engelhard builds plant in japan</title>
<dateline>    edison, n.j., march 11 - </dateline>engelhard corp &lt;ec&gt; said its
japanese joint venture nippon engelhard ltd is constructing a
new chemical catalyst manufacturing facility at its numazu
complex and expanding its research laboratories at ichikawa.
    start-up is targeted for third quarter 1987.
 reuter
</text>]