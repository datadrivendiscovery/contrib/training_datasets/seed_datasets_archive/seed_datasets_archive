[<text>
<title>&lt;anchor financial corp&gt; 1st qtr net</title>
<dateline>    myrtle beach, s.c., april 3 -
    </dateline>shr 31 cts vs 31 cts
    net 226,000 vs 173,000
    assets 73.1 mln vs 62.5 mln
    deposits 54.6 mln vs 51.5 mln
    note: earnings per share for 1987 affected by issuance of
166,750 shares of common stock in december 1986.
 reuter
</text>]