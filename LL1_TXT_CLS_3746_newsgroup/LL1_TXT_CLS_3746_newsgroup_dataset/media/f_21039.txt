[<text>
<title>durakon &lt;drkn.o&gt; to make acquisition</title>
<dateline>    lapeer, mich., oct 19 - </dateline>durakon industries inc said it has
entered into a definitive agreement to acquire dfm corp, a
maker of bug and gravel protective shields for trucks and cars,
for an undisclosed amount of cash and debentures, retroactive
to september one.
 reuter
</text>]