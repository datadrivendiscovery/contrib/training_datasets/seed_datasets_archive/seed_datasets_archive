[<text>
<title>enzo biochem inc &lt;enzo&gt; 2nd qtr jan 31 net</title>
<dateline>    new york, march 13 -
    </dateline>shr one ct vs three cts
    net 123,000 vs 371,000
    revs 2,944,000 vs 2,138,000
    avg shrs 11.4 mln vs 11.6 mln
    six mths
    shr five cts vs six cts
    net 531,000 vs 725,000
    revs 6,200,000 vs 4,128,000
    avg shrs 11.4 mln vs 11.6 mln
 reuter
</text>]