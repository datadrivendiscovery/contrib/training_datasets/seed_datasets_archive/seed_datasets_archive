[<text>
<title>dinner bell &lt;dinb.o&gt; leverage buy out dropped</title>
<dateline>    defiance, ohio, oct 20 - </dateline>dinner bell foods inc said the
talks concerning a proposed leveraged buy-out of the company
have been terminated.
    a spokesman said the group led by joseph f. grimes ii, a
director of the company, and b. rober kill has withdrawn their
proposal to acquire the company's stock for 23.50 dlrs a share.
    the company also said its board determined the previously
postponed annual meeting will be held on january five.
 reuter
</text>]