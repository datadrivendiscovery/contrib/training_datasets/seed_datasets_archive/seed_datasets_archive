[<text>
<title>jmb realty trust &lt;jmbrs&gt; 2nd qtr feb 28 net</title>
<dateline>    chicago, march 30 -
    </dateline>shr 31 cts vs 41 cts
    net 436,981 vs 583,715
    six months
    shr 63 cts vs 82 cts
    net 901,648 vs 1,160,178
 reuter
</text>]