[<text>
<title>fed adds reserves via customer repurchases</title>
<dateline>    new york, june 18 - </dateline>the federal reserve entered the
government securities market to supply temporary reserves
indirectly via two billion dlrs of customer repurchase
agreements, a spokesman for the new york fed said.
    fed funds were trading at 6-3/4 pct when the fed began
its action.
 reuter
</text>]