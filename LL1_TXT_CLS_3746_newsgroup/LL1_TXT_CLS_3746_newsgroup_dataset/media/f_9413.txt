[<text>
<title>int'l broadcasting &lt;ibca&gt; sets reverse split</title>
<dateline>    minneapolis, minn., march 25 - </dateline>international broadcasting
corp said shareholders at its annual meeting approved a one for
25 reverse stock split.
    the split will be effective after completion of filing
requirements, it said. new certificates will be needed, it
added.
    the media company said it currently has 40,950,000 common
shares issued and outstanding and, upon completion of the
reverse split, will have 1,638,000 shares outstanding.
 reuter
</text>]