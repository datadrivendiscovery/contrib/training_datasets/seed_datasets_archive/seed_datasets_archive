[<text>
<title>fed adds reserves via three-day repurchases</title>
<dateline>    new york, march 16 - </dateline>the federal reserve entered the u.s.
government securities market to arrange three-day system
repurchase agreements, a fed spokesman said.
    dealers said that federal funds were trading at 6-1/4 pct
when the fed began its temporary and direct supply of reserves
to the banking system.
 reuter
</text>]