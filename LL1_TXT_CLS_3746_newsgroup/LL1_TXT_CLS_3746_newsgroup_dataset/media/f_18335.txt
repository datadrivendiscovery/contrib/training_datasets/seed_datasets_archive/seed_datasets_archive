[<text>
<title>watkins-johnson &lt;wj&gt; wins contract</title>
<dateline>    palo alto, calif, june 2 - </dateline>watkins-johnson co said it
received a contract from general electric corp's &lt;ge&gt; g.e.
astro space division for nearly two mln dlrs to provide
traveling-wave-tube amplifiers on the mars observer spacecraft.
    the observer is scheduled to be launched into mars orbit in
the early 1990s.
 reuter
</text>]