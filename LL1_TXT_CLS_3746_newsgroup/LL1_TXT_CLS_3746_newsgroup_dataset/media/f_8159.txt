[<text>
<title>india bought 24,000 tonnes of rbd olein at tender</title>
<dateline>    london, march 23 - </dateline>the indian state trading corporation
(stc) bought four cargoes of rbd palm olein totalling 24,000
tonnes at its vegetable oil import tender last week, traders
said. market reports on friday said the stc had booked two
cargoes.
    the business comprised three 6,000 tonne cargoes for june
at 346 dlrs and 6,000 tonnes for july at 340 dlrs per tonne
cif.
    it also secured a 20,000 tonne cargo of optional origin
rapeseed oil for may 15/jun 15 shipment at 321 dlrs cif.
 reuter
</text>]