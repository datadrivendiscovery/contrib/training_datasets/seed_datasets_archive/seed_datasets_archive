[<text>
<title>american equine &lt;whoa&gt; files for offering</title>
<dateline>    south norwalk, conn., march 19 - </dateline>american equine products
inc said it has signed a letter of intent for a proposed
underwritten public offering.  it gave no details.
 reuter
</text>]