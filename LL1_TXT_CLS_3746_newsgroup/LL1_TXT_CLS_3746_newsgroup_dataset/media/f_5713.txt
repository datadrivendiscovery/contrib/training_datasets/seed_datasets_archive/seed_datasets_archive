[<text>
<title>ameritrust &lt;amtr&gt; sells two mln shares</title>
<dateline>    cleveland, march 16 - </dateline>ameritrust corp said it sold two mln
shares of its common stock to an investment group named
clevebaco ltd partnership.
    the partnership is controlled by alfred lerner, and a
subsidiary of the progressive corp &lt;prog&gt; is the limited
partner, according to the company.
    ameritrust said it was advised that the clevebaco group has
filed an application with bank regulatory authorities seeking
permission to acquire up to an additional three mln shares of
ameritrust common stock.
    the company said the group indicated to it that it had no
hostile takeover intent toward ameritrust, and that the
purchase was for investment purposes.
 reuter
</text>]