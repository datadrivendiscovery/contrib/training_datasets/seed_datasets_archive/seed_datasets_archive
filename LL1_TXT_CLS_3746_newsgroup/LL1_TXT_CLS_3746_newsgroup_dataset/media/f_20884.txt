[<text>
<title>granges &lt;gxl&gt; files for common stock offering</title>
<dateline>    washington, oct 19 - </dateline>granges exploration ltd filed with the
securities and exchange commission for a proposed offering of
up to 2,500,000 shares of common stock.
    painewebber inc and drexel burnham lambert inc will act as
underwriters for the proposed offering, granges said.
    proceeds from the offering will be used to acquire shares
of its hycroft resources and development corp subsidiary, and
as working capital to fund exploration, development and
acquisitions of additional mining properties.
 reuter
</text>]