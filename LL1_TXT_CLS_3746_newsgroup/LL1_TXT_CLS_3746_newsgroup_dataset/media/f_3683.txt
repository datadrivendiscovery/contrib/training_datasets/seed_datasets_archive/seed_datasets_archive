[<text>
<title>tenneco &lt;tgt&gt; declines comment on stock</title>
<dateline>    new york, march 11 - </dateline>the new york stock exchange said
tenneco inc declined to comment on whether there were any
corporate developments that might explain the unusual activity
in its stock.
    the exchange said it had requested a statement from the
company in light of the activity. tenneco was up 1-7/8 dlrs to
48-5/8.
 reuter
</text>]