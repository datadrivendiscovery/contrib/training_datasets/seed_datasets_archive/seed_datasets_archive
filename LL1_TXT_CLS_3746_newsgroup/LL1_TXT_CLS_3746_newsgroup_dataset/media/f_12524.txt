[<text>
<title>kloeckner sees further growth in engineering</title>
<dateline>    hanover, west germany, april 2 - </dateline>kloeckner-werke ag
&lt;klkg.f&gt; should have turnover this year around 1985/86's 2.4
billion marks though more growth is likely in engineering in
coming years, management board chairman herbert gienow said.
    he told a news conference at the trade fair here that by
the mid-1990s turnover should reach between six and seven
billion marks, mainly through acquisitions totalling "several
hundred million marks."
    kloeckner reported in march higher profits in its
engineering sector which enabled it to raise profits by nearly
a third in the 1985/86 year to 45.2 mln marks from 33.8 mln.
 reuter
</text>]