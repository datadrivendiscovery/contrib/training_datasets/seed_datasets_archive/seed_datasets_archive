[<text>
<title>morton thiokol &lt;mti&gt; gets 61.4 mln dlr contract</title>
<dateline>    washington, march 20 - </dateline>morton thiokol inc's wasatch
operations is being awarded a 61.4 mln dlr contract
modification finalizing a previously awarded contract for
missile rocket motors for terrier, tarter and aegis ships, the
defense department said.
    it said the work is expected to be completed in october 
1987.
 reuter
</text>]