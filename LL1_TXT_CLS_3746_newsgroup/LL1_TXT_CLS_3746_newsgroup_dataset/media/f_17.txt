[<text>
<title>national health enhancement &lt;nhes&gt; new program</title>
<dateline>    phoenix, ariz., feb 26 - </dateline>national health enhancement
systems inc said it is offering a new health evaluation system
to its line of fitness assessment programs.
    the company said the program, called the health test, will
be available in 60 days.
    customers who use the program will receive a
computer-generated report and recommendations for implementing
a program to improve their physical condition.
 reuter
</text>]