[<text>
<title>national royalty corp &lt;nroc&gt; 4th qtr loss</title>
<dateline>    tulsa, oklahoma, april 1 -
    </dateline>shr loss 22 cts vs loss 20 cts
    net loss 2,127,334 vs loss 1,629,432
    revs 1,306,658 vs 1,091,023
    avg shrs 9.7 mln vs 8 mln
    year
    shr loss 36 cts vs loss 35 cts
    net loss 3,519,251 vs loss 2,805,569
    revs 5,081,953 vs 4,410,954
    avg shrs 9.8 mln vs 8.1 mln
 reuter
</text>]