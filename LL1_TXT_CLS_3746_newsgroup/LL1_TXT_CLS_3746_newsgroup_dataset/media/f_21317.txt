[<text>
<title>household mortgage gets u.s. paper program</title>
<dateline>    london, oct 19 - </dateline>&lt;household mortgage corp&gt; said it has
arranged a 200 mln dlr commercial paper program in the u.s.,
for which goldman sachs and co will be the sole dealer.
    the program will be backed by a letter of credit from
australia and new zealand banking corp.
    the program also is supported by a multiple option facility
arranged in the euromarkets in august.
 reuter
</text>]