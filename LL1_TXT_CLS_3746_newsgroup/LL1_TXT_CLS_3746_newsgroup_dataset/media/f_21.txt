[<text>
<title>hughes capital unit signs pact with bear stearns</title>
<dateline>    fort lauderdale, fla., feb 26 - </dateline>hughes/conserdyne corp, a
unit of &lt;hughes capital corp&gt; said it made bear stearns and co
inc &lt;bsc&gt; its exclusive investment banker to develop and market
financing for the design and installation of its micro-utility
systems for municipalities.
    the company said these systems are self-contained
electrical generating facilities using alternate power sources,
such as photovoltaic cells, to replace public utility power
sources.
 reuter
</text>]