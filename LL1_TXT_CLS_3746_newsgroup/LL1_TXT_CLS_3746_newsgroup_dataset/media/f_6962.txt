[<text>
<title>foreign share in japan bond syndicate boosted</title>
<dateline>     tokyo, march 19 - </dateline>the finance ministry approved the
banking industry's plan to cut its share in the government bond
underwriting syndicate to 73.8 pct from 74 pct from april 1 to
allow six foreign securities firms to participate, a ministry
spokesman said.
    local and foreign securities housess may now underwrite
26.2 pct of 10-year bonds, up from 26 pct previously.
    under a separate agreement, japanese and foreign brokers
will allow the latter, including the six newcomers, to
underwrite five pct of the enlarged share, up from 1.19 pct,
underwriting sources said.
    the six new foreign brokers are &lt;e.f. hutton and co inc&gt;,
&lt;shearson lehman brothers asia inc&gt;, dresdner bank ag &lt;drsd.f&gt;,
&lt;swiss bank corp international asia inc&gt;, &lt;sogen security corp&gt;
and &lt;swiss union philips and drew ltd&gt;.
    the syndicate agreed after negotiation in april 1982 that
26 pct of 10-year government bonds should be underwritten by 93
securities firms, 17 of them foreign, and 74 pct by banks. the
ministry later approved the arrangement.
    the finance ministry is also considering public tender of
notes of over four-year maturity, adding to the current two,
three and four-year note auction, the ministry official said.
    the ministry has decided to lower the eligibility standard
for foreign brokers to participate in government note auctions
by abolishing a requirement that participants have current
accounts with the bank of japan, the official said.
    a request by four foreign banks to join the bond
underwriting syndicate is being considered, banking sources
said. the four are local subsidiaries of continental illinois
holding corp's &lt;cih.n&gt; belgian unit &lt;continental bank sa/nv&gt;,
amsterdam-rotterdam bank nv &lt;amro.as&gt;, &lt;canadian commercial
bank&gt; and &lt;union de banques arabes et francaises&gt;, they said.
 reuter
</text>]