[<text>
<title>insilco corp &lt;inr&gt; sets payout</title>
<dateline>    meriden, conn., june 18 -
    </dateline>qtly div 25 cts vs 25 cts prior
    pay aug one
    record july 10
 reuter
</text>]