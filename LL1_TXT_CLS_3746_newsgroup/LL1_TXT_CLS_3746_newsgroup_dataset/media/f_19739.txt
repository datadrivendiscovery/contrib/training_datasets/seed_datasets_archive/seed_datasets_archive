[<text>
<title>intel &lt;inct.o&gt; sets pact with silicon compiler</title>
<dateline>    san jose, calif, june 29 - </dateline>intel corp said it has signed a
letter of intent with privately-held silicon compiler systems
corp under which intel will become a value-added silicon vendor
for integrated circuits designed with silicon compiler's
genesil silicon compilation system.
    under the proposed pact, intel would provide initial
silicon foundry services for genesil-based designs.
    genesil users would be able to compile their
application-specific designs using intel's 1.5-micron
complementary metal-oxide semiconductor process.
   
 reuter
</text>]