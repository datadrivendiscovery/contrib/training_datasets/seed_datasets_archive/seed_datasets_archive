[<text>
<title>official says reagan against early sdi deployment</title>
<dateline>    washington, march 6 - </dateline>a senior defense department official
said he believed president reagan has decided against an early
deployment of the controversial anti-missile defense system
known as "star wars."
    but the official, assistant defense secretary richard
perle, told a luncheon reagan could decide within a few weeks
to adopt a less restrictive interpretation of the 1972
anti-ballistic missile (abm) treaty to justify a move towards
wider "star wars" testing and development.
    "i think the president has made a decision not to proceed
with an early deployment of sdi," said perle, referring to the
strategic defense initiative, as reagan's anti-missile project
is formally called.
   
 reuter
</text>]