[<text>
<title>chicago rivet and machine co &lt;cvr&gt; 4th qtr net</title>
<dateline>    naperville, ill., march 13 -
    </dateline>shr 21 cts vs 60 cts
    net 156,576 vs 443,404
    sales 5,309,519 vs 5,381,264
    year
    shr 1.06 dlrs vs 1.55 dlrs
    net 788,220 vs 1,151,330
    sales 22.3 mln vs 23.6 mln
 reuter
</text>]