[<text>
<title>american aircraft buys into helicopter builder</title>
<dateline>    san francisco, march 3 - </dateline>&lt;american aircraft corp&gt; said it
has acquired a 51 pct interest in privately-owned &lt;hunter
helicopter of nevada inc&gt; for an undisclosed amount of stock.
    an american aircraft official said the company has an
option to acquire the remaining 49 pct.
    hunter helicopter is in the business of building a two
passenger helicopter retailing for about 50,000 dlrs each which
is certified by the federal aviation administration. the
helicopters will be manufactured in american aircraft's uvalde,
texas, plant, it added.
 reuter
</text>]