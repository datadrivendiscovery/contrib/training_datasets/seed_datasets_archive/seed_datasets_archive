[<text>
<title>toronto dominion plans green line announcement</title>
<dateline>    toronto, march 5 - </dateline>&lt;toronto dominion bank&gt; will hold a news
conference tomorrow to make "a major announcement" about its 
green line investor service, which provides discount brokerage
services to customers, a bank spokesman said.
    the spokesman declined further comment except to say bank
president robert korthals would be available to answer
questions tomorrow.
    toronto dominion started green line in february 1984 and
became the first canadian bank to offer discount brokerage
services. the ontario government recently said banks will be
permitted to buy brokerage firms after june 30.

 reuter
</text>]