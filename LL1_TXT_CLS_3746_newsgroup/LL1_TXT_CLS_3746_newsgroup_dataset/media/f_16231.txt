[<text>
<title>cooper canada &lt;cpc.to&gt; sets share consolidation</title>
<dateline>    toronto, april 13 - </dateline>cooper canada ltd said it planned to
consolidate its common and class a non-voting shares into one
class of common shares, subject to shareholder approval on may
1.
    cooper canada said the proposal would result in only voting
shares being available to respond to charan industries ltd
&lt;chn.to&gt;'s previously announced six dlr a share takeover bid.
 reuter
</text>]