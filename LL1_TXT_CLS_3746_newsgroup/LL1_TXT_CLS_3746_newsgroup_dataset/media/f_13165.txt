[<text>
<title>biw cable systems inc &lt;biwc&gt; 4th qtr loss</title>
<dateline>    boston, april 3 -
    </dateline>shr loss 1.50 dlrs vs loss 14 cts
    net loss 3,395,933 vs loss 318,225
    revs 8,963,097 vs 12.6 mln
    year
    shr loss 2.93 dlrs vs profit two cts
    net loss 6,613,327 vs profit 49,421
    revs 44.4 mln vs 48.7 mln
    note: current periods include 2.2 mln dlr charge for
possible obsolete inventory and provision of 356,000 dlrs for
consolidation of cable manufacturing plant.
 reuter
</text>]