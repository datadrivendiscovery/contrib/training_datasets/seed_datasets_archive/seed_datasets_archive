[<text>
<title>&lt;cooper canada ltd&gt; year net</title>
<dateline>    toronto, march 16 -
    </dateline>shr 23 cts vs 42 cts
    net 1,387,000 vs 2,532,000
    revs 80.5 mln vs 82.6 mln
 reuter
</text>]