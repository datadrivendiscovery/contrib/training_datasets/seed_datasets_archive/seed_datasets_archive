[<text>
<title>fed not expected to add reserves</title>
<dateline>    new york, march 12 - </dateline>the federal reserve is not expected to
intervene in the government securities market today, several
economists said.
    they said the fed does not have a much of an adding need
this week and may wait until tomorrow or monday before
supplying reserves.
    but a few economists said there was an outside chance that
the fed may inject reserves indirectly via a small round of
customer repurchase agreements.
    federal funds hovered at 6-1/8 pct this morning after
averaging 6.32 pct yesterday.
 reuter
</text>]