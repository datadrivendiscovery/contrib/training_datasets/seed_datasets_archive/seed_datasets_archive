[<text>
<title>fab industries inc &lt;fit&gt; 1st qtr feb 28 net</title>
<dateline>    new york, april 9 -
    </dateline>shr 69 cts vs 67 cts
    net 2,488,000 vs 2,435,000
    revs 27.6 mln vs 26.5 mln
 reuter
</text>]