[<text>
<title>basix corp &lt;bas&gt; to sell unit to cubic &lt;cub&gt;</title>
<dateline>    new york, june 2 - </dateline>basix corp said it has agreed in
principle to sell the stock of its automatic toll systems inc
subsidiary to cubic corp for about 26 mln dlrs.
    the company said it would retain automatic toll assets
worth about nine mln dlrs to dispose of over time.
    the company said completion of the transaction is subject
to approval by both boards and basix's banks and the expiration
of the hart-scott-rodino waiting period.
 reuter
</text>]