[<text>
<title>photronic labs inc &lt;plab&gt; 1st qtr jan 31 net</title>
<dateline>    brookfield center, conn., march 20 -
    </dateline>shr 10 cts vs seven cts
    net 249,143 vs 175,476
    sales 3,034,010 vs 2,745,288
 reuter
</text>]