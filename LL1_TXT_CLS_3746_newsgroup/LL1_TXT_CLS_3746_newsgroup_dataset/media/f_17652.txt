[<text>
<title>cellular inc &lt;cels.o&gt; to sell unit, take gain</title>
<dateline>    denver, june 1 - </dateline>cellular inc said it reached a definitive
agreement to sell assets of its wholly owned michigan cellular
inc to century telephone enterprises inc &lt;ctl&gt; and add 28 cts a
share to the year's earnings as a result.
    it said the sale, subject to regulatory approval,
represents a capital gain in excess of 800,000 dlrs over the
original price paid by cellular for its cellular interests in
michigan, acquired in december 1986.
 reuter
</text>]