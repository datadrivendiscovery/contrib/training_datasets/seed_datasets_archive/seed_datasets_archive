[<text>
<title>barclays de zoete wedd sets up amsterdam office</title>
<dateline>    london, april 6 - </dateline>british securities house &lt;barclays de
zoete wedd&gt;, bzw, said it would open today a subsidiary office
in amsterdam, its first office in continental europe.
    bzw, a wholly-owned subsidiary of barclays plc &lt;bcs.l&gt;,
said in a statement the new company had been built on the
parent company's former dutch subsidiary, barclays kol.
    the new company, &lt;barclays de zoete wedd nederland nv&gt;,
will focus on trading in both dutch and international equities
and will develop a full range of investment banking services.
    it will initially employ 29, aiming to increase numbers to
35 by the year-end, it said.
    bzw said its parent barclays bank had subsidiary companies
active on the stock exchanges of zurich, milan and paris which
bzw planned to use to strengthen its presence in continental
europe.
    commenting on the move, bzw chairman sir martin jacombe
said while international deregulation had turned london, new
york and tokyo into "pivotal points for trading across time
zones, firms like bzw that seek to serve the world-wide
financial community must have a presence in europe, not just in
the u.k."
    bzw currently employs some 2,000 staff in its offices in
britain, hong kong, tokyo, new york and sydney.
 reuter
</text>]