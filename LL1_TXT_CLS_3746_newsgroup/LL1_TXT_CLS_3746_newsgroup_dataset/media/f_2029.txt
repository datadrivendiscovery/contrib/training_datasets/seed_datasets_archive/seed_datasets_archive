[<text>
<title>&lt;international thomson organisation ltd&gt; year</title>
<dateline>    toronto, mar 5 -
    </dateline>shr 33p vs 38p
    net 97 mln vs 111 mln
    revs 1.71 billion vs 1.76 billion
    note: figures in sterling.
    share results after deducting preferred share dividends of
one mln pounds sterling in 1986.
 reuter
</text>]