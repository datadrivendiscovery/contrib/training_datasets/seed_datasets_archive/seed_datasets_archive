[<text>
<title>re capital &lt;rcc&gt; begins stock repurchase</title>
<dateline>    stamford, conn., oct 20 - </dateline>re capital corp said it has begun
a stock buy-back program. the company gave no other details.
 reuter
</text>]