[<text>
<title>cherokee group &lt;chke&gt; share offering underway</title>
<dateline>    north hollywood, calif., april 2 - </dateline>cherokee group said an
offering of 2,500,000 common shares is underway at 22 dlrs each
through underwriters first boston inc &lt;fbc&gt; and american
express co's &lt;axp&gt; shearson lehman brothers inc.
    the company is selling 1,800,000 shares and shareholders
700,000.
 reuter
</text>]