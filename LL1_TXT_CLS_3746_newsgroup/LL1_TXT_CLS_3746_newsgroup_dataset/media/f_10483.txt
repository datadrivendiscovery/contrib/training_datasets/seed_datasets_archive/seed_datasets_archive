[<text>
<title>advanced tobacco &lt;atpi&gt; in licensing pact</title>
<dateline>    san antonio, texas, march 27 - </dateline>advanced tobacco products
inc said it signed a letter of intent to license a smoking
deterrent product to a unit of pharmacia ab &lt;phaby&gt; of sweden.
    under terms of the agreement, the company said pharmacia's
ab leo unit will pay an undisclosed sum, including
non-refundable royalties, for exclusive rights to evaluate
advanced tobacco's nicotine technology through july.
    the company said the unit will also have an option to buy
exclusive worldwide licensing rights for the technology for
additional cash and royalties.
 reuter
</text>]