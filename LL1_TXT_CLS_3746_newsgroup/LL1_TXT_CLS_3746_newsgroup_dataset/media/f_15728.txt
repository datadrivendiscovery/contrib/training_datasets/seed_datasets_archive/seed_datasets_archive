[<text>
<title>london grain freight enquiries</title>
<dateline>    london, april 9 - </dateline>antwerp/libya 5,500 mt bagged flour 14
daps 24-27/4.
    new orleans/guanta 9,387 mt bulk hss 3,000/13 days
25-4/5-5.
    naantali/saudi red sea 30,000/35,000 mt barley 4,000/3,000
20-30/4 or early may.
    dunkirk/xingang 12,000 mt bagged flour 1,500/1,700 13-20/4.
    toledo/seaforth 17,000 mt hss offers 18.50 dlrs four
days/8,000 13-15/4.
    river plate/malaysia 20,000/22,000 long tons hss
2,000/2,000 apr.
 reuter
</text>]