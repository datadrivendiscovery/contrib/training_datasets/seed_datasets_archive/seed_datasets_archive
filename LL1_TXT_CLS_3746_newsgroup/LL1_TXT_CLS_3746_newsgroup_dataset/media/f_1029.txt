[<text>
<title>scott's hospitality acquires capital food</title>
<dateline>    toronto, march 3 - </dateline>&lt;scott's hospitality inc&gt; said it
acquired all issued shares of capital food services ltd, of
ottawa. terms were not disclosed.
    scott's said capital food had 1986 sales of more than 20
mln dlrs and will continue to operate under its present name
with existing management.
    capital food provides food services to several ottawa
institutions, the company said.
 reuter
</text>]