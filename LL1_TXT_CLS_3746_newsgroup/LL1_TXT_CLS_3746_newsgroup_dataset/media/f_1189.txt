[<text>
<title>general automation &lt;gena&gt; sets leasing pact</title>
<dateline>    anaheim, calif., march 3 - </dateline>general automation inc said it
has arranged a ten-mln-dlr leasing facility with wells fargo
and co's &lt;wfc&gt; wells fargo leasing corp.
    the program is a one-year facility that will allow
financing for zebra computers, other general automation systems
and applications sofware, the company said.
 reuter
</text>]