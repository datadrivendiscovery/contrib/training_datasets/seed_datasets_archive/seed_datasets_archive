[<text>
<title>sino-u.s. venture in china to make rinsing agents</title>
<dateline>     peking, march 2 - </dateline>&lt;ecolab co&gt; of the united states signed
a contract with north china industrial co to set up the first
sino-u.s. joint venture in china to make rinsing agents, the
new china news agency said.
     it said total investment in the new venture, &lt;ecolab
chemical industrial co&gt;, is 2.4 mln dlrs. it said the venture
will be based in shanghai and produce agents for use in hotels
and industries.
    it gave no further details.
 reuter
</text>]