[<text>
<title>hartmarx corp &lt;hmx&gt; 2nd qtr may 31 net</title>
<dateline>    chicago, june 29 -
    </dateline>shr 40 cts vs 11 cts
    net 8,265,000 vs 2,255,000
    sales 248.3 mln vs 245.4 mln
    six mths
    shr 94 cts vs 51 cts
    net 19.4 mln vs 10.6 mln
    sales 531 mln vs 535.8 mln
 reuter
</text>]