[<text>
<title>two join nasdaq national market system</title>
<dateline>    new york, june 1 - </dateline>csc industries inc &lt;cpsl.o&gt; and enzon
inc &lt;enzn.o&gt; said their common stocks will be added to the
national association of securities dealers' nasdaq national
market system tomorrow.
 reuter
</text>]