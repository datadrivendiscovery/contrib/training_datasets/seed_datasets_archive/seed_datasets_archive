[<text>
<title>mcchip &lt;mcs.to&gt; to acquire reserve holding</title>
<dateline>    toronto, june 29 - </dateline>mcchip resources inc said it has agreed
to exchange its interest in oklahoma oil and gas properties
operated by reserve exploration co &lt;rexc.o&gt; for 638,435
restricted reserve common shares.
    the company said it will have a 44 pct stake in reserve's
outstanding shares as a result of the exchange.
 reuter
</text>]