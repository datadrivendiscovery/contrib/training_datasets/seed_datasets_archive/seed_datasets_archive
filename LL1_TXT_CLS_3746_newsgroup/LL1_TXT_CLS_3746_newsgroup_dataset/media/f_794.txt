[<text>
<title>white house withdraws gates nomination as cia chief</title>
<dateline>    washington, march 2 - </dateline>the white house announced it was
withdrawing the controversial nomination of robert gates as cia
director at gates's request and has not yet decided on a
replacement.
    withdrawal of the gates nomination was announced by new
white house chief of staff howard baker, who said president
reagan had several names under consideration to replace gates
but had made no decision on that score.
    baker said gates had sent reagan a letter today requesting
his name be withdrawn from nomination to succeed the ailing and
resigned william casey and that reagan had "accepted with great
regret."
 reuter
</text>]