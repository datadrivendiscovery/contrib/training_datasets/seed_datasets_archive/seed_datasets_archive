[<text>
<title>british caledonian seeks san diego service</title>
<dateline>    san diego, march 9 - </dateline>british caledonian airways said it has
filed an application with the british civil aviation authority
for a license to operate between san diego's lindbergh field
and london's gatwick airport.
    it said it would extend its existing los angeles/london
nonstop service to san diego and would initially offer three
roundtrips weekly.
 reuter
</text>]