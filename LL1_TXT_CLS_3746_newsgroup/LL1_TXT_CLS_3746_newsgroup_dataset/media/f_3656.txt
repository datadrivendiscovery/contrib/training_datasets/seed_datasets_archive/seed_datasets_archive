[<text>
<title>shl systemhouse &lt;nms&gt; files for public offering</title>
<dateline>    ottawa, ontario, march 11 - </dateline>shl systemhouse inc said it
filed a registration statement with the securities and exchange
commission relating to common shares owned by &lt;kinburn
technology corp&gt;.
    shl said the filing was made to satisfy exchange rights
under a proposed offering of u.s 100 mln dlrs principal amount
of exchangeable secured debentures due 2007 by kinburn.
    kinburn owns 31.6 pct of the issued and outstanding common
shares of shl, the company said.
    it said the debentures may be exchanged at any time prior
to maturity unless previously redeemed, for shl systemhouse
common shares at an exchange rate to be determined at the time
of the sale of the debentures.
    shl said it was advised that kinburn intends to exercise
this option to maintain its ownership and effective control of
systemhouse.

 reuter
</text>]