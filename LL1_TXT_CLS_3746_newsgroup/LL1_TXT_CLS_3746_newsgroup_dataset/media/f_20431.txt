[<text>
<title>aceto &lt;acet.o&gt; boosts share repurchase</title>
<dateline>    new york, oct 20 - </dateline>aceto corp said its board increased its
authorized share repurchase program to 500,000 shares from
200,000. the shares will be bought in the open market.
    it has bought back 185,422 shares under the previously
authorized 200,000 share program.
 reuter
</text>]