[<text>
<title>manufacturers hanover corp &lt;mhc&gt; sets dividend</title>
<dateline>    new york, march 17 - 
    </dateline>qtly div 82 cts vs 82 cts prior
    pay april 25
    record april one
 reuter
</text>]