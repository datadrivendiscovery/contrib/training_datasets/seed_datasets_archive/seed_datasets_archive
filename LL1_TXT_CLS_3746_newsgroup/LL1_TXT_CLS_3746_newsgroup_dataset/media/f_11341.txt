[<text>
<title>csce restructures coffee futures daily limits</title>
<dateline>    new york, march 31 - </dateline>the coffee, sugar and cocoa exchange
has expanded the normal daily trading limit in coffee "c"
contracts to 6.0 cents a lb, from the previous 4.0 cents,
effective today, the csce said.
    the new daily limits apply to all but the two nearby
positions, currently may and july, which trade without limits.
    in addition, the 6.0 cent limit can be increased to 9.0
cents a lb if the first two limited months both make limit
moves in the same direction for two consecutive sessions,
according to the csce announcement.
    before the rule change today, the csce required two days of
limit moves in the first three restricted contracts before
expanding the daily trading limit.
    under new guidelines, if the first two restricted
deliveries move the 6.0 cent limit for two days the exchange
will expand the limit. the expanded 9.0 cent limit will remain
in effect until the settling prices on both of the first two
limited months has not moved by more than the normal 6.0 cent
limit for other contracts in two successive trading sessions,
the csce said.
 reuter
</text>]