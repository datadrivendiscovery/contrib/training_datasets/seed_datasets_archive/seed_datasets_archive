[<text>
<title>inverness buys assets from parent silverton</title>
<dateline>    calgary, alberta, march 31 - </dateline>&lt;inverness petroleum ltd&gt; said
it acquired the oil and gas assets of its controlling
shareholder &lt;silverton resources ltd&gt; for 26.4 mln dlrs,
effective march 3, 1987.
    inverness said it issued 2,640,000 class a convertible
retractable redeemable preferred shares in exchange for the
assets, which include all silverton's oil and gas properties,
undeveloped acreage and its shares of australian subsidiary
s.r.l. exploration pty ltd.
    the preferred shares were immediately retracted for cash,
the company said.
    the transaction resulted in a discharge of silverton's bank
debt of 21.0 mln dlrs and a three mln dlr loan to inverness
from silverton, inverness said.
    as a result of the acquisition, inverness has bank debt of
18.0 mln dlrs, it said.
 reuter
</text>]