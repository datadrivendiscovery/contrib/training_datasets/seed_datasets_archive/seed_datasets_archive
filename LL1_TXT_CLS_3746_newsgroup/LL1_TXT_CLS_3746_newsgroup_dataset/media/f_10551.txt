[<text>
<title>hanson &lt;han&gt; to sell bond's delivery service</title>
<dateline>    new york, march 27 - </dateline>hanson industries, the u.s. arm of
hanson trust plc, said contracts have been exchanged in london
for the sale of bond's delivery service to rockwood holdings
plc for about 6.0 mln dlrs in cash.
    completion is subject to rockwood shareholder approval.
    in its most recent financial year, bond's which was
purchased
by hanson trust plc in its acquisition of imperial group plc in
april 1986, made 960,000 dlrs pre-tax profit on sales of 13.6
mln dlrs.
    net tangible assets are 5.2 mln dlrs, hanson said.
 reuter
</text>]