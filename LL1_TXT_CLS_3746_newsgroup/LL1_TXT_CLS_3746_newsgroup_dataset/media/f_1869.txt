[<text>
<title>nbi &lt;nbi&gt; introduces new products</title>
<dateline>    chicago, ill, march 4 - </dateline>nbi inc said it has reduced prices
of on its 5000 series publishing workstations by 30 pct and has
introduced a new electronic publishing workstation, a
mini-computer and an image scanner at the corporate electronic
publishing systems show.
    all of nbi's 5000 series are included in the new pricing
including the integrated workstation formerly priced at 14,490
dlrs and the pro publisher formerly priced at 15,490 dlrs.
  
 reuter
</text>]