[<text>
<title>&lt;franklin utilities fund&gt; sets payout</title>
<dateline>    san mateo, calif., march 2 -
    </dateline>qtly div 14 cts vs 14 cts prior
    pay march 13
    record march two
 reuter
</text>]