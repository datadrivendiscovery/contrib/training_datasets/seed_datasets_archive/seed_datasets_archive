[<text>
<title>claire's stores &lt;cle&gt; february sales rise</title>
<dateline>     miami, march 9 - </dateline>claire's stores inc said february sales
were up 46 pct to 6,360,000 dlrs from 4,350,000 dlrs a year
before, with same-store sales up 16 pct.
    the company said march sales may not increase at the sdame
rate, since easter falls late in april this year, but earnings
and sales for the first quarter should be up significantly from
a year ago.
    in last year's first quarter ended may three, claire's
earned 934,000 dlrs on sales of 18.4 mln dlrs, compared with
earnings of 2,289,000 dlrs on sales of 16.4 mln dlrs a year
earlier.
 reuter
</text>]