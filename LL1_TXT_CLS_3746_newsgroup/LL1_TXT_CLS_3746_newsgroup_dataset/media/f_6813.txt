[<text>
<title>central illinois &lt;cip&gt; 12 mths feb 28 net</title>
<dateline>    springfield, ill., march 18 -
    </dateline>shr 2.04 dlrs vs 1.83 dlrs
    net 76,172,000 vs 71,101,000
    revs 621.7 mln vs 670.3 mln
    note: central illinois public service co is full name of
company.
    most recent 12 months net excludes preferred dividends of
6.4 mln dlrs compared with 8.6 mln dlrs last year.
 reuter
</text>]