[<text>
<title>chicago pacific &lt;cpac&gt; sells convertible debt</title>
<dateline>    new york, april 9 - </dateline>chicago pacific corp is raising 150 mln
dlrs through an offering of convertible subordinated debentures
due 2012 with a 6-1/2 pct coupon and par pricing, said lead
manager goldman, sachs and co.
    the debentures are convertible into the company's common
stock at 62.50 dlrs per share, representing a premium of 25.63
pct over the stock price when terms on the debt were set.
    non-callable for two years, the issue is rated b-1 by
moody's investors service inc and b by standard and poor's
corp. first boston corp and lazard freres and co co-managed the
deal.
 reuter
</text>]