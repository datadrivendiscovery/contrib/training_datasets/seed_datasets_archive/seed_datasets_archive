[<text>
<title>lacana &lt;lcnaf&gt; names new ceo and president</title>
<dateline>    toronto, march 4 - </dateline>&lt;lacana mining corp&gt; said gil l.
leathley was appointed president and chief executive, replacing
william gross who retired.
    the company said gross will continue as a director and
executive committee member.
 reuter
</text>]