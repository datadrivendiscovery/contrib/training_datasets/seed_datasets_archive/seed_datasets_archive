[<text>
<title>allied irish banks opens tokyo office</title>
<dateline>    london, oct 20- </dateline>allied irish banks plc &lt;albk.l&gt; said it
plans to open a tokyo office.
    initially it will be a representative office but the
company said it hopes to have a full banking licence in about
two years.
 reuter
</text>]