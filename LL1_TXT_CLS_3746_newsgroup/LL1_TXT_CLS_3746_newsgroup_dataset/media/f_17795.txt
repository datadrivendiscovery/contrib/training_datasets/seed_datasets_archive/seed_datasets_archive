[<text>
<title>diana corp &lt;dna&gt; year march 28 oper net</title>
<dateline>    milwaukee, wis., june 1 - 
    </dateline>oper shr 74 cts vs 30 cts
    oper net 3,034,000 vs 1,225,000
    note: 1987 operating net excludes credits of 1,043,000 dlrs
or 25 cts a share.
    1986 operating net excludes discontinued operations of
84,000 dlrs or two cts, and extraordinary charges of 1,119,000
dlrs or a loss of 27 cts.
 reuter
</text>]