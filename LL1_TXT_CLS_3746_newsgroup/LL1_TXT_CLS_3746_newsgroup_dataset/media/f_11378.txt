[<text>
<title>brougher &lt;bigi&gt; to sell 40 pct of subsidiary</title>
<dateline>    greenwood, ind., march 31 - </dateline>brougher insurance group inc
said it plans to sell 40 pct of the stock of its subsidiary,
intercontinental corp, for one mln dlrs to three european
insurance companies.
    the parent company said it signed a letter of intent to
sell the stock to &lt;wasa europeiska forsakrings ab&gt; of sweden,
&lt;europeiske reiseforsikring a/s&gt; of norway, and &lt;europeiska
rejseforsikrings a/s&gt; of denmark.
    brougher said it expects to realize a net after-tax gain of
approximately 330,000 dlrs, or 12 cts per share, from issuing
stock of intercontinental.
 reuter
</text>]