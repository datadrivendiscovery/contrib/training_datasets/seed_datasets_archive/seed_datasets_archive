[<text>
<title>reebok international limited &lt;rbk&gt; 1st qtr</title>
<dateline>    new york, april 13 -
    </dateline>shr 72 cts vs 52 cts
    net 38.6 mln vs 25 mln
    revs 281.8 mln vs 174.5 mln
    avg shrs 53.5 mln vs 48.2 mln
    note: 1987 1st quarter amounts do not includes sales of
avia group international inc, acquired at the end of the first
quarter. 1987 1st quarter revenues include rockport revenues of
31 mln dlrs. 1986 1st quarter amounts do not include rockport,
as reebok acuqired that company in october 1986.
 reuter
</text>]