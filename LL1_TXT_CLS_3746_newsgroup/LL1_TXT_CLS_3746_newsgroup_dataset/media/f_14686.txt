[<text type="unproc">
ussr exchange rates - soviet state bank effective april 8, 1987
roubles per hundred unless stated u.s.                  63.85  
  (63.60) stg                  103.47    (102.56) bfr (1,000)  
        16.90     (17.01) dmk                   35.06    
(35.43) dfl                   31.00     (31.20) lit (10,000)   
       4.91      (4.94) can                   48.85     (48.79)
dkr                    9.28      (9.32) nkr                   
9.38      (9.34) ffr                   10.52     (10.59) skr   
               10.06      (unch) fin mrk               14.20   
  (unch) sfr                   42.03     (42.33) yen (1,000)   
        4.37      (4.34) aus sch                4.98     
(5.01) aus dlr               44.77      (unch) pak rup         
      3.74      (unch) ind rup                5.10      (unch)
irn ryl                0.88      (unch) lib din (one)         
2.09      (unch) mrc drhm               7.78      (unch)


</text>]