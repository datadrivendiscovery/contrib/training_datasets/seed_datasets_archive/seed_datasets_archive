[<text>
<title>prime computer &lt;prm&gt; unveils pc software</title>
<dateline>    natick, mass., march 2 - </dateline>prime computer inc said it has
introduced the prime medusa/pc software, a two dimensional
version of its prime medusa computer-aided-design software.
    prime said prime medusa/pc is for use on an international
business machines corp &lt;ibm&gt; pc/at operating within a prime 50
series minicomputer environment.
    the company said the software is available immediately to
customers who have or are currently ordering a prime medusa
license on one of prime's 50 series systems.
    prime added the software costs 5,000 dlrs per license with
monthly maintenance of 65 dlrs.
    prime said it also unveiled the prime medusa revision 4.0
system with a new feature for developing applications that
alows users to associate non-graphic information with graphic
elements on a drawing sheet.
    the prime medusa revision 4.0 is available immediately,
prime said.
 reuter
</text>]