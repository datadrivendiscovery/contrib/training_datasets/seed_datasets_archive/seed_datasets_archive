[<text>
<title>coachmen industries &lt;coa&gt; 3rd qtr net</title>
<dateline>    elkhart, ind., oct 20 -
    </dateline>shr loss eight cts vs profit six cts
    net loss 669,609 vs profit 530,641
    sales 94.9 mln vs 83.9 mln
    avg shrs 7,934,064 vs 8,220,797
    nine mths
    shr profit 19 cts vs profit 55 cts
    net profit 1,494,218 vs profit 4,486,510
    sales 289.1 mln vs 276.6 mln
    avg shrs 7,930,961 vs 8,208,033
 reuter
</text>]