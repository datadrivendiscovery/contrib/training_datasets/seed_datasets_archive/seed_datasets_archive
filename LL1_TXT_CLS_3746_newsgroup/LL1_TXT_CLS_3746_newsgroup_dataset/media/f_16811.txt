[<text>
<title>holiday corp &lt;hia&gt; sells stake in venture</title>
<dateline>    wichita, ka., april 17 - </dateline>residence inn corp said it has
agreed to buy holiday corp out of their equaly-owned joint
venture for 51.4 mln dlrs, with closing expected within the
next few weeks.
    the all-suite residence inn system, which is geated to
extended stays, currently has 93 open franchised or
company-owned hotels nationwide and another 55 in construction
or development.
 reuter
</text>]