[<text>
<title>mueller brass lowers product prices</title>
<dateline>    port huron, mich., june 2 - </dateline>mueller brass co said that
effective with shipments today, it is adjusting the price of
all brass mill products, except free-cutting brass rod and
related alloys and copper water tube and related products, to
reflect contained copper values at 73 cents a pound, down two
cents.
   
 reuter
</text>]