[<text>
<title>fed's johnson sees inflation controlled</title>
<dateline>    washington, march 19 - </dateline>federal reserve board vice chairman
manuel johnson said inflationary pressures are under control
and noted "wage and price pressures are very moderate."
    johnson told a women's group that the u.s. was not seeing
the kind of cost pressures of the past.
    he said the trade imbalance was a serious trouble spot and
strong protectionist pressures, if translated into policies,
could ultimately lead to higher inflation and a high interest
rate policy by the fed.
 reuter
</text>]