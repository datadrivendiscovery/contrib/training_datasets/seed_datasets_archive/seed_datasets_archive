[<text>
<title>aw computer systems inc &lt;awcsa&gt; year end dec 31</title>
<dateline>    mt laurel, n.j., march 30 -
    </dateline>shr 18 cts vs 17 cts
    net 584,493 vs 540,977
    revs 4,685,930 vs 4,524,315
 reuter
</text>]