[<text>
<title>westerbeke corp &lt;wtkb&gt; 1st qtr jan 31 loss</title>
<dateline>    avon, mass., march 17 -
    </dateline>shr loss 11 cts vs profit six cts
    net loss 217,200 vs profit 83,200
    revs 3,564,200 vs 3,171,900
    avg shrs 2,033,750 vs 1,334,950
 reuter
</text>]