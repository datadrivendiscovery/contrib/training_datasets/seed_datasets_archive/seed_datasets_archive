[<text>
<title>national city bancorp &lt;ncbm.o&gt; 1st qtr net</title>
<dateline>    minneapolis, april 13 -
    </dateline>shr 37 cts vs 27 cts
    net 1,194,000 vs 870,000
 reuter
</text>]