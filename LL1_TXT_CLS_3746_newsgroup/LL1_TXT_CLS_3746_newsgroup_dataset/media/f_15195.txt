[<text>
<title>research industries &lt;reic&gt; unveils new products</title>
<dateline>    salt lake city, april 8 - </dateline>research industries corp said it
is introducing three additions to its line of cardiovascular
specialty products.
    the company said the products are a patented drainage
bicaval cannula for open heart surgery, a proprietary catheter
for neonatal thoracic surgery and a line of specialized baloon
catheter shunts for carotid artery surgery.
 reuter
</text>]