[<text>
<title>media general &lt;meg.a&gt; ups dividend, sets split</title>
<dateline>    richmond, va., march 26 - </dateline>media general inc said it raised
the annual dividend on its class a and class b common stock to
68 cts a share from 64 cts.
    the company said it also declared a two-for-one stock split
of both stock issues, which is subject to shareholder approval
of an increase in the number of authorized class a shares.
    media general said the increased dividend is payable june
12 to shareholders of record may 29.
    the proposed stock split will be paid may 29 in shares of
class a shares, the company said.
    the company said it also approved an amendment to its
articles of incorporation allowing class b shares to be
coverted into class a shares at the option of the holder.
    media general said the moves should broaden investor
interest in its class a stock.
 reuter
</text>]