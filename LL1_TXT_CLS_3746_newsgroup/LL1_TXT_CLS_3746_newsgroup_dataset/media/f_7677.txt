[<text>
<title>majestic electronics plans issuer bid</title>
<dateline>    toronto, march 20 - </dateline>&lt;majestic electronic stores inc&gt; said
it intends to make a normal course issuer bid to purchase up to
five pct of its 5.2 mln outstanding common shares on the open
market during the 12 month period commencing april 13, 1987.
    the company said the purpose of the proposed issuer bid is
to avoid possible dilution of share value under a proposed
employee stock option plan.
    majestic expects to have its employee stock option plan in
place during the year, it said.
 reuter
</text>]