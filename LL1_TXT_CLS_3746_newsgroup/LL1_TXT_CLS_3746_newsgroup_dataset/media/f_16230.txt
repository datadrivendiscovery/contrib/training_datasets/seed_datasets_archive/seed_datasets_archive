[<text>
<title>merrill lynch and co &lt;mer&gt; 1st qtr net</title>
<dateline>    new york, april 13 -
    </dateline>shr primary one dlr vs 85 cts
    shr diluted 97 cts vs 81 cts
    net 108.6 mln vs 86.8 mln
    rev 2.70 billion vs 2.17 billion
 reuter
</text>]