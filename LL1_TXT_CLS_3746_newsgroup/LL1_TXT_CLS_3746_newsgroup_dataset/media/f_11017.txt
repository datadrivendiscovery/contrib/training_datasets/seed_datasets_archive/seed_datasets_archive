[<text>
<title>quartz engineering &lt;qrtz&gt; gets siemens order</title>
<dateline>    tempe, ariz., march 30 - </dateline>quartz engineering and materials
inc said it has received a 500,000 dlr order from &lt;siemens ag&gt;
of west germany for its atmoscan automated environmentally
controlled semiconductor wafer processing system.
 reuter
</text>]