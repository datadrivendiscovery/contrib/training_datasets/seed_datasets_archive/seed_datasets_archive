[<text>
<title>hudson bay mining cuts u.s., canada zinc prices</title>
<dateline>    toronto, march 9 - </dateline>&lt;hudson bay mining and smelting co ltd&gt;
said it cut prices for all grades of zinc sold in north america
by one u.s. ct a pound and by one canadian ct a pound,
effective immediately.
    the new price for high grade zinc is 37 u.s. cts and 49-1/2
canadian cts a pound, the company said.
    special high grade, prime western and continuous
galvanizing grade with controlled lead now costs 37-1/2 u.s.
cts and 50 canadian cts a pound. the new price for continuous
galvanizing grade alloyed with controlled lead and aluminum
additions is 37-3/4 u.s. cts and 50-1/4 canadian cts a pound.
 reuter
</text>]