[<text>
<title>magma power &lt;mgma&gt; names new chief executive</title>
<dateline>    san diego, march 30 - </dateline>magma power co said director arnold
l. johnson has been named president and chief executive
officer.
    he succeeds andrew w. hoch, who moved to chairman in
february following the resignation of b.c. mccabe sr.
    the company said johnson has also been named president and
chief executive officer of magma energy inc &lt;mage&gt;, which magma
power controls, succeeding hoch, who becomes magma energy
chairman as well. mccabe, whom hoch also succeeds there, has
been named chairman emeritus.
 reuter
</text>]