[<text>
<title>dupont's &lt;dd&gt; conoco says no interest in dome</title>
<dateline>    toronto, april 13 - </dateline>conoco, a unit of dupont co, said it is
not interested in buying dome petroleum ltd &lt;dmp&gt;, the canadian
oil company which has said it is in discussions with two
unidentified companies.
    "to my knowledge, there is no interest on our part," conoco
spokesman sondra fowler said.
    houston-based conoco has been mentioned in market
speculation on the identity of the two companies. yesterday,
dome also received a 4.3 billion canadian dlr offer for all its
assets from transcanada pipelines ltd &lt;tmp&gt;.
 reuter
</text>]