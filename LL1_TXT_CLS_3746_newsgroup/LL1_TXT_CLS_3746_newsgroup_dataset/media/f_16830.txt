[<text>
<title>wainoco oil &lt;wol&gt; to redeem some rights</title>
<dateline>    houston, april 17 - </dateline>wainoco oil corp said its board has
voted to redeem its shareholder value rioghts for 10 cts each,
payable june one to holders of record on april 27.
    the rights have been attached on a one-for-one basis to 
wainoco common shares.  as previously announced, the company is
redeeming the rights to have sufficient authorized and
unreserved common shares to proceed with a planned unit
offering of common stock and warrants.
    it said its shareholder purchase rights distributed in june
1986 are unaffected.
 reuter
</text>]