[<text>
<title>cme proposes nikkei stock average futures</title>
<dateline>    washington, june 1 - </dateline>the chicago mercantile exchange (cme)
has proposed a futures contract based on the nikkei stock
average, a price-weighted index of 225 stocks on the tokyo
stock exchange, federal regulators said.
    the commodity futures trading commission (cftc) said cme's
proposed contract is designed to be used as a hedging tool for
dealers and investors in japanese equities.
    the contract's unit of trading would be 500 japanese yen
times the nikkei index, cftc said. the minimum price
fluctuation would be 2,500 yen, with no daily price limits.
    public comment on the proposal is due by july 28.
 reuter
</text>]