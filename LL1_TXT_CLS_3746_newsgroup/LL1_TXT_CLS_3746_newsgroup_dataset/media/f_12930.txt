[<text>
<title>hadson &lt;hads&gt; to offer four mln shares</title>
<dateline>    oklahoma city, april 3 - </dateline>hadson corp said it is offering
four mln new shares of its common, par value 10 cts a share, at
8-1/2 dlrs a shares.
    it had previously announced it would offer 3,750,000 shares
in a filing with the securities and exchange commission.
    hadson said proceeds will be used for expansion of its
existing businesses and energy-related acquisitions, and for
general corporate purposes.
    shearson lehman brothers inc and painwebber inc are the
managing underwriters.
 reuter
</text>]