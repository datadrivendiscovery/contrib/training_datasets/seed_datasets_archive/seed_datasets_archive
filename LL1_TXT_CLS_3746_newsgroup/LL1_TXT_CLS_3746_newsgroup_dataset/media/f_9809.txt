[<text>
<title>mickelberry &lt;mbc&gt; completes sale of unit</title>
<dateline>    new york, march 26 - </dateline>mickelberry corp said it has completed
the previously-announced sale of the 51 pct of its c and w
group subsidiary that it had retained to n w ayer inc for
undisclosed terms.
    ayer bought the other 49 pct next year.
    mickelberry said it will report a gain on the transaction.
 reuter
</text>]