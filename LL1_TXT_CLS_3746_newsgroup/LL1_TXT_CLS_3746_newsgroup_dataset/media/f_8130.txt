[<text>
<title>ariadne unit confirms bid for san miguel</title>
<dateline>    hong kong, march 23 - </dateline>ariadne group unit &lt;barwon farmlands
ltd&gt; confirmed it offered 3.8 billion pesos in cash for the 38
mln shares of philippine brewing company &lt;san miguel corp&gt;.
    the australia-based barwon, 30 pct owned by new zealander
bruce judge's ariadne group, said in a statement released in
hong kong that a formal offer had been made to the philippines
government, which holds the shares.
    it said it was confident the offer will be reviewed
favourably.
    newspapers in manila and hong kong reported at the weekend
that an offer had been made.
    barwon said it was represented by australian stockbroker
&lt;jacksons ltd&gt;, which forwarded a formal offer to philippine
president corazon aquino of 100 pesos for each of the 38.1 mln
a and b shares of san miguel.
    the philippine government seized the shares, which
represent a 31 pct stake in the brewery firm, from the &lt;united
coconut planters bank&gt;, alleged by the government to be linked
with the country's deposed president ferdinand marcos.
    the barwon statement said a deal is expected to be
concluded between barwon and the philippines government in 14
days.
    barwon also said it made recommendations to the government
on how it could purchase the class a shares, which can only be
held by a philippine national or a firm which is at least 60
pct held by a member of the country. it did not elaborate.
    the hong kong economical journal quoted a spokesman of
jacksons as saying barwon plans to set up a branch in the
philippines to meet the criteria.
    &lt;san miguel brewery ltd&gt;, a locally listed firm 69.65 pct
held by san miguel's &lt;neptunia corp&gt; affiliate, was last traded
at 16.30 h.k. dlrs against 15.50 dlrs on friday.
 reuter
</text>]