[<text>
<title>hartmarx corp &lt;hmx&gt; 1st qtr feb 28 net</title>
<dateline>    chicago, april 1 -
    </dateline>shr 54 cts vs 40 cts
    net 11,105,000 vs 8,310,000
    sales 282.7 mln vs 290.3 mln
    avg shrs 20,599,000 vs 20,760,000
    note: per-share results restated for may 1986 three-for-two
stock split
 reuter
</text>]