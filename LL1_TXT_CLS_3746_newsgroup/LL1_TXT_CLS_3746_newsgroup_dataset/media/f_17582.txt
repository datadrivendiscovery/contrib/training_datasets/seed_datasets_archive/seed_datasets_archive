[<text>
<title>chicago pacific corp &lt;cpac.o&gt; regular payout</title>
<dateline>    chicago, june 1 -
    </dateline>qtly div five cts vs five cts previously
    pay july one
    record june 15
 reuter
</text>]