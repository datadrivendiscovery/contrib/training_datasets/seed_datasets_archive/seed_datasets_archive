[<text>
<title>rapitech systems inc &lt;rpsy.o&gt; 3rd qtr loss</title>
<dateline>    suffern, n.y., june 19 - </dateline>april 30 end
    shr losses not given
    net loss 449,000 vs loss 155,000
    revs 84,000 vs 52,000
    nine mths
    shr losses not given
    net loss 810,000 vs loss 394,000
    revs 173,000 vs 144,000
 reuter
</text>]