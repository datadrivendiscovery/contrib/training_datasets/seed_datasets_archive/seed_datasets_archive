[<text>
<title>n.y. times &lt;nyt&gt; to buy gwinnett daily news</title>
<dateline>    new york, june 19 - </dateline>the new york times co said it had an
agreement to buy the gwinnett daily news, an evening newspaper
published in lawrenceville, ga., terms were not disclosed.
    the company said gwinnett has a weekday circulation of
about 27,500 and a sunday circulation of about 30,900. the new
york times also said gwinnett county, a northeast suburb of
atlanta, is the fastest-growing county in the country.
    the purchase agreement includes the forsyth county news,
published on wednesday and sunday and the winder news, a
weekly, among other publications.
 reuter
</text>]