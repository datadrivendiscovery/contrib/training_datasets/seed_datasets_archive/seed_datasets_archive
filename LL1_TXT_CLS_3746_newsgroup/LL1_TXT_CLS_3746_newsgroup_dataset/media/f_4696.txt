[<text>
<title>u.s. lends sudan 57 mln dlrs for wheat purchases</title>
<dateline>    khartoum, march 13 - </dateline>the u.s. is to lend sudan 57 mln dlrs
to buy 309,000 tonnes of wheat and 73,000 tonnes of wheat
flour, according to an agreement signed here.
    under the agreement sudan will receive 50 mln dlrs for the
commodities and the rest for sea transportation.
    the loan is repayable in 40 years, including a 10-year
grace period. interest will be two pct for the grace period and
three pct over the repayment period of 30 years.
 reuter
</text>]