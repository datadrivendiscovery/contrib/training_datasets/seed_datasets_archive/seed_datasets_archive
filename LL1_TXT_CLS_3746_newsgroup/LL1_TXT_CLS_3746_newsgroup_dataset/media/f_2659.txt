[<text>
<title>&lt;cineplex odeon corp&gt; year net</title>
<dateline>    toronto, march 6 -
    </dateline>oper shr basic 1.04 dlrs vs 77 cts
    oper shr diluted 89 cts vs 60 cts
    oper net 31.6 mln vs 12.5 mln
    revs 500.6 mln vs 170.9 mln
    avg shrs 29.1 mln vs 14.3 mln
    note: 1985 net excludes extraordinary gain of 1,756,000
dlrs or 12 cts shr basic and eight cts shr diluted.
    1986 net involves 53-week reporting period to reflect
change in yr-end to coincide with calendar yr.
 reuter
</text>]