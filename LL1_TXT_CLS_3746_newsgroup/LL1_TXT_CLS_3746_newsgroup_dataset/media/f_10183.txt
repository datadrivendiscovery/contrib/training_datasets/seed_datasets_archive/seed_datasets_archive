[<text>
<title>u.s. m-1 money supply rises 1.2 billion dlr</title>
<dateline>    new york, march 26 - </dateline>u.s. m-1 money supply rose 1.2 billion
dlrs to a seasonally adjusted 740.2 billion dlrs in the march
16 week, the federal reserve said.
    the previous week's m-1 level was revised to 739.0 billion
dlrs from 738.7 billion, while the four-week moving average of
m-1 rose to 739.1 billion dlrs from 738.3 billion.
    economists polled by reuters said that m-1 would rise
anywhere from 700 mln dlrs to three billion dlrs. the average
forecast called for a 1.8 billion dlr increase.
 reuter
</text>]