[<text>
<title>moody's may downgrade service merchandise &lt;smch&gt;</title>
<dateline>    new york, march 6 - </dateline>moody's investors service inc said it
may downgrade service merchandise co's 300 mln dlrs of ba-2
senior subordinated notes.
    the rating agency said it had anticipated that service
merchandise would be able to achieve better operating results
than it actually did in fiscal 1986.
    moody's said it would assess future prospects for the
company, as well as for the whole catalogue showroom industry.
the agency will also examine the effects of recent acquisitions
on service merchandise's operating results, financial structure
and cash flow.
 reuter
</text>]