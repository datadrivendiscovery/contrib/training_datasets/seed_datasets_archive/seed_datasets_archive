[<text>
<title>liberty financial &lt;lfg&gt; president to resign</title>
<dateline>    horsham, pa., april 9 - </dateline>harold h. kline, 48, will resign on
may one as president of liberty financial group inc and its
liberty savings bank unit to pursue other business
opportunties, the company said.
    the company said kline's post will be filled by charles g.
cheleden, chairman and chief executive officer.
    kline will continue to serve on the boards of liberty
financial and liberty savings, the company added.
 reuter
</text>]