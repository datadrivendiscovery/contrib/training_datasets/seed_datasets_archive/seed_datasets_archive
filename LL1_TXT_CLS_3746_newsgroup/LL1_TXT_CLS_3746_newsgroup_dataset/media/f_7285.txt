[<text>
<title>retail chain to sell rocky mountain &lt;rmed&gt; item</title>
<dateline>    denver, march 19 - </dateline>rocky mountain medical corp said the
baby news store chain of children's stores will carry its
tendercare line of non-chemical disposable diapers.
    rocky claimed its patented dryness technology provides a
diaper that is demonstrably drier than conventional disposable
diapers and cloth diapers.
    baby news' 60 stores are located in the western u.s.
 reuter
</text>]