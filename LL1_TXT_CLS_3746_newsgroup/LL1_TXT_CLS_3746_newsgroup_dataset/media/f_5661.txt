[<text>
<title>suburban bancorp &lt;subba&gt; unit wins judgment</title>
<dateline>    palatine, ill, march 16 - </dateline>suburban bancorp said a cook
county circuit court ruled in favor of one of its companies,
suburban bancorp of bartlett (formerly bartlett state bank) on
claims against six former directors.
    suburban bank of barlett claimed that the former directors
breached their fiduciary duties and were negligent in opposing
suburban bancorp's tender offer in january 1985, costing
bartlett state bank several hundred thousand dollars.
    it said a hearing to determine the amount of damages owed
to the bank was set for april 21.
 reuter
</text>]