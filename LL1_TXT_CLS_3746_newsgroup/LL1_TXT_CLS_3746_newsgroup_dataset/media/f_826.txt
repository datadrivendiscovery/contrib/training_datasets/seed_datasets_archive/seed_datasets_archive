[<text>
<title>fda okays drug to lessen hemophiliacs' bleeding</title>
<dateline>    washington, march 2 - </dateline>the federal food and drug
administration said it approved for u.s. marketing a drug that
reduces bleeding in hemophiliacs requiring dental work.
    the fda said the drug, tranemaxic acid, would lessen and in
some cases eliminate altogether the need for blood transfusions
in hemophiliacs who must have teeth extracted.
    the drug will be made by kabivitrum of stockholm, sweden
and distributed here under the trade name cyklokapron by
kabivitrum inc of alameda, calif.
    hemophilia is a hereditary diseease whose victims lack the
particular proteins that promote blood clotting.
 reuter
</text>]