[<text>
<title>hoechst to research discs with u.s., japan firms</title>
<dateline>    frankfurt, march 2 - </dateline>hoechst ag &lt;hfag.f&gt;, &lt;kerdix inc.&gt;,
boulder, colorado, and &lt;nakamichi corp&gt;, tokyo, have agreed to
pool their research and development on magneto-optical memory
discs, hoechst said in a statement.
    research will be carried out at each company and hoechst
will start to produce the discs by mid-1988 and distribute them
worldwide under the brand name ozadisc.
    a hoechst spokesman said an eventual joint venture was
likely but could give no details.
 reuter
</text>]