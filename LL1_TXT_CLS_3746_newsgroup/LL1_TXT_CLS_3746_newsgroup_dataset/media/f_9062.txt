[<text>
<title>xoma &lt;xoma&gt; files for public offering</title>
<dateline>    berkeley, calif., march 24 - </dateline>xoma corp said it filed a
registration statement with the securities and exchange
commission covering a proposed public offering of 1.3 mln
common shares.
    xoma, a biotechnology company which produces monoclonal
antibody-based products to treat some forms of cancer, said
proceeds will be used to fund research and product development,
human clinical trials and the expansion of manufacturing
capacity and working capital.
    it said dillon read and co inc and alex brown and sons inc
will manage the underwriting.
 reuter
</text>]