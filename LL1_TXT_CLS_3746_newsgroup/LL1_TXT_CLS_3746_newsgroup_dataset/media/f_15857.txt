[<text>
<title>salomon inc &lt;sb&gt; denies rumors of bond losses</title>
<dateline>    new york, april 9 - </dateline>robert salomon, managing director of
salomon inc, said rumors the wall street firm suffered bond
trading losses in the first quarter are not true.
    salomon said the rumors affected the company's stock and 
are "unequivocally denied."
    the salomon share price was off 5/8 at 37-1/2, but it had
been off more than one dlr on heavy volume earlier in the day.
    analysts said the rumors have been circulating in the
market for several days.
   
    salomon said the firm will report first quarter results on
april 22 or 23.
    analysts said the stock would have reacted poorly today,
even without the rumors, because of the decline in the stock
and bond markets.
    other brokerages were also down today. e.f. hutton &lt;efh&gt;
fell one to 41-1/4. bear stearns and co &lt;bsc&gt; slipped 3/8 to
19-1/2. painewebber group &lt;pwj&gt; fell 1/2 to 36, and first
boston corp &lt;fbc&gt; slid 3/8 to 49-3/4. merrill lynch and co inc
was off 7/8 at 42-3/8.
 reuter
</text>]