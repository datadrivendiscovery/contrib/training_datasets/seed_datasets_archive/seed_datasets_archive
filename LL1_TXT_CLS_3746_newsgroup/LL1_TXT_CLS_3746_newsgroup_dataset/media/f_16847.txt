[<text>
<title>&lt;profitt's inc&gt; files for initial offering</title>
<dateline>    alcoa, tenn., april 17 - </dateline>profitt's inc said it has filed
for an initial public offering of one mln common shares at an
expected price of eight to 10 dlrs each through underwriters
led by morgan keegan inc &lt;mor&gt;.
    it said proceeds will be used to retire debt.
    profitts operates five specialty department stores in
tennessee.
 reuter
</text>]