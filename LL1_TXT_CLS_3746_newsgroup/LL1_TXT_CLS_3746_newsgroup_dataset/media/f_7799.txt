[<text>
<title>thermo electron &lt;tmo&gt; to sell convertible debt</title>
<dateline>    new york, march 20 - </dateline>thermo electron corp said it filed
with the securities and exchange commission a registration
statement covering a 75 mln dlr issue of convertible
subordinated debentures due 2012.
    proceeds will be used principally for acquisitions of
companies that complement or expand thermo's existing line of
business and for general corporate purposes, it said.
    the company said the offering will be sold through shearson
lehman brothers inc, drexel burnham lambert inc and tucker,
anthony and r.l. day inc. they have an over-allotment option to
purchase an additional 11.25 mln dlrs of the debt.
 reuter
</text>]