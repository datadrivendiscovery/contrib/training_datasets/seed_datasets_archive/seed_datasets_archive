[<text>
<title>neoax &lt;noax&gt; to sell novatronics for 20 mln dlrs</title>
<dateline>    lawrenceville, n.j., march 26 - </dateline>neoax inc said it has
agreed to sell the assets and business of its novatronics
division to veeco instruments inc &lt;vee&gt; for 20 mln dlrs.
    neoax said it expects a gain of about nine mln dlrs on the
transaction which is expected to becomleted during the second
quarter, adding the gain will be sheltered by its tax loss
carryforwards.
    novatronics makes military-specification power supplies and
avionics components for various prime government defense
contractors. it had 1986 sales of 21 mln dlrs.
 reuter
</text>]