[<text>
<title>north yemen bought white sugar at tender - trade</title>
<dateline>    london, april 13 - </dateline>north yemen at its weekend tender bought
white sugar from a french operator acting on behalf of a swiss
house at 214.70 dlrs a tonne c and f, traders said.
    the amount bought was not immediately available, although
the country had sought 30,000 tonnes of june arrival whites,
they said.
 reuter
</text>]