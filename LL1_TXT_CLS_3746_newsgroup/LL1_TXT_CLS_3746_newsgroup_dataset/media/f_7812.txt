[<text>
<title>d and p lifts general public utilities&lt;gpu&gt; unit</title>
<dateline>    chicago, march 20 - </dateline>duff and phelps today raised the
ratings on general public utilities' pennsylvania electric co.
subsidiary fixed income securities.
    first mortgage bonds and collateralized pollution control
revenue bonds have been raised to d&amp;p-6 (middle a) from d&amp;p-7
(low a), debentures raised to d&amp;p-7 from d&amp;p-8 (high bbb), and
preferred stock raised to d&amp;p-8 from d&amp;p-9 (middle bbb).
    about 766 mln dlrs of debt is affected.
    the upgrade reflects general improvement in the company's
financial condition, duff and phelps said, citing the partial 
restoration of service, and rates, to three mile island.
 reuter
</text>]