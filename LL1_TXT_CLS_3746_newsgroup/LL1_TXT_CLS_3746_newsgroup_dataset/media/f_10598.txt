[<text>
<title>beard co &lt;bec&gt; year loss</title>
<dateline>    oklahoma city, march 27 - 
    </dateline>shr loss 57 cts vs profit 3.02 dlrs
    net loss 3,606,000 vs profit 8,294,000
    revs 15.3 mln vs 23.9 mln
    note: net includes gains from sale of uspci inc &lt;upc&gt; stock
of 1.5 mln vs 20.5 mln
    year-ago net includes loss from discontinued operations of
10.3 mln.
 reuter
</text>]