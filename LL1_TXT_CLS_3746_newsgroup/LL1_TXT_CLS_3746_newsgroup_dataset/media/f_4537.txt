[<text>
<title>new hampshire savings &lt;nhsb&gt; to buy bank</title>
<dateline>    concord, n.h., march 12 - </dateline>new hampshire savings bank corp
said it agreed to buy &lt;seashore bankshares inc&gt; in an exchange
of stock.
    according to the terms of the deal, it said seashore's
61,000 shares will be exchanged for 9.8 mln dlrs of new
hampshire savings stock.
    it said seashore bankshares has assets of about 46 mln
dlrs.

 reuter
</text>]