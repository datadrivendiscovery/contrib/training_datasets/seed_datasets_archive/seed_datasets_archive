[<text>
<title>sri lanka tendering overnight for wheat</title>
<dateline>    chicago, march 24 - </dateline>sri lanka will tender overnight for
52,500 tonnes of u.s., canadian and/or australian wheats for
april 8/16 shipment, under the export enhancement program if
u.s. origin, u.s. exporters said.
 reuter
</text>]