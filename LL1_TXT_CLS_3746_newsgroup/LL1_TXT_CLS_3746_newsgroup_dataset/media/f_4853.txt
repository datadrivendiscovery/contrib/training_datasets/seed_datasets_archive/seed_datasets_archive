[<text>
<title>sheller-globe names new president</title>
<dateline>    toledo, ohio, march 13 - </dateline>&lt;sheller-globe corp&gt; said it has
named alfred grava president and chief operating officer to
replace james graham who was granted a medical leave in june,
1986.
    in addition, the company said it named george berry
executive vice president of the corporation and president,
automotive/truck operations, reporting to grava.
 reuter
</text>]