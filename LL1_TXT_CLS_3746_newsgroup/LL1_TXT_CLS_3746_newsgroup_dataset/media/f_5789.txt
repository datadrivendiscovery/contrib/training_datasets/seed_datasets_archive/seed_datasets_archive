[<text>
<title>new zealand press group buys more texas newspapers</title>
<dateline>    wellington, march 17 - </dateline>&lt;independent newspapers ltd&gt; (inl)
said it bought two more community newspapers in houston, texas,
through a subsidiary there, for an undisclosed sum.
    inl said in a statement &lt;houston community newspapers inc&gt;
bought the south west advocate and the south bend advocate,
with combined circulation of 74,000 copies, and associated
assets, from &lt;the advocate communications corp inc&gt;.
    inl publishes wellington's morning and evening newspapers
as well as new zealand provincial dailies and newspapers in
rhode island. just under 40 pct of inl is owned by &lt;news ltd&gt;,
an australian subsidiary of news corp ltd &lt;ncpa.s&gt;.
    production and administration of the two publications would
be transferred to the company's existing centre in houston. inl
said the acquisition took effect on march 1.
    inl chairman alan burnet said the purchase would enable the
subsidiary to offer advertisers a combined distribution of nine
community newspapers to 340,000 homes in the greater houston
area. trading conditions in the area are particularly difficult
because the city's economy depends to a large extent on the
fortunes of the petroleum industry, but the situation should
improve in the medium to long term and the investment will
prove to be sound, he said.
 reuter
</text>]