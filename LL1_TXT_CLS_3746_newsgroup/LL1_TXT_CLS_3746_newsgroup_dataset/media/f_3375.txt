[<text>
<title>texas air &lt;tex&gt; unit to expand service</title>
<dateline>    houston, march 9 - </dateline>texas air corp's continental airlines
said it will begin service in april to bakersfield, calif., and
eugene and medford, ore., from its hub at denver.
    from denver, continental will fly a daily nonstop and a
one-stop flight to each of the three cities, the company said.
service to the oregon cities will start april 5 and to
bakersfield, april 22.
 reuter
</text>]