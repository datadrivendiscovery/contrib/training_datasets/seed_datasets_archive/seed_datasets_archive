[<text>
<title>colgate-palmolive &lt;cl&gt; plans to file debt</title>
<dateline>    new york, june 18 - </dateline>colgate-palmolive co said it plans to
file with the securities and exchange commission a shelf
registration covering up to 300 mln dlrs of unsecured debt
securities.
    reuben mark, chairman and president of the company, told
this to a meeting of the new york society of security analysts,
colgate-palmolive said in a release.
    proceeds will be used for general corporate purposes.
 reuter
</text>]