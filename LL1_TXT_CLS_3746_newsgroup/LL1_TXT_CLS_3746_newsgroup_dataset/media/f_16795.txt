[<text>
<title>union planters corp &lt;upcm.o&gt; 1st qtr net</title>
<dateline>    memphis, tenn., april 17 -
    </dateline>shr 92 cts vs 1.16 dlrs
    qtly div 10 cts vs 10 cts prior
    net 5,700,000 vs 5,400,000
    avg shrs 6,100,000 vs 3,700,000
    note: dividend pay may 15, record may one.
 reuter
</text>]