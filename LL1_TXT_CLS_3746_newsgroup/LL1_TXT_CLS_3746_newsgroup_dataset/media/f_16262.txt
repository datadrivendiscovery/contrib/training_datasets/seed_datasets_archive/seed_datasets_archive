[<text>
<title>first marathon &lt;fms.a.to&gt; plans stock split</title>
<dateline>    toronto, april 13 - </dateline>first marathon inc said it planned a
two-for-one stock split, to be effective on shareholders'
approval at the june 4 annual meeting.
    the financial services company said it also completed the
previously reported 29.6 mln dlr private placement of 1.5 mln
non-voting preferred shares convertible one-for-one into
non-voting class a shares.
 reuter
</text>]