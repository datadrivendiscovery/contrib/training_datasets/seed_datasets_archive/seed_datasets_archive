[<text>
<title>general devices inc &lt;gdic&gt; year net</title>
<dateline>    norristown, pa., march 18 -
    </dateline>shr profit 48 cts vs loss 21 cts
    net profit 1,308,503 vs loss 561,384
    revs 56.0 mln vs 66.1 mln
    note: 1986 net includes pretax gain 2,429,563 dlrs from
sale of worldwide computer services inc subsidiary and 352,000
dlr tax credit.
 reuter
</text>]