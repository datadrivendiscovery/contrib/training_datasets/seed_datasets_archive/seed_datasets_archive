[<text>
<title>att-philips bid for cgct outlined</title>
<dateline>    paris, march 17 - </dateline>american telephone and telegraph co and
philips telecommunications bv (apt) would hold 36 pct through
direct and indirect holdings in france's &lt;cie generale de
constructions telephoniques&gt; if a joint bid with french
partners for the soon-to-be-privatised firm succeeds, a
director at one of the partner firms said.
    marc mathieu of &lt;societe anonyme de telecommunications&gt;
sat, told journalists the bid foresaw a direct stake of 20 pct
for apt, the joint firm set up by the u.s.'s att &lt;t.n.&gt; and the
nv philips gloeilampenfabrieken &lt;pglo.as&gt;.
    the other 80 pct would be owned by a holding company made
up of sat, apt, cie du midi &lt;mcdp.pa&gt; and five mutual funds.
   under french law, foreign investors are restricted to a 20
pct direct stake in privatised companies but can boost their
stake to 40 pct through indirect holdings.
    the make-up of the holding company, however, is subject to
close discussions within the government due to legal queries
over the nationality of the mutual funds, a finance ministry
official said.
    although bought by french citizens they are managed by
foreign banks &lt;morgan guaranty trust co of new york&gt; and
&lt;banque de neuflize, schlumberger, mallet sa&gt;, controlled by
algemene bank nederland nv &lt;abnn.as&gt;, an sat spokesman said.
    cgct, which controls 16 pct of the french public telephone
switching market, is to be sold by the government for 500 mln
francs by private tender.
    five groups are bidding for the company and the government
has said it will choose cgct's new owner by the end of april.
    apt vice-president wim huisman told a news conference a
capital increase was envisaged if sat-apt wins cgct, but
declined to give details or say how an increase would affect
foreign stakes in cgct.
    in 1985, cgct posted losses of 200 mln francs on sales of
three billion after 1984 losses of 997 mln francs.
    a joint sat-apt statement added that buyers were committed
to investing 240 mln francs in cgct research and production
plants. the apt-sat offer includes a provision for cgct to
produce apt 5ess-prx switching technology and adapt it to
french standards.
    the tender was launched after a 1985 draft agreement for
att to take over cgct was abandoned following the introduction
of the french government privatisation laws which reopened
bidding among a wider range of applicants.
    other candidacies to take over cgct include west germany's
siemens ag &lt;sieg.f&gt; allied with schneider sa &lt;schn.pa&gt;
subsidiary jeumont-schneider, sweden's telefon ab lm ericsson
&lt;eric.st&gt; allied with matra &lt;matr.pa&gt; and bouygues sa
&lt;bouy.pa&gt;, italy's &lt;italtel&gt;, and canada's northern telecom ltd
&lt;ntl.to&gt;.
 reuter
</text>]