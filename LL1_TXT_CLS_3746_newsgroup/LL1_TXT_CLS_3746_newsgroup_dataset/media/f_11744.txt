[<text>
<title>oppenheimer sells six pct cyclops &lt;cyl&gt; stake</title>
<dateline>    washington, march 30 - </dateline>oppenheimer, the brokerage and
investment subsidiary of oppenheimer group inc, told the
securities and exchange commission it sold its entire 6.0 pct,
stake of cyclops corp.
    oppenheimer said it sold the 243,400-share stake on march
27 at 95.00 dlrs a share.
    it said it initially bought the stock in connection with
risk arbitrage and other investment activities in the ordinary
course of its business.
 reuter
</text>]