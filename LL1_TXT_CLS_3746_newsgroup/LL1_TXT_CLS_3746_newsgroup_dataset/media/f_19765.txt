[<text>
<title>mitsubishi raises car/truck prices in u.s.</title>
<dateline>    fountain valley, calif, june 29 - </dateline>mitsubishi motor sales of
america inc said it is raising car prices by an average 1.8
pct, or 227 dlrs and truck prices by 1.6 pct, or 150 dlrs,
because of the continued strength of the japanese yen against
the u.s. dollar.
    the company also said the price for its montero
sport/utility vehicle will be increased by 1.5 pct, or 150 dlr.
    the changes are effective immediately, mitsubishi also
said.
 reuter
</text>]