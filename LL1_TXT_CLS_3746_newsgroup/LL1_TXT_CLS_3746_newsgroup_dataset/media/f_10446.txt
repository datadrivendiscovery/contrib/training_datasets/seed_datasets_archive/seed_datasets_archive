[<text>
<title>clearwater fine foods acquires channel foods</title>
<dateline>    new york, march 27 - </dateline>clearwater fine foods inc, a canadian
company minority owned by hillsdown holdings plc of london, has
acquired channel foods ltd, a cornwall, england producer of
chilled smoke fish and pate products, hillsdown said.
    privately held clearwater was sold for three mln stg, the
company said.
 reuter
</text>]