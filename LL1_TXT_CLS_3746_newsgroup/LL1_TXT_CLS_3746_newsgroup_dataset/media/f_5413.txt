[<text>
<title>ryan homes &lt;ryn&gt; calls debentures</title>
<dateline>    pittsburgh, march 16 - </dateline>ryan homes inc, controled by nv
homes lp &lt;nvh&gt;, said it has called for redemption on april 30
all 873,000 dlrs of its six pct convertible subordinated
debentures due 1991 at 1,000 dlrs pluys 19.83 dlrs in accrued
interest per 1,000 dlrs principal amount.
    the debentures are convertible through april 28 into ryan
common stock at 30.50 dlrs per share.
 reuter
</text>]