[<text>
<title>e.f. hutton &lt;efh&gt; said firm sound</title>
<dateline>    new york, oct 20 - </dateline>e.f. hutton group inc president and
chief executive officer robert rittereiser said in a statement
the firm is having no operating or financial difficulty despite
the volatility of financial markets.
 reuter
</text>]