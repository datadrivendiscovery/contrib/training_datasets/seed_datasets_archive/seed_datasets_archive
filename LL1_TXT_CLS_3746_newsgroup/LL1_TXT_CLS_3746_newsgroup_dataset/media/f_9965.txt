[<text>
<title>first medical devices corp &lt;fmdc&gt; year loss</title>
<dateline>    bellevue, wash., march 26 -
    </dateline>shr loss 97 cts
    net loss 1,364,453
    sales 737,971
    note: company in development stage.
 reuter
</text>]