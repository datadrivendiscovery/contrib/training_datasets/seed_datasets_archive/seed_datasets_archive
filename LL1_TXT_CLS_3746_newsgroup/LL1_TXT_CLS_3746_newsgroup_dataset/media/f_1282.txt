[<text>
<title>norfolk southern &lt;nsc&gt; in building venture</title>
<dateline>    norfolk, va., march 3 - </dateline>norfolk southern corp said it
entered into a joint venture with sovran financial corp &lt;sovn&gt;
to build the sovran iii building, which will be renamed norfolk
southern tower.
    norfolk southern said it will locate its headquarters in
the tower and occupy the 17th through 21st floors. it did not
disclose the financial terms of the venture.
    the company said construction of the building adjacent to
the royster building in downtown norfolk begain in october
1986. another story has been added to the original plans,
making a 21-story tower.
 reuter
</text>]