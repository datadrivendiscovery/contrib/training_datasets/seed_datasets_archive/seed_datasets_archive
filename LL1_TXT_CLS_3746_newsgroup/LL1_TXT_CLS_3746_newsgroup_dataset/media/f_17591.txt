[<text>
<title>painewebber analyst sees record airline profits</title>
<dateline>    new york, june 1 - </dateline>painewebber group inc &lt;pwj&gt; analyst john
pincavage says most airline stocks are undervalued in relation
to record industry earnings expected in 1987.
    noting that the airlines as a group earned 85 mln dlrs in
the first quarter of 1987 compared with a loss of 625 mln dlrs
in the first quarter of 1986, pincavage sees an improvement of
similar magnitude in the current quarter. he believes earnings
for the group will total about one billion dlrs in the second
quarter of 1987 compared with 250 mln dlrs in earnings in the
second quarter of 1986. "we're shaping up for a record year in
1987," pincavage told reuters.
    for the third quarter of 1987, pincavage expects the group
to earn about 1.5 billion dlrs compared with one billion dlrs
in the same period of 1986.
    noting that the summer quarter is always a peak period, he
says "at this point it's too soon to tell" about trends in the
latter months of 1987. but he says for the group as a whole,
fare discounting practices are not pressuring earnings like
they were last year.
    pincavage has not changed any recommendations recently,
maintaining buys on texas air corp &lt;tex&gt; nwa inc &lt;nwa&gt; and amr
corp &lt;amr&gt; and rating most others attractive.
 reuter
</text>]