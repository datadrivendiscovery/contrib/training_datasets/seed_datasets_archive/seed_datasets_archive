[<text>
<title>bonn expresses support for u.s. on ec veg oil tax</title>
<dateline>    hamburg, march 20 - </dateline>the west german government expressed
support for the u.s. position in opposing the proposed european
community tax on vegetable oils and fats, a u.s. embassy
spokesman said.
    the spokesman, speaking from bonn, said, "we have good
reason to think west germany holds to its resistance to the
proposed tax."
    several top government officials told the american soybean
association and the national soybean processors association
delegations there was no reason for american producers and
processors to pay for ec agriculture, the spokesman said.
    european agriculture was facing severe problems, but both
the community and the u.s. should work closely within the
framework provided by the general agreement on tariffs and
trade, he said.
    the delegations will continue their top level meetings in
bonn today but will not issue a statement before returning to
the u.s. at the weekend.
    the ec and the u.s. realised the tax issue would stay on
the agenda for several months and there were indications that
both sides would have to prepare for some tough negotiations,
the spokesman said.
 reuter
</text>]