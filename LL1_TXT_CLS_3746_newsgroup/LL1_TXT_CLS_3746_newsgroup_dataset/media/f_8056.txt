[<text>
<title>neoax inc &lt;noax&gt; 4th qtr</title>
<dateline>    lawrenceville, n.j., march 20 -
    </dateline>shr loss 13 cts vs loss six cts
    net loss 1.4 mln vs loss 635,000
    revs 40.3 mln vs 28.5 mln
    year
    shr profit 40 cts vs profit 26 cts
    net profit 4.2 mln vs 2.6 mln
    revs 166.4 mln vs 94.6 mln
    note:1986 4th qtr and year net reflects dividend
requirements of 1.5 mln dlrs and 3.3 mln dlrs, and charges of
257,000 dlrs and 4.6 mln dlrs respectively which is not
accruable or payable because of pre-reorganization tax loss
carryforwards.
    1985 4th qtr and year net reflects dividend requirement of
1.1 mln dlrs and 2.3 mln dlrs, respectively, and charges of
472,000 dlrs and 2.9 mln dlrs respectively which is not
accruable or payable because of pre-organization tax loss
carryforwards.
 reuter
</text>]