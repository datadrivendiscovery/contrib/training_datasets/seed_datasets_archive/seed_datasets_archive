[<text>
<title>life of indiana corp &lt;lifi&gt; 4th qtr loss</title>
<dateline>    indianapolis, ind., march 25 - 
    </dateline>shr loss 19 cts vs profit 57 cts
    net loss 103,005 vs profit 319,344
    year
    shr profit 22 cts vs profit 10 cts
    net profit 1,236,347 vs profit 570,222
 reuter
</text>]