[<text>
<title>american national corp &lt;fnb&gt; 1st qtr net</title>
<dateline>    chicago, april 13 -
    </dateline>net 12.8 mln vs 12.2 mln
    loans 2.8 billion vs 2.5 billion
    deposits 3.2 billion vs 2.9 billion
    assets 4.5 billion vs 3.8 billion
    note: american national corp is a wholly-owned subsidiary
of first chicago corp.
 reuter
</text>]