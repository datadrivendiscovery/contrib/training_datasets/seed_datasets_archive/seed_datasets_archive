[<text>
<title>canterra energy to drill well off nova scotia</title>
<dateline>    calgary, alberta, march 18 - </dateline>&lt;canterra energy ltd&gt; said it
will drill an exploratory well on the scotian shelf, about 280
kilometers east-southeast of halifax, nova scotia.
    drilling will begin in late april 1987 in 61 meters of
water and will be drilled to a total depth of 3,500 meters,
canterra said.
    canterra will operate the well and has a 39 pct interest in
it. &lt;petro-canada inc&gt; has 26 pct, &lt;trillium exploration corp&gt;
has 20 pct and &lt;nova scotia resources (ventures) ltd&gt; has 15
pct.
 reuter
</text>]