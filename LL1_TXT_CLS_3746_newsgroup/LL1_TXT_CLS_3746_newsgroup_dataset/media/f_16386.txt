[<text>
<title>park communications inc &lt;parc.o&gt; 1st qtr mar 31</title>
<dateline>    ithaca, n.y., april 13 -
    </dateline>shr 15 cts vs 14 cts
    net 2,028,000 vs 1,879,000
    revs 32.1 mln vs 29.5 mln
 reuter
</text>]