[<text>
<title>japan central bank intervenes in tokyo afternoon</title>
<dateline>    tokyo, april 7 - </dateline>the bank of japan intervened in early
afternoon tokyo trading to support the dollar against active
selling by institutional investors and speculative selling by
overseas operators, dealers said.
    the central bank had also bought dollars against the yen in
morning trade.
    the dollar traded around 145.20/30 yen when trading began
in the afternoon here and weakened only slightly, the dealers
said.
 reuter
</text>]