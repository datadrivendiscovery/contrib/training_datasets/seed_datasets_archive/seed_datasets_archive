[<text>
<title>consolidated freightways inc &lt;cnf&gt; 3rd qtr net</title>
<dateline>    palo alto, calif., oct 19 -
    </dateline>shr 43 cts vs 63 cts
    net 16,362,000 vs 24,325,000
    revs 589.3 mln vs 549.1 mln
    nine mths
    shr 1.40 dlrs vs 1.73 dlrs
    net 54,011,000 66,591,000
    revs 1.68 1.58 billion
 reuter
</text>]