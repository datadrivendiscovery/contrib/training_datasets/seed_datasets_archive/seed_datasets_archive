[<text>
<title>french subsidized corn for tunisia/morocco-usda</title>
<dateline>    washington, april 1 - </dateline>u.s. corn sales to tunisia, morocco
and other north african countries may face increased
competition from european community (ec) corn sales, the u.s.
agriculture department said.
    in its world production and trade report, the usda said
sales of french corn for nearby delivery have been confirmed
with an export subsidy of about 145 dlrs per tonne, bringing
the french price to about 72 dlrs per tonne, fob.
    while this is about the same price as u.s. corn, ec corn
has lower transport costs, the department noted.
    the french sales mark the beginning of commercial ec corn
exports which could reach 750,000 tonnes to north africa and
the middle east, areas which have traditionally purchased their
corn needs from the united states, the department said.
    department officials said the 750,000 tonnes of exports are
for the year up to september 30 1987.
    they said export licenses for about 500,000 tonnes have
been issued so far.
 reuter
</text>]