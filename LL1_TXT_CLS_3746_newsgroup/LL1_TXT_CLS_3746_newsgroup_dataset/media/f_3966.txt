[<text>
<title>turner broadcasting's &lt;tbs&gt; cnn to air in china</title>
<dateline>    altanta, march 11 - </dateline>turner broadcasting system inc's cnn
said it reached an agreement with china central television to
air its news programming in china.
    it said china central tv will use cnn reports during its
daily newscasts on two channels, reaching an audience of about
300 mln viewers.
    the company said the agreement also gives cnn the right to
sell advertising spots on china central tv.
    cnn also said it reached an agreement with televerket kabel
tv of sweden to broadcast 24-hour news on its cable tv network.
the program will reach an initial 118,000 viewers.
 reuter
</text>]