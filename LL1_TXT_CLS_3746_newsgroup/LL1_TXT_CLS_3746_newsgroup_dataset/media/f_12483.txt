[<text>
<title>bankamerica &lt;bacp&gt; raises prime rate to 7.75 pct</title>
<dateline>    san francisco, april 2 - </dateline>bankamerica corp, following moves
by other major banks, said it has raised its prime rate to 7.75
pct from 7.50 pct, effective today.
 reuter
</text>]