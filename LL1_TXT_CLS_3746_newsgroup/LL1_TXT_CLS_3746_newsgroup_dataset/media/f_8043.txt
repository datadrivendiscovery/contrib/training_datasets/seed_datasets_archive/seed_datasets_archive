[<text>
<title>macmillan bloedel &lt;mmblf&gt; to redeem debentures</title>
<dateline>    vancouver, british columbia, march 20 - </dateline>macmillan bloedel
ltd said it will redeem all outstanding nine pct series j
debentures on april 27, 1987 for 34.9 mln u.s. dlrs, plus a
premium of one pct and accrued and unpaid interest.
    the series j debentures were issued in europe in 1977 and
were due february 1992, the company said.
 reuter
</text>]