[<text>
<title>u.s house budget chairman proposes spending hold</title>
<dateline>    washington, march 17 - </dateline>house budget committee chairman
william gray, d-pa, said he would propose a freeze on all
spending for federal government programs for the fiscal 1988
budget to meet budget deficit targets.
    "an outlay freeze -- with no additional revenues -- produces
almost enough savings to reach the 108 billion dlr deficit
target. for fiscal 1988, this would mean total spending of just
over 1 trillion dlrs," gray said in a statement.
    gray said his plan would call for a freeze in spending on
each individual federal program at fiscal 1987 levels.
    he said this would mean numerous program cuts, lower
benefits for recipients of federal assistance and fewer people
being served by federal programs.
    gray said he would make the budget freeze proposal when the
house budget committee begins work on the budget resolution
this thursday. the senate budget committee began work on its
budget resolution this afternoon.
 reuter
</text>]