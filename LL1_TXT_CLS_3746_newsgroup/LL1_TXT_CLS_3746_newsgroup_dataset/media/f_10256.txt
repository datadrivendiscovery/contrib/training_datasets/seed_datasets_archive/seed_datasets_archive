[<text>
<title>bolivia to propose andean summit with reagan</title>
<dateline>     la paz, march 26 - </dateline>bolivian president victor paz
estenssoro will propose to andean pact countries a summit
meeting with u.s. president reagan to take place in venezuela
in may, foreign minister guillermo bedregal announced.
     he told reporters that the summit "if everything goes
well" will possibly take place in one or two months.
     "we are coordinating an eventual meeting of the andean
pact presidents and president reagan, which could take place in
venezuela in may", he said. the proposal is part of bolivia's
intention to relaunch the andean pact, he added.
     the andean pact, formed by colombia, venezuela, peru and
ecuador, is a sub-regional economic group set up in 1969 to
counteract economic progress of other more developed south
american countries, such as argentina and brazil.
 reuter
</text>]