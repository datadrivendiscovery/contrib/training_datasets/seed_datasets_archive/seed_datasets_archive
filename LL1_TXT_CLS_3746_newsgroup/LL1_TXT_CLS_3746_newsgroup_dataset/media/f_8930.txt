[<text>
<title>suffield financial corp &lt;sfcp&gt; raises quarterly</title>
<dateline>    suffield, conn., march 24 -
    </dateline>qtly div five cts vs three cts prior
    pay april 10
    record march 31
 reuter
</text>]