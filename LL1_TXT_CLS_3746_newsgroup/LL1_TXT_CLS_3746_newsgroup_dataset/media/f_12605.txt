[<text>
<title>u.k. to sell royal ordnance to british aerospace</title>
<dateline>    london, april 2 - </dateline>u.k. defence minister george younger said
he had accepted an offer from british aerospace plc &lt;bael.l&gt; to
buy state-owned armaments manufacturer &lt;royal ordnance&gt; for 190
mln stg.
    the british aerospace bid had been competing against a
rival offer from engineering group gkn plc &lt;gkn.l&gt;.
    younger told parliament the sale was conditional on
consideration by the office of fair trading.
    he said its recommendation should be available next week
and, subject to approval by the secretary of state for trade
and industry, should be completed before easter.
    the decision marks the end of a period of uncertainty about
ownership of the company, younger said.
    "but, equally important, it opens up the full range of
opportunities for development and growth of business which are
only really available under good private sector management," he
said.
 reuter
</text>]