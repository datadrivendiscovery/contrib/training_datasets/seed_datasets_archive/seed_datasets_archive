[<text>
<title>affiliated publications inc &lt;afp&gt; sets dividend</title>
<dateline>    boston, march 16 -
    </dateline>qtly div eight cts vs eight cts
    pay june one
    record may 15
 reuter
</text>]