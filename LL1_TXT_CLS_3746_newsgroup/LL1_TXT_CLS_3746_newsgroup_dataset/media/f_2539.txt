[<text>
<title>e.a. viner holdings ltd &lt;eavkf&gt; 4th qtr loss</title>
<dateline>    toronto, march 5 -
    </dateline>shr loss 10 cts vs profit seven cts
    net loss 918,000 vs profit 585,000
    revs 5,475,000 vs 4,430,000
    year
    shr profit 32 cts vs loss 24 cts
    net profit 2,909,000 vs loss 1,501,000
    revs 23.7 mln vs 15.0 mln
    note: 1986 4th qtr net includes 1.5 mln u.s. dlr, or 17 ct
shr, writedown of stake in heck's inc &lt;hex&gt; and 300,000 u.s.
dlr, or three ct shr, writedown of arbitrage positions. 1986
fl-yr net includes 900,000 dlr net writedown of stake in
heck's.
    u.s. dlrs.
 reuter
</text>]