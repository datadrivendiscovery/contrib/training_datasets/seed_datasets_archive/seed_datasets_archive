[<text>
<title>grumman &lt;gq&gt; holders approve liability limits</title>
<dateline>    bethpage, n.y., april 17 - </dateline>grumman corp said shareholders
at the annual meeting approved an amendment on the
indemnification of direcors, officers and employees against
liability.
 reuter
</text>]