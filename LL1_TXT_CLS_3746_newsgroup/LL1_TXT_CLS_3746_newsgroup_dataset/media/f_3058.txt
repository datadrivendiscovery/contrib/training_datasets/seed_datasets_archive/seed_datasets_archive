[<text>
<title>&lt;burton group plc&gt; to trade adr's in u.s.</title>
<dateline>    new york, march 9 - </dateline>british retailer burton group plc said
trading in its american depositary receipts will start today.
    each adr represents four burton shares.
 reuter
</text>]