[<text>
<title>m.d.c. asset investors &lt;mir&gt; in initial payout</title>
<dateline>    denver, march 20 - </dateline>m.d.c. asset investors inc, which
recently went public, said its board declared an initial
quarterly dividend of 45 cts per share, payable april 15 to
holders of record april one.
 reuter
</text>]