[<text>
<title>&lt;shell u.k. ltd&gt; year 1986</title>
<dateline>    london, march 24 -
    </dateline>sales proceeds 6.57 billion stg vs 8.81 billion
    duty and value added tax 1.84 billion vs 1.60 billion
    net proceeds 4.73 billion vs 7.21 billion
    net profit 757 mln vs 667 mln
    average capital employed 3.63 billion vs 3.71 billion
    capital and exploration expenditure 644 mln vs 618 mln
    cash surplus 423 mln vs 584 mln.
    note - company is wholly owned subsidiary of royal
dutch/shell group &lt;rd.as&gt;
 reuter
</text>]