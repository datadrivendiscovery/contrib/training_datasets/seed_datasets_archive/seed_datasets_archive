[<text>
<title>personal computer products &lt;pcpi.o&gt; places debt</title>
<dateline>    san diego, calif., april 13 - </dateline>personal computer products
inc said it completed a 4.2-mln-dlr, five pct convertible
preferred stock issue to private investors.
    net proceeds of about 3.8 mln dlrs will be used to expand
marketing, sales and research and development efforts, to
supplement working capital, and to repay the company's entire
bank debt of about 650,000 dlrs.
 reuter
</text>]