[<text>
<title>royal palm &lt;rpal&gt; says restrictions lifted</title>
<dateline>    west palm beach, fla., march 16 - </dateline>royal palm savings
association said it has entered into an agreement with the
federal home loan bank board and the florida department of
banking and finance resulting in the removal of limitations
which had severely restricted its growth and lending.
    the company said it will now be permitted to grow about 70
mln dlrs in calendar 1987, or about 15 pct for this year, and
at the same rate thereafter.
    royal palm said under the accord, it will resume commercial
and nonresidential lending but will place substantial emphasis
on residential loans.
    the company said it agreed to adopt new policies and
procedures but did not elaborate.
 reuter
</text>]