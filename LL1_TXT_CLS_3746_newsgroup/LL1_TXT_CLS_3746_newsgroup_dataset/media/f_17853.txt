[<text>
<title>ladd &lt;ladd.o&gt; unit completes acquisition</title>
<dateline>    hickory, n.c, june 1 - </dateline>ladd furniture inc's clayton-marcus
furniture subsidiary said it completed the previously announced
purchase of privately-held colony house furniture inc
for an undisclosed amount of cash and notes.
 reuter
</text>]