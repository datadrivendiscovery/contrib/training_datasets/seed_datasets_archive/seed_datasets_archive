[<text>
<title>borman's &lt;brf&gt; to buy safeway's utah division</title>
<dateline>    oakland, calif., march 4 - </dateline>safeway stores inc said it
agreed to sell the assets and operations of its salt lake city
division to borman's inc under undisclosed terms.
    the division includes 60 operating supermarkets in five
states, most of which are in utah, idaho and wyoming, together
with distribution and manufacturing facilities, safeway said.
    it said sales for the division for the year ended january
three were about 350 mln dlrs.
    safeway also said the transaction is subject to borman's
ability to obtain financing and to successfully negotiate new
labor agreements with the various unions involved.
 reuter
</text>]