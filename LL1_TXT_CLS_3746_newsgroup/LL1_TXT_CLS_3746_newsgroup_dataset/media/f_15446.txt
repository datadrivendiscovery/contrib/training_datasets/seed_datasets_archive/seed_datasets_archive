[<text>
<title>thailand to renew long term sugar contracts -trade</title>
<dateline>    london, april 9 - </dateline>thailand is to negotiate tomorrow with
selected trade houses for renewal of long term raw sugar sales
contracts, to cover the next five years at a rate of 60,000
tonnes annually, traders said.
    they also reported vague talk algeria may be seeking 50,000
tonnes of raws tomorrow but details are unclear.
 reuter
</text>]