[<text>
<title>leisure concepts &lt;lcic.o&gt;, lorimar &lt;lt&gt; in deal</title>
<dateline>    new york, june 1 - </dateline>leisure concepts inc said it has signed
a definitive agreement to be licensing agent for all
lorimar-telepictures corp television and film properties,
formalizing a preliminary agreement reached in january.
    it said undser the terms, lorimar has received a five-year
warrant to buy up to 250,000 leisure concepts shares at 6.25
dlrs each.  leisure concepts now has about 3,100,000 shares
outstanding.
 reuter
</text>]