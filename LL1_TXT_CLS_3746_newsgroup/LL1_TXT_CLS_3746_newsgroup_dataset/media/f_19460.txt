[<text>
<title>american royalty trust &lt;ari&gt; quarterly dividend</title>
<dateline>    houston, june 19 - 
    </dateline>qtly div 14.25 cts vs 14.25 cts prior
    pay sept 21
    record july 1
 reuter
</text>]