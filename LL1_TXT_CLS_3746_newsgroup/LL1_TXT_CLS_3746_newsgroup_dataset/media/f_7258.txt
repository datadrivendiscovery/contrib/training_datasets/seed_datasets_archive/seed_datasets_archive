[<text>
<title>litton &lt;lit&gt; wins 164.2-mln-dlr navy contract</title>
<dateline>    beverly hills, calif., march 19 - </dateline>litton industries said
its applied technology division won a 164.2 mln dlr contract
from the u.s. navy to produce an/alr-67 (v) and an/alr-45f
airborne threat warning systems.
    the company said the systems will be used on a variety of
navy attack and reconnaissance aircraft.
    deliveries are set from august this year to february, 1990.
    the navy also has an option in june to increase the
contract value to 226.6 mln dlrs, litton said.
 reuter
</text>]