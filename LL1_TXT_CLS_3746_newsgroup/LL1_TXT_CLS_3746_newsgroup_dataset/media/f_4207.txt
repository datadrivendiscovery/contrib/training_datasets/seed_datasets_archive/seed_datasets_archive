[<text>
<title>stan west &lt;swmc&gt; seeks construction financing</title>
<dateline>    scottsdale, ariz., march 12 - </dateline>stan west mining corp said it
engaged an investment banking firm to act as financial adviser
in arranging a construction financing package for mccabe mine.
    the company said it intends proceeds of the financing
package would be utilized to construct a 500 ton per day mil,
complete the underground construction necessary to start mining
and provide working capital.
 reuter
</text>]