[<text>
<title>guilford mills &lt;gfd&gt; to sell convertible debt</title>
<dateline>    new york, march 13 - </dateline>guilford mills inc said it filed with
the securities and exchange commission a registration statement
covering a 60 mln dlr issue of convertible subordinated
debentures.
    proceeds will be used to repay certain indebtedness and
increase working capital, as well as for general corporate
purposes.
    guilford mills said it expects the issue will be offered
later this month. the company named bear, stearns and co as
lead underwriter of the offering.
 reuter
</text>]