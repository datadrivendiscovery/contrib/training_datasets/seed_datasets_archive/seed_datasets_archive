[<text>
<title>dutch central bank intervenes to support dollar</title>
<dateline>    amsterdam, april 27 - </dateline>the dutch central bank intervened
modestly to support the dollar with spot market transactions,
dealers said.
    they said the bank bought dollars against guilders as the
u.s. currency dipped to a low of 2.0013 guilders from 2.0130-40
on opening, the lowest since the end of january.
    there was no intervention at the fix, however, which put
the dollar at 2.0045 guilders after 2.0280 last friday, and
dealers said the bank's buying was limited.
    "i'd be surprised if the bank had bought as much as 100 mln
dlrs," one dealer said.
 reuter
</text>]