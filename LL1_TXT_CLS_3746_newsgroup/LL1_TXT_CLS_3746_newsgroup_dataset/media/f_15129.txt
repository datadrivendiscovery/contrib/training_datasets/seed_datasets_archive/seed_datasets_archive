[<text>
<title>japan buys 4,000 tonnes canadian rapeseed</title>
<dateline>    winnipeg, april 8 - </dateline>japan bought 4,000 tonnes of canadian
rapeseed overnight at an undisclosed price for last half
may/first half june shipment, trade sources said.
 reuter
</text>]