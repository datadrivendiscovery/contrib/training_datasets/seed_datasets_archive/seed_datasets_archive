[<text>
<title>mannville oil and gas ltd &lt;mog.to&gt; 1st qtr net</title>
<dateline>    calgary, alberta, june 1 -
    </dateline>shr two cts vs seven cts
    net 164,000 vs 417,000
    revs 1,345,000 vs 2,021,000
 reuter
</text>]