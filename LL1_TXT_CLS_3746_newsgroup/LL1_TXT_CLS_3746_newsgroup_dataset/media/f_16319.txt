[<text>
<title>&lt;new milford savings bank&gt; 1st qtr net</title>
<dateline>    new milford, conn., april 13 -
    </dateline>shr 62 cts vs 26 cts
    net 2,312,000 vs 944,000
    note: 1987 includes five ct shr charge from loan loss
provision.
 reuter
</text>]