[<text>
<title>vw has no comment on seat loss reports</title>
<dateline>    wolfsburg, west germany, march 13 - </dateline>a volkswagen ag
&lt;vowg.f&gt; spokesman said the group had no immediate comment on
reports of greater than expected losses at its spanish
subsidiary sociedad espanola de automoviles de turismo (seat).
    german newspapers reported that werner schmidt, seat
supervisory board chairman, had told journalists that seat
losses for 1986 were around 27 billion pesetas, or about 386
mln marks, almost double original expectations.
    according to the boersen-zeitung newspaper, schmidt said vw
would invest 42 billion pesetas in seat this year and in the
years to 1995 would spend 462 billion on its new spanish unit.
 reuter
</text>]