[<text>
<title>american information technologies &lt;ait&gt; payout</title>
<dateline>    chicago, june 18 -
    </dateline>qtly div 1.25 dlrs vs 1.25 dlrs prior
    pay aug one
    record june 30
    note: american information technologies corp.
 reuter
</text>]