[<text>
<title>rowntree mackintosh plc&lt;rwnt.l&gt; year to end january</title>
<dateline> london, march 12 -
    </dateline>shr 35.0p vs 34.8p
    div final div 9.2p vs 8.2p
    pretax profit 84.0 mln stg vs 79.3 mln
    net after tax 66.2 mln vs 60.7 mln
    turnover 1,290.4 mln vs 1,205.2 mln
    trading profit 105.7 mln stg vs 101.3 mln, consisting -
    u.k  47.9 mln vs 45.3
    europe 7.8 mln vs 3.4 mln
    north america 34.7 mln vs 37.2 mln
    australasia 4.0 mln vs 2.3 mln
    rest of world 11.3 mln vs 13.1 mln
 reuter
</text>]