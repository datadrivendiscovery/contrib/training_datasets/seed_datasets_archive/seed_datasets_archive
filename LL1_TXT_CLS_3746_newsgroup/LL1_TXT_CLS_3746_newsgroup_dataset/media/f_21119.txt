[<text>
<title>senate boss asks reagan to fight stock fall</title>
<dateline>    washington, oct 19 - </dateline>senate democratic leader robert byrd,
pointing to panic selling on wall street, urged president
reagan to join congress in fighting trade and budget deficits.
    "it is time the administration put policies ahead of
politics," byrd said.
    byrd said the president needs "to begin working with
congress to reduce the double deficits in trade and the budget
that cause such a cloud on this nation's future."
 reuter
</text>]