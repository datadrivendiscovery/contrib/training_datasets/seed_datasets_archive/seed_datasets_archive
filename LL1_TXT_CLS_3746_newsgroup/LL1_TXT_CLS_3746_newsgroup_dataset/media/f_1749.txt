[<text>
<title>noland co &lt;nold&gt; names new chief executive</title>
<dateline>    newport news, va., march 4 - </dateline>noland co said effective april
23, when lloyd u. noland jr. turns 70 and retires as previously
announced as chairman and president, lloyd u. noland
iii will become chairman and chief executive officer and carl
watson president and chief operating officer.
    the company said noland iii, 43, is vice president, manager
of merchandising for plumbing and heating products. watson, 64,
is executive vice president, marketing.
    it said noland jr. will also leave the board on april 23,
along with s.q. groover a retired noland executive serving as a
consultant on real estate and facilities matters.


</text>]