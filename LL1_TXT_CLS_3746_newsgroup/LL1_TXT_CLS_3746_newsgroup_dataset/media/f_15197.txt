[<text>
<title>walgreen co &lt;wag&gt; votes quarterly dividend</title>
<dateline>    deerfield, ill., april 8 -
    </dateline>qtly div 13-1/2 cts vs 12-1/2 cts prior qtr
    pay 12 june
    record 21 may
 reuter
</text>]