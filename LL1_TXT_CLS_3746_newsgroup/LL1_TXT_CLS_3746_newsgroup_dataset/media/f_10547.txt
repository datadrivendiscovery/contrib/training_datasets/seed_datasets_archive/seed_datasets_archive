[<text>
<title>southwestern bell&lt;sbc&gt; votes split, ups payout</title>
<dateline>    st. louis, march 27 - </dateline>southwestern bell corp said its board
voted a three-for-one stock split and increased the dividend
8.8 pct to 1.60 dlrs a share.
    on a post-split basis, the increased dividend will be 58
cts a share, payable may one to holders of record april 10.
    southwestern bell said the stock split is
its first. it said shares will be mailed may 22, record may
four.
 reuter
</text>]