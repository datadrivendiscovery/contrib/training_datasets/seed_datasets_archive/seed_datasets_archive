[<text>
<title>first capital &lt;fch&gt; esop to buy more stock</title>
<dateline>    los angeles, oct 20 - </dateline>first capital holding corp said its
board authorized management to up to two mln shares of the
company's common stock for its employee stock ownership plan.
    the company said its board had previously authorized
purchases of up to one mln shares and to date, the plan has
only purchased 100,000 shares.
 reuter
</text>]