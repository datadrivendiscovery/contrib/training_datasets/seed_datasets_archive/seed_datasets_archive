[<text>
<title>u.s. subcommitee approves international debt bill</title>
<dateline>    washington, march 17 - </dateline>a house banking subcommittee
approved legislation which would require the treasury secretary
to begin negotiations on the establishment of an international
debt adjustment facility.
    the facility would buy up some of the debt of less
developed countries and sell it to banks at a discount.
    the facility was proposed by rep. john lafalce, d-n.y., who
said it would help ease the third world debt crisis. it was
approved by the house international finance subcommittee and
sent to the full house banking committee, which will consider
the measure next week.
    the legislation is part of the omnibus trade bill being
considered by several committees.
 reuter
</text>]