[<text>
<title>&lt;gateway medical systems inc&gt; 3rd qtr loss</title>
<dateline>    dallas, march 17 - </dateline>period end jan 31
    shr loss one cts vs profit eight cts
    net loss 52,198 vs profit 602,766
    revs 18.6 mln vs 7,833,424
    nine mths
    shr profit 10 cts vs profit six cts
    net profit 809,243 vs profit 393,372
    revs 36.3 mln vs 18.7 mln
 reuter
</text>]