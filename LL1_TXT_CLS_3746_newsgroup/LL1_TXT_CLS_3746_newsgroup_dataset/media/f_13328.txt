[<text>
<title>japan auctions 280 billion yen of four-year notes</title>
<dateline>    tokyo, april 7 - </dateline>the finance ministry auctioned 280 billion
yen of four-year government notes, the first auction of fiscal
1987 which started on april 1.
    the coupon was set at 3.7 pct, a record low for any
government note and below the 3.8 pct set on the last two-year
notes, auctioned in february.
    today's issue was the first for four-year notes since last
may, which produced an average price of 99.79, average yield of
4.561 pct and a coupon of 4.5 pct, the ministry said.
    the auction results will be announced tomorrow afternoon.
 reuter
</text>]