[<text>
<title>takeover speculation lifts hutton &lt;efh&gt; shares</title>
<dateline>    new york, june 1 - </dateline>e.f. hutton group inc shares rose on
speculation the company would receive a takeover offer, traders
said.
    hutton's stock also was affected by a newspaper report that
first boston corp &lt;fbc&gt; accumulated almost five pct of hutton's
stock on behalf of an outside client, traders said. traders
said the story, which appeared in usa today, added speculation
which began on the street last week. they said there were
rumors the stock was under accumulation and speculation
abounded the company would soon receive an offer. a hutton
official declined comment. hutton's stock rose 2-1/4 to 39-3/8.
    hutton several months ago rejected a buyout offer from
shearson lehman brothers inc &lt;she&gt;. the newspaper story
mentioned speculation american express co &lt;axp&gt;, the parent of
shearson, was a possible buyer. but traders said the rumors
today did not name buyers. first boston officials were not
immediatley available for comment.
    prudential bache analyst larry eckenfelder said he doubted
the speculation about american express. he said he believed
hutton, which is occassionally surrounded by rumors, moved up
today as a result of the newspaper article. "hutton is still a
takeover candidate," said eckenfelder.
 reuter
</text>]