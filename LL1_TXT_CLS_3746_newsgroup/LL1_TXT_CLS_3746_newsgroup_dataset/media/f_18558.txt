[<text>
<title>tnp enterprises &lt;tnp&gt; unit to sell bonds</title>
<dateline>    new york, june 18 - </dateline>texas-new mexico power co, a unit of
tnp enterprises inc, said it filed with the securities and
exchange commission a registration statement covering 65 mln
dlr issue of first mortgage bonds.
    proceeds will be used to repay short-term debt and for
other general purposes, the company said.
    it said the bonds would probably be offered through
shearson lehman brothers inc and salomon brothers inc.
 reuter
</text>]