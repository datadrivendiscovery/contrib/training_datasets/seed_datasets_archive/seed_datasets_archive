[<text>
<title>jennifer convertibles &lt;jenn&gt;initial offer starts</title>
<dateline>    new york, march 12 - </dateline>jennifer convertibles inc said an
initial public offering of 500,000 units is underway at eight
dlrs each through underwriters led by evans and co inc.
    each unit consists of two common shares and one redeemable
class a warrant enabling the holder to buy one common share at
five dlrs until march 11, 1992.
    the shares and warrants are immediately separately
transferable.
 reuter
</text>]