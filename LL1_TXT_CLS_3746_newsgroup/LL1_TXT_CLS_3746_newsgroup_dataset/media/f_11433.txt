[<text>
<title>cpl real estate &lt;cntrs&gt; cuts regular payout</title>
<dateline>    davenport, iowa, march 31 -
    </dateline>qtrly div 26 cts vs 27 cts prior
    payable april 27
    record april 10
    note: full name of company is cpl real estate trust
investment.
 
 reuter
</text>]