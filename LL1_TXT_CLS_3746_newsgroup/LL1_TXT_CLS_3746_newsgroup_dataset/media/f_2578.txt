[<text>
<title>instrumentarium acquires nokia subsidiary</title>
<dateline>    helsinki, march 6 - </dateline>finland's medical group instrumentarium
oy &lt;inmr.he&gt; said it has acquired electronics components
importers and marketers &lt;ferrado oy&gt; and &lt;insele oy&gt;,
subsidiaries of finland's electronics group nokia oy &lt;noks.he&gt;.
    it said in a statement ferrado and insele will be merged
into instrumentarium's professional electronics and information
systems division.
    it did not disclose a price for the acquisitions but said
it had issued 30,000 restricted b shares as partial payment to
nokia.
 reuter
</text>]