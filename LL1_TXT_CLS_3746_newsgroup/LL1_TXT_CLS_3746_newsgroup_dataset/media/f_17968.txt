[<text>
<title>eni unit agip petroli buys stake in u.s. company</title>
<dateline>    rome, june 2 - </dateline>a subsidiary of state energy concern ente
nazionali idrocarburi &lt;entn.mi&gt; (eni) said it has acquired a 50
pct stake in &lt;steuart petroleum co&gt;, an independent u.s. oil
products company. financial terms were not disclosed.
    agip petroli spa said in a statement that the remaining 50
pct of the u.s. firm is owned by &lt;steuart investment co&gt;, a
holding company which also has interests transportation, hotels
and insurance.
    the italian firm said steuart petroleum operates primarily
on the east coast of the u.s.
 reuter
</text>]