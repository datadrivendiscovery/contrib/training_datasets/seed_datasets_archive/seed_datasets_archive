[<text>
<title>chevron sells three north sea interests</title>
<dateline>    london, april 27 - </dateline>chevron corp's &lt;chv&gt; &lt;chevron petroleum
(u.k.) ltd&gt; said it sold interests in two north sea blocks and
part of a third block to &lt;midland and scottish resources ltd&gt;.
    the two firms said they have signed an agreement which
transfers chevron's interests, comprising 29.41 pct in block
2/10b under licence p326, 29.41 pct in block 2/15a under
licence p327 and part of block 2/10a under licence p234.
    the part of block 2/10a which is being sold covers
chevron's interest in the emerald accumulation, discovered by
the company in 1978. chevron is retaining its 25 pct interest
in the remainder of block 2/10a and in block 3/28a under
licence p234.
 reuter
</text>]