[<text>
<title>deutsche agrees full takeover of former eurasbank</title>
<dateline>    frankfurt, april 2 - </dateline>deutsche bank ag &lt;dbkg.f&gt; has agreed
to take over the outstanding 25 pct in &lt;deutsche bank (asia)
ag&gt;, the former &lt;european asian bank ag&gt;, from
creditanstalt-bankverein &lt;cabv.vi&gt;, deutsche management board
joint spokesman alfred herrhausen told a news conference.
    euras broke even in 1986 and required no funding from
deutsche, he said. he gave no details of the deal with
creditanstalt.
    press reports that deutsche was planning a takeover of
lloyds bank plc &lt;lloy.l&gt; were without foundation, he said.
    herrhausen said deutsche had taken part in the recent
capital increase of &lt;morgan grenfell group plc&gt;, but had no
plans to raise its 4.4 pct share in morgan grenfell.
    &lt;banca d'america e d'italia spa&gt;, in which 98.3 pct was
acquired from bankamerica corp &lt;bac.n&gt; late last year, would be
consolidated this year, and contribute to further expansion of
deutsche's business, management board joint spokesman f.
wilhelm christians said.
    following a ruling earlier this year from the federal high
court, deutsche had included its non-bank holdings in the
balance sheet section "participations," christians said.
    but christians said the bank still regarded shares in these
non-bank companies as an investment rather than an active
participation.
    parent bank total participations rose to 6.45 billion marks
in 1986 from a comparable 4.57 billion in 1985 including a rise
to 3.79 billion from 2.34 billion in bank participations.
    herrhausen said the grouping of holdings in the balance
sheet in this was was not a first step to floating them off in
a separate holding company.
 reuter
</text>]