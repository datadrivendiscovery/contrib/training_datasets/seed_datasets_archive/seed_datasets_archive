[<text>
<title>wickes companies &lt;wix&gt; completes sale of unit</title>
<dateline>    santa monica, calif., march 18 - </dateline>wickes companies inc said
it completed the sale of its sequoia supply division to a new
company created by the management of that division.
    paul hylbert, president of sequoia, has been named
president and chief executive officer of the new company.
 reuter
</text>]