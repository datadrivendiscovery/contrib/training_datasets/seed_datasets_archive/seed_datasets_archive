[<text>
<title>hawkeye entertainment &lt;sbiz&gt; completes offering</title>
<dateline>    los angeles, march 26 - </dateline>hawkeye entertainment inc said it
completed its initial public offering at the maximum
subscription level of 14 mln shares at 25 cts per share, for
proceeds of 3.5 mln dlrs.
    the company said the proceeds will be used to create,
acquire, develop, produce and package entertainment properties,
primarily in film and television and secondarily in music.
 reuter
</text>]