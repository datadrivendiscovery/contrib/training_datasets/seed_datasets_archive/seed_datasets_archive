[<text>
<title>financial benefit group inc &lt;fbgi&gt; year loss</title>
<dateline>    boca raton, fla., march 18 -
    </dateline>shr loss 11 cts vs loss 48 cts
    net loss 254,000 vs loss 784,000
    note: includes realized gains of one mln dlrs versus
840,000 dlrs.
 reuter
</text>]