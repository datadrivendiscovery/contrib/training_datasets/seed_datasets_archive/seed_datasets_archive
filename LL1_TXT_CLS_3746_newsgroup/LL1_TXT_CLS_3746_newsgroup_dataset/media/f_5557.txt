[<text>
<title>markel subsidiary sets initial share issue</title>
<dateline>    toronto, march 16 - </dateline>&lt;markel financial holdings ltd&gt; said
subsidiary morden and helwig group inc filed a preliminary
prospectus for a proposed initial share offering in canada by
way of treasury issue and secondary offering of subordinate
voting shares.
    markel did not elaborate on financial terms of the issue.
    morden, an independent insurance services company, expects
to use proceeds primarily for business acquisition and
expansion in the insurance services industry in canada and
possibly overseas. underwriters are wood gundy inc and dean
witter reynolds (canada) inc.
 reuter
</text>]