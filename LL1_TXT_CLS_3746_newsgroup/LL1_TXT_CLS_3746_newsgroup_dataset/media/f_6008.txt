[<text>
<title>international lease &lt;ilfc&gt; sells two jets</title>
<dateline>    beverly hills, calif., march 17 - </dateline>international lease
finance corp said it sold two boeing 737-200 aircraft and
leased one 737-200 in two transactions valued at 28 mln dlrs.
    it said the sales will result in a pre-tax gain for its
second fiscal quarter ending may 31.
    the two jets were sold for 18 mln dlrs to an investor group
and the third was leased for five years to pacific western
airlines ltd of calgary, alberta, international lease said.
 reuter
</text>]