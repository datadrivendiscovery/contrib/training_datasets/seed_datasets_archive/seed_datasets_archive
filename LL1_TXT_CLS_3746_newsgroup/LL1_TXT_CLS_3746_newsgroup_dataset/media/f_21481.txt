[<text>
<title>genentech &lt;gene.o&gt; in imaging drug venture</title>
<dateline>    seattle, oct 19 - </dateline>privately-held neorx corp said it has
entered into an exclusive agreement with genentech inc for the
development of an injectable diagnostic to image blood clots.
    it said it genentech will be responsible for marketing, and
neorx will receive royalties based on sales.  the product will
use a proprietary neorx method of attaching the radioisotope
technetium-99m to genentech's blood clot dissolving drug tissue
plasminogen activator, or activase.
 reuter
</text>]