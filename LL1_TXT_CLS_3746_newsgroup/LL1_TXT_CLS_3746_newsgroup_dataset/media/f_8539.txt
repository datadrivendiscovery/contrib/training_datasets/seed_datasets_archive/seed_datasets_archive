[<text>
<title>federal paper board &lt;fbp&gt; upgraded by moody's</title>
<dateline>    new york, march 23 - </dateline>moody's investors service inc said it
upgraded federal paper board co inc's 290 mln dlrs of debt.
    raised were the company's senior debt to ba-1 from ba-2 and
subordinated debt to ba-3 from b-1. moody's assigned a ba-3
rating to its convertible preferred stock.
    moody's cited reduced leverage, a strengthened equity base
and increased fixed-charge coverage. federal paper board's
outstanding 125 mln dlrs of subordinated debt will be redeemed
with the proceeds from the convertible preferred issue, the
agency noted. moody's also said it expects the company to have
strong earnings this year.
 reuter
</text>]