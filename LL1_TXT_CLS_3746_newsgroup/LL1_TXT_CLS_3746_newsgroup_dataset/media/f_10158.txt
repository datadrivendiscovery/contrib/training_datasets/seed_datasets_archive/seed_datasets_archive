[<text>
<title>americanture &lt;aaix&gt; buys american adventure</title>
<dateline>    los angeles, march 26 - </dateline>americanture inc said it has
purchased american adventure inc &lt;goaqc&gt; for cash, the
assumption of liabilities and the issuance of american
adventure inc common and preferred stock to creditors,
shareholders and members.
    the acquisition was pursuant of a chapter 11 reorganization
plan of american adventure.
    the company said the transaction involved assets valued at
more than 83 mln dlrs.
 reuter
</text>]