[<text>
<title>credo petroleum corp &lt;cred&gt; 1st qtr jan 31 net</title>
<dateline>    denver, march 18 -
    </dateline>shr profit one ct vs loss 27 cts
    net profit 22,000 vs loss 763,000
    revs 161,000 vs 316,000
    note: prior year net includes 1,209,000 dlr writedown of
oil properites and 314,000 dlr tax credit.
 reuter
</text>]