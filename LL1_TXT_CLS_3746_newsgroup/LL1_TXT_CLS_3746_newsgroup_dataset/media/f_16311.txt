[<text>
<title>&lt;national westminster bank plc&gt; 1st qtr net</title>
<dateline>    new york, april 13 -
    </dateline>net 17.7 mln vs 15.3 mln
    note: &lt;national westminster bank plc&gt; subsidiary.
    loan loss provision 13.8 mln vs 13.0 mln
    investment securitiesd gaons 2,003,000 dlrs vs 169,000
dlrs.
    figures in dollars.
 reuter
</text>]