[<text>
<title>lindberg co &lt;lind.o&gt; 3rd qtr net</title>
<dateline>    chicago, oct 19 -
    </dateline>shr profit 10 cts vs profit eight cts
    net profit 477,853 vs profit 348,384
    sales 17.7 mln vs 17.3 mln
    nine mths
    shr loss 35 cts vs profit 45 cts
    net loss 1,639,216 vs profit 2,305,700
    sales 56.2 mln vs 57.1 mln
    avg shrs 4,698,501 vs 5,075,717
    note: earnings in the 2nd qtr of 1987 were reduced by
3,262,000 dlrs, or 69 cts a share from a charge reflecting
elimination or transfer of certain product lines and operations
at the company's racine, wis. foundry
 reuter
</text>]