[<text>
<title>charter-crellin inc &lt;crtr&gt; year net</title>
<dateline>    new york, march 17 -
    </dateline>shr 1.40 dlrs vs 1.38 dlrs
    net 1,928,800 vs 1,485,600
    sales 35.2 mln vs 33.5 mln
    avg shrs 1,350,800 vs one mln
 reuter
</text>]