[<text>
<title>colt industries &lt;cot&gt; to sell british unit</title>
<dateline>    new york, june 1 - </dateline>colt industries inc said it signed a
conditional agreement to sell its woodville polymer engineering
ltd subsidiary in great britain to the dowty group plc of
gloucestershire, england, for 35.9 mln stg.
    the deal is scheduled to close by the end of june, the
company said.
    woodville, which makes high technology precision products
for aerospace, automotive and other industries, had 1986 sales
of about 24 mln stg, it said.
 reuter
</text>]