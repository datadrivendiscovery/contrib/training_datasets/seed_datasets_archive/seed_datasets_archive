[<text>
<title>mentor corp (mntr.o) to repurchase shares</title>
<dateline>    santa barbara, calif., oct 20 - </dateline>mentor corp said its board
of directors has authorized a plan to repurchase up to two  mln
shares of its common stock, subject to market conditions and
the company's own financial position.
 reuter
</text>]