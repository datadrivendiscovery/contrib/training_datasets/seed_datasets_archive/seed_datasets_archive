[<text>
<title>struthers wells &lt;suw&gt; sees 1986 nov 30 loss</title>
<dateline>    warren, pa., march 6 - </dateline>struthers wells corp said it expects
to report a loss, without tax benefit, of about 16 mln dlrs for
the fiscal year ended november 30, 1986, versus a profit of
295,000 dlrs in fiscal 1985.
    the company added, however, that about 13.6 mln dlrs of the
loss relates to discontinued operations and disposal of
subsidiaries.
    the company said the loss is part of its previously
announced restructuring that includes the sale of its foreign
and domestic units.
    struthers added that it has filed with the securities and
exchange commission for an extension to file its annual report
on form 10-k as a result of delays caused by the restructuring.
 reuter
</text>]