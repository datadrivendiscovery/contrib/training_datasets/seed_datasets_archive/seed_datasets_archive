[<text>
<title>alliant and macneal-schwendler sign pact</title>
<dateline>    littleton, mass., march 11 - </dateline>alliant computer systems corp
&lt;alnt&gt; said that it has signed an agreement with
macneal-schwendler corp &lt;mns&gt; where macneal will develop a new
version of its msc/nastran software for alliant's fx/series
computers.
    alliant said msc/nastran is used for finite element
analysis, particularly in the automotive and aerospace
industries.
 reuter
</text>]