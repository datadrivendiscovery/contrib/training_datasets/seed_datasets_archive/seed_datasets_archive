[<text>
<title>ford &lt;f&gt; selects firm to automate plant</title>
<dateline>    new york, march 11 - </dateline>ford motor co selected &lt;scicon corp's&gt;
systems control unit to install a manufacturing automation
system at ford's hermosillo, mexico, plant, scicon said.
    the system, the value of which was not disclosed, will
provide ford with automated vehicle tracking capabilities,
scicon said.
 reuter
</text>]