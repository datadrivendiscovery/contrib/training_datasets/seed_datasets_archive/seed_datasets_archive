[<text>
<title>allegheny int'l &lt;ag&gt; sells three overseas units</title>
<dateline>    pittsburgh, pa., march 27 - </dateline>allegheny international inc
said it sold three overseas subsidiaries to reil corp ltd, a
north sydney, australia, investment group.
    terms were not disclosed.
    the units sold were sunbeam corp ltd australia, sunbeam new
zealand ltd and victa (u.k.) ltd. the units make and distribute
various products, including lawn mowers, small appliances and
sheep shearing equipment. they employ a total of about 1,750.
 reuter
</text>]