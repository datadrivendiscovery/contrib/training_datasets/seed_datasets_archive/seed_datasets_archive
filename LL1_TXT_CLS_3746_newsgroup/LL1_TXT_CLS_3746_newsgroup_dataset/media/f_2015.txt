[<text>
<title>tanzanian railways secure 25.6 mln dlrs aid</title>
<dateline>    dar es salaam, march 5 - </dateline>state-run tanzania railway
corporation (trc) has secured 25.6 mln dlrs aid from banks and
european countries for a one-year emergency repair program,
transport minister mustafa nyang'anyi said.
    nyang'anyi told reuters on his return from a world bank
sponsored donors' conference in new york that the aid would
enable trc to buy spares for 32 locomotives, overhaul 800
wagons and replace 67,000 sleepers over the next 12 months.
    the world bank, african development bank, european
community, canada, belgium, west germany, britain, sweden,
italy and denmark had contributed to the package, he said.
    trc runs a rail network linking dar es salaam and the
northern port of tanga with the coffee-growing area around
mount kilimanjaro and ports on lake victoria and lake
tanganyika.
    it is under separate administration from the
tanzania-zambia railway linking dar es salaam with the zambian
copperbelt and the railway system of southern africa, which has
already received substantial aid as part of international
efforts to ease the dependence of landlocked african states on
trade routes through south africa.
    but this is the first international aid package for trc,
which also carries cargo for uganda, zaire and burundi.
 reuter
</text>]