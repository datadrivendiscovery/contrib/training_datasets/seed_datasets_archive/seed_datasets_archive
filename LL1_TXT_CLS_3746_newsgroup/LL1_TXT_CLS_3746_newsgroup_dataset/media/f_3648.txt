[<text>
<title>dnc international issues 10 billion yen bond</title>
<dateline>    london, march 11 - </dateline>dnc international finance a.s. is
issuing a 10 billion yen eurobond due april 7, 1994 with a five
pct coupon and priced at 102-3/8 pct, yamaichi international
(europe) ltd said as joint book-runner with tokai international
ltd.
    the non-callable bonds, guaranteed by den norske
creditbank, will be issued in denominations of one mln yen and
will be listed in luxembourg. total fees of 1-7/8 pct comprise
5/8 pct for management and underwriting and 1-1/4 pct for
selling. pay date is april 7.
 reuter
</text>]