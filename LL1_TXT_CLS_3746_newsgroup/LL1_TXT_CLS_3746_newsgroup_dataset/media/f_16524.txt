[<text>
<title>medical imaging &lt;mika.o&gt; l.p. plans offer</title>
<dateline>    san diego, calif., april 13 - </dateline>medical imaging centers of
america inc said shared imaging partners l.p., a delaware
limited partnership, filed a registration statement with the
securities and exchange commission covering a planned offering
of partnership units.
    si management corp, a wholly-owned medical imaging
subsidiary, is the general partner of shared imaging.
    the company said the partnership intends to offer between
7.5 mln dlrs and 18.5 mln dlrs of limited partnership interests
at a price of 1,000 dlrs per unit.
 reuter
</text>]