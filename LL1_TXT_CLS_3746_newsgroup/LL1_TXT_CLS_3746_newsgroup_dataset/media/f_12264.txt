[<text>
<title>nyse says loral &lt;lor&gt; will not comment on stock</title>
<dateline>    new york, april 1 - </dateline>the new york stock exchange said  loral
corp told the exchange its policy is not to comment on unusual
market activity in its stock.
    the exchange said it contacted the company and requested
that loral issue a public statement indicating whether any
corporate developments would explain the unusual activity in
its stock.
    the stock was trading at 47-1/2, up 2-1/2 points.
 reuter
</text>]