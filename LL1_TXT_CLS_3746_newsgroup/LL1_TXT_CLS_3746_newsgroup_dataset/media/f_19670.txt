[<text>
<title>unicorp &lt;uac&gt; reports gain from property sale</title>
<dateline>    new york, june 29 - </dateline>unicorp american corp said it sold
three properties in the boston area for aggregate proceeds of
about 6,300,000 dlrs, resulting in a pre-tax gain of of about
3,200,000 dlrs.
    income tax expense totaling about 1,700,000 dlrs reduced
this gain to about 1,500,000 dlrs for financial reporting
purposes, the company said.
    the properties included an industrial building in westwood,
mass., a building containing retail stores in lexington, mass.,
and an office building in bedford, mass., the company added.
 reuter
</text>]