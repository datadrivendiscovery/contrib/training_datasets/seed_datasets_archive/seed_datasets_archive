[<text>
<title>fuji film &lt;fuji.t&gt; to set up west german unit</title>
<dateline>    tokyo, march 31 - </dateline>fuji photo film co ltd will set up a
wholly-owned subsidiary in kleve, west germany, to produce
video cassette tapes, a spokesman said.
    the company, &lt;fuji magnetics gmbh&gt;, will be capitalised at
25 mln marks and produce two mln tapes a month for the european
market from early 1988, he said.
    total investment will be about 100 mln marks and the plant
will create 200 jobs, including about 190 for local workers.
    fuji photo plans to make the kleve plant its european
production centre for magnetic products, and may make audio
cassette tapes and floppy discs later, the spokesman said.
 reuter
</text>]