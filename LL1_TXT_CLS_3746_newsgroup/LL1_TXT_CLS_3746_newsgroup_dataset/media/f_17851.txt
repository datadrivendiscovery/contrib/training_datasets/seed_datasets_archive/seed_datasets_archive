[<text>
<title>digital communications &lt;dcai&gt; introduces items</title>
<dateline>    alpharetta, ga., june 1 - </dateline>digital communications associates
inc said it introduced four new personal computer
communications products, including equipment designed for the
recently anounced international business machines corp &lt;ibm&gt;
personal system/2, and apple computer inc's &lt;appl.o&gt; 's
macintosh ii and macintosh se computers.
 reuter
</text>]