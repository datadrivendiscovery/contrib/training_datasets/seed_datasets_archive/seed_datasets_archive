[<text>
<title>bay state gas &lt;bgc&gt; preferred upgraded by s/p</title>
<dateline>    new york, march 5 - </dateline>standard and poor's corp said it raised
to a from a-minus bay state gas co's preferred stock.
    s and p affirmed the company's a-rated senior debt and a-1
commercial paper. bay state has 75 mln dlrs of long-term debt
and preferred outstanding.
    the rating agency said its action mainly reflected debt
redemptions. s and p also said the outlook for firm sales
growth is positive because of a vibrant regional economy.
 reuter
</text>]