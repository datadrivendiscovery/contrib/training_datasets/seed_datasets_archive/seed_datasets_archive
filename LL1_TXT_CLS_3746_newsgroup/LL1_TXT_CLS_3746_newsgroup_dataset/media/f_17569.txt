[<text>
<title>raven industries &lt;rav&gt; buys truck body business</title>
<dateline>    sioux falls, s.d., june 1 - </dateline>raven industries inc said it
purchased the utility truck body business of (astoria
fibra-steel, inc) for cash.
    details of the transaction were not disclosed.
    the astoria product line, which has annual sales of about
2.5 mln dlrs, will be manufactured and sold by raven's newly
formed subsidiary, astoria industries inc, raven said. its
glasstite inc subsidiary also manufactures and sells utility
truck bodies.

 reuter
</text>]