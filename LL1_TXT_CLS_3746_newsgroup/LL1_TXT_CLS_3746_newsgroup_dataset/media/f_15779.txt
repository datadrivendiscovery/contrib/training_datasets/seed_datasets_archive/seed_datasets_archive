[<text>
<title>digital communications &lt;dcai&gt; sells switches</title>
<dateline>    atlanta, april 9 - </dateline>digital communications associates inc
said bank south corp &lt;bkso&gt; purchased several of its recently
introduced system 9000 intelligent t-1 switching systems.
    dca said the switching systems will be used to transmit
voice and data between the bank's downtown banking location
here and its new operations center near atlanta's hartsfield
international airport.
 reuter
</text>]