[<text>
<title>cyacq extends tender for cyclops &lt;cyl&gt;</title>
<dateline>    dayton, ohio, march 6 - </dateline>cyacq corp said it extended its 80
dlr a share tender offer for cyclops corp to march 20 from
today.
    cyacq was formed by citicorp capital investors ltd and
audio video affiliates inc &lt;ava&gt; to acquire cyclops. the tender
offer began on february six.
    the offer is conditioned upon at least 80 pct of the
outstanding shares and at least 80 pct of the voting securities
being tendered before expiration of the offer.
    as of march six, only 353 shares of cyclops' 4.1 mln
outstanding shares had been tendered.
 reuter
</text>]