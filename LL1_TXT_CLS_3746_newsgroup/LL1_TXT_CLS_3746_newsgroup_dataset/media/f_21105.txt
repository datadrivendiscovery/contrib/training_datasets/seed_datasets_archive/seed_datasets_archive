[<text>
<title>progressive bank inc &lt;psbk.o&gt; 3rd qtr net</title>
<dateline>    pawling, n.y., oct 19 -
    </dateline>shr 88 cts vs 73 cts
    net 2,580,000 vs 2,147,000
    nine mths
    shr 2.48 dlrs vs 2.33 dlrs
    net 7,266,000 vs 5,948,000
 reuter
</text>]