[<text>
<title>quebec issues 30 billion yen eurobond</title>
<dateline>    london, april 8 - </dateline>the province of quebec is issuing a 30
billion yen eurobond due may 7, 1997 paying five pct and priced
at 102-1/2 pct, lead manager bank of tokyo international ltd
said.
    the non-callable bond is available in denominations of one
mln yen and will be listed in luxembourg. the selling
concession is 1-1/4 pct, while management and underwriting
combined pays 3/4 pct.
    the payment date is may 7.
 reuter
</text>]