[<text>
<title>salomon &lt;sb&gt; sees lower 2nd qtr</title>
<dateline>    new york, june 17 - </dateline>robert salomon, managing director of
salomon inc, said the company's second quarter is "likely to be
comfotrably in the black, but down from the comparable year-ago
quarter," when salomon earned 117 mln dlrs or 78 cts a share on
a primary basis.
    "its been a difficult quarter," salomon told reuters,
adding that financial analysts today lowered their second
quarter earnings estimate for the company to 30 cts to 50 cts
per share from a range of 75 cts to 80 cts a share.
    salomon's stock closed at 34-3/4, off one, on heavy
trading.
   
    salomon said he was deluged with calls from analysts after
yesterday's announcement by first boston inc &lt;fbc&gt; that it will
report a loss for the second quarter due to steep losses in the
bond market during april.
    salomon acknowledged that the quarter was "difficult" but
said the company will report a profit for the period.
 reuter
</text>]