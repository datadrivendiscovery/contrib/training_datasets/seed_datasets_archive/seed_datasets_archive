[<text>
<title>central sprinkler corp &lt;cnsp&gt; 1st qtr jan 31 net</title>
<dateline>    lansdale, pa., march 6 -
    </dateline>shr 19 cts vs 20 cts
    shr diluted 18 cts vs 18 cts
    net 578,000 vs 554,000
    sales 10.7 mln vs 10.4 mln
    avg shrs 3,006,s302 vs 2,795,820
    avg shrs diluted 4,271,488 vs 4,081,534
 reuter
</text>]