[<text>
<title>brand companies &lt;bran&gt; sees first quarter loss</title>
<dateline>    chicago, april 13 - </dateline>brand companies inc said it expects to
report a 1987 first quarter loss of 15 to 17 cts a share on
revenues of 20 to 22 mln dlrs.
    in the 1986 first quarter, brand reported earnings of 21
cts on revenues of 28.5 mln dlrs.
    no reason was given for the expected loss.
    final quarterly results will be reported toward the end of
the month or the beginning of may, a company spokesman said.
 reuter
</text>]