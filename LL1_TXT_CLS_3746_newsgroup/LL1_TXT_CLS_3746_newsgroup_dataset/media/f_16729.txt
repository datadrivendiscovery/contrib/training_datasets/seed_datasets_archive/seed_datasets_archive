[<text>
<title>paradyne &lt;pdn&gt; in pact with univar &lt;uvx&gt; unit</title>
<dateline>    largo, fla, april 13 - </dateline>paradyne corp said it signed a
contract with van waters and rogers inc, a subsidiary of univar
corp, to supply a data communications network.
    the contract, valued at 1.5 mln dlrs, will include paradyne
modems, multiplexers, analysis network management system and
netcare services, paradyne said.
 reuter
</text>]