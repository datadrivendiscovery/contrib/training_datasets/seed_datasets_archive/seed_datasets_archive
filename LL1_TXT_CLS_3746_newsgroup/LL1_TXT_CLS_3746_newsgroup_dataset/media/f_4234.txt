[<text>
<title>perfectdata &lt;perf&gt; in joint licensing pact</title>
<dateline>    chatsworth, calif., march 12 -  </dateline>perfectdata corp said it
agreed to license certain disk cleaning related patents to the
&lt;texwipe co&gt;.
 reuter
</text>]