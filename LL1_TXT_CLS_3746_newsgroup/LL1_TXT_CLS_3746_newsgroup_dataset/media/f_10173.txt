[<text>
<title>courier &lt;crrc&gt; sees second quarter loss</title>
<dateline>    lowell, mass., march 26 - </dateline>courier corp said it expects to
report a small loss for the second fiscal quarter against a
profit of 828,000 dlrs a year ago.
    the company attributed the loss to competitive pressures
which have cut gross margins. in addition, it said it is
incurring significant expenses from management programs aimed
at reducing costs and boosting productivity.
    it said its murray printing co unit has undertaken a
program of extended work hours, and salary and job cuts which
will save more than 1.5 mln dlrs annually.
 reuter
</text>]