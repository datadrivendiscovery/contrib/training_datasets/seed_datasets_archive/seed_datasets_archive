[<text>
<title>firm to offer television programming for ships</title>
<dateline>    new york, april 9 - </dateline>shipboard satellite network inc &lt;ssn&gt;
said it will offer television service delivered by satellite to
ships at sea beginning june 1.
    the company, whose initial public offering began yesterday,
said its programming will include a mix of general and
financial news, sports and talk shows. in addition, the company
said it plans to offer first-run movies, sports and
entertainment specials.
    the company said its programming will be transmitted
several times a day to passenger cabins and lounges.
 reuter
</text>]