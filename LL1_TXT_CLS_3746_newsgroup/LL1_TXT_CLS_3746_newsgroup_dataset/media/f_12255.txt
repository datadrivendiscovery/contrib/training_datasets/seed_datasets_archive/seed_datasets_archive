[<text>
<title>le peep restaurants inc &lt;lpep&gt; 4th qtr loss</title>
<dateline>    denver, april 1 - 
    </dateline>shr loss 27 cts vs loss 81 cts
    net loss 998,764 vs loss 1,491,590
    revs 2,712,614 vs 1,237,850
    avg shrs 3,727,063 vs 1,838,294
    year
    shr loss 1.79 dlr vs loss 2.11 dlrs
    net loss 4,559,004 vs loss 3,882,235
    revs 8,510,004 vs 3,720,640
    avg shrs 2,544,271 vs 1,838,294
 reuter
</text>]