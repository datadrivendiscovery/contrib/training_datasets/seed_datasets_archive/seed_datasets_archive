[<text>
<title>pier one accelerates warrant &lt;pirws&gt; expiration</title>
<dateline>    fort worth, texas, april 2 - </dateline>pier one imports inc &lt;pir&gt;
said its board has accelerated the expiration date of the
company's warrants to may 11, 1987, from july 15, 1988.
    to the extent any warrants remain outstanding after may 11,
they will automatically convert into common shares at the rate
of one share for each 100 warrants.
    each warrant entitles the holder to purchase 3.46 common
shares and 0.17 of its 25 cts preferred for 22 dlrs, the
company noted.
 reuter
</text>]