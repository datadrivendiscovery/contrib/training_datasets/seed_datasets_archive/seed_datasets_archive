[<text>
<title>j.w. mays inc &lt;mays&gt; completes payments</title>
<dateline>    brooklyn, n.y., march 9 - </dateline>j.w. mays inc said it made the
final payment of 84,519 dlrs due its unsecured creditors under
its reorganization plan.
    the company said it has been out of bankrputcy since
february 14, 1984 and has now made its final payment to
creditors.
    mays said it had until march 1, 1988 to pay certain
interest due to unsecured creditors but has elected to prepay
that interest now.
 reuter
</text>]