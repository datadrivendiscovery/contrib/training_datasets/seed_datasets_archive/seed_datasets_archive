[<text>
<title>&lt;amplicon inc&gt; files for initial offering</title>
<dateline>    santa ana, calif., march 23 - </dateline>amplicon inc said it has
filed for an initial public offering of 1,650,000 common shares
through underwriters led by &lt;kidder, peabody and co inc&gt; and
e.f. hutton group inc &lt;efh&gt; at an expected price of 12 to 14
dlrs per share.
    the company well sell 1,400,000 shares and shareholders the
rest, with company proceeds used to finance growth in its
financing and leasing portfolio and for working capital. 
amplicon leases and sells business computers and peripherals.
 reuter
</text>]