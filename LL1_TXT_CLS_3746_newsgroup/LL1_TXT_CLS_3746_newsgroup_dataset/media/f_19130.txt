[<text>
<title>central fund canada &lt;cef&gt; plans u.s. issue</title>
<dateline>    ancaster, ontario, june 19 - </dateline>central fund of canada ltd
said it filed a registration statement with the u.s. securities
and exchange commission for a u.s. offering of 3,250,000 units,
each consisting of two class a shares and one warrant.
    central fund, a specialized investment holding company,
said net proceeds will be used mainly to buy gold and silver
bullion.
    the issue will be underwritten by drexel burnham lambert
inc and wood gundy inc.
 reuter
</text>]