[<text>
<title>occidental &lt;oxy&gt; unit in marketing plan</title>
<dateline>    houston, march 13 - </dateline>occidental petroleum corp's natural gas
pipeline co of america unit said it has introduced a "fast
break" plan for expedited processing of requests for
interruptible natural gas transportation service.
    it said under fast break, customers submitting streamlined
service requests will receive interim transportation agreements
and, in most cases, have gas flowing within seven working days.
 reuter
</text>]