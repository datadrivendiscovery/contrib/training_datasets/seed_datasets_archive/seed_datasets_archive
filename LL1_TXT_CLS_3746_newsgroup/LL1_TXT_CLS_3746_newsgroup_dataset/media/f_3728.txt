[<text>
<title>amex trading chemical waste &lt;chw&gt; options</title>
<dateline>    new york, march 11 - </dateline>the &lt;american stock exchange&gt; said it
has started put and call option trading in chemical waste
management inc stock as a replacement for options on
chesebrough-ponds's inc, which was recently acquired.
    initial expiration months are june and september and
initial exercise prices 30 and 35, it said.  position and
exercise limits are 5,500 contracts on the same side of the
market.
 reuter
</text>]