[<text>
<title>owens-corning &lt;ocf&gt; completes sale of plants</title>
<dateline>    toledo, ohio, march 17 - </dateline>owens-corning fiberglas corp said
it has completed the previously-announced sale of its three
foam products plants to atlas roofing corp for undisclosed
terms.
    the sale is part of its restructuring, the company said.
 reuter
</text>]