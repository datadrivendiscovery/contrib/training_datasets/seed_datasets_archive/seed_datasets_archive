[<text>
<title>coca-cola bottling &lt;coke&gt; terminates talks</title>
<dateline>    charlotte, n.c., april 7 - </dateline>coca-cola bottling co
consolidated said it terminated negotiations with the proposed
purchaser of its wholly owned subsidiary headquartered in
vancouver, b.c.
    the company said it is vigorously continuing its efforts to
sell its canadian operations, substantially on the terms and
conditions previously announced.
 reuter
</text>]