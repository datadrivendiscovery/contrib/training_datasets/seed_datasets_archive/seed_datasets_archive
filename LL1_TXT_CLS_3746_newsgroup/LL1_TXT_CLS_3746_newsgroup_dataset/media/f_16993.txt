[<text>
<title>fed expected to add reserves in money market</title>
<dateline>    new york, april 21 - </dateline>the federal reserve is expected to
enter the u.s. government securities market to add temporary
reserves, economists said.
    they said it is likely to supply the reserves indirectly by
arranging two to 2.5 billion dlrs of customer repurchase
agreements. there is less chance of a direct reserve add
instead via system repurchase agreements.
    federal funds, which averaged 6.21 pct yesterday, opened at
6-3/8 pct and traded between there and 6-7/16 pct.
 reuter
</text>]