[<text>
<title>stone and webster inc &lt;sw&gt; sets quarterly</title>
<dateline>    new york, march 18 -
    </dateline>qtly div 40 cts vs 40 cts prior
    pay may 15
    record april one
   
 reuter
</text>]