[<text>
<title>allied stores names ann taylor division officer</title>
<dateline>    new york, june 19 - </dateline>&lt;allied stores corp&gt; named michele
fortune president and division chief executive officer of its
ann taylor division stores.
    allied said fortune is currently senior vice
president-merchandising of &lt;lord and taylor&gt; of new york.
   
 reuter
</text>]