[<text>
<title>talman home federal preferred offering starts</title>
<dateline>    new york, march 6 - </dateline>lead underwriters morgan stanley group
inc &lt;ms&gt;, &lt;goldman, sachs and co&gt; and &lt;salomon inc&gt; said an
offering of 1,000 shares of market auction preferred stock of
&lt;talman home federal savings and loan association of illinois'&gt;
talman finance corp c unit is underway at 100,000 dlrs a share.
    it said the initial dividend rate is 4.35 pct and the
diovidend will be reset every 49 days in a dutch auction
process.  the first dutch action date is may 12.
 reuter
</text>]