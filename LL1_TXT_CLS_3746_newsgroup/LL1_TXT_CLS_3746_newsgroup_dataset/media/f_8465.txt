[<text>
<title>dominion &lt;d&gt; and csx &lt;csx&gt; in joint venture</title>
<dateline>    richmond, va., march 23 - </dateline>dominion resources inc said it
will establish a joint venture with csx corp's csx
transportation inc to develop electric power cogeneration
projects.
    the new venture, to be called energy dominion, is being set
up to consider, evaluate and possibly carry out one or more
cogeneration projects outside dominion's subsidiary, virginia
power's, service area, dominion said.
    energy dominion will consider specific projects on a case
by case basis, concentrating on coal- and gas-fired
cogeneration opportunities in the northeastern u.s., it said.
 reuter
</text>]