[<text>
<title>lowrance electronics &lt;leix&gt; sees orders off</title>
<dateline>    tulsa, okla., march 16 - </dateline>lowrance electronics inc said
results from operations in the third and fourth quarter may not
be comparable to the first and second quarters, which were
strong because of orders for new sonar equipment.
    for the six months ended jan 31, the company reported net
income almost tripled to 951,000 dlrs or 34 cts a share as
sales rose 38 pct to 20.6 mln dlrs.
    the company, which went public dec 23, also said it expects
to be able to fill back orders from the first two quarters
because of improved supply of computer chip components.
  
 reuter
</text>]