[<text>
<title>bicc plc &lt;bicc.l&gt; 1986 year</title>
<dateline>    london, march 25 -
    </dateline>shr 22.7p vs 20.3p.
    final div 8.25p, making 11.75p vs 11p.
    pre-tax profit 101 mln stg vs 92 mln.
    attributable profit 45 mln vs 39 mln.
    turnover 2.14 billion stg vs 2.11 billion.
 more
</text>]