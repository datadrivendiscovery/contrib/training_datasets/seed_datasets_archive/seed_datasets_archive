[<text type="unproc">
federal reserve weekly report 4 - feb 26
    one week ended feb 25       daily avgs-mlns
 foreign deposits.............219 down.......29
 gold stock................11,059 unch.........
 custody holdings.........168,348 down......366
 federal funds rate avg......5.95 vs.......6.21
    factors on wednesday, feb 25
 bank borrowings............1,239 vs........446
 including extended credits....92 vs........298
 matched sales..............8,250 vs......2,998
 including sales w/cust.....4,392 vs......2,998
 float........................935 vs......2,125
                                               
 reuter


</text>]