[<text>
<title>moore financial group &lt;mfgi.o&gt; quarterly div</title>
<dateline>    boise, idaho, june 19 - 
    </dateline>qtly div 30 cts vs 30 cts prior
    pay july 16
    record july 6
 reuter
</text>]