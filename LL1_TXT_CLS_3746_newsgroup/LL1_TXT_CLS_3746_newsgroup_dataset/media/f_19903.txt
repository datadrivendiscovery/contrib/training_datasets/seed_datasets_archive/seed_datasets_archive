[<text>
<title>henley &lt;heng.o&gt; has venezuelan refinery project</title>
<dateline>    houston, june 29 - </dateline>henley group inc's m.w. kellogg co
subsidiary said it in consortium with &lt;inelectra&gt; received a
contract from corpoven s.a., a venezuelan-owned domestic oil
company, to revamp and expand its el palito refinery.
    kellogg said the installed cost of the work to be performed
is estimated to be 130 mln dlrs. inelectra, kellogg said, is a
major venezuelan engineering firm.
   kellog said the project will enable the refinery to produce
btx products -- benzene, toluene, and orthoxylene -- by
processing naphtha feed from an expanded reformer-hydrotreater.
    kellogg said the refinery's reformer-hydrotreater will be
upgraded to 9,500 barrels a day capacity from 7,500.
    it said the new btx process units include aromatic
extraction, xylene fractionation, xylene isomerization and
thermal hydrodealkylation.
    kellogg pointed out that venezuela now imports all of its
btx aromatics.
 reuter
</text>]