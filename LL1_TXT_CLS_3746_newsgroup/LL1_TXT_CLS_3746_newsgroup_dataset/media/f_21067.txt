[<text>
<title>southland &lt;slc&gt; unit raises crude oil prices</title>
<dateline>    new york, oct 19 - </dateline>citgo petroleum corp, a subsidiary of
southland corp, said it raised the contract price it will pay
for all grades of crude oil by 50 cts a barrel, effective oct
16
    the increase brings citgo's postings for the west texas
intermediate and west texas sour grades to 19.00 dlrs/barrel,
while light louisiana sweet is now priced at 19.35 dlrs.
    citgo last changed it crude oil postings on sept 9.
 reuter
</text>]