[<text>
<title>sterivet laboratories ltd &lt;stvtf&gt; year loss</title>
<dateline>    toronto, april 8 -
    </dateline>shr loss 48 cts vs loss 19 cts
    net loss 746,000 vs loss 342,000
    revs 3,213,000 vs 2,925,000
 reuter
</text>]