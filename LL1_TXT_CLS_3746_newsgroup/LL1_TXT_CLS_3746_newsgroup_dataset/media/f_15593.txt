[<text>
<title>texas american &lt;tae&gt; omits preferred payout</title>
<dateline>    midland, texas, april 9 - </dateline>texas american energy corp said
its board has decided to again omit the quarterly dividend on
its 2.575 dlr cumulative convertible exchangeable preferred
stock.
    the dividend would have been payable may one.  the company
last paid 64.3 cts a share on the issue in august 1986.
 reuter
</text>]