[<text>
<title>the raymond corp &lt;raym&gt; declares qtly div</title>
<dateline>    greene, n.y., march 11 -
    </dateline>qtly div 11-3/4 cts vs 11-3/4 cts prior
    pay march 27
    record march 13
 reuter
</text>]