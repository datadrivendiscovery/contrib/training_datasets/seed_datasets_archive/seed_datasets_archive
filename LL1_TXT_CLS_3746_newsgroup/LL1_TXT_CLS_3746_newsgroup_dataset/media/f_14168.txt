[<text>
<title>milton roy co &lt;mrc&gt; sets quarterly</title>
<dateline>    st. petersburg, fla., april 7 -
    </dateline>qtly div 11 cts vs 11 cts prior
    pay june 15
    record may 15
 reuter
</text>]