[<text>
<title>primark corp &lt;pmk&gt; regular dividend</title>
<dateline>    mclean, va., march 23 -
    </dateline>qtly div 32.5 cts vs 32.5 cts in prior qtr
    payable mary 15
    record april 15
 reuter
</text>]