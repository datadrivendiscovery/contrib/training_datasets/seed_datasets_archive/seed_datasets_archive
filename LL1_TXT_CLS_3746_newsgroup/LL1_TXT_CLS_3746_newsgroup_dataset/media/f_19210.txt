[<text>
<title>amex starts trading sherwood group &lt;shd&gt;</title>
<dateline>    new york, june 19 - </dateline>the american stock exchange said it has
started trading the common stock of sherwood group inc, which
went public today.
 reuter
</text>]