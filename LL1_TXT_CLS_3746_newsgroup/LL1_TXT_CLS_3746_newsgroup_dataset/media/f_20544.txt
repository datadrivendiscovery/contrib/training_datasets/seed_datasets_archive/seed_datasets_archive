[<text>
<title>us west &lt;usw&gt; repurchasing shares</title>
<dateline>    denver, colo., oct 20 - </dateline>us west inc said it is aggressively
repurchasing its shares.
    in 1984, directors authorized the repurchase of up to 10
mln shares over any three year period.
 reuter
</text>]