[<text>
<title>mcdonnell douglas &lt;md&gt; awards contracts</title>
<dateline>    long beach, calif., march 27 - </dateline>mcdonnell douglas corp said
it awarded two contract totaling 284 mln dlrs for landing gear
for its new md-11 jetliner.
    the company said ic industries inc's &lt;icx&gt; pneumo abex corp
unit was given a 214 mln dlr contract to make main landing
gear, while &lt;ap precision hydraulics&gt; was awarded a 70 mln dlr
contract to build centerline and nose gear.
    the company said pneumo's cleveland pneumatic unit will
build 300 shipsets.
    ap hydraulics' contract calls for 200 shipsets and options
for an additional 100, the company said.
 reuter
</text>]