[<text>
<title>corrected - federal paper &lt;fbt&gt; raises payout</title>
<dateline>    montvale, n.j., march 18 -
    </dateline>qtly div 17-1/2 cts vs 17-1/4 cts
    pay april 15
    record march 31
    note: full name federal paper board co.
    (corrects headline and dividend figure in item appearing
march 17 to show dividend was raised.)

 reuter
</text>]