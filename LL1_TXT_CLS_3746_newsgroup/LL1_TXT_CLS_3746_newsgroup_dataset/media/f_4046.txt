[<text>
<title>moscow carries out nuclear test</title>
<dateline>    london, march 12 - </dateline>the soviet union carried out a nuclear
test early today, the official tass news agency reported.
    according to the report, monitored by the british
broadcasting corporation, the explosion was at 0200 gmt.
    a blast on february 26 ended a 19-month unilateral test
moratorium declared by the soviet union. moscow blamed the end
of the freeze on u.s. refusal to join a total test ban.
    tass said the latest explosion, with a power of up to 20
kilotonnes, had "the aim of improving military equipment."
 reuter
</text>]