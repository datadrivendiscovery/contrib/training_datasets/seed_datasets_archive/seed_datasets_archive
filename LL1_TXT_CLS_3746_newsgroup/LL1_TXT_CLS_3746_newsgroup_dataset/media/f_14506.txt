[<text>
<title>berry petroleum, norris oil &lt;noil&gt; to merge</title>
<dateline>    taft, calif., apri l7 - </dateline>berry petroleum co said its board
and the norris oil co board approved a merger agreement that
calls for norris to become a wholly-owned berry subsidiary.
    berry, a privately-owned company which already owns 80.6
pct of norris oil's stock, said the agreement calls for norris'
public shareholders to receive 0.0333 berry common shares for
each norris share held.
 reuter
</text>]