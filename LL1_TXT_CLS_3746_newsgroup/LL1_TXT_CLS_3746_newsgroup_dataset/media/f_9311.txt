[<text>
<title>fabri-centers &lt;fca&gt; 4th qtr ends jan 31 net</title>
<dateline>    cleveland, march 25 -
    </dateline>shr 40 cts vs 41 cts
    net 1,979,000 vs 2,101,000
    revs 67.7 mln vs 63.6 mln
    12 mths
    shr 36 cts vs 20 cts
    net 1,798,000 vs 1,034,000
    revs 239.4 mln vs 233.2 mln
 reuter
</text>]