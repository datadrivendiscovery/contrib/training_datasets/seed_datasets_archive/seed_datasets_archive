[<text>
<title>u.k. tory party chairman tebbit to join blue arrow</title>
<dateline>    london, oct 20 - </dateline>conservative party chairman norman tebbit,
who plans to give up the post an a date yet to be set, is
joining blue arrow plc &lt;bawl.l&gt; as a non-executive director,
blue arrow said.
    blue arrow, which has just completed the purchase of u.s.
employment agency &lt;manpower inc&gt;, said tebbit is joining the
board along with michael davies as non-executive directors from
november 1.
    davies is a director of several publicly-listed companies,
including british airways plc &lt;bab.l&gt;, and ti group plc
&lt;tigl.l&gt;.
 reuter
</text>]