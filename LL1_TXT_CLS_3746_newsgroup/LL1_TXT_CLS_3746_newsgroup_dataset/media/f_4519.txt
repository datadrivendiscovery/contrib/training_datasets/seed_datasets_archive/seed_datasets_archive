[<text>
<title>new york business loans fall 718 mln dlrs</title>
<dateline>    new york, march 12 - </dateline>commercial and industrial loans on the
books of the 10 major new york banks, excluding acceptances,
fell 718 mln dlrs to 64.87 billion in the week ended march 4,
the federal reserve bank of new york said.
    including acceptances, loans fell 581 mln dlrs to 65.63
billion.
    commercial paper outstanding nationally dropped 13 mln dlrs
to 336.02 billion.
    national business loan data are scheduled to be released on
friday.
 reuter
</text>]