[<text>
<title>fluorocarbon corp &lt;fcbn&gt; qtly dividend</title>
<dateline>    laguna niguel, calif., march 18 -
    </dateline>shr seven cts vs seven cts prior qtr
    pay april 30
    record april 15

 reuter
</text>]