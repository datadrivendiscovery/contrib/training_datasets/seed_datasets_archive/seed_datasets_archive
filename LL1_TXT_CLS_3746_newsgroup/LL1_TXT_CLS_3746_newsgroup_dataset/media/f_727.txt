[<text>
<title>j.p. industries &lt;jpi&gt; forms two operating groups</title>
<dateline>    ann arbor, mich., march 2 - </dateline>j.p. industries inc said it has
formed two operating groups to serve major markets in its
transportation components business -- an engine products group
to serve original equipment manufacturers and an automotive
aftermarket group to serve repair market customers.
    the company said senior vice president gareth l. reed has
been appointed president and general manager of the engine
products group. gerald w. mcgrath was appointed vice president
and general manager of the automotive aftermarket group. he was
formerly vice president of sales with the engine parts division
of clevite industries inc, recently acquired by j.p. industries.
 reuter
</text>]