[<text>
<title>&lt;first montauk securities&gt; in merger deal</title>
<dateline>    eatontown, n.j., april 13 - </dateline>first montauk securities corp
said it has reached a preliminary merger agreement with
mcc-presidential inc.
    the company said mcc shareholders would have a 25 pct stake
in the combined company and would receive a cash distribution
of about 35 cts per share.
 reuter
</text>]