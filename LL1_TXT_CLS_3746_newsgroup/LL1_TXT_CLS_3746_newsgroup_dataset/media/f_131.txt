[<text>
<title>olin corp &lt;olm&gt; to elect new ceo in april</title>
<dateline>    stamford, conn., feb 26 - </dateline>olin corp said its board will
elect in april john johnstone jr as its chief executive
officer.
    the company said he will succeed john m henske, who is also
chairman. it said johnstone, 54, is now president and chief
operating officer.
    henske, 53, has served as ceo since 1978 and chairman since
1980. he will continue as chairman until his retirement in june
1988.
 reuter
</text>]