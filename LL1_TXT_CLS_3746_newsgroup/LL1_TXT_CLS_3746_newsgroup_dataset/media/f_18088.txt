[<text>
<title>new york life offering a global mutual fund</title>
<dateline>    new york, june 2 - </dateline>&lt;new york life insurance co&gt; said it is
offering its first global mutual fund through its wholly-owned
subsidiary, mackay-shields financial corp.
    the new fund is called the mackay-shields global fund, the
company said. its portfolio contains foreign and u.s.
securities, the company said.
   
 reuter
</text>]