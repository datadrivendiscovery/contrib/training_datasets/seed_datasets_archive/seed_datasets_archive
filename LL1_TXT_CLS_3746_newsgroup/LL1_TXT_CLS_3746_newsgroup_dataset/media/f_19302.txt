[<text>
<title>leaseway &lt;ltc&gt; debt downgraded by s/p</title>
<dateline>    new york, june 19 - </dateline>standard and poor's corp said it
downgraded to b-plus from a about 50 mln dlrs of collateral
trust notes of leaseway transportation corp.
    s and p cited a leveraged buyout offer from an investor
group that was organized by citibank and included management.
    the buyout of leaseway will result in significant
deterioration of its balance sheet, the agency pointed out.
    leaseway's strong competitive position in auto hauling and
retail support services will enable it to generate sufficient
cash flow to service commitments. but s and p expects debt
leverage to remain extremely high for the forseeable future.
 reuter
</text>]