[<text>
<title>ncnb corp &lt;ncb&gt; 1st qtr net</title>
<dateline>    charlotte, n.c., april 13 -
    </dateline>shr 68 cts vs 68 cts
    net 53.9 mln vs 53.2 mln
    assets 26.5 billion vs 22.2 billion
    loans 15.7 billion vs vs 13.3 billion
    deposits 13.8 billion vs vs 11.6 billion
 reuter
</text>]