[<text>
<title>dupont unit raises crude oil postings</title>
<dateline>    new york, june 18 - </dateline>conoco inc, a subsidiary of dupont
denemours &lt;dd&gt;, said it raised the contract price it will pay
for all grades of crude oil 50 cts a barrel, effective
yesterday.
    the increase brings conoco's posted price for west texas
intermediate to 19.00 dlrs a barrel. the west texas sour grade,
at 34 api, now stands at 18.10 dlrs a barrel. light louisiana
was also raised 50 cts to 19.35 dlrs barrel.
    conoco last changed its crude postings on may 21.
 reuter
</text>]