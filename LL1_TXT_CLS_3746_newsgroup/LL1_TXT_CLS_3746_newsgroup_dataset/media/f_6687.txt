[<text>
<title>valley resources &lt;vr&gt; sets split, raises payout</title>
<dateline>    cumberland, r.i., march 18 - </dateline>valley resources inc said its
board declared a three-for-two stock split and raised the
quarterly dividend to 42 cts per share presplit from 38 cts,
both payable april 15, record march 31.
 reuter
</text>]