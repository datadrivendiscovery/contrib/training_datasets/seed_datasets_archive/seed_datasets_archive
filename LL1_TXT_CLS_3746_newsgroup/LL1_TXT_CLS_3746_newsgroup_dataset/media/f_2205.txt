[<text>
<title>mercantile stores co inc &lt;mst&gt; 4th qtr net</title>
<dateline>    wilmington, dela., march 5 - </dateline>qtr ends jan 31
    shr 3.26 dlrs vs 3.17 dlrs
    net 47.9 mln vs 46.7 mln
    revs 673.1 mln vs 630.2 mln
    12 mths
    shr 7.54 dlrs vs 6.95 dlrs
    net 111.1 mln vs 102.4 mln
    revs 2.03 billion vs 1.88 billion
.
</text>]