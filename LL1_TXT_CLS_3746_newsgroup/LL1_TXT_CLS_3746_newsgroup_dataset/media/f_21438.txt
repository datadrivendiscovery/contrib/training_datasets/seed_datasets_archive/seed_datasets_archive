[<text>
<title>att &lt;t&gt; plans computer using sun &lt;sunw.o&gt; chip</title>
<dateline>    new york, oct 19 - </dateline>american telephone and telegraph co said
it plans to build a new computer that incorrates a unified
version of its unix system iv operating system and sun
microsystems inc's recently annouced sparc micropocessor.
    the sparc chip is based on reduced instruction-set
computing, or risc, technology. 
    att said the version of unix used by the new computer will
incorporate po;ular features of the berkeley 4.2 system, a
dirative of the unix system used widely in scientific and
engineering markets, as well as features of sunos, a variant of
the berkely system sold by sun.
    att said it would take 18 months to two years to develop
new computers based on the sun microchip and the merged version
of the unix operating system.
    vittorio cassoni, president of att's newly-formed data
systems group, said the company will not offer the new machines
until the merged version of unix is completed.
    "it's the software that will determine the availability of
products based on sparc," he said.
    eventually att's entire line of 3b mini computers will be
converted to the sparc architecture, cassoni said.
   
    the investment of all current users will be protected,
meaning that they will be able to maintain the software used on
the current 3b line, he said.
    cassoni also said att's sales of computers declined in the
first nine months of the year compared with the first nine
months of last year.
    the lower sales were primarily the result of att's
transition to a new line of computers, he said.
    however, "demand for the new products is way, way above
expectation," he stated.
 reuter
</text>]