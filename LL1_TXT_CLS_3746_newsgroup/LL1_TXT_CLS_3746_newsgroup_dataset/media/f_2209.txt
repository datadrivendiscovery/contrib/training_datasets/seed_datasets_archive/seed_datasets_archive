[<text>
<title>telematics in initial public offering</title>
<dateline>    fort lauderdale, fla., march 5 - </dateline>&lt;telematics international
inc&gt; said 2,550,000 shares of common at 12 dlrs per share are
being offered in an initial public offering.
    it said it is offering 2,050,000 and certain shareholders
are offering 500,000 shares.
    &lt;alex. brown and sons inc&gt; and robertson, colman and
stephens are the managers of the syndicate offering the larger
number of shares, and alex. brown, colman and hambros bank
limited are the managers of the international offering of
500,000 shares.
 reuter
</text>]