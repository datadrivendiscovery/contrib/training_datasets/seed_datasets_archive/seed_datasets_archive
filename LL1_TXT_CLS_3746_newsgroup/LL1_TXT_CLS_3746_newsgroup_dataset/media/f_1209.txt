[<text>
<title>corrected - lifetime&lt;lft&gt; to buy nippon lace sahres</title>
<dateline>    new york, march 3 - </dateline>lifetime corp said it agreed to buy
five mln shares or 16 pct of &lt;nippon lace co ltd&gt; for 3.28 dlrs
a share, or 16.5 mln dlrs.
    it said it plans to enter the health care business in
japan.
    in addition, it said &lt;koba electronics co ltd&gt;, an
affiliate of nippon, will buy four mln unissued shares, or a 12
pct stake, of lifetime for 20 mln dlrs or five dlrs a share. -
corrects to show nippon's affiliate koba buying stake in
lifetime.
   
 reuter
</text>]