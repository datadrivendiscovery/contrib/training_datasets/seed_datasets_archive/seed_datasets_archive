[<text>
<title>ericsson &lt;ericy&gt; gets u s west &lt;usw&gt; order</title>
<dateline>    richardson, texas, april 7 - </dateline>l.m. ericsson telephone co
said it has received a contract to provide u s west inc with
axe digital central office switching equipment for use in
idaho, replacing 50 older electro-mechanical switches.
    value was not disclosed.
    the company said u s west's mountain bell will place the
first axe in service before year-end and the project is to be
completed by the end of 1991.
    the company said the replacement program follows an
agreement between mountain bell and the idaho public utilities
commission for conversion of a substantial part of the idaho
telephone network to digital from analog technology over the
next five years.
 reuter
</text>]