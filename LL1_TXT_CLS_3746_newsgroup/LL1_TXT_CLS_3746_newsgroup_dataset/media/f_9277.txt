[<text>
<title>brierley offer for progressive still valid</title>
<dateline>    wellington, march 25 - </dateline>&lt;brierley investments ltd&gt;, (bil),
said its offer of 4.20 n.z. dlrs per share for supermarket
group &lt;progressive enterprises ltd&gt; still stands, although
&lt;rainbow corp ltd&gt; said today it has 52 pct of progressive.
    bil said in a statement it will review events on a daily
basis.
    rainbow announced earlier that it had increased its stake
in progressive to 52 pct from 44 pct through the purchase of
9.4 mln shares at between 3.80 and 4.80 n.z. dlrs per share.
    bil chief executive paul collins said: "all rainbow has done
is to outlay a substantial amount of cash to purchase shares
from parties who presumably were supportive of the merger."
    rainbow has proposed a merger with progressive to form a
new company, &lt;astral pacific corp ltd&gt;. under the merger,
shareholders in both progressive and rainbow will be issued
shares in the new company on a one-for-one basis.
    "quite simply, rainbow should now bid for the balance of
progressive enterprises at 4.80 n.z. dlrs per share," collins
said.
 reuter
</text>]