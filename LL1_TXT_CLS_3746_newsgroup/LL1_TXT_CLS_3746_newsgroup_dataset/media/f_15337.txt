[<text>
<title>uap makes acquisitions</title>
<dateline>    montreal, april 8 - </dateline>&lt;uap inc&gt; said it has acquired slater
auto electric ltd, with two ontario stores, and united diesel
engine parts ltd, of dartmouth, nova scotia, for undisclosed
terms. it said the transactions, together with acquisitions
earlier this year, will increase its annual sales by about 4.5
mln dlrs.
 reuter
</text>]