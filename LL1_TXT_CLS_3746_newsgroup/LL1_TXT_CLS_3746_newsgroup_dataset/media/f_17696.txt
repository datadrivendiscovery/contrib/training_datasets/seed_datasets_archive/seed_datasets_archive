[<text>
<title>ford to invest 23 mln stg in u.k. engine plant</title>
<dateline>    london, june 1 - </dateline>ford motor co &lt;f&gt; is to invest 23 mln stg
in high technology equipment at its bridgend engine plant in
south wales, the press association said.
    the move includes a new computerised engine test facility
which the company says will make the factory one of the most
efficient of its kind in the world.
    bridgend currently supplies escort, orion and some fiesta
engines to ford plants in britain, west germany and spain.
 reuter
</text>]