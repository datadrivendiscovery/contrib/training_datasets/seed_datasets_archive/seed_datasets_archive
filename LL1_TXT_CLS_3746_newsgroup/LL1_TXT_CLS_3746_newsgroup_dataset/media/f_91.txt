[<text>
<title>consolidated gas &lt;cng&gt;unit says no rules broken</title>
<dateline>    clarksburg, w.va., feb 26 - </dateline>consolidated natural gas
system's consolidated gas transmission corp said it is in
compliance with all federal regulations regarding the disposal
of polychlorinated biphenyls, or pcbs.
    the company said it successfully cleaned up the only
earthen pit at which pcbs were reported to be above
environmental protection agency standards.
   
 reuter
</text>]