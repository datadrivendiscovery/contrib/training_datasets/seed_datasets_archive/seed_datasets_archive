[<text>
<title>wurlitzer &lt;wur&gt; elects new top officers</title>
<dateline>    dekalb, ill., april 24 - </dateline>wurlitzer co said george howell
was elected vice chairman, succeeding sid weiss who was named
chief executive officer.
    weiss became vice chairman in december 1986. weiss and
leonard friedman, wurlitzer chairman, led wurlitzer investments
ltd, a texas partnership which purchased a controlling interest
in wurlitzer.
    in other action, frank rubury was elected president and
chief operating officer of wurlitzer, effective april 27.
rubury was formerly president of horsman inc, a subsidiary of
drew industries inc &lt;drwi.o&gt;.
 reuter
</text>]