[<text>
<title>national convenience &lt;ncs&gt; to have 3rd qtr loss</title>
<dateline>    houston, march 5 - </dateline>national convenience stores inc said it
expects to report a loss for the third quarter ending march 31
due to continued poor sales in texas, especially in houston.
    in last year's third quarter, national convenience earned
1,788,000 dlrs or eight cts per share, including a gain of
2,883,000 dlrs from the sale of 186 stores to another operator.
 it said the results also included earnings from gasoline
operations of 2,500,000 dlrs or 11 cts per share caused by
unusually high gross margins on gasoline sales of 12.7 cts per
gallon that were caused by rapidly falling oil prices.
    national convenience said its third quarter is usually weak
due to winter weather.
 reuter
</text>]