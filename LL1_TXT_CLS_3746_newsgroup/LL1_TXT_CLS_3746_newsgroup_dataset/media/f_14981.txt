[<text>
<title>cxr telcom corp &lt;cxrl&gt; 3rd qtr march 31 net</title>
<dateline>    mountain view, calif., april 8 -
    </dateline>shr nil vs nil
    net 215,000 vs 16,000
    revs 2,800,000 vs 1,100,000
    nine mths
    shr one ct vs nil
    net 620,000 vs 231,000
    revs 8,100,000 vs 2,100,000
 reuter
</text>]