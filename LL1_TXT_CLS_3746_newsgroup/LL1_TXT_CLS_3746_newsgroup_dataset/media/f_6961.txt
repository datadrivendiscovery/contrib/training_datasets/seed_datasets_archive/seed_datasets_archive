[<text>
<title>sanwa bank acquires small stake in portuguese bank</title>
<dateline>    tokyo, march 19 - </dateline>sanwa bank ltd &lt;anwa.t&gt; has agreed to buy
a two pct stake in oporto-based &lt;banco portugues de investmento
sarl&gt; (bpi), portugal's largest merchant bank, a sanwa official
said.
    sanwa will purchase the shares from international finance
corp, a bpi shareholder and sister organisation of the world
bank, for 351 mln yen, he said.
    the acquisition will be completed this month as both the
japanese and portuguse governments are expected to give
permission soon.  this is the first time a japanese bank has
bought a stake in a portuguese bank.
    sanwa plans to increase its stake in bpi to four pct, the
ceiling for foreign shareholders, the official said.
    the bank has also agreed with &lt;banco portugues do
atlantico&gt;, a state-owned merchant bank in oporto, to exchange
information on customers and help accelerate japanese
investment and technological transfers to portugal, he said.
 reuter
</text>]