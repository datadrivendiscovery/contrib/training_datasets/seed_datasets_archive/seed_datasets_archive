[<text>
<title>colorado venture capital &lt;covc&gt; asset value up</title>
<dateline>    boulder, colo., march 20 - </dateline>colorado venture capital corp
said its net asset value rose 215,239 dlrs or two cts per share
during the fourth quarter and 175,712 dlrs or two cts per share
during the full year 1986.
 reuter
</text>]