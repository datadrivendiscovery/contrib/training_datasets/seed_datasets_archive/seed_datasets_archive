[<text>
<title>bevis &lt;bevi&gt; receives takeover inquiries</title>
<dateline>    providence, r.i., april 9 - </dateline>bevis industries inc, which has
been seeking to be acquired, said it recently received
inquiries concerning the purchase of the company.
    the company did not identify the parties that made the
inquiries, but it said they had been referred to its investment
bankers, tucker, anthony and r.l. day inc, for study.
    on march 18, the company said it engaged tucker, anthony to
seek purchasers of its operating units, greenville tube corp
and md pneumatics inc.
 reuter
</text>]