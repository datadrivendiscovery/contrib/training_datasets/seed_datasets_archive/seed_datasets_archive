[<text>
<title>morrison knudsen &lt;mrn&gt;gets 200 mln dlr contract</title>
<dateline>    boise, idaho, march 20 - </dateline>morrison knudsen corp said a unit
was awarded an 11-1/2-year contract totaling about 200 mln dlrs
for the operation of a steam coal mine near montgomery, w.va.
    the company said the contract carries an option for an
additional 10 years.
    of the 200 mln dlrs, the company said that only the first
five years, or about 90 mln dlrs, were included in its first
quarter backlog.
    the contract was awarded by &lt;cannelton industries inc&gt;, the
company said.
 reuter
</text>]