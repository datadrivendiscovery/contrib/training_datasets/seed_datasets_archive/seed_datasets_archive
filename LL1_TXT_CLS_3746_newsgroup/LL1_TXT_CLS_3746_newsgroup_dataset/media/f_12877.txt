[<text>
<title>turkish central bank sets lira/dollar, dm rates</title>
<dateline>    ankara, april 3 - </dateline>turkey's central bank set a lira/dollar
rate for april 6 of 781.95/785.86 to the dollar, up from
782.50/786.41. it set a lira/d-mark rate of 430.15/432.30 to
the mark, down from 428.30/430.44.
 reuter
</text>]