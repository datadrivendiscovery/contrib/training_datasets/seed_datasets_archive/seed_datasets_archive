[<text>
<title>universal holding corp &lt;uhco&gt; 4th qtr loss</title>
<dateline>    jericho, n.y., march 26 - 
    </dateline>shr profit nil vs profit nine cts
    net profit 2,000 vs profit 195,000
    revs 2,623,000 vs 2,577,000
    year
    shr loss 21 cts vs profit 13 cts
    net loss 425,000 vs profit 278,000
    revs 15.4 mln vs 8,637,000
    note: net includes capital gains of 63,000 vs 211,000 for
qtr and 304,000 vs 292,000 for year. current year net includes
charge of 716,000 from contract obligation to former chairman.
 reuter
</text>]