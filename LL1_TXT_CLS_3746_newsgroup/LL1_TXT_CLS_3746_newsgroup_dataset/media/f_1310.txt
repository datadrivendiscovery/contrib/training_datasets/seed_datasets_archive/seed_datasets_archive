[<text>
<title>nynex &lt;nyn&gt; unit wins army contract</title>
<dateline>    white plains, n.y., march 3 - </dateline>nynex business information
systems co said it won a contract valued at nine mln dlrs from
the u.s. army corp of engineers to provide an advanced
communications system for a facility in upstate new york.
    the company, a unit of nynex corp, said the contract was
for initial work on the project at the new fort drum light
infantry post near watertown, about 65 miles north of syracuse.
 reuter
</text>]