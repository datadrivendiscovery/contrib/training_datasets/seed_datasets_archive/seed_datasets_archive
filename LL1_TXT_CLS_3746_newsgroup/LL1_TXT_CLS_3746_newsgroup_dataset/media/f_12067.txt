[<text>
<title>waxman industries inc &lt;waxm&gt; regular payout</title>
<dateline>    bedford heights, ohio, april 1 -
    </dateline>qtly div class a two cts vs two cts prior
    qtly div class b one ct vs one ct prior
    pay april 17
    record april 10
 reuter
</text>]