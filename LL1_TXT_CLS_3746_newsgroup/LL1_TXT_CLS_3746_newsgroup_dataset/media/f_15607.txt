[<text>
<title>exxon &lt;xon&gt; cuts heating oil price, traders said</title>
<dateline>    new york, april 9 -- </dateline>oil traders in the new york area said
exxon corp's exxon u.s.a. unit reduced the price it charges
contract barge customers for heating oil in new york harbor
0.50 cent a gallon, effective today.
    they said the reduction brings exxon's contract barge price
to 49.75. the price decrease follows sharp declines in heating
oil prices in the spot and futures markets, traders said.
 reuter
</text>]