[<text>
<title>massachusetts gov. dukakis to seek white house</title>
<dateline>    boston, march 16 - </dateline>massachusetts governor michael dukakis,
a newcomer to national politics, ended weeks of speculation
with the announcement he plans to seek the 1988 democratic
presidential nomination.
    dukakis said he had authorized the formation of a
presidential campaign committee and would make a formal
announcement of candidacy in boston on may 4.
    the 53-year-old dukakis is serving his third term as
governor. he was reelected with 67 per cent of the vote last
november.
 reuter
</text>]