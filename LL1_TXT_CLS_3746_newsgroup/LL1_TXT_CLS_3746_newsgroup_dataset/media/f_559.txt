[<text>
<title>americus trust &lt;hpu&gt; extends deadline</title>
<dateline>    new york, march 2 - </dateline>americus trust for american home
products shares said it extended its deadline for accepting
tendered shares until november 26, an extension of nine months.
    the trust, which will accept up to 7.5 mln shares of
american home products &lt;ahp&gt;, said it has already received
tenders for about four mln shares.
    the trust is managed by alex. brown and sons inc &lt;absb&gt; and
was formed november 26, 1986.
 reuter
</text>]