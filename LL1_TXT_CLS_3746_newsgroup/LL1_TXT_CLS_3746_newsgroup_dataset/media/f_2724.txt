[<text>
<title>food lion &lt;fdlnb&gt; february sales up 26 pct</title>
<dateline>    new york, march 6 - </dateline>food lion inc said its sales totaled
207.5 mln dlrs last month, up 26.0 pct from the 164.7 mln dlrs
reported for february 1986.
    the company said sales for the first eight weeks of the
year were up 26.1 pct to 411.1 mln dlrs from 326.0 mln dlrs.
the company operates 396 supermarkets in six southeastern
states.
 reuter
</text>]