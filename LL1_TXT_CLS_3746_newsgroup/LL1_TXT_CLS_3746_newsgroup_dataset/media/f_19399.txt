[<text>
<title>kleinwort benson australian fund &lt;kba&gt; dividend</title>
<dateline>    new york, june 19 - </dateline>kleinwort benson australian income fund
inc
    qtly div from investment income 30 cts vs 36.5 cts in prior
qtr
    payable july 17
    record july one
 reuter
</text>]