[<text>
<title>alexander &lt;aal&gt; establishes insurance unit</title>
<dateline>    new york, june 18 - </dateline>alexander and alexander services inc
said it formed an insurance unit known as the environmental
protection insurance co-risk retention group to specialize in
environmental liability coverage.
    the company said the unit has started preliminary
underwriting activities and will provide environmental
impairment coverage to companies with pollution liability
exposures.
 reuter
</text>]