[<text>
<title>reuters &lt;rtrsy&gt; in real estate market venture</title>
<dateline>    new york, march 2 - </dateline>reuters holdings plc said its reuters
information services inc unit will join &lt;real estate financing
partnership&gt;, philadelphia, to offer an electronic market
access system for commercial property financing.
    reuters said the system, named real estate select view
program, or rsvp, will use its private communications network
to provide a confidential method for the purchasing, selling
and financing of commercial property.
    the system, set for testing in august in selected u.s.
cities, is expected to be operational 90 days after initial
testing, reuters said.
 reuter
</text>]