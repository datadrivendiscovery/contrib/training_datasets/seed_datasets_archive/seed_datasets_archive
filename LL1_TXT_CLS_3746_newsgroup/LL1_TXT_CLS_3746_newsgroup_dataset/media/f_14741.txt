[<text>
<title>interface &lt;ifsia&gt; begins offering</title>
<dateline>    lagrange, ga, april 7 - </dateline>interface flooring systems inc said
it began a public offering of 2.8 mln shares of class a stock
at 21.25 dlrs a share.
    proceeds will be used to finance the acquisition of 50 pct
of debron investments plc and to reduce debt.
 reuter
</text>]