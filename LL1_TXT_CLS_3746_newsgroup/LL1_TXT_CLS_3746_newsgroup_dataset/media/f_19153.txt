[<text>
<title>southern national corp &lt;snat.o&gt; raising payout</title>
<dateline>    lumberton, n.c., june 19 - </dateline>southern national corp said its
board approved increasing the dividend rate to 78 cts a year
from 76 cts, effective with the next quarterly dividend which
will be declared in july, payable august one.
 reuter
</text>]