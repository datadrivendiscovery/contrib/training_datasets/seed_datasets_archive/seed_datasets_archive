[<text>
<title>hong kong defends currency link with u.s. dollar</title>
<dateline>    osaka, japan, april 29 - </dateline>hong kong has not taken unfair
advantage of its currency's link with the u.s. dollar, hong
kong monetary affairs secretary david nendick told the annual
meeting of the asian development bank here.
    he said: "we have taken the rough with the smooth, having to
accept a downward adjustment of our economy in 1985 following a
period when the u.s. dollar was clearly overvalued in world
terms, but benefiting as that currency subsequently declined."
he said that since the establishment of the link in 1983, hong
kong's trade had been broadly in balance. this year a modest
deficit was expected.
    "under the link, hong kong's entirely free and open economy
continues to adjust quickly to any external imbalances, but the
burden of adjustment now falls almost entirely on our domestic
interest rates, money supply and price levels," nendick said.
    "the relative volatility in these domestic variables is the
price we pay for the stability of our currency against the u.s.
dollar," he said.
 reuter
</text>]