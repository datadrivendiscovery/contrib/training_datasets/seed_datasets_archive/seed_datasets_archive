[<text>
<title>contel &lt;ctc&gt; to buy walker county telephone</title>
<dateline>    atlanta, march 26 - </dateline>contel corp said it has agreed in
principle to acquire &lt;walker county telephone co&gt; of lafayette,
ga., for an undisclosed amount of common stock.
    walker has 7,600 customers in northeast georgia.
    the company said the agreement is subject to approval by
regulatory agencies, both boards and walker shareholders.
 reuter
</text>]