[<text>
<title>rocky mount undergarment &lt;rmuc&gt; 4th qtr loss</title>
<dateline>    rocky mount, n.c., march 31 -
    </dateline>shr loss 53 cts vs loss 32 cts
    net loss 1,548,000 vs loss 929,000
    revs 9,362,000 vs 11.3 mln
    12 mths
    shr loss 82 cts vs profit 17 cts
    net loss 2,408,000 vs profit 452,000
    revs 40.9 mln vs 39.5 mln
    note: full name of company is rocky mount undergarment co
inc.
 reuter
</text>]