[<text>
<title>delta natural gas &lt;dgas&gt; sells debentures</title>
<dateline>    new york, march 18 - </dateline>delta natural gas co inc is raising 14
mln dlrs through an offering of debentures due 2007 with an
8-5/8 pct coupon and par pricing, said sole manager edward d.
jones and co.
    that is 110 basis points more than the yield of comparable
treasury securities. the issue is non-callable for five years.
 reuter
</text>]