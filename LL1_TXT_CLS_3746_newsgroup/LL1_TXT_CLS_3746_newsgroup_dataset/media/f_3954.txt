[<text>
<title>vista resources inc &lt;vist&gt; 4th qtr net</title>
<dateline>    new york, march 11 -
    </dateline>shr 1.36 dlrs vs one dlr
    net 1,010,249 vs 750,856
    revs 15.2 mln vs 11.9 mln
    12 mths
    shr 3.24 dlrs vs 2.18 dlrs
    net 2,407,186 vs 1,627,250
    revs 57 mln vs 53.1 mln
 reuter
</text>]