[<text>
<title>charming shoppes &lt;chrs&gt; plans new stores</title>
<dateline>    bensalem, pa., march 24 - </dateline>charming shoppes inc said it
expects to open over 155 new stores -- including its first
stores in kansas, louisiana and oklahoma -- and remodel or
expand another 80 during 1987.
    the company said it expects to open about 75 new stores by
the end of june, which would bring its total store count at
that time to 753.
 reuter
</text>]