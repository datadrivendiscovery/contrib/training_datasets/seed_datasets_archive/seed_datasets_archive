[<text>
<title>restaurant management services &lt;resm&gt; in payout</title>
<dateline>    macon, ga., march 18 -
    </dateline>qtly div 1-3/4 cts vs 1-3/4 cts prior
    pay april 23
    record april nine
    note: full name restaurant management services inc
 reuter
</text>]