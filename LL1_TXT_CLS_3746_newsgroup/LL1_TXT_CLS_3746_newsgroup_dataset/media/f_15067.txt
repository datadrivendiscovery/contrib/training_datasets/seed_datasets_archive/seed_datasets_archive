[<text>
<title>marble financial corp &lt;mrbl&gt; 1st qtr net</title>
<dateline>    rutland, vt., april 8 -
    </dateline>oper shr 26 cts vs not given
    oper net 866,000 vs 480,000
    note: 1987 net excludes 157,000 dlr gain from termination
of pension plan.
    company went public in august 1986.
 reuter
</text>]