[<text>
<title>raleigh federal savings bank &lt;rfbk.o&gt; 3rd qtr</title>
<dateline>    raleigh, n.c., oct 20 -
    </dateline>shr 38 cts
    net 1.3 mln vs 668,000
    nine mths
    shr 84 cts
    net 2,892,000 vs 2,200,000
    note: company converted to stock ownership in july
 reuter
</text>]