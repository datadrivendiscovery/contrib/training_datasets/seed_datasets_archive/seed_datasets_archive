[<text>
<title>imre &lt;imre&gt; sells stock to european institutions</title>
<dateline>    seattle, march 6 - </dateline>imre corp said it has received
commitments for a group of european institutions to buy about
400,000 imre shares for 2,500,000 dlrs, with closing expected
on march 16.
                                                   
 reuter
</text>]