[<text>
<title>pan am &lt;pn&gt; unit sets intro fare to atlanta</title>
<dateline>    new york, april 8 - </dateline>pan american corp's pan am world
airways said it will begin daily service tomorrow from atlanta
to new york, washington and miami.
    the company said it will offer the flights for 49 dlrs
between april 9 and may 9. the 49 dlr fare is unrestricted, but
is capacity controlled, pan am said.
    pan am said the flights are timed to connect with european-
and south american-bound flights.
 reuter
</text>]