[<text>
<title>providence energy corp &lt;pvy&gt; regular dividend</title>
<dateline>    providence, r.i., april 3 -
    </dateline>qtly div 45 cts vs 45 cts prior
    pay may 15
    record april 24
 reuter
</text>]