[<text>
<title>aaron spelling productions inc &lt;sp&gt; 2nd qtr net</title>
<dateline>    los angeles, march 16 - </dateline>jan 31 end
    shr 31 cts vs 44 cts
    net 5,705,000 vs 8,101,000
    revs 50.6 mln vs 67.2 mln
    1st half
    shr 63 cts vs 71 cts
    net 11.6 mln vs 13.2 mln
    revs 80.9 mln vs 105.2 mln
    note: current half net includes 750,000 dlr charge from
reorganization.
 reuter
</text>]