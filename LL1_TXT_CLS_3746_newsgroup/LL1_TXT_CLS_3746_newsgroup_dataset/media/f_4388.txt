[<text>
<title>coastal bancorp &lt;csbk&gt; sets quarterly</title>
<dateline>    portland, maine, march 12 -
    </dateline>qtly div five cts vs five cts prior
    pay april 15
    record march 30
 reuter
</text>]