[<text>
<title>commodore &lt;cbu&gt; renews major credit facilities.</title>
<dateline>    west chester, pa., oct 20 - </dateline>commodore international ltd
said it agreed with its major lending banks to renew the
company's credit agreement of about 79 mln dlrs.
    in fiscal 1987, ended june 30, commodore said it repaid
about 68 mln dlrs to this lending group.
    commodore said the renewal of the company's major bank
facilities, together with the 60 mln dlr long-term financing
completed this past may with &lt;prudential insurance co of
america&gt;, provide a sound financial basis for the company.
 reuter
</text>]