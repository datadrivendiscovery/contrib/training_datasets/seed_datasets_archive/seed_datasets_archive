[<text>
<title>american variety acquisition pact terminated</title>
<dateline>    los angeles, march 23 - </dateline>&lt;american variety international
inc&gt; said its agreement to acquire &lt;first national
entertainment corp&gt; has been terminated because first national
was not able to fulfill terms of the agreement.
    it said due to protracted negotiations with first national,
several american variety divisions were inoperative in 1986.
    american variety said it is reevaluating its record and
tape library for possible conversion to compact discs.
 reuter
</text>]