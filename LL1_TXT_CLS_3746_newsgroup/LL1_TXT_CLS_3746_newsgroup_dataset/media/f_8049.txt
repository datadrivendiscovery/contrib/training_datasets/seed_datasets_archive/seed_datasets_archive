[<text>
<title>philippine debt talks to continue on saturday</title>
<dateline>    new york, march 20 - </dateline>the philippines and its bank advisory
committee completed another round of debt rescheduling talks 
and will meet again on saturday, a senior banker said.
    although today's negotiations did not produce a final
agreement, the decision to meet at the weekend appears to be a
signal that the two sides are making progress.
    the philippines seeks to restructure 9.4 billion dlr of its
27.2 billion dlr foreign debt. the interest rate to be charged
on the debt and manila's proposal to pay interest partly with
investment notes instead of cash have been the main sticking
points in the talks.
 reuter
</text>]