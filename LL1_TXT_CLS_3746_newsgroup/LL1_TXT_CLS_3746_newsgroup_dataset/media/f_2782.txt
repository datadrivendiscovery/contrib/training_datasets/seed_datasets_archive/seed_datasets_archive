[<text>
<title>&lt;technigen platinum corp&gt; in metals find</title>
<dateline>    vancouver, march 6 - </dateline>technigen platinum corp said it
initial results of a 13-hole drilling program on its r.m. nicel
platinum property in rouyn-noranda, quebec, indicate
"extensive" near-surface zones "highly" enriched in gold,
platinum and palladium were found in rocks on the periphery of
a sulphide deposit.
    it said values of up to 0.073 ounce of platinum, 0.206
ounce palladium, three pct copper and 4.5 pct nickel were found
over a drill section of 13 feet.
                  
 more
</text>]