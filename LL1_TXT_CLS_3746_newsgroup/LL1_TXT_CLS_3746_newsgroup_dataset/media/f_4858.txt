[<text>
<title>eldon's &lt;eld&gt; healthways in final settlement</title>
<dateline>    iselin, n.j., march 12 - </dateline>healthways inc, a unit of eldon
industries inc, said it reached a final settlement of the
year-old litigation with central jersey individual pratice
association, the union county medical society and individual
physicians.
    on february 2 the company, a health maintenance
organization, said a tentative agreement had been signed under
which all approved outstanding claims to central jersey
participating physicians in middlesex and union counties for
services provided before june 30 would be paid.
 reuter
</text>]