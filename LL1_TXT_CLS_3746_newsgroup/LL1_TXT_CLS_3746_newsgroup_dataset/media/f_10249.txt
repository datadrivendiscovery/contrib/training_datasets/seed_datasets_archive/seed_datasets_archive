[<text>
<title>two resign from scott instruments &lt;scti&gt; board</title>
<dateline>    denton, texas, march 26 - </dateline>scott instruments inc said two
members of its board resigned.
    the company did not give a reason for the departure of the
directors, david m. jacobs and frederick brodsky.
    scott said it does not plan to fill the vacancies left by
their resignations.
 reuter
</text>]