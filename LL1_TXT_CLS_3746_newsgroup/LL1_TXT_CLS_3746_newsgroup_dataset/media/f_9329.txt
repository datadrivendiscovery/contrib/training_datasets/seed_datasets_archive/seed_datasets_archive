[<text>
<title>&lt;xylogics inc&gt; initial offering underway</title>
<dateline>    new york, march 25 - </dateline>lead underwriters salomon inc &lt;sb&gt; and
cowen anbd co said an initial public offering of 1,089,300
shares of xylogics inc is underway at 16 dlrs per share.
    the company is selling 750,000 shares and shareholders the
rest.  the company has granted underwriters an overallotment
option to buy up to 150,000 more shares.
 reuter
</text>]