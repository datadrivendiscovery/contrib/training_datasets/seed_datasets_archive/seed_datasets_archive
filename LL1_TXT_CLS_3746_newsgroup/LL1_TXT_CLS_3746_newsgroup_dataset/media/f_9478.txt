[<text>
<title>&lt;encor energy corp inc&gt; year loss</title>
<dateline>    calgary, alberta, march 25 - 
    </dateline>shr not given
    net loss 406.6 mln vs profit 35.4 mln
    revs 138.1 mln vs 211.9 mln
    note: 1986 net includes 545.7 mln dlr asset writedown
before 139.2 mln dlr recovery of deferred taxes.
    48 pct-owned by dome petroleum ltd &lt;dmp&gt;.
 reuter
</text>]