[<text>
<title>u.s. senate group urges subsidies for ussr</title>
<dateline>    washington, march 5 - </dateline>a majority of the senate agriculture
committee urged president reagan to reverse his opposition to
export subsidies to the soviet union as a way to get its
negotiators to purchase some 500 mln dlrs in american wheat.
    the group, led by committee chairman patrick leahy, a
vermont democrat, urged reagan to step up negotiations with the
soviet union by providing export subsidies to help u.s.
farmers.
 reuter
</text>]