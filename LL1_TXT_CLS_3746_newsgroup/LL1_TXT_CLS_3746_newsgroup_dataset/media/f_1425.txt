[<text>
<title>metropolitan federal &lt;mftn&gt; to make acquisition</title>
<dateline>    nashville, tenn., march 4 - </dateline>metropolitan federal savings
and loan association said it has signed a letter of intent to
acquire american trust of hendersonville, tenn., for an
undisclosed amount of cash.
    american trust had year-end assets of over 40 mln dlrs.
 reuter
</text>]