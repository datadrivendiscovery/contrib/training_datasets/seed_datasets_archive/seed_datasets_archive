[<text>
<title>giant bay &lt;gbylf&gt; details gordon lake deposit</title>
<dateline>    vancouver, british columbia, march 5 - </dateline>giant bay resources
ltd said a metallurgical study of its gordon lake gold deposit
indicated an overall recovery of 95 pct to 96 pct of the gold
can be achieved by either direct cyanidation of ore or
flotation followed by cyanidation of concentrate.
    continuation of an underground program on the property will
begin in june, extending an existing drift along the
200-foot-level where the main ore zone was encountered, giant
bay said.
    the company did not elaborate on production figures for the
property.
 reuter
</text>]