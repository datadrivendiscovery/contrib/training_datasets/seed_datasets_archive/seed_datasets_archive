[<text>
<title>thai zinc exports fall in march</title>
<dateline>    bangkok, april 8 - </dateline>thai zinc ingot exports fell to 882
tonnes in march from 1,764 in february and 3,008 in march 1986,
the mineral resources department said.
    a spokesman for padaeng industry co ltd, the country's sole
exporter, attributed the decline to the company's lower stocks,
which averaged 5,000 tonnes in the first quarter against 16,000
tonnes in late 1985 when it began exporting.
    the department said major buyers included china, japan, the
philippines, south korea, singapore and taiwan.
    thailand exported 4,842 tonnes of zinc ingots during the
first quarter, down from 14,937 a year ago.
 reuter
</text>]