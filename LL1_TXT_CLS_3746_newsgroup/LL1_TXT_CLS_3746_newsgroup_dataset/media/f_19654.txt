[<text>
<title>bsn &lt;bsn&gt; has purchased macgregor &lt;mgs&gt; stock</title>
<dateline>    dallas, june 29 - </dateline>bsn corp said it purchased macgregor
sporting goods' common stock in recent open market
transactions.
    bsn said its position is less than the five pct ownership
which would require specific disclosure. the company will
continually review its position and may nelect to increase or
decrease the number of shares owned, it added.
 reuter
</text>]