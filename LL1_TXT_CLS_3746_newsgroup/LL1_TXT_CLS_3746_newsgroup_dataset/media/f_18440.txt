[<text>
<title>japan truck makers to curb exports to north korea</title>
<dateline>    tokyo, june 16 - </dateline>all four japanese makers of large trucks
will stop exports to north korea following the issue of
japanese government guidance, company spokesmen told reuters.
    the move responds to the u.s. government's expression of
concern that north korea was using japanese-made trucks as
missile launching pads, they said.
    &lt;nissan diesel motor co ltd&gt; shipped 156 vehicles including
large and mid-sized trucks and buses to north korea in 1986
while isuzu motors ltd &lt;isum.t&gt; exported more than 100 six to
eight-tonne trucks in 1986, company spokesmen said.
    a spokesman for mitsubishi motors corp &lt;mimt.t&gt; said it had
almost no trade with north korea last year.
    &lt;hino motors ltd&gt; shipped 28 five-tonne trucks in 1986, a
company spokesman said.
 reuter
</text>]