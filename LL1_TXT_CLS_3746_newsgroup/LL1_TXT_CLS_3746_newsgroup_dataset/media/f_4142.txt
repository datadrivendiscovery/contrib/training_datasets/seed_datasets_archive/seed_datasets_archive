[<text>
<title>southwest realty &lt;swl&gt; year loss</title>
<dateline>    dallas, march 12 -
    </dateline>shr loss 44 cts vs profit 1.13 dlrs
    net loss 1,544,000 vs profit 3,912,000
    note: cash flow 1,010,000 dlrs or 29 cts shr vs 2,835,000
dlrs or 82 cts shr.
 reuter
</text>]