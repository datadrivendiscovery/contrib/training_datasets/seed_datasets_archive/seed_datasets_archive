[<text>
<title>cistron biotechnology &lt;cist.o&gt; in marketing deal</title>
<dateline>    pine brook, n.j., june 1 - </dateline>cistron biotechnology inc said
it has reached a marketing agreement for a "major multinational
consumer products company" it did not name to seek u.s. food
and drug administration approvals for over the counter sales of
cistron's home test for detection of vaginal infections.
    the company said product commercialization could take place
within 18 months.  the new tests are expected to retail for 10
to 20 dlrs each and take about 30 minutes for results to become
known.  it said its agreement with the major company also
covers worldwide sales.
 reuter
</text>]