[<text>
<title>national fuel gas co &lt;nfg&gt; sets mail date</title>
<dateline>    new york, march 16 - </dateline>national fuel gas co said its mail
date for its previously-announced two-for-one stock split is
june 19, 1987.
    the company, which announced the split last week, had said
the record date for the split is may 29, 1987.
 reuter
</text>]