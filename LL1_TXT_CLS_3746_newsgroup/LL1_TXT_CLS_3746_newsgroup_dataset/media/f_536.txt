[<text>
<title>tranzonic cos &lt;tnz&gt; sets quarterly</title>
<dateline>    cleveland, march 2 -
    </dateline>qtly div 11 cts vs 11 cts prior
    pay april 17
    record march 20
 reuter
</text>]