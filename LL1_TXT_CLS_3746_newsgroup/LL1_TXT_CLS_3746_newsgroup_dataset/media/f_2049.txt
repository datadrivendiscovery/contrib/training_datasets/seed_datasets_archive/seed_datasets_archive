[<text>
<title>regis corp &lt;rgis&gt; regular dividend set</title>
<dateline>    minneapolis, march 5 -
    </dateline>qtly div 4-1/2 cts vs 4-1/2 cts prior
    pay april 15
    record march 24
 reuter
</text>]