[<text>
<title>great atlantic and pacific tea co inc &lt;gap&gt; div</title>
<dateline>    new york, march 24 -
    </dateline>qtly div 10 cts vs 10 cts prior
    payable may one
    record april 15
 reuter
</text>]