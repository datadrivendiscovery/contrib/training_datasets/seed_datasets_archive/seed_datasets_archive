[<text>
<title>turner corp &lt;tur&gt; unit builds hospital addition</title>
<dateline>    fontana, calif., march 2 - </dateline>the turner corp's construction
company new york said its orange county office has begun
building a 23 mln dlr addition to kaiser permanente medical
center.
    the five-story, 197,000 square foot adition will be used
primarily as an outpatient treatment clinic, it said.
    developer for the project is kaiser foundation hospitals in
pasadena, calif., the company said.
    the completion date is october 1988, according to the
company.
 reuter
</text>]