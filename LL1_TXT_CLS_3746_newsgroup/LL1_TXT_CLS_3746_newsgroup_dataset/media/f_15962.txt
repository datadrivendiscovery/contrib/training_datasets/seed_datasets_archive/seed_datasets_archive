[<text>
<title>medivix &lt;medx&gt; unit in pact for mail orders</title>
<dateline>    garden city, n.y., april 9 - </dateline>medivix inc said its american
subscription plan inc subsidiary signed a contract with 
&lt;midwest benefits corp&gt; to provide mail order prescription
services for employees of 300 companies.
    midwest acts as a third party administrator for the
companies, medivix said.
    the company said it anticipates the new pact will generate
substantial revenues during the next 12 months.
 reuter
</text>]