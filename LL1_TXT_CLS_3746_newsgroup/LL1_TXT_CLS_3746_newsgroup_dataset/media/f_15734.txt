[<text>
<title>sears &lt;s&gt; sales reflect competitive pricing</title>
<dateline>    chicago, april 9 - </dateline>analysts who follow sears, roebuck and
co said the company's 4.2 pct sales gain in march by its
merchandise group reflects more competitive pricing and
continues a pattern of growth started in late 1986.
    sears said sales for the five weeks ended april 4 rose to
2.62 billion dlrs from 2.51 billion dlrs a year earlier.
    linda kristiansen of paine webber inc called the numbers
"impressive," and said she looks for a full-year sales gain of
five pct for the nation's largest retailer.
reuter^m
</text>]