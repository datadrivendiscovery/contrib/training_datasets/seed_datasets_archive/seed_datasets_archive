[<text>
<title>northeast savings &lt;nesa.o&gt; sets initial payout</title>
<dateline>    hartford, conn., june 19 - </dateline>northeast savings said its board
declared an initial quarterly dividend of 15 cts per share,
payable august one, record july 17.
 reuter
</text>]