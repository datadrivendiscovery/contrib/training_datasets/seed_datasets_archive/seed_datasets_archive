[<text>
<title>franklin high yield sets higher payout</title>
<dateline>    san mateo, calif., april 13 -
    </dateline>mthly div eight cts vs 7.1 cts prior
    pay april 30
    record april 15
    note: franklin high yield tax-free income fund.
 reuter
</text>]