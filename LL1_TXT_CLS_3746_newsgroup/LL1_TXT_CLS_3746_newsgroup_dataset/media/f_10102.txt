[<text>
<title>cb and t &lt;cbtb&gt; to make acquisition</title>
<dateline>    columbus, ga., march 26 - </dateline>cb and t bancshares inc said the
board of carrolton state bank of carrolton, ga., has approved a
merger into cb and t for an undisclosed amount of stock,
subject to approval by regulatory agencies and carrolton
shareholders.
    carrolton has 26 mln dlrs in assets.
 reuter
</text>]