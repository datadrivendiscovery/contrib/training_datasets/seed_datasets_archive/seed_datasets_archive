[<text>
<title>alliance financial corp &lt;alfl&gt; dividend set</title>
<dateline>    dearborn, mich., march 19 -
    </dateline>qtly div 22 cts vs 22 cts previously
    pay april 15
    record march 31
 reuter
</text>]