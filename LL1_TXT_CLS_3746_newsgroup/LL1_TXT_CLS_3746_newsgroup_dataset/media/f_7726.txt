[<text>
<title>texas utilities co &lt;txu&gt; 12 mths feb 28 net</title>
<dateline>    dallas, march 20 -
    </dateline>shr 4.50 dlrs vs 4.30 dlrs
    net 637.4 mln vs 588.5 mln
    revs 3.95 billion vs 4.10 billion
    avg shrs 141.7 mln vs 136.9 mln
 reuter
</text>]