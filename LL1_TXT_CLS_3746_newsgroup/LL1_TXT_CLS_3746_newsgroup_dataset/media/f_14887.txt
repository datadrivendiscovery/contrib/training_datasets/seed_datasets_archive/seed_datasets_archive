[<text>
<title>japan to hold formal financial talks with france</title>
<dateline>    tokyo, april 8 - </dateline>the japanese finance ministry said it
plans to hold formal financial talks with france on april 17 in
tokyo, the second such meeting after a first session in may
last year. the agenda has not yet been decided.
    the japanese side will be headed by toyoo gyohten, vice
finance minister for international affairs, while the french
delegation will include daniel lebegue, director general of the
treasury.
 reuter
</text>]