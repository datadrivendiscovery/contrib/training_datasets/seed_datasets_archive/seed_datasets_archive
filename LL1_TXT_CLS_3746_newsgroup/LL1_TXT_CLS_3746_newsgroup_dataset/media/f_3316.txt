[<text>
<title>s/p may downgrade caesars world &lt;caw&gt; debt</title>
<dateline>    new york, march 9 - </dateline>standard and poor's corp said it is
reviewing debt of caesars world inc with negative implications
following new york investor martin sosnoff's 725.2 mln dlr bid
to buy the caesars world common shares he does not yet own.
    the issues include caesars world's bb-rated senior debt and
single-b-plus subordinated debt, and caesars world finance
corp's b-plus subordinated debt, guaranteed by the parent.
about 230.8 mln dlrs of debt is affected.
    s and p said that, if successful, the debt financed
acquisition would greatly increase the firm's financial risk
and cause a marked drop in earnings and cash flow.
 reuter
</text>]