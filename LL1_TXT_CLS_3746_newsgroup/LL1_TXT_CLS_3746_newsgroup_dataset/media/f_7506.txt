[<text type="unproc">
international pharmaceutical gets fda approval
    costa mesa, calif., march 19 &lt;international pharmaceutical
products inc&gt; said it received food and drug administration
approval to market an injectable drug for treating several
forms of cancer.
    the drug, called methotrexate sodium injection, is commonly
used alone or in combination with other ant-caner agents for
breast, head, neck and lung cancer.
 reuter


</text>]