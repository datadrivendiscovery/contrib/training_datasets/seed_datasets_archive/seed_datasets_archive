[<text>
<title>moseley holding corp &lt;mose.o&gt; sees 1st qtr loss</title>
<dateline>    new york, june 19 - </dateline>moseley holding corp said it expects a
loss for its first fiscal quarter ending june 30.
    the company said the losses are related to its municipal
bond, unit trust and corporate bond activities, all of which
were affected by the difficult bond market.
    the company said losses for those areas were close to four
mln dlrs for april and may.
    it said its mortgage-backed activities were profitable
during those months.
 reuter
</text>]