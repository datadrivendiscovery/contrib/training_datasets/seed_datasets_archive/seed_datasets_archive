[<text>
<title>washington nat'l &lt;wnt&gt; buys united presidential</title>
<dateline>    evanston, ill., march 9 - </dateline>washington national corp's
washington national insurance co said it bought the remaining
15 pct of united presidential corp's &lt;upco&gt; outstanding shares
at 19 dlrs a share cash.
    the acquisition of the shares is part of a plan of exchange
approved by united presidential shareholders at a special
meeting march 6.
    the purchase of the remaining united presidential stake
follows washington national's buying 85 pct of united
presidential in a 19 dlrs a share tender offer which terminated
december 12.
 reuter
</text>]