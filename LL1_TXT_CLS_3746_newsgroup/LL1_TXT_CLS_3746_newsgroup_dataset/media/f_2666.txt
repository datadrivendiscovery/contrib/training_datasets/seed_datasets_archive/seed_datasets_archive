[<text>
<title>heineken n.v. &lt;hein.as&gt; 1986 year</title>
<dateline>    amsterdam, march 6 -
    </dateline>pre-tax profit 513.2 mln guilders vs 545.5 mln
    net profit 285.3 mln guilders vs 265.4 mln
    consolidated net turnover 6.68 billion guilders vs 6.40
billion
    net profit per 25.00 guilder nominal share 11.11 guilders
vs 10.33, taking into account one-for-three scrip issue last
year
    final dividend two guilders vs same, making total 3.50
guilders vs same
 reuter
</text>]