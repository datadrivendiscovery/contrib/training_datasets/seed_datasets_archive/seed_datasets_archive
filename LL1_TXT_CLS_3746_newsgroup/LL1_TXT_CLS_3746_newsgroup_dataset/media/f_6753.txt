[<text>
<title>hyponex &lt;hypx&gt; sells debentures at 11.828 pct</title>
<dateline>    new york, march 18 - </dateline>hyponex corp is raising 150 mln dlrs
through an offering of senior subordinated debentures due 1999
yielding 11.828 pct, said sole underwriter drexel burnham
lambert inc.
    the debentures have an 11-3/4 pct coupon and were priced at
99.50, drexel said.
    the issue is non-callable for five years. there were no
ratings by moody's investors service inc or standard and poor's
corp at the time of the pricing, drexel said. hyponex said
proceeds will be used mainly to finance acquisitions of assets
and businesses.
 reuter
</text>]