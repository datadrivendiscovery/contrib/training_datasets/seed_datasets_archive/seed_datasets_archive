[<text>
<title>u.s. semiconductor industry form consortium</title>
<dateline>    washington, march 4 - </dateline>a consortium of electrics industry
firms to challenge japan's presence in the global semiconductor
market was announced by the semiconductor industry association.
    the initiative will be funded by u.s. manufacturers to
develop the most advanced semiconductor manufacturing
technologies, the association said at a news conference.
    government participation and financial support will also be
sought for the consortium called sematech, for "semiconductor
manufacturing technology." 
    "we can continue to sit back and watch the japanese target
and assualt yet another critical u.s. industry. or we can get
in gear and do what is necessary to repell this attack," said
irwin federman, chief executive officer of monolithic memories
inc.
    he said the objective of sematech, a non profit
corporation, was to maintain leadership in an industry that was
vital to the u.s. economy and national security.
    the plan was for sematech to establish a facility at
various manufacturers and equipment suppliers to pioneer
advanced manufacturing techniques for semiconductors.
reuter...
</text>]