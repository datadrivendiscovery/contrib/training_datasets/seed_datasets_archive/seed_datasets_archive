[<text>
<title>&lt;concorde capital ltd inc&gt; in franchise pact</title>
<dateline>    miami, march 6 - </dateline>concorde capital ltd inc said it has
signed an agreement to establish a minimum of 40 heidi's frozen
yogurt shoppes in south florida over the next 48 months.
    it said it signed the agreement with &lt;zenith capital inc&gt;,
which received 3,890,000 concorde shares, or a 10 pct interest,
for the exclusive franchise rights from orlando south in
florida.
    zenith now has seven company owned and 44 franchised
heidi's open and another 90 franchises sold.
    concorde said it expects to change its name soon to heidi's
frozen yogurt shoppes of florida inc.
 reuter
</text>]