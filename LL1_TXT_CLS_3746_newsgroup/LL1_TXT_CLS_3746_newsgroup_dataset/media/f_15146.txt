[<text>
<title>canada allows publishing takeover by harcourt</title>
<dateline>    ottawa, april 8 - </dateline>investment canada said it has allowed the
indirect takeover of holt, rinehart and winston canada ltd.,
w.b. saunders co of canada ltd and les editions hrw ltd by
harcourt brace jovanovich canada inc.
    the government agency said, however, harcourt canada has
agreed to sell control of the firms to canadian interests
within two years.
    harcourt canada's u.s. parent, harcourt brace jovanovich
inc &lt;hbj&gt;, indirectly acquired the canadian book publishing
companies when it purchased holt rinehart and winston from cbs
inc &lt;cbs&gt; last october.           
 reuter
</text>]