[<text>
<title>showa denko exports aluminium casting equipment</title>
<dateline>    tokyo, april 9 - </dateline>&lt;showa denko ltd&gt; said it is exporting
aluminium billet casting equipment and technology to countries
that have recently begun aluminium smelting.
    a company official said it won a 500 mln yen order to
deliver 10 sets of casting equipment to venezuela's venalum by
end-1987. he said it received an order for one set from
aluminium bahrain b.s.c. last year and expects further orders
from the bahrain smelter.
    showa denko withdrew from smelting last year but expects to
increase its sales of equipment and technology, he said.
 reuter
</text>]