[<text>
<title>bci holdings unit president named</title>
<dateline>    oak brook, ill., march 9 - </dateline>bci holdings corp said it named
stephen rowley president of day-timers inc, a subsidiary of the
company's beatrice consumer durables, effective immediately.
    it said he replaces robert dorney, founder and former
president, who will remain as senior vice president, new
product development of the company, which produces time
management systems.
 reuter
</text>]