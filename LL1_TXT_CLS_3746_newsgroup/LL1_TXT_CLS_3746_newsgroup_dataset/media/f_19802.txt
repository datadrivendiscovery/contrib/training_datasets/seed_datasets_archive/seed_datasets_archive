[<text>
<title>gunnar gold (ggg.to) in venture agreement</title>
<dateline>    calgary, june 29 - </dateline>gunnar gold inc said it and mill city
gold inc signed an option and joint venture agreement with
tyranex gold inc on the tyranite gold property in ontario.
    gunnar said it and mill city can earn a 50 pct interest in
tyranex's option to buy the tyranite gold mine by spending up
to five mln dlrs on exploration, development, and feasibility
studies by 1990.
    it said the companies may form a joint venture partnership
to bring the mine to full commercial production.
 reuter
</text>]