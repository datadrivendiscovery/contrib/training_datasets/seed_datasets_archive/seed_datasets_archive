[<text>
<title>french firm has five pct of calif. water &lt;cwtr&gt;</title>
<dateline>    washington, april 1 - </dateline>compagnie generale des eaux
&lt;eaug.pa&gt;, a french water, waste treatment and disposal,
heating, ventilation and air conditioning concern said it has a
5.0 pct stake in california water service co.
    in a filing with the securities and exchange commission,
compagnie generale said it bought its 139,200 california water
shares for a total of 7.0 mln dlrs for investment purposes.
    while it said it might acquire more shares in the company,
it said it has no plans to seek control of it.
 reuter
</text>]