[<text>
<title>ibm &lt;ibm&gt;, bell atlantic &lt;bel&gt; in study pact</title>
<dateline>    new york, oct 20 - </dateline>international business machines corp
said it and bell atlantic corp agreed to jointly study product
opportunities for intelligent network services in the
international marketplace.
    the company said the study will assess the marketplace
requirements outside the u.s. for a series of intelligent
network voice, data, and image application.
    the company said the applications include 800-number
service, alternate billing services, and private network
services.
 reuter
</text>]