[<text>
<title>sound warehouse inc &lt;swhi&gt; 3rd qtr feb 28 net</title>
<dateline>    dallas, april 8 -
    </dateline>shr 26 cts vs 52 cts
    net 1,386,000 vs 2,773,000
    revs 47.7 mln vs 38.5 mln
    nine mths
    shr 52 cts vs 97 cts
    net 2,765,000 vs 5,419,000
    revs 116.9 mln vs 97 mln
 reuter
</text>]