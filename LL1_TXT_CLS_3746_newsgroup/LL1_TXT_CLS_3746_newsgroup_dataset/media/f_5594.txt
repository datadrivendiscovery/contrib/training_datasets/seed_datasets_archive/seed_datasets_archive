[<text>
<title>&lt;bkla bancorp&gt; dec 31 year net</title>
<dateline>    los angeles, march 16 -
    </dateline>shr 90 cts vs 66 cts
    net 924,000 vs 679,000
    loans 88.7 mln vs 67.4 mln
    deposits 165.5 mln vs 106.7 mln
    assets 181.5 mln vs 124.5 mln
 reuter
</text>]