[<text>
<title>texas international &lt;tei&gt; completes reserve sale</title>
<dateline>    oklahoma city, march 17 - </dateline>texas international co said it
has completed the previously-announced 120 mln dlr sale of its
domestic oil and natural gas reserves to &lt;total compagnie
francaise des petroles&gt;.
    it said on closing it used part of the proceeds to retire
all 100 mln dlrs of its u.s. bank and u.s. senior debt and the
rest will be used for general corporate purposes.
 reuter
</text>]