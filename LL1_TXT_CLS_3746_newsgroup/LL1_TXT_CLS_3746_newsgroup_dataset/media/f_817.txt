[<text>
<title>mfs municipal income trust &lt;mfm&gt; sets payout</title>
<dateline>    boston, march 2 - </dateline>mfs municipal income trust said it
declared a monthly income distribution of 5.7 cts a share
compared with 5.5 cts a share paid in the previous month.
    it said the distribution is payable march 27 to
shareholders of record march 13.
 reuter
</text>]