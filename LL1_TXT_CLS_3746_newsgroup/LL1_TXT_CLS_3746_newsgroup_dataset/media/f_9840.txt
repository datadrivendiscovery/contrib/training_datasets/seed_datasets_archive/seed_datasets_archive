[<text>
<title>k mart &lt;km&gt; to supply home shopping network</title>
<dateline>    troy, mich, march 26 - </dateline>k mart corp said it signed an
agreement to supply merchandise for entertainment marketing
inc's &lt;em&gt; home shopping subsidiary, consumer discount network.
    k mart, through its ultra buying network, will begin
supplying non-electronic goods to consumer discount network as
of april 1.
    it said entertainment marketing and k mart agreed to share
in the profits. in addition, k mart will receive warrants to
buy two mln shares of entertainment marketing's common stock.
 reuter
</text>]