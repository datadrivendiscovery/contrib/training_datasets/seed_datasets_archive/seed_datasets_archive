[<text>
<title>luxtec &lt;luxt&gt; cuts warrant exercise price</title>
<dateline>    sturbridge, mass., april 1 - </dateline>luxtec corp said it has
reduced the exercise price of its class b common stock purchase
warrants to one dlr from two dlrs from today through june 30.
 reuter
</text>]