[<text>
<title>gm canada to make contract offer to union</title>
<dateline>    toronto, oct 19 - </dateline>the canadian division of general motors
corp will make its first economic offer later monday in 
contract negotiations with 40,000 members of the canadian auto
workers, the union said.
    the union is seeking the same contract pattern it has
reached at the canadian units of ford motor co and chrysler
corp including partial pension indexation and wage increases in
each year of a three-year pact.
    the union hareatened to strike at 10 a.m. edt on
thursday unless general motors of canada ltd meets the pattern
and settles a host of local issues at 11 plants in quebec and
ontario.
 reuter
</text>]