[<text>
<title>usx &lt;x&gt; unit raises prices on certain grades</title>
<dateline>    lorain, ohio, june 2 - </dateline>usx corp's uss division said it was
raising prices for all hot rolled bar and semi-finished
products 1100 series grades by 10 dlrs per ton effective july
one, 1987.
    the company said the increase reflects current market
conditions.
    the company could not say what percentage the increase
reflects from current prices, nor could it say how much per ton
the products sell for currently.
 reuter
</text>]