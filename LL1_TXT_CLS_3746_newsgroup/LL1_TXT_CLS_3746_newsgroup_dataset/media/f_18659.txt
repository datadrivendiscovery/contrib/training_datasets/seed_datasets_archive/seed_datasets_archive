[<text>
<title>ge &lt;ge&gt; gets 250 mln dlr engine order</title>
<dateline>    new york, june 18 - </dateline>general electric co said it has
received contracts worth 250 mln dlrs, including options, to
supply its cf6-80c2 engines for the 747-400 and md-11 airliners
ordered from boeing co &lt;ba&gt; and mcdonnell douglas corp &lt;md&gt;
respectively by thai airways international.
    the company said it also received a 16 mln dlr order from
thai air for cf6-50 engines on a mcdonnell douglas dc-10 series
30.
 reuter
</text>]