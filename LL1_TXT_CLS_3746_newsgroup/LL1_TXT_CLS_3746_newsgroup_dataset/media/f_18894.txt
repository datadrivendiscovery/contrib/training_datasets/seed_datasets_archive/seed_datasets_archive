[<text>
<title>national commerce &lt;ncbc.o&gt; in kroger &lt;kr&gt; deal</title>
<dateline>    memphis, tenn., june 18 - </dateline>national commerce bancorp said it
has agreed to operate or sublicense branch bank facilities in
supermarkets in florida and virginia kroger co supermarkets.
    it said initially 20 locations will be available in florida
and 10 in virginia.
 reuter
</text>]