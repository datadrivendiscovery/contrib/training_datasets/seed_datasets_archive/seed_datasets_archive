[<text>
<title>iceland to privatise state bank utvegsbanki</title>
<dateline>    reykjavik, march 18 - </dateline>the icelandic government said it will
privatise the state-owned bank utvegsbanki, the country's
second largest. parliament also granted an 800 mln crown cash
infusion to ease cash flow problems that arose when the bank
lost 600 mln crowns in a shipping firm bankruptcy two years
ago.
    utvegsbanki director halldor gudbjarnarson told reuters the
decision to privatise the bank was a relief and that foreign
banks had already expressed interest in taking a share.
    a quarter of the one billion crown total share capital will
be available to foreign investors, government officials said.
 reuter
</text>]