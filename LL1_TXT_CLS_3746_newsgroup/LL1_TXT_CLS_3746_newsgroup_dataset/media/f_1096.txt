[<text>
<title>ausimont compo nv &lt;aus&gt; 4th qtr net</title>
<dateline>    walahtam, mass., march 3 -
    </dateline>shr 42 cts vs 39 cts
    net 12.3 mln vs 9,382,000
    sales 172.0 mln vs 146.00 mln
    avg shrs 29.5 mln vs 24.3 mln
    year
    shr 1.63 dlrs vs 1.35 dlrs
    net 45.7 mln vs 30.0 mln
    sales 665.5 mln vs 446.2 mln
    avg shrs 28.0 mln vs 22.3 mln
    note: translated at 1,339 italian lire to dollar.
 reuter
</text>]