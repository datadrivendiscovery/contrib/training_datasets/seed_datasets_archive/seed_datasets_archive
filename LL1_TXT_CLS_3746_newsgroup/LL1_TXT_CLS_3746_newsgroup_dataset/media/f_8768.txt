[<text>
<title>geo. a. hormel &lt;hrl&gt; votes two-for-one split</title>
<dateline>    austin, minn., march 24 - </dateline>geo. a. hormel and co said its
directors voted a two-for-one split, payable june one, record
april 18.
 reuter
</text>]