[<text>
<title>community bank &lt;cbsi&gt; to make acquisition</title>
<dateline>    syracuse, n.y., march 3 - </dateline>community bank system inc said it
has entered into a definitive agreement to acquire nichols
community bank for 2,800,000 dlrs in common stock.
    it said subject to approval by nichols shareholders and
regulatory authorities, the transaction is expected to be
completed later this year.
 reuter
</text>]