[<text>
<title>&lt;mds health group limited&gt; in qtly payout</title>
<dateline>    toronto, march 3 - 
    </dateline>qtly div six cts vs six cts prior
    pay april one
    record march 23
 reuter
</text>]