[<text>
<title>mitsubishi heavy builds energy-saving tanker</title>
<dateline>    tokyo, june 16 - </dateline>mitsubishi heavy industries ltd &lt;mith.t&gt;
said it began building the world's most advanced energy-saving
tanker, which consumes only 48 tonnes of fuel oil a day.
    construction of the 258,000 dwt vlcc (very large crude
carrier) nisseki maru for &lt;tokyo tanker co ltd&gt;, a shipping arm
of nippon oil co ltd &lt;npol.t&gt;, is expected to be completed in
april 1988. it would run on the japan/gulf route, a company
statement said.
    the statement gave no other details.
    mitsubishi heavy last year finished building a vlcc of
similar size, but which consumed 57 tonnes of fuel a day.
 reuter
</text>]