[<text>
<title>nasd president leaves for hambrecht and quist</title>
<dateline>    new york, march 24 - </dateline>&lt;hambrecht and quist group&gt; said
gordon macklin has resigned as president of the &lt;national
association of securities dealers&gt; to become chairman and
co-chief executive officer of hambrecht and quist.
    the san francisco brokerage firm said macklin is expected
to join it by july.
    as chairman, he succeeds hugh t. wyles.
    the other chief executive officer is founder william r.
hambrecht.
    in washington, the nasd said macklin has offered to stay on
until a new president is named.  it said it will soon appoint a
committee to begin the search for a successor.
 reuter
</text>]