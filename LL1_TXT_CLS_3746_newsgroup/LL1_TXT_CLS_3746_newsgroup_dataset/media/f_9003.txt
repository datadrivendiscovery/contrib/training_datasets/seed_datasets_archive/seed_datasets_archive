[<text>
<title>numerex corp &lt;nmrx&gt; 2nd qtr jan 31 loss</title>
<dateline>    minneapolis, minn., march 24 - 
    </dateline>shr loss seven cts vs profit five cts
    net loss 149,421 vs profit 103,120
    sales 1,698,345 vs 1,920,010
    six mths
    shr loss five cts vs profit nine cts
    net loss 100,472 vs profit 191,614
    sales 3,836,794 vs 3,650,322
 reuter
</text>]