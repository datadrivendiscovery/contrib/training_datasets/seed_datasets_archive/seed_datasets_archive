[<text>
<title>small quantity of uk wheat sold to home market</title>
<dateline>    london, march 3 - </dateline>a total of 2,435 tonnes of british
intervention feed wheat were sold at today's tender for the
home market out of requests for 3,435 tonnes, the home grown
cereals authority, hgca, said.
    price details were not reported.
    no bids were submitted for intervention barley.
 reuter
</text>]