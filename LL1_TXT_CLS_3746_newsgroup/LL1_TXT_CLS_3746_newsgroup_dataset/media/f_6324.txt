[<text>
<title>bankers trust new york corp &lt;bt&gt; qtly dividend</title>
<dateline>    new york, march 17 - 
    </dateline>qtly div 41.5 cts vs 41.5 cts prior
    pay april 28
    record march 31
 reuter
</text>]