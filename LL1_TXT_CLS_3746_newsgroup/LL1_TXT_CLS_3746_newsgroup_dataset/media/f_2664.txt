[<text>
<title>france fund inc &lt;frn&gt; sets initial dividend</title>
<dateline>    new york, march 6 - </dateline>france fund inc said its board declared
an initial dividend of 1.12 dlrs per share, payable april six,
to holders of record march 20.
    the fund said the dividend represents two cts per share for
net investment income realized during 1986 and 1.10 dlrs from
net taxable gains realized during the year.
 reuter
</text>]