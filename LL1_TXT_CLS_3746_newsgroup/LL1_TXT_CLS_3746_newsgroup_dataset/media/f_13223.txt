[<text>
<title>sabine royalty trust &lt;sbr&gt; sets monthly payout</title>
<dateline>    dallas, april 3 - 
    </dateline>cash distribution 13.3 cts vs 8.4 cts prior
    pay april 29
    record april 15
 reuter
</text>]