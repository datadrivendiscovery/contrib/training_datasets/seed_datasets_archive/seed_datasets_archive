[<text>
<title>sithe to raise energy factors &lt;efac&gt; stake</title>
<dateline>    san diego, june 19 - </dateline>sithe-energies lp said it has signed
an agreement under which it will increase its interest in
energy factors inc to 70.0 pct from 53.4 pct now by investing
100 mln dlrs in energy factors stock.
 reuter
</text>]