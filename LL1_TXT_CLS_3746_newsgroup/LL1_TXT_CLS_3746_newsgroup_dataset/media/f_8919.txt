[<text>
<title>gordon jewelry &lt;gor&gt; completes sale of unit</title>
<dateline>    houston, march 24 - </dateline>gordon jewelry corp said it has
completed the previously-announced sale of the assets of its
catalog showroom stores to privately-held carlisle capital corp
for an undisclosed amount of cash and notes in excess of book
value.
 reuter
</text>]