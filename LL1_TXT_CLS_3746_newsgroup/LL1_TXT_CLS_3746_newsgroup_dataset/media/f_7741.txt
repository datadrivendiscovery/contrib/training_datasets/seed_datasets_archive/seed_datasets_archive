[<text>
<title>westcorp &lt;wcrp&gt; unit offers auto bonds</title>
<dateline>    orange, calif., march 20 - </dateline>westcorp's western financial
auto loans 2 inc said it has commenced an offering of 125 mln
dlrs of aaa-rated automobile receivable collateralized bonds.
    the bonds will be due march 1, 1990 and are priced at
99.9375 pct to yield 6.86 pct.
    this is the third such offering the company has made and
the second in the past four months.
 reuter
</text>]