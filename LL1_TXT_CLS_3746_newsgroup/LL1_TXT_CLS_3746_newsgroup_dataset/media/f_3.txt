[<text>
<title>texas commerce bancshares &lt;tcb&gt; files plan</title>
<dateline>    houston, feb 26 - </dateline>texas commerce bancshares inc's texas
commerce bank-houston said it filed an application with the
comptroller of the currency in an effort to create the largest
banking network in harris county.
    the bank said the network would link 31 banks having
13.5 billion dlrs in assets and 7.5 billion dlrs in deposits.
       
 reuter
</text>]