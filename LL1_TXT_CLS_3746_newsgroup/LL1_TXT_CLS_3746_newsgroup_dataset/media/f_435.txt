[<text>
<title>whittaker &lt;wkr&gt; to have gains from sales</title>
<dateline>    los angeles, march 2 - </dateline>whittaker corp said it will have a
gain on the sale of discontinued businesses after any operating
losses from the businesses up until the dates of disposition,
but it will defer reporting the gain until its restructuring
program hsa been substantially completed.
    the company said in the first quarter ended january 31,m it
completed the divestiture of its health maintenance
organization operations to travelers corp &lt;tic&gt; , sold its
whittar steel strip operations to &lt;dofascoxinc&gt; and sold its
equity investment in bertram-trojan inc to an affiliate of
&lt;investcorp&gt;.
    the company said it has entered into definitive agreements
to sell whittaker general medical corp, bennes marrel sa of
france and juster steel corp as well.
    the company said to date it has received proceeds of about
90 mln dlrs from divestitures and has used the funds to reduce
debt incurred in the repurchase of its common shares.
    whittaker today reported first quarter earnings from
continuing operations fell to 1,522,000 dlrs from 3,501,000
dlrs a year before. the year-earlier figure excluded a
1,817,000 dlr loss from discontinued operations.
 reuter
</text>]