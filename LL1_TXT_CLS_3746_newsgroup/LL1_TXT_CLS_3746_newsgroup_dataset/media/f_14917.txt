[<text>
<title>malaysian fund to be launched in u.s. in may</title>
<dateline>    kuala lumpur, april 8 - </dateline>the first malaysian investment fund
for foreign investors will be launched in the united states
market next month to raise between 60 to 69 mln dlrs, &lt;arab
malaysian merchant bank bhd&gt; (ambb) said.
    the fund, called &lt;malaysian fund inc&gt;, will be jointly
sponsored by the merchant bank and the international fund
corporation of the world bank, said ambb in a statement.
    the two institutions, together with merrill lynch capital
markets &lt;mer&gt; and morgan stanley inc &lt;ms&gt;, are underwriters for
the fund, it added.
    ambb said its wholly-owned subsidiary &lt;arab malaysian
consultant&gt; will be the fund's malaysian investment adviser
while &lt;morgan stanley asset management inc&gt; will manage it.
    it added that 80 pct of the money raised will be used to
invest in shares of malaysian companies on the kuala lumpur
stock exchange and the remaining in unlisted shares.
    ambb said the fund will expose the malaysian market to us
and japanese investors and help develop the local capital
market.
 reuter
</text>]