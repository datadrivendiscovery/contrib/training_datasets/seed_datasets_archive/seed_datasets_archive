[<text>
<title>sprint ii issues 35 mln dlr repackaged floater</title>
<dateline>    london, oct 19 - </dateline>sprint ii, a special purpose corporation
based in the cayman islands, is issuing 35 mln dlrs of floating
rate bonds due november 6, 1992 and priced at 100.10, said fuji
international finance ltd as lead manager.
    the bonds are backed by a pool of 48.7 mln dlrs of japanese
ex-warrant bonds. the bonds are priced to yield 20 basis points
over the six month london interbank offered rate. they are
available in denominations of 100,000 dlrs and are payable on
november 6. there is a seven basis point management and
underwriting fee, an eight basis point selling concession and a
two basis point praecipuum.
 reuter
</text>]