[<text>
<title>melville &lt;mes&gt; names chief financial officer</title>
<dateline>    harrison, n.y., march 12 - </dateline>melville corp said it elected
robert huth as executive vice president and chief financial
officer, succeeding kenneth berland on april six.
    berland will continue as vice chairman and chief
administrative officer.
    huth is a partner in the accounting firm of peat, marwick,
mitchell and co.
 reuter
</text>]