[<text>
<title>enseco &lt;ncco.o&gt; gets epa contract</title>
<dateline>    cambridge, mass, oct 19 - </dateline>enseco inc said it has received
4,600,000 dlrs in contract from the u.s. environmental
protection agency to test samples in support of the superfund
program over a 30-month period and to perform special
analytical services.
 reuter
</text>]