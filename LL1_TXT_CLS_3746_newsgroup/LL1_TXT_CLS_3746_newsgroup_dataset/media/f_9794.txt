[<text>
<title>sandoz has 50 mln dlr euro-cp program</title>
<dateline>    london, march 26 - </dateline>sandoz corp, a u.s. subsidiary of sandoz
ag, is establishing a 50 mln dlr euro-commercial paper (cp)
program, morgan guaranty ltd said as one of the dealers.
    the other dealer is swiss bank corp international ltd and
morgan guaranty trust co of new york is issuing and paying
agent.
    paper will have maturities between seven and 183 days and
will be issued in global and definitive form.
 reuter
</text>]