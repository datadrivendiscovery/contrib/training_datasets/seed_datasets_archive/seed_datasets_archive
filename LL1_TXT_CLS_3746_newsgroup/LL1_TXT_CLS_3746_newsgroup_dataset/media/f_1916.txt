[<text>
<title>n. zealand markets prepare for trading bank strike</title>
<dateline>    wellington, march 5 - </dateline>new zealand clearing house &lt;databank
systems ltd&gt; said it will know tomorrow what services it will
be able to provide during a strike by bank officers against
trading banks and databank set for march 9 and 10.
    trading banks polled by reuters said their ability to offer
money market services during the strike depends on whether
databank remains open, and whether the banks have enough staff
to process transactions.
    a spokesman for the new zealand foreign exchange
association said dealers would be able to trade during the
strike.
    but the spokesman added that from march 6 to 10 the value
date of currency transactions will be march 16.
    trading bank spokesmen told reuters they will try to honour
transactions in which an offshore party sought payment on march
9 or 10, but they could not guarantee settlement.
    the futures exchange said trading members and their clients
will be able to continue trading provided they have made
suitable financial arrangements.
    the trading banks affected are the &lt;bank of new zealand&gt;,
australia and new zealand banking group ltd &lt;anza.s&gt;, westpac
banking corp &lt;wstp.s&gt; and &lt;national bank of new zealand ltd&gt;.
 reuter
</text>]