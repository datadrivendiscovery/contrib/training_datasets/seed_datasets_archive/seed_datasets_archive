[<text>
<title>usair group inc &lt;u&gt; sets quarterly</title>
<dateline>    washington, march 25 -
    </dateline>qtly div three cts vs three cts prior
    pay april 30
    record april 16
 reuter
</text>]