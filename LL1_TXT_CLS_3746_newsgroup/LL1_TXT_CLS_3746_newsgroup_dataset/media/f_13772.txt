[<text>
<title>airbus to use ge &lt;ge&gt; engine on a-340 airliner</title>
<dateline>    paris, april 7 - </dateline>the european &lt;airbus industrie&gt; consortium
is to change the lead engine on its a-340 aircraft because the
high-technology superfan engine will not be ready by 1992, a
company spokesman said.
   airbus has chosen the franco-u.s. cfm-56-s3 engines to
replace the &lt;international aero engine&gt; (iae) consortium's
superfan. the spokesman added the new engine, to be produced by
general electric co of the u.s. and snecma of france, can
provide greater thrust than the iae rival.
    the spokesman said the iae consortium, which includes pratt
and whitney of the u.s. and britain's &lt;rolls royce&gt;, is unable
to supply its superfan engines in time for mid-1992 when the
a-340 is due to become operational.
    airbus signed an agreement with iae last december,
replacing an earlier accord with ge and &lt;snecma&gt;, whose cfm-56
engine could then offer no more than a 28,600 lbs thrust,
compared to the proposed superfan's 30,000 lbs.
    the latest version of the cfm-56, however, can provide a
30,600 lbs thrust.
    the airbus spokesman said the consortium's decision did not
exclude the iae superfan, once ready, being offered as an
alternative engine on the a-340 as the superfan was more
fuel-efficient than the cfm engine.
    the spokesman said airbus expected iae to come back with
some proposals on superfan's new production timetable within
the next few weeks or months.
 reuter
</text>]