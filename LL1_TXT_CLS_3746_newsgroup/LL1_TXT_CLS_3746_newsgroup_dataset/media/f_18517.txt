[<text>
<title>toyota and nissan exports fall in may</title>
<dateline>    tokyo, june 18 - </dateline>toyota motor corp &lt;toyo.t&gt; and nissan
motor co ltd &lt;nsan.t&gt; both said may vehicle exports fell from a
year earlier mainly due to the yen rise against the dollar.
    toyota's exports fell 9.5 pct from a year earlier to
161,266 vehicles in may while nissan's fell 17.6 pct to
100,221.
    toyota's exports to the u.s. fell 13.9 pct from a year
earlier to 90,096 in may and those to europe fell 2.8 pct from
a year earlier to 34,876.
    nissan's exports to the u.s. fell 25.4 pct to 53,604 but
those to europe rose 0.7 pct to 29,721.
 reuter
</text>]