[<text>
<title>matsushita in joint venture with ford, mazda</title>
<dateline>    tokyo, june 3 - </dateline>matsushita electric industrial co ltd
&lt;mc.t&gt;, mazda motor corp &lt;mazt.t&gt; and ford motor co &lt;f&gt; have
signed an agreement to set up a joint venture to make car air
conditioners in japan, matsushita said in a statement.
    the new venture, &lt;japan climate systems corp&gt; will be
capitalised at 1.5 billion yen, and the three partners will
hold equal shares.
    the partners will invest 3.6 billion yen in constructing a
plant in hiroshima, due for completion in late 1988. the plant
will initially make 100,000 air conditioners a year for mazda,
it said.
 reuter
</text>]