[<text>
<title>management group has restaurant &lt;ra&gt; majority</title>
<dateline>    new york, oct 19 - </dateline>restaurant associates industries inc
said the management group led by chairman martin brody and
president max pine through october 16 had received 1,796,727
class a and 1,766,091 class b shares in response to its tender
offer for all shares at 18 dlrs each, giving thema majority of
each class of shares.
    the company said the tender has been extended until
november 6.
 reuter
</text>]