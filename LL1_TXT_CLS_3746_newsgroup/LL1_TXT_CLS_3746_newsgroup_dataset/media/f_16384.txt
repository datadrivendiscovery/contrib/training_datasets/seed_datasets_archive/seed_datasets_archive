[<text>
<title>burroughs-wellcome sets research spending</title>
<dateline>    montreal, april 13 - </dateline>(burroughs-wellcome inc) said it plans
to invest 10 mln dlrs in research and development-related
activities over the next five years if changes planned to
canada's patent act become law.
    the company said it would increase its commitment to
clinical research if the patent act is changed to provide
further protection for patent rights, as planned.
 reuter
</text>]