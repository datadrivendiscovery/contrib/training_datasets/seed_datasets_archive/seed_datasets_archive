[<text>
<title>whirlpool &lt;whr&gt; names new chairman</title>
<dateline>    benton harbor, mich., oct 19 - </dateline>whirlpool corp said it named
david whitwam to the additional position of chairman, effective
december one, replacing jack sparks, who retires november 30.
    it said whitwam was elected president and chief executive
officer effective july one.
    sparks will continue to serve on whirlpool's board of
directors as chairman of the finance committee.
 reuter
</text>]