[<text>
<title>the gap inc &lt;gps&gt; names president</title>
<dateline>    san bruno, calif., march 19 - </dateline>the gap inc said it named
millard drexler as its president.
    drexler, 42, had been executive vice preisdent for
merchandising and president of the gap stores division.
    previously, chairman and chief executive officer donald
fisher also held the position of president as well.
    drexler retains his position as president and chief
executive officer of the gap stores division.
 reuter
</text>]