[<text>
<title>epitope &lt;epto&gt; to market aids test</title>
<dateline>    beaverton, ore., march 4 - </dateline>epitope inc said it has
developed a western blot aids test and it will begin worldwide
marketing efforts immediately.
 reuter
</text>]