[<text>
<title>harcourt brace &lt;hbj&gt; may be lowered by moody's</title>
<dateline>    new york, march 12 - </dateline>moody's investors service inc said it
may downgrade 200 mln dlrs of debt of harcourt brace jovanovich
inc.
    the rating agency cited harcourt brace's recent aggressive
acquisition activity and increasing leverage.
    harcourt brace has offered 220 mln dlrs for harper and row
publishers, moody's noted. this follows harcourt's completed
acquisitions of the publishing group of cbs inc and marineland.
 reuter
</text>]