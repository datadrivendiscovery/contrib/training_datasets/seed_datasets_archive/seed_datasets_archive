[<text>
<title>&lt;american eagle petroleums ltd&gt; year loss</title>
<dateline>    calgary, alberta, april 9 -
    </dateline>shr loss 10 cts vs profit 17 cts
    net loss 1,546,000 vs profit 4,078,000
    revs 22.6 mln vs 38.9 mln
 reuter
</text>]