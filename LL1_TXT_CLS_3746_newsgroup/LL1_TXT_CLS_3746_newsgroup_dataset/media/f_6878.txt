[<text>
<title>american international &lt;aipn&gt; in suit</title>
<dateline>    new york, march 18 - </dateline>american international petroleum corp
said a law suit alleging breach of contract and
misrepresentation has been filed against its majestic crude
corp subsidiary by amoni ltd.
    the suit seeks 600,000 dlrs in damages.
    american international said it will defend against the
charges.
 reuter
</text>]