[<text>
<title>genetics institute &lt;geni&gt; buys plant</title>
<dateline>    cambridge, mass., march 17 - </dateline>genetics institute inc said it
has purchased a 139,000 square foot plant on 50 acres in
andover, mass., to which it will move all of its process
development and pilot and clinical production groups from its
cambridge, mass., headquarters, from henley group inc &lt;heng&gt;
for 15.3 mln dlrs.
    genetics said the purchase of the plant will allow a
fourfold expansion of its pilot and clinical production
facilities for biotechnology-based human pharmaceuticals.
    the company said it plans to invest another 20 mln dlrs to
equip the facility for process development activities and
clinical scale production.  construction is to be completed by
late 1987.
 reuter
</text>]