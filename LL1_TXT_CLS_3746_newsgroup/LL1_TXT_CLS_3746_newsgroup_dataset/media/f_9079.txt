[<text>
<title>volcker calls dollar slide enough</title>
<dateline>    washington, march 24 - </dateline>federal reserve board chairman paul
volcker said that the dollar's slide in currency markets has
been enough, a fed spokesman said.
    the spokesman confirmed that volcker, who spoke to a group
of financial analysts, said in answer to a question about the
dollar's recent slide that "enough is enough."
    volcker has often expressed concern about the dollar
falling too rapidly in currency markets.
 reuter
</text>]