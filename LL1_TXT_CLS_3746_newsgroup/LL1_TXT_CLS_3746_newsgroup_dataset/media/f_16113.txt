[<text>
<title>u.s. auto union will fight to stop job/wage cuts</title>
<dateline>    chicago, april 12 - </dateline>the united auto workers union (uaw)
vowed to fight wage and job cuts in a round of labour talks
starting in july that cover nearly 500,000 workers at general
motors corp &lt;gm&gt; and ford motor co &lt;f&gt;.
    "the uaw doesn't go around picking fights, but we don't run
away from them when they're forced on us ... we want peace, but
peace comes at a price," union president owen bieber said.
    "and if it's war, the uaw will be ready for it," he said, in
an address to 3,000 union leaders at the start of a four-day
special convention.
    he said the detroit carmakers had turned their backs on
america by the increasing use of imports that cost u.s. jobs.
    he said the union was ready to strike for "job and income
guarantees," bans on shifting production to foreign and other
non-union sources, and increases in pay and profit-sharing.
    gm, under pressure from declining sales and profits, has
said this year's talks will be difficult because it wants to
limit wage rises and shed parts-manufacturing operations
considered uncompetitive.
    contracts at both companies expire at midnight on september
14.
 reuter
</text>]