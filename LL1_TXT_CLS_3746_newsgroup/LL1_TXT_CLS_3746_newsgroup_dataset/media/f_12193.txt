[<text>
<title>chevron &lt;chv&gt; unit accepts american express</title>
<dateline>    san francisco, april 1 - </dateline>chevron corp's domestic oil and
gas unit, chevron usa, said it will honor american express
cards at all of the company's chevron outlets but only some of
its gulf branded outlets.
    chevron usa, with more than 15,000 chevron and gulf outlets
nationwide, predicted all of its independent dealers will elect
to honor the cards. because of recent divestitures, chevron usa
said the agreement does not cover gulf brand outlets in
alabama, florida, georgia, kentucky, mississippi, north
carolina, south carolina and tennessee, as well as delaware,
new jersey, new york, pennsylvania and new england.
 reuter
</text>]