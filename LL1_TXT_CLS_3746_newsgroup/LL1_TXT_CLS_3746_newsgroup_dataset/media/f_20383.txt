[<text>
<title>honeywell &lt;hon&gt; elects new chief executive</title>
<dateline>    minneapolis, oct 20 - </dateline>honeywell inc said its board of
directors elected its president, dr james renier, 57, as chief
executive officer to succeed edson spencer, who will continue
as chairman of the board.
 reuter
</text>]