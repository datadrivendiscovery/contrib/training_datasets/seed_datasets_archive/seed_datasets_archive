[<text>
<title>federal industries sets commercial paper issue</title>
<dateline>    winnipeg, manitoba, march 2 - </dateline>&lt;federal industries ltd&gt; said
it introduced a commercial paper program with an authorization
of 440 mln dlrs through agents &lt;bank of montreal&gt;, &lt;dominion
securities inc&gt; and wood gundy inc.
    net proceeds from the sale of notes will be used for
general corporate purposes and will replace existing
outstanding debt, the company said.
    it did not elaborate on financial terms of the issue.
 reuter
</text>]