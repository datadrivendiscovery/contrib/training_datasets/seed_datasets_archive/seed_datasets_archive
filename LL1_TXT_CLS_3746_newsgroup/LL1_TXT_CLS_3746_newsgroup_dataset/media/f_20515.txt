[<text>
<title>petrolane partners l.p. &lt;lpg&gt; 3rd qtr loss</title>
<dateline>    long beach, calif., oct 20 -
    </dateline>shr loss five cts vs profit six cts
    net loss 1.2 mln vs profit 1.4 mln
    revs 114.9 mln vs 109.3 mln
    nine months
    shr profit one dlrs vs profit 84 cts
    net profit 23.7 mln vs profit 19.9 mln
    revs 430.9 mln vs 435.4 mln
    note: results are in pro forma form. partnership became
public on march 19, 1987. previous results reported from texas
eastern corp's &lt;tet&gt; petrolane inc domestic division.
 reuter
</text>]