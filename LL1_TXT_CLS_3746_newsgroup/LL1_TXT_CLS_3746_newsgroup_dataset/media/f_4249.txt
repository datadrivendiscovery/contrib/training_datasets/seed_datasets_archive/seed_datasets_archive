[<text>
<title>leader &lt;ldco&gt; buys petrosurance stake</title>
<dateline>    columbus, ohio, march 12 - </dateline>leader development corp said it
has purchased 300,000 shares of convertible preferred stock in
&lt;petrosurance inc&gt; for 1,500,000 dlrs in cash and real estate.
    petrosurance specializes in property and casualty insurance
for the oil industry.
    leader said the stock is convertible into a petrosurance
common. leader said it already owns 12.5 pct of petrorusrance
and conversion would give it about 45.0 pct.
    the company said petrosurance will use the sale proceeds to
support growth and improve the structure of its reinsurance
treaties to retain a larger part of premiums written.
 reuter
</text>]