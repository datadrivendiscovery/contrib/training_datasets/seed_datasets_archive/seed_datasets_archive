[<text>
<title>witco corp &lt;wit&gt; sets regular qtly payout</title>
<dateline>    new york, march 3 -
    </dateline>qtly div 28 cts vs 28 cts prior
    pay april one
    record march 13
 reuter
</text>]