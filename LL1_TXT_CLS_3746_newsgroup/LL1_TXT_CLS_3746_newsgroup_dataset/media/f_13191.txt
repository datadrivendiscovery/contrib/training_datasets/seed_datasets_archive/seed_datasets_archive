[<text>
<title>u.s. company earnings highlights</title>
<dateline>    new york, april 3 - </dateline>first quarter
 anchor financial corp shr 31 cts vs 31
 nine months
 biomet inc shr 49 cts vs 36
 federal co shr 3.55 dlrs vs 1.66
 richardson electronics shr 59 cts vs 53
 year
 eac industries inc oper shr loss 65 cts vs
 loss 97 cts
 fine art acquisitions shr 15 cts vs 10
 mangood corp oper shr loss 6.07 dlrs vs  loss  7.64
 reuter
</text>]