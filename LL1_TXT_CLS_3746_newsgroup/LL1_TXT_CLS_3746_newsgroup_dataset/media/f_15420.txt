[<text>
<title>johnson matthey's platinum group prices</title>
<dateline>    london, april 9 - </dateline>johnson matthey today issued the
following platinum group base prices (unfabricated), all u.s.
dlrs per troy ounce.
 previous prices in parentheses.
 platinum  -   562   (567)
 palladium -   130   (130)
 iridium   -   400   (400)
 rhodium     1,230 (1,230)
 ruthenium -    80    (80)
</text>]