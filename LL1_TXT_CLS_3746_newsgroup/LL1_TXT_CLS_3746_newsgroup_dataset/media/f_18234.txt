[<text>
<title>asamera &lt;asm&gt; completes refinery sale</title>
<dateline>    calgary, alberta, june 2 - </dateline>asamera inc said wholly owned
asamera oil (u.s.) inc concluded the 25.25 mln u.s. dlr sale of
its denver refinery to total petroleum north america ltd.
    in addition, total is purchasing crude oil and refined
product inventories at market value, asamera said.
 reuter
</text>]