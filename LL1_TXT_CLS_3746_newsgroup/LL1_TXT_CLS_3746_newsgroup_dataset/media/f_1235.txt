[<text>
<title>perini investment &lt;pnv&gt; buys florida properties</title>
<dateline>    framingham, mass., march 3 - </dateline>perini investment properties
inc said it has purchased three industrial/warehouse properties
in boca raton, fla., and one in pompano beach, fla., for an
undisclosed amount.
    the company said the purchases increase its
industrial/warehouse inventory by about 252,000 square feet.
 reuter
</text>]