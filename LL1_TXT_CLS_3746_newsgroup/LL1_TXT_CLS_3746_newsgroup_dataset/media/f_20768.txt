[<text>
<title>tenera ltd &lt;tlpzz.o&gt; 3rd qtr net</title>
<dateline>    berkeley, calif., oct 20 -
    </dateline>shr 25 cts vs 26 cts
    net 2,200,000 vs 2,100,000
    revs 8,500,000 vs 9,600,000
    nine mths
    shr 77 cts vs 63 cts
    net 6,900,000 vs 4,800,000
    revs 27.9 million vs 25.3 million
 reuter
</text>]