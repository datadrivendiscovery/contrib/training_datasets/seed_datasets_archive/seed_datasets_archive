[<text>
<title>peps boys-manny, moe and jack &lt;pby&gt; set payout</title>
<dateline>    philadelphia, march 20 -
    </dateline>qtly div 5.5 cts vs 5.5 cts prior
    pay april 27
    record april one.
 reuter
</text>]