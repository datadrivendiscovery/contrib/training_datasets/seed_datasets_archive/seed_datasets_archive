[<text>
<title>idaho power co &lt;ida&gt; sets quarterly</title>
<dateline>    boise, idaho, march 12 -
    </dateline>qtly div 45 cts vs 45 cts prior
    pay may 20
    record april 24
 reuter
</text>]