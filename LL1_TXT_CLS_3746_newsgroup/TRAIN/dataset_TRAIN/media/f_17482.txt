[<text>
<title>compaq computer &lt;cpq&gt; cuts portable ii prices</title>
<dateline>    houston, june 1 - </dateline>compaq computer corp said it has cut
prices on its portable ii models 2 and 4 to 2,699 dlrs from
2,999 dlrs and to 3,999 dlrs from 4,499 dlrs respectively.
 reuter
</text>]