[<text>
<title>city national corp &lt;ctyn&gt; raises dividend</title>
<dateline>    beverly hills, calif., feb 26 -
    </dateline>shr 16 cts vs 13 cts
    pay april 15
    record march 31
 reuter
</text>]