[<text>
<title>atlantic city electric &lt;ate&gt; debt lowered by s/p</title>
<dateline>    new york, march 6 - </dateline>standard and poor's corp said it cut
the ratings of atlantic city electric co.  about 500 mln dlrs
of long-term debt and 66 mln dlrs of preferred is outstanding.
    s/p said prior ratings reflected anticipation of a
significant improvement in financial condition following the
completion of the hope creek nuclear unit and receipt of
subsequent rate relief. but the new jersey board of public
utilities' recent rate action will not permit enhancement of
key financial indicators.
    ratings lowered include the senior debt to a-plus from
aa-minus and preferred stock to a from aa-minus.
 reuter
</text>]