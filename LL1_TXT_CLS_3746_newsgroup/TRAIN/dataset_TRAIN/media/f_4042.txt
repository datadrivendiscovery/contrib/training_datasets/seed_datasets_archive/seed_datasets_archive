[<text>
<title>japan relaxes rules on securities company outlets</title>
<dateline>    tokyo, march 12 - </dateline>japan has relaxed its limit on the
establishment of securities company outlets in order to service
a growing number of individual investors, the finance ministry
said.
    japanese securities companies can now set up as many as 21
new outlets in the two years before march 31, 1989, against the
previous maximum of 13.
    the rules apply to outlets in department stores,
supermarkets and other locations convenient for individuals.
    foreign securities firms are not affected by the ruling, it
said.
 reuter
</text>]