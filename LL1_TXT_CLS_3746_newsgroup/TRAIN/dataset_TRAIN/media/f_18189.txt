[<text>
<title>k-tron &lt;ktii.o&gt; elects new president</title>
<dateline>    pitman, n.j., june 2 - </dateline>k-tron international inc said its
board elected john c. tucker president, filling a position that
had been vacant since july 30, 1985.
    tucker joined k-tron in 1981 as vice president, chief
financial officer and treasurer. in 1986 he was promoted to
executive vice president and chief operating officer.
 reuter
</text>]