[<text>
<title>cherokee group inc &lt;chke&gt; 1st qtr feb 28 net</title>
<dateline>    north hollywood, calif., march 30 -
    </dateline>shr 22 cts vs 16 cts
    net 2,460,000 vs 1,730,000
    sales 37.0 mln vs 27.3 mln
    note: share adjusted for two-for-one stock split in
february 1987.
 reuter
</text>]