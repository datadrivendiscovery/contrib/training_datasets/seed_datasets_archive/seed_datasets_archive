[<text>
<title>shadowfax &lt;shfxf&gt; sets normal course issuer bid</title>
<dateline>    toronto, march 11 - </dateline>shadowfax resources ltd said it set a
normal course issuer bid to acquire for cancellation up to five
pct of its three mln outstanding common shares on the open
market on the alberta stock exchange, subject to regulatory
approvals.
 reuter
</text>]