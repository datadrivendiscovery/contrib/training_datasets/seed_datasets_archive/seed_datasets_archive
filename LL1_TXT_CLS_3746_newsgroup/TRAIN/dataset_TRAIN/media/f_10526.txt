[<text>
<title>synergy group sells subordinated notes</title>
<dateline>    new york, march 27 - </dateline>synergy group is offering 85 mln dlrs
of senior subordinated notes due 1997 with an 11-5/8 coupon and
par pricing, said sole manager l.f. rothschild, unterberg,
towbin inc.
    non-redeemable for five years, the notes are rated b-3 by
moody's investors service inc and b-plus by standard and poor's
corp.  the issue was increased from an initial offering of 75
mln dlrs.
 reuter
</text>]