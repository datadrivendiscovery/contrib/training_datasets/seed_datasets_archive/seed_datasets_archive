[<text>
<title>consumers power&lt;cms&gt; to redeem preferred stock</title>
<dateline>    jackson, mich., march 12 - </dateline>consumers power co said it will
redeem 200,000 shares of its 3.85 dlr preference stock at 27.50
dlrs a share plus accrued dividends of 0.320833 dlrs a share.
    it said the redemption will be made may one to holders of
record march 23, with shares selected by random lot.
    it said some 100,000 of the shares are being redeemed under
mandatory sinking fund provisions. the balance will be redeemed
at the company's option as part of its efforts to reduce its
cost of capital.
 reuter
</text>]