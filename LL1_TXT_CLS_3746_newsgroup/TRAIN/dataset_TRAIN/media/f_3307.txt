[<text>
<title>faa endorses singer &lt;smf&gt; wind shear simulator</title>
<dateline>    dallas/fort worth airport, texas, march 9 - </dateline>singer co said
its flight simulator designed to train pilots in dealing with
wind shear was endorsed by the federal aviation administration,
faa.
    the wind sheer profile will be available on all of singer's
simuflite training international phase ii jet simulators by
mid-1987, the company said.
    while the company can use the device to train pilots
without the faa evaluation and qualification, it said the faa
act is an endorsement.
 reuter
</text>]