[<text>
<title>genisco technology &lt;ges&gt; unit sets pact</title>
<dateline>    cypress, calif., march 20 - </dateline>genisco technology corp's
genisco peripheral systems unit said it set a pact with
westward technology ltd, based in england, to exchange
technology efforts and grant mutual distribution rights to a
variety of computer graphics products.
    under the pact, westard and genisco will sell both
companies' graphics products in europe and the u.s.
    the two companies will also swap equipment repair
operations and expand their overall sales forces.
 reuter
</text>]