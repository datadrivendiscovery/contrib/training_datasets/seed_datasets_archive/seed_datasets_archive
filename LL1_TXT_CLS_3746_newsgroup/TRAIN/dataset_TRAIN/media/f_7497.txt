[<text>
<title>harmon &lt;hrmn&gt; to buy recycling firm for cash</title>
<dateline>    kansas city, mo., march 19 - </dateline>harmon industries inc said it
signed a letter of intent to acquire for about 3.5 mln dlrs, a
majority of the stock of snp inc, a portland, oregon-based
company which has patents to reprocess used railroad ties into
new ties for resale.
 reuter
</text>]