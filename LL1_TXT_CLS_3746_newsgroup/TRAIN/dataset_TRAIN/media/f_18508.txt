[<text type="unproc">
glaxo has no knowledge of bearish press article london, june 18
- a spokesman for pharmaceuticals giant glaxo holdings plc
&lt;glxo.l&gt; said he had no knowledge of a bearish u.s. press
article which london share dealers said has helped depress the
shares in early trading here.
    glaxo shares fell to a low of 1,725 this morning after
heavy selling in the u.s. overnight on rumours the new england
medical journal had published an article critical of the "zantac"
anti ulcer drug. the shares later rallied to stand 34p down at
1,751.
    dealers said the share price move was also based on profit
taking after yesterday's rise on bullish remarks from chairman
paul girolami and the shares' debut on the tokyo market.
 reuter


</text>]