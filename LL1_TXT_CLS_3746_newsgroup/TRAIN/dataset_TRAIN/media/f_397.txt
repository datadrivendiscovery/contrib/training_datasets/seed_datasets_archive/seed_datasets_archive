[<text>
<title>writers guild of america strikes two networks</title>
<dateline>    new york, march 2 - </dateline>the writers guild of america said its
members have struck the news staffs of cbs inc &lt;cbs&gt; and
capital cities/abc inc &lt;ccb&gt; this morning after negotiations
for a new contract broke down.
    the guild said there had been extensions prior to the
strike deadline this morning, but said the strike was called
after the companies refused to negotiate.
    the guild said the companies failed to put a final offer on
the table, made no money offer at all, and did not deviate
substantially from their original proposals, which, the guild
said, would have gutted the union contract.
    the guild said the networks demanded the right to terminate
employees at will and lay them off without the  arbitration,
and the hiring of temporary employees to replace staffer
employees.
    the guild represents newswriters, editors, desk assistants,
researchers, production assistants, promotion writers and
graphic artists.
    the strike affects unions in new york, chicago, washington
and los angeles. picketing will commence at corporate
headquarters in new york and other locations, the guild said.
 reuter
</text>]