[<text>
<title>turkish central bank sets lira/dollar, dm rates</title>
<dateline>    ankara, april 1 - </dateline>the turkish central bank set a
lira/dollar rate for april 2 of 780.00/783.90 to the dollar,
down from the previous 777.00/780.89. the bank also set a
lira/mark rate of 429.15/431.30 to the mark, up from the
previous 430.50/432.65.
 reuter
</text>]