[<text>
<title>microsoft corp &lt;msft.o&gt; 1st qtr net</title>
<dateline>    redmond, wash., oct 19 -
    </dateline>shr 38 cts vs 29 cts
    net 21.3 mln vs 15.8 mln
    revs 102.6 mln vs 66.8 mln
 reuter
</text>]