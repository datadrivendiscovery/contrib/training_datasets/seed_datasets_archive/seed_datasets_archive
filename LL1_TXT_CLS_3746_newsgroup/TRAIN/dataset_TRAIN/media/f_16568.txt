[<text>
<title>health and rehabilitation &lt;hrp&gt; sets first divi</title>
<dateline>    cambridge, mass., april 13 - </dateline>health and rehabilitation
properties trust said it declared its initial dividend of 55
cts per share for the quarter ending march 31, 1987.
    the dividend is payable may 20 to shareholders of record
april 20, 1987, it said.
    the dividend includes five cts attributable to the period
between dec 23 and 31, 1986, and 50 cts attributable to the
first quarter of 1987, ending march 31, 1987.

 reuter
</text>]