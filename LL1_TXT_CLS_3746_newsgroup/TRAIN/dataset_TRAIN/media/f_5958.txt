[<text>
<title>viceroy resource corp details gold assays</title>
<dateline>    vancouver, british columbia, march 17 - </dateline>viceroy resource
corp said recent drilling on the lesley ann deposit extended
the high-grade mineralization over a width of 600 feet.
    assays ranged from about 0.35 ounces of gold per short ton
over a 150-foot interval at a depth of 350 to 500 feet to about
1.1 ounces of gold per ton over a 65-foot interval at a depth
of 200 to 410 feet.
 reuter
</text>]