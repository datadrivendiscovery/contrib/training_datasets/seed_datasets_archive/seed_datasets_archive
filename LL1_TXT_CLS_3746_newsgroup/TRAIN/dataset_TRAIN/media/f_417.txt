[<text type="unproc">
usda lifts cross-compliance for 1987 crop oats
    repeat from late friday
    washington, feb 27 - the usda said it is lifting the
limited cross-compliance requirement for 1987 crop oats.
    deputy secretary peter myers said the action was being
taken to help alleviate the short supply of oats.
    under limited cross compliance, the plantings of other
program crops on the farm may not exceed the crop acreage bases
of those crops.
    the lifting of the cross-compliance on oats permits the
planting of oats in excess of the oat acreage base without
sacrificing eligibility for other crop program benefits.
    myers said soybean plantings are expected to decrease as a
result of the action on oats, planting of which are expected to
increase by two to three mln acres.
 reuter


</text>]