[<text>
<title>ericsson &lt;ericy.o&gt; to buy rest of spanish unit</title>
<dateline>    new york, april 13 - </dateline>l.m. ericsson of sweden said it agreed
in principle to buy the 49 pct of intelsa, its spanish unit,
that it does not already own from &lt;telefonica&gt;, spain's
telecommunications administration.
    terms of the agreement were not disclosed.
    ericsson said intelsa, which controls about 40 pct of the
spanish telephone switch market, has 2,400 employees and annual
sales of 800 mln crowns, or 117 mln dlrs.
    "the purchase will not affect the close working
relationship between telefonica and intelsa," ericsson said.
 reuter
</text>]