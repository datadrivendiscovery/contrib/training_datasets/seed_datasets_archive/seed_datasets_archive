[<text>
<title>colgate-palmolive co &lt;cl&gt; sets quarterly</title>
<dateline>    new york, march 12 -
    </dateline>qtly div 34 cts vs 34 cts prior
    pay may 15
    record april 24
 reuter
</text>]