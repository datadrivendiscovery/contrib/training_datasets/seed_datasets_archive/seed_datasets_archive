[<text>
<title>pork checkoff refunds less than expected</title>
<dateline>    chicago, march 3 - </dateline>the national pork board announced at the
american pork congress convention in indianapolis that refunds
under the legislative checkoff program are running less than
expected.
    the board oversees collection and distribution of funds
from the checkoff program that was mandated by the 1985 farm
bill. virgil rosendale, a pork producer from illinois and
chairman of the national pork board, said over 2.2 mln dlrs was
collected in january and refunds are running almost nine pct,
considerably less than expected.
    "we believe that this indicates good producer support for
the new checkoff. we're getting good compliance from markets,
from packers and from dealers," rosendale said.
 reuter
</text>]