[<text>
<title>killearn properties inc &lt;kpi&gt; 3rd qtr net</title>
<dateline>    tallahassee, fla., march 17 -</dateline>qtr ends jan 31
    shr 23 cts vs eight cts
    net 309,963 vs 110,356
    revs 2,503,451 vs 1,351,076
    nine mths
    shr 62 cts vs 25 cts
    net 851,776 vs 331,666
    revs 6,739,351 vs 4,107,713
 reuter
</text>]