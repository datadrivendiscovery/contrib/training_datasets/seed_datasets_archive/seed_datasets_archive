[<text>
<title>alitalia spa &lt;azpi.mi&gt; calendar 1986</title>
<dateline>    sanremo, march 22 -
    </dateline>provisional net profit 55 billion lire vs 48 billion
    turnover 3,750 billion vs 3,369 billion.
    note -  official results for alitalia, italy's national
airline which is controlled by the state industrial holding
company (istituto per la ricostruzione industriale -iri), are
expected to be announced at an annual shareholders meeting in
april.
 reuter
</text>]