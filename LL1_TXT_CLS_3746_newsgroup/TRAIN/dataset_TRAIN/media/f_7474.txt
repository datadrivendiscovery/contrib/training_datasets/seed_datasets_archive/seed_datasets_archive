[<text>
<title>goulds pumps inc &lt;guld&gt; sets regular payout</title>
<dateline>    west palm beach, fla., march 19 -
    </dateline>qtly div 19 cts vs 19 cts prior
    pay april 15
    record april three
 reuter
</text>]