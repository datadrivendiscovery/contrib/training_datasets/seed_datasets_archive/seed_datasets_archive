[<text>
<title>lowrance electronics inc &lt;leix&gt; 2nd qtr jan 31</title>
<dateline>    tulsa, okla., march 16 -
    </dateline>shr profit 17 cts vs loss two cts
    net profit 520,000 vs loss 51,000
    sales 11.1 mln vs 6,897,000
    1st half
    shr profit 34 cts vs profit 12 cts
    net profit 951,000 vs profit 320,000
    sales 20.6 mln vs 14.9 mln
 reuter
</text>]