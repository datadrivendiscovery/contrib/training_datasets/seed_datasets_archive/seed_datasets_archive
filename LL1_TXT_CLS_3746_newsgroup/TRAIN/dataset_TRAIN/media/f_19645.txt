[<text>
<title>new generation products makes acquisition</title>
<dateline>    salt lake city, utah, june 29 - </dateline>&lt;new generation products
inc&gt; said it has acquired a 20 pct ownership in &lt;personal
protection technolgies inc&gt;.
    new generation said personal protection is developing a
group of personal care products, which are effective in killing
a variety of viruses and bacteria on contact, for u.s. and
export markets.
 reuter
</text>]