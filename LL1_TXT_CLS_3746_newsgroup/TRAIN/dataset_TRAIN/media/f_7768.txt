[<text>
<title>entex energy development ltd &lt;eed&gt; sets payout</title>
<dateline>    houston, march 20 -
    </dateline>qtly div 15 cts vs 15 cts prior
    pay may 29
    record march 31
                                                    
 reuter
</text>]