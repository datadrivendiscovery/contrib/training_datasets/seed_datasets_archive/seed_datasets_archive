[<text>
<title>ricoh reorganizes u.s. units</title>
<dateline>    west caldwell, n.j., march 26 - </dateline>ricoh corp, the u.s. unit
of japan-based &lt;ricoh ltd&gt;, said it merged ricoh systems inc
into ricoh corp.
    the u.s. unit also purchased ricoh electronics inc from the
parent company.
    the company said the reorganization unites the sales,
research and development, and manufacturing operations of all
the u.s. units.
 reuter
</text>]