[<text>
<title>bristol-myers co &lt;bmy&gt; declares dividend</title>
<dateline>    new york, june 2 - </dateline>bristol-myers co said it declared a
dividend of 35 cts a share on its common, reflecting the
two-for-one stock split approved at the may five annual meeting
of the company.
    the dividend is payable august one to shareholders of
record july three.
 reuter
</text>]