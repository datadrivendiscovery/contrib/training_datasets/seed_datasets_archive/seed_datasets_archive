[<text>
<title>brown transport &lt;btcu&gt; declares first payout</title>
<dateline>    atlanta, march 12 - </dateline>brown transport co inc said its board
declared an initial quarterly dividend of four cts a share,
payable april 10 to stockholders of record march 31.
 reuter
</text>]