[<text>
<title>international seaway trading corp &lt;ins&gt; year</title>
<dateline>    cleveland, ohio, april 1 -
    </dateline>oper shr 64 cts vs 97 cts
    oper net 845,000 vs 1,285,000
    revs 20.0 mln vs 23 mln
    note: 1986 and 1985 oper net excludes gain of 315,000 dlrs
and 585,000 dlrs, respectively, for extraordinary item.
 reuter
</text>]