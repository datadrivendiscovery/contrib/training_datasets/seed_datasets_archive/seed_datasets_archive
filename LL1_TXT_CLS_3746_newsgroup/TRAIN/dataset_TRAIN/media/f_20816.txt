[<text>
<title>payless cashways &lt;pci&gt; sets share buyback plan</title>
<dateline>    kansas city, mo, oct 20 - </dateline>payless cashways inc said it will
repurchase up to one mln of its common shares from time to time
in the open market.
    the company, which has 34.7 mln shares outstanding, called
the stock buyback a good investment at current prices. its
stock closed at 12-1/2, down 1/8, following a 3-3/8 point drop
on monday.
 reuter
</text>]