[<text>
<title>vms strategic &lt;vlans&gt; sets initial dividend</title>
<dateline>    chicago, april 1 - </dateline>vms strategic land trust said it
delcared an initial quarterly cash dividend of 30 cts a share,
payable may 15 to shareholders of record april 20.
    the company also said that effective today it will be
trading on the nasdaq system under the symbol &lt;vlans&gt;.
 reuter
</text>]