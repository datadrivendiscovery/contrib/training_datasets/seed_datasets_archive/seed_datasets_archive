[<text>
<title>iran reports important victories on southern front</title>
<dateline>    london, april 7 - </dateline>iran said it had achieved important
victories against iraq on the southern war fronts last night.
    a brief iranian news agency irna report said "important
victories achieved in southern fronts monday night." it gave no
further details.
    iran launched a major offensive, codenamed karbala-5,
towards the major southern iraqi port of basra in january, but
there have been no reports of heavy fighting in the area in
recent weeks.
 reuter
</text>]