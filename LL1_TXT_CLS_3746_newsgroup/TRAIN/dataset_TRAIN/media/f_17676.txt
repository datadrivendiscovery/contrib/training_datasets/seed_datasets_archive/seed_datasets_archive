[<text>
<title>texas air's &lt;tex&gt; continental in expansion</title>
<dateline>    cleveland, june 1 - </dateline>texas air corp's continental airlines
said it will begin creation of a cleveland hub july one,
doubling the number of cities it serves nonstop from cleveland
and increasing daily flights by 50 pct.
    continental said it will spend about 14 mln dlrs for
facilities expansion in cleveland and will increase its local
employment from 48 to 100 people by july one.
    continental will begin four daily nonstop flights to
washington-national, two daily nonstops to los angeles, and one
daily nonstop each to san francisco, orlando and tampa.
 reuter
</text>]