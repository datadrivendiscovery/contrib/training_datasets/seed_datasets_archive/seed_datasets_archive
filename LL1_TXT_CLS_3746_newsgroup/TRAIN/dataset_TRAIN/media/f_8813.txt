[<text>
<title>great lakes &lt;glk&gt; in venture with johnson &lt;jnj&gt;</title>
<dateline>    west lafayette, ind., march 24 - </dateline>great lakes chemical corp
said it agreed to make an ingredient to be used by mcneil
specialty products co, a subsidiary of johnson and johnson, in
the manufacture of sucralose, a high intensity sweetener.
    sucralose is currently under review by the food and drug
administration.
    great lakes chemical and mcneil specialty will build the
facilities needed for production, it said.
 reuter
</text>]