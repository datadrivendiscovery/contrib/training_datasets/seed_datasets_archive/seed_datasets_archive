[<text>
<title>perkin-elmer &lt;pkn&gt; wins epa contract</title>
<dateline>    norwalk, conn., oct 19 - </dateline>perkin-elmer corp said it won a
contract to provide laboratory information management systems
to the enviromental protection agency's 10 regional
laboratories.
    the value and the exact duration of the contract was not
disclosed.
    the company said the contract will include hardware,
software, installation, support services, and software analyst
consultations.
 reuter
</text>]