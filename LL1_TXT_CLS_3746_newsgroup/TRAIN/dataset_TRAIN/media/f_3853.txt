[<text>
<title>&lt;ranchmen's resources ltd&gt; year loss</title>
<dateline>    calgary, alberta, march 11 -
    </dateline>shr loss seven cts vs loss 3.83 dlrs
    net profit 1,700,000 vs loss 13,900,000
    revs 18.7 mln vs 25.6 mln
    note: current shr after preferred dividends of 2.0 mln dlrs
    prior shr and net includes 34.5 mln dlr writedown on oil
properties
 reuter
</text>]