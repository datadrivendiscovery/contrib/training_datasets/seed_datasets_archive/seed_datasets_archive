[<text>
<title>carlton buys stake in central independent tv</title>
<dateline>    london, march 16 - </dateline>carlton communications plc &lt;ccml.l&gt; said
in a statement it had purchased a 20 pct stake or some 5.1 mln
shares in central independent television from ladbroke group
plc &lt;ladb.l&gt; at 578p per share.
    the consideration of 29.5 mln stg will be met with 18.2 mln
stg in cash and the issue of one million ordinary carlton
shares, it said.
    central showed pretax profits up by 57 pct to 18.8 mln stg
for the year ended 30 september 1986.
 reuter
</text>]