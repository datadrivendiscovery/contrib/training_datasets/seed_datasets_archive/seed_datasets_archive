[<text>
<title>new york futures exchange to relocate</title>
<dateline>    new york, april 3 - </dateline>lewis horowitz, president of the new
york futures exchange, said the new york stock exchange board
yesterday approved in principle an agreement to relocate the
futures exchange at the new york cotton exchange.
    the agreement is subject to the approval of the cotton
exchange board, which meets tuesday, april 7.
 reuter
</text>]