[<text>
<title>cargill u.k. strike ends</title>
<dateline>    london, april 7 - </dateline>cargill u.k. ltd's oilseed processing
plant in seaforth, northwest england, will resume operations
tomorrow morning following settlement of a labour dispute which
has paralysed production there since december 19, a company
spokesman said.
    it is likely to resume deliveries of soymeal and oil within
a couple of days. force majeure had previously been declared
for supplies up to, and including may, but the company will
attempt to fulfill all outstanding contracts, the spokesman
said.
 reuter
</text>]