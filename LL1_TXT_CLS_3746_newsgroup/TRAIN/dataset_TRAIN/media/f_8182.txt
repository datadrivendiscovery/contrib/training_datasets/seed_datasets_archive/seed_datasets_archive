[<text>
<title>u.k. money market shortage forecast revised down</title>
<dateline>    london, march 23 - </dateline>the bank of england said it revised down
its forecast of the deficit in the money market today to 750
mln stg from 800 mln.
 reuter
</text>]