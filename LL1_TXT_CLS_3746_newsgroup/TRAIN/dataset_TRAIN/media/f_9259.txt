[<text>
<title>timminco acquires universal adhesives</title>
<dateline>    toronto, march 25 - </dateline>&lt;timminco ltd&gt; said it acquired
universal adhesives inc, of memphis, for undisclosed terms, in
a move to expand timminco's operations into the united states.
    the company said universal adhesives, with five u.s.
plants, has annual sales of 12 mln u.s. dlrs, which will double
timminco's presence in the north american adhesives market.
    timminco said universal adhesives will complement the
company's canadian-based industrial adhesives division and is a
key step in its long-term goal for expansion in the specialty
chemical field.
 reuter
</text>]