[<text>
<title>allied bankshares &lt;albn&gt; offers stock</title>
<dateline>    thomson, ga., march 18 - </dateline>allied bankshares inc said it
offered 500,000 shares of stock at 12.50 dlrs a share.
    the offering is underwritten by johnson, lane, space, smith
and co inc and interstate securities corp.
 reuter
</text>]