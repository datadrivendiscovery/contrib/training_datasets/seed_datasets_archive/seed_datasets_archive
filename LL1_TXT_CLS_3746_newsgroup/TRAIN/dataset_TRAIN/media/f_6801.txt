[<text>
<title>central pennsylvania financial corp &lt;cpsa&gt;payout</title>
<dateline>    shamokin, pa., march 18 -
    </dateline>qtly div 10 cts vs 10 cts in prior qtr
    payable april 22
    record april 10
 reuter
</text>]