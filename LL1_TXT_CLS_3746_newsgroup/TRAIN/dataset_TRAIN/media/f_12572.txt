[<text>
<title>health images inc &lt;himg&gt; 4th qtr loss</title>
<dateline>    atlanta, april 2 -
    </dateline>shr loss one ct vs loss seven cts
    net profit 108,419 vs loss 241,192
    revs 2,044,882 vs 317,266
    year
    shr loss 18 cts vs loss 23 cts
    net loss 430,027 vs loss 432,982
    revs 5,088,065 vs 416,777
    note: share after preferred dividends.
 reuter
</text>]