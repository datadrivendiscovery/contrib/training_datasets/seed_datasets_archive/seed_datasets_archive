[<text>
<title>turkish central bank sets lira/dollar, dm rates</title>
<dateline>    ankara, april 2 - </dateline>turkey's central bank set a lira/dollar
rate for april 3 of 782.50/786.41 to the dollar, down from
780.00/783.90. it set a lira/d-mark rate of 428.30/430.44 to
the mark, up from 429.15/431.30.
 reuter
</text>]