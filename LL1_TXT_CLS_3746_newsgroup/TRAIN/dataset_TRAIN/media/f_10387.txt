[<text>
<title>macmillan bloedel &lt;mmb&gt; stock split approved</title>
<dateline>    vancouver, british columbia, march 27 - </dateline>macmillan bloedel
ltd said shareholders authorized a previously announced
three-for-one stock split, applicable to holders of record
april nine.
    the company said its stock will begin trading on a split
basis on april 3, subject to regulatory approvals.
 reuter
</text>]