[<text>
<title>computer horizons &lt;chrz&gt; in acquisition</title>
<dateline>    new york, march 4 - </dateline>computer horizons corp said it
purchased computerknowledge inc, a software training education
company headquartered in dallas.
    terms were not disclosed.
                
 reuter
</text>]