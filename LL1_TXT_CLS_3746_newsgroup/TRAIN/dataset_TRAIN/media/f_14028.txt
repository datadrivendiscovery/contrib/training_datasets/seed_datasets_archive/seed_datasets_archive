[<text>
<title>american sports advisors &lt;piks&gt; to liquidate</title>
<dateline>    carfle place, n.y., april 7 - </dateline>american sports advisors inc
said it has agreed to sell its sports handicapping and
publication business to professor enterprises for about
1,650,000 dlrs and intends to liquidate after closing.
    the transaction is subject to shareholder approval.
    professor is owned by american sports president edward c.
horowitz and mike warren, it said.
 reuter
</text>]