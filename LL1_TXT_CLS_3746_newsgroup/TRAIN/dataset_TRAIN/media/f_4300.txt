[<text>
<title>colorocs &lt;clrx&gt; signs pact with sharp</title>
<dateline>    norcross, ga., march 12 - </dateline>colorocs corp said it signed a
manufacturing agreement with sharp corp &lt;shrp&gt; of japan for
sharp to build colorocs' full-color copier.
    no other details of the agreement were disclosed, but the
company said more details would follow shortly.
 reuter
</text>]