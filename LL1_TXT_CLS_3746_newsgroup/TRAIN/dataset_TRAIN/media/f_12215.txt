[<text>
<title>magma lowers copper price 0.25 ct to 65.50 cts</title>
<dateline>    new york, april 1 - </dateline>magma copper co, a subsidiary of
newmont mining corp, said it is lowering its copper cathode
price by 0.25 cent to 65.50 cents a lb, effective immediately.
 reuter
</text>]