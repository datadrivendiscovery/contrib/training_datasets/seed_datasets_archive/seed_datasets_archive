[<text>
<title>india and japan to discuss iron ore prices</title>
<dateline>    new delhi, march 5 - </dateline>the state-owned minerals and metals
trading corp will send a team to japan next week to negotiate
an iron ore export contract for 1987/88 beginning april 1,
trade sources said.
    japan, the biggest buyer of indian iron ore with imports of
around 23 mln tonnes a year, has asked india to reduce prices
from the current average of 18 dlrs a tonne, the sources said.
    "japan has said it may be forced to reduce ore imports from
india next year if new delhi fails to reduce the price," one
source said, but declined to give further details.
 reuter
</text>]