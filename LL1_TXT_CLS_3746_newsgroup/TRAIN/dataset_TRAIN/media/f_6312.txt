[<text>
<title>d/p selected utilities &lt;dnp&gt; hikes payouts</title>
<dateline>    chicago, march 17 - </dateline>duff and phelps selected utilities inc
said it approved payment of higher monthly dividends.
    the closed end-investment company approved payment of 4.5
cts on april 10, record march 31; of five cts, payable may 11,
record april 30; and of 5.5 cts, payable june 10, record may
29.
    duff and phelps first monthly dividend of four cts was paid
on march 10, it noted.
 reuter
</text>]