[<text>
<title>&lt;european american bancorp&gt; 3rd qtr net</title>
<dateline>    new york, oct 19 -
    </dateline>net 13,185,000 vs 6,715,000
    nine mths
    net 26.2 mln vs 15.3 mln
    note: company is owned by a consortium of european banks.
 reuter
</text>]