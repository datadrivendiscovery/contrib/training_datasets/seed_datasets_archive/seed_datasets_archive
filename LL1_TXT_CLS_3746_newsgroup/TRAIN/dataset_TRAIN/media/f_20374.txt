[<text>
<title>united service advisors inc &lt;usvsp.o&gt; 1st qtr</title>
<dateline>    san antonio, texas, oct 20 -</dateline>qtr ends sept 30
    shr profit seven cts vs loss two cts
    net profit 228,691 vs loss 54,115
    revs 2,415,419 vs 1,389,579
    avg shrs 3,056,787 vs 2,933,058
 reuter
</text>]