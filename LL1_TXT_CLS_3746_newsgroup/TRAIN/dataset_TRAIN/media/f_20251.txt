[<text>
<title>kelsey-hayes canada ltd &lt;kel.to&gt; nine mths net</title>
<dateline>    windsor, ontario, oct 20 -
    </dateline>shr 44 cts vs 1.23 dlrs
    net 2,889,010 vs 8,105,462
    sales 105.8 mln vs 119.6 mln
 reuter
</text>]