[<text>
<title>queensland government development has cp program</title>
<dateline>    london, march 18 - </dateline>the queensland government development
authority is launching a 300 mln u.s. dlr euro-commercial paper
program, morgan guaranty ltd said as one of the dealers.
    the other dealer is swiss bank corp international ltd and
issuing and paying agent is morgan guaranty trust co of new
york's london office.
    the program is guaranteed by the government of queensland.
paper will be issued in denominations of 500,000 dlrs and will
have maturities between seven and 365 days.
 reuter
</text>]