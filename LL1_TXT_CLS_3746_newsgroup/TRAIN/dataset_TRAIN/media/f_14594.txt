[<text>
<title>itt &lt;itt&gt; to buy back common, preferred issues</title>
<dateline>    new york, april 7 - </dateline>itt corp said its board authorized the
repurchase of up to 10 mln shares of its common and preferred
stock as market conditions warrant.
    the company said the repurchased shares will be held as
treasury shares.
    an itt spokesman said the board approved the proposed stock
buy back as part of its program to enhance shareholder value
and increase itt's per-share earnings.
    he said itt would fund the purchases with a portion of the
1.3 billion dlrs in cash it received from selling a majority
interest in its telecommunications operations to &lt;compagnie
generale d'electricite&gt; of france.
 reuter
</text>]