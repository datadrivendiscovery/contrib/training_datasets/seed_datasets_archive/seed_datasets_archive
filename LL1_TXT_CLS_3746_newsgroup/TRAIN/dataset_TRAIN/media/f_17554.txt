[<text>
<title>berkshire gas co &lt;bgas.o&gt; raises payout</title>
<dateline>    pittsfield, mass., june 1 -
    </dateline>qtly div 30-1/2 cts vs 28-1/2 cts prior
    pay july 15
    record june 30
 reuter
</text>]