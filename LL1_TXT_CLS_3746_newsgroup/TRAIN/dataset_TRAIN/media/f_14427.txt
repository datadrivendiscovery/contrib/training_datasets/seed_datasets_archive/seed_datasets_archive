[<text>
<title>green tree acceptance inc &lt;gnt&gt; 1st qtr net</title>
<dateline>    st. paul, april 7 -
    </dateline>shr 50 cts vs 40 cts
    net 9,421,000 vs 9,312,000
    rev 37.7 mln vs 36.3 mln
    avg shrs 17,049,920 vs 21,173,570
    note: per-share results adjusted for two-for-one common
stock split in june 1986
 reuter
</text>]