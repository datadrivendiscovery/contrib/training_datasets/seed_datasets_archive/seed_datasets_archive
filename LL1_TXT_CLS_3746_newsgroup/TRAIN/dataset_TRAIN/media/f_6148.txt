[<text>
<title>masscomp &lt;mscp&gt;, vi in deal</title>
<dateline>    westford, mass, march 17 - </dateline>masscomp said it reached a joint
marketing relationship to promote and sell v.i. corp's line of
dataview s graphics software products.
    masscomp said it will promote the products to key
government and commercial accounts.
 reuter
</text>]