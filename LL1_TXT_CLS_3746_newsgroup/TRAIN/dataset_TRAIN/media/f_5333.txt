[<text>
<title>hog and cattle slaughter guesstimates</title>
<dateline>    chicago, march 16 - </dateline>chicago mercantile exchange floor
traders and commission house representatives are guesstimating
today's hog slaughter at about 295,000 to 302,000 head versus
293,000 week ago and 309,000 a year ago.
    cattle slaughter is guesstimated at about 123,000 to
126,000 head versus 123,000 week ago and 121,000 a year ago.
 reuter
</text>]