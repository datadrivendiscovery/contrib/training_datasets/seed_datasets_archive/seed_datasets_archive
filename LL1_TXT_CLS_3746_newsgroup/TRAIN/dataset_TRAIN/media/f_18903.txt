[<text>
<title>ge &lt;ge&gt; signs contract on small power plant</title>
<dateline>    schenectady, n.y., june 18 - </dateline>general electric said it
signed a 21 mln contract to build a 12.5 megawatt power plant,
fueled by agricultural waste, for el nido biomass power plant
associates.
    ge said pacific gas and electric co will purchase the net
electric output of the el nido, calif. plant, which is
scheduled for completion in mid-1989.
    ge said the fuel system will be capable of firing up to six
different types of agricultural waste, and will primarily burn
almond prunings, cotton stalks and rice straw, as well as other
well as otehr alternate agricultural waste.
   
 reuter
</text>]