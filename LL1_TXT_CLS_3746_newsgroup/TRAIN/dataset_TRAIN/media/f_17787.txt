[<text>
<title>kingsbridge, masco set merger</title>
<dateline>    new york, june 1 - </dateline>kingsbridge holdings ltd, said it signed
a letter of intent for a merger with &lt;masco sports inc&gt;.
    the transaction calls for 230 mln sahres of kingsbridge
common stock to be issued to shareholders of masco.

 reuter
</text>]