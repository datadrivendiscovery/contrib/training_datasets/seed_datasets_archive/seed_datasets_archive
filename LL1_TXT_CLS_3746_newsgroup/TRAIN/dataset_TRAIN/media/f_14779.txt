[<text>
<title>bank of japan intervenes soon after tokyo opening</title>
<dateline>    tokyo, april 8 - </dateline>the bank of japan bought a small amount of
dollars shortly after the opening at around 145.30 yen, dealers
said.
    the central bank intervened as a medium-sized trading house
sold dollars, putting pressure on the u.s. currency, they said.
    the dollar was also supported by a major electrical
consumer goods company, which was a speculative dollar buyer at
around 145.25 yen, they added.
    the dollar opened at 145.33 yen against 145.60/70 in new
york and 145.25 at the close here yesterday.
 reuter
</text>]