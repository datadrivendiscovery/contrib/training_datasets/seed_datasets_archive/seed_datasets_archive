[<text>
<title>liberty all-star equity fund initial div</title>
<dateline>    westport, conn., feb 26 -
    </dateline>qtly div five cts vs n.a.
    payable april two
    record march 20
    note:1986 dividend includes special two cts per share for
the period beginning with the fund's commencement of operations
on novebmer three through december 31, 1986.
 reuter
</text>]