[<text>
<title>burr-brown &lt;bbrc&gt; sees lower 1st qtr earnings</title>
<dateline>    tucson, ariz., march 26 - </dateline>burr-brown corp said its first
quarter 1987 results will show profits significantly below the
1,058,000 dlrs, or 11 cts per share, earned in the first
quarter last year.
    the company said the profit decline will be the result of
an increase in reserves for inventory valuation. the increase
will be to cover potential write-downs of certain inventories
or products used in compact-disc stereo systems.
    burr-brown said the possible write-down is being
precipitated by a shift in market demand toward higher
performance products.
 reuter
</text>]