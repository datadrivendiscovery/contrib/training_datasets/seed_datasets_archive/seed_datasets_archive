[<text>
<title>chrysler &lt;c&gt; to put four plants on overtime</title>
<dateline>    detroit, march 12 - </dateline>chrysler corp's chrysler motors unit
said it will put four of its u.s. assembly plants on overtime
during the week of march 16.
    three of the plants will also operate saturday, march 14,
beyond the normal five-day work week.
    chrysler facilities at newark, del., sterling heights,
mich., warren, mich., and the jefferson plant in detroit will
work overtime hours during the week. newark, sterling heights
and jefferson will work saturday.
 reuter
</text>]