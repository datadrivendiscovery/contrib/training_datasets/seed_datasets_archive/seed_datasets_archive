[<text>
<title>tultex &lt;ttx&gt; increases dividend</title>
<dateline>    martinsville, va., march 19 -
    </dateline>qtly div nine cts vs eight cts prior
    payable july one
    record june 12
 reuter
</text>]