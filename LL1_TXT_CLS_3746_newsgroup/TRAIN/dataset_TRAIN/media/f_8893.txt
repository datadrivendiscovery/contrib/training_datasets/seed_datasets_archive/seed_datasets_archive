[<text>
<title>scientific measurement systems inc &lt;scms&gt; net</title>
<dateline>    austin, texas, march 24 - </dateline>2nd qtr jan 31
    shr loss three cts vs loss seven cts
    net loss 352,000 vs loss 568,000
    revs 636,000 vs 640,000
    avg shrs 12.7 mln vs 8,377,000
    1st half
    shr loss six cts vs loss 10 cts
    net loss 594,000 vs loss 865,000
    revs 1,245,000 vs 1,063,000
    avg shrs 10.5 mln vs 8,333,000
 reuter
</text>]