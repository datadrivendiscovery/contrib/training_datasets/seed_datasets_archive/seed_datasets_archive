[<text>
<title>grumman&lt;gq&gt; unit gets 303.9 mln dlr sdi contract</title>
<dateline>    washington, march 18 - </dateline>the grumman space systems division
of grumman corp is being awarded a 303.9 mln dlr air force
contract for work related to the strategic defense initiative
(sdi), the defense department said.
    it said the contract, similar to one awarded to lockheed
corp, is for a 34 month sdi boost surveillance and tracking
system demonstration/validation effort which is expected to be
completed in january 1990.
    the department said 34.3 mln dlrs of the contract funds
will expire at the end of the current fiscal year. it added
that 38 bids for the work were solicited and three proposals
were received.
 reuter
</text>]