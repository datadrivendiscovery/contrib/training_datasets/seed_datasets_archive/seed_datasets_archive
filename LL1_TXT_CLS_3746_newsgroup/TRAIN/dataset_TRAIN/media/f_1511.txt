[<text>
<title>general datacomm &lt;gdc&gt; in sale/leaseback deal</title>
<dateline>    middlebury, conn., march 4 - </dateline>general datacomm industries
inc said it has agreed to sell for 40 mln dlrs its network
services facility and its corporate facility and lease back the
facilities.
    it said it has an option to buy the buildings back at a
later date.  proceeds from the financing will be used to reduce
debt, it said.
 reuter
</text>]