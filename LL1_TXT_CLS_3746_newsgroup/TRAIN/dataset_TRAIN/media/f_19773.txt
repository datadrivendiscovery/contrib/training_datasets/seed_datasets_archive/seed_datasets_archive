[<text>
<title>ssmc &lt;ssm&gt; to buy cutters exchange division</title>
<dateline>    stamford, conn, june 29 - </dateline>ssmc inc said it has executed a
letter of understanding to acquire the parts catalog division
of &lt;cutters exchange inc&gt; for an undisclosed amount.
    ssmc, spun off from the singer co &lt;smf&gt; a year ago, said
that the parts catalog division wholesales parts and needles to
the industrial sewing trade in the u.s.
    under the agreement, key managers and employees of the
parts catalog division in nashville, tenn, will relocate to the
ssmc facility in murfreesboro, ssmc said.
 reuter
</text>]