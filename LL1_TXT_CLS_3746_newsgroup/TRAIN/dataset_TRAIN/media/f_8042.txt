[<text>
<title>louisiana land &lt;llx&gt; assumes inexco's debt</title>
<dateline>    new orleans, march 20 - </dateline>louisiana land and exploration co
said it assumed the obligation to pay the principal and
interest on the 8-1/2 pct convertible subordinated debentures
due september 1, 2000 of inexco oil co, a unit of louisiana
land.
    effective march 23, the debentures will be listed as the
debentures of louisiana land, llxoo.
 reuter
</text>]