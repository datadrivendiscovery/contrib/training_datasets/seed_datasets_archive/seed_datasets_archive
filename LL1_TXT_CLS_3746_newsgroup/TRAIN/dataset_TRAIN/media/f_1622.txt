[<text>
<title>first savings bank florida &lt;fsbf&gt; sets meeting</title>
<dateline>    tarpon springs, fla., march 4 - </dateline>first savings bank of
florida said it expects a special shareholder meeting to be
held around may 21 to consider the proposed merger into
gibraltar financial corp &lt;gfc&gt;.
    it said the annual meeting will be held april 30 to elect
two directors and ratify the appointment of auditors.
 reuter
</text>]