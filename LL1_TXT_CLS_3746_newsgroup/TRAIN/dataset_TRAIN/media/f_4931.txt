[<text>
<title>chrysler &lt;c&gt; canada sets more cash rebates</title>
<dateline>    toronto, march 13 - </dateline>chrysler canada ltd, wholly owned by
chrsyler corp, said it added some car and truck models to the
company's cash rebate program and discontinued the models from
its 3.9 pct financing plan.
    chrysler canada said effective march 16 it will offer a 750
dlr cash rebate on 1986 and 1987 dodge aries and plymouth
reliant sales. it will also offer a 500 dlr rebate to buyers of
1986 and 1987 tursimo and charger cars as well as ram 50 and
power ram 50 trucks.
    the financing plan on the models will be discontinued march
15.
 reuter
</text>]