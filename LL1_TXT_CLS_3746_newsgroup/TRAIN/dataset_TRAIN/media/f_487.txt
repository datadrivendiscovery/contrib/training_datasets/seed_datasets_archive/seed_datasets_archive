[<text>
<title>lotus &lt;lots&gt; introduces new software</title>
<dateline>    cambridge, mass., march 2 - </dateline>lotus development corp said it
has unveiled a new software product, named galaxy, to
complement the newly introduced apple computer inc &lt;aapl&gt;
macintosh ii and macintosh se.
    lotus said galaxy will be formally introduced over the
summer.
    lotus said galaxy will include command language and
dynamically linked modules, unlike any other software product
currently available for the macintosh product family, which
enables the user to execute a series of commands with a single
learned keystroke.
                
 reuter
</text>]