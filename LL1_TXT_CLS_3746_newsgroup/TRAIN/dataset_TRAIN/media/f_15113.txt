[<text>
<title>avx &lt;avx&gt; files to offer convertible debentures</title>
<dateline>    new york, april 8 - </dateline>avx corp said it filed with the
securities and exchange commission a registration statement
covering a 75 mln dlr issue of convertible subordinated
debentures.
    the company named shearson lehman brothers inc as manager
of the offering.
 reuter
</text>]