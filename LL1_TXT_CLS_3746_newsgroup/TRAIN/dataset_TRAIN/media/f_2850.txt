[<text>
<title>coradian corp &lt;cdin&gt; year net</title>
<dateline>    latham, n.y., march 6 -
    </dateline>shr profit one cent vs loss 37 cts
    net profit 148,000 dlrs vs loss 1,686,000
    revs 11.4 mln vs 10.9 mln
    note: company said net is before extraordinary items and
taxes and declined to provide data on those items
 reuter
</text>]