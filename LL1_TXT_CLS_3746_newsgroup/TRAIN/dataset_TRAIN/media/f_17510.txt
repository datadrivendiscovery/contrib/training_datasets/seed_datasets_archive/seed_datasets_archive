[<text>
<title>canada southern &lt;csw.to&gt; to offer shares</title>
<dateline>    calgary, june 1 - </dateline>canada southern petroleum ltd said its
board has authorized an offering of about three mln shares of
limited voting stock to shareholders of record on june 23.
    the company said the rights offering will expire august
three.  the rights are nontransferable.
    it said shareholders will be entitled to buy one new share
for every three held at one dlr u.s. or 1.34 dlrs canadian.
shareholders subscribing for their entire allotments will have
the option of subscribing for shares not bought by others on a
contingent allotment basis in any amount up to 100 pct of their
original allotments.
 reuter
</text>]