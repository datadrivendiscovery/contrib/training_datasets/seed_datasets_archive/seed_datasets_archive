[<text>
<title>kinder-care inc &lt;kndr&gt; first qtr net</title>
<dateline>    montgomery, ala., march 18 - </dateline>qtr ends dec 31
    shr one cent vs 15 cts
    net 466,000 vs 6,866,000
    revs 123.1 mln vs 93.5 mln
    note: the company changed its fiscal year end from aug 31
to dec 31. qtr prior ended jan 17, 1986 and included two more
weeks than current qtr.
    current qtr includes loss 899,000 for accounting change.
 reuter
</text>]