[<text>
<title>nashua &lt;nsh&gt; to purchase private disc maker</title>
<dateline>    nashua, n.h., march 24 - </dateline>nashua corp said it signed a
letter of intent to purchase &lt;lin data corp&gt;, a private
manufacturer of high-capacity rigid discs for storage of
computer data.
    under the terms of the letter, nashua said it will acquire
all classes of lin stock for 24 mln dlrs. in addition, it said
it will loan lin 1,200,000 dlrs to support its operations.
    the closing of the sale is set for the second quarter of
1987, the company said.
 reuter
</text>]