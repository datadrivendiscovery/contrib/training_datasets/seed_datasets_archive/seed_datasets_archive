[<text>
<title>empire &lt;empa.to&gt; to buy sobeys &lt;sysa.to&gt; stock</title>
<dateline>    toronto, april 13 - </dateline>empire co ltd said it will acquire all
shares of sobeys stores ltd it does not already own under an
arrangement approved by directors of both companies.
    holders of sobeys class a non-voting shares and class b
common shares may elect to receive either 1.6 non-voting class
a empire shares or one non-voting class a empire share and
one-half of an eight pct redeemable retractable preferred
share. the preferred share has a par value of 25 dlrs and is
retractable at the holder's option on may 1, 1994.

 reuter
</text>]