[<text>
<title>japanese crushers buy canadian rapeseed</title>
<dateline>    winnipeg, april 2 - </dateline>japanese crushers bought 2,000 tonnes
of canadian rapeseed in export business overnight for may
shipment, trade sources said.
    they also reported rumors that mexico may have purchased a
sizeable quantity of canadian flaxseed, although no price or
shipment details were available.
 reuter
</text>]