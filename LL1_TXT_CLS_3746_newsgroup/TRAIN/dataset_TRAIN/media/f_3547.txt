[<text>
<title>new world pictures &lt;nwp&gt; sells taft &lt;tfb&gt; stake</title>
<dateline>    los angeles, march 11 - </dateline>new world pictures ltd said it sold
456,900 shares or about five pct of taft broadcasting co common
stock for a gain of 17.8 mln dlrs.
    the company said in a brief statement that it acquired the
stock in late 1986.  it gave no further details and company
officials were not immediately available for comment.
    on friday, taft vice chairman dudley s. taft and
narragansett capital inc &lt;narr&gt; offered to acquire taft for 145
dlrs per share.  dudley taft and his family have owned 12 pct
of the company.
    an investment group leds by robert m. bass, one of the bass
brothers of fort worth, texas, has been reported as owning
about 25 pct of taft stock, and &lt;american financial corp&gt;
chairman carl lindner has been reported to own about 16 pct. 
both bass and linder have acquired taft shares in recent months.
 reuter
</text>]