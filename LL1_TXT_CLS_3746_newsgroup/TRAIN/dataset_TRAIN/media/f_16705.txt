[<text>
<title>isc systems &lt;iscs.o&gt; 3rd qtr march 27 net</title>
<dateline>    spokane, wash., april 13 -
    </dateline>shr seven cts vs 24 cts
    net 1,114,000 vs 3,676,000
    revs 43.6 mln vs 41.2 mln
    nine mths
    shr 25 cts vs 64 cts
    net 3,952,000 vs 9,614,000
    revs 118.6 mln vs 119.9 mln
 reuter
</text>]