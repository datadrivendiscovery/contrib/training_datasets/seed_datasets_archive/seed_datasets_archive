[<text>
<title>infinite graphics &lt;infg.o&gt; in joint veenture</title>
<dateline>    minneapolis, june 29 - </dateline>infinite graphics inc said it signed
a letter of intent to create a joint venture with &lt;computer
design equipment co&gt; to expand sales of its computer-aided
design equipment in the midwest.
    infinite graphics said it will manage and hold the
controlling interest in the venture, national cadd-pro upper
midwest.
    computer design has headquarters in syracuse, ind.
 reuter
</text>]