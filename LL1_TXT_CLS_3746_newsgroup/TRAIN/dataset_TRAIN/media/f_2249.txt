[<text>
<title>corrected - pegasus gold inc&lt;pgulf&gt; 4th qtr net</title>
<dateline>    vancouver, british columbia, march 5 -
    </dateline>shr profit 20 cts vs loss two cts
    net profit 2,665,000 vs loss 202,000
    revs 12,141,000 vs 5,993,000
    year
    shr profit 35 cts vs loss 11 cts
    net profit 4,653,000 vs loss 1,167,000
    revs 35.1 mln vs 18.0 mln
    note: company corrects reporting period to 4th qtr and year
from 3rd qtr and nine mths
 reuter
</text>]