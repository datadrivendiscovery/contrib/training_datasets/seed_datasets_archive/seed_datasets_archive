[<text>
<title>bic corp &lt;bic&gt; sets quarterly</title>
<dateline>    milford, conn., march 31 -
    </dateline>qtly div 15 cts vs 15 cts prior
    pay april 30
    record april 14
 reuter
</text>]