[<text>
<title>general motors &lt;gm&gt; gets 97 mln dlr contract</title>
<dateline>    washington, april 13 - </dateline>general motors corp is being awarded
a 97 mln dlr air force contract for 176 turboprop engines in
three separate designs, the defense department said.
    it said the contract, which combines purchases for the u.s.
navy, the u.s. air force reserves, japan and norway under the
foreign military sales program, is expected to be completed in
december 1987.
 reuter
</text>]