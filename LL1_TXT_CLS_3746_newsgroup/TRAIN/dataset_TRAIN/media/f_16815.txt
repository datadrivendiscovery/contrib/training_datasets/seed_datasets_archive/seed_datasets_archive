[<text>
<title>endata inc &lt;data.o&gt; 1st qtr net</title>
<dateline>    nashville, tenn., april 17 -
    </dateline>oper shr 16 cts vs 11 cts
    oper net 660,000 vs 447,000
    revs 9,936,000 vs 9,005,000
    note: 1986 net excludes 381,000 dlr tax credit.
 reuter
</text>]