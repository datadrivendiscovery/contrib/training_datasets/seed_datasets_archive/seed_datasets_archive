[<text>
<title>ual &lt;ual&gt; unit attacked on minority hiring</title>
<dateline>    chicago, march 2 - </dateline>ual inc's united airlines was accused in
a congressional hearing today of locking blacks out of key
jobs, but the company said it had made tremendous progress in
minority hiring.
    the issue was aired during a hearing of a house government
operations subcommittee whose chairman, rep. cardiss collins,
called the treatment of minorities by the country's largest
airline "pathetic."
    "it strikes me odd," said the illinois democrat, "that the
number of white women pilots is more than double that of black
pilots, and that white women are more fairly represented in
upper management."
    united has been under a court order since 1976 to increase
its minority employment.
    david pringle, united's senior vice president for human
resources, said "we take a very aggressive approach to minority
hiring and have made tremendous progress ... we will continue
to pursue even wider goals."
 reuter
</text>]