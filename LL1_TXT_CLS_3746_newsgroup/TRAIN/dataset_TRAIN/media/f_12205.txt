[<text>
<title>itel &lt;itel&gt; completes flexi-van acquisition</title>
<dateline>    chicago, april 1 - </dateline>itel corp said it completed the
previously announced purchase of the container fleet and
certain related assets of flexi-van leasing inc for about 130
mln dlrs cash and marketable securities, 30 mln dlrs in notes,
three mln shares of newly issued itel common and assumption of
certain liabilities.
    the company said it obtained financing from a syndicate of
major banks for 150 mln dlrs.
 reuter
</text>]