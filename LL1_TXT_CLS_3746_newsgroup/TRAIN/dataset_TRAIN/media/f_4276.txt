[<text>
<title>modern controls inc &lt;mocon&gt; 4th qtr net</title>
<dateline>    minneapolis, march 12 -
    </dateline>shr 10 cts vs 11 cts
    net 226,000 vs 236,000
    sales 1.3 mln vs 1.5 mln
    year
    shr 38 cts vs 45 cts
    net 819,000 vs 1,001,000
    sales 5.8 mln vs 6.4 mln
 reuter
</text>]