[<text>
<title>lyphomed &lt;lmed&gt; to market antibiotic</title>
<dateline>    rosemont, ill., march 18 - </dateline>lyphomed inc said the food and
drug administration gave it approval to market vancomycin hcl,
an antibiotic used in hospitals to treat life-threatening
infections.
    it said the product is currently marketed by eli lilly and
co &lt;lly&gt; under the trade name vancocin. sales in 1986 were 110
mln dlrs.
    lyphomed said its objective is to get a 15 to 20 pct share
of the market.
 reuter
</text>]