[<text>
<title>&lt;four seasons hotels inc&gt; year net</title>
<dateline>    toronto, march 5 -
    </dateline>oper shr 99 cts vs 54 cts
    oper net 9,515,000 vs 3,449,000
    revs 509.3 mln vs 440.5 mln
    note: 1985 net excludes extraordinary gain of 1.2 mln dlrs.
 reuter
</text>]