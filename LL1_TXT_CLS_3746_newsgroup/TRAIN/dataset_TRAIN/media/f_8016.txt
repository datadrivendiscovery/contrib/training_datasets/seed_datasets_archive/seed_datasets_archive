[<text>
<title>montgomery street income &lt;mts&gt; monthly dividend</title>
<dateline>    san francisco, march 20 -
    </dateline>mthly div 15 cts vs 15 cts
    pay april 15
    record april 1
 reuter
</text>]