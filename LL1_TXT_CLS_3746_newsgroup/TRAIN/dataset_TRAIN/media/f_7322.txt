[<text>
<title>dresser industries &lt;di&gt; sees return to profit</title>
<dateline>    dallas, march 19 - </dateline>dresser industries inc said it expects
the joint ventures it has entered into and a gradual
improvement in the energy market to allow it to regain
profitability before the end of the current year.
    dresser earned 9,600,000 dlrs for the year ended october 31
-- after a 95.0 mln dlr gain from a change in accounting and
pension plan curtailment and a 25.3 mln dlr writedown of
oilfield assets.
 reuter
</text>]