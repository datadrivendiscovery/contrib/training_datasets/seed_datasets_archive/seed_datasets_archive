[<text>
<title>byrd says reagan may win highway veto fight</title>
<dateline>    washington, march 27 - </dateline>senate majority leader robert byrd
said president reagan may win his latest veto battle with
congress over a nearly 88 billion highway construction bill.
    the west virginia democrat said it would be "tough" for the
senate to override the veto. reagan is expected to announce he
is rejecting the measure later today.
    byrd said it would be difficult for the senate to overcome
a white house public relations campaign against the bill.
    the house, voting first, is expected to override it, house
republican leader bob michel has said. it requires two houses
to override a veto by two-thirds majorities.
 reuter
</text>]