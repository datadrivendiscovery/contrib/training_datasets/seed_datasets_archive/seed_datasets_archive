[<text>
<title>prime computer inc &lt;prm&gt; 3rd qtr sept 28</title>
<dateline>    natick, mass., oct 20 -
    </dateline>shr 32 cts vs 25 cts
    net 15.9 mln vs 12.1 mln
    revs 236.2 mln vs 221.9 mln
    nine mths
    shr 88 cts vs 68 cts
    net 43.5 mln vs 32.8 mln
    revs 693.9 mln vs 629.2 mln
 reuter
</text>]