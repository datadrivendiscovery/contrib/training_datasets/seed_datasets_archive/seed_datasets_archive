[<text>
<title>general computer corp &lt;gccc&gt; 3rd qtr feb 28 net</title>
<dateline>    twinsburg, ohio, march 25 -
    </dateline>shr 10 cts vs 20 cts
    net 146,000 vs 230,000
    revs 3,766,000 vs 3,271,000
    avg shrs 1,458,000 vs 1,125,000
    nine mths
    shr 15 cts vs 58 cts
    net 212,000 vs 653,000
    revs 10.6 mln vs 9,561,000
    avg shrs 1,458,000 vs 1,125,000
 reuter
</text>]