[<text>
<title>chrysler &lt;c&gt; canada february car sales fall</title>
<dateline>    windsor, ontario, march 4 - </dateline>chrysler canada ltd, wholly
owned by chrysler corp, said february car sales fell to 9,640
units from year-earlier 11,967 units.
    chrysler canada said year-to-date car sales fell to 18,094
units from 22,073 units in the same period last year.
 reuter
</text>]