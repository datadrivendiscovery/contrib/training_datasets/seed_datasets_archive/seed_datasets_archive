[<text>
<title>amity bancorp inc &lt;amty.o&gt; semi-annual div</title>
<dateline>    new haven, conn - </dateline>june 19
    semi-annual div eight cts vs 15 cts prior
    pay aug 1
    record july 1
    note: before the establishment of the amity bancorp inc
holding company, amity bank declared a full year dividend of 15
cts for the year ended december 31, 1986.
 reuter
</text>]