[<text>
<title>advanced institutional &lt;aims.o&gt; in new facility</title>
<dateline>    syosset, n.y., april 13 - </dateline>advanced institutional management
software inc said it renegotiated its bank credit facility from
a demand note to a 10-year term note with a 25-year payment
schedule.
 reuter
</text>]