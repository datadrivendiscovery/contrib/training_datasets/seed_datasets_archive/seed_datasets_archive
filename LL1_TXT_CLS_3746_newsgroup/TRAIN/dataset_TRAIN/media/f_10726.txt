[<text>
<title>toray industries names new president</title>
<dateline>    tokyo, march 30 - </dateline>toray industries inc &lt;tort.t&gt;, japan's
top synthetic fibre maker, named managing director katsunosuke
maeda as president, effective april 16.
    current president yoshikazu ito will become chairman.
 reuter
</text>]