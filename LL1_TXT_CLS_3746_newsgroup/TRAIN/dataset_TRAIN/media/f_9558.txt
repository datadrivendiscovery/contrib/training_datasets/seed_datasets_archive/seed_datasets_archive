[<text>
<title>ampal-american israel corp &lt;ais.a&gt; year net</title>
<dateline>    new york, march 25 -
    </dateline>shr 27 cts vs 25 cts
    net 6,416,000 vs 5,988,000
    revs 112.2 mln vs 99.8 mln
    note: 1985 includes extraordinary income of 647,000 dlrs or
three cts/shr. 1985 restated.
 reuter
</text>]