[<text>
<title>reagan says u.s. still probing acid rain problem</title>
<dateline>    washington, march 19 - </dateline>president reagan said the u.s.
government is still looking into the problem of acid rain.
    "we're still investigating this," he told a nationally
televised news conference.
    reagan added that the government will now work with u.s.
industry to help address the problem.
    however, he declined to endorse the writing of federal
standards for emissions that would help alleviate the problem.
    the reagan administration yesterday announced a 2.5 billion
dlr, five-year program to deal with acid rain, a problem that
troubles u.s. relations with canada.
    reagan was asked by a reporter whether the government
should set emission standards.
    he responded that the deeper that officials went in
examining the acid rain problem, the more complex it appeared.
    he said the government did not want to rush into adopting a
measure that might ultimately prove fruitless.
 reuter
</text>]