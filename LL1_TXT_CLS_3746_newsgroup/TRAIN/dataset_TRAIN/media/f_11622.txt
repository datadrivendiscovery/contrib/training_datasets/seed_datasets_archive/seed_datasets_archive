[<text>
<title>devon resource investors &lt;din&gt; year end dec 31</title>
<dateline>    oklahoma city, march 31 -
    </dateline>shr loss 14 cts vs profit 23 cts
    net loss 835,000 vs profit 950,000
    revs 8,617,000 vs 11.8 mln
 reuter
</text>]