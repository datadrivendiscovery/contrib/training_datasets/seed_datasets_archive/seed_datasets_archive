[<text>
<title>&lt;broad&gt; acquires &lt;vogt and conant&gt; unit</title>
<dateline>    river rouge, mich., march 11 - </dateline>broad corp said it acquired
the construction activities of vogt and conant co of cleveland.
    the combined companies, to be called broad, vogt and conant
inc, will be the largest structural steel erection company in
the u.s. combined sales of the two operations were more than 40
mln dlrs in 1986.
 reuter
</text>]