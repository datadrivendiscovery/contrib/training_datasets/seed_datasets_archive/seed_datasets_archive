[<text>
<title>permian basin royalty trust &lt;pbt&gt; dividend</title>
<dateline>    fort worth, texas, june 19 -
    </dateline>mthly div 3.8784 cts vs vs 4.3742 cts prior
    pay july 15
    record june 30
 reuter
</text>]