[<text>
<title>national data corp &lt;ndta&gt; 3rd qtr feb 28 net</title>
<dateline>    atlanta, march 25 -
    </dateline>shr 31 cts vs 26 cts
    net 3,516,000 vs 2,972,000
    revs 40.0 mln vs 36.3 mln
    avg shrs 11.4 mln vs 11.2 mln
    nine mths
    shr 89 cts vs 73 cts
    net 10.0 mln vs 8,146,000
    revs 116.8 mln vs 105.0 mln
    avg shrs 11.3 mln vs 11.1 mln
 reuter
</text>]