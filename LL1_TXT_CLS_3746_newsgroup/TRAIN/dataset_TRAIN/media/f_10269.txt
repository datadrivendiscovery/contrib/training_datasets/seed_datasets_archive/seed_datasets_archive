[<text>
<title>ntt considering buying cray supercomputer</title>
<dateline>    tokyo, march 27 - </dateline>&lt;nippon telegraph and telephone co&gt; (ntt)
is considering buying a cray ii supercomputer worth some three
billion yen from cray research inc &lt;cyr&gt;, and a decision is
expected in early april, an ntt official said.
    he told reuters ntt was considering the cray machine
because of its superior operational speed and memory capacity,
and not because of any pressure to buy foreign hardware.
    industry sources said foreign frustration was growing over
lack of access to japan's semiconductor and telecommunications
markets, and washington had criticised japanese government
agencies for not buying u.s. supercomputers.
 reuter
</text>]