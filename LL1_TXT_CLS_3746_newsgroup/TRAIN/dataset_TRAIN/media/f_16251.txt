[<text>
<title>u.k. money market given further 68 mln stg help</title>
<dateline>    london, april 13 - </dateline>the bank of england said it provided the
market with a further 68 mln stg assistance this afternoon,
bringing its total assistance on the day to 143 mln stg.
   shortly before, the bank said it had revised its estimate
of the shortage up to 450 mln stg from the earlier forecast of
400 mln.
    during the afternoon, the bank bought 22 mln stg of band
two bank bills at 9-13/16 pct and two mln stg of local
authority bills plus 44 mln stg of bank bills in band four at
9-11/16 pct. these rates were in all cases unchanged from
previous intervention levels.
 reuter
</text>]