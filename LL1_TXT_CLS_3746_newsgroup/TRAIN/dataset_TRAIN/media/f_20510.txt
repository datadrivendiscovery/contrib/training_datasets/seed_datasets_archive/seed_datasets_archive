[<text>
<title>shearson &lt;she&gt; to buyback up to 3 pct of stock</title>
<dateline>    new york, oct 20 - </dateline>shearson lehman brothers holdings inc
said it will repurchase up to three mln common shares or about
three pct of its total common shares outstanding on a fully
diluted basis.
    the company said the recent decline in the market price of
its shares presented an attractive investment opportunity and
that the repurchase program would enhance shareholder value.
    the shares will be repurchased in the open market from time
to time based on market conditions, the company said.
 reuter
</text>]