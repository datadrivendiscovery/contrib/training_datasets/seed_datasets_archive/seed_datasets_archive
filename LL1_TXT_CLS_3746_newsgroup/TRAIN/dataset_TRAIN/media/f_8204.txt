[<text>
<title>general partners in gencorp &lt;gy&gt; proxy fight</title>
<dateline>    new york, march 23 - </dateline>general partners, the group tendering
for all gencorp inc shares at 100 dlrs each, said it has
started soliciting proxies against gencorp's proposals to
increase its number of authorized shares outstanding, create a
board with staggered election dates and eliminate cumulative
voting.
    the proposals are to be voted on at the march 31 annual
meeting.
    general partners, a partnership of privately-held &lt;wagner
and brown&gt; and afg industries inc &lt;afg&gt;, made the disclosure in
a newspaper advertisement.
    the partnership has already filed suit in u.s. district
court in columbus, ohio, seeking to block a vote on the
proposals and to invalidate gencorp's defensive preferred share
purchase rights.
    general partners asked shareholders to either vote against
the proposals or abstain from voting on them.                  
   
 reuter
</text>]