[<text>
<title>oncor inc &lt;oncr.o&gt; 1st qtr loss</title>
<dateline>    gaithersburg, md., june 2 -
    </dateline>shr loss 12 cts vs loss five cts
    net loss 347,849 vs loss 103,489
    sales 222,697 vs 150,534
 reuter
</text>]