[<text>
<title>bryn mawr bank corp &lt;bmtc&gt; ups dividend</title>
<dateline>    bryn mawr, pa, march 12 -
    </dateline>qtly div 30 cts vs 30 cts prior
    payable may one
    record april 10
    note:bryn mawr was reorganized as a holding company on
january 2, 1987, resulting in each share of bryn mawr trust co
being converted into three shares of the new holding company's
stock. the 30 cts dividend represents a 15 pct increase over
prior quarter.
 reuter
</text>]