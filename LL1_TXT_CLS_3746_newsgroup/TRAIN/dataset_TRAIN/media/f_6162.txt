[<text>
<title>consolidated energy &lt;cps&gt; unit files chapter 11</title>
<dateline>    denver, march 17 - </dateline>consolidated energy partners lp said 99
pct owned master limited partnership consolidated operating
partners lp has defaulted on a 10 mln dlr principal payment to
its lending banks and has filed for reorganization under
chapter 11 of the federal bankruptcy code.
    the company said consolidated operating partners intends to
file a plan of reorganization within 90 days.
    it said a request for an extension of the payment time was
not granted.
    consolidated energy, an affiliate of consolidated oil and
gas inc &lt;cgs&gt;, said the value of the properties owned by
consolidated operating partners substantially exceeds the 46.3
mln dlrs of nonrecourse debt due lender banks first interstate
bancorp &lt;i&gt; and republicbank corp &lt;rpt&gt;.  the partnership has
other debt totalling about 530,000 dlrs, it said.
 reuter
</text>]