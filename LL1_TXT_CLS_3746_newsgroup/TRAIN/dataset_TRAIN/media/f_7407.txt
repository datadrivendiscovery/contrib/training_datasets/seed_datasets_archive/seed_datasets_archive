[<text>
<title>ambassador financial &lt;afgi&gt; unit to buy builder</title>
<dateline>    tamarac, fla., march 19 - </dateline>ambassador financial group inc
said its ambassador real estate equities corp, agreed to buy
heritage quality construction co inc.
    ambassador said its unit would purchase 100 pct of
heritage's stock for an initial payment of approximately
500,000 dlrs and subsequent payments in cash over a five-year
period equal to 50 pct of the net after-tax profit of heritage.
    if the acquisition is consummated, ambassador said it
agreed to contribute 250,000 dlrs to the existing capital of
heritage.
 reuter
</text>]