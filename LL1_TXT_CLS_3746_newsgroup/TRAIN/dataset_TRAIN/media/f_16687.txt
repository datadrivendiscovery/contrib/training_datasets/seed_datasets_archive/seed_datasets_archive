[<text>
<title>montgomery street income &lt;mts&gt; 1st qtr net</title>
<dateline>    san francisco, april 13 -
    </dateline>shr 49 cts vs 50 cts
    net 3,922,533 vs 3,979,580
    note: full name montgomery street income securities inc.
 reuter
</text>]