[<text>
<title>egypt buys pl 480 wheat flour - u.s. traders</title>
<dateline>    kansas city, april 9 - </dateline>egypt bought 125,723 tonnes of u.s.
wheat flour in its pl 480 tender yesterday, trade sources said.
    the purchase included 51,880 tonnes for may shipment and
73,843 tonnes for june shipment. price details were not
available.
 reuter
</text>]