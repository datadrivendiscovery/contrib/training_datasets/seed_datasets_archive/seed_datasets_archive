[<text>
<title>firm acquires &lt;american nutrition works inc&gt;</title>
<dateline>    pittsburgh, march 4 - </dateline>&lt;nusource investments inc&gt;, a
publicly held shell company, said it acquired american
nutrition works inc through a transaction in which american
nutrition shareholders received 28 mln shares of nusource stock
in exchange for their shares.
    american nutrition operates a chain of stores sellings
vitamins and health products.
    nusource said shareholders elected a new board consisting
of richard a. trydahl, samuel mineo and charles e. flink and
voted to change the name of the company to anw inc.
 reuter
</text>]