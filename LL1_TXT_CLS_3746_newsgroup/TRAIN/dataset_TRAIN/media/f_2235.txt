[<text>
<title>chrysler &lt;c&gt; overseas unit redeeming debentures</title>
<dateline>    detroit, march 5 - </dateline>chrysler overseas capital corp said it
authorized redemption on april 17 of its 4-3/4 pct and five pct
convertible debentures due 1988.
    the chrysler corp unit said the move is prompted by the
parent company's three-for-two stock split, which requires a
recalculation of the debentures' conversion prices that will
adversely affect those prices by at least five pct.
    chrysler said holders converting debentures before the
stock split becomes effective march 23 will receive a greater
number of shares on a post-split basis than if they convert
afterwards.
 reuter
</text>]