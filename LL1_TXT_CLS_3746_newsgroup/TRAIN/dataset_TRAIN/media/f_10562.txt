[<text>
<title>cabot &lt;cbt&gt; calls notes for redemption</title>
<dateline>    waltham, mass., march 27 - </dateline>cabot corp said it called for
redemption its outstanding 12-1/4 pct notes due november 1,
1994 and 10-3/4 pct notes due september 1, 1995, aggregating 89
mln dlrs at face amount.
    it said the redemption is part of a restructuring in which
it has said it would divest itself of certain discontinued
businesses, repurchase shares and reduce long-term debt.
    the 12-1/4 pct notes will be redeemed at 108.47 pct plus
interest from november 1, 1986. the 10-3/4 pct notes will be
redeemed at 108.96 pct plus accrued interest from march 1,
1987.
 reuter
</text>]