[<text>
<title>medtronic inc &lt;mdt&gt; sets payout</title>
<dateline>    minneapolis, march 4 - 
    </dateline>qtly dividend 22 cts vs 22 cts
    pay april 30
    record april 10
 reuter
</text>]