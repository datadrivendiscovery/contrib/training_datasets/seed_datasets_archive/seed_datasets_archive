[<text>
<title>&lt;gsw inc&gt; year net</title>
<dateline>    toronto, feb 26 -
    </dateline>oper shr 2.16 dlrs vs 2.07 dlrs
    oper net 8,037,000 vs 7,710,000
    revs 136.4 mln vs 133.3 mln
    note: 1986 net excludes extraordinary gain of 13 mln dlrs
or 3.50 dlrs shr from sale of &lt;camco inc&gt; shares vs yr-ago loss
of 4.3 mln dlrs or 1.14 dlrs shr.
 reuter
</text>]