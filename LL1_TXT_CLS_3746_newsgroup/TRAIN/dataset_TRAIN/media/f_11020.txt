[<text>
<title>warner-lambert &lt;wla&gt; wins packaging suit</title>
<dateline>    morris plains, n.j., march 30 - </dateline>warner-lambert co said a
federal court judgment resulted in &lt;my-k laboratories inc&gt;
agreeing to discontinue the sale of cough syrup and a
children's allergy elixir because it imitates warner-lambert 
packaging.
    warner-lambert said the final consent judgment was entered
in the u.s. district court of the northern district of
illinois. it also said the packaging in question imitated its
own packaging of benylin and benadryl products.
    my-k agreed to adopt different packaging, warner-lambert
said.
 reuter
</text>]