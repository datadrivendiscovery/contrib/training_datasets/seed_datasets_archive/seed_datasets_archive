[<text>
<title>allis-chalmers &lt;ah&gt; sells swiss unit</title>
<dateline>    milwaukee, wisc., march 25 - </dateline>allis-chalmers corp said it
has sold its elex ag unit in zurich, switzerland, to private
investors for an undisclosed amount.
    the company said elex produces electrostatic precipitators
used in air pollution control.
 reuter
</text>]