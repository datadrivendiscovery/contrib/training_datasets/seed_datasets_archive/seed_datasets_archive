[<text>
<title>securities dealer has leaseway &lt;ltc&gt; stake</title>
<dateline>    washington, march 11 - </dateline>alpine associates, a cresskill, n.j.
securities dealer, told the securities and exchange commission
it has acquired 565,100 shares of leaseway transport
corp, or 5.9 pct of the total outstanding common stock.
    alpine, a limited partnership, said it bought the stock for
28.1 mln dlrs as an investment in the ordinary course of its
business as a securities dealer.
    it left open the possibility that it might buy more
leaseway stock or sell some or all of its current stake, but
said it has no plans to seek control of the company.

 reuter
</text>]