[<text>
<title>&lt;bachelor lake gold mines inc&gt; year loss</title>
<dateline>    montreal, march 19 -
    </dateline>shr loss seven cts vs loss 19 cts
    net loss 497,452 vs loss 1,306,875
    revs 10.6 mln vs 9.6 mln
 reuter
</text>]