[<text>
<title>burst agritech inc &lt;brzt&gt; year nov 30 loss</title>
<dateline>    overland, kan., march 3 -
    </dateline>net loss 705,496 vs loss 182,766
    sales 642,590 vs 1,126,315
 reuter
</text>]