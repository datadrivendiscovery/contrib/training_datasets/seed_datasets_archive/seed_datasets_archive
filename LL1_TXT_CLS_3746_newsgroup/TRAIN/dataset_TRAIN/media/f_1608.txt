[<text>
<title>webcor electronics inc &lt;wer&gt; 3rd qtr dec 31</title>
<dateline>    garden city, n.y., march 4 -
    </dateline>shr loss 51 cts vs loss 44 cts
    net loss 1.8 mln vs loss 1.5 mln
    revs 3.1 vs 5.4 mln
    nine months
    shr loss 1.16 dlrs vs loss 1.33 dlrs
    net loss 4.0 mln vs loss 4.6 mln
    revs 9.9 mln vs 12.3 mln
 reuter
</text>]