[<text>
<title>chartwell group ltd &lt;ctwl&gt; 4th qtr net</title>
<dateline>    carlstadt, n.j., march 5 -
    </dateline>shr nine cts vs three cts
    net 549,000 vs 72,000
    rev 7.0 mln vs 2.8 mln
    year
    shr 49 cts vs 32 cts
    net 2,441,000 vs 801,000
    rev 19.6 mln vs 9.7 mln
   
 reuter
</text>]