[<text>
<title>first connecticut &lt;fco&gt; sets quarterly payout</title>
<dateline>    bridgeport, conn., march 18 -
    </dateline>qtly div 25 cts vs 25 cts prior
    pay april 24
    record april three
    note: first connecticut small business investment co.
 reuter
</text>]