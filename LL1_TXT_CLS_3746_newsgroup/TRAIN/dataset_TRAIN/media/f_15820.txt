[<text>
<title>gte &lt;gte&gt; unit signs deal with skyswitch</title>
<dateline>    golden, colo, april 9 - </dateline>&lt;skyswitch satellite communications
co&gt; said it gte corp's gte spacenet has signed an original
equipment manufacturing agreement with skyswitch that will
enable the two companies to set the standard for satellite news
gathering voice communications systems.
    the equipment will be provided under gte's specifications
and label, it said.
 reuter
</text>]