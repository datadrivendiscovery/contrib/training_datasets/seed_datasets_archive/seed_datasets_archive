[<text>
<title>versar &lt;vsr&gt; to buy marietta &lt;ml&gt; unit</title>
<dateline>    springfield, va., march 11 - </dateline>versar inc said it has agreed
in principle to acquire martin marietta corp's martin marietta
environmental systems unit for about 5,300,000 dlrs.
    versar said it would use its working capital and an
established life of credit to find the purchase, which is
subject to approval by both boards and is expected to be
completed in april.
    marietta environmental had 1986 revenues of about nine mln
dlrs.
    versar said the acquisition should have a "moderately"
favorable effect on its earnings this year.
 reuter
</text>]