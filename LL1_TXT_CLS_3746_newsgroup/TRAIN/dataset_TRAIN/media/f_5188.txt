[<text>
<title>&lt;the national bank of new zealand ltd&gt;</title>
<dateline>    wellington, march 16 - </dateline>year to december 31, 1986
    net profit 78 mln n.z. dlrs vs 45 mln
    pre-tax profit 147 mln vs 88 mln
    total assets 7.7 billion vs 6.4 billion
    notes - the company is 100 pct owned by lloyds bank plc
&lt;lloy.l&gt;. results include for the time first a pre-tax profit,
of 11 mln n.z. dlrs, from australian unit &lt;lloyds bank nza
ltd&gt;.
 reuter
</text>]