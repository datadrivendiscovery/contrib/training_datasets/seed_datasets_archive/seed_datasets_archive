[<text>
<title>campeau and edward debartolo complete purchase</title>
<dateline>    new york, april 1 - </dateline>&lt;campeau corp&gt; and the &lt;edward j.
debartolo corp&gt; have closed on their previously-announced 
purchase of five of the regional shopping centers of allied
stores corp.
    campeau said it and the debartolo association will each
hold a 50 pct ownership interest in the shopping centers.
    campeau separately announced that, as required by a bank
agreement, it has contributed an additional 50 mln dlrs of
capital to allied.
    campeau acquired allied stores corp earlier this year, the
company said.
 reuter
</text>]