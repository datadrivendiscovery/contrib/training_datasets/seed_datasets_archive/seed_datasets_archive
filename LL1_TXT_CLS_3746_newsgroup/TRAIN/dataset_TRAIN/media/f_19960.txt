[<text>
<title>bristol meyers &lt;bmy&gt; loses aids tests case</title>
<dateline>    chicago, june 29 - </dateline>a u.s. judge denied bristol meyers inc's
request to halt abbott laboratories' &lt;abt&gt; planned supply of
aids tests to the red cross, a spokeswoman for abbott said.
    judge joyce hens green of the u.s. district court in
washington, d.c., rejected bristol meyers' request for a
temporary restraining order against abbott's contract, set to
begin july one, to supply the aids tests, and tests for
hepatitis, to be used on blood donated to the red cross, the
spokeswoman said. she could not immediately provide green's
reasons for the ruling. a bristol meyers spokesman was not
immediately available for comment.
 reuter
</text>]