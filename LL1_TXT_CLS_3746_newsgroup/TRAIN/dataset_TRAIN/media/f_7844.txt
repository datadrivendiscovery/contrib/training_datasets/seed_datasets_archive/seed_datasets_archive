[<text>
<title>amex lists landmark technology &lt;lco&gt;</title>
<dateline>    new york, march 20 - </dateline>the american stock exchange said it
has listed the common stock of landmark technology corp, which
had been traded on the nasdaq system.
 reuter
</text>]