[<text>
<title>mobil &lt;mob&gt; unit names group president</title>
<dateline>    chicago, march 24 - </dateline>montomgery ward and co, a subsidiary of
mobil corp, said it named john weil as president of its apparel
group, replacing richard bourret, who has resigned.
    it said weil has been a consultant to the company since
1986.
 reuter
</text>]