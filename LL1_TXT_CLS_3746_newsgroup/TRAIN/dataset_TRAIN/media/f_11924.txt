[<text>
<title>biotherapeutics &lt;bitx&gt; in nasdaq expansion</title>
<dateline>    franklin, tenn., april 1 - </dateline>biotherapeutics inc said its
common stock will be included in the nasdaq national market
system starting april seven.
    as a result, it said the stock will become marginable.
 reuter
</text>]