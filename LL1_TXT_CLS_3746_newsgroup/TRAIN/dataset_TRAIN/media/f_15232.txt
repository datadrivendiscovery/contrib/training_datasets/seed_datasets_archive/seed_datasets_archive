[<text>
<title>ashton-tate &lt;tate&gt; names senior scientist</title>
<dateline>    torrance, calif., april 8 - </dateline>ashton-tate corp said it
appointed harry wong to the newly created position of senior
scientist.
    wong, formerly of the university of california's lawrence
berkeley laboratory, will provide technical direction in
advanced database architectures and structured query language,
sql, a computer language.
    the company also said it is acquiring technology to develop
a future database product that incorporates sql.
 reuter
</text>]