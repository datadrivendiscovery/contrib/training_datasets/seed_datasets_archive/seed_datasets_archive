[<text>
<title>us sprint to serve spain, denmark and sweden</title>
<dateline>    kansas city, mo., march 18 - </dateline>us sprint communications co, a
joint venture of gte corp &lt;gte&gt; and united telecommunications
inc &lt;ut&gt;, said it will initiate long distance service to spain,
denmark and sweden on april two.
    us sprint said the additions will bring to 34 the number of
countries it serves.
   
 reuter
</text>]