[<text>
<title>tokyo stock trading to be shortened from monday</title>
<dateline>    tokyo, march 27 - </dateline>the tokyo stock exchange said it will
shorten the trading session for stocks and convertible bonds by
30 minutes from march 30 to cope with high volume and will
maintain the cut as long as excessive trading continues.
    stock turnover hit a record 2.6 billion shares today,
exceeding the previous peak of 2.4 billion set on march 18.
    afternoon trading will start at 1330 local time (0430 gmt)
instead of the current 1300. (0400).
 reuter
</text>]