[<text>
<title>&lt;american west bank&gt; first qtr net</title>
<dateline>    encino, calif., april 3 -
    </dateline>net 90,501 vs 56,960
    assets 42.0 mln vs 34.9 mln
    deposits 35.9 mln
    loans 27.6 mln vs 23.9 mln
    note: earnings per share and 1985 deposits figure  not
supplied by company.
 reuter
</text>]