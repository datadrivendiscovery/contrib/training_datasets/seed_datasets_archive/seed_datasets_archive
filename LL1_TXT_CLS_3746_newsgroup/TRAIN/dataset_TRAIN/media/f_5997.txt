[<text>
<title>ltv &lt;qltv&gt; gets marine corps contract</title>
<dateline>    south bend, ind., march 17 - </dateline>ltv corp said it has received
a 23.5 mln dlr fixed-price contract to rebuild 923 m54 five-ton
cargo trucks into more modern m813 cargo trucks.
    it said it will deliver 120 conversion kits a months,
starting late in the second quarter and ending early in the
first quarter of 1988.
 reuter
</text>]