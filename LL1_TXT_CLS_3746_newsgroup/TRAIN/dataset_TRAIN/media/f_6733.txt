[<text>
<title>del-val financial corp &lt;dvl&gt; sets payout</title>
<dateline>    bogota, n.j., march 18 -
    </dateline>mthly div 14-1/2 cts vs 14-1/2 cts prior
    pay july one
    record june 17
 reuter
</text>]