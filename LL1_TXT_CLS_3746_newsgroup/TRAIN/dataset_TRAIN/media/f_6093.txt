[<text>
<title>cyclops &lt;cyl&gt; holder withdraws takeover motion</title>
<dateline>    new york, march 17 - </dateline>&lt;dixons group plc&gt;, which is in a
battle with &lt;cyacq corp&gt; for control of cyclops corp, said a
cyclops shareholder had agreed to withdraw a motion in u.s.
district court to prevent dixons from completing its tender
offer for cyclops, which expires 2400 est today.
    dixons did not name the shareholder and did not disclose
the holder's stake in cyclops.
   
 reuter
</text>]