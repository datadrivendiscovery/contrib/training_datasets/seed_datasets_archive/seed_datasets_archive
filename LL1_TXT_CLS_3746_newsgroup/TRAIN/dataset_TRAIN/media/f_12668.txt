[<text>
<title>vanguard technologies &lt;vti&gt; acquires company</title>
<dateline>    new york, april 2 - </dateline>vanguard technologies international inc
said it acquired 100 pct of the stock of &lt;diversified data
corp&gt;.
    diversified, and its wholly-owned subsidiary, techpro corp,
have current revenues from operations of approximately four mln
dlrs, vanguard said.
    the acquisition was for cash, but the amount was not
disclosed.
 reuter
</text>]