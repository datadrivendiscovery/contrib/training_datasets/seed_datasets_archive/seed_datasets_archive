[<text>
<title>boliden says it now leader in mining gear</title>
<author>    by per isaksson, reuters</author>
<dateline>    stockholm, april 1 - </dateline>swedish mining and metals group
boliden ab &lt;blds.st&gt; said the takeover of the u.s.
allis-chalmers corp's &lt;ah.o&gt; mining machinery division made it
the world's leading maker of such equipment.
    president kjell nilsson, announcing the 600 mln crown deal,
told a news conference boliden would now become a truly
international concern with operations in brazil, chile and
other big minerals-producing nations.
    he said the allis-chalmers' division, accounting for some
50 pct of the u.s. group's sales, would fit in well into
boliden.
 reuter
</text>]