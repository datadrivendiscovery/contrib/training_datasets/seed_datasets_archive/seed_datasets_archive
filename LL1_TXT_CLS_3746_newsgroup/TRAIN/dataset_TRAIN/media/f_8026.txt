[<text>
<title>sunbelt nursery group inc &lt;sbn&gt; 2nd qtr feb 28</title>
<dateline>    fort worth, texas, march 20 -
    </dateline>shr loss 40 cts vs loss 29 cts
    net loss 1.5 mln vs loss 1.1 mln
    revs 28.9 mln vs 28.5 mln
    six months
    shr loss 99 cts vs loss 69 cts
    net loss 3.7 mln vs loss 2.6 mln
    revs 52.5 mln vs 51.7 mln
   
 reuter
</text>]