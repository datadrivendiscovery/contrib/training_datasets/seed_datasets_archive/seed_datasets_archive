[<text>
<title>general videotex adds investment &lt;ives&gt; service</title>
<dateline>    cambridge, mass., march 5 - </dateline>&lt;general videotex corp&gt; said it
added investment technologies co's vestor on-line stock market
analysis database to its delphi network service.
    it said the vestor service will be available on delphi
within 30 days.
 reuter
</text>]