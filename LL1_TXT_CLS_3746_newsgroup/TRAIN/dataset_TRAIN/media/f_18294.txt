[<text>
<title>intermetco ltd &lt;int.to&gt; six mths april 30 net</title>
<dateline>    hamilton, ontario, june 2 -
    </dateline>shr 18 cts vs 27 cts
    net 283,000 vs 435,000
    revs 97.8 mln vs 95.1 mln
 reuter
</text>]