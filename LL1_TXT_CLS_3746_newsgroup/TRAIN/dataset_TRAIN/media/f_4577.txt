[<text>
<title>meatpackers respond to occidental&lt;oxy&gt; offer</title>
<dateline>    chicago, march 12 - </dateline>local 222 of the united food and
commercial workers union said it is calling a membership
meeting, possibly sunday, to discuss its response to a decision
by iowa beef processors inc to lift a lockout at its dakota
city, nebraska plant and resume operations.
    the ufcwu will consider all options available to it
including a strike or returning to work under ibp's last labor
contract proposal, a spokeswoman for local 222 said by phone.
    about 2,800 ufcwu members have been locked out at the
dakota city plant since december 14.
    ibp is a subsidiary of occidental petroleum corp.
 reuter
</text>]