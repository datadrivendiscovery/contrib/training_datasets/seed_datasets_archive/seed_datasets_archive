[<text>
<title>edac technologies corp &lt;edac.o&gt; 3rd qtr net</title>
<dateline>    milwaukee, wis., oct 9 -
    </dateline>shr three cts vs two cts
    net 109,000 vs 67,000
    sales 11.3 mln vs 11.3 mln
    nine mths
    shr seven cts vs 99 cts
    net 221,000 vs 3,213,000
    sales 30.6 mln vs 35.9 mln
    order backlog 22.7 mln vs 13.5 mln
    note: 1986 figures include life insurance proceeds of 2.5
mln dlr or 78 cts a share.
 reuter
</text>]