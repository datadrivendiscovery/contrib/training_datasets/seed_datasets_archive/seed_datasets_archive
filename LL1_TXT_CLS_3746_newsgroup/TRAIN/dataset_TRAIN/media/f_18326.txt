[<text>
<title>minntech corp &lt;mntx.o&gt; 4th qtr mar 31 net</title>
<dateline>    minneapolis, june 2 -
    </dateline>opr shr nil vs seven cts
    opr net 3,000 vs 99,000
    revs 2,745,000 vs 2,395,000
    avg shrs 1,500,000 vs 1,375,000
    year
    opr shr 24 cts vs 20 cts
    opr net 343,000 vs 271,000
    revs 10.7 mln vs 8,232,000
    avg shrs 1,459,000 vs 1,341,000
    note: earnings for year exclude gains due to tax loss
carryforward of 210,000 dlrs or 14 cts a share in 1987 and
198,000 dlrs or 15 cts in 1986.
 reuter
</text>]