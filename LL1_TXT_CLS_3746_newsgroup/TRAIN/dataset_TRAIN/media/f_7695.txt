[<text>
<title>treasury's baker opposes closing bank loophole</title>
<dateline>    washington, march 20 - </dateline>treasury secretary james baker said
the administration still opposes senate banking committee
legislation that would close a key loophole in federal bankinng
law.
    the loophole has spawned a proliferation of limited service
banks known as non-bank banks that can engage in risky
activities barred to traditional banks.
    baker has been asked to support the legislation by banking
chairman william proxmire (d-wisc).
    proxmire's panel approved closing the loophole in a bill
recapitalizing the thrift insurance fund.
    baker said the administration supports moving a bill
through congress that would address only the recapitalization
issue, for fear such "collateral provisions" as the
loophole-closing measure would stall the bill.
    proxmire later told reporters he backs the committee bill
with or without administration support.
    "i'm going to push it through in any event," he said.
    the administration also opposes the committee bill on
grounds it bars new banking powers.
    the administration is a long time backer of banking
deregulation.
 reuter
</text>]