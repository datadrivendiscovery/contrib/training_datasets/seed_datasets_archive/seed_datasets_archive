[<text>
<title>fine homes int'l initial offering priced</title>
<dateline>    new york, june 19 - </dateline>merrill lynch and co inc said its
initial public offering of six mln limited partnership
preference units of fine homes international l.p. was priced at
18 dlrs a unit.
    in addition, there was also an offering by fine homes of
1.5 mln preference units to its employees.
    merrill lynch said it will continue to own about 21 mln
subordinated units.
    fine homes is involved in residential real estate and in
the relocation management business and related mortgage
banking.
 reuter
</text>]