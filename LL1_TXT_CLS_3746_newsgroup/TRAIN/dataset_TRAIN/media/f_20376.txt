[<text>
<title>wolohan lumber co &lt;wlhn.o&gt; 3rd qtr net</title>
<dateline>    saginaw, mich., oct 20 -
    </dateline>shr 46 cts vs 33 cts
    net 2,731,000 vs 1,928,000
    sales 66.2 mln vs 58.8 mln
    nine mths
    shr seven cts vs 61 cts
    net 6,310,000 vs 3,579,000
    sales 171.8 mln vs 141.9 mln
 reuter
</text>]