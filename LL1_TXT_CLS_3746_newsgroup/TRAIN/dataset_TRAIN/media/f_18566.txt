[<text>
<title>csr declares pioneer sugar bid unconditional</title>
<dateline>    sydney, june 18 - </dateline>csr ltd &lt;csra.s&gt; said it had declared
unconditional its takeover bid for &lt;pioneer sugar mills ltd&gt;
following pioneer's recommendation that shareholders accept.
    this meant pioneer shareholders would be paid for all
shares tendered within 14 days, csr said in a statement.
    csr's statement follows industrial equity ltd's &lt;inea.s&gt;
disclosure yesterday that it had built up a 9.8 pct stake in
pioneer at 2.54 dlrs a share, topping csr's cash bid of 2.50.
    csr is also offering one share, currently worth 4.04 dlrs,
plus 1.20 cash, for every two pioneer shares, which values
pioneer at 2.62 per share. it holds about 33 pct of pioneer.
 reuter
</text>]