[<text>
<title>treasury's baker says new money not debt panacea</title>
<dateline>    washington, march 25 - </dateline>treasury secretary james baker said
massive new lending to debtor nations could actually increase
their difficulties in the period ahead without protections.
    in testimony before the senate committee on governmental
affairs, baker said, "throwing money at the debtor nations won't
solve their problems."
    baker told the committee such an approach could "worsen
their difficulties unless the new financing can be productively
absorbed and is consistent with their ability to grow and
service debt."
    he said the debt initiative associated with his name is a
long-term approach and further progress "will be gradual and
will vary among nations, depending upon their own determination
to implement growth-oriented reforms and on the continued
active support of the international community."
 reuter
</text>]