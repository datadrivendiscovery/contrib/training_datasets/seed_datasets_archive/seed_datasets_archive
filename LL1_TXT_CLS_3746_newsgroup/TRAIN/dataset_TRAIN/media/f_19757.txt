[<text>
<title>genrad &lt;gen&gt; introduces test system</title>
<dateline>    concord, mass., june 29 - </dateline>genrad inc said it has introduced
the hitest test generation system.
    the company said hitest takes a modular approach to test
generation, using a set of interactive software tools that
automate many parts of the test generation process.
    hitest runs on digital equipment corp &lt;dec&gt; vax and
microvax computers and is priced at 64,000 dlrs for a software
license, genrad said.
    it added that delivery is immediate.
 reuter
</text>]