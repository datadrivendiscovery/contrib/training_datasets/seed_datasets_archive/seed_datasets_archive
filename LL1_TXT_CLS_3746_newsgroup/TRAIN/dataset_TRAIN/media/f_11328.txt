[<text>
<title>radiation disposal &lt;rdis&gt; touts new method</title>
<dateline>    charlotte, n.c., march 31 - </dateline>radiation disposal system inc
said tests have been successful in using radioactive materials
to reduce the volume of low-level organic radioactive waste.
    the company said the tests were completed at north carolina
state university in raleigh. the tests showed the process
reduced the volume of radioactive waste without any
environmental release of radioactive solids or gases, the
company said.
 reuter
</text>]