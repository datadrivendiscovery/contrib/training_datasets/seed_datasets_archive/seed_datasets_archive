[<text>
<title>morgan stanley group &lt;ms&gt; unit in gas deal</title>
<dateline>    houston, april 28 - </dateline>morgan stanley group inc unit natural
gas clearinghouse inc said it has reached agreement with
(pan-alberta gas ltd) of canada to import substantial
quantities of natural gas for its u.s. customers.
    the company said potentially 500 mln cubic feet a day of
canadian natural gas could be imported under the agreement.
    it said the natural gas would be competitively priced but
did not refer to specific prices.
    pan-alberta is owned by nova &lt;nva.a.t&gt; and alberta energy
co, the company added.
 reuter
</text>]