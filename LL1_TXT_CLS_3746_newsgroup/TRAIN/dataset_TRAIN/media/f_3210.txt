[<text>
<title>diamond shamrock offshore &lt;dsp&gt; in find</title>
<dateline>    dallas, march 9 - </dateline>diamond shamrock offshore partners said
it has started development drilling on west cameron 178 block
off louisiana in the gulf of mexico after a significant natural
gas find on the block.
    it said the discovery well there encountered 46 feet of net
natural gas pay. no flow tests have been conducted, it said.
    diamond shamrock offshore said it has a 46.15 pct interest
in the block, phillips petroleum co 28.85 pct and santa fe
energy partners lp &lt;sfp&gt; 25.00 pct. diamond shamrock corp &lt;dia&gt;
owns 80.3 pct of diamond shamrock offshore.
 reuter
</text>]