[<text>
<title>gates learjet corp &lt;glj&gt; 4th qtr loss</title>
<dateline>    tucson, ariz., march 17 -
    </dateline>shr loss 12 cts vs loss 99 cts
    net loss 1,476,000 vs loss 11,965,000
    sales 83.4 mln vs 110.9 mln
    year
    shr loss 1.79 dlrs vs loss 1.90 dlrs
    net loss 21,720,000 vs loss 22,969,000
    sales 259.0 mln vs 317.3 mln
 reuter
</text>]