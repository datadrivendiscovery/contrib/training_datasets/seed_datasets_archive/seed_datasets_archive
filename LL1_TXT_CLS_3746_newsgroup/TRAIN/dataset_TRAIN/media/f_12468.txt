[<text>
<title>toshiba regrets link with u.k. access issue</title>
<dateline>    tokyo, april 2 - </dateline>toshiba corp &lt;tsba.t&gt; said it regrets its
plan to enter the u.k. business facsimile and telephone market
may be caught up in a diplomatic row over the position of cable
and wireless plc's &lt;cawl.l&gt; in the japanese market.
    britain is considering how to retaliate against japan's
attempt to prevent cable and wireless from taking a major
position in a japanese international telecommunications
venture.
    "as a matter of timing it is regrettable that this has been
linked with the question of market access in japan," a toshiba
spokesman told reuters.
    &lt;toshiba information systems (u.k.) ltd&gt;, a toshiba
subsidiary, said yesterday it planned to enter the u.k. market
under the toshiba own brand name and had applied for government
approval to do so.
    toshiba has supplied equipment to u.k. manufacturers for
sale under their brand names since last year.
    the toshiba spokesman said the sale of such equipment was
not comparable to cable and wireless' efforts to take a stake
in the new japanese telecommunications firm.
    "they are matters of a different category," he said.
 reuter
</text>]