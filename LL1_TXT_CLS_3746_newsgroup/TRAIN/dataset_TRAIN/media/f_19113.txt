[<text>
<title>fidelcor &lt;ficr.o&gt; buys new england &lt;bkne.o&gt;stake</title>
<dateline>    philadelphia, june 19 - </dateline>fidelcor inc said it has acquired a
substantial portion of the assets of bank of new england corp's
lazere financial corp subsidiary for undisclosed terms.
    the company said the transaction includes most of lazere's
loan portfolio and other assets, including lazere's miami
office.                                
 reuter
</text>]