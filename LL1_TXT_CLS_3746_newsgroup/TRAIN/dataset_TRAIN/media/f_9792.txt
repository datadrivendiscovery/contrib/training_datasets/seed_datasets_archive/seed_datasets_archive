[<text>
<title>currency exchange loss pushes malaysia's debt up</title>
<dateline>    kuala lumpur, march 26 - </dateline>an exchange loss of 7.6 billion
ringgit in 1986 pushed malaysia's outstanding external debt up
to 50.99 billion ringgit, from 1985's 42.3 billion, the central
bank said in its annual report.
    bank negara said although malaysia's net borrowing dropped
in 1986, its external debt rose due to the 30 pct appreciation
of the basket of currencies against which the ringgit is
pegged.
    the basket comprises principally the u.s. dollar, yen,
mark, swiss franc, french franc, sterling, guilder, canadian
and singapore dollars, it added.
    bank negara said growth in external debt, which declined
progressively from a peak of 58 pct in 1982 to 13.6 pct in
1985, rose by 20.2 pct in 1986.
    malaysia's debt serving ratio of 17.6 pct of its exports in
1986 is within the prudency limit of 20 pct, bank negara
governor jaafar hussein told reporters.
 reuter
</text>]