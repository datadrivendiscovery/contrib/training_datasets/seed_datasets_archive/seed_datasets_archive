[<text>
<title>du pont &lt;dd&gt; ups stake in perceptive systems</title>
<dateline>    houston, march 23 - </dateline>du pont co has increased its equity
stake in &lt;perceptive systems inc&gt; to 33.5 pct from 20 pct,
perceptive systems said.
    perceptive systems, a venture capital firm based in
houston, makes digital imaging equipment.
 reuter
</text>]