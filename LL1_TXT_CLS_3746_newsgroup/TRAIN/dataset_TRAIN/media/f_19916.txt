[<text>
<title>western union &lt;wu&gt; in pact with electronic data</title>
<dateline>    upper saddle river, n.j., june 29 - </dateline>western union said it
signed an agreement with &lt;electronic data systems corp&gt;.
    under the agreement, eds will provide western union's
easylink electronic mail service to the 35,000 eds diamond
communications users of general motors corp &lt;gm&gt; internal
electronic mail network. diamond is an interface linking eight
incompatible electronic mail systems at gm's various divisions.
    diamond users will now have a communications path to
160,000 easylink users and 1.5 mln telex users worldwide,
western union said.
 reuter
</text>]