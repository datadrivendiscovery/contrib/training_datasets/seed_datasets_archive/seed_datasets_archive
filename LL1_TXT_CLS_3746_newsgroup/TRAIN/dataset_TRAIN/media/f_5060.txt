[<text>
<title>u.s. business loans rise 377 mln dlrs</title>
<dateline>    washington, march 13 - </dateline>business loans on the books of major
u.s. banks, excluding acceptances, rose 377 mln dlrs to 279.085
billion dlrs in the week ended march 4, the federal reserve
board said.
    the fed said that business loans including acceptances
increased 484 mln dlrs to 281.546 billion dlrs.
 reuter
</text>]