[<text>
<title>chronar &lt;crnr&gt; closes 20 mln dlr placement</title>
<dateline>    princeton, n.j., march 4 - </dateline>chronar corp said it received a
total of 20 mln dlrs through the completion of a private
placement of its stock, long-term debt and associated warrants
with the sheet metal workers' national pension fund and an
affiliate of &lt;harbert corp&gt;.
    it also said the fund and harbert solar inc, the affiliate,
now own about 16 pct of chronar's common.
    chronar also said it expects to report a loss for the year
of about 4.5 mln dlrs on sales of about 12 mln dlrs compared
with a loss of 513,153 dlrs on revenues of 10 mln dlrs in 1985.
 reuter
</text>]