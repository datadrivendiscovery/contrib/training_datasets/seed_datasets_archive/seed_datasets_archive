[<text>
<title>senate seeks u.s. probe of canadian corn levy</title>
<dateline>    washington, march 26 - </dateline>the senate voted unanimously to seek
an expedited u.s. probe of canadian tariffs on corn imports to
determine if the united states should retaliate.
    by 99 to 0, the senate went on record against the 84.9
cents per bushel tariff approved by the canadian import
tribunal.
    the non binding measure asked for a probe by the u.s. trade
representative to determine within 30 days whether the tariff
violates the general agreement on tariffs and trade, and if so
recommend within 60 days to president reagan retaliatory action
against canada.
 reuter
</text>]