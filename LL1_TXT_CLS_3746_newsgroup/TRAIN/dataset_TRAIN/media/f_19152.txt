[<text>
<title>total erickson &lt;tle.t&gt; buys mining property</title>
<dateline>    vancouver, b.c., june 19 - </dateline>total erickson resources ltd and
consolidated silver standard mines ltd &lt;cds.v&gt; said that total
erickson has purchased all consolidated's interests in its dome
mountain property for 60,000 total erickson shares and 70,000
dlrs in cash.
    the companies said the property has several gold-bearing
veins and has considerable exploration potential.
 reuter
</text>]