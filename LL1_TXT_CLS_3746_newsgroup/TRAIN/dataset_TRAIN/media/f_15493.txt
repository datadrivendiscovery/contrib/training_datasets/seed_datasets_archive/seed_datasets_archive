[<text>
<title>pse to start option trade on baker hughes &lt;bhi&gt;</title>
<dateline>    san francisco, april 9 - </dateline>the pacific stock exchange said it
will start options trading on baker hughes inc today.
    expirations will be january, may, july and october.
 reuter
</text>]