[<text>
<title>interstate bakeries &lt;ibc&gt; signs pact</title>
<dateline>    kansas city, mo., march 5 - </dateline>interstate bakeries corp said
it has entered into a joint venture with pain jacquet s.a.,
europe's leading bread baker, for distribution of a three item
french bread line.
    interstate said terms of transaction were not disclosed.
 reuter
</text>]