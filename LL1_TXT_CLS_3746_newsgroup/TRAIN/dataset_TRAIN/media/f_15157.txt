[<text>
<title>mellon participating mortgage trust &lt;mpmts&gt; div</title>
<dateline>    new york, april 8 -
    </dateline>qtly div 25 cts vs 25 cts prior
    payable may 6
    record april 24
 reuter
</text>]