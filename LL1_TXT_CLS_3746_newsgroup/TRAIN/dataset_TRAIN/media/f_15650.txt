[<text>
<title>iran says it opens new offensive north of baghdad</title>
<dateline>    london, april 9 - </dateline>iran said it launched a fresh offensive
today north of baghdad, three days after it began a new thrust
against iraq on the southern gulf war front.
    the iranian news agency irna, received in london, said the
offensive began before dawn northeast of qasr-e-shirin, on the
border 110 miles northeast of the iraqi capital.
    "heavy casualties and losses have been inflicted on iraqi
forces in the fresh iranian assault," irna said.
    iran today reported major gains on the southern front near
the major iraqi port city of basra, saying its forces had
captured an important defensive line.
 reuter
</text>]