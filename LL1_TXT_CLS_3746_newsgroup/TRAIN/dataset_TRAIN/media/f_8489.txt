[<text>
<title>alc &lt;alcc&gt; anticipates 1st qtr profit</title>
<dateline>    birmingham, mich., march 23 - </dateline>alc communications corp said
that because of strong traffic growth and cost reductions it
anticipates reporting a profit for the first quarter of 1987,
versus a loss of 1.4 mln dlrs, or 15 cts a share, for the first
quarter of 1986.
    earlier, the company reported a net after-tax loss for 1986
of 60.8 mln dlrs, or 4.63 dlrs a share, compared with a loss of
28.9 mln dlrs, or 2.43 dlrs a share, in 1985.
   
 reuter
</text>]