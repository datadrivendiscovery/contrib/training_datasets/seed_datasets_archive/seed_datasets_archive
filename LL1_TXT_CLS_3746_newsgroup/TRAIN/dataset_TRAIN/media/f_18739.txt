[<text>
<title>mobil &lt;mob&gt; has north sea natural gas find</title>
<dateline>    new york, june 18 - </dateline>mobil corp said the 49/28-14 wildcat
well in block 49/28 of the british north sea flowed 50.6 mln
cubic feet of natural gas per day from depths of 7,742 to 7,777
feet.
    the company said it has a 23.33 pct interest and other
interest holders include atlantic richfield co &lt;arc&gt;, sun co
&lt;sun&gt; and deminex ag 10 pct.
 reuter
</text>]