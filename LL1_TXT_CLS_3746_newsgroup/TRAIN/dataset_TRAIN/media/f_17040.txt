[<text>
<title>setback seen for nakasone in japanese parliament</title>
<dateline>    tokyo, april 24 - </dateline>japan's lower house passed the 1987
budget after the ruling liberal democratic party agreed to a
mediation proposal that could kill its plan to introduce a
controversial sales tax, political analysts said.
    the move was seen as a major blow to prime minister
yasuhiro nakasone, the leading advocate of the five pct tax.
    some analysts predicted nakasone might be forced to step
down just after the june summit of leaders from major
industrial democracies, well before his one-year term is due to
expire at the end of october.
    the ruling party though was anxious to pass the budget
before nakasone leaves next week for the u.s. so that he could
tell washington the japanese government was doing its utmost to
boost the sagging economy and imports.
 reuter
</text>]