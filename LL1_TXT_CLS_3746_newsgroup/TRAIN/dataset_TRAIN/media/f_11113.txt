[<text>
<title>neoax &lt;noax&gt; buys rexnord &lt;rex&gt; unit</title>
<dateline>    lawrenceville, n.j., march 30 - </dateline>neoax inc said it bought
rexnord inc's fairfield manufacturing co for 70.5 mln dlrs
cash.
    the unit makes custom gears for industrial use and had
sales of 84 mln dlrs in its oct. 31, 1986 fiscal year.
 reuter
</text>]