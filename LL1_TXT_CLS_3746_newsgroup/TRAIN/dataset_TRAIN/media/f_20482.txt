[<text>
<title>k mart corp &lt;km&gt; sets qtly dividend</title>
<dateline>    troy, mich., oct. 20 -
    </dateline>qtly div 29 cts vs 29 cts prior qtr
    pay dec 7
    record nov 19
 reuter
</text>]