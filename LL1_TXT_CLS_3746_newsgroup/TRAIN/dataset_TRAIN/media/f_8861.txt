[<text>
<title>prudential records best results in six years</title>
<dateline>    london, march 24 - </dateline>&lt;prudential corporation plc&gt;, which
earlier announced a 62 pct rise in 1986 pre-tax profits, said
it had recorded its best general insurance result for six years
but had not reached satisfactory levels of profit in other
areas.
    group chief executive brian corby told a news conference
that despite returning to trading profits, the international
division and the mercantile and general division had not
reached satisfactory levels.
    but he said he welcomed mercantile and general trading
profits in 1986 and was optimistic about both that and the
international division.
    the acquisition of the u.s. life company &lt;jackson national&gt;
had a small effect in 1986 but its full effect would be felt in
the 1987 results, corby said.
    the group also intended to expand the number of its estate
agency firms bought last year, and hoped they will comprise
between 10 and 15 pct of total company profits in the future.
    "we hope they will be very profitable very shortly. we are
looking for profits from the estate agencies themselves as well
as the insurance products associated with them," corby said.
    prudential's pre-tax profits rose from 1985's 110.1 mln stg
to 178.1 mln stg in 1986.
 reuter
</text>]