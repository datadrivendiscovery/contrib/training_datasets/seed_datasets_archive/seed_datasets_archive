[<text>
<title>first southern &lt;fsfa&gt; to make acquisition</title>
<dateline>    mobile, ala., march 4 - </dateline>first southern federal savings and
loan association said it has agreed in principle to acquire
horizon financial corp and horizon funding corp from &lt;victor
federal savings and loan association&gt; of muskogee, okla., for
undisclosed terms.
    the company said the purchase is subject to approval of the
boards of first southern and victor and regulatory agencies.
    horizon financial services mortgage loans and horizon
funding is a wholesale loan purchasing operation. horizon
services 3.2 billion dlrs in mortgage loans.
 reuter
</text>]