[<text>
<title>markitstar &lt;mark&gt; files to offer shares</title>
<dateline>    new york, march 19 - </dateline>markitstar inc said it has filed for
an offering of 1,700,000 common shares through underwriters led
by woolcott and co inc.
    the company said it will sell 1,500,000 shares and
shareholders the rest.  company proceeds will be used for the
development and expansion of marketing and sales operations and
facilities, for research and for working capital.
 reuter
</text>]