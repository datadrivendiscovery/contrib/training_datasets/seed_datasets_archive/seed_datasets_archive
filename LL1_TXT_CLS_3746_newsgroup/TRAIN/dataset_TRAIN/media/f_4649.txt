[<text>
<title>sandoz ag &lt;sanz.z&gt; year 1986</title>
<dateline>    basle, march 13 - </dateline>group 1986 net profit 541 mln swiss
francs vs. 529 mln
    dividend 105 francs per 250 francs nominal share vs. 100,
21 francs per 50 franc nominal participation certificate vs 20
    group turnover 8.36 billion francs vs. 8.45 billion
    cash flow 956 mln francs vs. 941 mln
    parent company net profit 153.8 mln francs vs. 135.3 mln
 reuter
</text>]