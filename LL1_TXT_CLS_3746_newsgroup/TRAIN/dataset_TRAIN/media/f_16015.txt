[<text>
<title>napa valley bancorp &lt;nvbc&gt; 1st qtr net</title>
<dateline>    napa, calif, april 9 -
    </dateline>shr 20 cts vs 25 cts
    net 487,000 vs 435,000
 reuter
</text>]