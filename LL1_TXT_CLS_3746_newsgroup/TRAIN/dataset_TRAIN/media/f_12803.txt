[<text>
<title>norsk hydro, saga seek drilling rights in gabon</title>
<dateline>    oslo, april 3 - </dateline>norwegian oil companies norsk hydro a/s
&lt;nhy.ol&gt; and saga petroleum a/s &lt;sago.ol&gt; said they have
applied for offshore exploration drilling licenses in gabon on
africa's west coast.
    saga petroleum said it has applied for a 35 pct share and
operatorship on one block, adding finnish oil company neste
&lt;neoy.he&gt;(25 pct), spain's hispanoil (25 pct), and the world
bank's international finance corporation (ifs) (15 pct) have
joined saga to fill remaining shares in the application.
    saga spokesman roy halvorsen told reuters he expected
gabonese officials would reply to the application by easter.
    halvorsen said this is the first time saga has applied to
operate on opec-member gabon's continental shelf, adding that
italian oil company agip is heading a group of applicants in a
separate bid for the same license.
    norsk hydro has also applied for an undisclosed share in a
single exploration license in which u.s. oil company tenneco
has already been assigned operatorship, company spokesman
bjoern tretvoll said.
 reuter
</text>]