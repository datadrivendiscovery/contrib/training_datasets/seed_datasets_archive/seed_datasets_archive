[<text>
<title>friedman industries inc &lt;frd&gt; quarterly div</title>
<dateline>    houston, march 25 - 
    </dateline>qtly div seven cts vs seven cts prior
    pay june one
    record may four
 reuter
</text>]