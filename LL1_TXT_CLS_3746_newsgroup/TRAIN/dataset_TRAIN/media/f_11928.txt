[<text>
<title>divi hotels &lt;dvh&gt; warrant price adjusted</title>
<dateline>    new york, april 1 - </dateline>divi hotels nv of aruba said the
exercise price of its warrants has been cut to 11 dlrs per
share from 12 dlrs until september 17 and to 13 dlrs from 14
dlrs thereafter due to the impact of a recent 1,500,000 share
offering.
 reuter
</text>]