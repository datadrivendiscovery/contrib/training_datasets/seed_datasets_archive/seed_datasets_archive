[<text>
<title>met-pro corp &lt;mpr&gt; to pay regular dividend</title>
<dateline>    harleysville, pa., march 13 -
    </dateline>qtrly 15 cts vs 15 cts prior
    pay may eight
    record april 24
 reuter
</text>]