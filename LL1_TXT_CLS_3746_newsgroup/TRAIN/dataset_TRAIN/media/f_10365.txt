[<text>
<title>walt disney increases eurobond to 75 mln aust dlrs</title>
<dateline>    london, march 27 - </dateline>the amount of a eurobond issued
yesterday by walt disney co ltd has been raised to 75 mln
australian dlrs from an original 60 mln, lead manager warburg
securities said.
    the issue, which matures on may 7, 1990, has a 14-1/2 pct
coupon and is priced at 101-3/8 pct.
 reuter
</text>]