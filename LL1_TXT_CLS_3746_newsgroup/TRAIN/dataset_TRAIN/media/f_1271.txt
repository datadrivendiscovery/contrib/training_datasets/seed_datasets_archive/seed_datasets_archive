[<text>
<title>royal resources corp &lt;rrco&gt; 2nd qtr dec 31 loss</title>
<dateline>    denver, colo., march 3 -
    </dateline>shr loss 72 cts vs loss 1.48 dlrs
    net loss 4,466,006 vs loss 9,091,688
    revs 608,181 vs 1,280,727
    six mths
    shr loss 77 cts vs loss 1.51 dlrs
    net loss 4,752,455 vs loss 9,265,457
    revs 1,444,149 vs 2,791,188
   
 reuter
</text>]