[<text>
<title>humana &lt;hum&gt; to sell medical offices</title>
<dateline>    louisville, ky., april 9 - </dateline>humana inc said it has agreed in
principle to sell 68 medfirst primary medical care facilities
to &lt;primedical corp&gt; for undisclosed terms, with transfers
taking place over the next four months.
    it said it retains 37 medfirst offices, mostly in the
chicago area.
    the transaction is not expected to have any impact on
earnings, humana said.
 reuter
</text>]