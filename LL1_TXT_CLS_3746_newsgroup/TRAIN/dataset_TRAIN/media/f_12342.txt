[<text>
<title>masstor &lt;msco&gt; in pact with nat'l westminster</title>
<dateline>    santa clara, calif., april 1 - </dateline>masstor systems corp said it
it signed a volume purchase agreement with national westminster
bank plc worth up to 7.3 mln dlrs.
 reuter
</text>]