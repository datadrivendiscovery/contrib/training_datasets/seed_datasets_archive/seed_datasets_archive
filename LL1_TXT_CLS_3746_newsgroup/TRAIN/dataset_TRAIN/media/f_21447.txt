[<text>
<title>southwest bancorp &lt;swb&gt; 3rd qtr loss</title>
<dateline>    vista, calif., oct 19 -
    </dateline>oper shr loss 82 cts vs profit 12 cts
    oper net loss 4,134,000 vs profit 544,000
    avg shrs 5,030,000 vs 3,927,000
    nine mths
    oper shr loss 80 cts vs profit 32 cts
    oper net loss 3,615,000 vs profit 1,457,000
    avg shrs 4,557,000 vs 3,927,000
    note: 1986 net excludes tax loss carryforwards of 105,000
dlrs in quarter and 195,000 dlrs in nine mths.
    1986 quarter net includes 212,000 dlr tax credit.
    1987 net both periods includes 3,700,000 dlr addition to
loan loss reserves due mostly to one out-of-state real estate
transaction.
 reuter
</text>]