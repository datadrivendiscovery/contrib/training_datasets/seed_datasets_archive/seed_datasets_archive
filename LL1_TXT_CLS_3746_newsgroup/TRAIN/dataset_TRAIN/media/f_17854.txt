[<text>
<title>hycor bio &lt;hybd.o&gt; forms unit to sell product</title>
<dateline>    fountain valley, june 1 - </dateline>hycor biomedical inc said it was
forming a unit, hycor instruments inc, to distribute a line of
chemistry analyzers made by &lt;jeol ltd&gt; of japan.
    the product, called a "clinalyzer," is an automated blood
analyzer capable of providing diagnostic and other tests for
hospitals and clinics, the company said.
    the company also said it has discussed distributing other
products made by jeol.

 reuter
</text>]