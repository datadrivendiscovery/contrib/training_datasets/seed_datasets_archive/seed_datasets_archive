[<text>
<title>japan asks banks to cut dollar sales - dealers</title>
<dateline>    tokyo, april 3 - </dateline>the finance ministry has asked japanese
commercial banks to moderate their dollar sales, bank dealers
said.
    they said the ministry had telephoned city and long-term
banks earlier this week to make the request.
    one dealer said this was the first time the ministry had
made such a request to commercial banks.
    finance ministry officials were unavailable for immediate
comment. dealers said the ministry has already asked
institutional investors to reduce their sales of the dollar.
   reuter
</text>]