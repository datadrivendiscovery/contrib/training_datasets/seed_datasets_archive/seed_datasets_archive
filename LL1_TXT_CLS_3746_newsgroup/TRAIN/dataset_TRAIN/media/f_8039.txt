[<text>
<title>sulpetro loss due to writedowns, asset disposals</title>
<dateline>    calgary, alberta, march 20 - </dateline>&lt;sulpetro ltd&gt; said its 1986
fiscal year net loss of 276.4 mln dlrs, or 19.22 dlrs per
share, was due to several factors, the largest of which was a
writedown of 125.0 mln dlrs of oil and gas properties.
    sulpetro also recorded a writeoff of deferred charges
amounting to 67.0 mln dlrs, a loss of 22.5 mln dlrs on the
disposal of all properties in the united kingdom and an equity
loss of 21.2 mln dlrs from affiliate sulbath exploration ltd.
    there was also a loss on other investments of 4.6 mln dlrs
and a loss on operations of 36.1 mln dlrs after interest,
depletion, depreciation and income tax recoveries.
    in the fiscal year ended october 31, 1985, sulpetro had a
net loss of 45.6 mln dlrs, or 3.90 dlrs per share.
    the company also said its non-recourse project financing
for the irish-lindergh heavy oil field remains in default due
to continuing low oil prices.
 reuter
</text>]