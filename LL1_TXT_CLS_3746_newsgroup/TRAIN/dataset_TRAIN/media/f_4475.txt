[<text>
<title>asamera inc &lt;asm&gt; year loss</title>
<dateline>    calgary, alberta, march 12 -
    </dateline>oper shr loss 48 cts vs profit 50 cts
    oper net loss 11.3 mln vs profit 18.1 mln
    revs 262.8 mln vs 399.7 mln
    note: 1986 net excludes tax gain of 1.1 mln dlrs or three
cts shr vs yr-ago gain of 5.6 mln dlrs or 17 cts shr.
    1986 net includes 15 mln dlr charge for reduction in
carrying value of refinery and related assets. 1985 net
includes 10.8 mln dlr gain on sale of canadian natural gas
property.
    u.s. dlrs.
 reuter
</text>]