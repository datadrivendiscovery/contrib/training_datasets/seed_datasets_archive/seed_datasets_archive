[<text>
<title>waste management &lt;wmx&gt; board okays modulaire buy</title>
<dateline>    oak brook, ill., june 1 - </dateline>waste management inc said its
directors approved a may 10 accord with modulaire industries
&lt;modx.o&gt; under which waste management would acquire modulaire.
    under the agreement, modulaire stockholders would receive
16 dlrs in waste management stock for each modulaire share.
    modulaire has scheduled a special shareholders meeting for
july 15 to vote on the merger. waste management said it has
received proxies from holders of 49.6 pct of modulaire's common
stock that could be voted in favor of the merger.
    the hart-scott-roding waiting period on the takeover will
expire june 17.
 reuter
</text>]