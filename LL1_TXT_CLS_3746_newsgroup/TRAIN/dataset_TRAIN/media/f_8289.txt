[<text>
<title>westworld community health &lt;wchi&gt; 4th qtr loss</title>
<dateline>    lake forest, calif., march 23 -
    </dateline>shr loss 15.23 dlrs vs profit 12 cts
    net loss 124,634,000 vs profit 882,000
    revs 38.4 mln vs 41.0 mln
    year
    shr loss 15.46 dlrs vs profit 48 cts
    net loss 126,434,000 vs profit 3,555,000
    revs 187.0 mln vs 133.2 mln
    avg shrs 8,177,000 vs 7,450,000
    note: current year results include charges related to
closing or divestitures of facilities and other assets.
    full name westworld community healthcare inc.
 reuter
</text>]