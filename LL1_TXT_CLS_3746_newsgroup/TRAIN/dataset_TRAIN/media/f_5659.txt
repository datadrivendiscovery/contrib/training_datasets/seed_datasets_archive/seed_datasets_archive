[<text>
<title>mcgraw hill &lt;mhp&gt; buys harper/row &lt;hpr&gt; unit</title>
<dateline>    new york, march 16 - </dateline>mcgraw-hill inc said it bought
&lt;medecines et sciences internationales sa&gt;, a french healthcare
publisher, from harper and row publishers inc.
    the sum of the deal was not disclosed.
    it said the french company publishes original titles by
french authors, as well as translations of american, british
and german medical books. the company will be consolidated with
mcgraw-hill france, it said.
 reuter
</text>]