[<text>
<title>entwistle &lt;entw.o&gt; has 5.1 pct of espey &lt;esp&gt;</title>
<dateline>    washington, april 24 - </dateline>entwistle co told the securities and
exchange commission it has acquired 62,500 shares of espey
manufacturing and electronics corp, or 5.1 pct of the total
outstanding common stock.
    entwistle, a hudson, mass., machinery maker and military
contractor, said it bought the stake for investment purposes
and has no plans to seek control of the company or to seek
representation on its board of directors.
    but entwistle said it has indicated its interest to espey
management in acquiring a family-held 19 pct stake in the
company in addition to its current stake.
 reuter
</text>]