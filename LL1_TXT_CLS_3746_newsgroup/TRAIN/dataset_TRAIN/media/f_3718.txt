[<text>
<title>bremer landesbank issues australian dollar bond</title>
<dateline>    london, march 11 - </dateline>bremer landesbank kreditanstalt finance
curacao nv is issuing a 40 mln australian dlr eurobond due
april 24, 1990 paying 15 pct and priced at 101-3/8 pct, lead
manager anz merchant bank ltd said.
    the issue, which is guaranteed by bremer landesbank
kreditanstalt, is available in denominations of 1,000 dlrs and
will be listed in luxembourg.
    fees comprise one pct selling concession and 1/2 pct for
management and underwriting. pay date is april 24.
 reuter
</text>]