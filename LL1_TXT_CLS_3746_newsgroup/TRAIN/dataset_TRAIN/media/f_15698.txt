[<text>
<title>jp industries &lt;jpi&gt; confirms proposal</title>
<dateline>    new york, april 9 - </dateline>jp industries inc confirmed the
announcement by clevite industries inc &lt;clev&gt; that jp
industries has submitted a proposal to clevite for the
acquisition of the company at 13.50 dlrs per share in cash.
    john psarouthakis, chairman and president of jp industries,
said that the company hopes to promptly negotiate an agreement
with the special committee of clevite's board.
    in february, jp industries said it purchased clevite's
engine parts division.
    jp industries said it is not aware of any other reason for
activity in its stock.
 reuter
</text>]