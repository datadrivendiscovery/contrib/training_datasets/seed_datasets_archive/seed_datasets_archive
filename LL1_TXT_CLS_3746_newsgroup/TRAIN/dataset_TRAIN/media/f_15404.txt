[<text>
<title>pergamon holdings reduces bpcc and hollis stakes</title>
<dateline>    london, april 9 - </dateline>&lt;pergamon holdings ltd&gt; and its associate
companies said that they had sold 30 mln ordinary shares in the
british printing and communication corp plc &lt;bpcl.l&gt; and 10.5
mln in &lt;hollis plc&gt; together with other securities.
    no total price was given but the company said the proceeds
of the sales would be used to fund pergamon's expansion
programme and worldwide acquisition stategy. the company said
that following these sales pergamon's ordinary shareholdings in
both bpcc and hollis remained above 51 pct. it said it had no
intention of further reducing its holdings in either company.
 reuter
</text>]