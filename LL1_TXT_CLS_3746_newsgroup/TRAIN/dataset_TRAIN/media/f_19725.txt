[<text>
<title>&lt;corrected&gt; - general instrument corp &lt;grl&gt; divi</title>
<dateline>    new york, june 29 -
    </dateline>qtly div 6.25 cts vs 6.25 cts prior
    pay oct 2
    record sept 1
    (corrects amount of qtly div in item that ran on friday jan
26)
 reuter
</text>]