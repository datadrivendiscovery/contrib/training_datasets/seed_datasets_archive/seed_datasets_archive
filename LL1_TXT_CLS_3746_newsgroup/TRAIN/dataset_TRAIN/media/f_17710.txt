[<text>
<title>rapid-american completes k mart &lt;km&gt; store buy</title>
<dateline>    new york, june 1 - </dateline>privately-held rapid-american corp said
it has completed the previously-announced acquisition of 66
kresge and jupiter stores from k mart corp.
    the company said it plans to operate 57 of the stores as
mccrory five and 10 variety stores and close the others by the
end of july.
 reuter
</text>]