[<text>
<title>currency instability will not last - sumita</title>
<dateline>    tokyo, april 1 - </dateline>bank of japan governor satoshi sumita said
the present foreign exchange market instability will not last
long as there is caution in the market regarding the rapid
decline of the u.s. unit.
    he told reporters the major currency nations are determined
to continue their concerted intervention whenever necessary to
stave off speculative dollar selling in line with their
february 22 currency stability agreement in paris.
    sumita also said he did not see the recent dollar drop as
anything like a free-fall.
 reuter
</text>]