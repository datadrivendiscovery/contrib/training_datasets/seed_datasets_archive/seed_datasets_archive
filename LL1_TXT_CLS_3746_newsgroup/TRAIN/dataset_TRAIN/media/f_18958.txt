[<text>
<title>corrected - crazy eddie &lt;crzy&gt; may be downgraded</title>
<dateline>    new york, june 18 - </dateline>moody's investors service inc said it
may downgrade crazy eddie inc's 81 mln dlrs of b-1 convertible
subordinated debentures.
    moody's cited crazy eddie's reduced profitability, recent
acquisition proposals for the company that could increase debt
leverage, and an uncertain earnings outlook.
    the agency said its review would consider the company's
concentration in the highly competitive new york marketplace,
as well as crazy's ability to maintain adequate bank financing.
    moody's corrects amount of debt from 65 mln dlrs.
 reuter
</text>]