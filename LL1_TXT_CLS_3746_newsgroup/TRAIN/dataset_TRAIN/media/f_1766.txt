[<text>
<title>irvine sensors &lt;irsn&gt; completes unit offering</title>
<dateline>    costa mesa, calif., march 4 - </dateline>irvine sensors corp said it
completed a public offering of 1.25 mln units, consisting of
two shares of common stock and one warrant to purchase an
additional share.
    the units were sold at a price of 1.60 dlrs each, irvine
sensors said, adding, the units will trade under the nasdaq
symbol "irsnu" and the new warrants under the symbol "irsnw."
 reuter
</text>]