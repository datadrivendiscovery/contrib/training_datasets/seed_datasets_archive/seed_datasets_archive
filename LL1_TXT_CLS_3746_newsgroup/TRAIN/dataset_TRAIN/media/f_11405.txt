[<text>
<title>telecast &lt;tcst&gt; completes acquisition financing</title>
<dateline>    fraser, mich., march 31 - </dateline>telecast inc said it closed on
the financing portion of its previously-announced acquisition
of approximately 14,600 hotel rooms from &lt;dynavision inc&gt;.
    the three mln dlr financing package was provided by sanwa
business credit corp, a subsidiary of &lt;sanwa bank ltd&gt; of
japan, the company said.
 reuter
</text>]