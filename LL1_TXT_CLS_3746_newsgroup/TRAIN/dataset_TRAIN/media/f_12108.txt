[<text>
<title>proposed offerings recently filed with the sec</title>
<dateline>    washington, april 1 - </dateline>the following proposed securities
offerings were filed recently with the securities and exchange
commission:
    southeast banking corp &lt;stb&gt; - offering of 50 mln dlrs of
convertible subordinated capital notes due 1999 through lazard
freres and co.
    burlington industries inc &lt;bur&gt; - offering of 100 mln dlrs
of sinking fund debentures due 2017 through kidder, peabody and
co inc and an offering of 75 mln dlrs of convertible
subordinated debentures due 2012 through an underwriting group
led by kidder, peabody.
 reuter
</text>]