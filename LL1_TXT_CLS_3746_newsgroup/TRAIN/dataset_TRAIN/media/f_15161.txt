[<text>
<title>u.k. gec declines comment on u.s. purchase rumour</title>
<dateline>    london, april 8 - </dateline>general electric co plc &lt;gecl.l&gt; (gec)
declined comment on rumours on the london stock market that it
is planning another purchase in the u.s. medical equipment
field, in addition to its existing u.s. subsidiary &lt;picker
international inc&gt;.
    a gec spokesman said that it is company policy not to
comment on acquisition rumours.
    stock exchange traders said the rumour helped gec's share
price to rise 5p, to a final 206p from yesterday's closing
price of 201p.
 reuter
</text>]