[<text>
<title>southwest bancorp &lt;swb&gt; has half of preferred</title>
<dateline>    vista, calif., april 17 - </dateline>southwest bancorp said through
yesterday, it had receioved 53,571 shares of its 2.50 dlr
cumulative convertible preferred stock in response to its offer
to exchange 11 common shares for each of the 108,710 shares of
the preferred.
    the company said the offer, which was to have expired
today, has been extended until may one.
 reuter
</text>]