[<text>
<title>ames &lt;add&gt; names new chief financial officer</title>
<dateline>    rocky hill, conn., april 1 - </dateline>ames department stores inc
said duane wolter has been named executive vice president and
chiedf financial officer, succeeding as chief financial officer
ralph m. shulansky.
    the company said shulansky remains senior vice
president-administration and investor relations.
    ames said yesterday that inventory shortages found at its
secaucus, n.j., distribution center would hurt results for the
year ended january 31.
    wolter had been senior vice president, finance, and chief
financial officer of &lt;rapid-american corp's&gt; mccrory corp unit.
 reuter
</text>]