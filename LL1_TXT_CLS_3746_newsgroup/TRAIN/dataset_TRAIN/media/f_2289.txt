[<text>
<title>cadbury requests stock exchange enquiry</title>
<dateline>    london, march 5 - </dateline>cadbury-schweppes plc &lt;cadb.l&gt; said it
had asked the london stock exchange to launch a formal enquiry
into dealings in the company's shares in recent months.
    it said it believed such a move was in the best interests
of shareholders following recent charges being made under u.k.
insider dealing laws about the shares.
    last week, former &lt;morgan grenfell group plc&gt; executive
geoffrey collier was charged with insider dealing in cadbury
shares.
    collier resigned from the bank last year and was later
charged with offences on deals in shares of ae plc &lt;aeng.l&gt;.
    a stock exchange spokeswoman said she could give no
specific details about the request. an investigation would be
requested from the exchange as it had access to the relevant
dealing records.
    if it appeared that some offence had been committed details
would be passed to the u.k. trade department, which had the
power to bring charges under the companies' act.
    no spokesman for cadbury was immediately available for
comment.
 reuter
</text>]