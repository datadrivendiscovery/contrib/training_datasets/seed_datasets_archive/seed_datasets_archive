[<text>
<title>perini corp &lt;pcr&gt; sets quarterly</title>
<dateline>    framingham, mass., march 11 -
    </dateline>qtly div 20 cts vs 20 cts prior
    pay june 16
    record may 22
 reuter
</text>]