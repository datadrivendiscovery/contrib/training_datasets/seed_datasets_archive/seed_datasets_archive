[<text>
<title>qintex says princeville &lt;pvdc&gt; in credit deal</title>
<dateline>    new york, march 20 - </dateline>&lt;qintex ltd&gt; of brisbane said it has
arranged for a leading australian financial institution it did
not name to provide princeville development corp with the
letter of credit required by the terms of qintex's proposed
acquisition of princeville.
    the company also said through yesterday, when its tender
offer for 3,300,000 princeville shares was to have expired, it
had received over seven mln princeville shares in the bid,
which has been extended until march 23 at 1800 est.
    qintex said the letter of credit is to insure payment of
princeville's contingent subordinated notes that would be
distributed to shareholders of record on the date immediately
following completion of the tender offer.
    the company said it believes the conditions of the
commitment letter will be satisfactory to princeville.  it said
princeville had previously held talks with a financial
institution on obtaining a letter of credit but was unable to
reach mutually acceptable terms.
 reuter
</text>]