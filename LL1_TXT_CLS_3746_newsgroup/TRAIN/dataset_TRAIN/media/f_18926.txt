[<text>
<title>first wisconsin &lt;fwb&gt; to sell building for gain</title>
<dateline>    milwaukee, june 18 - </dateline>first wisconsin corp said it has
agreed to sell its first wisconsin center in milwaukee and
adjacent property to trammell crow co for 195 mln dlrs,
resulting in a gain of 77 mln dlrs after tax.
    it said 36 mln dlrs of that amount will be included in 1987
earnings and the remainder will be accounted for over the next
10 years.
    first wisconsin said the transaction was valued at 195 mln
dlrs.
    first wisconsin said trammell crow officials said they
planned to build a high-rise, tower next to the 42-story
center, where first wisconsin will continut to have its
headquarters.
    in addition to the center, first wisconsin said the
property being sold includes two buildings directly east of the
center. first wisconsin said it will continue to occupy the
area, leasing the space back from trammell crow.
 reuter
</text>]