[<text>
<title>the dexter corp &lt;dex&gt; 3rd qtr net</title>
<dateline>    windsor locks, conn., oct 20 -
    </dateline>shr 41 cts vs 33 cts
    net 10.2 mln vs 8,309,000
    revs 193.3 mln vs 157.7 mln
    nine months
    shr 1.29 dlrs vs 1.02 dlrs
    net 32.1 mln vs 25.4 mln
    revs 582 mln vs 486.5 mln
 reuter
</text>]