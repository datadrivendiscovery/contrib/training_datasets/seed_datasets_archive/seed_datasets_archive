[<text>
<title>fed expected to set customer repurchases</title>
<dateline>    new york, march 16 - </dateline>the federal reserve is expected to
intervene in the government securities market to add reserves
via two to 2.5 billion dlrs of customer repurchase agreements,
economists said.
    economists said the fed will inject temporary reserves
indirectly to offset pressure on the federal funds rate
associated with quarterly corporate tax payments to the
treasury department.
    fed funds opened at 6-1/4 pct and remained at that level
late this morning. friday funds averaged 6.05 pct.
 reuter
</text>]