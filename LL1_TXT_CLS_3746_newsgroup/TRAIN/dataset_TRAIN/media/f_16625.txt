[<text>
<title>canandaigua wine co inc &lt;cdg.a&gt; 2nd qtr net</title>
<dateline>    canandaigua, n.y., april 13 - </dateline>qtr ended feb 28
    shr 35 cts vs 38 cts
    net 1,682,047 vs 1,817,820
    revs 36.1 mln vs 29.9 mln
    six mths
    shr 73 cts vs 75 cts
    net 3,518,515 vs 3,606,689
    revs 74.1 mln vs 62.7 mln
 reuter
</text>]