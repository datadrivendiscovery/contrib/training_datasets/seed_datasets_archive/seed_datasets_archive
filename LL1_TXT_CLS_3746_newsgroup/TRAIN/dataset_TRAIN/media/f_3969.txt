[<text>
<title>power corp details preferred share subscription</title>
<dateline>    montreal, march 11 - </dateline>(power corp of canada) said it
received subscriptions for 252,223 shares of its recent issue
of participating preferred shares for total proceeds of 4.3 mln
dlrs.
 reuter
</text>]