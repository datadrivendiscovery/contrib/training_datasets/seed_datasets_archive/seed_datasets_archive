[<text>
<title>mitsubishi electric to assemble pc's in u.s.</title>
<dateline>    tokyo, june 19 - </dateline>mitsubishi electric corp &lt;miet.t&gt; plans to
assemble personal computers in the u.s. to counteract the
imposition of a 100 pct import tax in april and a drop in
profits due to the yen's appreciation against the dollar, a
company spokesman told reuters.
    it will assemble 16-bit mp-286 and 32-bit mp-386 desk-top
computers at its wholly-owned computer and computer-related
equipment sales unit &lt;mitsubishi electronics america inc&gt; in
torrance, california at a rate if 10,000 a month, he said. this
will include 2,000 to 3,000 to be sold in the u.s. under the
mitsubishi name, he said without giving more details.
 reuter
</text>]