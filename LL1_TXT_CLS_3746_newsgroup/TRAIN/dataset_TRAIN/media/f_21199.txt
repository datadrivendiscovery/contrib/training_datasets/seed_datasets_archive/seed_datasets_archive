[<text>
<title>united savings and loan &lt;unsa.o&gt; 2nd qtr net</title>
<dateline>    greenwood, s.c., oct 19 - </dateline>sept 30
    shr 44 cts
    net 905,000 vs 631,000
    six months
    shr 88 cts
    net 1,793,000 vs 1,378,000
    assets 221 mln vs 223.2 mln
    deposits 186.4 mln vs 189.8 mln
    loans 176.5 mln vs.7 mln
 reuter
</text>]