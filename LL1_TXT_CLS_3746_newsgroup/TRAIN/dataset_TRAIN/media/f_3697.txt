[<text>
<title>bonneville pacific corp &lt;bpco&gt; 3rd qtr jan 31</title>
<dateline>    salt lake city, march 11 -
    </dateline>shr 52 cts vs 41 cts
    net 4,921,601 vs 3,157,070
    revs 35.8 mln vs 31.7 mln
    avg shrs 8,939,955 vs 7,600,000
    nine mths
    shr 52 cts vs 34 cts
    net 4,604,406 vs 2,585,621
    revs 36.0 mln vs 32.1 mln
    avg shrs 8,939,955 vs 7,600,000
 reuter
</text>]