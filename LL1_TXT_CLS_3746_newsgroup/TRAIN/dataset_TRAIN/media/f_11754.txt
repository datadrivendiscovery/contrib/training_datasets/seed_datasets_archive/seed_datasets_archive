[<text>
<title>microbiological sciences inc &lt;mbls&gt;4th qtr loss</title>
<dateline>    dallas, march 31 - 
    </dateline>oper shr loss 10 cts vs profit nine cts
    oper net loss 387,000 vs profit 313,000
    revs 6,486,000 vs 5,613,000
    year
    oper shr loss two cts vs profit four cts
    oper net loss 96,000 vs profit 120,000
    revs 23.8 mln vs 21.3 mln
    note: 1986 oper excludes extraordinary gains of 299,000 for
qtr and year.
 reuter
</text>]