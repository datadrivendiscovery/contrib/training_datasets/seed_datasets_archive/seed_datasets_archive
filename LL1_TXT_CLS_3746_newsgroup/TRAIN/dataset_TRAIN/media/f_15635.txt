[<text>
<title>ranger &lt;rgo&gt; sets terms of eurobond offer</title>
<dateline>    calgary, alberta, april 9 - </dateline>ranger oil ltd said its
previously reported public 75 mln u.s. convertible debenture to
be placed in the european market would mature april 28, 2002,
bear yearly interest of 6-1/2 pct and have a six u.s. dlr a
share conversion price.
    lead managers are credit suisse first boston ltd and
cazenove and co.
 reuter
</text>]