[<text>
<title>collins foods &lt;cf&gt; to redeem debentures</title>
<dateline>    los angeles, march 25 - </dateline>collins foods international inc
said that on april 30 it will redeem all of its outstanding 12
pct subordinated debentures due december 15, 2008.
    there are currently 30 mln dlrs principal amount of the
debentures outstanding, the company said.
 reuter
</text>]