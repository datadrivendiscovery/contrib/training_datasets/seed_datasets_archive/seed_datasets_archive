[<text>
<title>century bancorp files for stock offering</title>
<dateline>    somerville, mass., april 8 - </dateline>&lt;century bancorp inc&gt; said it
filed for an initial public offering of 1.6 mln shares of class
a common shares through an underwriting group managed by
moseley securities corp.
    the company said the class a shares are non-voting except
under certain limited conditions.
 reuter
</text>]