[<text>
<title>simmons &lt;simm&gt; february load factor declines</title>
<dateline>    chicago, march 17 - </dateline>simmons airlines inc said its load
factor for february declines to 41.8 pct from 46.1 pct a year
ago.
    traffic increased 31.3 pct to 15 mln revenue passenger
miles from 11.4 mln, while capacity increased 45.1 pct to 35.8
mln available seat miles from 24.7 mln.
 reuter
</text>]