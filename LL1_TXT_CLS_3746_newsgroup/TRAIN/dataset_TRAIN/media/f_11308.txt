[<text>
<title>d.o.c. optics corp &lt;doco&gt; 4th qtr loss</title>
<dateline>    southfield, mich., march 31 -
    </dateline>shr loss 11 cts vs profit 12 cts
    net loss 286,817 vs profit 292,014
    revs 9,972,379 vs 9,413,304
    year
    shr profit 63 cts vs profit 57 cts
    net profit 1,547,893 vs profit 1,481,703
    revs 43.9 mln vs 41.0 mln
    avg shrs 2,474,820 vs 2,617,768
 reuter
</text>]