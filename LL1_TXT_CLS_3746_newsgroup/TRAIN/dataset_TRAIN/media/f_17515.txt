[<text>
<title>occidental &lt;oxy&gt; sets debt retirement date</title>
<dateline>    los angeles, june 1 - </dateline>occidental petroleum corp said the
previously reported retirement of nine series of natural gas
pipeline co of america debt will occur on july two.
    retirement of the subsidiary's was announced by occidental
last week.
    it said notices of redeemptions and pre-payments will be
mailed shortly to all registered holders.
 reuter
</text>]