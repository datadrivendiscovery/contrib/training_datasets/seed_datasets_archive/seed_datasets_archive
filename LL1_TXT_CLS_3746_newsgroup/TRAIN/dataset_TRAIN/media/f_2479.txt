[<text>
<title>&lt;newscope resources ltd&gt; year loss</title>
<dateline>    calgary, alberta, march 5 -
    </dateline>shr loss 94 cts vs profit 28 cts
    net loss 6,319,337 vs profit 1,702,016
    revs 2,899,513 vs 5,239,106
    note: 1986 net includes 5,250,000 dlr writedown of oil and
gas properties.
 reuter
</text>]