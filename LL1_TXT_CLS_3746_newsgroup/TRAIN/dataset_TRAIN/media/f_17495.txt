[<text>
<title>genetech &lt;gene.o&gt; sees fda approval of drug</title>
<dateline>    south san francisco, june 1 - </dateline>genetech inc said it still
remains confident that it will be able to work with the u.s.
food and drug adminstration to obtain approval of its tissue
plasminogen activator activase for use in treating heart attack
victims.
    on friday an advisory committee of the fda recommended that
the blood clot dissolving drug not be approved until additional
mortality data be developed.
    genetech said it will provide the additional data and
believes that much if not all of it is already being developed.
 reuter
</text>]