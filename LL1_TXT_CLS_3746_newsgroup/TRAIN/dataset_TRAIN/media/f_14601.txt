[<text>
<title>dayton hudson &lt;xxx&gt; downgraded by moody's</title>
<dateline>    new york, april 7 - </dateline>moody's investors service inc said it
downgraded dayton hudson corp's 1.3 billion dlrs of debt.
    the agency cut to aa-3 from aa-2 dayton hudson's senior
notes, debentures, revenue bonds and eurodebt, and shelf
registration to provisional aa-3 from provisional aa-2.
    moody's said it expected that leverage, which has risen
because of an aggressive expansion program, would remain high
due to the company's spending plans. the agency noted that
dayton hudson's operating returns recently declined.
    but moody's also said the company's recent operating
problems would probably be temporary.
 reuter
</text>]