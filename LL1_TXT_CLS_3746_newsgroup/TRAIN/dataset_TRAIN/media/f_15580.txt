[<text>
<title>argentine grain belt weather report</title>
<dateline>    buenos aires, abr 9 - </dateline>argentine grain belt temperatures
(centigrade) and rain (mm) in the 24 hours to 12.00 gmt were:
    ...............max temp..min temp..rainfall
    buenos aires.......24.......12............0
    bahia blanca.......22........7............0
    tres arroyos.......22........8............0
    tandil.............22........7............0
    junin..............24.......11............0
    santa rosa.........--........6............0
    cordoba............23.......12............1
    santa fe...........21.......18...........17 reuter
</text>]