[<text>
<title>&lt;acc corp&gt; 3rd qtr net</title>
<dateline>    new york, oct 19 -
    </dateline>shr profit three cts vs profit nine cts
    net profit 102,136 vs profit 307,516
    revs 8,549,182 vs 8,469,476
    nine mths
    shr loss 13 cts vs profit 28 cts
    net loss 458,823 vs profit 1,014,969
    revs 25.5 mln vs 24.6 mln
 reuter
</text>]