[<text>
<title>first financial &lt;ffmc&gt; acquires tel-a-data</title>
<dateline>    atlanta, march 20 - </dateline>first financial management corp said it
acquired the data processing contracts and certain related
assets of tel-a-data l.p. for about 5.7 mln dlrs cash plus the
assumption of certain liabilities of about 2.5 mln dlrs.
    tel-a-data serves about 50 bank and thrift institutions
through a processing center in lombard, illinois.
    first financial offers data processing services to over 800
financial institutions through 35 data processing centers.
 reuter
</text>]