[<text>
<title>nissan mexicana to take over car engine exports</title>
<dateline>    tokyo, march 30 - </dateline>&lt;nissan mexicana sa de cv&gt;, owned 96.4
pct by nissan motor co ltd &lt;nsan.t&gt; and the rest by marubeni
corp &lt;mart.t&gt;, will supply all engines for manual transmission
"sentra" cars produced by nissan motor's wholly-owned subsidiary
&lt;nissan motor manufacturing corp usa&gt; (nmmc), a nissan
spokeswoman said.
    nissan's shift to mexican production from japan is due to
improved quality there and the yen's rise against the dollar.
    from july 1987, all car engines for nmmc will be supplied
by nissan mexicana. as a start, nissan mexicana shipped a total
of 17,000 car engines to nmmc last summer, the spokeswoman
said.
    nmmc in smyrna, tennessee, which has produced sentras since
march 1985, made 65,000 cars.
    nmmc will more than double sentra production to 140,000
this year including 80,000 to 90,000 manuals, she said.
 reuter
</text>]