[<text>
<title>philadelphia suburban &lt;psc&gt; buys software firm</title>
<dateline>    bryn mawr, pa., march 31 - </dateline>philadelphia suburban corp said
it acquired mentor systems inc, a lexington, ky., computer
software company, for common stock.
    detailed terms were not disclosed.
    mentor specializes in public sector accounting systems. it
has 73 employees at its lexington facility, four branch offices
in the midwest and one in new york.
 reuter
</text>]