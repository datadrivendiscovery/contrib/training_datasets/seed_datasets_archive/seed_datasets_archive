[<text>
<title>cannon &lt;can&gt; audit to show significant 1986 loss</title>
<dateline>    los angeles, march 12 - </dateline>the cannon group inc said its
financial statements will show substantial losses for fiscal
1986 and "significant downward adjustments in previously
reported stockholders' equity."
    the company also said its 1986 audit being conducted by
&lt;arthur young and co&gt; will cover the year ended january 3,
1987, instead of the nine-month period ended september 27, 1986
as previously announced.
    it said it anticipates the results of the audit will be
available in mid to late april 1987.
 reuter
</text>]