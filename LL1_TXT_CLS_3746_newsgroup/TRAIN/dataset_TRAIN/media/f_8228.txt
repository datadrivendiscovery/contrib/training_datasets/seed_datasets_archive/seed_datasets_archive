[<text>
<title>tandon &lt;tcor&gt; gets 50 mln dlr contract</title>
<dateline>    chatsworth, calif., march 23 - </dateline>tandon corp said it has
agreed in principle to sell to personal computer maker &lt;amstrad
plc&gt; during 1987 about 50 mln dlrs of 20 megabyte winchester
disk drives.
    it said shipments have already begun and about 30 mln dlrs
of the drives are scheduled for delivery during the first half.
 the drives are in half-height configuration.
 reuter
</text>]