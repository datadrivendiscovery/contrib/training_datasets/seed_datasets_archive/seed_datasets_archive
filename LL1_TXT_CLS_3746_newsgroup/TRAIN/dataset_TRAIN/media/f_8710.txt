[<text>
<title>sumita says bank will intervene if necessary</title>
<dateline>    tokyo, march 24 - </dateline>bank of japan governor satoshi sumita
said in a statement the central bank will intervene in foreign
exchange markets to stabilise exchange rates if necessary in
close cooperation with other major industrial nations.
    sumita said the bank will take adequate measures including
market intervention, if necessary, in line with the february 22
paris agreement by six major industrial nations.
    canada, britain, france, japan, the u.s. and west germany
agreed to cooperate in stabilising exchange rates around
current levels. sumita's statement was issued after the dollar
slipped below 150 yen to hit a record low of 148.40.
    "it is inevitable that exchange rates fluctuate under the
system of floating rates," sumita said.
    the fact the dollar plunged below 150 yen does not mean
anything significant under the floating system, he said.
    the six nations agreed in paris exchange rates prevailing
then were broadly consistent with underlying economic
fundamentals and further substantial rate shifts could damage
growth and adjustment prospects in their countries, the paris
statement said.
 reuter
</text>]