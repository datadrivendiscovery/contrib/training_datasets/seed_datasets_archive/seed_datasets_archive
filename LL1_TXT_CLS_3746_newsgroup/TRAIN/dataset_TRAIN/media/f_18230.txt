[<text>
<title>first tennessee &lt;ften&gt; sells 12-year notes</title>
<dateline>    new york, june 2 - </dateline>first tennessee national corp is
offering 75 mln dlrs of subordinated capital notes due 1999
yielding 10.43 pct, said lead manager goldman, sachs and co.
    the notes have a 10-3/8 pct coupon and were priced at 99.65
to yield 175 basis points more than comparable treasury
securities.
    non-callable to maturity, the debt is rated baa-2 by
moody's and bbb-plus by standard and poor's. the gross spread
is seven dlrs, the selling concession is four dlrs and the
reallowance is 2.50 dlrs. salomon brothers co-managed the deal.
 reuter
</text>]