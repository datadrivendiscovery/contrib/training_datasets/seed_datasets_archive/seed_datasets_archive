[<text>
<title>strong demand for u.s. corn in japan -- usda</title>
<dateline>    washington, april 1 - </dateline>japan appears to be relying less on
corn from china, argentina and south africa and more on
supplies from the united states, the u.s. agriculture
department said.
    in its world production and trade report, the department
said in the past seven weeks reported u.s. corn sales of nearly
three mln tonnes to japan are about three times the level
during this period last year.
    reports of short argentine supplies and the apparent
unwillingness of the chinese to sell at current world prices
may have caused japanese buyers to turn to the united states
for corn supplies, the department said.
 reuter
</text>]