[<text>
<title>j and j snack &lt;jjsf&gt; to sell convertible debt</title>
<dateline>    new york, march 19 - </dateline>j and j snack foods corp said it filed
with the securities and exchange commission a registration
statement covering a 25 mln dlr issue of convertible
debentures.
    the company said proceeds would be used for potential
future acquisitions.
 reuter
</text>]