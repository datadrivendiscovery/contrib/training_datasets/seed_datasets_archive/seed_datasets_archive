[<text>
<title>advanced tobacco &lt;atpi&gt; merger talks end</title>
<dateline>    san antonio, texas, march 9 - </dateline>advanced tobacco products inc
said it has ended talks on being acquired by sterling drug inc
&lt;sty&gt; but has resumed acquisition talks with other parties.
    the company had previously announced that a "major u.s.
based company" that it did not identify was evaluating its
nicotine technology.
    advanced said sterling's board has decided not to enter the
nicotine product market.
    it said it received a 200,000 dlr payment to deal
exclusively with sterling through march six.
    advanced said it had suspended merger talks with other
parties as a result of the exclusivity agreement.
 reuter
</text>]