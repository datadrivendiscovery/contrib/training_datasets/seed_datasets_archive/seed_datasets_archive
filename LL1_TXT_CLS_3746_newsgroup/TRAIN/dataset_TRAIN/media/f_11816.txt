[<text>
<title>singapore's uic to buy into teck hock coffee firm</title>
<dateline>    singapore, april 1 - </dateline>singapore's united industrial corp ltd
(uic) has agreed in principle to inject 16 mln dlrs in
convertible loan stock into &lt;teck hock and co (pte) ltd&gt;, a
creditor bank official said.
    uic is likely to take a controlling stake in the troubled
international coffee trading firm, but plans are not finalised
and negotiations will continue for another two weeks, he said.
    teck hock's nine creditor banks have agreed to extend the
company's loan repayment period for 10 years although a
percentage of the new capital injection will be used to pay off
part of the debt.
    teck hock owes more than 100 mln singapore dlrs and since
last december the banks have been allowing the company to
postpone loan repayments while they try to find an investor.
    the nine banks are oversea-chinese banking corp ltd, united
overseas bank ltd, banque paribas, bangkok bank ltd, citibank
n.a., standard chartered bank ltd, algemene bank nederland nv,
banque nationale de paris and chase manhattan bank na.
 reuter
</text>]