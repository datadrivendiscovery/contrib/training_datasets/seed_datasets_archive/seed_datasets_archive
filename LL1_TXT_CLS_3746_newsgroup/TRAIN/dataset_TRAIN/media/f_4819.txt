[<text>
<title>consumers financial corp &lt;cfin&gt; 1986 net</title>
<dateline>    camp hill, pa., march 13 -
    </dateline>shr 61 cts vs 42 cts
    net 6,247,000 vs 5,587,000
    rev 65.4 mln vs 53.6 mln
    note: 1986 net includes investment gains of 25 cts a share,
versus six cts a share for 1985, and extraordinary gain of
seven cts a share.
 reuter
</text>]