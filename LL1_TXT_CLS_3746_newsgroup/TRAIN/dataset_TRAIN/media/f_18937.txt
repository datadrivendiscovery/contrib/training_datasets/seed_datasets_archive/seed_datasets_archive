[<text>
<title>nynex corp &lt;nyn&gt; sets qtly dividend</title>
<dateline>    new york, june 18 - 
    </dateline>qtly div 95 cts vs 95 cts prior
    pay august three
    record june 30
 reuter
</text>]