[<text>
<title>egypt submits identical bid in eep wheat tender</title>
<dateline>    kansas city, june 15 - </dateline>egypt for the third time submitted a
bid of 89 dlrs per tonne in its tender for 200,000 tonnes of
soft red or white wheat for june-july delivery under the export
enhancement program, u.s. exporters said.
    usda has rejected that bid twice, the sources noted.
 reuter
</text>]