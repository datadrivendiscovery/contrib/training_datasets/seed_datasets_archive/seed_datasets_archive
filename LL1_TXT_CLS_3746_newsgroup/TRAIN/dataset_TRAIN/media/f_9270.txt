[<text>
<title>vanguard technologies international inc &lt;vti&gt;</title>
<dateline>    fairfax, va., march 25 -
    </dateline>shr 19 cts vs 10 cts
    net 653,464 vs 287,606
    revs 10.6 mln vs 7,600,000
    year
    shr 68 cts vs 46 cts
    net 2,309,181 vs 1,408,813
    revs 38.4 mln vs 26.0 mln
 reuter
</text>]