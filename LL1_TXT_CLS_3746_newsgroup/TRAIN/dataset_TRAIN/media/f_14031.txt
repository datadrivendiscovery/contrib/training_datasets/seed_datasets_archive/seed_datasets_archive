[<text>
<title>portuguese airline confirms airbus a310 order</title>
<dateline>    london, april 7 - </dateline>portugal's "flag carrier" airline tap has
confirmed an order for three airbus industrie &lt;ainp.pa&gt;
a310-300 passenger aircraft, british aerospace plc &lt;bael.l&gt;
(bae), one of four shareholders in the international airbus
consortium, said.
    bae said in a statement tap had also taken options on
another two airbus aircraft, either the existing a310-300
medium range craft or the long-range four-engined a340 which
airbus hopes to launch in 1992, depending on the airline's
needs.
    bae said the firm order confirmed a commitment taken by tap
in january. details of the value of the contract, delivery
dates or which engine would power the aircraft were not
available.
 reuter
</text>]