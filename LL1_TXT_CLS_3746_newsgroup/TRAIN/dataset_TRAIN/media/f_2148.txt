[<text>
<title>&lt;signtech inc&gt; nine mths jan 31 net</title>
<dateline>    mississauga, ontario, march 5 -
    </dateline>shr 55 cts vs 24 cts
    net 1.9 mln vs 800,000
    revs 17.6 mln vs 12.8 mln
 reuter
</text>]