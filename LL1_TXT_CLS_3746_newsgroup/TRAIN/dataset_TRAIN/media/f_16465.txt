[<text>
<title>arrays &lt;aray&gt; completes merger</title>
<dateline>    van nuys, calif., april 13 - </dateline>arrays inc said it completed
its merger with haba systems inc.
    terms of the merger called for each share of arrays to be
exchange for a share of haba in a transaction valued at 4.1 mln
dlrs, the company said.
    both companies produce and market microcomputer software.
 reuter
</text>]