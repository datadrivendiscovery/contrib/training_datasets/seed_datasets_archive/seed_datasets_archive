[<text>
<title>bell group confirms standard chartered stake</title>
<dateline>    perth, april 8 - </dateline>the bell group ltd &lt;blla.s&gt; said it now
holds 14.9 pct of the issued capital of standard chartered plc
&lt;stch.l&gt; after acquiring further shares.
    the one-sentence statement from bell's headquarters
confirmed what its brokers warburg securities told reuters in
london yesterday.
    bell previously held 10 pct of standard.
    bell chairman robert holmes a court, who is also a director
of standard, was not available for comment on his company's
intentions in boosting its holding and other company officials
contacted here by reuters declined to comment.
 reuter
</text>]