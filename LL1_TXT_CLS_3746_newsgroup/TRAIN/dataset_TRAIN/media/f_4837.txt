[<text>
<title>penril &lt;pnl&gt; elects president, sets financing</title>
<dateline>    rockville, md., march 13 - </dateline>penril corp said its board
elected henry david epstein president, chief executive officer
and a director.
    the company said epstein and other investors have agreed to
invest 1.5 mln dlrs in the company's shares and warrants
subject to satisfaction of certain conditions.
    penril said its founder, alva t. bonda, remains as
chairman.
    it noted epstein is chairman of computer communications inc
&lt;ccmm&gt;. he is a former senior executive of loral corp &lt;lor&gt; and
of texas instruments inc &lt;txn&gt;.
 reuter
</text>]