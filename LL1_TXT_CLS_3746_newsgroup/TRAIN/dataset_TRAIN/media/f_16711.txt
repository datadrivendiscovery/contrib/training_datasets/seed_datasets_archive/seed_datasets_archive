[<text>
<title>convergent technologies &lt;cvgt.o&gt; sees qtr loss</title>
<dateline>    san jose, calif., april 13 - </dateline>convergent technologies inc
said it expects to report in the first quarter a loss more than
twice the size of the 4.8-mln-dlr loss reported in the fourth
quarter of 1986.
    convergent reported a first quarter 1986 profit of
2,100,000 dlrs, or five cts per share.
    the company said results declined in the quarter both in
its traditional oem business and its business systems group.
    the anticipated loss reflects lower than expected operating
margins, start-up costs for new product manufacturing and
higher than planned expenses.
 reuter
</text>]