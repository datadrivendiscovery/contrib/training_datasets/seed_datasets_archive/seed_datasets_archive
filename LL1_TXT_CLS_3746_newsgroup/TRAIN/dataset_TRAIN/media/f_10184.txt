[<text>
<title>cpi &lt;cpic&gt; to sell att &lt;t&gt; computer products</title>
<dateline>    st. louis, march 26 - </dateline>cpi corp said american telephone and
telegraph has authorized its business telephone systems
division to represent its computer product line to the st.
louis and chicago business markets.
    cpi said the products included in the agreement are
computers, printers, telephone modems and fax machines.
 reuter
</text>]