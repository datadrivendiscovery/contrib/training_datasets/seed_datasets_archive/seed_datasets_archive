[<text>
<title>swissair &lt;swsz.z&gt; year 1986</title>
<dateline>    zurich, march 19 -
    </dateline>net 64.5 mln swiss francs vs 68.5 mln
    div 33 francs per share vs 38
    turnover 4.03 billion vs 4.35 billion.
 reuter
</text>]