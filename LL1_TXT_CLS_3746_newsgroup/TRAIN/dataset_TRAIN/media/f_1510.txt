[<text>
<title>data architects &lt;drch&gt; gets tokyo bank contract </title>
<dateline>    waltham, mass., march 4 - </dateline>data architects inc said it
received a contract from the &lt;bank of tokyo&gt; to expand the
installation of its bank electronic support systems at the
bank.
    it said the contract exceeds one mln dlrs.
 reuter
</text>]