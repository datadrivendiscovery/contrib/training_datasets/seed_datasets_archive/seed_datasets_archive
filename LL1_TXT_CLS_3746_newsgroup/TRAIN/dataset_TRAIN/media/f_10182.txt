[<text>
<title>boston edison &lt;bse&gt; executives retiring</title>
<dateline>    boston, march 26 - </dateline>boston edison co chairman stephen
sweeney said two of the company's senior officers will take
early retirements, and the company is looking for a new
president and chief financial officer.
    sweeney, who also holds the titles of president and chief
executive officer, said james lydon, executive vice president
and chief operating officer, joseph tyrrell, executive vice
president and chief financial officer, have asked to take early
retirements following a transition period.
    lydon and tyrrell have each been with the company more than
35 years, sweeney said.
 reuter
</text>]