[<text>
<title>maxicare &lt;maxi.o&gt; ends plans to sell unit</title>
<dateline>    birmingham, ala., june 19 - </dateline>maxicare health plans inc  said
it ended negotiations to sell maxicare alabama l.p. to
(complete health inc).
    the companies said last week they had reached agreement in
principle for complete health to purchase the health
maintenance organization, the largest in alabama.
    maxicare said the terms would not be in the best interests
of the heatlh care providers who have built the alabama hmo.   
 it said it now intends to work with its health care providers
in alabama to further develop the hmo.
 reuter
</text>]