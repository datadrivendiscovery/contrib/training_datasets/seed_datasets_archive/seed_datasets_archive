[<text>
<title>gte corp &lt;gte&gt; 1st qtr mar 31</title>
<dateline>    stamford, conn, april 13 -
    </dateline>shr 78 cts vs 86 cts
    net 265.0 mln vs 283.0 mln
    revs 3.7 billion vs 3.6 billion
    avg shrs 329.0 mln vs 319.0 mln
                    
 reuter
</text>]