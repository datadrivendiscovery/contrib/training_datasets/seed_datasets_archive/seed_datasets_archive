[<text>
<title>rocky mountain medical corp &lt;rmedu&gt; 1st qtr loss</title>
<dateline>    greenwood village, colo., march 6 - </dateline>dec 31
    shr loss one ct
    net loss 176,639
    revs 150,300
    note: company went public in april 1986.
 reuter
</text>]