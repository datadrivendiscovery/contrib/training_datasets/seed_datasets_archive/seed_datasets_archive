[<text>
<title>moody's upgrades southwest airlines &lt;luv&gt; unit</title>
<dateline>    new york, april 9 - </dateline>moody's investors service said it has
raised its ratings on about 50 mln dlrs of outstanding
securities of southwest airlines co unit, transstar airlines
corp, formerly named musa air corp.
    the agency has up the company's senior secured debt to ba2
from b2.
    the action reflects the improvement in the company's
financial position since its acquisition by southwest.
    southwest operates transstar as a separate unit and has not
legally assumed its debt.
   
 reuter
</text>]