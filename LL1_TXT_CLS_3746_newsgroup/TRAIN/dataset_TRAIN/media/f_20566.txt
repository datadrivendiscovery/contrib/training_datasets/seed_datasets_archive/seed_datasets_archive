[<text>
<title>pittsburgh and west virginia &lt;pw&gt; 3rd qtr net</title>
<dateline>    pittsburgh, oct 20 -
    </dateline>shr 14 cts vs 14 cts
    net 213,000 vs 210,000
    revs 229 mln vs 229 mln
    nine months
    shr 42 cts vs 42 cts
    net 630,000 vs 628,000
    revs 689,000 vs 689,000
    note: full name pittsburgh and west virginia railroad.
 reuter
</text>]