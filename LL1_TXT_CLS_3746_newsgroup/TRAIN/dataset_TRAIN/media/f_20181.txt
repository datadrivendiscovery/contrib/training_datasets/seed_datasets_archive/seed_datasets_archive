[<text>
<title>ust inc &lt;ust&gt; 3rd qtr net</title>
<dateline>    greenwich, conn., oct 20 -
    </dateline>shr 60 cts vs 48 cts
    net 35.0 mln vs 27.8 mln
    revs 147.2 mln vs 131.6 mln
    nine mths
    shr 1.67 dlrs vs 1.38 dlrs
    net 97.3 mln vs 77.9 mln
    revs 422.4 mln vs 385.5 mln
    avg shrs 58.4 mln vs 56.5 mln
 reuter
</text>]