[<text>
<title>isuzu denies plans to import gm cars to japan</title>
<dateline>    tokyo, june 19 - </dateline>isuzu motors ltd &lt;isum.t&gt; has no plans to
import cars made by general motors corp &lt;gm.n&gt; to japan, an
isuzu spokesman told reuters.
    the japanese daily yomiuri shimbun reported that isuzu had
decided to import cars directly from gm.
    each month isuzu's domestic distributors sell five to 10
cars from gm's buick, chevrolet, and oldsmobile range.
    the cars are supplied by yanase and co ltd, a japanese
importer and distributor.
    isuzu is owned 38.6 pct by gm.
 reuter
</text>]