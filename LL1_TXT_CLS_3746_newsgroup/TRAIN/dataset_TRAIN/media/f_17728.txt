[<text>
<title>piosec &lt;pio.al&gt; acquires semiconductor stake</title>
<dateline>    vancouver, b.c., may 31 - </dateline>piosec technology ltd said it
exchanged 4.5 mln common shares for 21 pct of privately owned
&lt;alliance semiconductor corp&gt; of santa clara, calif.
    followin the acquisition, a piosec spokesman said, the
company has 6.5 mln shares outstanding.
 reuter
</text>]