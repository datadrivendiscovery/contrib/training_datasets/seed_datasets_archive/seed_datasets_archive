[<text>
<title>united illuminating co &lt;uil&gt; two months feb 28</title>
<dateline>    new haven, conn., march 23 -
    </dateline>shr 1.51 dlr  vs 1.08 dlr
    net 23.1 mln vs 18.2 mln
    oper revs 81.1 mln vs 86.5 mln
    12 mths
    shr 6.41 dlrs vs 5.77 dlrs
    net 106.5 mln vs 99.2 mln
    oper revs 465.8 mln vs 509.8 mln
    note: 1987 periods do not reflect the terms of earnings
stipulation agreement among the company and various departments
submitted for approval on march 18.
   
 reuter
</text>]