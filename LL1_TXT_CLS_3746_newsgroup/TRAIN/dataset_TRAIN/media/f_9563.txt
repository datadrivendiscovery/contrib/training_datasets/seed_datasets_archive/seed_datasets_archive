[<text>
<title>medar &lt;mdxr&gt; changes fiscal year</title>
<dateline>    farmington hills, mich., march 25 - </dateline>medar inc said it
changed the end of its fiscal year to december 31 from march
31.
    the company, in reporting its annual results, said the
change was made to bring its financial reporting in phase with
the order cycle of its major customers.
    medar earlier said it lost 558,800 dlrs in its final 1986
quarter, compared to a loss of 469,200 dlrs in the same 1985
quarter.
 reuter
</text>]