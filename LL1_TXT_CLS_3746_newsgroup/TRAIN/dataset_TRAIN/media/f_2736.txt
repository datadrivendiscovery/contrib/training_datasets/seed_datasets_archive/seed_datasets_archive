[<text>
<title>hadson &lt;hads&gt; plans 3,750,000 share offering</title>
<dateline>    oklahoma city, march 6 - </dateline>hadson corp said it intends to
file a registration with the securities and exchange commission
within one week covering an offering of 3,750,000 common
shares.
    the company had 14,260,000 shares outstanding on december
31 and 75.0 mln shares authorized.
    it said proceeds of the offering are intended to be used to
finance expansion of existing operations, for acquisitions and
for working capital.
 reuter
</text>]