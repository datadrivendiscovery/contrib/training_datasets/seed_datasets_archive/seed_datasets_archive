[<text>
<title>&lt;virtusonics corp&gt; extends exercise period</title>
<dateline>    new york, june 19 -</dateline>virtusonics corp said it extended to
july 31 the period to exercise its outstanding warrants at 1.5
cts a share.
    the company on june 18 said it had reduced the exercise
price of its warrants from 2.5 cts a common share for the
period june 22 to july 22.
 reuter
</text>]