[<text>
<title>conagra takes over west german trade house</title>
<dateline>    bremen, april 27 - </dateline>one of west germany's major feedstuff
and grain traders, kurt a. becher gmbh and co kg, said conagra
inc of the u.s. was taking it over, effective june 1.
    becher said conagra, which already owns 50 pct of the trade
house, would take over the remaining 50 pct from the becher
family. further details were not immediately available.
    conagra, based in omaha, nebraska, is a foodstuff company
and has access to world markets through its agricultural trade
subsidiary, conagra international.
 reuter
</text>]