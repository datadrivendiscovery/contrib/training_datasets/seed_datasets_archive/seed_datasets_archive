[<text>
<title>roy f. weston wins government contract</title>
<dateline>    west chester, pa., march 27 - </dateline>roy f. weston inc said it was
awarded a three-year contract valued at three mln dlrs to
provide technical assistance to the u.s. environmental
protection agency.
    the company said it is a subcontractor to sanford cohen and
associates of mclean, va.
 reuter
</text>]