[<text>
<title>kelley oil &lt;kly&gt; buying oil properties</title>
<dateline>    houston, june 18 - </dateline>kelley oil and gas partners ltd said it
has agreed to purchase all of cf industries inc's oil and
natural gas properties for about 5,500,000 dlrs, effective july
1.
    it said the louisiana properties had proven reserves at
year-end of 11 billion cubic feet of natural gas and 85,000
barrels of oil, condensate and natural gas liquids.
    kelley said it currently owns working interests in some of
the properties.
 reuter
</text>]