[<text>
<title>video library inc &lt;vlvl&gt; 4th qtr loss</title>
<dateline>    san diego, calif.,march 17 -
    </dateline>shr loss two cts vs profit three cts
    net loss 59,299 vs profit 88,843
    revs 3,487,693 vs 2,123,488
    year
    shr profit 25 cts vs loss two cts
    net profit 816,395 vs loss 44,541
    revs 12.2 mln vs 7,413,328
    avg shrs 3,208,472 vs 2,348,559
 reuter
</text>]