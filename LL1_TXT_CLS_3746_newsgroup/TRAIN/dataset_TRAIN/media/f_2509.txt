[<text>
<title>arizona appetitio's &lt;aapi&gt; to sell franchise</title>
<dateline>    phoenix, ariz., march 5 - </dateline>arizona appetito's stores inc
said it has tentatively agreed to sell its franchise operation
to privately-held appetito's inc.
    under the agreement, appetito's inc would acquire all
arizona appetito's assets except for 25,000 dlrs in cash and
assume all liabilities for 1,018,000 shares of arizona
appetito's stock and 150,000 dlrs in a three-year secured
promissory note.
    after the transaction, which is subject to shareholder
approval, arizona appetito's said it proposes to seek to merge
or acquire an existing private company in the food sector.
 reuter
</text>]