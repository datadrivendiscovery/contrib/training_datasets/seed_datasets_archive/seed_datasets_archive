[<text>
<title>&lt;c.h. beazer holdings plc&gt; to offer u.s. shares</title>
<dateline>    new york, april 1 - </dateline>c.h. beazer holdings plc said it has
filed with the u.s. securities and exchange commission for an
offering of 6,250,000 american depositary shares representing
25.0 mln ordinary shares, or a 9.5 pct interest in beazer,
through underwriters led by american express co's &lt;axp&gt;
shearson lehman brothers inc and robinson-humphrey co inc.
    the company said the ads's are expected to be traded on the
nasdaq system.  beazer will grant underwriters an option to
purchase another 937,500 ads's to cover overallotments.
    beazer said it will use proceeds initially to reduce debt
and then for acquisitions and capital spending.
 reuter
</text>]