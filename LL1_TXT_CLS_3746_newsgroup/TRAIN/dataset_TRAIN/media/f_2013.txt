[<text>
<title>japan fund &lt;jpn&gt; seekers confident of financing</title>
<dateline>    new york, march 5 - </dateline>the &lt;sterling grace capital management
l.p.&gt; group said it is confident financing can be arranged if
the japan fund's board recommend's the group's acquisition
proposal.
    the group, which also includes &lt;anglo american security
fund l.p.&gt; and t.b. pickens iii, tuesday proposed an entity it
controls acquire for cash all the assets of japan fund for 95
pct of the fund's aggregate net asset value.
    the group said it has had a number of meetings over the
past few days with domestic and overseas financial institutions.
    the sterling grace capital group said certain of these
institutions have expressed serious interest in providing
financing for the proposed acquisition of japan fund, "adding
we are reasonably confident that the financing can be quickly
arranged if the japan fund's board of directors is willing to
recommend the transaction to shareholders."
 reuter
</text>]