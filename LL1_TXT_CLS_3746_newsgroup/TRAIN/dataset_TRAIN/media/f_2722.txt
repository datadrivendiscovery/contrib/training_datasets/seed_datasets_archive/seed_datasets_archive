[<text>
<title>cie financiere de paribas &lt;pari.pa&gt; 1986 year</title>
<dateline>    paris, march 6 -
    </dateline>parent company net profit 385 mln francs vs 226.9 mln
    dividend five francs vs no comparison
    note - the financial and banking group was privatised by
the government in january this year.
 reuter
</text>]