[<text>
<title>liberian ship grounded in suez canal refloated</title>
<dateline>    london, march 27 - </dateline>a liberian motor bulk carrier, the
72,203 dw tonnes nikitas roussos, which was grounded in the
suez canal yesterday, has been refloated and is now proceeding
through the the canal, lloyds shipping intelligence said.
 reuter
</text>]