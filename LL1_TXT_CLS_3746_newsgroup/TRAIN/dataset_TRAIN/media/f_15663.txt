[<text>
<title>varity &lt;vat&gt; capital restatement approved</title>
<dateline>    toronto, april 9 - </dateline>&lt;varity corp&gt; said shareholders approved
a previously reported proposal to transfer 137.5 mln u.s. dlrs
to the contributed surplus account on the company's balance
sheet from the stated capital account for common shares.
    the company said the move would allow it to meet canadian
government tests related to dividend payments.
 reuter
</text>]