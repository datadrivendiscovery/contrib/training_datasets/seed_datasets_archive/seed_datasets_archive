[<text>
<title>guinness sues boesky in federal court</title>
<dateline>    new york, march 24 - </dateline>&lt;guinness plc&gt; has joined investors
suing former wall street speculator ivan boesky, alleging it
was deceived into putting money into his one billion dlr
investment partnership in 1986.
    guinness, the largest limited partner in ivan f. boeksy and
co. l.p., is the latest to file suit in federal court in
manhattan against boesky, court papers show.
    about 40 other investors have also filed suit over similar
allegations, including that boesky did not reveal his illegal
insider trading activities.
    guinness is charging it was induced to join in the boesky
partnership through a prospectus that contained "material untrue
statements and omissions."
    the suit also alleged that the boesky corporation, which
became a part in the formation of the investment partnership,
ivan f. boesky and co., l.p., "had achieved its extraordinary
rates of return as a result of trading on inside information
and other violations of the securities laws."
    in addition, the suit charged that boesky and other
defendants unlawfully "schemed with and provided substantial
assistance to one another to evade the registration provisions"
of securities law.
 reuter
</text>]