[<text>
<title>stanley works &lt;swk&gt; sets quarterly</title>
<dateline>    new britain, conn., march 4 -
    </dateline>qtly div 19 cts vs 19 cts prior
    pay march 31
    record march 12
 reuter
</text>]