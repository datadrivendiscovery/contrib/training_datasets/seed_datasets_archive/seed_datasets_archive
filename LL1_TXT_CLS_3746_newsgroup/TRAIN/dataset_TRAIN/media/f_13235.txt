[<text>
<title>s. korea orders mergers of troubled companies</title>
<dateline>    seoul, april 4 - </dateline>south korea ordered the takeover of six
financially troubled overseas construction firms and two
shipping companies under the government's industrial
realignment programme, finance ministry officials said.
    they said the industrial policy deliberation committee
decided to allow concessionary loans to three firms taking over
the eight. the companies involved would be exempted from
corporate tax and property transfer and registration taxes.
    under the programme, five subsidaries of &lt;chung woo
development co ltd&gt; will be taken over by &lt;byuck san corp&gt;,
&lt;korea develpoment corp&gt; by &lt;daelim industrial co ltd&gt;, and two
subsidaries of &lt;korea shipping corp&gt; by the &lt;hanjin group&gt;.
    another five construction companies were ordered to improve
their financial status by reorganising their subsidiaries or
selling off unproductive real estate holdings to help repay
bank loans, the officials said.
    these are &lt;pacific construction co ltd&gt;, &lt;life construction
co ltd&gt;, &lt;chin heung international co ltd&gt;, &lt;samick co ltd&gt; and
&lt;hanshin construction co ltd&gt;.
    a further five shipping companies -- &lt;hanjin container line
ltd&gt;, &lt;choyang shipping co ltd&gt;, &lt;hyundai merchant marine co
ltd&gt;, &lt;pan ocean shipping co&gt; and &lt;dooyang line co ltd&gt; -- will
be allowed to defer the repayment of bank loans and interest,
but details have yet to be finalised, the officials said.
    they said the government would take similar measures for
more companies in august in line with the continuing
realignment policy, which had already affected 56 firms last
year.
 reuter
</text>]