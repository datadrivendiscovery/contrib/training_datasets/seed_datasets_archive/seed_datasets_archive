[<text>
<title>&lt;new harding group inc&gt; 1st qtr jan 31 net</title>
<dateline>    brantford, ontario, march 31 -
    </dateline>shr 19 cts
    net 653,000
    revs 45.6 mln
    note: prior results not given due to november, 1986
acquisition of 56 pct stake in continuous colour coat ltd
 reuter
</text>]