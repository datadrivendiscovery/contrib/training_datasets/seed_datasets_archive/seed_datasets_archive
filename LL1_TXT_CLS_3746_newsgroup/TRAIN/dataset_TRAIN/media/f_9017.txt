[<text>
<title>eaton (etn) gets 53.0 mln dlr contract</title>
<dateline>    washington, march 24 - </dateline>eaton corp's ail division has
received a 53.0 mln dlr contract for jamming system work for
the ea-6b electronic warfare aircraft, the navy said.
 reuter
</text>]