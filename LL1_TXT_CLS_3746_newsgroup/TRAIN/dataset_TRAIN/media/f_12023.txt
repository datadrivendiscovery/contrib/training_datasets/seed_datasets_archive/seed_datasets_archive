[<text>
<title>american motors &lt;amo&gt; march output falls</title>
<dateline>    southfield, mich., april 1 - </dateline>american motors corp said it
produced 2,578 u.s. cars in march, down from 5,922 in the same
1986 month.
    the automaker's march u.s. jeep output was 21,390, up from
16,215 in the same month last year.
    year-to-date u.s. car production by american motors through
the end of march was 8,647 vehicles compared to 12,553 in the
same 1986 period. year-to-date jeep output was 58,597 compared
to 56,801.
                                     
 reuter
</text>]