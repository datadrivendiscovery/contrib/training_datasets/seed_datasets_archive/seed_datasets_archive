[<text>
<title>usp real estate &lt;uspts&gt; has gain on sale</title>
<dateline>    cedar rapids, iowa, march 16 - </dateline>usp real estate investment
trust said it will post a first-quarter gain of 2,258,216 dlrs
on the sale of the spanish villa apartments in savannah, ga.,
which was completed last week.
 reuter
</text>]