[<text>
<title>jordan, sudan sign 100 mln dlr barter trade pact</title>
<dateline>    amman, march 19 - </dateline>jordan and sudan signed a barter trade
agreement under which they will exchange 100 mln dlrs' worth of
goods a year, sudanese officials said.
    they said sudan will export corn, sesame, peanuts, spices
and cow hides, while jordan will export cement, tomato puree,
chemicals and pharmaceuticals.
 reuter
</text>]