[<text>
<title>american building maintenance &lt;abm&gt; 1st qtr net</title>
<dateline>    san francisco, march 16 -
    </dateline>shr 35 cts vs 44 cts
    net 1,311,000 vs 1,619,000
    revs 125.2 mln vs 117.2 mln
    note: american building maintenance industries inc.
 reuter
</text>]