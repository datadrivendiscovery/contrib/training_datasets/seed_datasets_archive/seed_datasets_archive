[<text>
<title>audiotronics corp &lt;ado&gt; to offer debt</title>
<dateline>    los angeles, march 4 - </dateline>audiotronics corp said it registered
with the securities and exchange commission to offer five mln
dlrs of convertible subordinated debentures due 2002.
    h.j. meyers and co inc will manage the underwriting of the
offer.
    the company said proceeds will be used to repay bank debt
and for working capital.
 reuter
</text>]