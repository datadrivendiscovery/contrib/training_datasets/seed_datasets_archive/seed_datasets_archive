[<text>
<title>dayton hudson &lt;dh&gt; march sales off 4.9 pct</title>
<dateline>    minneapolis, april 9 - </dateline>dayton hudson corp said retail sales
for the five weeks ended april four were 791.8 mln dlrs
compared to 755.6 mln dlrs a year ago. on a comparable store
basis, it said sales declined 4.9 pct.
    sales for the nine months were 1.39 billion dlrs, up from
1.26 billion dlrs in the same 1986 period. on a comparable
store basis, the sales rose 1.2 pct.
 reuter
</text>]