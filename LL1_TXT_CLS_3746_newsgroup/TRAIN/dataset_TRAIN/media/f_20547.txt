[<text>
<title>redken laboratories &lt;rdkn.o&gt; to buy back stock</title>
<dateline>    canoga park, calif., oct 20 - </dateline>redken laboratories inc said
its board authorized management to repurchase up to 600,000 of
the company's common shares.
    the repurchase program may be implimented over the next 18
months, or sooner, depending on whether or not the shares are
bought in the open market or in privately negotiated
transactions, redken said.
    the program will be funded with cash on hand and future
cash flow, the company also said.
 reuter
</text>]