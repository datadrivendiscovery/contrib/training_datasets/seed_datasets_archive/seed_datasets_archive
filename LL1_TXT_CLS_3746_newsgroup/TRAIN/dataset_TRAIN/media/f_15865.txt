[<text>
<title>usda forecasts brazil/argentine soybean crops</title>
<dateline>    washington, april 9 - </dateline>the u.s. agriculture department
forecast brazil's 1986/87 soybean crop at 17.00 mln tonnes, vs
17.00 estimated last month. it put the 1985/86 crop at 13.90
mln tonnes, vs 13.70 mln last month.
    the department forecast argentina's 1986/87 soybean crop at
7.70 mln tonnes, vs 7.70 mln last month. it projected the
1985/86 crop at 7.30 mln tonnes, vs 7.30 mln last month.
    brazil's 1986/87 soybean exports were forecast at 2.50 mln
tonnes, vs 2.50 mln tonnes estimated last month. usda projected
1985/86 exports at 1.20 mln tonnes, vs 1.20 mln last month.
 reuter
</text>]