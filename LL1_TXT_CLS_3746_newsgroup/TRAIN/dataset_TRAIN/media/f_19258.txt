[<text>
<title>megavest to acquire computer trade development</title>
<dateline>    new york, june 19 - </dateline>&lt;megavest industries inc&gt; said it has
agreed in principle to acquire unlisted computer trade
development corp in exchange for 119 mln shares of its common
stock.
    the company said it has about 21.5 mln shares outstanding.
    it said computer trade had revenues of about 6.1 mln dlrs
last year.
 reuter
</text>]