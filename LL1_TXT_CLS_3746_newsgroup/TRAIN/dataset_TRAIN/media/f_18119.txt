[<text>
<title>wharf resources &lt;wfra.o&gt; sets deal with nevard</title>
<dateline>    calgary, alberta, june 2 - </dateline>wharf resources ltd said it has
reached a verbal agreement with &lt;nevada packard&gt; for a joint
venture to explore, evaluate and produce the nevada precious
metal property near lovelock, nev.
    wharf said it wil receive an exclusive option to update an
existing feasibility study and evaluate a production decision.
    subject to completion of necessary documentation, wharf
said it expects work on the nevada property to begin
immediately.
 reuter
</text>]