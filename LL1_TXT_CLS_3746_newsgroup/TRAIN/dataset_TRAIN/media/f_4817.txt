[<text>
<title>home group &lt;hme&gt; approved for nyse listing</title>
<dateline>    new york, march 13 - </dateline>home group inc said its common stock
will be listed on the &lt;new york stock exchange&gt; on march 31.
    the stock now trades on the amex.
 reuter
</text>]