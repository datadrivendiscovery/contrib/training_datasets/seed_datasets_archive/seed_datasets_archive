[<text>
<title>vernitron &lt;vrn&gt; tentatively sets merger vote</title>
<dateline>    lake success, n.y., april 2 - </dateline>vernitron corp said it
anticipates that a special shareholders meeting will be held in
june to vote on a proposed merger with sb holding corp.
    vernitron said the record date for the meeting has been
tentatively set for may 15.
    the company said it filed preliminary proxy materials with
the securities and exchange commission for the special
shareholders meeting.
    following its tender offer in november 1986, sb holding
owns 54.7 pct of vernitron.
 reuter
</text>]