[<text>
<title>hercules &lt;hpc&gt; gets 131.5 mln dlr contract</title>
<dateline>    washington, april 1 - </dateline>hercules inc has been awarded a 131.5
mln dlr contract for stage three follow-on production work on
the mx missile program, the air force said.
 reuter
</text>]