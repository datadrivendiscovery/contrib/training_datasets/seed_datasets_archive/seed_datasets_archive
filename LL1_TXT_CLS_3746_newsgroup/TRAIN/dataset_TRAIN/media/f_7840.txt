[<text>
<title>san juan basin royalty &lt;sjt&gt; hikes distribution</title>
<dateline>    fort worth, tex., march 20 -
    </dateline>cash distribution 4.2621 cts vs 3.2384 cts prior
    pay april 14
    record march 31
    note: company's full name is san juan basin royalty trust.
 reuter
</text>]