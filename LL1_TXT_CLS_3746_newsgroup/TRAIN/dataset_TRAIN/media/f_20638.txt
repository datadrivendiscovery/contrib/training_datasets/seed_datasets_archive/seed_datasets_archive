[<text>
<title>alaska air &lt;alk&gt; to buy back stock</title>
<dateline>    seattle, oct 20 - </dateline>alaska air group inc said its board
authorized management to repurchase up to 320,000 common shares
in the open market from time to time.
    the company said its current stock price does not reflect
its full value.
 reuter
</text>]