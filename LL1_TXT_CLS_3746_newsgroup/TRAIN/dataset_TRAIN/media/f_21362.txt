[<text>
<title>first federal savings &lt;fcht.o&gt; 1st qtr net</title>
<dateline>    chattanooga, tenn., oct 19 -
    </dateline>shr 59 cts
    qtly div eight cts vs eight cts prior
    net 1,675,000 vs 1,302,000
    assets 613.3 mln vs 603.5 mln
    deposits 523.7 mln vs 517.8 mln
    loans 469.2 mln vs 449.5 mln
    note: 1986 per share figures not available because bank
converted to stock ownership dec 18, 1986. dividend payable dec
11 to shareholders of record nov 13. full name of company is
first federal savings and loan association of chattanooga.
 reuter
</text>]