[<text>
<title>pall corp &lt;pll&gt; sets quarterly</title>
<dateline>    glen cove, n.y., oct 19 -
    </dateline>qtly div 8-1/2 cts vs 8-1/2 cts prior
    pay nov 13
    record oct 30
 reuter
</text>]