[<text>
<title>corona &lt;icr.to&gt; favors royex &lt;rgm.to&gt; offer</title>
<dateline>    toronto, april 13 - </dateline>international corona resources ltd said
its board of directors believes that terms of royex gold mining
corp's previously announced offer are fair and reasonable, but
it decided it will make no recommendation on the offer to its
shareholders.
    royex on march 31 offered to buy four mln corona shares.
for each corona share it offered four dlrs cash, one series b
share of royex, one series c share of royex and one share
purchase warrant. it also bid for all corona warrants expiring
aug 31, 1987.

 reuter
</text>]