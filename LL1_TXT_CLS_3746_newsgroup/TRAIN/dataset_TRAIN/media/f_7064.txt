[<text>
<title>caribou energy reverse split approved</title>
<dateline>    dallas, march 19 - </dateline>&lt;caribou energy inc&gt; said shareholders
have approved a one-for-100 reverse split that will take effect
by tomorrow and the company has changed its name to &lt;texas
petroleum corp&gt;.
 reuter
</text>]