[<text>
<title>sportscene acquires christophe van houtte chain</title>
<dateline>    montreal, april 7 - </dateline>(sportscene restaurants inc) said it
acquired (cafe christophe van houtte inc), a cafe chain with
ten franchises and one corporate restaurant, for an unspecified
amount of cash and sportscene shares.
 reuter
</text>]