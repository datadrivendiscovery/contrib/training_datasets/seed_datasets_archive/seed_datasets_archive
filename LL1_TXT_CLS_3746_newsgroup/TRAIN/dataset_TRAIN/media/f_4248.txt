[<text>
<title>perle said preparing to announce resignation</title>
<dateline>    washington, march 12 - </dateline>assistant defense secretary richard
perle is preparing to announce his resignation from the
pentagon, possibly as early as today, pentagon officials said.
    the officials, who asked not to be identified, told reuters
that perle was expected to quit in order to become a private
defense consultant.
    perle, a staunch critic of u.s.-soviet arms control
agreements and an influential spokesman for anti-soviet
hardliners in and out of the reagan administration, has served
as assistant defense secretary for international security
policy since 1981.
 reuter
</text>]