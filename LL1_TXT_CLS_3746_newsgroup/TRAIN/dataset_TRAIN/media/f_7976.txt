[<text>
<title>pico &lt;ppi&gt;, anixter get tele-comm &lt;tcom&gt; order</title>
<dateline>    liverpool, n.y., march 20 - </dateline>pico products inc and &lt;anixter
communications&gt; jointly announced that they received a purchase
order from tele-communications inc, for pay tv security
products to be included in tele-comm's on-site control systems.
    terms were not disclosed.
 reuter
</text>]