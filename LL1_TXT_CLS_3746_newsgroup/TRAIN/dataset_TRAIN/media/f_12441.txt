[<text>
<title>guinness to sell retail interests</title>
<dateline>    london, april 2 - </dateline>guinness plc &lt;guin.l&gt; said that as part
of a new strategy for the company it will be selling its retail
interests to concentrate resources on developing its
international beverage businesses.
    among the firms to be sold are martin's and gordon drummond
pharmacies, the 7-eleven convenience stores, speciality u.s.
food importer richter brothers and the health products company
nature's best/dsl.
    guinness said in a statement that the company's strength
was in its well known beer and spirits brands.
    several had good brand development potential, including
gleneagles, champneys, cranks, hediard and and guinness
publications.
    guinness shares were trading at 323p after the announcement
after closing yesterday at 317p.
 reuter
</text>]