[<text>
<title>vms mortgage investors lp ii &lt;vmlpz.o&gt; in payout</title>
<dateline>    chicago, june 19 -
    </dateline>qtly div 21 cts vs 21 cts prior
    pay aug 14
    record july one
 reuter
</text>]