[<text>
<title>cheyenne resources &lt;chyn&gt; completes restructure</title>
<dateline>    cheyenne, wyo., march 4 - </dateline>cheyenne resources inc said it
has restructured its 499,750 debt to the first interstate bank
of denver to 250,000.
    it said it gave the bank 500,000 shares of restricted
cheyenne resources common stock. it said it would pay the
balance on a monthly basis over 28 months.
    cheyenne also reported it had settled out of court a
112,000 dlr judgment against it for 60,000 dlrs. it said the
judgment involved a land sale which cheyenne refused payment on
after it learned it was not the land advertised in the sale.
 reuter
</text>]