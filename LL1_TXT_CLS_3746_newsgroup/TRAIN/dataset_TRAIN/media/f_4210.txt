[<text>
<title>dorchester hugoton ltd &lt;dhulz&gt; raises payout</title>
<dateline>    dallas, march 12 -
    </dateline>qtly div 10 cts vs eight cts prior
    pay april 16
    record march 31
 reuter
</text>]