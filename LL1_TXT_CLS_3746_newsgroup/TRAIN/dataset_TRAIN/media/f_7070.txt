[<text>
<title>u.s. first time jobless claims fell in week</title>
<dateline>    washington, march 19 - </dateline>new applications for unemployment
insurance benefits fell to a seasonally adjusted 340,000 in the
week ended march 7 from 373,000 in the prior week, the labor
department said.
    the number of people actually receiving benefits under
regular state programs totaled 2,507,000 in the week ended feb
28, the latest period for which that figure was available.
    that was up from 2,477,000 the previous week.
     
 reuter
</text>]