[<text>
<title>augat inc &lt;aug&gt; sets quarterly dividend</title>
<dateline>    mansfield, mass., march 16 -
    </dateline>qtly div 10 cts vs 10 cts
    pay april 30
    record april 10
 reuter
</text>]