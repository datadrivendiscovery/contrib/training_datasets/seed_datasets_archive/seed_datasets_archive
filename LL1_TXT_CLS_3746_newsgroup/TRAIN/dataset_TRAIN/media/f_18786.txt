[<text>
<title>storage &lt;stk&gt; says court clears reorganization</title>
<dateline>    louisville, colo., june 18 - </dateline>storage technology corp said
the u.s. bankruptcy court has approved its plan of
reorganization and its emergence from chapter 11 bankruptcy.
    under the terms of the plan, the company said all approved
creditors claims will be paid in full in cash, equity and
notes. it said it will settle about 800 mln dlrs in liabilities
through the payment of about 132.5 mln dlrs in cash, 285 mln
dlrs of 10-year 13.5 pct notes and 192 mln additional common
shares.
    the company said existing common shareholders will hold
about 15 pct of the company's shares after the distribution.
    the company said the distribution of the cash, equity and
notes to creditors will start in july.
 reuter
</text>]