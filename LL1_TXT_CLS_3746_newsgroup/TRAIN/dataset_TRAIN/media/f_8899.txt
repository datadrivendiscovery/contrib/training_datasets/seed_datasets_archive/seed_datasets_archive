[<text>
<title>corrected - manhattan national corp &lt;mlc&gt; 4th</title>
<dateline>    new york, march 24 -
    </dateline>oper shr loss 20 cts vs loss 81 cts
    oper net loss 1,042,000 vs loss 4,077,000
    revs 38.5 mln vs 50.3 mln
    12 mths
    oper shr loss six cts vs loss 43 cts
    oper net loss 336,000 vs loss 2,176,000
    revs 137.8 mln vs 209.1 mln
    note: in item moved march 23, company corrects its error to
show loss for current 12 mths and qtr, not profit.
 reuter
</text>]