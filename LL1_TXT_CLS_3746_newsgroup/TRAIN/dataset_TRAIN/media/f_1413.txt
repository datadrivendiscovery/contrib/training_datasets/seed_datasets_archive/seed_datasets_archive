[<text>
<title>reagan nominates fbi director to head cia</title>
<dateline>    washington, march 3 - </dateline>president reagan nominated federal
bureau of investigation director william webster to be director
of the central intelligence agency (cia), in succession to
ailing william casey.
    the announcement came one day after reagan withdrew the
nomination of deputy cia dirctor robert gates, who faced
opposition in the senate because of the cia's role in the
iran-contra scandal.
    "bill webster will bring a remarkable depth and breadth of
experience as well as a remarkable record of achievement to
this position," said reagan in a statement read by white house
spokesman marlin fitzwater.
 reuter
</text>]