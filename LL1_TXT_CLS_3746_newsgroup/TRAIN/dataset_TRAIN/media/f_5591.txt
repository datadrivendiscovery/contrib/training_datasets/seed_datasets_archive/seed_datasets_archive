[<text>
<title>&lt;kent toys inc&gt; in pact with lorimar &lt;lt&gt;</title>
<dateline>    kent, ohio, march 16 - </dateline>kent toys inc said it signed an
agreement with lorimar-telepictures corp to produce a
television game show called lottery.
    under terms of the agreement, kent said it will receive a
share of the above the line production, a percentage of net
profits and a percentage of first-run syndication fees.
 reuter
</text>]