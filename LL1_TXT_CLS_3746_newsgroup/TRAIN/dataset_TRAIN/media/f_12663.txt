[<text>
<title>mcdowell enterprises &lt;me&gt; makes acquisition</title>
<dateline>    nashville, tenn., april 2 - </dateline>mcdowell enterprises inc said
it has completed the acquisition of 80 pct of privately-held
interpharm inc, a maker of generic pharmaceuticals, for 488,000
common shares, plus another 521,000 to be issued on approval by
mcdowell shareholders.
    it said subject to future sales and profit levels, mcdowell
could over a four-year period acquire the remaining 20 pct of
interpharm for another 1,543,000 shares.
 reuter
</text>]