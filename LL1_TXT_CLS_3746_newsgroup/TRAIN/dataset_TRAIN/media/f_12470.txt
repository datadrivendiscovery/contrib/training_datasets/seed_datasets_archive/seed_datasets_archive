[<text>
<title>bank of japan intervenes just after tokyo opening</title>
<dateline>    tokyo, april 2 - </dateline>the bank of japan intervened just after
the tokyo market opened, buying dollars at around 147.65 yen,
dealers said.
    they were unsure of the amount of the central bank's
purchasing, but it seemed to prevent the dollar from weakening
against the yen amid bearish sentiment for the u.s. currency,
they said.
    the dollar opened at 147.65 yen against 147.20/30 in new
york and 146.90 at the close here yesterday.
 reuter
</text>]