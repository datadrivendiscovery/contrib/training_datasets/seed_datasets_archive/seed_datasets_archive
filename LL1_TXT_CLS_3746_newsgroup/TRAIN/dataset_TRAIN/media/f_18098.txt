[<text>
<title>synercom technology &lt;synr.o&gt; executive leaves</title>
<dateline>    houston, june 2 - </dateline>synercom technology inc said vice
president charles w. ryle will be leaving the company for
personal reasons.
 reuter
</text>]