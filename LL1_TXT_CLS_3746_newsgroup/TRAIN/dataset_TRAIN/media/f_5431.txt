[<text>
<title>business computer &lt;bcsi&gt; had 4th quarter profit</title>
<dateline>    miami, march 16 - </dateline>business computer solutions inc said it
expects to report a profit for the fourth quarter ended
february 28 -- its first quarterly profit ever -- of about
175,000 dlrs on revenues of about 750,000 dlrs.
    a year before, it lost 217,852 dlrs on sales of 469,274
dlrs.  the company attributed the improved results to increased
purchases of its zfour language and development environment for
computer software.
    business computer said it expects to report a full-year
loss of about 500,000 dlrs on sales of about 2,100,000 dlrs.
last year it lost 1,079,000 dlrs on revenues of 720,000 dlrs.
 reuter
</text>]