[<text>
<title>equipment co of america &lt;ecoa&gt; 4th qtr net</title>
<dateline>    hialeah, fla., march 30 -
    </dateline>oper shr seven cts vs four cts
    oper net 159,000 vs 94,000
    sales 4,528,000 vs 3,683,000
    avg shrs 2,376,000 vs 2,189,000
    year
    oper shr 19 cts vs 15 cts
    oper net 435,000 vs 339,000
    sales 15.7 mln vs 14.4 mln
    note: net excludes tax credits of 17,000 dlrs vs 39,000
dlrs in quarter and 268,000 dlrs vs 294,000 dlrs in year.
 reuter
</text>]