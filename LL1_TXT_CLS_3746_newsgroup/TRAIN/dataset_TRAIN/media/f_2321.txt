[<text>
<title>ual inc &lt;ual&gt;'s united airlines adds flights</title>
<dateline>    los angeles, march 5 - </dateline>ual inc's united airlines said it is
adding flights into los angeles international airport between
now and june.
    united said it will be adding flights between los angeles
and  phoenix, ariz., oakland, calif., sacramento, calif.,
chicago and  salt lake city.
 reuter
</text>]