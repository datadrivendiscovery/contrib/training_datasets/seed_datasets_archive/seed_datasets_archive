[<text>
<title>federal realty investment trust &lt;frt&gt; in payout</title>
<dateline>    bethesda, md., march 12 -
    </dateline>qtly div 27 cts vs 27 cts prior
    pay april 15
    record march 25
 reuter
</text>]