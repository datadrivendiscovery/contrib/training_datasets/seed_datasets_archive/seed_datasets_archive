[<text>
<title>merrill lynch &lt;mer&gt; partnership fully subscribed</title>
<dateline>    new york, march 5 - </dateline>merrill lynch, pierce, fenner and smith
inc said it has fully subscribed the 120 mln dlrs in
partnership interests it offered in the ml venture partners ii,
lp.
    the firm said ml venture partners ii will make equity
investments in new and developing privately held companies in
high technology industries, or young privately held companies
offering innovative services or advanced manufacturing
processes.
    merrill lynch said the partnership will make another cash
distribution of 364 dlrs per 5000 dlrs invested this month.
 reuter
</text>]