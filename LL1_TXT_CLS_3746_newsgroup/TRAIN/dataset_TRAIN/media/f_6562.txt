[<text>
<title>camco signs letter of intent for reed tool</title>
<dateline>    london, march 18 - </dateline>pearson plc &lt;pson.l&gt; said &lt;camco inc&gt;,
its 65.4 pct owned u.s. oil and oil services subsidiary, signed
a letter of intent covering camco's purchase from baker
international corp &lt;bko.n&gt; of substantially all the business of
&lt;reed tool co&gt;.
    reed, a leading manufacturer of drilling bits, had sales
for 1986 of around 76 mln dlrs.
    the transaction is subject to negotiation of a definitive
agreement approved by the baker and camco boards and by the
u.s. department of justice, with which talks are already taking
place concerning the combination of baker and hughes tool.
    baker international has proposed a merger with hughes tool
which could create a 1.2 billion dlr oilfield services company.
    pearson shares were down 4p to 567 after the announcement.
 reuter
</text>]