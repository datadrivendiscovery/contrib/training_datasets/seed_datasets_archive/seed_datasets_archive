[<text>
<title>gerber &lt;geb&gt; sets deadline for unit's buyout</title>
<dateline>    fremont, mich., april 3 - </dateline>gerber products co said it has
given management of its cwt inc trucking subsidiary 60 days to
pursue a leveraged buyout of the subsidiary.
    it said cwt inc, which has operations in the midwest and
southeast, has annual revenues of approximately 135 mln dlrs.
 reuter
</text>]