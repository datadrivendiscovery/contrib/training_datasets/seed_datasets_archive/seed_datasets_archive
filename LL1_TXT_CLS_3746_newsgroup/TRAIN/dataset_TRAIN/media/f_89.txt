[<text>
<title>systematics inc &lt;syst&gt; regular payout</title>
<dateline>    little rock, ark., feb 26 -
    </dateline>qtly div three cts vs three cts prior
    pay march 13
    record february 27
 reuter
</text>]