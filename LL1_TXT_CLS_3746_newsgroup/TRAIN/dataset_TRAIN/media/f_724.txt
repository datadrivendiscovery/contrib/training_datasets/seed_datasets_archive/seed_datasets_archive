[<text>
<title>wavehill international to make acquisition</title>
<dateline>    new york, march 2 - </dateline>&lt;wavehill international ventures inc&gt;
said it has agreed to acquire personal computer rental corp of
coral gables, fla., in a transaction in which shareholders of
personal computer will receive shares respresenting about a 25
pct interest in the combined company.
    the company said it will have about two mln shares
outstanding on a fully-diluted basis after the transaction. it
said after the acquisition it will infuse perconal computer
with cash for expansion.  it said personal computer now has 26
franchised locations and plans to add over 30 in 1987, seeking
eventually to expand into 420 markets in the u.s. and abroad.
 reuter
</text>]