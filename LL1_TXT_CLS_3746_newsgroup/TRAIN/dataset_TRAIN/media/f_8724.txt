[<text>
<title>campbell red lake &lt;crk&gt; sets quarterly dividend</title>
<dateline>    toronto, march 24 -
    </dateline>qtly div 10 cts vs 10 cts prior
    pay may 25
    record april 20
    note: canadian funds
 reuter
</text>]