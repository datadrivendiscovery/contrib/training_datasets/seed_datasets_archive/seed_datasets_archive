[<text>
<title>northwest teleproductions &lt;nwtl.o&gt; 4th qtr net</title>
<dateline>    minneapolis, minn., june 2 - 
    </dateline>shr 15 cts vs 16 cts
    net 239,034 vs 264,485
    sales 2,932,782 vs 2,664,853
    year
    shr 57 cts vs 45 cts
    net 929,524 vs 741,121
    sales 10.9 mln vs 9,708,792
   
 reuter
</text>]