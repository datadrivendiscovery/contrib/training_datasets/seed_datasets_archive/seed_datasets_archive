[<text>
<title>cummins engine co inc &lt;cum&gt; sets payout</title>
<dateline>    columbus, ind., april 7 -
    </dateline>qtly div 55 cts vs 55 cts prior
    pay june 15
    record june 1
 reuter
</text>]