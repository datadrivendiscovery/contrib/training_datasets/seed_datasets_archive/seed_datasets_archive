[<text>
<title>u.s. meat group to file trade complaints</title>
<dateline>    washington, march 13 - </dateline>the american meat institute, ame,
said it intended to ask the u.s. government to retaliate
against a european community meat inspection requirement.
    ame president c. manly molpus also said the industry would
file a petition challenging korea's ban of u.s. meat products.
    molpus told a senate agriculture subcommittee that ame and
other livestock and farm groups intended to file a petition
under section 301 of the general agreement on tariffs and trade
against an ec directive that, effective april 30, will require
u.s. meat processing plants to comply fully with ec standards.
  
 reuter
</text>]