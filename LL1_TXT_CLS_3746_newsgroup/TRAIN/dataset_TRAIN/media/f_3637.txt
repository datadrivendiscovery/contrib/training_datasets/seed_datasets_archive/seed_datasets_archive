[<text>
<title>transtech industries inc &lt;trti&gt; year net</title>
<dateline>    scotch plains, n.j., march 11 -
    </dateline>oper shr 91 cts vs seven cts
    oper net 4,356,774 vs 289,764
    revs 69.2 mln vs 50.2 mln
    avg shrs 4,736,692 vs 4,151,672
    note: 1985 net excludes 3,027,714 dlr loss from
discontinued operations.
 reuter
</text>]