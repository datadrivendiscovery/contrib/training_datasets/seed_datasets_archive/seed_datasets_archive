[<text>
<title>kasler corp &lt;kasl&gt; 1st qtr jan 31 net</title>
<dateline>    los angeles, march 3 -
    </dateline>shr profit three cts vs loss seven cts
    net profit 161,000 vs loss 367,000
    revs 24.3 mln vs 26.5 mln
 reuter
</text>]