[<text>
<title>protective life &lt;prot&gt; in acquisition</title>
<dateline>    birmingham, ala., april 9 - </dateline>protective life corp said it
has signed a letter of intent to assume &lt;liberty life insurance
co's&gt; group insurance on july 1, subject to regulatory
approvals.
    terms were not disclosed.

 reuter
</text>]