[<text>
<title>india buys palm olein at tender - traders</title>
<dateline>    london, june 19 - </dateline>the indian state trading corp (stc) are
reported to have taken three parcels, around 5,000 tonnes each,
of palm olein at yesterday's weekly vegetable oil tender,
traders said.
    all are for august shipment at 364 dlrs a tonne cif. india
passed on all other materials, they added.
 reuter
</text>]