[<text>
<title>healthsouth rehabilitation corp &lt;hsrc&gt; 4th qtr</title>
<dateline>    birmingham, ala., march 12 -
    </dateline>shr profit eight cts vs loss 10 cts
    net profit 622,000 vs loss 564,000
    revs 7,508,000 vs 1,913,000
    year
    shr profit 15 cts vs loss 28 cts
    net profit 933,000 vs loss 1,548,000
    revs 19.8 mln vs 4,799,000
 reuter
</text>]