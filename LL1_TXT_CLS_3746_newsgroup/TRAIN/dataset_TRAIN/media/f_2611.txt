[<text>
<title>okc limited partnership &lt;okc&gt; sets lower payout</title>
<dateline>    dallas, march 6 - </dateline>okc limited partnership said it will make
a five ct per share distribution to unitholders, down from 15
cts in december and payable march 30 to holders of record march
18.
    the partnership said the payout is the largest quarterly
cash distribution allowable under terms of its letter of credit.
 reuter
</text>]