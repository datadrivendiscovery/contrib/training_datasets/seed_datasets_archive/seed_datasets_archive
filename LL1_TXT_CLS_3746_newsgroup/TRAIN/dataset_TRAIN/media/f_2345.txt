[<text>
<title>nyse sells 17 nyfe seats</title>
<dateline>    new york, march 5 - </dateline>the new york stock exchange said it
sold 17 seats on the new york futures exchange, three for 200
dlrs, unchanged from the previous sale on february 25, and 14
for 100 dlrs, down 100 dlrs.
    the exchange said the current bid is 100 dlrs and the
current offer is 300 dlrs.
 reuter
</text>]