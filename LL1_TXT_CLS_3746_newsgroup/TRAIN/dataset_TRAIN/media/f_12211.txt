[<text>
<title>csx corp &lt;csx&gt; 1st qtr net</title>
<dateline>    richmond, va, april 1 -
    </dateline>shr 47 cts vs 56 cts
    net 73.0 mln vs 85.0 mln
    revs 1.89 billion vs 1.69 billion
    note: figures reflect the merger of sea-land corp completed
feb 11, 1987.
 reuter
</text>]