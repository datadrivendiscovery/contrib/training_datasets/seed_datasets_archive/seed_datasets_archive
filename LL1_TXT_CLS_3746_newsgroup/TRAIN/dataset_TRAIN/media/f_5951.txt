[<text>
<title>gruen marketing &lt;gmc&gt; sees year net off</title>
<dateline>    secaucus, n.j., march 17 - </dateline>gruen marketing corp said it
expects to report earnings for the year ended january 31 of 60
to 65 cts per share on about 7,309,000 average shares, down
from 78 cts on 6,545,000 shares a year before.
    it said sales fell about 10 pct from the year-earlier 104.9
mln dlrs.
 reuter
</text>]