[<text>
<title>enserch issues 100 mln dlr convertible bond</title>
<dateline>    london, march 25 - </dateline>enserch inc is issuing 100 mln dlrs of
convertible debt due october 4, 2002 carrying an indicated
coupon of six to 6-3/8 pct and priced at par, salomon brothers
international said as lead manager.
    enserch's stock closed at 22-1/2 last night on the new york
stock exchange. the conversion premium is indicated to be 20 to
23 pct. the securities are non-callable for three years.
    there is a 1-1/2 pct selling concession and two pct
combined management and underwriting fee.
 reuter
</text>]