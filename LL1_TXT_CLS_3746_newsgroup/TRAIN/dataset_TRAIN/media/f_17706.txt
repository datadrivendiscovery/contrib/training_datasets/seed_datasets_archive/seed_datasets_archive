[<text>
<title>u.s. video &lt;vvco.o&gt;, first national in merger</title>
<dateline>    new york, june 1 - </dateline>u.s. video vending corp said it
completed acquiring first national telecommunications inc from
first national entertainment corp for about 10 mln, or a
controlling interest of u.s. video vending shares.
    pursuant to the transaction, harvey seslowsky and william
hodes resigned from u.s. video's board and were replaced by
four members of first national.
 reuter
</text>]