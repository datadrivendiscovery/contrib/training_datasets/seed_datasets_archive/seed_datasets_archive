[<text>
<title>hanson &lt;han&gt; unit to sell kaiser terminal/plant</title>
<dateline>    new york, march 11 - </dateline>hanson industries, the u.s. arm of
hanson trust plc &lt;han&gt;, said it has proposed to sell, in
separate transactions, kaiser cement's northwest terminals and
montana city plant, to lone star industries inc &lt;lce&gt; and &lt;ash
grove cement west inc&gt;, respectively for a total of 50.2
mln dlrs.
    hanson said the deals are subject to normal conditions of
closing.
    hanson industries completed the purchase of kaiser cement
on march 3, for about 250 mln dlrs.
    hanson said kaiser cement is now an indirect wholly owned
unit of hanson trust and forms part of its building products
group.
    "these sales are a continuation of an asset redeployment
program at kaiser cement and will allow kaiser to concentrate
its efforts in the california marketplace, where it is the
largest cement producer and holds a premiere market position,"
hanson industries chairman gordon white said.
   
 reuter
</text>]