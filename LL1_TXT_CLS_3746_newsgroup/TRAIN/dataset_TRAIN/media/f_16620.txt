[<text>
<title>nl industries &lt;nl&gt; names president</title>
<dateline>    new york, april 13 - </dateline>nl industries inc said it named j.
landis martin as president and chief executive officer,
succeeding harold simmons, who will continue as chairman.
    the company said martin, 41, was also named chairman and
chief executive of its nl chemicals inc unit.
 reuter
</text>]