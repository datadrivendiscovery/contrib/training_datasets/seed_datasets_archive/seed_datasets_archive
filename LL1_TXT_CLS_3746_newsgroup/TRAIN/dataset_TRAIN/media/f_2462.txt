[<text>
<title>unicorp &lt;uac&gt;, lincoln in definitve pact</title>
<dateline>    new york, march 5 - </dateline>unicorp american corp said it signed a
definitive agreement to acquire lincoln savings bank fsb.
    under terms of the agreement announced in january, lincoln
would be acquired by a unit of unicorp which is minority-owned
by lincoln president alton marshall.
    the acquisition will take place through a voluntary 
conversion of lincoln to a federally chartered stock savings
bank from a mutual federal savings bank.
    in connection with the conversion, unicorp will contribute
150 mln dlrs in cash to lincoln.
 reuter
</text>]