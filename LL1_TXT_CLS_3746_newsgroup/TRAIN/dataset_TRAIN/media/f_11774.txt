[<text>
<title>bank of japan intervenes in early tokyo afternoon</title>
<dateline>    tokyo, april 1 - </dateline>the bank of japan intervened in the market
in the early afternoon, buying dollars around 147.30 yen and
continuing to buy them as high as 147.50 yen, dealers said.
    the bank intervened just after the dollar started rising on
buying by securities houses at around 147.05 yen, and hoped to
accelerate the dollar's advance, they said.
    the dollar rose as high as 147.50 yen.
  reuter
</text>]