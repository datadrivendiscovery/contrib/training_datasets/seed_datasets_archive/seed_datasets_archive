[<text>
<title>tracor inc &lt;trr&gt; gets 60.1 mln dlr na</title>
vy contract
<dateline>    washington, april 17 - </dateline>tracor applied sciences, a division
of tracor inc, is being awarded a 60.1 mln dlr contract for the
design and development of radio communication suites for the
aegis cg-60 through cg-70 ships, the defense department said.
    it said work on the contract is expected to be completed in
1992.
 reuter
</text>]