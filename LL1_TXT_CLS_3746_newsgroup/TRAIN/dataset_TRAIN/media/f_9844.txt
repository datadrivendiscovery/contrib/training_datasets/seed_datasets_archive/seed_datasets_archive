[<text>
<title>ethyl corp &lt;ey&gt; units complete acquisiton</title>
<dateline>    richmond, va., march 26 - </dateline>ethyl corp said its subsidiaries
completed the acquisiton of nelson research and development co
&lt;nelr&gt;.
    the merger was approved following completion on jan 27 of a
tender offer valued at approximately 55 mln dlrs, the company
said.
    it added that nelson, based in irvine, calif., will be
operated as a wholly-owned subsidiary of ethyl.
    nelson designs and develops new drugs, ethyl said.
 reuter
</text>]