[<text>
<title>mining technology unit enters sales agreement</title>
<dateline>    roanoke, va., march 27 - </dateline>&lt;mining technology and resources
inc&gt; said its wholly-owned subsidiary, mine support systems
inc, entered into a sales agreement with &lt;r.m. wilson co inc&gt;
of wheeling, w. va.
    r.m. wilson will serve as mine support's exclusive sales
agent for the company's mine roof support system in the u.s.,
the company said.
 reuter
</text>]