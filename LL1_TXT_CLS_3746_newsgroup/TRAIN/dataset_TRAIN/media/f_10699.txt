[<text>
<title>arbed sa &lt;arbb.br&gt; year 1986</title>
<dateline>    luxembourg, march 30 - </dateline>net profit 890 mln luxembourg francs
vs 1.12 billion.
    turnover 57.8 billion francs vs 65.3 billion.
    cash flow 5.72 billion francs vs 6.70 billion.
    steel production 3.74 mln tonnes, down seven pct.
    board will decide on april 24 whether to pay a dividend. no
dividend has been paid since 1984.
 reuter
</text>]