[<text>
<title>dranetz technologies inc &lt;dran&gt; year dec 31 net</title>
<dateline>    edison, n.j., march 12 - 
    </dateline>shr 46 cts vs 77 cts
    qtly div six cts vs six cts prior
    net 2,198,469 vs 3,635,565
    revs 23.1 mln vs 26.0 mln
    note: 1986 net includes one-time charge of 249,000 dlrs or
five cts a share from discontinuation of boat sentry and
lakontek products.
    qtly div payable april 15 to shareholders of record march
24.
 reuter
</text>]