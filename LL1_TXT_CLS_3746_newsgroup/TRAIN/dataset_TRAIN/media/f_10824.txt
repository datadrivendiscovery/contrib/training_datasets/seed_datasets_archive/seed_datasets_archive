[<text>
<title>nasd starts search for new president</title>
<dateline>    washington, march 30 - </dateline>the &lt;national association of
securities dealers inc&gt; said it has named a search committee to
seek a replacement for outgoing president gordon s. macklin,
who recently announced plans to become chairman of &lt;hambrecht
and quist&gt;.
 reuter
</text>]