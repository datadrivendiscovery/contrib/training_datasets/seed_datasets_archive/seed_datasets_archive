[<text>
<title>premier industrial &lt;pre&gt; names executives</title>
<dateline>    cleveland, march 3 - </dateline>premier industrial corp said it named
robert warren to the new post of vice chairman and william
hamilton president, succeeding warren.
    morton mandel continues as chairman and chief executive
officer and warren continues as chief operating officer, the
company said.
    hamilton, formerly senior vice president, will have the
company's three business segments -- maintenance products,
distribution of electronic components and manufacture of
fire-fighting accessories -- reporting to him.
    additionally, philip sims and bruce johnson were named
executive vice presidents, responsible for finance and the
electronics group, respectively. sims continues as treasurer
and chief financial officer, premier said.
 reuter
</text>]