[<text>
<title>mobil &lt;mob&gt; adds natural gas reserves in 1986</title>
<dateline>    new york, march 13 - </dateline>mobil corp increased net proven
reserves of natural gas liquids in 1986 from the previous year
according to data in its 1986 annual report.
    the report states that total net proved reserves at year's
end stood at 2.5 billion barrels, an increase of 94 mln barrels
or four pct above the previous year and detailed data show that
the gains resulted from an increase in net proved reserves of
natural gas liquids.
    mobil said gains were in indonesia where a sixth production
facility began operation in october with a capacity to
manufacture 1.7 mln tonnes of liquified natural gas.
    the company also said that new capacity brought onstream
last year replaced 120 pct of mobil's production, which
declined by about four pct in 1986 from the previous year.
    detailed data on reserves shows that u.s. net proved
reserves of crude oil fell to 837 mln barrels from 853 mln
barrels in 1985, natural gas liquid reserves were also lower in
1986 from the previous year.
    net proved crude oil reserves also fell in canada to 224
mln barrels and in europe to 403 mln barrels from the previous
year's level of 231 mln barrels and 439 mln barrels,
respectively.
 reuter
</text>]