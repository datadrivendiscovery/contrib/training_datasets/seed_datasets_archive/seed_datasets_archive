[<text>
<title>unisys &lt;uis&gt; to redeem 7-1/4 pct convertibles</title>
<dateline>    new york, june 1 - </dateline>unisys corp said it will redeem on july
one all of its outstanaing 200 mln dlrs of 7-1/4 pct
convertible subordinated debentures of 2010.
    it will buy back the convertibles at 106.525 pct of 1,000
dlr par value, plus accrued interest.
    unisys also said it expected that substantially all of the
debentures will be surrendered for conversion into its common
stock. each 1,000 dlr debenture is convertible into 12.88
shares of common before the close of business on june 26.
    the company the conversion of the debt to equity would
strengthen its capital structure.
 reuter
</text>]