[<text>
<title>burlington coat factory warehouse corp &lt;bcf&gt; net</title>
<dateline>    burlington, n.j., march 3 - </dateline>jan 31 end
    shr 1.40 dlrs vs 1.10 dlrs
    net 16.4 mln vs 12.9 mln
    revs 196.2 mln vs 157.5 mln
 reuter
</text>]