[<text>
<title>talking point/u.s. brokerage firms</title>
<author>    by patti domm, reuters</author>
<dateline>    new york, march 24 - </dateline>wall street's biggest brokerage firms,
eager for funds and foreign ties to fuel international
expansion, are expected to find more partnerships among japan's
cash-rich companies, analysts said.
    yesterday, american express co's &lt;axp&gt; board formally
approved a linkup with &lt;nippon life insurance co&gt; for itself
and its &lt;shearson lehman brothers inc&gt; brokerage unit. nippon
life will receive 13 pct of shearson for 538 mln dlrs.
    nippon life's investment in shearson follows last year's
investment by &lt;sumitomo bank ltd&gt; in &lt;goldman, sachs and co&gt;.
sumitomo paid 500 mln dlrs for 12.5 pct of the firm.
    "there certainly is potential for additional investment or
further linkups with other japanese financial institutions. i
think the pattern has established itself here, and it's
reasonable to expect further investment down the road," said
prudential-bache securities analyst larry eckenfelder.
    speculation of such partnerships spread to other brokerage
stocks, resulting in rising stock prices in recent sessions.
    "the brokerage industry needs capital and the evolution of
the value of the yen and the dollar suggests there's going to
be a great deal of investment by japanese firms in the u.s.,"
said another analyst.
    as globalization of the financial markets accelerates,
japanese firms are expected to turn their sights on the
expertise of the u.s. brokerage industry.
    competiton to gain a foothold in the important asian market
and europe has also created a craving for more capital by u.s
firms.
    "morgan (stanley), first boston, salomon (brothers),
merrill (lynch). those would be the firms more likely to
consider hooking up with a japanese firm, and those would be
the ones the japanese would be most interested in," eckenfelder
said.
    all of those firms, he said, have been establishing
themselves in japan.
    while wall street views the investment by nippon favorably,
there is some underlying concern that japanese companies may
learn enough from their u.s. partners to ultimately pose the
same type of competitive threat they have
made in electronics and automobiles.
    "the difficult issue is what investment really will lead to
as markets deregulate. passive investment is an education
gathering period. obviously, there are regulatory barriers
toward exercising management control," said samuel liss, a
brokerage industry analyst with salomon brothers.
    the nippon purchase of a stake in shearson must be approved
by u.s. regulators and the japan finance ministry. there will
also be a public offering for 18 pct of shearson.
    nippon and shearson have already said they would form a
joint venture company in london to work on investment advisory,
asset management, and consulting on financing.
    analysts believe nippon has willingly paid a premium for
its stake, but it will also receive warrants for one mln
american express shares and the 13 mln shares in shearson will
be cumulative preferred, convertible to common after u.s.
regulatory approval is granted.
    american express has also guaranteed it will retain a
minimum of 40 pct of shearson until 1990.
    american express and nippon are also expected to work
closely. for instance, analysts expect it to market the
american express card more actively in japan with the help of
nippon.
    "the key to the deal is not just raising capital for
shearson, but forming a strategic alliance with nippon," said
alan zimmerman of kidder, peabody and co.
    american express stock responded favorably to the
announcements, climbing 2-1/8 to 79 in active trading today.
    some analysts believe nippon is also interested in gaining
a foothold in the u.s. market with the help of american express
and shearson.
    analysts predicted other japanese insurers may be eager to
follow nippon life. u.s. insurers have found numerous partners
themselves in the brokerage industry.
    for instance, equitable life assurance co owns donaldson,
lufkin and jenrette securities corp, and prudential life
insurance co owns prudential-bache securities inc.
    eckenfelder said japanese brokerage firms would not be
likely acquirors of u.s. firms for the time being, although
they have often been rumored suitors in the past. last year,
when shearson unsuccessfully courted e.f. hutton group &lt;efh&gt;,
japanese firms were also rumored to be willing partners.
 reuter
</text>]