[<text>
<title>vestar securities inc &lt;ves&gt; sets payout</title>
<dateline>    philadelphia, march 18 -
    </dateline>qtrly div 30.1 cts vs 34.1 cts prior
    pay april 14
    record march 31
    note: company said prior qtr includes end of year
additional four cts dividend.
 reuter
</text>]