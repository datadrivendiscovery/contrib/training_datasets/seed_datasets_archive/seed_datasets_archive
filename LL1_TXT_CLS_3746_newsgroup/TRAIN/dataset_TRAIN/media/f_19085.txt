[<text>
<title>s/p affirms national westminster debt</title>
<dateline>    new york, june 19 - </dateline>standard and poor's corp said it
affirmed the aaa senior debt and a-1-plus commercial paper of
national westminster bank plc &lt;nwbl.l&gt;.
    the aa long-term and a-1-plus short-term certificates of
deposit of national westminster bank usa were also affirmed.
    s and p cited the bank's decision to increase group
provisions for sovereign debt exposure by 466 mln stg. the
action raised total provisions against 35 countries with
payment difficulties to 29.8 pct from 13 pct, s and p said. s
and p said this puts the bank's debt coverage in line with that
of its international peers.
 reuter
</text>]