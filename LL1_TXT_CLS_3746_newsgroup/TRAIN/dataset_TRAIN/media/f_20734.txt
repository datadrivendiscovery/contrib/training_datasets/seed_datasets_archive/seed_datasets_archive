[<text>
<title>&lt;reser's fine foods inc&gt; to go private</title>
<dateline>    portland, ore., oct 20 - </dateline>reser's fine foods inc said
certain directors and officers, who currently represent about
85 pct of the company's stock, plan to take reser's private
through a cash buyout.
    the company said the group plans to offer 12.50 dlrs per
share for the 15 pct of its stock currently outstanding.
 reuter
</text>]