[<text>
<title>amex starts trading lawrence insurance &lt;lwr&gt;</title>
<dateline>    new york, april 13 - </dateline>the &lt;american stock exchange&gt; said it
has started trading the common stock of lawrence insurance
group inc, which went public today.
 reuter
</text>]