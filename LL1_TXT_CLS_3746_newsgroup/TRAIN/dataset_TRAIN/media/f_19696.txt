[<text>
<title>asea group awarded 110 mln dlr power contract</title>
<dateline>    new york, june 29 - </dateline>asea group ab &lt;aseay&gt; said it has been
awarded a 110 mln dlr contract from the swedish state power
board and the finnish utility &lt;imitran oma oy&gt;.
    the company said the contract is for a 500 megawatt 400 kv
fenno skan high voltage dc transmission to be built between
sweden and finland.
    the company also said three other asea companies; asea
transmission, ludvika sweden and strondberg vaasa in finland,
received a related order for converter equipment.
 reuter
</text>]