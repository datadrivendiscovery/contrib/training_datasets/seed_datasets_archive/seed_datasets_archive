[<text>
<title>monarch avalon inc &lt;mahi&gt; 3rd qtr jan 31 loss</title>
<dateline>    baltimore, march 13 -
    </dateline>shr loss 11 cts vs profit four cts
    net loss 199,000 vs profit 81,000
    rev 1.9 mln vs 2.5 mln
    nine months
    shr loss 14 cts vs profit 15 cts
    net loss 261,000 vs profit 273,000
    rev 6.4 mln vs 7.6 mln
    note: per share information adjusted for three-for-two
stock split on january 31, 1986.
 reuter
</text>]