[<text>
<title>kiena plans two-for-one stock split</title>
<dateline>    toronto, march 11 - </dateline>&lt;kiena gold mines ltd&gt; said it planned
a two-for-one common stock split, pending shareholder approval
on april 7.
    it said approval would require 66-2/3 pct of votes cast.
kiena said 57 pct-owner campbell red lake mines ltd &lt;crk&gt; was
expected to vote in favor of the split.
 reuter
</text>]