[<text>
<title>uk money market deficit revised to one billion stg</title>
<dateline>    london, march 13 - </dateline>the bank of england said it has revised
its estimate of today's shortfall to one billion stg, before
taking account of 646 mln stg morning assistance.
 reuter
</text>]