[<text>
<title>the home depot inc &lt;hd&gt; 4th qtr feb one net</title>
<dateline>    atlanta, march 19 -
    </dateline>shr 27 cts vs 10 cts
    net 7,684,000 vs 2,587,000
    revs 273.9 mln vs 203.7 mln
    year
    shr 90 cts vs 33 cts
    net 23.9 mln vs 8,219,000
    revs 1.01 billion vs 700.7 mln
 reuter
</text>]