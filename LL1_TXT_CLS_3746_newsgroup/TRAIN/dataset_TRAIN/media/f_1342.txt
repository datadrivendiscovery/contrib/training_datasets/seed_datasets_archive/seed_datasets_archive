[<text>
<title>cetec corp &lt;cec&gt; quarterly dividend</title>
<dateline>    kahului, hawaii, march 3 -
    </dateline>qtly div five cts vs five cts
    pay may 22
    record may 8
 reuter
</text>]