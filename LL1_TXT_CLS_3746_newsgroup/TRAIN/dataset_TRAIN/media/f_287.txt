[<text>
<title>japan distributor may import mazda u.s.-made cars</title>
<dateline>    tokyo, march 2 - </dateline>&lt;autorama inc&gt;, a distributor for ford
motor co &lt;f&gt; in japan, is considering importing cars made by
&lt;mazda motor manufacturing (usa) corp&gt;, (mmuc), a wholly owned
u.s. subsidiary of mazda motor corp &lt;mazt.t&gt;, an autorama
spokesman said.
    mazda, owned 24 pct by ford, is due to begin production of
the 2,000-cc-engine cars at the michigan plant in september at
an annual rate of 240,000, of which between 60 and 70 pct will
go to ford and the rest to mazda's own u.s. sales network.
 reuter
</text>]