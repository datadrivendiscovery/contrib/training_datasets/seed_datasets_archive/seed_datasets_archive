[<text>
<title>&lt;tridel enterprises inc&gt; sets initial dividend</title>
<dateline>    toronto, march 18 -
    </dateline>qtly div five cts
    pay april 1
    record march 27
 reuter
</text>]