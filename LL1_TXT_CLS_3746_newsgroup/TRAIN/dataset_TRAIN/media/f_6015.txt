[<text>
<title>bank of ireland launches u.s. paper program</title>
<dateline>    london, march 17 - </dateline>bank of ireland said it launched in the
u.s. market a commercial paper program for up to 200 mln dlrs,
becoming the first irish issuer of paper in that market.
    it said the first tranche of an undisclosed amount was sold
today through goldman sachs and co inc and that the paper last
week had received the top a-1/p-1 rating of standard and poor's
corp and moody's investors service inc, respectively.
    in a statement, the bank of ireland noted that the u.s.
paper market will provide it with a new source of funding.
 reuter
</text>]