[<text>
<title>pictel &lt;pctl&gt; offers units at six dlrs each</title>
<dateline>    peabody, mass., march 27 - </dateline>pictel corp said it began a
public offering of 1,050,000 units priced at six dlrs a unit
through managing underwriters f.n. wolf and co and sherwood
capital inc.
    each unit consists of five shrs of common stock and three
redeemable common stock purchase warrants exercisable at 1.25
dlrs a share for five years.
    the company said it granted f.n. wolf an option to buy up
to 157,500 additional units to cover over-allotments.
    proceeds will be used for product development, marketing,
capital equipment and working capital, the telecommunications
products company said.
 reuter
</text>]