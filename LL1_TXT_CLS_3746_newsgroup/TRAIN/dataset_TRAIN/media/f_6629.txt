[<text>
<title>may department stores co &lt;ma&gt; raises dividend</title>
<dateline>    st. louis, march 18 -
    </dateline>qtly div 28-1/2 cts vs 26 cts previously
    pay june 15
    record june one
 reuter
</text>]