[<text>
<title>audio/video affiliates &lt;ava&gt; 4th qtr jan 31 net</title>
<dateline>    dayton, ohio, april 9 -
    </dateline>shr 17 cts vs 28 cts
    net 2,668,000 vs 3,655,000
    revs 93.9 mln vs 83.8 mln
    avg shrs 15.7 mln vs 13.2 mln
    12 mths
    shr 48 cts vs 58 cts
    net 7,510,000 vs 7,482,000
    revs 228.8 mln vs 181.9 mln
    avg shrs 15.7 mln vs 12.9 mln
    note: full name of company is audio/video affiliates inc.
 reuter
</text>]