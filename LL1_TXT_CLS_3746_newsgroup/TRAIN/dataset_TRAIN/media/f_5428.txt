[<text>
<title>gm &lt;gm&gt;, ppg &lt;ppg&gt; sign auto paint pact</title>
<dateline>    lansing, mich., march 16 - </dateline>general motors corp and ppg
industries inc announced jointly that they had signed an
agreement that will give ppg responsibility for operating the
paint shop in gm's buick reatta craft center here.
    the reatta is a two-seat luxury car that will be introduced
in the spring of 1988, said gm.
    the companies described the agreement as an arrangement
aimed at integrating gm's auto production expertise with ppg's
coatings expertise.
    under the agreement, ppg's and gm's paint operating staff
at the center will be consolidated under the ppg area manager.
   
 reuter
</text>]