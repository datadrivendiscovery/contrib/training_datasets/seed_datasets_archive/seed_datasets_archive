[<text>
<title>french free market cereal export bids detailed</title>
<dateline>    paris, march 5 - </dateline>french operators have requested licences
to export 40,000 tonnes of free market feed wheat, 32,500
tonnes of soft bread wheat, 375,000 tonnes of barley and
465,000 tonnes of maize at today's european community tender,
trade sources here said.
    rebates requested ranged between 134 and 136.50 european
currency units (ecus) a tonne for the feed wheat, 137.39 and
141.50 ecus a tonne for the bread wheat, 137.93 and 142.95 ecus
for the barley and 133.75 and 140.25 ecus for the maize.
 reuter
</text>]