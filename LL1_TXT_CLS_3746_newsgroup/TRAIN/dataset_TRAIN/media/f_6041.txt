[<text>
<title>icn biomedicals inc &lt;bimd&gt; 1st qtr feb 28 net</title>
<dateline>    costa mesa, calif., march 17 -
    </dateline>shr 10 cts vs eight cts
    net 856,000 vs 574,000
    sales 9,593,000 vs 9,232,000
    avg shrs 8,809,000 vs 6,969,000
 reuter
</text>]