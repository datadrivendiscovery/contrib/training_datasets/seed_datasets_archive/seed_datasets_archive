[<text>
<title>howe owners federal &lt;hfsl&gt; holders ok more stock</title>
<dateline>    boston, march 2 - </dateline>howen owners federal savings and loan
association said its stockholders have approved an amendment to
its charter increasing the number of authorized common to 32
mln shares from eight mln and the number of authorized
preferred shares to eight mln from two mln.
 reuter
</text>]