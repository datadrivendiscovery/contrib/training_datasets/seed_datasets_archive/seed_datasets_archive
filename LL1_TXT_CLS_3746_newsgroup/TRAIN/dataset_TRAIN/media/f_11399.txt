[<text>
<title>lyng has no comment on wheat subsidy to soviets</title>
<dateline>    washington, march 31 - </dateline>u.s. agriculture secretary richard
lyng declined to confirm statements made today by a farm state
congressman that the united states will offer subsidized wheat
to the soviet union within the next 10 days to two weeks.
    when asked to clarify comments by rep. pat roberts of
kansas that the administration would soon offer export
enhancement wheat to the soviet union lyng said, "well it won't
be today," and then added, "we have no official comment one way
or the other."
    lyng would not comment on whether a wheat subsidy offer to
the ussr is under more active consideration at the usda, saying
that any remarks by him would be tantamount to an official
announcement and could be construed inappropriately.
 reuter
</text>]