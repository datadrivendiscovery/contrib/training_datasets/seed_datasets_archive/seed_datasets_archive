[<text>
<title>advanced institutional &lt;aims&gt; cuts workforce</title>
<dateline>    syosset, n.y., march 20 - </dateline>advanced institutional management
software inc said it has cut its workforce to 53 from 74 and
closed its atlanta office to cut expenses and improve
profitability.
    the company said it is also in the process of reducing its
office space in four of its six offices nationwide.
    advanced also said it has named executive vice president
steven b. sheppard chief operating officer.
    the company said president and chief executive officer
morris moliver had been chief operating officer as well.
 reuter
</text>]