[<text>
<title>fed expected to add temporary reserves</title>
<dateline>    new york, march 4 - </dateline>the federal reserve is expected to
enter the u.s. government securities market to add temporary
reserves, economists said.
    they expect it to supply the reserves indirectly by
arranging around 1.5 of customer repurchase agreements.
    federal funds, which averaged 6.22 pct yesterday, opened at
six pct and remained there in early trading.
 reuter
</text>]