[<text>
<title>amertek inc &lt;atekf&gt; 1st qtr net</title>
<dateline>    woodstock, ontario, april 8 -
    </dateline>shr profit 20 cts vs loss three cts
    net profit 849,299 vs loss 82,512
    revs 7,929,138 vs 3,849,224
 reuter
</text>]