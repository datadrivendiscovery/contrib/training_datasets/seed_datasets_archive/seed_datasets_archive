[<text>
<title>new bedford institution for savings &lt;nbbs&gt; year</title>
<dateline>    new bedford, mass., april 7 -
    </dateline>net 12.3 mln vs 6,438,000
    note: company went public in march.
 reuter
</text>]