[<text>
<title>amoskeag bank &lt;amkg&gt; to acquire entrepo</title>
<dateline>    manchester, n.h., march 5 - </dateline>amoskeag bank said it signed an
agreement to acquire &lt;entrepo financial resources inc&gt;, a
philadelphia-based company which leases and remarkets high
technology equipment.
    terms of the acquisition were not disclosed.
    it said entrepo has assets of 20 mln dlrs.
 reuter
</text>]