[<text>
<title>old republic int'l corp &lt;oldr&gt; hikes dividend</title>
<dateline>    chicago, ill., march 26 -
    </dateline>qtly div 20 cts vs 19-1/2 cts prior
    pay june 20
    record june 5
 reuter
</text>]