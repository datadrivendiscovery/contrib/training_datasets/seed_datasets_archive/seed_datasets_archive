[<text>
<title>laidlaw &lt;ldmfa&gt; splits stock, hikes payout</title>
<dateline>    toronto, april 1 -
    </dateline>three for two stock split
    pay and record date subject to shareholder confirmation may
four
    qtly dividend 5-1/4 cts vs four cts
    pay may 15
    record may one
    note: dividends declared on pre-split shares.
    laidlaw transportation ltd
 reuter
</text>]