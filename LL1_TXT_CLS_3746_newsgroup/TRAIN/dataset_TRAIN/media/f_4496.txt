[<text>
<title>allegheny beverage &lt;abev&gt; downgraded by moody's</title>
<dateline>    new york, march 12 - </dateline>moody's investors service inc said it
cut to caa from b-2 allegheny beverage corp's 112 mln dlrs of
subordinated debt.
    the rating agency cited allegheny's high leverage and poor
operating performance, as well as reduced cash flow.
    moody's said the company's prospects for improvement are
limited by increasing competition in the food service industry
and decreasing demand for the vending services that allegheny
provides. moody's also said a renegotiated bank agreement
includes accelerated amortization requirements that exacerbate
allegheny's already weakened ability to service its debt.
 reuter
</text>]