[<text>
<title>international corona &lt;icr.to&gt; 2nd qtr net</title>
<dateline>    toronto, june 1 - </dateline>period ended march 31
    oper shr profit four cts vs loss 17 cts
    oper net profit 584,000 vs loss 2,165,000
    revs 7,493,000 vs not given
    six mths
    oper shr profit eight cts vs loss 14 cts
    oper net profit 1,177,000 vs loss 1,778,000
    revs 14.8 mln vs not given.
   
 reuter
</text>]