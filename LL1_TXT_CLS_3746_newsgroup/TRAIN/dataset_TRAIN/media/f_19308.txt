[<text>
<title>mars graphic services inc &lt;wmd&gt; 1st qtr may 31</title>
<dateline>    westville, n.j., june 19 -
    </dateline>shr 12 cts vs 10 cts
    net 189,578 vs 100,254
    sales 3,403,914 vs 3,122,983
    avg shrs 1,617,600 vs 954,400
 reuter
</text>]