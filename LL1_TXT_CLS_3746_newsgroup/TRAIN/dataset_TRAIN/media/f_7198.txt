[<text>
<title>tele-art &lt;tlarf&gt; extends warrants</title>
<dateline>    denver, march 19 - </dateline>tele-art inc said its class a warrants
which were due to expire march 19 have been extended to april
18.
    tele-art designs and manufactures digital watches and
clocks.
 reuter
</text>]