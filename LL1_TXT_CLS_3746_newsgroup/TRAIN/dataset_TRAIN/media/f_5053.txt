[<text>
<title>wheeling and lake erie railway co &lt;wle&gt; div</title>
<dateline>    roanake, va., march 13 - 
    </dateline>qtly div 1.4375 dlrs vs 1.4375 dlrs
    pay may 1  
    record april 3     
    note: dividend paid to all shareholders other than norfolk
southern corp's &lt;nsc&gt; norfolk and western railway co.
 reuter
</text>]