[<text>
<title>american woodmark &lt;amwd&gt; to build plant</title>
<dateline>    winchester, va., april 9 - </dateline>american woodmark corp said it
plans to build a 100,000 square foot kitchen cabinet component
plant in toccoa, ga., with completion expected in the spring of
1988.
 reuter
</text>]