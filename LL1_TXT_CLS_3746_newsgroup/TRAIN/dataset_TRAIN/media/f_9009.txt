[<text>
<title>southmark &lt;sm&gt; unit in public offering of stock</title>
<dateline>    dallas, march 24 - </dateline>southmark corp's national heritage inc
said it has started the initial public offering of 2,000,000
shares of common stock at 9.50 dlrs per share.
    it said all shares are being traded though  nasdaq under
the symbol &lt;nher&gt;.
    the lead underwriter for the offering is drexel burnham
lambert inc, with bear, stearns and co inc., and e.f. hutton
and co inc acting as co-underwriters, the company said.
    proceeds will be used to augment working capital, complete
scheduled renovations at some national heritage leased
facilities and repay certain debts to southmark, it said.
 reuter
</text>]