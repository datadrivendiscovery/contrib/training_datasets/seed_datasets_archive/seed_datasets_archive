[<text>
<title>di industries florida unit gets water contract</title>
<dateline>    houston, june 2 - </dateline>drillers inc of florida, a unit of &lt;di
industries inc&gt;, formerly drillers inc &lt;drl&gt;, said it has
received two separate contracts for water injection projects
valued at 408,500 dlrs and 850,000 dlrs respectively.
    di industries said another unit, drillers inc of texas
completed a 560,000-dlr department of defense project which
involved drilling a vertical firing shaft for the department's
r and d railgun.
    di said the projects represented efforts of the company to
diversify from its traditional oil and gas drilling contracts.
 reuter
</text>]