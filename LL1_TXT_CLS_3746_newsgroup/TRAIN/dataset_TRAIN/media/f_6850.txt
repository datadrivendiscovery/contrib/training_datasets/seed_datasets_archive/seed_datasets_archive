[<text>
<title>ault inc &lt;ault&gt; 3rd qtr march one net</title>
<dateline>    minneapolis, minn., march 18 - 
    </dateline>shr profit eight cts vs loss 16 cts
    net profit 153,000 vs loss 310,000
    sales 3,937,000 vs 2,364,000
    nine mths
    shr profit five cts vs loss 53 cts
    net profit 97,000 vs loss 1,042,000
    sales 10.2 mln vs 7,564,000
 reuter
</text>]