[<text>
<title>grumman &lt;gq&gt; wins 28 mln dlr contract</title>
<dateline>    bethpage, n.y., april 1 - </dateline>grumman corp said it was awarded
a 28 mln dlr contract by mcdonnell douglas corp &lt;md&gt; to develop
flight control surfaces for the united states air force's new
c-17 transport aircraft.
    grumman said it will design, develop and build the 
ailerons, rudders and elevators for the transport.
    the company said the production phase of the contract has a
potential value of about 300 mln dlrs.
 reuter
</text>]