[<text>
<title>u.s. business loans fell 822 mln dlrs</title>
<dateline>    washington, april 3 - </dateline>business loans on the books of major
u.s. banks, excluding acceptances, fell 822 mln dlrs to 277.94
billion dlrs in the week ended march 25, the federal reserve
board said.
    the fed said that business loans including acceptances fell
971 mln dlrs to 280.22 billion dlrs.
 reuter
</text>]