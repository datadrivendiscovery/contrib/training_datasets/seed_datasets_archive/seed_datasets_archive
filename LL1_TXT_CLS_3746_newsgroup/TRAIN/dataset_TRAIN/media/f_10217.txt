[<text>
<title>ceco corp repurchases 20 mln dlrs in notes</title>
<dateline>    oak brook, ill., march 26 - </dateline>&lt;the ceco corp&gt; said it
repurchased the 20 mln dlr 13-1/4 pct senior subordinate notes
due november 30, 1996 issued last december nine as part of the
financing for its leveraged buyout.
    ceco said it financed the purchase through an increase in
its existing credit line.
    ceco said drexel burnham lambert inc acted as ceco's agent
in this transaction.
 reuter
</text>]