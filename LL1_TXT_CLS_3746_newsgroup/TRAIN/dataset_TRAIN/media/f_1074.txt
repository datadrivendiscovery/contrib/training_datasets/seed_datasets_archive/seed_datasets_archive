[<text>
<title>calmar &lt;clmi&gt; seeks to be acquired by &lt;kebo ab&gt;</title>
<dateline>    watchung, n.j., march 3 - </dateline>calmar inc said keboo ab of
sweden, which now owns about 64 pct of calmark, has approved
the acquisition of remaining calmar shares at 25.375 dlrs in
cash at the request of the calmar board.
    calmar said a special meeting of its board will be held
march nine to form a special committee of directors not
affiliated with kebo to evaluate the transaction.
    kebo is in turn 60 pct owned by &lt;investment ab beijar&gt; of
sweden.
 reuter
</text>]