[<text>
<title>s-k-i ltd &lt;skii&gt; 2nd qtr jan 25 net</title>
<dateline>    killington, vt., march 2 -
    </dateline>shr 81 cts vs 57 cts
    net 3,660,273 vs 2,437,914
    revs 28.5 mln vs 23.1 mln
    six mths
    shr 29 cts vs 12 cts
    net 1,325,755 vs 483,559
    revs 31.7 mln vs 26.4 mln
 reuter
</text>]