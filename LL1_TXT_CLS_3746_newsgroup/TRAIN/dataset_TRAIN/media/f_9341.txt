[<text>
<title>escagen corp &lt;esn&gt; initial offering starts</title>
<dateline>    san carlos, calif., march 25 - </dateline>escagen corp said an initial
offering of two mln common shares is underway at nine dlrs each
through underwriters led by &lt;prudential-bache group inc&gt;.
    the company is purchasing the assets and business of
international plant research institute and will apply plant
biotechnology to developing food products and planting
materials.
 reuter
</text>]