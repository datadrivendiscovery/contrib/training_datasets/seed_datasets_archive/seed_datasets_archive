[<text>
<title>southmark &lt;sm&gt; to issue american realty rights</title>
<dateline>    dallas, april 9 - </dateline>southmark corp said shareholders will be
issued, as a special dividend, rights to acquire 22 shares of
american realty trust &lt;arb&gt; for each 100 shares of southmark
owned.
    the record date for southmark shareholders to receive these
rights will be may one with an ex-dividend date of april 27.
    southmark received these rights on april six, as the holder
of about 84 pct of american realty trust's outstanding shares.
   
 reuter
</text>]