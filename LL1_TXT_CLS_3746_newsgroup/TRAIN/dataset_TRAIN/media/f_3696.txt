[<text>
<title>citizens fidelity &lt;cfdy&gt; paper upgraded by s/p</title>
<dateline>    new york, march 11 - </dateline>standard and poor's corp said it
raised to a-1-plus from a-1 the commercial paper of citizens
fidelity corp and its unit citizens fidelity bank and trust.
    s and p cited the completion of the firm's merger with pnc
financial corp &lt;pncf&gt;, saying pnc has shown solid earnings,
excellent liquidity and a conservative capital position.
    the rating agency also upgraded long-term debt supported by
letters of credit of lead bank citizens fidelity bank to
aa-plus from a. the bank's certificates of deposit were upped
to aa-plus and a-1-plus from a and a-1 respectively.
 reuter
</text>]