[<text>
<title>matthews and wright &lt;mw&gt; says bonds tax-exempt</title>
<dateline>    new york, june 29 - </dateline>matthews and wright group inc, citing
the opinion of bond counsel, said certain bond issues that it
underwrote were validly issued and will continue to receive
tax-exempt status.
    the company issues the statement as its stock fell 2-1/8 to
4-5/8 on the american stock exchange. matthews and wright said
that in response to press stories, governmental agencies have
made preliminary inquiries seeking more information.
 reuter
</text>]