[<text>
<title>st. joseph light &lt;saj&gt; sets split, hikes payout</title>
<dateline>    st. joseph, mo., march 18 - </dateline>st. joseph light and power corp
said its board declared a three-for-two stock split and raised
the quarterly dividend on presplit shares to 49 cts per share
from 47 cts.
    the company said the dividend is payable may 18 to holders
of record may 4 and the split is subject to approval by
shareholders at the may 20 annual meeting.
 reuter
</text>]