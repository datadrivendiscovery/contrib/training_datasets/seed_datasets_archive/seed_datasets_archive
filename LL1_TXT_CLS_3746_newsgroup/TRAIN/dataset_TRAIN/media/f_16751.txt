[<text>
<title>taiwan imports 210,000 tonnes south african maize</title>
<dateline>    taipei, april 14 - </dateline>taiwan imported about 210,000 tonnes of
south african maize between january 1 and april 13, the joint
committee of local maize importers said.
    under a three-year agreement signed last year, south africa
will export 600,000 tonnes of maize a year to taiwan.
    a committee spokesman told reuters the rest of this year's
quota will be shipped during the rest of 1987.
 reuter
</text>]