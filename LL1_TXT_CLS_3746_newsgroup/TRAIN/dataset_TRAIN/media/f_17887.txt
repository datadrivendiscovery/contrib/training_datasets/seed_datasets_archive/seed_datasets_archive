[<text>
<title>fujitsu ltd &lt;itsu.t&gt; group year ended march 31</title>
<dateline>    tokyo, june 2 -
    </dateline>group shr - 13.56 yen vs 27.06
    net - 21.61 billion vs 38.93 billion
    current - 37.66 billion vs 46.70 billion
    operating - 57.37 billion vs 79.91 billion
    sales - 1,789 billion vs 1,692 billion
 reuter
</text>]