[<text>
<title>aluminum co of america &lt;aa&gt; regular dividend</title>
<dateline>    pittsburgh, march 6 -
    </dateline>qtly div 30 cts vs 30 cts in prior qtr
    payable may 25
    record may one
 reuter
</text>]