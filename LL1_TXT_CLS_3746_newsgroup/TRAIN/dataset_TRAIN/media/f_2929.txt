[<text>
<title>metex corp &lt;mtx&gt; 4th qtr dec 28</title>
<dateline>    edison, n.j., march 6 -
    </dateline>shr 22 cts vs 49 cts
    net 296,994 vs 657,416
    revs 6.5 mln vs 9.5 mln
    year
    shr 78 cts vs 1.51 dlrs
    net 1.0 mln vs 2.0 mln
    revs 27.6 mln vs 29.4 mln
 reuter
</text>]