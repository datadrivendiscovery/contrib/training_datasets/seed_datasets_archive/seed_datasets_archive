[<text>
<title>louvem plans option to buy back own shares</title>
<dateline>    quebec, april 1 - </dateline>&lt;societe miniere louvem inc&gt; said its
board of directors has offered to buy an option to buy back its
owns shares from provincially-owned soquem, its major
shareholder.
    louvem said the option to buy the 3.1 mln shares would be
valid until december 15 at a purchase price of 3.15 dlrs a
share. louvem said it could exercise the option for itself or 
for a third party.
    the company said soquem is considering the offer.
 reuter
</text>]