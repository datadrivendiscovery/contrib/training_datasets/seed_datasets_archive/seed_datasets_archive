[<text>
<title>miyazawa says exchange rates will stay stable</title>
<dateline>    tokyo, march 13 - </dateline>finance minister kiichi miyazawa told a
press conference he thinks exchange rates will remain stable
due to the currency accord reached in paris last month by six
major industrialised nations but he did not say for how long.
    the dollar has hovered around 153 yen since the six agreed
to cooperate to bring about currency stability.
    asked to comment on remarks by some u.s. officials calling
for a further decline of the dollar, miyazawa said only the
u.s. president and the u.s. treasury secretary can make
official statements about exchange rates.
 reuter
</text>]