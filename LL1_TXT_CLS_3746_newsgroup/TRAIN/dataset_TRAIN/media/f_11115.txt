[<text>
<title>&lt;r.l. crain inc&gt; 4th qtr net</title>
<dateline>    ottawa, march 30 -
    </dateline>shr 80 cts vs 79 cts
    net 4,212,000 vs 4,142,000
    sales 111.1 mln vs 107.1 mln
 reuter
</text>]