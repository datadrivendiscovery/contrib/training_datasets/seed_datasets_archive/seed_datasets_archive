[<text>
<title>ags computer &lt;ags&gt; unit gets ibm license</title>
<dateline>    new york, march 5 - </dateline>systems strategies inc, a unit of ags
computers inc, said international business machines corp &lt;ibm&gt;
has licensed five of its systems network architecture and
binary synchronous software packages.
    systems said it modified the software to run on the ibm
4361 under the ix/370 operating system.
 reuter
</text>]