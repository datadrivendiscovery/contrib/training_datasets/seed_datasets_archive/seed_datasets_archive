[<text>
<title>3com corp &lt;coms&gt; offers shares of common</title>
<dateline>    santa clara, calif., march 13 - </dateline>3com corp said it is
offering 1.2 mln shares of its common stock for sale at 23.50
per share in a public offering.
    it said the sale is being co-managed by goldman, sachs and
co and montgomery securities.
    the company said it granted the underwriters an option to
buy 180,000 shares to cover over-allotments.
    net proceeds from the sale will be added to the company's
working capital to fund future growth, 3com said.
 reuter
</text>]