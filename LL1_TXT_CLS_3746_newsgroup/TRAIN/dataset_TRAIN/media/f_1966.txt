[<text>
<title>cargill u.k. strike talks postponed till monday</title>
<dateline>    london, march 5 - </dateline>talks set for today between management
and unions to try to solve the labour dispute at cargill u.k.
ltd's seaforth oilseed crushing plant have been rescheduled for
monday, a company spokesman said.
    oilseed processing at the mill has been at a standstill
since december 19.
 reuter
</text>]