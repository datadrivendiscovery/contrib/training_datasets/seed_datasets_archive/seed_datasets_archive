[<text>
<title>additional ccc credit guarantees for korea--usda</title>
<dateline>    washington, june 1 - </dateline>the commodity credit corporation (ccc)
has reallocated 50.0 mln dlrs in credit guarantees from the
previously announced undesignated line to provide additional
guarantees for sales of feedgrains, oilseeds, and wheat to
south korea, the u.s. agriculture department said.
    the department said the action increases the feed grains
line by 23 mln dlrs to 63 mln, the oilseed line by seven mln
dlrs to 52 mln, and the wheat guarantee line by 20 mln to 165
mln dlrs.
    the undesignated line is reduced to zero.
    the commodities are for delivery during the current fiscal
year ending this september 30, it said.
 reuter
</text>]