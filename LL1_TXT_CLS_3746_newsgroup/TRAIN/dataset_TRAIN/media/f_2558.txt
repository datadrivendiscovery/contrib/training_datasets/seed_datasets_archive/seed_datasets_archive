[<text>
<title>tpa of america inc &lt;tps&gt; 4th qtr loss</title>
<dateline>    los angeles, march 6 -
    </dateline>shr loss five cts
    net loss 753,000
    revs 8,932,000
    avg shrs 16.0 mln
    year
    shr loss seven cts
    net loss 995,000
    revs 27.9 mln
    avg shrs 14.8 mln
    note: company started operating in august 1985.
    results reflect change in fiscal year from november 30 end.
 reuter
</text>]