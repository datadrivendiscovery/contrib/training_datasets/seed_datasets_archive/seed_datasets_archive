[<text>
<title>uspci &lt;upc&gt; files for offering</title>
<dateline>    oklahoma city, april 3 - </dateline>uspci inc said it filed a
registration statement with the securities and exchange
commission for a public offering of one share of common stock
in the u.s. and 372,500 shares in a concurrent international
offering.
    of the one mln being offered in the u.s., 227,500 will be
issued and sold by the company, 600,000 by the beard co,
100,000 by beard oil co and 72,500 by shareholders.
    after completion of the offering, beard will own about 25
pct of the stock.
    proceeds will be used to fund capital expenditures.

 reuter
</text>]