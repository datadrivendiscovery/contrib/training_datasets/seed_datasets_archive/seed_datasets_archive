[<text>
<title>u.s. senate panel makes conservation exemption</title>
<dateline>    washington, march 4 - </dateline>the u.s. senate agriculture committee
approved a measure that would exempt farmers who planted
alfalfa or other multiyear grasses and legumes between 1981 and
1985 from a federal conservation requirement.
    sen. edward zorinsky, d-neb., said his bill would restore
equity under federal sodbuster rules, which currently deny farm
program benefits to farmers who, between 1981 and 1985, planted
alfalfa and other multiyear grasses and legumes without
interrupting the plantings with a row crop.
    an official from a leading conservation group, who asked
not to be identified, said the panel's move was "an unfortunate
first action" because it could lead to the exemption of
potentially millions of acres from the sod buster regulations,
established under the 1985 farm bill.
 reuter
</text>]