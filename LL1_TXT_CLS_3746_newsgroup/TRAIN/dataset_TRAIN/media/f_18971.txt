[<text>
<title>southern california edison &lt;sce&gt; raises dividend</title>
<dateline>    rosemead, calif., june 18 -
    </dateline>qtly div 59.5 cts vs 57 cts
    pay july 31
    record july 2
 reuter
</text>]