[<text>
<title>mercantile stores &lt;mst&gt; february sales up 6.9 pct</title>
<dateline>    wilmington, del., march 5 - </dateline>mercantile stores co inc said
its february sales totaled 113.2 mln dlrs, up 6.9 pct 105.9 mln
dlrs in the year earlier month.
    the company said its comparable store sales rose 4.3 pct
last month.
 reuter
</text>]