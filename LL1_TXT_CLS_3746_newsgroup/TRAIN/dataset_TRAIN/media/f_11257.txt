[<text>
<title>rexnord &lt;rex&gt; sells unit to neoax &lt;noax&gt;</title>
<dateline>    chicago, march 31 - </dateline>rexnord inc, 96 pct owned by banner
industries inc &lt;bnr&gt; following a recent tender offer, said it
has completed the sale of its fairfield manufacturing co
subsidiary to neoax inc for 70.5 mln dlrs in cash.
    rexnord said it still plans to sell its process machinery
division and mathews conveyor co as part of its planned program
to divest five businesses with 200 mln dlrs in assets.
bellofram corp and railway maintenance equipment co have
already been sold.
 reuter
</text>]