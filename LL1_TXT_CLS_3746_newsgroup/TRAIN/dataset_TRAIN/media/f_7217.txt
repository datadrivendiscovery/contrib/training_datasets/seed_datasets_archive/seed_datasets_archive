[<text>
<title>domtar plans two-for-one stock split</title>
<dateline>    montreal, march 19 - </dateline>&lt;domtar inc&gt; said it plans a
two-for-one stock split to take effect may 14.
    the company said shareholders will be asked to approve the
split at the annual meeting on april 29.
    domtar said its directors believe the split could favorably
affect marketability of the shares and encourage wider
distribution.
    the shares have been trading recently in a range of 45
dlrs. domtar stock was previously split on a two-for-one basis
in june, 1985.
 reuter
</text>]