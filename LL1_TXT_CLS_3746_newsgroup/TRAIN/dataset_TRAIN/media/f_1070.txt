[<text>
<title>charming shoppes inc &lt;chrs&gt; 4th qtr jan 31 net</title>
<dateline>    bensalem, penn., march 3 -
    </dateline>shr 28 cts vs 22 cts
    net 14 mln vs 10.6 mln
    revs 163.8 mln vs 127.3 mln
    year
    shr 81 cts vs 59 cts
    net 40.5 mln vs 28.7 mln
    revs 521.2 mln vs 391.6 mln
 reuter
</text>]