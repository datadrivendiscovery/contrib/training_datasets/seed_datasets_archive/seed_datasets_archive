[<text>
<title>tandem computers &lt;tndm&gt; gets installation</title>
<dateline>    cupertino, calif., march 23 - </dateline>tandem computers inc said
pacific gas and electric co &lt;pcg&gt; installed a tandem computer
system for on-line information management at the diablo canyon
nuclear power plant near san luis obispo, calif.
    no value for installation of the nonstop vlx system was
given.
 reuter
</text>]