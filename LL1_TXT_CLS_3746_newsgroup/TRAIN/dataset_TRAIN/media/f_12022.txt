[<text>
<title>south atlantic financial corp &lt;soaf&gt; 4th qtr</title>
<dateline>    stamford, conn., april 1 -
    </dateline>shr 12 cts vs 33 cts
    net 699,037 vs 1,349,077
    year
    shr 54 cts vs 55 cts
    net 2,748,280 vs 1,833,766
    note: per shr amounts reported after preferred stock
dividend requirements.
 reuter
</text>]