[<text>
<title>u.k. money market given early assistance</title>
<dateline>    london, march 12 - </dateline>the bank of england said it had bought
bills worth 1.059 billion stg from the market for resale on
march 31 at rates of interest between 10-7/16 pct and 10-17/32
pct.
    earlier, the bank said it estimated the liquidity shortage
in the market today at around 1.55 billion stg.
 reuter
</text>]