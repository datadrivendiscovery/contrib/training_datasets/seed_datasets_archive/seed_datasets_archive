[<text>
<title>vermont financial services &lt;vfsc&gt; sets payout</title>
<dateline>    brattleboro, vermont, march 24 - </dateline>vermont financial services
corp said its board approved a regular 20 cts per share cash
dividend payable april 25 to shareholders of record march 26.
 reuter
</text>]