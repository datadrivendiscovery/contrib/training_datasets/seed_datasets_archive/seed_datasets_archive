[<text>
<title>nova &lt;nva.a.to&gt; not planning dome &lt;dmp&gt; bid</title>
<dateline>    toronto, april 13 - </dateline>nova, an alberta corp, chief executive
robert blair expressed hope that dome petroleum ltd &lt;dmp&gt;
remains under canadian ownership, but added that his company
plans no bid of its own for debt-troubled dome.
    "we've no plan to bid," blair told reporters after a speech
to a business group, although he stressed that nova and 57
pct-owned husky oil ltd &lt;hyo&gt; were interested in dome's
extensive western canadian energy holdings.
    "but being interested can sometimes be different from
making a bid," blair said.
    transcanada pipelines ltd &lt;trp&gt; yesterday bid 4.30 billion
dlrs for dome, but dome said it was discontinuing talks with
transcanada and was considering a proposal from another company
and was also talking with another possible buyer, both rumored
to be offshore.
    asked by reporters if dome should remain in canadian hands,
blair replied, "yes. i think that we still need to be building
as much canadian position in this industry as we can and i
think it would be best if dome ends up in the hands of canadian
management."
    he said he did not know who other possible bidders were.
    blair said that any move to put dome's financial house in
order "will remove one of the general problems of attitude that
have hung over western canadian industry."
    he added, however, that the energy industry still faced "a
couple of tough, tough additional years."
    asked about nova's 1987 prospects, blair predicted that
nova's net profit would rise this year to more than 150 mln
dlrs from last year's net profit of 100.2 mln dlrs due to
improved product prices and continued cost-cutting.
 reuter
</text>]