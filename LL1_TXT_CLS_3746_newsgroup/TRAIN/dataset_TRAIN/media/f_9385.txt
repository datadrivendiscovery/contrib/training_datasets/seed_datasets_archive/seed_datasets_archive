[<text>
<title>raytheon co &lt;rtn&gt; sets quarterly</title>
<dateline>    lexington, mass., march 25 -
    </dateline>qtly div 45 cts vs 45 cts prior
    pay april 30
    record april 10
 reuter
</text>]