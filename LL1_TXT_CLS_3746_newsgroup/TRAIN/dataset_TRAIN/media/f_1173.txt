[<text>
<title>knutson mortgage &lt;knmc&gt; sees strong second qtr</title>
<dateline>    bloomington, minn., march 3 - </dateline>knutson mortgage corp said it
expects strong earnings performance for its initial fiscal
second quarter earnings ending march 31 since going public in
september 1986.
    albert holderson, knutson chairman, said he expects
earnings of about 40 cts per share for the quarter as a result
of a strong mortgage business during the quarter.
    knutson earlier declared a quarterly dividend of 10 cts a
share, versus 10 cts a share prior, payable april 13 to
shareholders of record march 13.
 reuter
</text>]