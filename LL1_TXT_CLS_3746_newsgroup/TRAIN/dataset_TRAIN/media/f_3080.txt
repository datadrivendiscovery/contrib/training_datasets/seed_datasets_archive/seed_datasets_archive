[<text>
<title>gulf resources &lt;gre&gt; to have gain on stock sale</title>
<dateline>    boston, march 9 - </dateline>gulf resources and chemical corp said it
has sold 9,534,633 shares of &lt;imperial continental gas
association&gt; for 720 pence a share, or 68.6 mln stg, in a
private transaction for a pretax gain of about 19.6 mln dlrs.
    gulf resources said it still owns 6.7 mln shares of
imperial continental, or a 4.6 pct interest, and continues to
study various alternatives.
 reuter
</text>]