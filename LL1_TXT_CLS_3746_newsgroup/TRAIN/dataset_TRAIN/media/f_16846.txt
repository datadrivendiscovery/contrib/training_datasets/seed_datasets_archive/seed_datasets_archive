[<text>
<title>first federal brooksville&lt;ffbv&gt; adjourns meeting</title>
<dateline>    brroksville, fla., april 17 - </dateline>first federal savings and
loan association of brooksville said it has adjourned its
annual meeting until may 11 due to the unexpected death of
director j.r. underwood, creating the need for a substitute
nominee for the board.
 reuter
</text>]