[<text>
<title>the federated group &lt;fegp&gt; 4-week sales down</title>
<dateline>    city of commerce, calif., march 9 - </dateline>the federated group inc
said its four-week march 1 sales totaled 25.7 mln dlrs, down
eight pct from the same period a year ago.
    the company said sales in the 13 weeks ended march 1 rose
11 pct to 136.6 mln dlrs.
    in the 52-weeks ended march 1 sales totaled 430.2 mln dlrs,
up 18 pct from a year earlier, the company said.
 reuter
</text>]