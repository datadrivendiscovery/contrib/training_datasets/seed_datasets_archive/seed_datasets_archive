[<text>
<title>federal industries launches eurobond issue</title>
<dateline>    winnipeg, manitoba, feb 26 - </dateline>&lt;federal industries ltd&gt; said
it launched a 40 mln canadian dlr eurobond issue for five
years, bearing a coupon of 9-1/4 pct.
    issue price is 100-5/8. lead manager is union bank of
switzerland.
    proceeds will be used to reduce short-term debt.
 reuter
</text>]