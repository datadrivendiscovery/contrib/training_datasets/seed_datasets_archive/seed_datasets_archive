[<text>
<title>miyazawa does not think dollar in freefall</title>
<dateline>    tokyo, april 27 - </dateline>japanese finance minister kiichi miyazawa
told a parliamentary upper house budget committee that he does
not think the dollar is in a freefall.
    he said concerted intervention is only a supplementary
measure to moderate volatility in exchange rates and repeated
that policy coordination among major industrial nations is
necessary. "we cannot expect currency stability only through
coordinated market intervention," he said.
    miyazawa also told the committee the u.s. has not called on
japan to cut its 2.5 pct discount rate.
    miyazawa said the government is not considering investing
in u.s. government bonds to help stabilize exchange rates. this
matter has to be dealt with carefully because it involves the
public's money and exchange rates are moving widely, he added.
    the ministry will consider where to invest its funds when
exchange rates become stable, he said.
    asked if japan is considering a request to the u.s. for it
to raise its discount rate to stabilize exchange rates,
miyazawa said the u.s. has not been able to take action now
because it has to maintain its economic growth.
 reuter
</text>]