[<text>
<title>hogan systems &lt;hogn&gt; in acquisition</title>
<dateline>    dallas, march 9 - </dateline>hogan systems inc said it acquired
&lt;systems 4 inc&gt; of durango, colo., for 1.7 mln dlrs.
    hogan said systems 4 provides integrated applications
software and processing services to about 30 community banks.
    systems 4 has revenues of 1.5 mln dlrs a year, hogan said.
 reuter
</text>]