[<text>
<title>bowater buys builders' merchants hooper and ashby</title>
<dateline>    london, april 21 - </dateline>bowater industries plc &lt;bwtr.l&gt; said it
had agreed to buy southampton-based builders' merchants &lt;hooper
and ashby ltd&gt; for 718,545 bowater shares, floating rate
unsecured loan stock and cash.
    it gave no further financial details besides saying that a
final payment of cash or loan stock would be made when audited
accounts were available.
    hooper is a family-owned business which had a turnover of
around 25 mln stg in 1986 and net assets of about nine mln stg.
    bowater shares were unchanged at 495p on thursday.
 reuter
</text>]