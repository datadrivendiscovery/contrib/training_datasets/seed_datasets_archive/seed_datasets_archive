[<text>
<title>commonwealty realty &lt;crtyz&gt;, bay &lt;bay&gt; end talks</title>
<dateline>    princeton, n.j., march 215 - </dateline>commonwealth realty trust said
preliminary merger talks with bay financial corp have been
terminated due to a failure to agree on terms.
 reuter
</text>]