[<text>
<title>federal paper board co &lt;fbt&gt; sets payout</title>
<dateline>    montvale, n.j., march 17 -
    </dateline>qtly div 17-1/4 cts vs 17-1/4 cts prior
    pay april 15
    record march 31   
 reuter
</text>]