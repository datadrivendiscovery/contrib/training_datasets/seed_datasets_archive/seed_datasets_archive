[<text>
<title>mtech &lt;mtch&gt; to offer convertible debentures</title>
<dateline>    dallas, march 6 - </dateline>mtech corp said it expects to file
shortly for an offering of subordinated debentures convertibe
into common stock.
    it gave no details on the size of the offering.
 reuter
</text>]