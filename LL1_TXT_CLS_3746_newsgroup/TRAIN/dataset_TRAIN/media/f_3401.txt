[<text>
<title>market discounts higher soviet grain imports</title>
<dateline>    chicago, march 9 - </dateline>grain analysts said the increase of
three mln tonnes in 1986/87 soviet grain imports is unlikely to
affect the market tuesday.
    they said the market already has discounted higher soviet
imports, partly on news last month that the soviet union bought
one mln tonnes of u.s. corn, and on rumors that the reagan
administration is pushing for authority to sell the soviets
u.s. wheat under the export enhancement program.
    in its supply-demand report, the usda raised its estimate
for 1986/87 soviet grain imports to 26 mln tonnes from 23 mln.
    "that was business already done, for all practical
purposes," said drexel burnham analyst dale gustafson,
reflecting similar statements made by other analysts.
 reuter
</text>]