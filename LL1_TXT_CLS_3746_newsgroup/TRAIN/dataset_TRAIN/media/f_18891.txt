[<text>
<title>gulf canada &lt;goc&gt; in deal with bondholders</title>
<dateline>    toronto, june 18 - </dateline>a compromise reached between gulf canada
corp and debenture holders at the annual meeting today will
cost the company or its major shareholder "a couple of million"
dollars, chairman marshall cohen said.
    after about a half-hour discussion with debenture holders
during the meeting, cohen and gulf lawyers agreed to compensate
them for about six months of interest that they would have lost
under a corporate reorganization plan taking effect july one.
the holders then withdrew a proposed amendment that sought to
delay the plan's closing to july 16 so they could collect
interest due to be paid july 15.
    "we have not sorted out the mechanics of how we're going to
do this," cohen told reporters later.
    "it may well be that in the end the principal shareholder
may absorb that interest," he said, referring to the reichmann
family's olympia and york developments ltd, which owns about 79
pct of gulf canada.                                     cohen
said he wanted to ensure that gulf canada need not revise
reorganization proposals already on file with revenue canada
and the u.s. securities and exchange commission.
    "if gulf can't pay it (the interest) without upsetting the
applecart, olympia and york will pay," he said.
    the reorganization will see gulf canada corp renamed gulf
canada resources ltd. shareholders will be offered shares of
three separate publicly traded companies, gulf canada
resources, abitibi-price inc &lt;a.to&gt; and gw utilities ltd.
    newly formed gw utilities will hold gulf canada's interests
in consumers' gas co ltd &lt;cgt.to&gt;, hiram walker-gooderham and
worts ltd and interprovincial pipe line ltd &lt;ipl.to&gt;.
    cohen said olympia and york's interest in gulf canada
resources will slip to about 68 or 69 pct as a result of a
previously announced plan to sell 450 mln dlrs of new stock.
                             
    in answer to a reporter's question, cohen said there was a
possibility that the size of the offering could be increased.
    "there seems to be pretty strong interest," he said, but he
added that much depended upon market conditions at the time of
pricing. the issue will be priced later this month, according
to a company official.
  
 reuter
</text>]