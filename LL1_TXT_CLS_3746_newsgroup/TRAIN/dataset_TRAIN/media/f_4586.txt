[<text>
<title>eager technology acquires nuclad</title>
<dateline>    denver, march 12 - </dateline>&lt;eager technology inc&gt; said it signed a
letter of intent to acquire nuclad inc, a private colorado
corporation, and its subsidiaries.
    terms of the acquisition were not disclosed.
 reuter
</text>]