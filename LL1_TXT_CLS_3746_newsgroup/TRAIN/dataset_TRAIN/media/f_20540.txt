[<text>
<title>electromedics 5-for-1 reverse split approved</title>
<dateline>    englewood, colo., oct 20 - </dateline>electromedics inc &lt;elmd.o&gt; said
its shareholders voted to approve a 5-for-1 reverse stock
split.
    each five authorized and outstanding shares of one ct par
value common stock are to be combined into one share of a newly
authorized five ct par value common stock, the company said.
    the company said the new common stock will trade under the
nasdaq symbol elmdv until at least 30 pct of the outstanding
shares of the old common stock have been exchanged for new
stock.
   
    electromedics estimates that the reverse split will reduce
the number of shares outstanding from 45.5 mln to about
9,091,882.
    electromedics is a manufacturer and marketer of high
technology medical equipment used in blood conservation.
 reuter
</text>]