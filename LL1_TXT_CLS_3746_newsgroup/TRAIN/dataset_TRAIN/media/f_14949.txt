[<text>
<title>conrac &lt;cax&gt; in merger talks with several</title>
<dateline>    stamford, conn., april 8 - </dateline>conrac corp sait has started
negotiations with several interested parties on its possible
acquisition.
    it said there can be no assurance that any transaction will
result from the talks.  it gave no further details.
    mark iv industries inc &lt;iv&gt; started tendering for all
conrac shares at 25 dlrs each on march 24 and owned 9.9 pct of
conrac before starting the bid.
    conrac is a producer and marketer of computer-related
information display and communications equipment which also
produces special purpose architectural and industrial products.
    it owns code-a-phone corp, a producer of telephone
answering machines.
    for 1986, the company reported profits of 7.8 mln dlrs, or
1.16 dlrs a share, on sales of 153.9 mln dlrs. it has nearly
6.8 mln shares outstanding.
 reuter
</text>]