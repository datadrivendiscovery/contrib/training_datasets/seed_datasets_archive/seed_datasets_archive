[<text>
<title>japanese banks confirm argentina loan commitment</title>
<dateline>    tokyo, june 19 - </dateline>japanese commercial creditor banks
confirmed an earlier commitment to extend some 360 mln dlrs in
new loans to argentina, banking sources said.
    the commitment will represent a part of the 1.95 billion
dlrs in new loans to argentina agreed to by 350 banks worldwide
last april 24, the sources said.
    the 350 banks were supposed to confirm their argentina loan
commitments by june 17.
    about 91 pct of them have already done so, but it may take
some more time before all have confirmed their intentions, the
sources added.
 reuter
</text>]