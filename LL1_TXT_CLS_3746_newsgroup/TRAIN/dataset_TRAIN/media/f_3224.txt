[<text>
<title>amoskeag co names new chairman</title>
<dateline>    boston, mass., march 9 - </dateline>&lt;amoskeag co&gt; said joseph b. ely
ii, became chairman, in addition to his duties as chief
executive officer, succeeding albert b. hunt, who served in
that position since 1972.
    it also said james m. fitzgibbons, currently executive vice
president and chief operating officer, will beomce president, a
position ely also held.
 reuter
</text>]