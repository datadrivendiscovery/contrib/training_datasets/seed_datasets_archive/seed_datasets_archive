[<text>
<title>hitech &lt;thex&gt; and qume in pact</title>
<dateline>    san jose, calif., march 3 - </dateline>the hitech engineering co and
&lt;qume corp&gt; said that hitech has agreed to design and certify
certain qume desktop laser printers for the industrial and
government tempest market.
    the tempest program is a set of standards for government
information processing equipment designed to minimize
electromagnetic eavesdropping.
    the companies said that the total tempest market is
estimated to be around two billion dlrs.
 reuter
</text>]