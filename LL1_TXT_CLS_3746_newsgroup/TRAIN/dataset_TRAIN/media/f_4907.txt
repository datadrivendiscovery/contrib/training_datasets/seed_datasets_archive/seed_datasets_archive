[<text>
<title>union, phoenix steel unable to reach agreement</title>
<dateline>    claymont, del., march 13 - </dateline>&lt;phoenix steel corp&gt; said it and
the union representing workers at its seamless tubing mill in
phoenixville, penn., were unable to reach agreement to extend
the union contract that expires march 31.
    phoenix said the union rejected its offer to extend a labor
agreement through may 30 and a medical insurance agreement for
active employees through june 30.
    the company said no further bargaining sessions are planned
between it and united steelworkers local 2322.
 reuter
</text>]