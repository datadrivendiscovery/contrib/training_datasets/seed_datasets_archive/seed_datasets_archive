[<text>
<title>gm unit &lt;gmh&gt; negotiating for british contract</title>
<dateline>    el segundo, calif., june 2 - </dateline>general motors corp's hughes
aircraft co subsidiary said it has been chosen by british
satellite broadcasting to negotiate for a contract worth about
300 mln dlrs to deliver two satellites for direct broadcast
television.
    the company said the first of the satellites is scheduled
to start up three channels of direct broadcast satellite
television by late 1989.
    the company said the contract should be signed by june 30. 
each satellite would be equipped with three 110-watt channels.
 reuter
</text>]