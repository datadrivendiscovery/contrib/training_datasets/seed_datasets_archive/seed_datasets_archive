[<text>
<title>lufthansa sets washington/frankfurt service</title>
<dateline>    washington, march 17 - </dateline>&lt;deutsche lufthansa ag&gt; said on
april 1 it will start nonstop service between washington and
frankfurt, initially operating four roundtrips weekly.
    it said frequency of the mcdonnell douglas corp &lt;md&gt; dc-10
flights will rise to five times weekly on april 26.
    it said the lowest priced holiday fares on the route,
requiring 21-day advance payment, will be 449 dlrs roundtrip in
april, 672 dlrs midweek in may and from september 15 to october
30 and 799 dlrs midweek for the summer months.                 
    
 reuter
</text>]