[<text>
<title>kimbark oil and gas co &lt;kimb.o&gt; 1986 year loss</title>
<dateline>    denver, april 17 -
    </dateline>shr loss 57 cts vs loss 2.88 dlrs
    net loss 3,442,000 vs loss 13,750,000
   
 reuter
</text>]