[<text>
<title>&lt;canadian satellite communications&gt; six mths net</title>
<dateline>    montreal, march 27 -
    </dateline>period ended february 28
    oper shr profit six cts vs loss 15 cts
    oper profit 474,000 vs loss 1,175,000
    revs 17,946,000 vs 9,271,000
    note: current shr and net exclude tax gain of 513,000  dlrs
or five cts share
    full name &lt;canadian satellite communications inc&gt;
 reuter
</text>]