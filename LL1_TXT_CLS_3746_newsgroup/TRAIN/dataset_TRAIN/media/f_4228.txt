[<text>
<title>circuit city stores inc &lt;cc&gt; sets quarterly</title>
<dateline>    richmond, va., march 12 -
    </dateline>qtly div 1-1/2 cts vs 1-1/2 cts prior
    pay april 15
    record march 30
 reuter
</text>]