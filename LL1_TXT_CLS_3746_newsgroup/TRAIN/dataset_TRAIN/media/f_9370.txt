[<text>
<title>some firms curb commodities business with drexel</title>
<dateline>     new york, march 25 - </dateline>&lt;drexel burnham lambert inc&gt; said
some wall street firms have limited their commodities trading
business with drexel.
    rumors circulated in financial and commodity markets
yesterday that units of &lt;shearson lehman brothers inc&gt;, salomon
inc &lt;sb&gt; and &lt;goldman, sachs and co&gt; had restricted commodities
business with drexel because of its involvement in the
government's insider trading probe.
    in response to questions, a drexel spokesman said, "any
questions raised about our credit conditions are ridiculous as
those few competitors who have instituted restrictions for
whatever their motives well know."
    securities industry sources said the three firms were
restricting their business with drexel particularly in oil and
precious metals. the firms have restricted business in some
areas, but not others, the sources said.
    all three firms declined to comment, and the drexel
spokesman did not identify the companies by name.
    drexel and some officials, including michael milken, head
of its "junk bond" department, were subpoenaed following the
government's settlement of insider trading charges against
arbitrager ivan boesky.
    securities industry sources said the three firms were
restricting their business with drexel particularly in oil and
precious metals. the firms have restricted business in some
areas, but not others, the sources said.
    all three firms declined to comment, and the drexel
spokesman did not identify the companies by name.
    drexel and some officials, including michael milken, head
of its "junk bond" department, were subpoenaed following the
government's settlement of insider trading charges against
arbitrager ivan boesky.
    "our commodity trading group, &lt;dbl trading corp&gt;, is
operating normally and very profitably. it has had absolutely
no disruption in its trading and operation," a drexel spokesman
said.
    he added, "the financial picture of the firm as a whole has
never been healthier. we have capital of 1.9 billion dlrs, the
fourth largest on wall street, equity of 1.3 billion dlrs, and
excess net capital of one billion dlrs."
    "people who invent and circulate rumors about any financial
institution, including drexel burnham, are doing a serious,
grave disservice to the investing public and marketplace," the
spokesman said.
 reuter
</text>]