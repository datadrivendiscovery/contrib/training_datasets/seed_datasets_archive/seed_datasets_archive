[<text>
<title>investment group ups stake in scandinavia &lt;scf&gt;</title>
<dateline>    washington, march 26 - </dateline>a multinational shareholder group
told the securities and exchange commission it increased its
stake in scandinavia fund inc to 35.5 pct, from 30.5 pct.
    the investors include ingemar rydin industritillbehor ab,
of sweden, and vbi corp, of the west indies.
 reuter
</text>]