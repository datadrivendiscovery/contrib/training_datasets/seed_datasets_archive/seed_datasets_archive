[<text>
<title>inspeech &lt;insp&gt; buys norma bork, bork medical</title>
<dateline>    valley forge, pa., april 1 - </dateline>inspeech inc said it acquired
norma bork associates inc and bork medical services inc for
undisclosed terms.
    these firms, with combined revenues of about one mln dlrs,
are providers of speech pathology, physical therapy and
occupational therapy services.
 reuter
</text>]