[<text>
<title>buell industries inc &lt;bue&gt; sets quarterly</title>
<dateline>    waterbury, conn., june 18 -
    </dateline>qtly div eight cts vs eight cts prior
    pay aug 28
    record aug three
 reuter
</text>]