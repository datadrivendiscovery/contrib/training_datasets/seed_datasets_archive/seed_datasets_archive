[<text>
<title>antonovich &lt;furs&gt; initial offering under way</title>
<dateline>    new york, april 1 - </dateline>antonovich inc said an initial offering
of 750,000 class a common shares is under way at 9.625 dlrs
each through underwriters led by evans and co inc.
    it said it is selling 500,000 shares and shareholders the
rest.
 reuter
</text>]