[<text>
<title>manufacturers hanover corp &lt;mhc&gt; 3rd qtr net</title>
<dateline>    new york, oct 20 -
    </dateline>shr profit 2.73 dlrs vs 2.29 dlrs
    net 129.1 mln vs 105.8 mln
    nine mths
    shr loss 28.33 dlrs vs profit 6.42 dlrs
    net loss 1.16 billion vs profit 301.8 mln
    note: 3rd qtr includes previously reported gain of 55.0 mln
dlrs, or 29.4 mln after-tax, by capturing excess pension funds.
    nine mths include 1.7 billion dlr addition to loan loss
reserves in 2nd qtr, mostly for shaky ldc debts.
 reuter
</text>]