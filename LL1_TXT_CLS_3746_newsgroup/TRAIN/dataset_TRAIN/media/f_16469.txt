[<text>
<title>franklin minnesota sets monthly payout</title>
<dateline>    san mateo, calif., april 13 -
    </dateline>mthly div 6.6 cts vs 6.6 cts prior
    pay april 30
    reord april 15
    note: franklin minnesota insured tax-free income fund.
 reuter
</text>]