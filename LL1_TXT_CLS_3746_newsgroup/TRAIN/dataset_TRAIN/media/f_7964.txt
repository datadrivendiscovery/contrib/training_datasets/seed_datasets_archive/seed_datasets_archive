[<text>
<title>yankee cos &lt;ynk&gt; unit to sell asssets</title>
<dateline>    cohasset, n.y., march 20 - </dateline>yankee cos inc's eskey inc &lt;esk&gt;
subsidiary said it reached an agreement in principle to sell
its eskey's yale e. key inc subsidiary to a new concern formed
by key's management and a private investor for about 15.5 mln
dlrs.
    as part of sale, eskey said the buyers will assume the 14.5
mln dlrs of publicly held eskey 10-3/4 pct debentures due 2003.
it said the debentures will continue to be converted into
yankee preferred. the remainder of the price will be a one mln
dlr note to eskey. yankee said the sale will result in a loss
of 1.5 mln dlrs.
 reuter
</text>]