[<text>
<title>nyfe seat sells for 1,500 dlrs</title>
<dateline>    new york, april 3 - </dateline>the new york stock exchange said a seat
on the new york futures exchange sold for 1,500 dlrs, which is
up 250 dlrs from the previous sale yesterday.
    the exchange said the current bid is 1,250 and the current
offer is 1,500 dlrs.
 reuter
</text>]