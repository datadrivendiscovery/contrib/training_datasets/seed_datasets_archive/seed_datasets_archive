[<text>
<title>independent bank corp &lt;ibcp&gt; regular dividend</title>
<dateline>    ionia, mich., april 8 -
    </dateline>qtly div 10 cts vs 10 cts previously
    pay april 20
    record april 10
 reuter
</text>]