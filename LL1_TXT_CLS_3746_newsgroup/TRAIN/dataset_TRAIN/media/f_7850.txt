[<text>
<title>texas instruments inc &lt;txn&gt; sets quarterly</title>
<dateline>    dallas, march 20 -
    </dateline>qtly div 50 cts vs 50 cts prior
    pay april 20
    record march 31
 reuter
</text>]