[<text>
<title>fao approves emergency food aid for ethiopia</title>
<dateline>    rome, april 1 - </dateline>the united nations food and agriculture
organization, fao, said it approved emergency food aid worth
more than 14.3 mln dlrs for drought victims in ethiopia.
    the aid will include 30,000 tonnes of wheat and 1,200
tonnes of vegetable oil for farmers in the wollo and illubabor
regions.
    fao said it has also approved more than 1.4 mln dlrs of
food aid for 8,000 families in sri lanka. in addition, 583,225
dlrs of aid will be made available to malawi to feed 96,700
people displaced from mozambique and a further 340,200 dlrs for
cyclone victims in vanuatu in the south pacific.
 reuter
</text>]