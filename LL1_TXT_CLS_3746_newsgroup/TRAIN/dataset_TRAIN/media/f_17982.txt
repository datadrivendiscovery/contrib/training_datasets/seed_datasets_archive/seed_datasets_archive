[<text>
<title>unilever capital corp issues nz dlr eurobond</title>
<dateline>    london, june 2 - </dateline>unilever capital corp nv is issuing a 65
mln new zealand dlr eurobond due july 7, 1989 with an 18-1/4
pct coupon and priced at 101-1/4, bookrunner hambros bank ltd
said.
    joint lead managers are ebc amro bank ltd and hambros.  the
bonds, guaranteed by unilever plc, will be issued in
denominations of 1,000 and 5,000 dlrs and listed in luxembourg.
    fees comprise 1/2 pct for management and underwriting
combined and 7/8 pct for selling.
 reuter
</text>]