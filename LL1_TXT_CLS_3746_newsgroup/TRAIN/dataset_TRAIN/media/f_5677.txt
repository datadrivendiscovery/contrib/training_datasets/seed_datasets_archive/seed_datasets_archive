[<text>
<title>three d departments inc &lt;tdd&gt; 2nd qtr jan 31</title>
<dateline>    east hartford, conn., march 16 -
    </dateline>shr four cts vs 15 cts
    net 132,851 vs 501,537
    revs 10 mln vs 15.5 mln
    six mths
    shr 12 cts vs 24 cts
    net 409,383 vs 812,045
    revs 19.4 mln vs 29.1 mln
 reuter
</text>]