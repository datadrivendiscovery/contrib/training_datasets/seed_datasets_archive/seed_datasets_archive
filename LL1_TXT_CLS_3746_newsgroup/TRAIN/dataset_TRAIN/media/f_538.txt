[<text>
<title>cronus industries inc &lt;crns&gt; 4th qtr loss</title>
<dateline>    dallas, march 2 -
    </dateline>oper shr loss 40 cts vs loss 10 cts
    oper net loss 2,136,000 vs loss 467,000
    revs 21.9 mln vs 12.9 mln
    12 mths
    oper shr loss 63 cts vs loss 30 cts
    oper net loss 3,499,000 vs loss 1,756,000
    revs 82.0 mln vs 54.5 mln
    note: excludes income from discontinued operations of
1,478,000 vs 952,000 for qtr, and 31.2 mln vs 6,500,000 for
year.
    excludes extraordinary charge of 2,503,000 for current qtr,
and 4,744,000 for year.
 reuter
</text>]