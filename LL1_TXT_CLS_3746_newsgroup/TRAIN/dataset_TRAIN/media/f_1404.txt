[<text>
<title>&lt;dexleigh corp&gt; six mths dec 31 net</title>
<dateline>    toronto, march 3 -
    </dateline>shr four cts vs three cts
    net 4,505,000 vs 4,493,000
    revs 23.3 mln vs 21.4 mln
 reuter
</text>]