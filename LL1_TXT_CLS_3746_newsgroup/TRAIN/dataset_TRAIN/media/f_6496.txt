[<text>
<title>sierra spring water co &lt;wtr&gt; 4th qtr loss</title>
<dateline>    sacramento, calif., march 18 -
    </dateline>shr loss 10 cts vs profit six cts
    net loss 986,000 vs profit 576,000
    rev 9.6 mln vs 9.1 mln
    year
    shr loss seven cts vs profit 27 cts
    net loss 714,000 vs profit 2,299,000
    rev 42.8 mln vs 34.6 mln
 reuter
</text>]