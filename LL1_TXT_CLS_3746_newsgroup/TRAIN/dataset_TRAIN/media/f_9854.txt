[<text>
<title>essex chemical &lt;esx&gt; sells convertible debt</title>
<dateline>    new york, march 26 - </dateline>essex chemical corp is raising 60 mln
dlrs via an offering of convertible subordinated debentures due
2012 with a six pct coupon and par pricing, said lead manager
thomson mckinnon securities inc.
    the debentures are convertible into the company's common
stock at 40 dlrs per share, representing a premium of 23.6 pct
over the stock price when terms on the debt were set.
    non-callable for two years, the issue is rated b-1 by
moody's investors service inc and b by standard and poor's
corp. painewebber inc co-managed the deal.
 reuter
</text>]