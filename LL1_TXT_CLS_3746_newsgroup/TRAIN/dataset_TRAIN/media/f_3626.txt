[<text>
<title>regency cruises inc &lt;ship&gt; 4th qtr net</title>
<dateline>    new york, march 11 -
    </dateline>shr profit nine cts vs loss two cts
    net profit 1,419,000 vs loss 314,000
    revs 8,097,000 vs 4,794,000
    avg shrs 15.8 mln vs 15.5 mln
    year
    shr profit 37 cts vs loss 10 cts
    net profit 5,695,000 vs loss 1,268,000
    revs 40.9 mln vs 4,794,000
    avg shrs 15.6 mln vs 12.5 mln
    note: company began operations nov 17, 1985.
 reuter
</text>]