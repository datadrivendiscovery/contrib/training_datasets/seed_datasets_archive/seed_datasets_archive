[<text>
<title>&lt;m-corp inc&gt; raises dividend</title>
<dateline>    montreal, march 6 -
    </dateline>semi-annual div 7-1/2 cts vs five cts
    pay april nine
    record march 26
 reuter
</text>]