[<text>
<title>sandustry plastics inc &lt;spi&gt; 4th qtr net</title>
<dateline>    new york, march 26 -
    </dateline>shr seven cts vs 26 cts
    net 200,000 vs 450,000
    revs 7,291,000 vs 1,177,000
    12 mths
    shr 37 cts vs 77 cts
    net 801,000 vs 1,329,000
    revs 26 mln vs 28.6 mln
    note: 1985 year includes extraordinary gain of 10 cts per
share.
 reuter
</text>]