[<text>
<title>santos to acquire total exploration australia</title>
<dateline>    adelaide, june 2 - </dateline>santos ltd &lt;stos.s&gt; said it would buy
&lt;total exploration australia pty ltd&gt; from &lt;total holdings
(australia) pty ltd, a wholly-owned subsidiary of total-cie
francaise des petroles &lt;tpn.pa&gt;.
    total exploration had interests ranging from 18.75 to 25
pct in four blocks in permit atp259p in south-west queensland,
santos said in a statement.
    the santos group stakes will rise to between 52.5 and 70
pct of the four atp259p blocks as a result of the purchase. the
price was not disclosed.
    santos said a number of oil and gas fields have been
discovered in the total exploration areas and that it regards
them as having very good prospects for further discoveries.
    total's reserves amount to 75 billion cubic feet of gas and
5.5 mln barrels of oil and condensate, it said.
    it said it will promote a vigorous exploration program in
the areas for the rest of 1987 and in the future.
    the acquisition is the latest in a series by santos as part
of a program to expand from its origins in the south australian
cooper basin.
 reuter
</text>]