[<text>
<title>nynex &lt;nyn&gt; unit files to offer debt secutities</title>
<dateline>    new york, march 30 - </dateline>new york telephone co, a unit of nynex
corp, said it filed with the securities and exchange commission
a shelf registration statement covering up to 500 mln dlrs of
debt securities.
    proceeds will be to refinance outstanding debt or for
general corporate purposes, the company said.
 reuter
</text>]