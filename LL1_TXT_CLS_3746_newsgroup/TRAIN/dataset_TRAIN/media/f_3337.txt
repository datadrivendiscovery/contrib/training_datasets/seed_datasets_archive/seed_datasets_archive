[<text>
<title>france to request permanent maize rebates</title>
<dateline>    paris, march 9 - </dateline>french maize producers will ask the ec
commission to grant permanent maize export rebates following
the recent ec/u.s. accord guaranteeing the u.s. an annual
export quota of two mln tonnes of maize for spain over four
years, the french maize producers association, agpm, said.
    the commission has already decided to accord rebates for
the export of 500,000 tonnes of french maize, of which rebates
for around 100,000 tonnes have been granted.
    the request will be made when export certificates have been
granted for all the 500,000 tonnes, the agpm said.
    the association said that the request would cover exports
to all destinations, adding that the soviet union, which has
important maize needs, is currently excluded from the list of
destination countries for the 500,000 tonnes of french maize.
    the u.s. agriculture department has forecast soviet maize
imports for the 1986/87 campaign at 4.90 mln tonnes against
10.40 mln in 1985/86.
 reuter
</text>]