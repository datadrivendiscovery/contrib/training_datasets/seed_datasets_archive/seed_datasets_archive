[<text>
<title>vulcan corp &lt;vul&gt; regular dividend</title>
<dateline>    cincinnati, april 8 -
    </dateline>qtly div 20 cts vs 20 cts in prior qtr
    payable june 10
    record may 22
 reuter
</text>]