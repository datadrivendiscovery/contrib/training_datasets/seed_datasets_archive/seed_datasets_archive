[<text>
<title>atlas yellowknife &lt;ay.to&gt; six mths loss</title>
<dateline>    calgary, alberta, june 2 - </dateline>period ended march 31
    shr loss nil vs profit one ct
    net loss 36,000 vs profit 310,000
    revs 1,172,000 vs 1,686,000
    note: full name atlas yellowknife resources ltd.
 reuter
</text>]