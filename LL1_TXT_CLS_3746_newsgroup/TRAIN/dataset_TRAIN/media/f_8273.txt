[<text>
<title>market loan could be pinned to u.s. trade bill</title>
<dateline>    boca raton, fla., march 23 - </dateline>sen. david pryor, d-ark., said
he was considering amending the senate finance committee's
trade bill with a provision to require a marketing loan for
soybeans, corn and wheat.
    pryor told the futures industry association that there was
great reluctance among members of the senate agriculture
committee to reopen the 1985 farm bill, and that a marketing
loan might have a better chance in the finance panel.
    the arkansas senator said the marketing loan -- which in
effect allows producers to pay back their crop loans at the
world price -- had led to a 300 pct increase in u.s. cotton
exports in 14 months and a 72 pct increase in rice exports.
    pryor serves on both the senate finance and agriculture
committees.
 reuter
</text>]