[<text>
<title>medco containment &lt;mccs&gt; sets initial payout</title>
<dateline>    elmwood park, n.j., march 2 - </dateline>medco containment services
inc said its board declared an initial annual dividend of 10
cts per share, its first payout, payable march 19 to holders of
record march 12.
 reuter
</text>]