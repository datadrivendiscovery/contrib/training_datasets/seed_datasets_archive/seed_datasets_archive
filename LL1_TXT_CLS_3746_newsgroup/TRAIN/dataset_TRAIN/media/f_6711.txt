[<text>
<title>novar electronics corp &lt;novr&gt; 4th qtr jan three</title>
<dateline>    barberton, ohio, march 18 -
    </dateline>shr loss eight cts vs loss eight cts
    net loss 220,724 vs loss 210,120
    revs 4,194,466 vs 4,224,633
    year
    shr profit eight cts vs profit four cts
    net profit 207,514 vs profit 98,050
    revs 17.8 mln vs 16.1 mln
    note: quarter net includes tax credits of 162,600 dlrs vs
236,100 dlrs.
 reuter
</text>]