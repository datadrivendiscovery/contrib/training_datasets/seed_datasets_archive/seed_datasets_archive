[<text>
<title>montedison concludes talks with antibioticos</title>
<dateline>    milan, march 2 - </dateline>montedison spa &lt;moni.mi&gt; said it has
concluded its negotiations with spanish pharmaceuticals company
&lt;antibioticos sa&gt;.
    a company spokesman told reuters "we have concluded the
talks and we are now awaiting authorization from spanish
authorities." he declined to comment further.
    earlier today the italian company postponed a scheduled
press conference on its talks with antibioticos. an italian
press report today said montedison has agreed to acquire
antibioticos for 500 billion lire.
 reuter
</text>]