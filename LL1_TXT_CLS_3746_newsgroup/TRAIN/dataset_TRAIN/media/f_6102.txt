[<text>
<title>noel industries inc &lt;nol&gt; 1st qtr jan 31 loss</title>
<dateline>    new york, march 17 -
    </dateline>shr loss 26 cts vs loss 12 cts
    net loss 289,649 vs loss 138,372
    revs 5,944,286 vs 5,902,074
 reuter
</text>]