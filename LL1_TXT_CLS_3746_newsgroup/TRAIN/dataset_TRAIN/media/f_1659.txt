[<text>
<title>digicon &lt;dgc&gt; completes sale of unit</title>
<dateline>    houston, march 4 - </dateline>digicon inc said it has completed the
previously-announced disposition of its computer systems
division to an investment group led by &lt;rotan mosle inc's&gt;
rotan mosle technology partners ltd affiliate.
 reuter
</text>]