[<text>
<title>great western financial &lt;gwf&gt; unit buys banks</title>
<dateline>    beverly hills, calif., march 5 - </dateline>great western financial
corp's subsidiary great western bank said it will purchase
three retail banking branches in south florida with total
deposits of 90 mln dlrs.
    great western said it will purchase branches in deerfield
beach and hollywood with approximately 80 mln dlrs in deposits
from guardian savings and loan association, and one in palm
beach with approximately 10 mln in deposits from goldome
savings bank.
 reuter
</text>]