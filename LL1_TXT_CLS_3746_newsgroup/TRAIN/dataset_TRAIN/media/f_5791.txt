[<text>
<title>u.s. agriculture secretary warns ec on soy oil tax</title>
<dateline>    champaign, ill., march 17 - </dateline>u.s. agriculture secretary
richard lyng warned the european community yesterday it will
face serious retaliation if it enacts a new tax on products
such as u.s. soybean oil.
    speaking at a news conference before a scheduled speech,
lyng said he did not think the tax, which is still in the
discussion stage, would be approved.
    he said the u.s. would take serious retaliatory action
because if implemented, the tax would have a considerable
impact on u.s. farmers.
 reuter
</text>]