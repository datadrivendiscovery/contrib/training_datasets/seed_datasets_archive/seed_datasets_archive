[<text>
<title>integrated generics &lt;ign&gt; unit in negotiations</title>
<dateline>    bellport, n.y., march 31 - </dateline>intergrated generics inc said
its biopharmaceutics inc subsidiary is in negotiations with a
manufacturer and distributor of generic equivalents of brand
name prescription drugs.
    the company said a successful agreement would allow it to
distribute chlorazepate dipotasssium, an anti-anxiety drug with
a market potential of 80 mln dlrs.
    integrated said the licensor has asked for anonymity at
this time. finalization of the agreement is expected within two
or three weeks, the company said.
 reuter
</text>]