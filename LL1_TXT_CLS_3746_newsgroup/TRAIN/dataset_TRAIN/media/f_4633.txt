[<text>
<title>japan minister says about 170 yen appropriate</title>
<dateline>    tokyo, march 13 - </dateline>international trade and industry minister
hajime tamura told a parliamentary session japan's small- and
medium-sized enterprises are seriously suffering from the yen's
rise and can only stand levels around 170 yen.
    he also said he still believes a dollar exchange rate level
plus or minus 10 yen from 170 yen would be within levels agreed
upon last month in paris by six major industrial nations.
finance ministers of britain, canada, france, japan, the u.s.
and west germany agreed on february 22 to cooperate in
stabilizing exchange rates around the current levels. the
dollar had closed here at 153.77 yen on february 20.
 reuter
</text>]