[<text>
<title>nippon shinpan to issue credit card with mastercard</title>
<dateline>    tokyo, oct 19 - </dateline>nippon shinpan co ltd &lt;nsht.t&gt; has agreed
with &lt;mastercard international&gt; to issue a new credit card
called nippon shinpan-master joint card for use in japan and
abroad, a nippon shinpan spokesman said.
    the card, to be issued within the year, will be honoured at
mastercard-affiliated outlets in 160 countries and at locations
accepting the nippon shinpan card, he said.
    nippon shinpan reached a similar agreement with &lt;visa
international&gt; on september 29.
 reuter
</text>]