[<text>
<title>bei holdings ltd &lt;beih&gt; 1st qtr jan 31 net</title>
<dateline>    atlanta, march 5 -
    </dateline>shr 13 cts vs eight cts
    net 1,364,712 vs 881,082
    rev 11.6 mln vs 11.5 mln
    note: qtr includes extraordinary gain of 586,826 dlrs, or
six cts a share, versus 183,850 dlrs or two cts a share in
fiscal 1986's first qtr.
 reuter
</text>]