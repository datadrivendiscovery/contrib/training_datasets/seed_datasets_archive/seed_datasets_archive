[<text>
<title>princeville development corp &lt;pvdc&gt; year loss</title>
<dateline>    princeville, hawaii, march 5 -
    </dateline>shr diluted loss 31 cts vs profit 17 cts
    net loss 2,806,005 vs profit 1,513,395
    revs 15.0 mln vs 10.4 mln
    avg shrs diluted 8,982,754 vs 8,804,899
    note: current year includes loss of 3.4 mln dlrs from
takeover defense expenses. also includes losses of 1.8 mln dlrs
vs 332,000 dlrs from equity in limited partnerships.
 reuter
</text>]