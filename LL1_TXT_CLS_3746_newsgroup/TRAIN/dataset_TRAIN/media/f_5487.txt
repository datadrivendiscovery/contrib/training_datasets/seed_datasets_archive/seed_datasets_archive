[<text>
<title>union votes to strike dakota city ibp plant</title>
<dateline>    chicago, march 16 - </dateline>the united food and commercial workers
union, local 222 said its members voted sunday to strike the
iowa beef processors inc dakota city, neb., plant, effective
tuesday.
    the company said it submitted its latest offer to the union
at the same time announcing that on tuesday it would end a
lockout that started december 14.
    union members unanimously rejected the latest company offer
that was submitted to the union late last week, ufcw union
spokesman allen zack said.
 reuter
</text>]