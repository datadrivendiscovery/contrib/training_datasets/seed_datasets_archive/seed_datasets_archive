[<text>
<title>&lt;canadian roxy petroleum ltd&gt; year net</title>
<dateline>    calgary, alberta, march 16 -
    </dateline>shr profit five cts vs loss 23 cts
    net profit 916,000 vs loss 2,886,000
    revs 20.0 mln vs 28.2 mln
 reuter
</text>]