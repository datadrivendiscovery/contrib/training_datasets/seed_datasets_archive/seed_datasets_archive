[<text>
<title>unocal &lt;ucl&gt; unit begins plant construction</title>
<dateline>    los angeles, march 18 - </dateline>unocal corp said its desert power
subsidiary began construction of a 47,500 kilowatt geothermal
plant in southern california's imperial valley.
    the company said desert power expects the plant to be
operational by early 1989.
    the new plant will be located at the southern end of the
salton sea, about one-half mile from an existing 10,000
kilowatt generating plant owned and operated by southern
california edison co &lt;sec&gt;, unocal also said.
 reuter
</text>]