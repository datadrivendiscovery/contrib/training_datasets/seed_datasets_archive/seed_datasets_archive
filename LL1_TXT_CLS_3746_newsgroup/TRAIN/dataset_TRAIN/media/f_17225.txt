[<text>
<title>algeria signs major lng deal with u.s. group</title>
<dateline>    algiers, april 27 - </dateline>algeria's national petroleum agency
sonatrach and the u.s. panhandle-trunkline &lt;pel.n&gt; group signed
a 20-year accord for the delivery of liquefied natural gas
(lng), the official aps news agency said.
    deliveries will start next winter and rise over three years
to reach 4.5 billion cubic metres annually, with 60 pct of the
gas carried in algerian ships to a terminal at lake charles,
la., aps said.
   aps said the pricing formula will "preserve the purchasing
power of lng and the interests of sonatrach, and take into
account ... the world (and) the american market."
    the agreement follows months of negotiations between
panhandle eastern petroleum corp and sonatrach over the new lng
contract.
    the talks followed an accord in july 1986 which resolved a
long-standing dispute between sonatrach and panhandle
subsidiary &lt;trunkline gas co&gt; after the u.s. group unilaterally
suspended purchases of algerian lng.
 reuter
</text>]