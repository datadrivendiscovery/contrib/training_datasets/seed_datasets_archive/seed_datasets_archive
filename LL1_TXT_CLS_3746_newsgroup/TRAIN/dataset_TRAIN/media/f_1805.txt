[<text>
<title>meridian diagnostics &lt;kits&gt; gets fda approval</title>
<dateline>    cincinnati, march 4 - </dateline>meridian diagnostics inc said it
received approval from the food and drug administration to
market a test to detect a disease which drains fluids from the
bodies of aids victims.
    the company said the test detects cryptosporidium sp in
stool specimens. the disease can result in life threatening
loss of fluids, the company added.
 reuter
</text>]