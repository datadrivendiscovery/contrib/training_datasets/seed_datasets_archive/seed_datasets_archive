[<text>
<title>usda reports corn sold sold to unknown</title>
<dateline>    washington, march 3 - </dateline>the u.s. agriculture department said
private u.s. exporters reported sales of 104,000 tonnes of corn
to unknown destinations for shipment in the 1986/87 marketing
year.
    the marketing year for corn began september 1, it said.
 reuter
</text>]