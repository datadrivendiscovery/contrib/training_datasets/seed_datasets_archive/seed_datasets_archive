[<text>
<title>licht sees rise in european beet area: trade</title>
<dateline>    london, march 11 - </dateline>west german sugar statistician f.o.
licht estimates european beet plantings this year at 7.22 mln
hectares compared with a revised 1986 figure of 7.21 mln,
traders said.
    in its first estimate for 1987, it puts ec plantings at
1.85 mln hectares compared with 1.89 mln in 1986, while it
estimates sowings in western europe (including ec) at 2.49 mln
hectares compared with 2.50 mln in 1986.
    traders said licht forecasts eastern europe plantings at
4.73 mln hectares against 4.72 mln in 1986.
 reuter
</text>]