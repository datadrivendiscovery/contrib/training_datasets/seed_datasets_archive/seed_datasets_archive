[<text>
<title>putnam launhces new global income fund</title>
<dateline>    boston, june 2 - </dateline>putnam financial services inc said its
newest fund offering, putnam global governmental income trust,
is among the few global funds with an investment objective of
high current income through a portfolio of mainly government
bonds.
    putnam claimed the trust allows investor money to go where
the best yields are found worldwide, yet investors still have
the assurance of governmental obligations.
 reuter
</text>]