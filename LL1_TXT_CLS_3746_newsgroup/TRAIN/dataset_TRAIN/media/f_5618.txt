[<text>
<title>pro-med capital &lt;prmd&gt; to be included in nms</title>
<dateline>    north miami beach, fla., march 16 - </dateline>pro-med capital inc
said its common will be included in the next expansion of the
nasdaq national market system, which will take place tomorrow.
 reuter
</text>]