[<text>
<title>elxsi &lt;elxsf&gt; wins mcdonnell douglas &lt;md&gt; order</title>
<dateline>    san jose, calif., april 1 - </dateline>elxsi ltd said it sold three
computer systems worth four mln dlrs to douglas aircraft co, a
unit of mcdonnell douglas corp.
    the company said the systems will be used in the c-17
military transport program. one was shipped last month and two
others will be shipped in june, it said.
 reuter
</text>]