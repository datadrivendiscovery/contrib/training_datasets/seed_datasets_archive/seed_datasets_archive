[<text>
<title>resdel &lt;rsdl.o&gt; to merge with san/bar &lt;sbar.o&gt;</title>
<dateline>    newport beach, calif., april 13 - </dateline>resdel industries said it
and san/bar corp has agreed to merge san/bar into resdel.
    the arrangement calls for san/bar to spin-off assets of its
break-free division to shareholders then exchange its own
shares for resdel stock at a ratio of one resdel share for each
san/bar share held, resdel said.
 reuter
</text>]