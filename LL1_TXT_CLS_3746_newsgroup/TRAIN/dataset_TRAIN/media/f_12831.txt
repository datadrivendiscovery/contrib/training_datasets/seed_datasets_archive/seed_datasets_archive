[<text>
<title>hilton &lt;hlt&gt; 1st qtr earnings up about 37 pct</title>
<dateline>    beverly hills, calif., april 3 - </dateline>hilton hotels corp said
its first quarter net income rose 37 pct, paced by strength in
both hotels and gaming.
    based on preliminary results, the company said, net income
rose to about 24 mln dlrs, or 96 cts a share, from 17.4 mln
dlrs, or 70 cts a share, in 1986's initial three months.
 reuter
</text>]