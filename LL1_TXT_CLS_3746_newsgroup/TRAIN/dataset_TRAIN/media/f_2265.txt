[<text>
<title>aequitron &lt;aqtn&gt; sees 4th qtr charge</title>
<dateline>    minneapolis, march 5 - </dateline>aequitron medical inc said costs
related to its previously announced plan to consolidate life
products operations in boulder, colo, are expected to total
720,000 dlrs, or eight cts a share for the fourth quarter
ending april 30.
    it said the costs including moving expenses, severance pay
and future lease payments.
    the company said it will consolidate life products into the
company's headquarters and manufacturing operations in
minneapolis.
 reuter
</text>]