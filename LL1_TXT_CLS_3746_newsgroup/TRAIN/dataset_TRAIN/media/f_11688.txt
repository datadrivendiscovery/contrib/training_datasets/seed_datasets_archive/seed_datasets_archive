[<text>
<title>onex packaging closes new brunswick plant</title>
<dateline>    st andrew's, new brunswick, march 31 - </dateline>&lt;onex packaging inc&gt;
said it closed a small metal can manufacturing plant at st.
andrew's employing eight people that has been inactive since
late 1985.
    onex said the plant, producing six-ounce, two-piece metal
cans for the fish industry, shut down when its major customer
suddenly ceased canning operations.
    onex has not been able to find sufficient demand to support
this type of can manufacturing in the region, it said.
 reuter
</text>]