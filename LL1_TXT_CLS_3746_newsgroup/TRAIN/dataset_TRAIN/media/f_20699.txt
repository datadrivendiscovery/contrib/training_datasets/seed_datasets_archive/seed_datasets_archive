[<text>
<title>sphinx mining &lt;spnxf.o&gt; finds gold</title>
<dateline>    fort smith, ark., oct 20 - </dateline>sphinx mining inc said leased
mining claims in alaska could produce revenues between 322 mln
dlrs and 966 mln dlrs from gold reserves.
    the range of the value of the reserves is attributed to the
wide range of grade estimates of the ore, the company said.
    a 1984 feasibility study put the grade at 0.008 ounces per
cubic yard, while subsequent exploration work proved that areas
of higher-grade gravel of up to 0.027 ounces/yard do exist,
sphinx said.
    the claims are located 80 miles northwest of fairbanks.
 reuter
</text>]