[<text>
<title>&lt;biflyx&gt; 2nd qtr dec 31 net</title>
<dateline>    garden grove, calif., march 20 -
    </dateline>shr nil vs nil
    net profit 24,000 vs loss 66,000
    revs 235,000 vs 93,000
    six mths
    shr nil vs nil
    net profit 40,000 vs loss 153,000
    revs 394,000 vs 99,000
 reuter
</text>]