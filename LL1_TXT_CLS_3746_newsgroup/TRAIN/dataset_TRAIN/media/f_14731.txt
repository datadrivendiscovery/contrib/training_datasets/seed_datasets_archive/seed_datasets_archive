[<text>
<title>logicon inc &lt;lgn&gt; gets contract add-ons</title>
<dateline>    los angeles, april 7 - </dateline>logicon inc said it has received a
12 mln dlr add-on contract, including options, to continue
developing the joint analytic warfare system for the
organization of the joint chiefs of staff.
    the new award increases the total contract value, including
options, to over 13.8 mln dlrs, including two mln dlrs for the
initial nine-month period and another ten mln dlrs in options
over the next five years.
 reuter
</text>]