[<text type="unproc">
klm net profit declines in 1986/87 year
    amsterdam, june 18
    year ended march 31
    net profit 301 mln guilders vs 312 mln
    profit per 20 guilder nominal ordinary share 5.93 guilders
vs 6.14 on unchanged 50.85 mln shares.
    operating revenues 5.38 billion guilders vs 5.85 billion.
    profit on sale of fixed assets 23 mln guilders vs 62 mln.
    dividend eight pct or 1.60 guilders per ordinary share vs
same, and five pct vs same for priority and preferential
shares.
    costs including depreciation 5.08 billion guilders vs 5.59
billion.
    financial charges 34 mln vs 33 mln.
    profit from participations 11 mln vs 15 mln.
    extraordinary gain one mln vs nil.
    note: the company was not required to pay corporation tax
in the 1986/87 book year thanks to fiscal compensation
possibilities. the full company name is koninklijke luctvaart
maatschappij nv &lt;klm.as&gt;.
    klm released provisional figures on may 26.
    these were -
    net profit 301 mln guilders vs 312 mln.
    operating revenues 5.4 billion guilders vs 5.9 billion.
 reuter


</text>]