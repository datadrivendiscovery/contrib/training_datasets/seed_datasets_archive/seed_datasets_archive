[<text>
<title>chrysler &lt;c&gt; to run six of seven u.s. plants</title>
<dateline>    detroit, march 26 - </dateline>chrysler corp said it scheduled six of
its seven u.s. car and truck assembly plants to operate the
week of march 30, with four on overtime.
     it also said one plant will operate saturday, march 28.
 reuter
</text>]