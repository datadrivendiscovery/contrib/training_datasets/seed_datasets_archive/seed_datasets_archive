[<text>
<title>richardson &lt;rell&gt;to buy italian semiconductor firm</title>
<dateline>    lafox, ill., march 19 - </dateline>richardson electronics ltd said it
agreed to buy for undisclosed terms, g.e.b., giant electronics
brand, a florence, italy-based distributor of electron tubes
and semiconductors.
 reuter
</text>]