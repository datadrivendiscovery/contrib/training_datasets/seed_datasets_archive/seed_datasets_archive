[<text>
<title>&lt;daiwa securities co&gt; to supply market update</title>
<dateline>    new york, march 2 - </dateline>daiwa securities co ltd said it will
provide financial news network &lt;fnni&gt; with an exclusive daily
market update from tokyo.
    the report can be seen on fnn's world business update, the
company said.
    daiwa securities said the new program describes major
business developments around the world, using videotaped news
and feature stories as well as market and commodities
information.
    viewers will be able to get closing prices in tokyo prior
to opening trading in new york due to the time difference.
 reuter
</text>]