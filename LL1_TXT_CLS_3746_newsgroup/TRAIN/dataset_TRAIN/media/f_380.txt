[<text>
<title>dh technology &lt;dhtk&gt; chairman sells shares</title>
<dateline>    san diego, march 2 - </dateline>dh technology inc said it has
repurchased 500,000 of its shares from cofounder helmut falk at
4.25 dlrs each and falks has sold another 500,000 shares to
venture capital firm ta associates at the same price.
    the company said falk has resigned as chairman of dhl and
now owns 213,567 shares.  it said ta now owns 928,0000 shares.
    the company said falk, who will remain on the board, has
agreed to sell no more than 75,000 of his remaining shares in
the next year without company consent.  it said president and
chief executive officer william h. gibb has assumed the added
post of chairman.
 reuter
</text>]