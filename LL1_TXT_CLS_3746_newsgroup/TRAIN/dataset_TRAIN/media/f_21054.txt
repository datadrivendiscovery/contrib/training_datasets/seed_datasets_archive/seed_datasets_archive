[<text>
<title>king world &lt;kwp&gt; says show extended</title>
<dateline>    new york, oct 19 - </dateline>king world productions inc said its
syndicated television series "the oprah winfrey show" has been
extended through the 1989-90 broadcast season.
    it said six of the top 10 markets have renewed the series
through the end of the decade.
 reuter
</text>]