[<text>
<title>dow jones falls more than 400 points</title>
<dateline>    new york, oct 19 - </dateline>the dow jones industrial average plunged
to 1844.97, a 404 point decline in the leading market
indicator.
    the last time the dow index touched the 1840 level was in
november, 1986.
 reuter
</text>]