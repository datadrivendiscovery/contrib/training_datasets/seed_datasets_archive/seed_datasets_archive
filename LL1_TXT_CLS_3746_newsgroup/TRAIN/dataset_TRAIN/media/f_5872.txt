[<text>
<title>western health &lt;whp&gt; president resigns</title>
<dateline>    san diego, march 17 - </dateline>western health plans inc said douglas
c. werner has resigned as president and chief executive officer
to pursue other business opportunities.
    it said executive vice president ray mendoza will serve as
interim chief executive.
 reuter
</text>]