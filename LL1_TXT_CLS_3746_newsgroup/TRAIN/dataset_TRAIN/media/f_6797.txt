[<text>
<title>emhart corp &lt;emh&gt; set to increase earnings</title>
<dateline>    boston, march 18 - </dateline>emhart corp said it plans to increase
worldwide revenues and earnings at an annual compounded growth
rate of 15 pct and about 13 pct, respectively.
    it said these objectives were based on several assumptions,
including a four pct average inflation rate through 1989 and a
two pct to three pct gnp real growth.
    in 1986, emhart reported a net loss of 10 mln dlrs or 35
cts a share, after a 90 mln after-tax restructuring charge,
which realigned the company's assets.
    emhart has divested itself of many of its units to focus on
three primary markets--industrial products, consumer products,
and information and electronic systems.
    emhart said industrial products should account for about 62
pct of projected 1987 revenues of 2.3 billion dlrs, while
consumer products should account for about 20 pct of those
revenues and information and electronic systems about 18 pct.
 reuter
</text>]