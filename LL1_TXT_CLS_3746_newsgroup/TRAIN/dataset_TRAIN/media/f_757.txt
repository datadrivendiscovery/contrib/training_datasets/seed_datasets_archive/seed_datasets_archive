[<text>
<title>u.s. approves buyout of coastal bancorp &lt;csbk&gt;</title>
<dateline>    portland, maine, march 2 - </dateline>coastal bancorp said the u.s.
federal reserve board approved the acquisition of coastal by
suffield financial corp &lt;ssbk&gt;.
    the acquisition still requires approval from the banking
department in maine, the company noted.
 reuter
</text>]