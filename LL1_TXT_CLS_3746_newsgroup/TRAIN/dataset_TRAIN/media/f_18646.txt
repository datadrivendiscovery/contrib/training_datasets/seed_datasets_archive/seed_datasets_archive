[<text>
<title>price co &lt;pclb.o&gt; 3rd qtr june seven net</title>
<dateline>    san diego, june 18 -
    </dateline>shr 30 cts vs 24 cts
    net 14.7 mln vs 11.3 mln
    sales 738.9 mln vs 605.1 mln
    avg shrs 49.0 mln vs 47.9 mln
    nine mths
    shr 1.11 dlrs vs 93 ctsd
    net 54.2 mln vs 42.9 mln
    sales 2.45 billion vs 1.95 billion
    avg shrs 48.9 mln vs 46.4 mln
    note: twelve- and 40-week periods.
 reuter
</text>]