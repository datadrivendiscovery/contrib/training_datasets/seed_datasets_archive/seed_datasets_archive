[<text>
<title>polydex pharmaceuticals ltd &lt;polxf.o&gt; 1st qtr</title>
<dateline>    toronto, june 1 - </dateline>april 30 end
    shr loss one ct vs loss two cts
    net loss 83,116 vs loss 266,037
    sales 1,393,455 vs 1,035,500
 reuter
</text>]