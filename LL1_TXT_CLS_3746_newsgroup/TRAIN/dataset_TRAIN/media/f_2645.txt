[<text>
<title>&lt;peter miller apparel group inc&gt; 3rd qtr loss</title>
<dateline>    toronto, march 6 - </dateline>period ended january 31
    shr loss 28 cts vs profit seven cts
    net loss 931,000 vs profit 7,000
    sales 2,303,000 vs 2,006,000
    nine mths
    shr loss 55 cts vs profit seven cts
    net loss 1,619,000 vs profit 185,000
    sales 7,684,000 vs 7,059,000
    note: per share reflects issue of 600,000 shares in october
1986.
 reuter
</text>]