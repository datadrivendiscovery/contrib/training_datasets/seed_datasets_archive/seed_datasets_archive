[<text>
<title>allied-signal &lt;ald&gt; unit to cut employees</title>
<dateline>    arlington, va., april 7 - </dateline>allied-signal inc's bendix test
systems division is cutting between 75 and 125 employes as part
of its streamlining efforts.
    bendix, which is part of the aircraft systems co of
allied-signal's aerospace's bendix sector, said the cuts would
create a more cost-effective unit. it said employment is
expected to remain at approximately 1,200 employes.
    the reductions began last month when the division announced
a voluntary retirement program for eligible employes.
 reuter
</text>]