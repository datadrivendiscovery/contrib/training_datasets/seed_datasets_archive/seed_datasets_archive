[<text>
<title>decision expected on u.k royal ordnance sale</title>
<dateline>    london, april 2 - </dateline>u.k. defence secretary george younger is
expected to announce the government's decision on the sale of
state-owned arms manufacturer &lt;royal ordnance&gt; today,
parliamentary sources said.
    the government originally intended to float the munitions
and explosives concern on the stock market, but last july said
a private sale was a more appropriate way to dispose of the
firm.
    the bidders for the company were british aerospace plc
&lt;bael.l&gt; and engineering group gkn plc &lt;gkn.l&gt;. royal ordnance
sold its leeds tank factory last summer to vickers plc
&lt;vick.l&gt;.
    defence electronics manufacturer ferranti plc &lt;fnti.l&gt; and
shipping and property group trafalgar house plc &lt;thsl.l&gt; both
pulled out of the bidding shortly before last month's deadline.
    royal ordnance made pre-tax profits of 26 mln stg on sales
of 487 mln stg in calendar 1985, its first full year of
commercial operation. the company has assets of around 240 mln
stg and employs 17,000 at 15 sites in britain.
    other state-held companies earmarked for privatisation this
year include engine maker &lt;rolls royce plc&gt; and the &lt;british
airports authorities plc&gt;.
 reuter
</text>]