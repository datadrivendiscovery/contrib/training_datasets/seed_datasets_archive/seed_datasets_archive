[<text>
<title>l'air liquide &lt;airp.pa&gt; year ended dec 31</title>
<dateline>    paris, march 19 -
    </dateline>parent company net profit 754.45 mln francs vs 674.1 mln.
    dividend 19.50 francs vs same.
    note - company said the dividend would apply to shares
issued under capital increases during 1986. this means a 32 pct
rise in total dividend payments to 528.14 mln francs on 1986
results from 399.62 mln the previous year.
 reuter
</text>]