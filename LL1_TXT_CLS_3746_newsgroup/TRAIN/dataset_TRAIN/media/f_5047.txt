[<text>
<title>armatron &lt;art&gt; negotiates new credit line</title>
<dateline>    melrose, mass., march 13 - </dateline>armatron international inc said
it negotiated a new seasonal line of credit with three lenders
for 10 mln dlrs for working capital requirements to support its
lawn and garden product line.
 reuter
</text>]