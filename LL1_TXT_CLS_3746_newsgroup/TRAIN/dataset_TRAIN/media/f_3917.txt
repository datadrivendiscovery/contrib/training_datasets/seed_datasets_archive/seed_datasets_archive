[<text>
<title>bangor hydro-electric co &lt;bang&gt; sets dividend</title>
<dateline>    bangor, maine, march 11 -
    </dateline>qtly div 25 cts vs 25 cts
    pay april 20
    record march 31
 reuter
</text>]