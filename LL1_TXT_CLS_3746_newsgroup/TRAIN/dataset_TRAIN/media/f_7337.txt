[<text>
<title>great &lt;gami&gt; to buy standard&lt;srd&gt; units' assets</title>
<dateline>    chicago, march 19 - </dateline>great american management and
investment inc said its 80 pct-owned subsidiary agreed to buy
certain assets of two subsidiaries of standard oil co, for 40
mln dlrs and the assumption of certain liabilities.
    great american industrial group inc agreed to acquire all
the u.s. and united kingdom assets of pfaudler co and the stock
of certain brazilian, mexican, and west german subsidiaries of
kennecott mining corp, it said. pfaudler and kennecott are
subsidiaries of standard oil.
 reuter
</text>]