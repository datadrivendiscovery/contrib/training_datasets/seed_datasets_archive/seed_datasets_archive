[<text>
<title>ball corp &lt;bll&gt; 3rd qtr net</title>
<dateline>    muncie, ind., oct 20 -
    </dateline>shr 80 cts vs 72 cts
    net 18,900,000 vs 17,100,000
    sales 267.4 mln vs 288.6 mln
    nine mths
    shr 2.26 dlrs vs 2.04 dlrs
    net 53,600,000 vs 48,200,000
    sales 839.3 mln vs 836.3 mln
 reuter
</text>]