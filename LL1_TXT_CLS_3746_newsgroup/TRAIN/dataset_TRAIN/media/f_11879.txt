[<text>
<title>tempo enterprises inc &lt;tpo&gt; year net</title>
<dateline>    tulsa, april 1 -
    </dateline>shr 40 cts vs 36 cts
    net 2,309,000 vs 2,076,000
    revs 28.2 mln vs 30.4 mln
 reuter
</text>]