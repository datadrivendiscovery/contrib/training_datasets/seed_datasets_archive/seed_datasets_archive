[<text>
<title>service resources &lt;src&gt; unit sets purchase</title>
<dateline>    new york, march 30 - </dateline>service resources corp's chas. p.
young co subsidiary said it agreed to acquire atwell fleming
printing ltd, a canadian financial printer, for about 3.2 mln
dlrs.
    young said the acquisition, which is expected to close in
april, is subject to due dilligence review.
 reuter
</text>]