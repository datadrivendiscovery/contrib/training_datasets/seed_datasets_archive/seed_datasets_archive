[<text>
<title>hre properties &lt;hre&gt; 1st qtr jan 31 net</title>
<dateline>    new york, march 17 -
    </dateline>shr 38 cts vs 47 cts
    net 2,253,664 vs 2,806,820
    gross income 5,173,318 vs 5,873,904
    note: net includes gains on sale of real estate of 126,117
dlrs vs 29,812 dlrs.
 reuter
</text>]