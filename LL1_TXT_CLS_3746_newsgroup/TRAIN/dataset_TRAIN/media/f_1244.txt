[<text>
<title>&lt;d.h. howden and co ltd&gt; increases dividend</title>
<dateline>    london, ontario, march 3 -
    </dateline>semi-annual 30 cts vs 25 cts prior
    pay june 30
    record june 15
 reuter
</text>]