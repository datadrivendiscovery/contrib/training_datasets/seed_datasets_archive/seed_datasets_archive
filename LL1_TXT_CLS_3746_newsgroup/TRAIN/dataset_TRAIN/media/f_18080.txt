[<text>
<title>3m &lt;mmm&gt; acquires control data &lt;cda&gt; unit</title>
<dateline>    st paul, minn., june 2 - </dateline>minnesota mining and manufacturing
said it acquired a computerized hospital information systems
business from control data corp.
    terms were not disclosed.
    the business, which has 145 employees and supplies
computers and software for hospital information systems, will
be integrated into 3m's hospital software business.
    control data said the divestiture was part of its strategy
to focus on narrower markets.
 reuter
</text>]