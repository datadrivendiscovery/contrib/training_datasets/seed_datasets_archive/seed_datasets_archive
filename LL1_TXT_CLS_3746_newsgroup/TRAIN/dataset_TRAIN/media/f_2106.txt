[<text>
<title>wal-mart stores &lt;wmt&gt; february sales up 44 pct</title>
<dateline>    bentonville, ark., march 5 - </dateline>wal-mart stores inc said
february sales were up 44 pct to 885 mln dlrs from 615 mln dlrs
a year before, with same-store sales up 14 pct.
 reuter
</text>]