[<text>
<title>u.k. oilmeal/veg oil production rose in 1986</title>
<dateline>    london, march 26 -  </dateline>the u.k. produced 820,400 tonnes of
oilcake and meal and 431,000 tonnes of crude vegetable oil in
calendar 1986, ministry of agriculture figures show.
    they compare with 785,800 tonnes of oilcake and meal and
407,400 tonnes of crude vegetable oil produced in 1985.
    total oilseeds crushed rose to 1.27 mln tonnes from 1.21
mln in 1985.
 reuter
</text>]