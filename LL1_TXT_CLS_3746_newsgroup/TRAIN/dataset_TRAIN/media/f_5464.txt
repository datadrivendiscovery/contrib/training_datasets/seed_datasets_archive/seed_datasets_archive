[<text>
<title>hechinger co &lt;hech&gt; 4th qtr net</title>
<dateline>    landover, md., march 16 -
    </dateline>shr primary 28 cts vs 22 cts
    shr diluted 26 cts vs 21 cts
    net 8,637,000 vs 6,577,000
    sales 140.3 mln vs 116.8 mln
    year
    shr primary 92 cts vs 77 cts
    shr diluted 88 cts vs 75 cts
    net 28.3 mln vs 23.1 mln
    sales 588.4 mln vs 479 mln
 reuter
</text>]