[<text>
<title>northern foods to sell u.s. unit for 24 mln dlrs</title>
<dateline>    london, april 21 - </dateline>northern foods plc &lt;nfds.l&gt; said its
&lt;northserv inc&gt; unit had agreed to sell &lt;flagship cleaning
services inc&gt; to &lt;best co inc&gt; of nevada for 24.6 mln dlrs
cash.
    completion is due on april 30. flagship is based in
philadelphia and holds the sears, roebuck and co &lt;s&gt; franchise
for domestic carpet and upholstery cleaning throughout the u.s.
    in the year to end march, 1986, flagship - then known as
keyserv - reported pre-tax profits of 2.0 mln dlrs with
end-year assets of 8.4 mln, giving a book profit on disposal of
16.2 mln.
    northern foods shares were unchanged at 297p.
 reuter
</text>]