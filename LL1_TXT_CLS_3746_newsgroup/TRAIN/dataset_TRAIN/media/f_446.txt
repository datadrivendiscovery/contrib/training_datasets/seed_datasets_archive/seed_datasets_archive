[<text>
<title>xerox corp &lt;xrx&gt; adds capacity to system</title>
<dateline>    rochester, n.y., march 2 - </dateline>xerox corp said it has added a
system that can handle more than 3,000 calls per hour and store
up to 526 hours of messages to its voice message exchange
product line.
    available with 12 to 64 ports, the new system v is designed
to serve 800 to 10,000 users, the company said. it is
compatible with the company's entire voice message exchange
line, it added.
    xerox said the system may be rented annually, with an
option to purchase, starting at 4,700 dlrs per month, or
purchased for 123,000 dlrs.

 reuter
</text>]