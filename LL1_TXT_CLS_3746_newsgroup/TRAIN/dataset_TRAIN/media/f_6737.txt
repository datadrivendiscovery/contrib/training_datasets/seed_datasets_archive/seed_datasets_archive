[<text>
<title>allison's place &lt;alls&gt; sales increase in february</title>
<dateline>    los angeles, march 18 - </dateline>allison's place inc president
marvin schenker said company-owned stores sales for february
increased 82 pct over the same period last year.
    he said comparable store sales in february increased 36
pct.
    the company, which owns and franchises a total of 237
clothing outlets where all articles cost six dlrs, will
increase that figure to seven dlrs starting march 1, schenker
said.
    he said the impact of that boost will start to be felt in
the early part of the company's second quarter and continue
throughout the year.
    schenker said costs of the company's merchandise will not
increase.
 reuter
</text>]