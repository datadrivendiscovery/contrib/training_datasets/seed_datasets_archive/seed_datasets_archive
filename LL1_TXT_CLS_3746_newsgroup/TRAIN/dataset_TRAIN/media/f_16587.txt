[<text>
<title>honeywell inc &lt;hon&gt; 1st qtr oper net</title>
<dateline>    minneapolis, minn., april 13 -
    </dateline>oper shr 96 cts vs 79 cts
    oper net 43.7 mln vs 36.4 mln
    sales 1.48 billion vs 1.15 billion
    note: 1987 sales includes operations of sperry aerospace.
    1986 operating net excludes a charge from discontinued
operations of 10.2 mln dlrs or 22 cts a share.
 reuter
</text>]