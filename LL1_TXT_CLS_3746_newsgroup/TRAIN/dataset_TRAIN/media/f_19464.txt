[<text>
<title>dart group declines comment on dayton hudson&lt;dh&gt;</title>
<dateline>    new york, june 19 - </dateline>dart group corp &lt;darta.o&gt; said it has
no comment on reports the company has been accumulating shares
of dayton hudson corp.
    dayton hudson said in a letter to shareholders it told an
"aggressive buyer" of its stock that it does not want to be
acquired.
    wall street sources have identified the buyer as dart,
which earlier this year was thwarted in a takeover attempt for
the now privately held supermarkets general corp.
 reuter
</text>]