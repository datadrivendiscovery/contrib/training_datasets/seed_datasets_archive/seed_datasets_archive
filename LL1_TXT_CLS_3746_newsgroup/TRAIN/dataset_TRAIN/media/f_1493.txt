[<text>
<title>dow chemical co &lt;dow&gt; unit increases prices</title>
<dateline>    midland, mich., march 4 - </dateline>the dow chemical co said its
engineering thermoplastics department will increase the selling
prices of standard grades of magnum abs resins by three cts per
pound.
    it also announced an increase of five cts per pound for
performance grades of the resins.
    both increases are effective april 1, 1987.

 reuter
</text>]