[<text>
<title>keycorp &lt;key&gt; sells subordinated capital notes</title>
<dateline>    new york, march 18 - </dateline>keycorp is offering 75 mln dlrs of
subordinated capital notes due 1999 with an 8.40 pct coupon and
par pricing, said sole manager first boston corp.
    that is 122 basis points more than the yield of comparable
treasury securities.
    non-callable for life, the issue is rated a-3 by moody's
investors service inc and a-minus by standard and poor's corp.
 reuter
</text>]