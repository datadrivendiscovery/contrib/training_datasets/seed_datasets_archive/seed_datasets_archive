[<text>
<title>franklin resources &lt;ben&gt; says net may double</title>
<dateline>    san mateo, calif., march 13 - </dateline>franklin resources inc said
it believes earnings could double this year as compared to a
year ago when the company reported income of 32 mln dlrs on 143
mln dlrs in revenues.
    franklin resources is a financial services company. its
fiscal year ends september 30.
 reuter
</text>]