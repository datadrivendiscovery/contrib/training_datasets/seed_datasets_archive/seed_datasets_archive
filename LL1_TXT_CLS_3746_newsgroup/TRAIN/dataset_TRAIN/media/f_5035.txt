[<text>
<title>b.c. resources has new 360 mln dlr credit line</title>
<dateline>    vancouver, british columbia, march 13 - </dateline>british columbia
resources investment corp said it successfully concluded
refinancing negotiations with bankers for a new 360 mln dlr
restructured credit facility.
    the credit line will be in place for four years to march
31, 1991, but is extendable up to 10 years under certain
circumstances which were not specified by the company.
    b.c. resources said subsidiaries westar timber and westar
petroleum have settled revised lending agreements, but debt
discussions regarding subsidiary westar mining are continuing.

 reuter
</text>]