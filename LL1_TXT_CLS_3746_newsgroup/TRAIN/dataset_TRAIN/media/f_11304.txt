[<text>
<title>home group &lt;hme&gt; starts trading on nyse</title>
<dateline>    new york, march 31 - </dateline>home group inc said its common stock
starts trading today on the &lt;new york stock exchange&gt;.
    the stock had been traded on the &lt;american stock exchange&gt;.
 reuter
</text>]