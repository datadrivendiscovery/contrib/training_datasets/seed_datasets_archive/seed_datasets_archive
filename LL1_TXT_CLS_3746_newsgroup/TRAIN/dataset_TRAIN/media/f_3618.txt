[<text>
<title>american communications &lt;astve&gt; gets exception</title>
<dateline>    gainesville, fla., march 11 - </dateline>american communications and
television inc said its common stock will continue to be
included in the national association of securities dealers'
nasdaq system due to an exception from filing requirements,
which it failed to meet as of february 14.
    the company said it believes it can meet conditions imposed
by the nasd for the exception, but there can be no assurance
that it will do so.
 reuter
</text>]