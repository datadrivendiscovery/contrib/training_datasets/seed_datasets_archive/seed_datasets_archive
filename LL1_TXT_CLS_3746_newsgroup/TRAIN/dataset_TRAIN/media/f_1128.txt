[<text>
<title>supradur cos inc &lt;supd&gt; year net</title>
<dateline>    rye, n.y., march 3 -
    </dateline>oper shr 1.58 dlrs vs 77 cts
    oper net 1,648,000 vs 817,000
    sales 25.7 mln vs 20.5 mln
    note: net excludes discontinued operations gain 451,000
dlrs vs loss 4,310,000 dlrs.
 reuter
</text>]