[<text>
<title>moody's may lower atlantic city electric &lt;ate&gt;</title>
<dateline>    new york, march 12 - </dateline>moody's investors service said it is
reviewing 648 mln dlrs of securities issued by atlantic city
electric co for a possible downgrade because of an unresponsive
rate order from the new jersey board of public utilities.
    the net effect of the board's recent ruling was to reduce
the company's rates by 15.9 mln dlrs, moody's said.
    ratings under review include the company's first mortgage
bonds and secured pollution control revenue bonds, currently
aa-3.
 reuter
</text>]