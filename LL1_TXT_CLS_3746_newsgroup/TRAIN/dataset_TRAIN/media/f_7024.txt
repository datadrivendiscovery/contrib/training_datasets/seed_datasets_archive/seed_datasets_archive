[<text>
<title>nippon life seeking tie with u.s. securities house</title>
<dateline>    tokyo, march 19 - </dateline>&lt;nippon life insurance co&gt; is pursing a
possible link with an american securities house to expand its
overseas investment portfolio, a company spokesman said.
    but he declined to comment on rumours the company would
take a 10 pct stake in &lt;shearson lehman brothers&gt;, an
investment banking unit of american express co &lt;axp&gt;.
    he said the firm started to sound out several u.s.
investment banks on capital participation about 18 months ago
and was narrowing the number of prospects, but he did not say
if it had set its sights on one firm.
    nippon life, japan's largest life insurer, also plans to
set up a wholly owned investment unit, &lt;nissei international
america&gt;, in new york next month and subsidiaries in canada,
singapore, the cayman islands and jersey this year, he said.
    these moves are in line with its long-term strategy to put
more emphasis on overseas investment management as
opportunities at home are declining while the company's assets
are growing.
    the company is especially attracted by the scale and depth
of u.s. money and credit markets and wants to establish a firm
foothold there, the spokesman added.
 reuter
</text>]