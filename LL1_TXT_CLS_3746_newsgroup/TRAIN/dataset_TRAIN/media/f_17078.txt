[<text>
<title>dealers see moderate dutch central bank yen sales</title>
<dateline>    amsterdam, april 24 - </dateline>the dutch central bank has intervened
in the currency markets today, in apparent concerted action
with other central banks, foreign exchange dealers said.
    they detected selling of the yen for dollars, which some
estimated would run to a moderate 200 mln guilders, comparable
to token dutch intervention reported last week.
    other dealers, however, said they believed today's moderate
intervention had been in guilders against dollar.
    the dealers agreed the intervention was minimal and more a
political gesture than a market moving force.
 reuter
</text>]