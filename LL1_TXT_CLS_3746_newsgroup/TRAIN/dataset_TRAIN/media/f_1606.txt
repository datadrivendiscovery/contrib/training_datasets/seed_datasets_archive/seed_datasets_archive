[<text>
<title>comdata network &lt;cdn&gt; in new acquisition talks</title>
<dateline>    nashville, tenn., march 4 - </dateline>comdata network inc said it is
in active talks with other parties on a possible acquisition or
recapitalization of comdata in an effort to maximize
shareholder values.
    comdata said &lt;rosewood financial inc&gt; together with
&lt;cypress partners lp&gt; and &lt;driftwood ltd&gt; have acquired over
five pct of comdata stock and rosewood intends to acquire over
15 pct of comdata.
    comdata said it has not yet reached a definitive agreement
with &lt;mason best co&gt; for the previously-announced
recapitalization and self-tender offer.
 reuter
</text>]