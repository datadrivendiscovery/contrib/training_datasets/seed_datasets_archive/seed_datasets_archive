[<text>
<title>first women's bank investor group offer expires</title>
<dateline>    new york, march 6 - </dateline>the investor group owning about 42 pct
of the outstanding capital stock of &lt;the first women's bank&gt;
said a cash tender offer for the bank's remaining outstanding
shares at 11 dlrs per share expired on march three.
    the investors said about 132,000 shares, or about 20 pct of
the outstanding, had been tendered.
 reuter
</text>]