[<text>
<title>yankee cos &lt;ynk&gt; seeks debt restructuring</title>
<dateline>    cohasset, mass., june 2 - </dateline>yankee cos inc said it will be 
working with the holders of substantially all of its 12-5/8 pct
senior secured notres due 1996 to develop and overall debt and
asset restructuring program, and as a first step, the
noteholder has agreed to the deferral of the june one interest
payment on the notes until june 30.
    yankee said interest was paid to other noteholders
yesterday.
    the company said it plans to meet and work with all
interest parties, including the holders of its three debt
issues and federal banking authorities, over the next several
weeks to formulate and implement a restructuring plan.
 reuter
</text>]