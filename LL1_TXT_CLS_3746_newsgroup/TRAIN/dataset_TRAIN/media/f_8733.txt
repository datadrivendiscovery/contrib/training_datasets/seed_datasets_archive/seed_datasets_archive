[<text>
<title>mcrae industries &lt;mri-a&gt; increases payout</title>
<dateline>    mt gilead, n.c., march 24 - </dateline>mccrae industries inc said it
raised its preferred dividend on its class a common stock to 12
cts per share from 11 cts per share.
    it said the dividend was payable april 20, 1987, to
shareholders of record april 6.
 reuter
</text>]