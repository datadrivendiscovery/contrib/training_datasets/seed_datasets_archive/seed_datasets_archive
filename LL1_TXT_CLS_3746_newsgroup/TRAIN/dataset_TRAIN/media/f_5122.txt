[<text>
<title>manville &lt;qman&gt; names financial officer</title>
<dateline>    denver, march 13 - </dateline>manville corp said it named john roach
senior vice president and chief financial officer.
    the post of chief financial officer has been vacant since
w.t. stephens was appointed president of the company last
april, manville said.
    roach, 43, was most recently a partner of braxton
associates, a unit of the accounting firm touche ross and co.
he was previously with northrop corp &lt;noc&gt;.
 reuter
</text>]