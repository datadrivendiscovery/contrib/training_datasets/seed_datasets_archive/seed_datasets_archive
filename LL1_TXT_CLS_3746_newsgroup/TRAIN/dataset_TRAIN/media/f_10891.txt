[<text>
<title>datron corp &lt;datr&gt; 4th qtr net</title>
<dateline>    minneapolis, march 30 -
    </dateline>shr 19 cts vs 13 cts
    net 166,000 vs 118,000
    rev 3.2 mln vs 2.5 mln
    year
    shr 34 cts vs 30 cts
    net 303,000 vs 269,000
    rev 10.8 mln vs 10.2 mln
 reuter
</text>]