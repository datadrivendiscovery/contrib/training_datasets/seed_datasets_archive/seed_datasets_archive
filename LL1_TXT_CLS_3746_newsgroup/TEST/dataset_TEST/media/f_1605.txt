[<text>
<title>federal express tentatively gets us/tokyo route</title>
<dateline>    washington, march 4 - </dateline>the department of transportation said
that an administrative law judge had recommended that small
package service between the united states and tokyo be granted
to federal express corp &lt;fdx&gt;.
    a final decision by the department is expected within 90
days.
    in addition, the department said the law judge recommended
that backup service be granted to &lt;orion air inc&gt;. the order,
if finally approved, would be for five years in the case of
federal and one year in the case of orion.
 reuter
</text>]