[<text>
<title>boeing&lt;ba&gt; gets 53 mln dlrs in defense contracts</title>
<dateline>    washington, march 18 - </dateline>boeing military airplane co, a
subsidiary of boeing co, is being awarded two air force
contracts totalling 53 mln dlrs, the defense department said.
    it said the company was being awarded a 49.8 mln dlr
contract for 10 b707 aircraft, components, engineering and data
for a reengining program, adding that the work is expected to
be completed in january 1988.
    the department also said it awarded the company a 3.2 mln
dlr increase to a contract for work modifying two g-22b
aircraft.
 reuter
</text>]