[<text>
<title>imf approves development aid for jamaica</title>
<dateline>    washington, march 3 - </dateline>the international monetary fund,
imf, said it approved 125.9 mln sdr's to assist development in
jamaica.
    over the next 15 months 85 mln sdr's can be drawn under a
standby arrangement in support of the government's economic and
financial program.
    another 40.9 mln is available immediately following an
export shortfall for jamaica in the year ended september 1986
due to reduced income from bauxite, alumina and tourism, the
imf said.
 reuter
</text>]