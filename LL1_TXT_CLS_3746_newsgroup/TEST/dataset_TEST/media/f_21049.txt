[<text>
<title>burnham service corp &lt;bsco.o&gt; 3rd qtr net</title>
<dateline>    columbus, ga., oct 19 -
    </dateline>shr 45 cts vs 36 cts
    net 2,554,000 vs 1,954,000
    revs 44.4 mln vs 32.5 mln
    nine mths
    shr 1.00 dlrs vs 75 cts
    net 5,461,000 vs 3,756,000
    revs 109.5 mln vs 89.9 mln
 reuter
</text>]