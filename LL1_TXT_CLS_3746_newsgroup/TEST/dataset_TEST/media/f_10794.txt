[<text>
<title>skanska to take stake in canadian firm</title>
<dateline>    stockholm, march 30 - </dateline>swedish construction and real estate
company skanska ab &lt;skbs.st.&gt; said it will sell its 49 pct
holding in canadian building firm &lt;canadian foundation company
ltd&gt; to rival &lt;banister continental ltd&gt;.
    a company spokeswoman told reuters skanska will receive
banister shares as payment, giving the swedish group 15 pct of
the stock in the expanded banister firm.
    she said skanska will also be appointing two board members
to the canadian company.
 reuter
</text>]