[<text>
<title>photon technology &lt;phon&gt; finalizes agreement</title>
<dateline>    princeton, n.j., april 7 - </dateline>photon technology international
said it signed a research and development agreement with &lt;m-l
technology ventures l.p.&gt;, a research and development limited
partnership sponsored by merrill lynch capital markets
   the agreement calls for mltv to pay photon more than 3.1 mln
dlrs over a three-year period to develop new electro-optical
technologies with applications in the medical field, the
company said.
    in 1988, mltv may, at its option, have the right to acquire
warrants to purchase, during the next six years, 475,000 shares
of the company's common stock at 6.50 dlrs per share, adjusted
for future earnings, photon said.
 reuter
</text>]