[<text>
<title>bear stearns &lt;bsc&gt; to relocate headquarters</title>
<dateline>    new york, march 9 - </dateline>bear stearns cos inc said it was
relocating its wall street headquarters to a mid-town location
on park ave. and signed a lease with &lt;olympia and york&gt;.
    the firm said it will occupy 537,000 square beginning
january 1988 under a 15 year lease. it will begin to move into
the new offices later this year.
    it said the space will accommodate the increase in
personnel, which has grown 30 pct since the firm went public in
october 1985. it will centralize all of its executive offices
at the new location.
 reuter
</text>]