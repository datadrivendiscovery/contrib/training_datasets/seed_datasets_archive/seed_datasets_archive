[<text>
<title>first bank system &lt;fbs&gt; sells lewiston bank</title>
<dateline>    minneapolis, april 17 - </dateline>first bank system said it has
agreeed to sell its first bank lewiston subsidiary, of
lewiston, mont., to two local bankers for undisclosed terms.
    first bank lewiston has assets of 101.4 mln dlrs at the end
of the first quarter.
 reuter
</text>]