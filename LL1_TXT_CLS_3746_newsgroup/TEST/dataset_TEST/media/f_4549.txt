[<text>
<title>u.s. market loan not that attractive-boschwitz</title>
<dateline>    washington, march 12 - </dateline>a marketing loan for u.s. wheat,
feedgrains and soybeans would do nothing to help the surplus
production situation and would be extremely costly, sen. rudy
boschwitz (r-minn.) said.
    "i think i would not support a marketing loan now," he told
the house agriculture subcommittee on wheat, soybeans and
feedgrains. boschwitz was one of the original supporters of a
marketing loan for cotton and rice, but has since focused
support on decoupling legislation, the boren/boschwitiz bill.
    a market loan for grains and soybeans would encourage more
production, especially in high-yielding areas, would be much
more expensive than the current cotton and rice marketing loans
and not increase exports significantly, he said.
 reuter
</text>]