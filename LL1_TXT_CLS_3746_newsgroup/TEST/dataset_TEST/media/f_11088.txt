[<text>
<title>genova &lt;gnva&gt; to merge into genova products</title>
<dateline>    davison, mich., march 30 - </dateline>genova inc said its shareholders
approved a merger into &lt;genova products inc&gt;, which will be
consumated within a few days. under the agreement, genova said
each of its shareholders will receive 5.375 dlrs per share in
cash.
 reuter
</text>]