[<text>
<title>safety-kleen &lt;sk&gt; to buy mckesson &lt;mck&gt; unit</title>
<dateline>    elgin, ill., march 27 - </dateline>safety-kleen corp said it agreed in
principle to acquire mckesson envirosystems co, a subsidiary of
mckesson corp.
    it said mckesson envirosystems' current annual gross
revenues are about 14 mln dlrs.
    the company collects flammable solvents from its industrial
customers for a fee, after which it analyzes and processes the
solvents before they are burned.
 reuter
</text>]