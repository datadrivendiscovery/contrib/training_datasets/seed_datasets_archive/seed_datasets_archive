[<text>
<title>vtx &lt;vtx&gt; to distribute att &lt;t&gt; products</title>
<dateline>    farmingdale, n.y., march 9 - </dateline>vtx electronics corp said it
signed a distribution agreement with american telephone and
telegraph co under which it will act as an agent for att's
electronic cable and wire, fiber-optic cable and related
products to original equipment manufacturers in the northeast.
    terms of the contract, reached with att's network systems
group, were not disclosed.
 reuter
</text>]