[<text>
<title>water sports network buys debt from ntn</title>
<dateline>    carlsbad, calif., march 20 - </dateline>&lt;water sports network&gt; said it
purchased 750,000 dlrs in convertible debentures from &lt;ntn
communications inc&gt;.
    the companies said they are exploring ways to mutually
market the programs and properties of each company.
 reuter
</text>]