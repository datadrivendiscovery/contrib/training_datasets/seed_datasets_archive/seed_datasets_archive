[<text>
<title>first federal &lt;ffsd.o&gt; to take writeoff</title>
<dateline>    decatur, ala., june 19 - </dateline>first federal savings bank said it
will take a 274,000 dlr or 25 ct per share writeoff of its
secondary reserve with the federal savings and loan insurance
corp in the third quartger ending june 30.
 reuter
</text>]