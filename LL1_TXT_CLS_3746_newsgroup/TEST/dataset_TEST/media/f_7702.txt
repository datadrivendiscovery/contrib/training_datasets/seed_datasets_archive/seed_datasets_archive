[<text>
<title>freeport-mcmoran oil and gas &lt;fmr&gt; payout rises</title>
<dateline>    houston, march 20 -
    </dateline>mthly div 14.248 cts vs 4.912 cts prior
    pay april 10
    record march 31
 reuter
</text>]