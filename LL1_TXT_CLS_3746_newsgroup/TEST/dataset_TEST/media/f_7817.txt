[<text>
<title>group has 5.1 pct of unicorp american &lt;uac&gt;</title>
<dateline>    washington, march 20 - </dateline>two affiliated new york investment
firms and an investment advisor told the securities and
exchange commission they have acquired 555,057 shares of
unicorp american corp, or 5.1 pct of the total outstanding.
    the group, which includes mutual shares corp, said it
bought the stake for 5.3 mln dlrs for investment purposes and
has no intention of seeking control of unicorp american.
 reuter
</text>]