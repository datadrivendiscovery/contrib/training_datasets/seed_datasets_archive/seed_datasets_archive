[<text>
<title>no extension on u.s. dairy herd buyout - lyng</title>
<dateline>    washington, april 2 - </dateline>u.s. agriculture secretary richard
lyng said he would not agree to an extension of the 18-month
whole dairy herd buyout program set to expire later this year.
    speaking at the agriculture department to representatives
of the u.s. national cattlemen's association, lyng said some
dairymen asked the program be extended.
    but he said the reagan administration, which opposed the
whole herd buyout program in the 1985 farm bill, would not
agree to an extension.
    the program begun in early 1986, is to be completed this
summer. u.s. cattlemen bitterly opposed the scheme, complaining
that increased dairy cow slaughter drove cattle prices down
last year.
 reuter
</text>]