[<text>
<title>national fsi inc &lt;nfsi&gt; 4th qtr loss</title>
<dateline>    dallas, march 3 -
    </dateline>shr loss six cts vs profit 19 cts
    net loss 166,000 vs profit 580,000
    revs 3,772,000 vs 5,545,000
    year
    shr loss 13 cts vs profit 52 cts
    net loss 391,000 vs profit 1,425,000
    revs 15.4 mln vs 16.6 mln
    note: 1985 year figures pro forma for purchase accounting
adjustments resulting from march 1985 reeacquisition of company
by its original shareholders before august 1985 initial public
offering. 
 reuter
</text>]