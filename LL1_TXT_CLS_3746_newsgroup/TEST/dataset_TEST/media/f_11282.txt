[<text>
<title>penney &lt;jcp&gt; sets stock split, raises quarterly</title>
<dateline>    new york, march 31 - </dateline>j.c. penney co inc said its board
declared a two-for-one stock split and raised the quarterly
dividend 12 cts per share on a presplit basis to 74 cts.
    both are payable may one to holders of record april 10.
 reuter
</text>]