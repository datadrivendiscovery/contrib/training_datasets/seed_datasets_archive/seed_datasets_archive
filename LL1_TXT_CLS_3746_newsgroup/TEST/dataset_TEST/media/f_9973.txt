[<text>
<title>house of fabrics inc &lt;hf&gt; 4th qtr jan 31 net</title>
<dateline>    sherman oaks, calif., march 26 -
    </dateline>shr 34 cts vs 20 cts
    net 2,253,000 vs 1,332,000
    sales 89.7 mln vs 85.9 mln
    year
    shr 94 cts vs 64 cts
    net 6,191,000 vs 4,257,000
    sales 316.4 mln vs 286.7 mln
    note: prior year net both periods includes 2,100,000 dlr
charge from sale of craft showcase stores.
 reuter
</text>]