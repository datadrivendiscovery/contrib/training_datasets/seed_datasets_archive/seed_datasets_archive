[<text>
<title>moody's upgrades ge &lt;ge&gt; mortgage insurance unit</title>
<dateline>    new york, march 4 - </dateline>moody's investors service inc said it
upgraded to aaa from aa-1 the insurance rating of general
electric mortgage insurance co of ohio, a unit of general
electric co.
    moody's said this reflected the strengthening of terms in a
revised net worth agreement between gemico and its immediate
parent, general electric credit corp, whose long-term unsecured
debt is also a top-flight aaa.
    however, the rating agency said the agreement does not
benefit other gemico affiliates in california, florida and
north carolina.
 reuter
</text>]