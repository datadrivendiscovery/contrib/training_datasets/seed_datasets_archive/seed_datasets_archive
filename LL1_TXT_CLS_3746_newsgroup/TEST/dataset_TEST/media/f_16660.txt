[<text>
<title>lockheed &lt;lk&gt; unit gets 275 mln dlr contract</title>
<dateline>    washington, april 13 - </dateline>lockheed corp's lockheed missiles
and space co is being awarded a 275 mln dlr modification to a
navy contract for development and production of trident ii
missiles, the defense department said.
    it said work on the contract is expected to be completed in
march 1990.

 reuter
</text>]