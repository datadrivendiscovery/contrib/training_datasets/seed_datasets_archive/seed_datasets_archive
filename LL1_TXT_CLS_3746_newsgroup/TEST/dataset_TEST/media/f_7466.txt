[<text>
<title>national bancorp inc &lt;nibca&gt; year end loss</title>
<dateline>    hartford, conn., march 19 -
    </dateline>oper shr loss 1.02 dlr
    oper loss 631,000 dlrs
    note: prior year results are not available as company
completed its public offering in april 1986.
 reuter
</text>]