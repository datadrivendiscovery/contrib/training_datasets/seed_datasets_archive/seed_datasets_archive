[<text>
<title>monier says britain's redland may bid for it</title>
<dateline>    sydney, april 8 - </dateline>diversified building materials group
monier ltd &lt;mnra.s&gt; said talks are taking place which may lead
to britain's redland plc &lt;rdld.l&gt; making an offer for the
monier shares it does not already hold, chairman bill locke
said.
    redland already holds about 49 pct of monier's 156.28 mln
issued shares, he said in a brief notice to the australian
stock exchange.
    locke said shareholders would be advised as soon as the
discussions progressed and recommended that they keep their
shares.
    monier shares were trading at a 1987 high of 3.10 dlrs
today, up from the previous peak of 2.80 at yesterday's close,
and well above the 1987 low of 2.18 dlrs.
    monier is the largest concrete roof tile manufacturer in
australia, the u.s. and new zealand and the world's largest
marketer of fly ash, according to its annual report for 1985/86
ended june 30.
    it recently reported first-half 1986/87 net fell to 15.02
mln dlrs from 17.09 mln a year earlier due to the australian
housing downturn, although foreign earnings rose.
 reuter
</text>]