[<text>
<title>gte &lt;gte&gt; comments on justice dep't proposals</title>
<dateline>    washington, march 13 - </dateline>gte corp said it supports
recommendations by the department of justice to allow the
regional bell companies to enter some new businesses but that
it rejects the government's proposal to let the companies offer
long distance services.
    "as a vigorous competitor in many telecommunications
businesses, gte supports efforts by policy makers to preserve
and promote fair opportunities for competition," gte said in
comments filed with the u.s. district court, which oversees the
regulation of the bell companies.
    but the stamford, conn.-based company added that entry by
the regional bell companies into the long distnce market "would
set back the progress toward a truly competitive interexchange
environment that has occurred."
    gte owns the largest independent telephone system in the
country and manufacturers telecommunications equipment through
joint ventures with &lt;fujitsu&gt; of japan and &lt;siemens ag&gt; of west
germany.
    gte also holds a 50 pct stake in &lt;us sprint communications
co&gt;, the third-largest long distance carrier in the country.
 reuter
</text>]