[<text>
<title>security pacific corp &lt;spc&gt; qtly dividend</title>
<dateline>    los angeles, oct 20 -
    </dateline>shr 45 cts vs 45 cts prior qtr
    pay november 20
    record november three
 reuter
</text>]