[<text>
<title>first chicago &lt;fnb&gt; raises prime rate</title>
<dateline>    chicago, april 2 - </dateline>first chicago corp said its first
national bank of chicago raised its prime rate to 7-3/4 pct
from 7-1/2 pct, effective immediately.
 reuter
</text>]