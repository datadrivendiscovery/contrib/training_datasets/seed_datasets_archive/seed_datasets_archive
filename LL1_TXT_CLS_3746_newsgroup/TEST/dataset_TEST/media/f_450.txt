[<text>
<title>canada industrial prices up 0.2 pct in month</title>
<dateline>    ottawa, march 2 - </dateline>canada's industrial product price index
rose 0.2 pct in january after falling 0.2 pct in each of the
two previous months, statistics canada said.
    the rise was led by price gains for papers, pharmaceuticals
and petroleum and coal products. price declines were recorded
for meat products, lumber and motor vehicles.
    on a year over year basis, the federal agency said the
index fell 0.9 pct in january, the largest yearly decline on
record.
 reuter
</text>]