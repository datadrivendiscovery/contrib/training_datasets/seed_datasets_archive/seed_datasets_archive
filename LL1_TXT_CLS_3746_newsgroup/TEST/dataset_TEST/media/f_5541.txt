[<text>
<title>bp &lt;bp&gt; unit sees mine proceeding</title>
<dateline>    new york, march 16 - </dateline>british petroleum co plc said based on
a feasibility report from &lt;ridgeway mining co&gt;, its joint
venture ridgeway project in south carolina could start
commercial gold production by mid-1988.
    the company said the mine would produce at an approximate
rate of 158,000 ounces of gold per year over the first four
full years of operation from 1989 through 1992 and at an
average of 133,000 ounces a year over the full projected
11-year life of the mine.
    bp's partner in the venture is galactic resources ltd
&lt;galcf&gt; of toronto.
    the company said subject to receipt of all statutory
permits, finalization of financing arrangements and management
and joint venture review, construction of a 15,000 short ton
per day processing facility can start.  capital costs to bring
the mine into production are estimated at 76 mln dlrs.
 reuter
</text>]