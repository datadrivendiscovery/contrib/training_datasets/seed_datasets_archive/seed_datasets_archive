[<text>
<title>pantera's  &lt;pant&gt; acquires 10 restaurants</title>
<dateline>    st. louis, april 3 - </dateline>pantera's corp said it closed on its
agreement to acquire ten pizza restaurant locations in
southeastern colorado.
    it said the purchase price was 1,250,000 dlrs, which was
paid by cash and stock.
 reuter
</text>]