[<text>
<title>emerson &lt;emr&gt; sells new zealand dollar notes</title>
<dateline>    new york, march 11 - </dateline>emerson electric co is offering in the
u.s. debt market 100 mln new zealand dlrs of notes due 1989
with an 18.55 pct coupon and par pricing, said lead underwriter
prudential-bache securities inc.
    that is 170 basis points below comparable new zealand
rates.
    non-callable to maturity, the issue is rated a top-flight
aaa by both moody's investors service inc and standard and
poor's corp.
 reuter
</text>]