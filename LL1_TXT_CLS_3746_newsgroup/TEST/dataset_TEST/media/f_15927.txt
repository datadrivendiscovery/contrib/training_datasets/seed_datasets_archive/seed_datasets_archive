[<text>
<title>export bonus wheat flour to iraq --usda</title>
<dateline>    washington, april 9 - </dateline>the commodity credit corporation
(ccc) accepted a bid for an export bonus to cover a sale of
12,500 tonnes of u.s. wheat flour to iraq, the u.s. agriculture
department said.
    the department said the bonus awarded was 105.82 dlrs per
tonne and the wheat flour is for shipment july 1-10, 1987.
    the bonus was awarded to the pillsbury company and will be
paid in the form of commodities from ccc stocks.
    an additional 150,000 tonnes of wheat flour is still
available to iraq under the export enhancement program
initiative announced january 7, 1987, the department said.
 reuter
</text>]