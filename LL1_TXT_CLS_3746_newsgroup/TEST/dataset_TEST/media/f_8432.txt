[<text>
<title>pacificare &lt;phsy&gt; in talks to acquire hmo</title>
<dateline>    cypress, calif., march 23 - </dateline>pacificare health systems inc
said it is in negotiations to acquire capital health care, a
40,000 member health maintenance organization servicing salem
and corvallis, ore.
    the company said it will not disclose terms or other
details of the acquisition until negotiations are completed.

 reuter
</text>]