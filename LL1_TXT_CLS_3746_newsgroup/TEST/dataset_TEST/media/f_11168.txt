[<text>
<title>woodside petroleum ltd &lt;wpla.s&gt; 1986 year</title>
<dateline>    melbourne, march 31 -
    </dateline>shr nil vs same
    final and yr div nil vs same
    pre-tax, pre-minorities loss 3.53 mln dlrs vs profit 17.40
mln.
    net attributable loss 17.14 mln dlrs vs loss 8.73 mln
    sales 220.84 mln vs 173.50 mln
    other income 17.77 mln vs 12.02 mln
    shrs 666.67 mln vs same.
    note - attributable net loss is after tax 10.04 mln dlrs vs
18.59 mln, interest 82.36 mln vs 65.94 mln, depreciation 64.77
mln vs 35.74 mln and minorities 3.57 mln vs 7.55 mln but before
net extraordinary loss 1.22 mln vs loss 3.91 mln.
 reuter
</text>]