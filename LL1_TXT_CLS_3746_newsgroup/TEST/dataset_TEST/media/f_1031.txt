[<text>
<title>yankee &lt;ynk&gt; swaps stock for debentures</title>
<dateline>    cohasset, mass., march 3 - </dateline>yankee cos inc said it has
acquired 3,916,000 dlrs of 7-1/2 pct convertible subordinated
debentures due may 15, 1998 of its yfc international finance nv
affiliate for 501,807 common shares from an institutional
investor.
 reuter
</text>]