[<text>
<title>mds health &lt;mhga.to&gt; gets order</title>
<dateline>    toronto, oct 20 - </dateline>mds health group ltd said that mass
spectrometry equipment developed and made by its sciex division
will be used in british-made contraband detection systems being
purchased by the japanese government.
    the company said the japanese customs bureau placed an
initial 21 mln dlr order.
    the equipment is able to detect illegal drugs in cargo, the
company said. the systems are made by british aerospace
&lt;bael.l&gt;.
 reuter
</text>]