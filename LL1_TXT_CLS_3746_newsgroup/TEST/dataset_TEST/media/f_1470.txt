[<text>
<title>eastern &lt;eml&gt; acquires emhart &lt;emh&gt; product line</title>
<dateline>    naugatuk, conn., march 4 - </dateline>eastern co said it purchased the
tooling, equipment and inventory used to produce the corbin
cabinet lock product line from emhart corp for undisclosed
terms.
 reuter
</text>]