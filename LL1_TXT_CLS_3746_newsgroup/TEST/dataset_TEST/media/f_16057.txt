[<text>
<title>data architects &lt;drch&gt; to make offering</title>
<dateline>    waltham, mass, april 9 - </dateline>data architects inc said it
intends to make a public offering of 850,000 shares of its
common stock, of which 672,000 shares will be sold by the
company and 178,000 by certain shareholders.
    proceeds will be used for general corporate purposes.
 reuter
</text>]