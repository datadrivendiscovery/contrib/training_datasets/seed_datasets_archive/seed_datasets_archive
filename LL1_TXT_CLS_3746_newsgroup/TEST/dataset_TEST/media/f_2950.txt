[<text>
<title>twa &lt;twa&gt; to refile with u.s. agency</title>
<dateline>    new york, march 6 - </dateline>transworld airlines inc plans to refile
an application monday with the department of transportation for
approval to acquire usair group &lt;u&gt;.
    the dot late today dismissed twa's application to acquire
usair.
    "when the dot opens for business on monday morning, we will
be refiling a perfected and completed section 408 application,"
said twa general counsel mark buckstein.
    buckstein said the dot ruled the application, which was
filed wednesday, was incomplete. he said he did not yet know
why the agency objected.
 reuter
</text>]