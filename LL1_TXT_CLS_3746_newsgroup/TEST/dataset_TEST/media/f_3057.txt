[<text>
<title>hog and cattle slaughter guesstimates</title>
<dateline>    chicago, march 9 - </dateline>chicago mercantile exchange floor
traders and commission house representatives are guesstimating
today's hog slaughter at about 285,000 to 300,000 head versus
292,000 week ago and 309,000 a year ago.
    cattle slaughter is guesstimated at about 125,000 to
131,000 head versus 129,000 week ago and 119,000 a year ago.
 reuter
</text>]