[<text>
<title>&lt;clayton and dubilier inc&gt; sells burpee</title>
<dateline>    new york, march 30 - </dateline>privately-held clayton and dubilier
inc said it has sold w. atlee burpee co to a new company formed
by wicks capital corp, bankers trust new york corp &lt;bt&gt; and
burpee management for undisclosed terms.
    the garden supply company was acquired from itt corp &lt;itt&gt;
in december along with o.m. scott and sons co.
 reuter
</text>]