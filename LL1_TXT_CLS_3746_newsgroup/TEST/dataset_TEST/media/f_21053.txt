[<text>
<title>erc international inc &lt;erc&gt; 3rd qtr net</title>
<dateline>    fairfax, va., oct 19 -
    </dateline>shr 31 cts vs nine cts
    net 1,345,000 vs 368,000
    revs 31.9 mln vs 26.4 mln
    nine mths
    shr 91 cts vs 40 cts
    net 3,890,000 vs 3,556,000
    revs 89.3 mln vs 71.7 mln
    note: 1986 qtr and nine mths include loss 831,000 dlrs, or
19 cts per share, and loss 1,872,000 dlrs, or 44 cts per share,
respectively, from discontinued operations.
 reuter
</text>]