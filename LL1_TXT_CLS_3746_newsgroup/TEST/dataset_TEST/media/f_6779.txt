[<text>
<title>marshall industries &lt;mi&gt; 3rd qtr feb 28 net</title>
<dateline>    el monte, calif., march 18 -
    </dateline>shr 14 cts vs 12 cts
    net 1,017,000 vs 877,000
    sales 68.1 mln vs 61.2 mln
    nine mths
    shr 40 cts vs 17 cts
    net 2,986,000 vs 1,215,000
    sales 205.3 mln vs 174 mln
    note: per share figure reflects two-for-one stock split of
july 1986.
 reuter
</text>]