[<text>
<title>general automation &lt;gena&gt; sets joint venture</title>
<dateline>    anaheim, calif., april 8 - </dateline>general automation inc said it
signed a joint venture with madrid, spain-based ingenieria de
sistemas electronics s.a.
    the pact establishes general automation iberica s.a. as a
local manufacturer and master distributor of general
automation's zebra multi-user business systems in spain and
portugal.
    general automation iberica will be equally financed by
general automation and ingenieria.
 reuter
</text>]