[<text>
<title>texas air &lt;tex&gt; unit completes system sale</title>
<dateline>    miami, april 13 - </dateline>texas air corp's eastern airlines said it
completed its previously announced plan to sell its travel
agency automation system, systemone direct access inc, and its
computer and communications support unit, eal automation
systems inc, to systemone corp, a wholly-owned subsidiary of
texas air corp.
 reuter
</text>]