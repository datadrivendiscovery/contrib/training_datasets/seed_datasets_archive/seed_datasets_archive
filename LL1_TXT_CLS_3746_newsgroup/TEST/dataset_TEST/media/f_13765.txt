[<text>
<title>3com &lt;coms&gt; supports new &lt;ibm&gt; computers</title>
<dateline>    santa clara, calif., april 7 - </dateline>3com corp said it supports
international business machines corp's personal system/2 series
of computers, and will provide network operating software and
interfaces.
    the software and interfaces will allow the new machines to
be integrated into existing 3system workgroup systems, 3com
said.
    3com said it is modifying its 3+ network software to work
under dos 3.3, ensuring campatibility with current personal
snd ibm's new token ring adaps
micro channel architecture.
 reuter
</text>]