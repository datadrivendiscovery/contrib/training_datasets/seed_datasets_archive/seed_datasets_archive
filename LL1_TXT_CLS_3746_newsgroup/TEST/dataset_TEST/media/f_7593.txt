[<text>
<title>klm expands talks with british and commonwealth</title>
<dateline>    amsterdam, march 20 - </dateline>klm royal dutch airways &lt;klm.as&gt; said
it agreed to take full control of a partially owned dutch-based
parcel delivery service and will offer a minority stake in it
to british and commonwealth shipping plc &lt;bcom.l&gt;
    klm, seeking to strengthen its market position in the fast
growing door-to-door delivery market, said it agreed with dutch
retailer vendex international &lt;venn.as&gt; to take over vendex's
50-pct in their jointly-owned courier, &lt;xp system vof&gt;.
    ownership of xp will now be brought into the talks started
by klm last week with british and commonwealth for a one-third
stake in the latter's &lt;iml air services group ltd&gt; courier.
    when announcing the negotiations with british and
commonwealth last week, klm said buying a minority stake in iml
could involve a convertible loan issue.
    a klm spokeswoman said the dutch flag carrier would now
offer a minority stake in xp to british and commonwealth in the
negotiations on iml, but declined to elaborate on financial
aspects of the talks.
    she said klm would like the two courier services to
cooperate in future and did not exclude a future merger between
them to combine iml's strong world-wide network with xp's
mainly european activities.
    xp system is based in the southern dutch airport of
maastricht and has an annual turnover of 100 mln guilders.
    klm, which is also negotiating with british and
commonwealth for a 15-pct stake in the latter's regional
airline &lt;air u.k. ltd&gt;, says door-to-door delivery courier
services are seeing substantially faster growth than
traditional cargo activities.
 reuter
</text>]