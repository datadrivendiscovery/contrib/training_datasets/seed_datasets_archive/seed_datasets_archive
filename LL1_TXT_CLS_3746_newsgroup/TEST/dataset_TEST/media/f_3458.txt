[<text>
<title>japan crushers start april u.s. soybean buying</title>
<dateline>    tokyo, march 11 - </dateline>japanese crushers, starting to buy u.s.
soybeans for april shipment, have recently made purchases of
some 48,000 tonnes, trade sources said.
    the sources said they could not estimate the total volume
to be purchased for april shipping because japan's crushing
program for april and june is unclear.
    they had predicted earlier that crushers' april shipment
u.s. bean purchases would drop to 260,000 to 270,000 from the
monthly average of 300,000 to 330,000 tonnes due to low soybean
meal prices.
 reuter
</text>]