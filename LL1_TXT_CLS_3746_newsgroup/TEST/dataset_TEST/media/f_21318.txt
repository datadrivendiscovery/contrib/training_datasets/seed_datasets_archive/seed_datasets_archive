[<text>
<title>transamerica &lt;ta&gt; unit buys reinsurance unit</title>
<dateline>    los angeles, oct 19 - </dateline>transamerica insurance group, the
main property-liability insurance operation of transamerica
corp, said it signed a definitive agreement to acquire a
newly-formed insurer, commerical risk underwriters insurance
co, from &lt;clarendon group ltd&gt;.
    transamerica said the unit, which will be renamed
transamerica reinsurance co, will initially be capitalized at
about 185 mln dlrs.
    transamerica said the acquisition represents its first move
into specialty treaty reinsurance.
    the company said about 28 members of clarendon will join
transamerica as part of the acquisition, which is expected to
close in november and is subject to various regulatory
approvals.
 reuter
</text>]