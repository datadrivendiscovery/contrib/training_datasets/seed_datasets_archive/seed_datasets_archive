[<text>
<title>f.w. woolworth co &lt;z&gt; hikes dividend</title>
<dateline>    new york, april 8 -
    </dateline>qtly div 33 cts vs 28 cts prior
    pay june 1
    record may 1
 reuter
</text>]