[<text>
<title>(corrected)-crazy eddie &lt;crzy&gt; executive off board</title>
<dateline>    edison, n.j., april 13 - </dateline>crazy eddie inc said executive
vice president sam antar, who turns 66 soon, has resigned from
its board and has been replaced by william h. saltzman, vice
president and general counsel of sun/dic acquisition corp.
    the company said antar remains an executive vice president.
    corrects to delete references to antar being chief
financial officer and a member of the office of the president.
 reuter
</text>]