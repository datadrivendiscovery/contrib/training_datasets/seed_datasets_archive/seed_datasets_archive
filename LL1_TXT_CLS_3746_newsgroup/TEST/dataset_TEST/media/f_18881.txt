[<text>
<title>triton group &lt;trro.o&gt; president resigns</title>
<dateline>    la jolla, calif., june 18 - </dateline>triton group ltd said john
stiska has resigned as its president. he will be replaced by
triton chairman charles scott.
    stiska will return to private law practice and will
continue to serve as special counsel to triton and to intermark
inc &lt;imi&gt;.
    intermark, which owns 41 pct of triton's common stock, took
a controlling interest in the company in march, 1986.
 reuter
</text>]