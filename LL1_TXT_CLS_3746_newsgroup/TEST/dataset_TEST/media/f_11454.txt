[<text>
<title>gm &lt;gm&gt; canada to hold news conference today</title>
<dateline>    montreal, march 31 - </dateline>general motors of canada ltd said it
will hold a news conference at its ste-therese, quebec assembly
plant at 1315 est today.
    canadian published reports, quoting government sources, say
the company will reveal details of a 220 mln canadian dlr
interest-free loan offered by ottawa and the quebec provincial
government to modernize the aging plant.
    quebec premier robert bourassa and federal industry
minister michel cote will also be at the plant.
 reuter
</text>]