[<text>
<title>measurex corp &lt;mx&gt; 1st qtr net</title>
<dateline>    cupertino, calif., march 18 -
    </dateline>shr 34 cts vs 27 cts
    qtrly div six cts vs 4.5 cts prior
    net 6,448,000 vs 4,969,000
    revs 51.1 mln vs 44.7 mln
    avg shrs 19.1 mln vs 18.7 mln
    note: pay for dividend was march 11 to shareholders of
record on feb 20.
 reuter
</text>]