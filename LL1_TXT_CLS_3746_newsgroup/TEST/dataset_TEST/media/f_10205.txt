[<text>
<title>mobil &lt;mob&gt; postpones highrise development</title>
<dateline>    chicago, march 26 - </dateline>mobil corp said its plans to erect a
72-story office building in downtown chicago at the site once
occupied by a montgomery ward department store have been
postponed.
    a spokesman for dearborn land co, a unit of mobil land
development corp, said the current office market in the area is
overbuilt.
 reuter
</text>]