[<text>
<title>mellon bank &lt;mel&gt; sells five-year deposit notes</title>
<dateline>    new york, march 6 - </dateline>mellon bank corp is raising 150 mln
dlrs via an offering of deposit notes due 1992 with a 7.45 pct
coupon and par pricing, said sole manager first boston corp.
    that is 73 basis points more than the yield of comparable
treasury securities.
    the issue has a one-time call after three years, first
boston detailed. if mellon does not redeem the notes at that
time, they will be non-callable to maturity.
    moody's investors service inc rates the debt aa-2 and
standard and poor's corp rates it aa.
 reuter
</text>]