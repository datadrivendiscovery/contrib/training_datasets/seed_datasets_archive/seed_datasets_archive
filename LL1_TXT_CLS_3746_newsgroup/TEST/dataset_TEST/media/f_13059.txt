[<text>
<title>us west &lt;usw&gt; hikes dividend</title>
<dateline>    denver, april 3 -
    </dateline>qtly div 82 cts vs 76 cts prior
    pay may 1
    record april 16
 reuter
</text>]