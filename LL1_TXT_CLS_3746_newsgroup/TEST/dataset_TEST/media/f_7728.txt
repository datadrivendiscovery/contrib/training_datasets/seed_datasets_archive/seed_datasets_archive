[<text>
<title>&lt;guardian trustco inc&gt; 4th qtr net</title>
<dateline>    montreal, march 20 -
    </dateline>shr 15 cts vs nine cts
    net 528,000 vs 374,000
    revs not given
    year
    shr 1.25 dlrs vs 42 cts
    net 2,853,000 vs 1,579,000
    revs 55.3 mln vs 46.8 mln
    note: shr after preferred dividends
 reuter
</text>]