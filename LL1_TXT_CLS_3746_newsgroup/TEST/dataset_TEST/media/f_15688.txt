[<text>
<title>zayre corp &lt;zy&gt; raises quarterly</title>
<dateline>    framingham, mass., april 9 -
    </dateline>qtly div 10 cts vs eight cts prior
    pay june four
    record may 14
 reuter
</text>]