[<text>
<title>small quantity of u.k. wheat sold to home market</title>
<dateline>    london, april 21 - </dateline>a total of 2,769 tonnes of british
intervention feed wheat, out of an available 57,300 tonnes, was
sold at today's tender for the home market, the home grown
cereals authority, hgca, said.
    price details were not reported.
 reuter
</text>]