[<text>
<title>final trust for thrift institutions payout set</title>
<dateline>    boston, march 2 - </dateline>&lt;massachusetts financial services co&gt;
said it has set the final income and capital gain distributions
for &lt;trust for thrift institutions high yield series&gt; of 1.069
dlrs and 7.645 dlrs, respectively, payable today.
 reuter
</text>]