[<text>
<title>telecredit inc &lt;tcrd&gt; 3rd qtr jan 31 net</title>
<dateline>    los angeles, march 2 -
    </dateline>shr 32 cts vs 22 cts
    net 3,454,000 vs 2,224,000
    revs 33.2 mln vs 28.1 mln
    nine mths
    shr 64 cts vs 38 cts
    net 6,935,000 vs 3,877,000
    revs 86.8 mln vs 70.9 mln
 reuter
</text>]