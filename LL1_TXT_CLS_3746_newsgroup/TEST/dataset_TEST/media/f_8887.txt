[<text>
<title>wedgestone realty &lt;wdg&gt; files for offering</title>
<dateline>    newton, mass., march 23 - </dateline>wedgestone realty investors trust
said it has filed for an offering of 1,250,000 common shares
through underwriters led by ladenburg, thalmann and co and
moseley holding corp &lt;mose&gt;.
    it said the offering is expected to be made in mid-april.
 reuter
</text>]