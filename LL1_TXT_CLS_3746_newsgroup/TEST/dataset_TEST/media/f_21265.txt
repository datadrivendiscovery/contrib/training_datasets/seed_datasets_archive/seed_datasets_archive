[<text>
<title>universal furniture ltd &lt;ufurf.o&gt; 3rd qtr net</title>
<dateline>    high point, n.c., oct 19 -
    </dateline>shr 34 cts vs 26 cts
    net 6,150,000 vs 4,743,000
    revs 61.4 mln vs 49.5 mln
    nine months
    shr 89 cts vs 70 cts
    net 16 mln vs 11.8 mln
    revs 170 mln vs 137.5 mln
    note: all share and per share data have been adjusted to
reflect 100 pct stock dividend distrition on april 24, 1987 and
the public offier of two mln shares ofthe company on june 4,
1986.
 reuter
</text>]