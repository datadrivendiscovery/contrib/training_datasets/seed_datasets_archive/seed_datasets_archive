[<text>
<title>nyse to review program trading</title>
<dateline>    new york, march 17 - </dateline>the new york stock exchange said it
will review the long-term effects on securities markets of
computer-driven trading techniques known as program trading.
    the nyse said, "the study will review major new trading
techniques involving programmed portfolio hedging and index
arbitrage for their potential benefits and risks to the
financial system.
    "it will also explore the regulatory implications of these
trading techniques and whether their increased use could
possibly lead to market abuse."
    the exchange said a final report is expected before the end
of 1987. it said program trading is becoming increasingly
important as a market factor.
 reuter
</text>]