[<text>
<title>mcdonnell douglas &lt;md&gt;, siemens sign memorandum</title>
<dateline>    new york, june 18 - </dateline>mcdonnell douglas corp and siemens ag
&lt;sieg.f&gt; said they signed a memorandum of understanding dealing
with the ah-64 apache helicopter.
    they said the memorandum deals with depot maintenance of
the u.s. army's helicopters based in west germany and a joint
study of the potential development and production of the
helicopter for the west german army.
 reuter
</text>]