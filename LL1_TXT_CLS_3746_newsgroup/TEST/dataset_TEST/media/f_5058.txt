[<text>
<title>american city &lt;ambj&gt; sets initial preferred div</title>
<dateline>    kansas city, mo. - march 13 - </dateline>american city business
journals inc said it declared an initial dividend of 15.4 cts a
share on its recent issue of 1.6 mln shares of convertible
exchangeable preferred stock.
    the dividend is payable march 31 to shareholders of record
march 20, american city said, adding that future dividends will
be paid on a quarterly basis.
    the preferred stock was issued on february 23.
 reuter
</text>]