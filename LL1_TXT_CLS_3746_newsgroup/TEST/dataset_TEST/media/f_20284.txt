[<text>
<title>arkansas best corp &lt;abz&gt; 3rd qtr net</title>
<dateline>    fort smith, ark., oct 20 -
    </dateline>shr diluted 31 cts vs 60 cts
    net 3,276,776 vs 6,846,367
    revs 187.7 mln vs 181.3 mln
    nine mths
    shr diluted 52 cts vs 1.51 dlrs
    net 5,301,876 vs 16.4 mln
    revs 535.7 mln vs 513.0 mln
 reuter
</text>]