[<text>
<title>commodore &lt;cbu&gt; in pact for computer graphics</title>
<dateline>    west chester, pa., march 26 - </dateline>commodore international corp
said it entered into two agreements to provide amiga computer
graphics technology to the coin-operated amusement industry.
    the agreements are with london-based &lt;mastertronic ltd&gt; and
&lt;grand products&gt; of elk grove, ill., and they join a similar
previously-announced agreement with bally manufacturing corp
&lt;bly&gt;, the company said.
    under the terms of the agreement, commodore said it will
supply the companies with amiga printed circuit board and
commodore technical knowhow and will obtain software licensing
rights to video games developed for the amiga hardware.
</text>]