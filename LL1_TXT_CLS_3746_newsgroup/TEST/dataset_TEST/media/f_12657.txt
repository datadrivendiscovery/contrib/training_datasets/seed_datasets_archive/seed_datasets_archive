[<text>
<title>first interstate &lt;i&gt; raises prime rate</title>
<dateline>    los angeles, april 2 - </dateline>the first interstate bank of
california subsidiary of first interstate bancorp said it is
raising its prime lending rate to 7-3/4 pct from 7-1/2 pct,
effective immediately.
 reuter
</text>]