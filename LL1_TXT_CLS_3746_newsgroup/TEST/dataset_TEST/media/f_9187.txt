[<text>
<title>babcock international plc &lt;babk.l&gt; year 1986</title>
<dateline>    london, march 25 -
    </dateline>div 4.7p making 8.7, an increase of 13.9 pct
    shr 16.3p vs 17.9p adjusted
    pretax profit 37.09 mln stg vs 34.55 mln
    net 22.12 mln vs 24.13 mln
    interest payable 8.10 mln vs 5.35 mln
    share of associated co's 6.88 mln vs 5.42 mln
    turnover 1.22 billion stg vs 1.10 billion
 reuter
</text>]