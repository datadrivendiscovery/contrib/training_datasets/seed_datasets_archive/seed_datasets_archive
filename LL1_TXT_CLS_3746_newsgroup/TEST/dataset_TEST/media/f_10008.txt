[<text>
<title>questech inc &lt;qtec&gt; year net</title>
<dateline>    mclean, va., march 26 - 
    </dateline>shr loss nil vs profit 19 cts
    net loss 3,175 vs profit 284,945
    revs 13.6 mln vs 10.6 mln
    year
    shr profit 13 cts vs profit 56 cts
    net profit 195,202 vs profit 857,006
    revs 47.5 mln vs 42.9 mln
    note: current year net includes charge against discontinued
operations of 1,060,848 dlrs.
 reuter
</text>]