[<text>
<title>nissan affiliate to acquire u.s. autoparts maker</title>
<dateline>    tokyo, march 25 - </dateline>&lt;kokusan kinzoku kogyo co ltd&gt; (kkk), a
japanese autoparts maker owned 25 pct by nissan motor co ltd
&lt;nsan.t&gt;, has exchanged a memorandum to acquire over 50 pct of
u.s. autoparts firm &lt;master-cast co&gt; to avoid losses on u.s.
sales caused by the yen's rise against the dollar, a kkk
spokesman said.
    the final agreement should be signed this year when kkk
forms the new company &lt;alfa k technology&gt;, he said.
    the new firm should supply all the u.s. major car makers,
including ford motor co &lt;f&gt;, general motor corp &lt;gm&gt; and
chrysler corp &lt;c&gt;, he said.
 reuter
</text>]