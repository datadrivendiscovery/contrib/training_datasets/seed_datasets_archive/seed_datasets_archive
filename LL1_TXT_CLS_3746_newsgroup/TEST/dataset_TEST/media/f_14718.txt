[<text>
<title>egypt said to cancel pl480 soft wheat tender</title>
<dateline>    kansas city, april 7 - </dateline>egypt has cancelled its export bonus
tender for 200,000 tonnes of soft red winter wheat for may-june
shipment after failing to bid a price acceptable to usda,
private export sources said.
 reuter
</text>]