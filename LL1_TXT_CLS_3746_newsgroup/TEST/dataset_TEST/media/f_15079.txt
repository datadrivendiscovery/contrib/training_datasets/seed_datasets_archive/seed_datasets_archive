[<text>
<title>raytheon co &lt;rtn&gt; 1st qtr net</title>
<dateline>    lexington, mass., april 8 -
    </dateline>shr 1.37 dlrs vs 1.19 dlrs
    net 101.8 mln vs 92.3 mln
    revs 1.750 billion vs 1.725 billion
    avg shrs 74.2 mln vs 77.8 mln
 reuter
</text>]