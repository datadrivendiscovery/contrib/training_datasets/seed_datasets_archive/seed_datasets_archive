[<text>
<title>u.s. sets 9.75 billion dlr year-bill auction</title>
<dateline>    washington, april 3 - </dateline>the u.s. treasury said it will sell
9.75 billion dlrs of one-year bills at auction.
    the april 9 sale will pay down about 25 mln dlrs as 9.763
billion dlrs in bills are maturing.
    the bills will be issued april 16 in minimum amounts of
10,000 dlrs and mature april 14, 1988.
 reuter
</text>]