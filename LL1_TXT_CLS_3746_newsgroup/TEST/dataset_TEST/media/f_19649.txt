[<text>
<title>martin processing &lt;mpi&gt; accepts courtaulds bid</title>
<dateline>    martinsville, va., june 29 - </dateline>martin processing inc said its
board approved an agreement to be acquired by courtaulds plc
&lt;cou.l&gt; for 20 dlrs a share.
    the company said the transaction is valued at more than 99
mln dlrs, based on its 4,963,620 shares of stock outstanding.
    it said courtaulds has privately agreed to purchase a
majority of martin's common from trusts established by julius
hermes for 20 dlrs a share. it said courtaulds intends to
conduct a tender offer for all the martin stock it does not
own, beginning as soon as necessary documents are prepared.
 reuter
</text>]