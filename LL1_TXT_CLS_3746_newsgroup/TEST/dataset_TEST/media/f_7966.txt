[<text>
<title>wendy's &lt;wen&gt; declnes comment on market rumors</title>
<dateline>    chicago, march 20 - </dateline>wendy's international inc declined to
comment on vague rumors by traders that it might be a takeover
target.
    wendy's is currently trading up one at 11-3/4 on turnover
of more than 1.6 mln shares.
    a wendy's spokesman said it was corporate policy not to
comment on market rumors.
    he further declined to attribute active trading in wendy's
stock to a published report which stated wall street
professionals believe that wendy's was possibly being studied
by coca cola co (ko) with view to a possible acquisition.
 reuter
</text>]