[<text>
<title>strata corp &lt;stata&gt; year dec 31 loss</title>
<dateline>    columbus, ohio, april 8 -
    </dateline>shr loss 1.11 dlrs vs loss 1.53 dlrs
    net loss 7.1 mln vs loss 8.8 mln
    revs 3.1 mln vs eight mln
 reuter
</text>]