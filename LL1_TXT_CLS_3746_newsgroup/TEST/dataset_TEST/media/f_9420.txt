[<text>
<title>j.a.m. &lt;jamy&gt; wins 1.5 mln dlr contract</title>
<dateline>    washington, march 25 - </dateline>j.a.m. inc said it won a 1.5 mln dlr
contract to produce and develop a series of courses on computer
and data processing systems for use by colleges and 
businesses.
    j.a.m. executive vice president anthony busch said he could
not disclose the name of the firm that awarded j.a.m. the
contract because of a confidentiality agreement. but he
described it as a leading supplier of educational materials.
    j.a.m., headquartered in rochester, n.y., develops 
training programs and supplies video production services.
 reuter
</text>]