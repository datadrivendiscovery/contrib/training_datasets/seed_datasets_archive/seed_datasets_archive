[<text>
<title>u.k. money market given 129 mln stg assistance</title>
<dateline>    london, march 30 - </dateline>the bank of england said it had provided
the money market with assistance worth 129 mln stg in the
afternoon session. this compares with the bank's forecast of a
shortage in the system today of around 100 mln stg.
    the central bank purchased 129 mln stg bank bills in band
one at 9-7/8 pct.
 reuter
</text>]