[<text>
<title>ec opens special rebate for maize - paris trade</title>
<dateline>    paris, march 5 - </dateline>the ec commission decided to open a
special daily export rebate today for maize exports to morocco,
israel, canary islands and zone 5c (sub-saharan africa), trade
sources said here.
    the rebate was set at 153 european currency units per tonne
for march and 133 for april through july.
 reuter
</text>]