[<text>
<title>s-k-i  ltd &lt;skii&gt; says skier visits a record</title>
<dateline>    killington, vt., march 24 - </dateline>s-k-i ltd said its skier visits
for the curret season have exceeded 1,500,000, up from the
record of 1,400,000 set last year.
    the company said its killington ski area in central vermont
is expected to operate into june and its mount snow area in
southern vermont will close in may.
 reuter
</text>]