[<text>
<title>advanced micro &lt;amd&gt; unveils computer chips</title>
<dateline>    new york, april 8 - </dateline>advanced micro devices inc said it is
developing a set of five computer chips for use in digital
communications systems.
    the company said the new chips support the integrated
services digital network, or isdn, the international standard
for digital voice and data communications.
    advanced micro also said it has signed a licensing
agreement with american telephone and telegraph co &lt;t&gt; under
which att has provided it with certain digital communications
specifications.
    today's announcement follows the company's introduction
last week of a 32-bit microprocessor.
    "we have now reached a key point in the development of
communications devices that implement a worldwide isdn
standard," said john east, group vice president for advanced
micro's logic businesses.
    he said the company's new computer chips, to be available
by the end of this year, should boost the acceptance and
implementation of isdn services.
     the chip set includes five separate devices in 100 unit
quantities. the devices are priced from 7.25 dlrs to 29.25
dlrs. similar chip sets for isdn communications are already
being sold by a number of companies including intel corp
&lt;intc&gt;, att, rockwell international &lt;rok&gt; and a joint venture
between northern telecom ltd &lt;nt&gt; and motorola inc &lt;mot&gt;.
    the isdn standard, portions of which are still being
defined, will provide for the simultaneous transmission of
voice, data and video.
    advanced micro chip sets will be used in equipment such as
private branch exchanges and desktop computers.
    the company also said it is developing an isdn protocol
controller. it said the controller will connect directly with
its current line of isdn devices.
    the device supports several digital communications
protocols, including ones from international business machines
corp &lt;ibm&gt; and att.
 reuter
</text>]