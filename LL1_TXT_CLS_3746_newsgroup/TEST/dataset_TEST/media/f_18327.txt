[<text>
<title>tcby &lt;tcby.o&gt; withdraws registration</title>
<dateline>    little rock, ark., june 2 - </dateline>tcby enterprises inc said it
will apply to the securities and exchange commission to
withdraw its registration statement filed may 14 for 30 mln
dlrs of subordinated convertible debentures due to unfavorable
market conditions.
    the company said other forms of financing available to the
company and working capital are more than sufficient to fund
the uses for which the proceeds of the debentures offering were
to be applied.
 reuter
</text>]