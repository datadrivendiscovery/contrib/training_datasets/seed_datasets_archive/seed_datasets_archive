[<text>
<title>ny meat cutters union rejects latest offer</title>
<dateline>    chicago, april 29 - </dateline>members of local 174 of the united food
and commercial workers (ufcw) union, representing 1,900 meat
cutters in new york city, yesterday rejected the latest
management offer on a new contract.
    "members rejected the latest offer by a 2 to 1 margin," bob
wilson, executive officer of local 174 said. but workers have
been asked to remain on the job while negotiations continue.
    the union has been in negotiations with the greater new
york association of meat and poultry dealers, a group of meat
wholesalers and distributors. the contract expired april 26 and
the latest management offer sought "give backs" in holiday and
sick leave which the membership rejected, wilson said.
 reuter
</text>]