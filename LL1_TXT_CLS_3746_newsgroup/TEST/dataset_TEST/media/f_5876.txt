[<text>
<title>&lt;wavehill international inc&gt; makes purchse</title>
<dateline>    new york, march 17 - </dateline>wavehill international ventures inc
said it has completed the previously-announced acquisition of
personal computer rental corp for 500,000 restricted common
shares, giving former shareholders of personal computer a 25
pct interest in the combined company.
 reuter
</text>]