[<text>
<title>manor care inc &lt;mnr&gt; 3rd qtr feb 28 net</title>
<dateline>    silver spring, md., march 19 -
    </dateline>shr 24 cts vs 21 cts
    net 9,700,000 vs 8,286,000
    revs 120.6 mln vs 115.7 mln
    avg shrs 40.0 mln vs 39.9 mln
    year
    shr 69 cts vs 68 cts
    net 27.8 mln vs 27.1 mln
    revs 374.9 mln vs 358.8 mln
    avg shrs 40.0 mln vs 39.9 mln
    note: 1986 year net includes charge of 2,396,000 dlrs, or
six cts a share, for debt redemption
 reuter
</text>]