[<text>
<title>&lt;concorde ventures inc&gt; in merger agreement</title>
<dateline>    denver, march 12 - </dateline>concorde ventures inc said it has signed
a letter of intent to acquire englewood, colo., homebuilder
winley inc for 12 mln common shares.
    the investment company said after the merger the combined
company will have 15 mln shares outstanding.
    for the year ended january 31, winley earned 116,000 dlrs
pretax on revenues of 11.7 mln dlrs.
 reuter
</text>]