[<text>
<title>avnet &lt;avt&gt; files convertible debt offering</title>
<dateline>    new york, april 8 - </dateline>avnet inc said it filed with the
securities and exchange commission a registration statement
covering a 150 mln dlr issue of convertible subordinated
debentures due 2012.
    proceeds will be used for general working capital and the
anticipated domestic and foreign expansion of avnet's
businesses, the company said.
    avnet named dillon, read and co inc as manager of the
offering.
 reuter
</text>]