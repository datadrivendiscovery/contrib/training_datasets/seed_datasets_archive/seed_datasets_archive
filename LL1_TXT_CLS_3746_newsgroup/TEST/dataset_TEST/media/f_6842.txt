[<text>
<title>wilson foods &lt;wilf&gt; sets final creditor payment</title>
<dateline>    oklahoma city, okla., march 18 - </dateline>wilson foods corp said it
made the final creditor payment under its 1984 plan of
reorganization.
    the plan resulted from its 1983 filing under chapter 11
filing of the u.s. bankruptcy code.
    including the final payment, the company said it has paid
its creditors 31 mln dlrs plus interest at 9.5 pct, repaying
all outstanding debt owed under the plan.
 reuter
</text>]