[<text>
<title>saab u.s. unit sees limited impact on its cars</title>
<dateline>    detroit, oct 20 - </dateline>the plunge in stock market prices should
have its biggest impact on the middle range of the car market
but have less impact on cheap cars as well as luxury vehicles,
the president of &lt;saab-scania of america inc&gt; said.
    robert sinclair, head of the u.s. subsidiary of saab-scania
ab &lt;sabs.st&gt; of sweden, told reporters "we're much better off
than the middle of the market -- those buyers will wait to see
what happens."
    sinclair said the greatest impact would come on buyers of
cars in the 14,000 dlr to 18,000 dlr range, which has been
targeted for a new product assault by general motors corp &lt;gm&gt; 
and is the segment where ford motor co &lt;f&gt; has had considerable
success with its mid-sized cars.
 reuter
</text>]