[<text>
<title>&lt;the music shop&gt; signs stock option agreement</title>
<dateline>    nashville, tenn, march 5 - </dateline>the music shop said it signed a
stock option agreement with the &lt;saxon co&gt; calling for it to
receive 100,000 dlrs in return for 210 mln shares and an option
for saxon to buy additional common stock for up to 400,000
dlrs.
    the company also said it will hold a special shareholders'
meeting april 15 to vote on a proposed one-for-100 share
reverse split.
    saxon is a venture capital firm from dallas, the company
said.
 reuter
</text>]