[<text>
<title>hydraulic &lt;thc&gt; splits 3-for-2, hikes dividend</title>
<dateline>    bridgeport, conn., march 24 - </dateline>the hydraulic co said its
board approved a three-for-two stock split of its common stock
and increased its quarterly cash dividend.
    it said the stock split will occur through a 50 pct stock
distribution on hydraulic's common stock, payable april 30 to
stockholders of record on april 3.
    the quarterly cash dividend, payable april 15 to
stockholders of record on april 3, is to be paid on hydraulic's
pre-split shares that are currently outstanding, the company
said.
    the dividend will be 54.75 cts per share, up from 52 cts
per share.it will represent a quarterly common stock cash
dividend of 36.50 cts per share on the share that will be
outstanding after the stock split, the company said.
 reuter
</text>]