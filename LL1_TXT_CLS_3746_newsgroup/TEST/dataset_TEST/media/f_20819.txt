[<text>
<title>first federal wooster sets first payout</title>
<dateline>    wooster, n.h., oct 20 - </dateline>&lt;first federal savings and loan
association of wooster&gt; said it declared its first quarterly
dividend of seven cts per share.
    the dividend is payable november 20 to stockholders of
record november 2, the company said.
    the company converted to a stock savings and loan on april
7, 1987.
 reuter
</text>]