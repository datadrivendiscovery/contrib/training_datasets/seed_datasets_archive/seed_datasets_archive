[<text>
<title>usx &lt;x&gt; uss unit raises prices</title>
<dateline>    lorain, ohio, march 24 - </dateline>usx corp's uss subsidiary said
that effective with shipments beginning july 1 prices for all
leaded grades and 1200-series grades of hot rolled bar and
semi-finished products from its lorain, ohio, facility will be
increased by 15 dlrs a ton over the prices in effect june 1.
    it said the increase is being made to reflect current
market conditions.
 reuter
</text>]