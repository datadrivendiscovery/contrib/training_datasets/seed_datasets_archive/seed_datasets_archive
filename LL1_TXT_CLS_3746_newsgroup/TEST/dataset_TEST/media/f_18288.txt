[<text>
<title>general electric &lt;ge&gt; sells engines to bahrain</title>
<dateline>    evendale, ohio, june 2 - </dateline>general electric co said the
bahrain amiri air force selected its engines to power 12 of its
general dynamics corp &lt;gd&gt; fighter aircraft.
    the company did not disclose the price of the engines.
    the company said bahrain was the fourth foreign nation to
select the engine. the others are the greek, israeli and
turkish air forces, the company said.
 reuter
</text>]