[<text>
<title>commerce clearing house inc &lt;cclr&gt; hikes div</title>
<dateline>    chicago, march 26 -
    </dateline>qtly div 32 cts vs 30 cts prior
    pay april 29
    record april 10
 reuter
</text>]