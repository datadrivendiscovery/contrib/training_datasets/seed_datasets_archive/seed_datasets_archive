[<text>
<title>u.k. money market deficit forecast at 800 mln stg</title>
<dateline>    london, april 2 - </dateline>the bank of england said it forecast a
shortage of around 800 mln stg in the money market today.
    among the main factors affecting liquidity, bills for
repurchase by the market will drain some 664 mln stg while
bills maturing in official hands and the take-up of treasury
bills will take out around 508 mln stg and a rise in note
circulation some 45 mln stg.
    partly offsetting these outflows, exchequer transactions
and bankers' balances above target will add some 380 mln stg
and 35 mln stg to the system respectively.
 reuter
</text>]