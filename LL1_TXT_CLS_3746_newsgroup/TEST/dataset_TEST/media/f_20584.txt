[<text>
<title>total capital acquires dunhill compact</title>
<dateline>    denver, oct 20 - </dateline>&lt;total capital corp&gt; said it acquired
&lt;dunhill compact classics inc&gt; for an unspecified amount of
stock.
    the surviving company will be controlled by dunhill's
principals, total capital said.
    dunhill makes compact discs.
 reuter
</text>]