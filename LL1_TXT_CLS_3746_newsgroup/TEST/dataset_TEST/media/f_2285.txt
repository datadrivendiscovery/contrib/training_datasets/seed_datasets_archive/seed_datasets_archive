[<text>
<title>datatmetrics corp &lt;dmcz&gt; wins army contract</title>
<dateline>    chatsworth, calif., march 5 - </dateline>datametrics corp said it won
a 4.9-mln-dlr contract from the u.s. army.
    the company said datametrics will deliver printer/plotters
to the army's redstone arsenal, beginning in september, 1987.
    datametrics said it had a record backlog of funded and
unfunded projects of 31.8 mln dlrs on march 1, 1987.
 reuter
</text>]