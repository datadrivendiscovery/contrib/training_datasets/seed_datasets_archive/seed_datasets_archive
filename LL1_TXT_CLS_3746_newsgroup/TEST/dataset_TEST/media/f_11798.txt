[<text>
<title>u.k. money market given 689 mln stg early help</title>
<dateline>    london, april 1 - </dateline>the bank of england said it had provided
the money market with early assistance of 689 mln stg in
response to an early round of bill offers from the discount
houses. this compares with the bank's estimate that the system
would face a shortage of around 1.2 billion stg today.
    the central bank made outright purchases of bank bills
comprising 347 mln stg in band one at 9-7/8 pct, 207 mln stg in
band two at 9-13/16 pct and 135 mln stg in band three at 9-3/4
pct.
 reuter
</text>]