[<text>
<title>cross and trecker &lt;ctco&gt; to sell unit</title>
<dateline>    cleveland, march 18 - </dateline>cross and trecker corp said its
warner and swasey subsidiary will seek to sell its grinding
division to focus on other areas of its business.
    the company said the grinding division had sales last year
of about 18 mln dlrs. it makes grinding machines.
 reuter
</text>]