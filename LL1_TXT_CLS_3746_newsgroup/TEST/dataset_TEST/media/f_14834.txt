[<text>
<title>japanese consortium wins new york subway car order</title>
<dateline>    tokyo, april 8 - </dateline>&lt;nissho iwai corp&gt; and kawasaki heavy
industries ltd &lt;kawh.t&gt; have jointly won an order to build 200
subway cars worth about 200 mln dlrs for the metropolitan
transportation authority of new york, a nissho spokesman said.
    the cars, to be produced with 51 pct u.s. content, will be
delivered between early 1988 and august 1989, he said.
    ten will be built in japan, but the others will be made at
a yonkers, new york, plant equally owned by nissho and
kawasaki.
 reuter
</text>]