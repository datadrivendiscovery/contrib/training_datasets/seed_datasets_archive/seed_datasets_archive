[<text>
<title>reliable life insurance co &lt;rlifa&gt; ups dividends</title>
<dateline>    st. louis, march 18 -
    </dateline>qtly div class a 27.5 cts vs 26.4 cts prior
    qtly div class b 2.5 cts vs 2.4 cts prior
    pay june one
    record may one
 reuter
</text>]