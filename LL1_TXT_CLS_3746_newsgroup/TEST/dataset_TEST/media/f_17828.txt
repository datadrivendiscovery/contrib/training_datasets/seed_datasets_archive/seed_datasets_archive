[<text>
<title>chrysler &lt;c&gt; may car output falls</title>
<dateline>    detroit, june 1 - </dateline>chrysler corp said its may north american
car production fell 12.8 pct to 94,715 from 108,595 a year ago.
    chrysler said its may truck production rose 32 pct to
61,151 from 46,332 last year.
 reuter
</text>]