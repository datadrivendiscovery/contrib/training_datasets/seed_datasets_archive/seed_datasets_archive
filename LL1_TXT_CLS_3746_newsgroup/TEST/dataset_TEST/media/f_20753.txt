[<text>
<title>first republicbank &lt;frb.n&gt; corp 3rd qtr loss</title>
<dateline>    dallas, oct 20 -
    </dateline>shr diluted loss 46 cts
    net loss 6,300,000
    nine mths
    shr diluted loss 10.89 dlrs
    net loss 309,100,000
    note: no comparisons because company was formed in june
1987 through merger of republicbank corp and interfirst corp.
    nine mths includes previously reported provision of 325 mln
dlrs in second quarter for possible losses on ldc loans.
 reuter
</text>]