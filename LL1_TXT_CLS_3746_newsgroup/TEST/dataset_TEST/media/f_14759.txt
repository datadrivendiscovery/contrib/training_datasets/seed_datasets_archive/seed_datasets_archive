[<text>
<title>comsat &lt;cq&gt; changes name of hi-net unit</title>
<dateline>    washington, april 7 - </dateline>communications satellite corp said it
changed the name of its hi-net communications unit to comsat
video enterprises to better reflect the nature of the unit's
operations.
    comsat recently took full control of hi-net, which provides
in-room entertainment to hotels by satellite, by acquiring
holiday corp's &lt;hia&gt; 50 pct interest in the firm.
    in addition, comsat said it signed new seven-year contracts
with laquinta motor inns inc &lt;lqm&gt; and &lt;drury inns inc&gt;.
 reuter
</text>]