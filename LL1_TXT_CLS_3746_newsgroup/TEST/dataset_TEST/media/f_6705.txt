[<text>
<title>ohio art co &lt;oar&gt; sets quarterly</title>
<dateline>    bryan, ohio, march 18 -
    </dateline>qtly div six cts vs six cts prior
    pay may eight
    record april 10
 reuter
</text>]