[<text>
<title>cri insured &lt;cii&gt; to set special distribution</title>
<dateline>    rockville, md., march 4 - </dateline>cri insured mortgage investments
inc said its advisor will recommend a special distribution of
50 cts per share due to the sale of a federally insured first
mortgage on park meadows i in madison, wis., for 4,267,871
dlrs.
    it said it received a 3,695,465 dlr return of capital and
572,406 dlrs in income on the sale, and the 50 ct distribution
would represent a 43.294 ct return of principal and a 6.706 ct
capital gain and would be payable june 30 to holders of record
may 31.
 reuter
</text>]