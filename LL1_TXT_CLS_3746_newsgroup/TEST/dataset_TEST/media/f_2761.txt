[<text>
<title>&lt;warrantech corp&gt; 3rd qtr dec 31 net</title>
<dateline>    new york, march 6 -
    </dateline>shr profit nil vs loss nil
    net profit 28,565 vs loss 204,553
    revs 507,529 vs 6,563
    nine mths
    shr loss nil vs loss nil
    net loss 404,011 vs loss 649,495
    revs 938,345 vs 32,535
 reuter
</text>]