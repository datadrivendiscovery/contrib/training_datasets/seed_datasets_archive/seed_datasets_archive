[<text>
<title>josephson &lt;json.o&gt; repurchases 445,874 shares</title>
<dateline>    new york, oct 20 - </dateline>josephson international inc said it
repurchased 445,874 common shares yesterday under an
authorization to buy back up to 500,000 shares and its board
has authorized the further repurchase of another 500,000 shares
in the open market and privately.
 reuter
</text>]