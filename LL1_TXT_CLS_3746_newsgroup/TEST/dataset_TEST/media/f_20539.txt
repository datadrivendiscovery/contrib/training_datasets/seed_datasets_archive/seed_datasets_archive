[<text>
<title>northrop &lt;noc&gt; backlog up slightly</title>
<dateline>    los angeles, oct 20 - </dateline>northrop corp said its backlog at
september 30 stood at 4.37 billion dlrs, up slightly from the
4.32 billion recorded a yeaer earlier.
    northrop also reported a third quarter profit of 34.1 mln
dlrs, or 73 cts per share, compared with a year-earlier loss of
30.5 mln dlrs, or 65 cts per share.
    the company said its operating profit for the quarter
totaled 100.8 mln dlrs, compared with a 17 mln dlr operating
loss a year ago.
    sales for the period climbed to 1.46 billion dlrs from 1.26
billion last year, northrop said.
 reuter
</text>]