[<text>
<title>walker telecommunications &lt;wtel&gt; gets contract</title>
<dateline>    hauppauge, n.y., april 7 - </dateline>walker telecommunications corp
said teledial america has selected the fiber optic system
operated by walker's mutual signal corp subsidiary to carry
teledial's traffic in michigan and link interstate facilities
throughout the country.
 reuter
</text>]