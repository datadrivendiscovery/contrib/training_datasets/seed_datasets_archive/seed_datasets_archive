[<text>
<title>greenwood resources &lt;grrl&gt; sells company stake</title>
<dateline>    denver, march 3 - </dateline>greenwood resources inc said it has sold
its 4,300,000 common share majority holding in &lt;new london oil
ltd&gt; of london to an affiliate of &lt;guinness peat group plc&gt; of
london and an affiliate of &lt;sidro sa&gt; of belgium for a total of
1,700,0000 dlrs in cash.
    the company said it will apply the proceeds of the sale to
support its line of credit and as part of a proposed debt
restructuring with colorado national bancshares &lt;colc&gt; and
greenwood shareholders.
    it said it will retain a seat on the new london board.
 reuter
</text>]