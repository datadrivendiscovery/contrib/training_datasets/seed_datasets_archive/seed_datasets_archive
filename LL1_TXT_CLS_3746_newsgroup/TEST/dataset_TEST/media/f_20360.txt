[<text>
<title>freeport-mcmoran oil and gas &lt;fmr&gt; payout off</title>
<dateline>    houston, oct 20 -
    </dateline>mthly div 7.354 cts vs 8.362 cts prior
    pay jan 10
    record oct 30
    note: freeport-mcmoran oil and gas royalty trust.
 reuter
</text>]