[<text>
<title>university bank and trust co &lt;ubtc.o&gt; dividend</title>
<dateline>    newton, mass., june 19 -
    </dateline>qtly div five cts vs five cts prior
    pay july 15
    record june 30
 reuter
</text>]