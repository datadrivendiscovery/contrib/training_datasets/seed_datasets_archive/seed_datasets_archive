[<text>
<title>royal palm savings &lt;rpal.o&gt; names president</title>
<dateline>    west palm beach, fla, april 13 - </dateline>royal palm savings said it
named joseph burgoon as president, a position which has been
vacant since jay rosen left it in december 1985 to pursue other
business ventures.
    burgoon most recently served as executive vice president
and chief operating officer of cardinal federal savings bank of
cleveland.
 reuter
</text>]