[<text>
<title>japan to widen participation in telecom services</title>
<dateline>    tokyo, march 13 - </dateline>japan agreed in talks with the u.s. to
amend its laws to allow more companies to engage in
international value-added network telecommunications services,
a post and telecommunications ministry official said.
    such services make communications between otherwise
incompatible computers possible over telecommunications lines.
    the official said firms registering as "special type two
telecommunications firms," which are those leasing lines from
common carriers, will be permitted to re-lease the lines to
users and provide international services.
    steps will also be taken to assure fairness in negotiations
between common carriers and firms seeking to lease lines, the
official said.
    since december, 10 companies have registered as special
type two firms, including two which have american telephone and
telegraph co &lt;t&gt; or mcdonnell douglas corp &lt;md&gt; as major
shareholders, the official said.
    currently only &lt;kokusai denshin denwa co ltd&gt; and &lt;nippon
telegraph and telephone corp&gt; are allowed to operate
international telecommunications services.
 reuter
</text>]