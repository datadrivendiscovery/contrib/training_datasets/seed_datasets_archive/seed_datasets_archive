[<text>
<title>may &lt;ma&gt; to sell sycamore division</title>
<dateline>    st. louis, march 31 - </dateline>the may department stores co said it
has signed an agreement to sell its sycamore specialty store
division to an investment group that includes syacmore senior
management.
    may said it expects to close the deal, which includes the
entire division and its 1,000 employees, in april.
    sycamore has 111 women's apparel stores in indiana, ohio,
illinois, kentucky and michigan, may said.
    may added that the indiana national bank of indianapolis
provided a portion of the deal's financing.
 reuter
</text>]