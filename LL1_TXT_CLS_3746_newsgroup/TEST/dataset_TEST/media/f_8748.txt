[<text>
<title>argyll sells subsidiary's assets for 14 mln stg</title>
<dateline>    london, march 24 - </dateline>food and drink retailer argyll group plc
&lt;ayll.l&gt; said it has agreed to sell its u.k. subsidiary &lt;george
morton ltd&gt; to &lt;seagram united kingdom ltd&gt; for about 14 mln
stg in cash.
    the consideration for morton's fixed assets, stocks,
debtors and goodwill is payable on completion of the sale. the
disposal will bring argyll an extraordinary credit of some 8.4
mln stg.
    argyll added the agreements also depend on an indication
from the u.k. office of fair trading by june 23 that the sale
will not be referred to the monopolies commission. argyll
shares were up 12p to 440, firming before the announcement.
 reuter
</text>]