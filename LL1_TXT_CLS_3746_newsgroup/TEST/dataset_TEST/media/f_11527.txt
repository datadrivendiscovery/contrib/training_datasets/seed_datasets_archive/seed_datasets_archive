[<text>
<title>lincoln foodservice &lt;linn&gt; completes purchase</title>
<dateline>    fort wayne, ind, march 31 - </dateline>lincoln foodservice products
inc said it completed purchasing certain assets of redco
product line of food slicers, cutters and wedgers from the
dean/alco food prep division of &lt;alco foodservice equipment
co.&gt;
    terms were not disclosed.
 reuter
</text>]