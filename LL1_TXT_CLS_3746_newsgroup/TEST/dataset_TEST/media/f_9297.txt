[<text>
<title>general electric &lt;ge&gt; sells rights to technique</title>
<dateline>    troy, n.y., march 25 - </dateline>general electric co said &lt;bioreactor
technology inc&gt; obtained its rights to an experimental
technique for analyzing blood samples to detect disease
antibodies.
    general electric said bioreactor will attempt to develop
test market kits from the advances made by general electric.
    general electric said it granted the rights to bioreactor
because the innovation, first announced in 1972, did not fit
into the company's long-range product plans.
    in exchange for the patent, ge said it will receive an
unspecified interest in the company.
               
 reuter
</text>]