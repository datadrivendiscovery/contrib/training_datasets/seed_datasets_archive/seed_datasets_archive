[<text>
<title>new jersey steel files for initial stock sale</title>
<dateline>    sayreville, n.j., april 9 - </dateline>&lt;new jersey steel corp&gt; said it
filed a registration statement with the securities and exchange
commission for the initial public offering of 1.8 mln shares of
common stock at an estimated price of 20 dlrs to 22 dlrs a
share.
    the company said it would sell one mln shares and that &lt;von
roll ltd&gt;, would sell the remaining 800,000 shares.
    proceeds from the offering, to be made through an
underwriting group managed by painewebber inc, will be used for
capital expenditures and repayment of long-term debt owed to
von roll, the company said.
 reuter
</text>]