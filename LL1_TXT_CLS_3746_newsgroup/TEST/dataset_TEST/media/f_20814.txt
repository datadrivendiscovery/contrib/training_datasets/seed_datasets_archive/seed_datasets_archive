[<text>
<title>alfa corp &lt;alfa.o&gt; 3rd qtr net</title>
<dateline>    montgomery, ala., oct 20 -
    </dateline>shr 28 cts vs 21 cts
    net 4,653,815 vs 3,564,451
    revs 34.0 mln vs 12.8 mln
    nine mths
    shr 61 cts vs 53 cts
    net 10.4 mln vs 8,881,825
    revs 61.8 mln vs 38.4 mln
    note: per shr amounts are after giving retroactive effect
for a 2-for-1 split effected as a 100 pct stock dividend which
was paid june 1, 1987.net includes net realized investment
gains of 1,213,471 vs 937,801 in nine mths 1987 vs 1986, and
604,172 vs 474,556 in qtr 1987 vs 1986. net includes net
investment income of 12.5 mln vs 11.2 mln in nine mths 1987 vs
1986, and 4,480,540 vs 3,781,245 in qtr 1987 vs 1986.
 reuter
</text>]