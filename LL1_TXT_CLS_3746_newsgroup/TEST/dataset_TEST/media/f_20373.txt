[<text>
<title>first oak brook &lt;fobba.o&gt; 3rd qtr net</title>
<dateline>    oak brook, ill., oct 20 -
    </dateline>shr 61 cts vs 55 cts
    net 726,000 vs 669,000
    nine mths
    shr 1.78 dlrs vs 1.63 dlrs
    net 2,133,000 vs 1,960,000
    note: full name is first oak brook bancshares inc
 reuter
</text>]