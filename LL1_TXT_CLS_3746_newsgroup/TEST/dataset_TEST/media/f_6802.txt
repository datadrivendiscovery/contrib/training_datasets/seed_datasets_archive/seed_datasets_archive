[<text>
<title>hartford national corp &lt;hnat&gt; regular dividend</title>
<dateline>    hartford, conn., march 18 -
    </dateline>qtly div 30 cts vs 30 cts in prior qtr
    payable april 20
    record march 31
 reuter
</text>]