[<text>
<title>biotech electronics inc &lt;ion.to&gt; 3rd qtr net</title>
<dateline>    montreal, april 13 -
    </dateline>shr eight cts vs one-half ct
    net 508,000 vs 21,000
    revs 7.2 mln vs 7.4 mln
    nine mths
    shr 28 cts vs 11 cts
    net 1,523,000 vs 567,000
    revs 26.2 mln vs 22.6 mln
    note: period ended february 28.
reuter
 reuter
</text>]