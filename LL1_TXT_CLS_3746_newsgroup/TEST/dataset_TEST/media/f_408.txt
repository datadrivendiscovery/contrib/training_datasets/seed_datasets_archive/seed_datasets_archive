[<text>
<title>pittston &lt;pco&gt; agrees to acquire wtc &lt;waf&gt;</title>
<dateline>    stamford, conn., march 2 - </dateline>pittston co said it has
tentatively agreed to acquire wtc international n.v. in a
tax-free exchange of stock.
    pittston said it agreed to exchange 0.523 common share for
each of the about 8,612,000 wtc common shares outstanding.
    pittston said wtc's three principal shareholders, who own
62 pct of its stock, are parties to this agreement. they have
granted pittston the right of first refusal to their shares.
    wtc has granted pittston an option to buy wtc shares equal
to 18.5 poct of its outstanding stock. the agreement is subject
to approval of both boards and wtc shareholders.
    pittston said described wtc as a fast growing air freight
forwarding company with operations throughout the world. its
revenues totaled nearly 200 mln dlrs in the year ended november
30 and for the quarter ended on that date it earned 1.3 mln
dlrs on revenues of 55.8 mln dlrs.
    pittston said its burlington air express subsidiary
generates about two-thirds of its 450 mln dlrs in annual
revenes with its domestic air freight services.
 reuter
</text>]