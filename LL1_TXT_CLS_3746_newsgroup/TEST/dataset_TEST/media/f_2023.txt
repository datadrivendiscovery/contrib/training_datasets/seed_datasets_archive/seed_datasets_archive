[<text>
<title>supermarkets general &lt;sgc&gt; february sales rise</title>
<dateline>    woodbridge, n.j., march 5 - </dateline>supermarkets general corp
reported sales of 424.8 mln dlrs for the four-week period ended
feb 28, 1987, a 7.2 pct increase over sales of 396.4 mln dlrs
for the comparable period last year.
 reuter
</text>]