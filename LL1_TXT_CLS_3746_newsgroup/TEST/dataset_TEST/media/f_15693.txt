[<text>
<title>u.s., britain bar sanctions against south africa</title>
<dateline>    united nations, april 9 - </dateline>the united states and britain
served notice that they would block a move in the
u.n. security council to impose sanctions against south africa
in a bid to end its rule of namibia (south west africa).
    their vetoes would repeat the joint action they took on
february 20 to bar selective sanctions against pretoria over
its apartheid policy.
    "measures of this sort would be counterproductive, giving
south africa the excuse to remain intransigent," john birch, the
british delegate, said. vernon walters, the u.s.
representative, said washington flatly opposed mandatory
sanctions.
   
 reuter
</text>]