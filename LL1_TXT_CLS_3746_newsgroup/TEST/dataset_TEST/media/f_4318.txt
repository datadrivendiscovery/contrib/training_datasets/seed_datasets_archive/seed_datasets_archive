[<text>
<title>lands end inc &lt;leys&gt; year jan 31 net</title>
<dateline>    dodgeville, wis., march 12 -
    </dateline>shr 1.46 dlrs vs 1.13 dlrs
    net 14,650,000 vs 11,270,000
    sales 265 mln vs 227.1 mln
    avg shrs 10,020,000 vs 9,980,000
    note: earnings are pro forma, including the increase in
common shares that took place last october when the company
went public through an initial offering of 1.4 mln shares. avg
shrs assume the shares sold to public and employees were
outstanding during the entire period.
 reuter
</text>]