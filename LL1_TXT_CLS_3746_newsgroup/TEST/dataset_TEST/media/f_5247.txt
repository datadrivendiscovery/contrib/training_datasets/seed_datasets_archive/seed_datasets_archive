[<text>
<title>bridgestone and clevite in car parts venture</title>
<dateline>    tokyo, march 16 - </dateline>bridgestone corp &lt;brit.t&gt; said it and
&lt;clevite industries inc&gt; of the united states will launch a
joint venture in the u.s. to produce parts for car engines.
    the joint firm is to have a capital of 40 mln dlrs and will
produce rubber for cushioning car engines from vibration.
launch date for the company, be owned 51 pct by clevite and 49
pct by bridgestone, is to be in june.
 reuter
</text>]