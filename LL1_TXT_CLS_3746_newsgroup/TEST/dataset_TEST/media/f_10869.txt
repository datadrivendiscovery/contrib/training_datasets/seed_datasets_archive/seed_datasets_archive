[<text>
<title>westinghouse &lt;wx&gt; to move power generation unit</title>
<dateline>    pittsburgh, march 30 - </dateline>westinghouse electric corp said it
has relocated its power generation service division
headquarters to locations of existing westinghouse operations
in monroeville, pa., and orlando, fla.
    the company said about 25 of the 135 employees at the
broomall site will be moved to monroeville and 45 to orlando,
with the remaining 65 having their employment terminated.
    it said terminated employees will receive enhanced
separation benefits.
 reuter
</text>]