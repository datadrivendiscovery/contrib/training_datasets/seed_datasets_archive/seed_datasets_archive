[<text>
<title>cater hawley hale &lt;chh&gt; march sales up 7.8 pct</title>
<dateline>    los angeles, april 9 - </dateline>carter hawley hale stores inc said
sales for the five weeks ended april four were up 7.8 pct to
314.0 mln dlrs from 291.3 mln dlrs a year earlier, with
same-store sales up 5.5 pct.
    the company said sales for the first two months of its
fiscal year were up 7.7 pct to 554.7 mln dlrs from 515.1 mln
dlrs a year earlier, with same-store sales up 5.6 pct.
    carter hawley said sales were helped by shifting some
promotions to march from april to compensate for the later
easter this year.
 reuter
</text>]