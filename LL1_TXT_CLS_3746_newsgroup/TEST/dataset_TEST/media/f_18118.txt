[<text>
<title>immunomedics &lt;immu.o&gt; has new linker technology</title>
<dateline>    newark, n.j., june 2 - </dateline>immunomedics inc said it has
developed a technology that makes it possible to join chemical
molecules to antibodies in a more selective manner than now
possible.
    it said it has filed a patent application, which is
expected to be issued shortly as a patent for the technology.
 reuter
</text>]