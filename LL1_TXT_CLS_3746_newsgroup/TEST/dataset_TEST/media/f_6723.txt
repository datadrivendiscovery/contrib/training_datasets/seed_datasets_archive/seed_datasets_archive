[<text>
<title>novar electronics &lt;novr&gt; sees results improving</title>
<dateline>    barberton, ohio, march 18 - </dateline>novar electronics corp said it
expects improved earnings this year due to a rapid expansion of
its logic one computerized buolding management system customer
base and expectations of good crime deterrent business.
    the company today reported earnings for the year ended
january three of 207,514 dlrs, up from 98,050 dlrs a year
before.
 reuter
</text>]