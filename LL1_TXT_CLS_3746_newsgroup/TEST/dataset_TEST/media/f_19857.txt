[<text>
<title>piedmont &lt;pie&gt; accepts reservations for bahamas</title>
<dateline>    winston-salem, n.c., june 29 - </dateline>piedmont aviation inc said
it is accepting reservations for nonstop service to nassau,
bahamas from charlotte, n.c. beginning november 15.
    the new service will connect more than 30 cities on the
piedmont system to nassau, the company said.
 reuter
</text>]