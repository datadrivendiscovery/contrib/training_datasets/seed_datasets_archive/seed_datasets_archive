[<text>
<title>rpc energy services inc &lt;res&gt; 1st qtr sept 30</title>
<dateline>    atlanta, oct 20 -
    </dateline>shr profit one cent vs loss 29 cts
    net profit 116,000 vs loss 4,195,000
    revs 20.2 mln vs 6,393,000
 reuter
</text>]