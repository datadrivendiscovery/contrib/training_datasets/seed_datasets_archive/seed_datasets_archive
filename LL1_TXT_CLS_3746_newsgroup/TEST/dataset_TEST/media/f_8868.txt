[<text>
<title>technitrol inc &lt;tnl&gt; 4th qtr</title>
<dateline>    philadelphia, march 24 -
    </dateline>shr 54 cts vs 47 cts
    net 1.1 mln vs 941,000
    revs 8.9 mln vs 10.3 mln
    year
    shr 1.65 dlrs vs 1.64 dlrs
    net 3.3 mln vs 3.3 mln
    revs 37.4 mln vs 39.0 mln
    
 reuter
</text>]