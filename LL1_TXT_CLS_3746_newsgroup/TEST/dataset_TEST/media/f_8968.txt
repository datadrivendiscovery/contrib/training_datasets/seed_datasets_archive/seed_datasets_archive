[<text>
<title>signet banking corp regular dividend</title>
<dateline>    richmond, va, march 24 -
    </dateline>qtly div 31 cts vs 31 cts prior
    payable april 22
    record april three
 reuter
</text>]