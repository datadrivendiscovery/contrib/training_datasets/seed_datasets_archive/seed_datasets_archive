[<text>
<title>binks mfg co &lt;bin&gt; regular dividend set</title>
<dateline>    franklin park, ill., march 20 -
    </dateline>qtly div 25 cts vs 25 cts previously
    pay april 17
    record march 30
 reuter
</text>]