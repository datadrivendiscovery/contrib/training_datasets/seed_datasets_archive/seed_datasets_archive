[<text>
<title>proposed offerings recently filed with the sec</title>
<dateline>    washington, march 11 - </dateline>the following proposed securities
offerings were filed recently with the securities and exchange
commission:
    minnesota power and light co &lt;mpl&gt; - offering of 50 mln
dlrs in first mortgage bonds due april 1, 1994 and 75 mln dlrs
in first mortgage bonds due april 1, 1997 through painewebber
inc.
    international lease finance corp &lt;ilfc&gt; - shelf offering of
up to 1,000 shares of dutch auction rate transferable
securities (darts) preferred stock through salomon brothers inc
and shearson lehman brothers inc.
    trinity industries leasing co - offering of 100 mln dlrs of
convertible debentures due april 1, 2012 through merrill lynch
capital markets.
 reuter
</text>]