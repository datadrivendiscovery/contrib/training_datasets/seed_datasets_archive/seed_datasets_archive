[<text>
<title>intek diversified corp &lt;idcc&gt; 4th qtr net</title>
<dateline>    los angeles, march 3 -
    </dateline>shr three cts vs three cts
    net 98,20000 vs 91,898
    revs 2,843,520 vs 2,372,457
    year
    shr 13 cts vs 21 cts
    net 401,179 vs 681,374
    revs 10.5 mln vs 9,699,535
 reuter
</text>]