[<text>
<title>laidlaw industries inc &lt;lwsi&gt; 2nd qtr feb 28</title>
<dateline>    hurst, texas, april 2 -
    </dateline>shr 20 cts vs 20 cts
    net 4.1 mln vs 4.0 mln
    qtly div five cts vs five cts prior
    revs 118.0 mln vs 45.2 mln
    six months
    shr 40 cts vs 35 cts
    net 8.1 mln vs 6.9 mln
    revs 215.6 mln vs 90.3 mln
    note:dividend payable july 31 to holders of record july 15.
 reuter
</text>]