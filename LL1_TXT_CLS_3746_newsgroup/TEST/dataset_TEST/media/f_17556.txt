[<text>
<title>marcade group inc &lt;mar&gt; 1st qtr may 2 net</title>
<dateline>    new york, june 1 -
    </dateline>shr four cts vs 10 cts
    shr diluted two cts vs 10 cts
    net 1,841,000 vs 978,000
    revs 36.1 mln vs 20.5 mln
    avg shrs 25,734,000 vs 9,200,000
    avg shrs diluted 48,878,000 vs 9,200,000
 reuter
</text>]