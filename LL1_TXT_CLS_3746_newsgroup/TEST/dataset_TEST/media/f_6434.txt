[<text>
<title>rank organisation current trading satisfactory</title>
<dateline>    london, march 18 - </dateline>rank organisation plc &lt;rank.l&gt; said
trading in the current year had continued satisfactorily taking
into account seasonal fluctuations.
    association companies, such as &lt;rank-xerox ltd&gt;, indicated
an improved performance, a statement issued at the annual
meeting said.
    it said it planned to spend some 15 mln stg on refurbishing
its odeon cinema chain and the rank film distributors unit was
committed to spending 20 mln to secure international
distribution rights of films.
    investment in new activities in 1987 should continue at a
relatively high level to exploit opportunities for growth. rank
said it did not exclude the possibility of making large as well
as small acquisitions.
    in the year to end-october, rank reported a rise in pretax
profits to 164.1 mln stg from 136.0 mln previously.
    rank shares firmed in morning trading to be quoted at 712p
at 1320 gmt after 697p at last night's close.
 reuter
</text>]