[<text>
<title>leader development &lt;ldco.o&gt; merger approved</title>
<dateline>    columbus, ohio, june 18 - </dateline>leader development corp said
shareholders at the annual meeting approved the acquisition of
privately-held clinton american corp and two related
partnerships for 3,450,000 common shares, with the transaction
to be accounted for as a pooling of interests.
    the company said clinton president f. daniel ryan will
become president of leader.
 reuter
</text>]