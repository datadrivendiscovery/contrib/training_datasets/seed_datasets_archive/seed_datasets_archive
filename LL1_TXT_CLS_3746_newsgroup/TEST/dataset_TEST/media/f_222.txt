[<text>
<title>britain's alliance opposition wins by-election</title>
<dateline>    london, feb 27 - </dateline>britain's centrist liberal-social
democratic alliance won a surprise victory in a parliamentary
by-election in the london borough of greenwich, a seat held by
the main opposition labour party for the past 50 years.
    rosie barnes, a social democratic member of the alliance,
won with 18,287 votes, or 53 pct, and a majority of 6,611 seats
over her nearest rival, labour candidate deirdre wood.
    the conservatives came third with 3,852 votes.
    the result is expected to play a key role in determining
when prime minister margaret thatcher, leader of the ruling
conservatives, might call a general election.
 reuter
</text>]