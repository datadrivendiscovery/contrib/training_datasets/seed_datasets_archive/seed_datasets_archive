[<text>
<title>oppenheimer industries &lt;opp&gt; sees year loss</title>
<dateline>    kansas city, march 16 - </dateline>oppenheimer industries inc said it
expects to report a loss for the year ended january 31 of about
980,000 dlrs, compared with a profit of 211,074 dlrs a year
before.
    the company blamed the loss on the continuing depression in
agriculture, the discontinuance of several programs due to the
passage if the 1986 tax bill and the failure to close the sale
of four ranches in the california carrizo plains during the
year as expected.
    the company said the prospective purchaser forfieted a
500,000 dlr deposit  it said it is in talks on a new contract.
 reuter
</text>]