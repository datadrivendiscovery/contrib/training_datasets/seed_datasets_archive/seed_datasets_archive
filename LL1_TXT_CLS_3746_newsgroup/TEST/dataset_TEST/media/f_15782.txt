[<text>
<title>central bancorp inc &lt;cban&gt; 1st qtr net</title>
<dateline>    cincinnati, april 9 -
    </dateline>shr 1.02 dlrs vs 78 cts
    net 14.4 mln vs 11.0 mln
    note: 1987 net includes gain 2,222,000 dlrs from
termination of pension plan.
    results restated for pooled acquisitions and share adjusted
for stock dividends.
 reuter
</text>]