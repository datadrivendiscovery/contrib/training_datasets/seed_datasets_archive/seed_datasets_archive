[<text>
<title>presidio oil &lt;prs&gt; begins exchange offer</title>
<dateline>    denver, april 3 - </dateline>presidio oil co said it began an offer to
exchange shares of its recently created class a common stock
for shares of its class b common stock on a share-for-share
basis.
    the oil and gas company also said it plans to declare an
annual cash dividend of 10 cts a share, payable quarterly, on
the class a stock. though not restricted from doing so, it does
not plan to pay cash dividends on the class b common.
    the exchange offer is on a one-time only basis and will be
open for 90 days, it said.
 reuter
</text>]