[<text>
<title>quest &lt;qbio&gt; signs pact with alza &lt;aza&gt;</title>
<dateline>    detroit, march 31 - </dateline>quest biotechnology inc said its quest
blood substitute inc subsidiary executed an agreement with alza
corp &lt;aza&gt; which will make alza a preferred shareholder of its
subsidiary.
    quest said the agreement also offers alza the right to
acquire up to 25 pct of the unit's equity in exchange for the
acquisition of patent rights to alza technology in an area
where quest has an interest.
    quest also said its signed a merger agreement with &lt;hunt
research corp&gt; and its affiliate &lt;icas corp&gt;. quest said it
expects to complete the merger within the next several weeks.
 reuter
</text>]