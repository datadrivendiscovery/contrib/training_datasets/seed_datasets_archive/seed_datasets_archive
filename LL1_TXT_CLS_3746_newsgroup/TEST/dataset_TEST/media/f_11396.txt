[<text>
<title>chubb corp &lt;cb&gt; completes acquisition</title>
<dateline>    new york, march 31 - </dateline>the chubb corp said it completed the
previously-announced merger of its subsidiary with and into
sovereign corp &lt;sovr&gt;, a life insurance holding company.
    under the terms of the merger, sovereign stockholders
wil receive, in a tax free exchange, 0.1365 of a share of chubb
common stock for each share of sovereign held, the company
said. 
    this equals 9.11 dlrs per share of sovereign stock based on
the closing price of chubb common stock on march 30.
   
 reuter
</text>]