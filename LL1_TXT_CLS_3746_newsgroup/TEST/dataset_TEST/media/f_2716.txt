[<text>
<title>valero energy knows of no reason for stock gain</title>
<dateline>    san antonio, tex., march 6 - </dateline>valero energy corp &lt;vlo&gt; said
it knows of no reason for its stock activity today.
    the stock rose 1-5/8 to 10-7/8, a gain of about 17.5 pct.
    a spokesman for valero, steve fry, said the company knows
of no reason for the increased volume and sharp gain in the
stock.
 reuter
</text>]