[<text>
<title>franklin insured sets monthly payout</title>
<dateline>    san mateo, calif., april 13 -
    </dateline>mthly div 7.1 cts vs 7.1 cts prior
    pay april 30
    reord april 15
    note: franklin insured tax-free income fund.
 reuter
</text>]