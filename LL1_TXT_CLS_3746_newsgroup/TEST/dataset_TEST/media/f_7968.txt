[<text>
<title>champion parts&lt;creb&gt; asks declaratory judgment</title>
<dateline>    oak brook, ill., march 20 - </dateline>champion parts rebuilders inc
said it asked the federal district court in chicago for a
declaratory judgment upholding its recent 5.4 mln dlr sale of
common shares and warrants to echlin inc &lt;ech&gt;.
    champion said in hearings thursday morning before the
federal judge on its lawsuit charging federal securities law
violations against cormier corp, odilon cormier, morris navon
and other defendants, the cormier-navon defendants indicated
they would challenge the transaction.
    champion's suit claims that various champion investors
alligned themselves with cormier and navon who failed to
disclose properly under federal laws that they were acting in
concert and they intended to spin off parts of the company and
sell the balance within two years once they got control.
 reuter
</text>]