[<text>
<title>tandy &lt;tan&gt; debt may be upgraded by moody's</title>
<dateline>    new york, june 2 - </dateline>moody's investors service inc said it
may upgrade tandy corp's 175 mln dlrs of long-term debt.
    the agency said the review would focus on tandy's capacity
to sustain its current earnings recovery and on the company's
ability to further improve its cash flow performance by greater
operating efficiencies.
    moody's said it would also evaluate tandy's capital
structure. that structure has changed significantly as a result
of the retirement of about 250 mln dlrs of debt in the past
year. under review are tandy's a-3 senior debt and baa-1
subordinated debt.
 reuter
</text>]