[<text>
<title>national sanitary supply co &lt;nssx.o&gt; 3rd qtr</title>
<dateline>    los angeles, oct 19 -
    </dateline>shr 16 cts vs 14 cts
    net 954,000 vs 777,000
    revs 24.7 mln vs 21.5 mln
    avg shrs 6,000,000 vs 6,000,000
    nine mths
    shr 39 cts vs 34 cts
    net 2,314,000 vs 1,805,000
    revs 69.2 mln vs 59.3 mln
    avg shrs 6,000,000 vs 5,363,000
 reuter
</text>]