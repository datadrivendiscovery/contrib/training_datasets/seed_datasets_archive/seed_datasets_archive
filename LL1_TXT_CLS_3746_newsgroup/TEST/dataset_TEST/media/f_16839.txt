[<text>
<title>diamond shamrock offshore partners &lt;dsp&gt; payout</title>
<dateline>    dallas, april 17 -
    </dateline>qtly div 70 cts vs 70 cts prior
    pay june eight
    record may eight  
 reuter
</text>]