[<text>
<title>pacific lighting corp &lt;plt&gt; sets qtly div</title>
<dateline>    los angeles, june 2 -
    </dateline>shr 87 cts vs 87 cts prior
    pay august 14
    record july 20
 reuter
</text>]