[<text>
<title>vicon industries &lt;vii&gt; votes staggered board</title>
<dateline>    melville, n.y., march 26 - </dateline>vicon industries inc said its
shareholders approved an amendment to divide its nine-member
board into three classes with staggered tenures.
    it also said shareholders expanded the indemnification
rights of the company's directors and officers.
    both actions were taken at the company's annual
shareholders meeting, vicon said.
 reuter
</text>]