[<text>
<title>stanline inc &lt;stan&gt; regular dividend</title>
<dateline>    norwalk, calif., april 3 -
    </dateline>qtly div eight cts vs eight cts in prior qtr
    payable april 30
    record april 15
    company said its board intends to declare cash dividends
quarterly and plans to pay a five pct stock dividend annually
following the close of each fiscal year. the initial five pct
stock dividend was paid december 22 to holders of record
november 30.
 reuter
</text>]