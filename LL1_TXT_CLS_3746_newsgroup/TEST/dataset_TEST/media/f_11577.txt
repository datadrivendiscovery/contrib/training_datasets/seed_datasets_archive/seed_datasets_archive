[<text>
<title>itt's &lt;itt&gt;sheraton to add 34 properties in year</title>
<dateline>    boston, march 31 - </dateline>itt corp's sheraton corp subsidiary said
during 1987 it plans to add 34 properties, down from 40 in
1986.
    it said revenues of owned, leased, managed and franchised
properties in 1987 were 3.6 billion dlrs, up from 3.3 billion
dlrs in 1985.
 reuter
</text>]