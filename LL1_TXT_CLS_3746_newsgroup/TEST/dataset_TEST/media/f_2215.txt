[<text>
<title>sapporo breweries issues five year sfr notes</title>
<dateline>    zurich, march 5 - </dateline>sapporo breweries ltd of japan is issuing
100 mln swiss francs of five year notes with a 4-5/8 pct coupon
and 100-1/4 issue price, lead manager swiss bank corp said.
    the issue is guaranteed by fuji bank.
    payment is due march 17.
 reuter
</text>]