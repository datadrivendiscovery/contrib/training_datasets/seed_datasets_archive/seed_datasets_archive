[<text>
<title>reagan sends int'l rubber agreement to senate</title>
<dateline>    washington, oct 20 - </dateline>president reagan sent the five-year
international natural rubber agreement to the senate for
approval.
    reagan said the accord is designed to stabilize rubber
prices without disturbing long-term market trends and to foster
expanded natural rubber supplies at reasonable prices.
    it continues a buffer stock of not more than 550,000 tonnes
established by a 197agreement. this will be used to defend a
regularly adjusted price range and will be financed equally by
importing and exporting members of the agreement.
 reuter
</text>]