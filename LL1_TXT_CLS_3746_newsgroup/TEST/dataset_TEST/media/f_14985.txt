[<text>
<title>&lt;first mercantile currency fund inc&gt; 1st qtr net</title>
<dateline>    toronto, april 8 -
    </dateline>shr profit 63 cts vs 22 cts
    net 775,868 vs 276,446
    revs 2,255,742 vs 706,130
 reuter
</text>]