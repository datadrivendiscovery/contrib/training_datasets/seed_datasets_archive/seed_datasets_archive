[<text>
<title>new generation foods &lt;ngen&gt; sells shares</title>
<dateline>    new york, march 26 - </dateline>new generation foods inc said 17
warrant holders have exercised 1,032,384 warrants, acquiring
2,064,768 new generation shares for 1,032,384 dlrs.
    the company said president jerome s. flum and flum
partners, which he controls, acquired 307,138 and 940,680
shares respectively through the exercise of the warrants.  it
said it also issued another 44,268 common shares on the
exercise of 132,812 warrants in a separate non-cash exchange.
    new generation now has about 161,000 warrants still
outstanding.
    new generation also said it has retained howard saltzman,
flum partners limited partner, to serve as a consultant for one
year.
    it said saltzer will work closely with flum and play a
major role in the day-to-day management of new generation.
 reuter
</text>]