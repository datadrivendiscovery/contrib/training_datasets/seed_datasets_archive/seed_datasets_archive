[<text>
<title>traders detail irish intervention barley tender</title>
<dateline>    london, march 5 - </dateline>the european commission authorised the
export of 33,500 tonnes of irish intervention barley at today's
tender for non-european community destinations at 53.10 ecus
per tonne, grain traders said.
 reuter
</text>]