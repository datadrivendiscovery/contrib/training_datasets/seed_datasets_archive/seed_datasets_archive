[<text>
<title>masco corp &lt;mas&gt; regular dividend set</title>
<dateline>    taylor, mich., march 12 -
    </dateline>qtly div nine cts vs nine cts previously
    pay may 11
    record april 17
 reuter
</text>]