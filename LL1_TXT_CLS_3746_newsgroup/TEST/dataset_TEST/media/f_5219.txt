[<text>
<title>pearson plc &lt;pson.l&gt; yr ended dec 31</title>
<dateline>    london, march 16 -
    </dateline>shr 37.4p vs 30p.
    final div 7p, making 12p vs 10p.
    pre-tax profit 121.1 mln stg vs 109.3 mln.
    net profit before minorities 76.6 mln stg vs 62.8 mln.
    turnover 952.6 mln vs 970.1 mln.
    pre-interest profit 132.1 mln vs 124.6 mln.
    net interest 11 mln vs 15.3 mln.
    tax 44.5 mln vs 46.5 mln.
    minority interests 3.1 mln vs 5.2 mln.
    extraordinary debit 9.1 mln vs credit 11.5 mln.
    note - extraordinary debit reflected full provision for
discontinuing the financial times's printing operations at
bracken house in 1988, partly offset by gains on disposals.
 reuter
</text>]