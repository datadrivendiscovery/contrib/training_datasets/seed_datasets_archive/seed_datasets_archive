[<text>
<title>standard microsystems corp &lt;smsc.o&gt; 4th qtr net</title>
<dateline>    hauppauge, n.y., april 13 - </dateline>feb 28 end
    shr profit four cts vs loss nil
    net profit 448,000 vs loss 28,000
    revs 15.1 mln vs 11.5 mln
    avg shrs 11.2 mln vs 11.1 mln
    year
    shr profit four cts vs profit nil
    net profit 459,000 vs profit 51,000
    revs 53.2 mln vs 44.5 mln
    avg shrs 11.2 mln vs 11.1 mln
    note: net includes tax credits of 53,000 dlrs vs 1,023,000
dlrs in quarter and 48,000 dlrs vs 2,557,000 dlrs in year.
 reuter
</text>]