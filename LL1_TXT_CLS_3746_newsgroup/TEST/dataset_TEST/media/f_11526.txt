[<text>
<title>quest biotechnology &lt;qbio&gt; unit in merger pact</title>
<dateline>    detroit, march 31 - </dateline>quest biotechnology inc said its new
subsidiary, quest blood substitute inc, signed the agreement
and plan of merger with hunt research corp and its affiliate,
icas corp.
    it said quest blood expects to complete the merger within
the next several weeks. terms were not disclosed.
    in a related transaction, quest blood said it executed an
agreement with alza corp, which will make alza a preferred
shareholder of quest blood and offer alza the right to acquire
a total equity position of up to 25 pct of quest blood in
exchange for acquisition of patent rights to alza technology.
 reuter
</text>]