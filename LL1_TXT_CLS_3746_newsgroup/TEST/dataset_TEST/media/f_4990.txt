[<text>
<title>desoto inc &lt;dso&gt; regular dividend set</title>
<dateline>    des plaines, ill., march 13 -
    </dateline>qtly div 35 cts vs 35 cts previously
    pay april 17
    record march 31
 reuter
</text>]