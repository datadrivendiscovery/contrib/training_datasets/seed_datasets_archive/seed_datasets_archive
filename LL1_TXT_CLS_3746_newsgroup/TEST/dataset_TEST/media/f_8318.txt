[<text>
<title>&lt;canada lease financing ltd&gt; 3rd qtr dec 31 net</title>
<dateline>    toronto, march 23 -
    </dateline>shr 30 cts vs 12 cts
    net 727,000 vs 266,000
    revs 27.8 mln vs 21.1 mln
    nine mths
    shr 59 cts vs 48 cts
    net 1,355,000 vs 1,098,000
    revs 69.4 mln vs 59.1 mln
 reuter
</text>]