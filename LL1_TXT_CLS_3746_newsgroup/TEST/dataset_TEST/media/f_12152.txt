[<text>
<title>ico talks set to end with no quota debate</title>
<dateline>    london, april 1 - </dateline>the international coffee organization
executive board meeting will end tomorrow without any move to
reopen the debate on restoring coffee export quotas, delegates
said.
    talks have focused on administrative matters and
verification of stocks in producer countries, they said.
    producers met briefly today to exchange views on the market
situation but there seems little chance discussion on quotas
will begin much before the ico's annual council session in
september, they said.
    delegates earlier thought the meeting would end tonight,
but a further session is scheduled tomorrow at 1030 gmt to
complete reports on stock verification.
    meantime, the executive board will meet may 12 to discuss
possible action on the consultancy report on the ico presented
today to the board, consumer delegates said.
 reuter
</text>]