[<text>
<title>telxon &lt;txln&gt; sees slightly higher 4th qtr net</title>
<dateline>    akron, ohio, march 3 - </dateline>telxon corp said it expects
per-share earnings for its fourth quarter ending march 31 to be
about 23 cts to 26 cts on revenues of 26 mln dlrs to 28 mln
dlrs.
    for the year-ago quarter, the company earned 22 cts a
share, adjusted for a three-for-two stock split, on revenues of
24.2 mln dlrs.
    the company said it made the earnings estimate in response
to analysts' forecasts, which it said called for per-share
earnings of 24 cts to 30 cts and revenues of 29 mln dlrs to 32
mln dlrs.
 reuter
</text>]