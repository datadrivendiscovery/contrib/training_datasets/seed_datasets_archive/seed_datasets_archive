[<text>
<title>shell canada &lt;shc&gt; raises crude 32 cts canadian</title>
<dateline>    new york, june 18 - </dateline>shell canada &lt;shc&gt;, whose majority
interest is owned by the royal dutch/shell group of companies,
raised the postings of light sweet and sour crude oil from
edmonton/swann hills 32 canadian cts a barrel, effective today.
    the new price for light sweet crude oil is 25.60 canadian
dlrs a barrel while the new price for light sweet sour crude is
24.08 canadian dlrs a barrel.
    the royal dtuch/shell group owns 72 pct of shell canada and
public shareholders, primarily canadian, own the remaining 28
pct of the company, a shell canada spokesman said.
 reuter
</text>]