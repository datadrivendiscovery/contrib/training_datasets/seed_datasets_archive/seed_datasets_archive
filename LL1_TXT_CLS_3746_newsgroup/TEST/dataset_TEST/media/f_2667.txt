[<text>
<title>national security insurance co &lt;nsic&gt; 4th qtr</title>
<dateline>    elba, ala., march 6 -
    </dateline>oper shr loss 15 cts vs profit 57 cts
    oper net loss 151,000 vs profit 570,000
    year
    oper shr profit 2.08 dlrs vs loss 12 cts
    oper net profit 2,122,000 vs loss 127,000
    note: net excludes realized capital loss 19,000 dlrs vs
gain 896,000 dlrs in quarter and gains 1,646,000 dlrs vs
1,331,000 dlrs in year.
    1986 net both periods excludes tax credit 1,288,000 dlrs.
 reuter
</text>]