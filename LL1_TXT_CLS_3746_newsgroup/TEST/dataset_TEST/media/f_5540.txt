[<text>
<title>integrated circuits &lt;ictm&gt; sets record date</title>
<dateline>    redmond, wash., march 16 - </dateline>integrated circuits inc said it
set march 17 as the record date for its previously announced 10
pct stock dividend.
    the company said it will distribute the dividend on march
31.
 reuter
</text>]