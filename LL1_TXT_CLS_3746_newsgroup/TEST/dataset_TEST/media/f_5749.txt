[<text>
<title>wings west airlines &lt;wing&gt; 3rd qtr net</title>
<dateline>    san luis obispo, calif., march 16 - </dateline>qtr ended jan 31
    shr one ct vs nine cts
    net 29,000 vs 349,000
    revs 7,892,000 vs 4,721,000
    note: nine mth data unavailable
    .
 reuter
</text>]