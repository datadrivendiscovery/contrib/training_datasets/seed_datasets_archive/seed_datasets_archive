[<text>
<title>franklin ohio insured tax-free sets payout</title>
<dateline>    san mateo, calif., march 2 -
    </dateline>mthly div 6.1 cts vs 6.1 cts prior
    pay march 31
    record march 16
    note: franklin ohio insured tax-free income fund.
 reuter
</text>]