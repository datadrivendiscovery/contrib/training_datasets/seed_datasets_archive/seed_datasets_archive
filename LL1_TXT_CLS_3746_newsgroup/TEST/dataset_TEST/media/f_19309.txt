[<text>
<title>winthrop insured mortgage &lt;wmi&gt; sets payout</title>
<dateline>    boston, july 19 -
    </dateline>qtly div 35 cts vs 35 cts prior
    pay july 15
    record june 30
    note: full name is winthrop insured mortgage investors ii
 reuter
</text>]