[<text>
<title>federal express &lt;fdx&gt; pursues overseas route</title>
<dateline>    memphis, march 4 - </dateline>federal express corp said it received a
recommendation from an administrative law judge that it be
awarded the exclusive air express route between the u.s. and
japan.
    federal express said the recommendation now must be
approved by the secretary of transporation elizabeth dole and
president reagan.
    federal express said it received late last year preliminary
approval by a panel over a consortium of united parcel service
and dhl, and other air express competitors.
 reuter
</text>]