[<text>
<title>lone star industries &lt;lce&gt; promisary notes</title>
<dateline>    new york, march 27 - </dateline>lone star industries inc said it has
arranged 150 mln dlrs of financing through the issuance of
promisary notes to a consortium of 22 lenders.
    lone star said the 8.75 pct notes will mature on march 26,
1992. the proceeds will be used to refinance and retire debt,
and other general corporate purposes, the company said.
 reuter
</text>]