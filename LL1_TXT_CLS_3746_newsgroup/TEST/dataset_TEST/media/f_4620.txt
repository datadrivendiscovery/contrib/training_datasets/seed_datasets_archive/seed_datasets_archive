[<text type="unproc">
chock full o'nuts corp 2nd qtr jan 31 loss
    new york, march 12
    oper shr loss 38 cts vs profit 24 cts
    oper net loss 2,243,000 vs profit 1,192,000
    revs 43.6 mln vs 41.0 mln
    six mths
    oper shr loss 35 cts vs profit 28 cts
    oper net loss 2,090,000 vs profit 1,399,000
    revs 84.3 mln vs 72.9 mln
    note: year-ago oper excludes loss from discontinued
operations of 1,028,000 dlrs for qtr and 1,076,000 dlrs for six
mths.
 reuter


</text>]