[<text>
<title>copley properties inc &lt;cop&gt; increases dividend</title>
<dateline>    boston, mass, april 3 -
    </dateline>qtly div 42 cts vs 41.5 cts prior
    payable april 28
    record april 14
 reuter
</text>]