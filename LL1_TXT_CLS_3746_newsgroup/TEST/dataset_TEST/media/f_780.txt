[<text>
<title>republic automotive parts &lt;raut&gt; 4th qtr loss</title>
<dateline>    brentwood, tenn., march 2 -
    </dateline>shr loss 85 cts vs loss 88 cts
    net loss 2,410,000 vs loss 2,466,0000
    revs 24.0 mln vs 23.9 mln
    year
    shr loss 1.18 dlrs vs loss 81 cts
    net loss 3,338,000 vs loss 2,275,000
    revs 101.4 mln vs 112.3 mln
 reuter
</text>]