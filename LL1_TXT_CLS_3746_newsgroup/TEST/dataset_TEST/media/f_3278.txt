[<text>
<title>lmfe to concentrate on cash settled contracts</title>
<dateline>    london, march 9 - </dateline>the london meat futures exchange, lmfe,
will cease trading in its deliverable pigmeat contract from
april 3, the lmfe said.
    the move will enable the exchange to concentrate on the
cash settled contracts introduced last year.
    "this allows the exchange and its members to concentrate our
marketing on the pig and live cattle contracts," lmfe official
peter freeman said. "these two have already shown their
effectiveness for hedging, and the trade in both contracts is
increasing," he added.
    "using the futures market allows better planning and forward
price fixing that our industry needs," chairman pat elmer said.
    some 131 pig contracts were settled against the meat and
livestock commission's average all pigs price in february,
which represents hedging for over 13,000 pigs, more than twice
the number cash-settled in january, the lmfe said.
    cash settlement on the new cattle and pig contracts was
introduced last june in the expectation that the appeal of the
futures market to farmers, abattoirs and users of meat would
increase because of the absence of a delivery requirement.
 reuter
</text>]