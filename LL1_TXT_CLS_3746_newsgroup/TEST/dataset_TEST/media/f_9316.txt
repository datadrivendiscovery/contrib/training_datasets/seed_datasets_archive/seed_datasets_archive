[<text>
<title>russ togs inc &lt;rts&gt; 4th qtr jan 31 net</title>
<dateline>    new york, march 25 -
    </dateline>shr 82 cts vs 76 cts
    net 4,200,000 vs 3,954,000
    sales 58.7 mln vs 60.6 mln
    year
    oper shr 2.68 dlrs vs 2.47 dlrs
    oper net 13.8 mln vs 13.0 mln
    sales 274.3 mln vs 276.8 mln
    note: prior year net excludes loss 1,120,000 dlrs from
discontinued operations and loss on disposal of 922,000 dlrs.
 reuter
</text>]