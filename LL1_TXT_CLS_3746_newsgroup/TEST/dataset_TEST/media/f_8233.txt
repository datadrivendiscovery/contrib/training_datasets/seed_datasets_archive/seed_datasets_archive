[<text>
<title>hewlett-packard &lt;hwp&gt; begins test shipments</title>
<dateline>    new york, march 23 - </dateline>hewlett-packard co said it began
limited customer shipments of its hp 3000 series 930 business
computer and remains on schedule for first commercial shipments
in mid-1987.
    shipments of the higher-performance series 950 business
computers are scheduled for the second half of this year, it
said.
    in september, the company postponed initial shipments of
the series 930 computer until mid-1987 because performance of
the operating software system was below target.
 reuter
</text>]