[<text>
<title>u.s. to allow temporary imports of s.a. uranium</title>
<dateline>    washington, march 9 - </dateline>the treasury department said it would
temporarily permit imports of south african uranium ore and
oxide pending clarification of anti-apartheid sanctions laws
passed by congress last fall.
    the decision was announced late friday. it applies, until
july 1, to uranium ore and oxide imported into the u.s. for
processing and re-export to third countries.
    the treasury said it took the action because it felt that
when congress passed the comprehensive south african sanctions
bill last fall over president reagan's veto it had not intended
to hurt u.s. industry.
    in addition, the treasury said it would permit u.s.-made
goods to be imported temporarily from south african
state-controlled organizations for repair or servicing.
   
 reuter
</text>]