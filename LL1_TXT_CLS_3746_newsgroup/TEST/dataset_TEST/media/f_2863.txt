[<text>
<title>lockheed &lt;lk&gt; gets 80.0 mln dlr contract</title>
<dateline>    washington, march 6 - </dateline>lockheed corp has received an 80.0
mln dlr contract for engineering support services for high
energy laser facilities, the defense department said.
 reuter
</text>]