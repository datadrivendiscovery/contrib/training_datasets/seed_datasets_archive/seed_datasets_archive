[<text>
<title>united artists &lt;uaci.o&gt; suspends merger talks</title>
<dateline>    denver, oct 20 - </dateline>united artists communications inc and
&lt;united cable television corp&gt; said they have indefinitely
suspended negotiations on a proposed merger of their companies
previously announced.
    the companies cited a combination of extraordinary market
conditions and unresolved terms of the merger as contributing
to the action.
 reuter
</text>]