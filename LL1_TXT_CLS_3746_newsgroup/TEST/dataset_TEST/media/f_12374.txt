[<text>
<title>novamin board to recommend breakwater &lt;bwrlf&gt; bid</title>
<dateline>    vancouver, british columbia, april 1 - </dateline>&lt;novamin inc&gt; said
its board will recommend to shareholders the takeover offer
made by breakwater resources ltd.
    breakwater is offering to exchange one breakwater share for
each two shares of novamin, the company said.
    breakwater will issue about 3.7 mln shares for all novamin
shares presently issued and outstanding. additional shraes of
breakwater will be issued if warrants and options of novamin
are exercised.
 reuter
</text>]