[<text>
<title>wall and redekop corp &lt;wrk.to&gt; three mths net</title>
<dateline>    vancouver, june 29 -
    </dateline>shr five cts vs one ct
    net 299,869 vs 54,775
    revs 5.7 mln vs 5.2 mln
    note: period ended april 30.
 reuter
</text>]