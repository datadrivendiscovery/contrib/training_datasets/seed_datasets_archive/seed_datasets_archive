[<text>
<title>smithkline &lt;skb&gt; opens plant in china</title>
<dateline>    philadelphia, oct 19 - </dateline>smithkline beckman corp said it
opened an 8.5 mln dlr pharmaceutical manufacturing plant in
tianjin, china.
    the plant will make pharmaceutical products for the joint
venture company, tianjin smith kline and french laboratories.
    smithkline said the joint venture combines smith kline and
french laboratories, the pharmaceutical unit of smithkline
beckman, the &lt;tianjin medical co&gt;, and &lt;hebel chemical plant&gt;
in tianjin.
    the plant has the capability to produce one billion tablets
and 200 mln capsules a year.
 reuter
</text>]