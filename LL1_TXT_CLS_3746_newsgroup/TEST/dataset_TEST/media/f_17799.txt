[<text>
<title>ltv &lt;qltv&gt; gets contract</title>
<dateline>    dallas, june 1 - </dateline>ltv corp's ltv aircraft products group
said it was awarded a contract valued at more than 10 mln dlrs
to continue production of engine covers and thrust reversers
for the challenger 601-3a executive business jet.
    the aircraft is manufactured by canadair ltd. of montreal,
it said.

 reuter
</text>]