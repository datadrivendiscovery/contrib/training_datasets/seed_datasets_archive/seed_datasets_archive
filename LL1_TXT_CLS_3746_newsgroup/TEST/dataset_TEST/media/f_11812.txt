[<text>
<title>u.k. merger clearances</title>
<dateline>    london, april 1 - </dateline>the secretary of state for trade and
industry said he had decided not to refer the proposed
acquisition by reed international plc &lt;reed.l&gt; of &lt;technical
publishing company inc&gt; to the monopolies and mergers
commission.
    the proposed acquisition by &lt;rosehaugh plc&gt; of &lt;the general
funds investment trust plc&gt; was also cleared.
 reuter
</text>]