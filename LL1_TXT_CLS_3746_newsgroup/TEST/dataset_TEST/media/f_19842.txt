[<text>
<title>arcturus inc &lt;artu&gt; launches new megabeam</title>
<dateline>     acton, mass., june 29 - </dateline>arcturus inc said it introduced it
megabeam product, a three-tube high resolution data display
projector.
    the company said the large screen projector is more than
twice as bright as competing models.
    it said the projector would be used to show the image of a
14-inch computer terminal to large groupd on a six-foot wall
screen.
    it said the megabeam is priced at 11,995 dlrs per unit.
 reuter
</text>]