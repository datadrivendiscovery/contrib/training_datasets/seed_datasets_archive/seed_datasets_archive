[<text>
<title>cri insured mortgage investments lp &lt;crm&gt; payout</title>
<dateline>    rockville, md., march 20 -
    </dateline>mthly div 16-1/2 cts vs 16-1/2 cts prior
    pay may 15
    record march 31
 reuter
</text>]