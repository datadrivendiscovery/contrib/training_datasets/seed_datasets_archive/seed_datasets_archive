[<text>
<title>atlas consolidated mining &lt;acmb&gt; 4th qtr</title>
<dateline>    manila, philippines, april 8 -
    </dateline>shr loss 17.3 cts vs 21.5 cts
    net loss 14.5 mln vs loss 18.0 mln
    revs 27.3 mln vs 23.7 mln
    year
    shr loss 58 cts vs loss 1.01 dlrs
    net loss 48.3 mln vs loss 84.2 mln
    revs 111.7 mln vs 141.9 mln
 reuter
</text>]