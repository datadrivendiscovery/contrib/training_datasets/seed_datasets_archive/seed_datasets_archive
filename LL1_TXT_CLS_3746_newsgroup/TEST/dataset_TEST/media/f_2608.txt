[<text>
<title>residential mortgage investments inc &lt;rmi&gt;payout</title>
<dateline>    fort worth, texas, march 6 -
    </dateline>qtly div 24 cts vs 24 cts prior
    pay april 10
    record march 31
 reuter
</text>]