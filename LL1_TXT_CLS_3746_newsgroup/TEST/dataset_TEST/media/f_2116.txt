[<text>
<title>pepsico &lt;pep&gt; upgraded by kidder peabody</title>
<dateline>    new york, march 5 - </dateline>kidder peabody and co analyst roy burry
issued a strong buy recommendation on pepsico inc, citing an
improved profit outlook for both domestic soft drinks and
frito-lay snack foods.
    pepsico climbed 7/8 to 34-3/4 on 615,000 shares by
midmorning.
    burry forecast earnings of 2.00 dlrs per share in 1987 and
2.30 dlrs in 1988. in 1986 the company earned 1.75 dlrs per
share. burry previously had a sell recommendation on pepsico.
"we're looking at 10 to 15 pct earnings growth for frito-lay in
1987, a trend that should continue through the decade."
 reuter
</text>]