[<text>
<title>texscan reaches reorganization agreement</title>
<dateline>    phoenix, ariz., march 30 - </dateline>&lt;texscan corp&gt;, which filed for
bankruptcy in november 1985, said it reached an agreement in
principle with its lending bank group on a joint reorganization
plan.
    the company said it expects to file the plan within the
next 10 days.
    additional detail was not provided.
 reuter
</text>]