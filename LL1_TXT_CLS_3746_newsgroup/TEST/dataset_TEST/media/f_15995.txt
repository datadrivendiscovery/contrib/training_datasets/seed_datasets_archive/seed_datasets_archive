[<text>
<title>jacor &lt;jcor&gt; to buy two denver radio stations</title>
<dateline>    cincinnati, april 9 - </dateline>jacor communications inc said it
agreed to buy two denver radio stations from a.h. belo corp
&lt;blc&gt; for 24 mln dlrs in cash and notes.
    jacor said the two stations are koa-am and koaq-fm.
    the acquisitions must be approved by the federal
communications commission, jacor added.
 reuter
</text>]