[<text>
<title>amex seat sells for record 380,000 dlrs</title>
<dateline>    new york, april 9 - </dateline>the american stock exchange said a seat
on the exchange sold for 380,000 dlrs, a record price that is
5,000 dlrs higher than the previous sale on feb 17.
    the current bid is 330,000 dlrs and the offer, 425,000
dlrs, the exchange said.
 reuter
</text>]