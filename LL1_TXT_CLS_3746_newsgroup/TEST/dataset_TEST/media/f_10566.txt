[<text>
<title>doskocil &lt;dosk&gt; shareholders vote reverse split</title>
<dateline>    hutchinson, kan., march 27 - </dateline>doskocil cos inc said its
shareholders approved a one-for-10 reverse stock split, which
is expected to become effective by april 30.
    the company said the reverse split will reduce the its
outstanding shares to about six mln from 60 mln.
 reuter
</text>]