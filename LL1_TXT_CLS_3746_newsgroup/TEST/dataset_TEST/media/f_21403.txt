[<text>
<title>home savings bank &lt;hmsb.o&gt; 3rd qtr net</title>
<dateline>    new york, oct 19 -
    </dateline>shr 57 cts
    net 6,889,000 vs 10.7 mln
    nine mths
    shr 1.67 dlrs
    net 20.1 mln vs 22.9 mln
    assets 1.63 billion vs 1.47 billion
    deposits 1.17 billion vs 1.19 billion
    loans 1.25 billion vs 936.5 mln
    note: 3rd qtr and nine mths 1986 per share figures not
available because bank converted to stock form nov 28, 1986.
 reuter
</text>]