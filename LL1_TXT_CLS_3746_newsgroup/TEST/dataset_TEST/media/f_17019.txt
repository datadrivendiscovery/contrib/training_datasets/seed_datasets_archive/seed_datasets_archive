[<text>
<title>csx &lt;csx&gt; unit chairman retires</title>
<dateline>    white sulphur springs, w. va., april 21 - </dateline>csx corp said
that joseph abely, chairman and chief executive officer of
csx's sea-land corp unit, will retire by the end of the month.
    the company said he will be replaced by robert hintz,
executive vice president of csx and president and chief
executive officer of the company's energy and properties
groups.
 reuter
</text>]