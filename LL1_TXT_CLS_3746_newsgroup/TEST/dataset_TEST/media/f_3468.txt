[<text>
<title>bejam group plc &lt;bjam.l&gt; 27 weeks to january 3</title>
<dateline>    london, march 11 -
    </dateline>shr 5.95p vs 4.41p
    div 2.25p vs 2.0p
    pre-tax profit 11.6 mln vs 9.1 mln
    tax 4.2 mln vs 3.6 mln
    turnover 256.3 mln vs 185.3 mln
    note - company said it was unlikely second-half profits
will show same rate of increase as first. but it had great
confidence in prospects for future growth.
 reuter
</text>]