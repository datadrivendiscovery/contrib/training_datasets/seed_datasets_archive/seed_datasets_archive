[<text>
<title>united jersey banks &lt;ujb&gt; to merge two banks</title>
<dateline>    princeton, n.j., march 19 - </dateline>united jersey banks said it
plans to merge two of its southern new jersey member banks into
one 704 mln dlr organization.
    it said united jersey bank/fidelity bank, based in
pennsauken, will be merged into united jersey bank/south in
order to improve customer service and maximize efficiency
throughout the southern new jersey markets.
    raymond silverstein, currently chairman of the board of
united jersey bank/fidelity, will be chairman of the merged
bank, the company said.
 reuter
</text>]