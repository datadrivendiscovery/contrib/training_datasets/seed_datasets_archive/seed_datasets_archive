[<text>
<title>bluefield supply &lt;bfld&gt; in liquidating payout</title>
<dateline>    bluefield, w. va., march 16 - </dateline>bluefield supply co said its
board declared its second liquidating dividend of 1.71 dlrs per
share, payable march 16 to shareholders of record march 13.
    the company paid an initial liquidating dividend of 15.75
dlrs per share on january eight.
 reuter
</text>]