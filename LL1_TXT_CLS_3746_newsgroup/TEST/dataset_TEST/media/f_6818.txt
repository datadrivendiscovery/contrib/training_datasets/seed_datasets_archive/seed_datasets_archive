[<text>
<title>pilgrim venture in merger agreement</title>
<dateline>    fort lauderdale, fla., march 18 - </dateline>&lt;pilgrim venture corp&gt;
said it signed a letter of intent to merge with &lt;marketing
technologies group inc&gt;, rockville center, n.y.
    under terms of the agreement, pilgrim, a publicly held
corporation, said it will issue two mln shares of authorized
but unissued restricted common stock to marketing technologies
shareholders. the company said it expects to complete the
merger by june 17.
    marketing technologies is developing a computer-based
advertising system geared toward large advertisers, the company
said.
 reuter
</text>]