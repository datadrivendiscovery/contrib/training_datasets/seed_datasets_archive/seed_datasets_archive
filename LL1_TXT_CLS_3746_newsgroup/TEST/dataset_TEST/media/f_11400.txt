[<text>
<title>new harding group sets first payout since 1978</title>
<dateline>    brantford, ontario, march 31 - </dateline>&lt;new harding group inc&gt;,
formerly harding carpets ltd, said it declared its first
dividend since fiscal 1978 of 10 cts per subordinate voting
share and multiple voting share, pay april 15, record april 8.
    the company said the dividend establishes a new policy for
the payment of quarterly dividends.
    new harding earlier reported profit of 653,000 dlrs or 19
cts a share for first quarter ended january 31. it said prior
results were not comparable due to the company's november, 1986
acquisition of a 57 pct stake in continuous colour coat ltd.
 reuter
</text>]