[<text>
<title>century business credit &lt;cpy&gt; to make statement</title>
<dateline>    new york, april 1 - </dateline>century business credit corp said it
will make an announcement concerning its delay on the american
stock exchange this morning.
    president andrew tananbaum said the announcement will
follow a regularly-scheduled board meeting.
   
 reuter
</text>]