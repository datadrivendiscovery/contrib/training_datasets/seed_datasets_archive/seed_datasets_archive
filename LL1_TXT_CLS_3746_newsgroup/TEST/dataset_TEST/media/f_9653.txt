[<text>
<title>first federal/arkansas &lt;fark&gt; in court ruling</title>
<dateline>    little rock, ark., march 25 - </dateline>first federal savings of
arkansas fa said a federal district court denied a motion
seeking class-action status for a lawsuit that alleges the bank
violated securities laws in an offering of common stock.
    the lawsuit alleges that first federal violated securities
laws in connection with its initial public offering.
 reuter
</text>]