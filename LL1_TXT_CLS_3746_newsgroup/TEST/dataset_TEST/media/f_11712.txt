[<text>
<title>itt &lt;itt&gt;, alcatel settle final payments</title>
<dateline>    new york, march 31 - </dateline>itt corp said it received a final 400
mln dlr payment for its telecommunications joint venture with
&lt;alcatel nv&gt;, settling all accounts in the venture.
    the company had previously said it was due to receive the
400 mln dlr payment.
    alcatel nv was formed last year when itt and &lt;cie generale
d'electricite&gt; of france merged their telecommunications
business. itt received a 37 pct interest in alcatel and 1.3
billion dlrs in cash, including today's payment.
 reuter
</text>]