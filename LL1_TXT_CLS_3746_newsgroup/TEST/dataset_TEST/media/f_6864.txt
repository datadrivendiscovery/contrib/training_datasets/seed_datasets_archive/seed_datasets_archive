[<text>
<title>metro mobile &lt;mmct&gt; declares stock dividend</title>
<dateline>    new york, march 18 - </dateline>metro mobile cts inc said it declared
a 10 pct stock dividend.
    the dividend will be distributed on april 13 to holders of
record march 30.
   
 reuter
</text>]