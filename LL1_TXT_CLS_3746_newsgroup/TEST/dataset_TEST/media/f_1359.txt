[<text>
<title>florida public &lt;fput&gt; splits stock, ups div</title>
<dateline>    west palm beach, fla, march 3 - </dateline>florida public utilities co
said its board declared a three-for-two stock split on its
common stock.
    it said holders of record april 15 will receive one
additional share may one for each two shares held.
    the company also said it raised the dividend on its common
stock by two cts to 33 cts a share on a pre-split basis.
    the dividend is payable april one to holders of record
march 18.
 reuter
</text>]