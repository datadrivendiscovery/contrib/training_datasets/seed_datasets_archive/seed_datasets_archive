[<text>
<title>wilson brothers completes sale</title>
<dateline>    miami, fla., march 31 - </dateline>wilson brothers said it completed
the sale of most of the assets of its enro shirt co inc,
enro-at-ease inc and foxcroft shirt ltd subsidiaries to enro
acquisition corp for about 24.2 mln dlrs, half in cash and half
in subordinated promissory notes.
    enro acquisition is a newly formed corporation and owns
ramar intercapital corp and wilson's chief operating officer,
v. jerome kaplan, and other managers.
    enro acquisition also assumed most of the units'
liabilities including a 6.2 mln dlrs term loan.
    completion of the sale and recent sales of substantially
all the assets of the company's 50 pct owned affiliate gmw
industries inc, are expected to result in a net gain of about
nine mln dlrs in the first quarter of 1987.
    for the year ago first quarter, wilson reported net income
of 28,000 dlrs, including a 103,000 dlrs credit, on sales of
15.8 mln dlrs.
 reuter
</text>]