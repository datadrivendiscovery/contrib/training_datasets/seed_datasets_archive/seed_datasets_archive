[<text>
<title>scan-graphics, captive venture capital merge</title>
<dateline>    broomall, pa., march 31 - </dateline>&lt;scan-graphics inc&gt; said it has
completed a merger with &lt;captive venture capital inc&gt; in which
former shareholders of scan-graphics have beome majority
shareholders of the merged company and the scan-graphics board
has been named the board of the merged company.
    it said the merged company is now known as scan-graphics
inc and expects to be listed on the nasdaq system soon.
    it said captive venture issued 1,600,000 restricted
preferred shares convertible into 16 mln common shares for
scan-graphics.  captive had 2,649,500 common shares outstanding
before the merger.
 reuter
</text>]