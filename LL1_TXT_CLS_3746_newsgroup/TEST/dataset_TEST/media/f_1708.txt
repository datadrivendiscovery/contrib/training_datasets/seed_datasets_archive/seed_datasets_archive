[<text>
<title>e-systems inc &lt;esy&gt; sets quarterly</title>
<dateline>    dallas, march 4 -
    </dateline>qtly div 12-1/2 cts vs 12-1/2 cts prior
    pay april one
    record march 13
 reuter
</text>]