[<text>
<title>syntech international &lt;syne.o&gt; sues southland</title>
<dateline>    reno, nev., oct 20 - </dateline>syntech international inc said it
initiated a suit in nevada district court against southland
corp &lt;sm&gt; for alleged breach of contract.
    syntech said its complaint alleges that southland failed to
make payments in connection with the purchase of 208 syntech
ticketing terminals.
    the company said some 3.8 mln dlrs is past due on the
contract.
 reuter
</text>]