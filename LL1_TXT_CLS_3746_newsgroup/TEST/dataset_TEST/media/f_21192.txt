[<text>
<title>international telecharge inc &lt;itil.o&gt; 3rd qtr</title>
<dateline>    dallas, oct 19 -
    </dateline>shr loss five cts vs loss eight cts
    net loss 657,000 vs loss 566,535
    revs 9,341,755 vs 260,468
    avg shrs 14,323,384 vs 7,081,688
    nine mths
    shr loss 19 cts vs loss 39 cts
    net loss 2,449,094 vs loss 1,408,789
    revs 15,571,230 vs 683,684
    avg shrs 12,655,172 vs 3,612,300
 reuter
</text>]