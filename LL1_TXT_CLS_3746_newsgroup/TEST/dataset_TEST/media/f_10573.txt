[<text>
<title>education systems &lt;espc&gt; year net</title>
<dateline>    belleville, n.j., march 27 - 
    </dateline>shr 27 cts vs 34 cts
    net 174,390 vs 222,720
    revs 4,948,622 vs 4,516,042
    note: current net includes non-recurring loss on
investments of 82,034.
    full name is education systems and publications corp.
 reuter
</text>]