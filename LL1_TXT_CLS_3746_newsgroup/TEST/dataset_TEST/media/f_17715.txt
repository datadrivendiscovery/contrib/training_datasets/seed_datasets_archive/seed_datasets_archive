[<text>
<title>&lt;grand union co&gt; sees higher year spending</title>
<dateline>    elmwood park, n.j., june 1 - </dateline>grand union co, a subsidiary
of generale occidentale sa of france, said it plans capital
spending of 111 mln dlrs this year mostly for new stores and
expansions of existing units, up from 76 mln dlrs for the year
ended march 28, 1987.
 reuter
</text>]