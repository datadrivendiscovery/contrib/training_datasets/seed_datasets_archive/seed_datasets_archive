[<text>
<title>derlan acquires 80 pct of aurora industries</title>
<dateline>    toronto, march 16 - </dateline>&lt;derlan industries ltd&gt; said it
acquired 80 pct of aurora industries inc of montgomery,
illinois for an undisclosed price.
    closing is subject to completion of legal formalities,
derlan said.
 reuter
</text>]