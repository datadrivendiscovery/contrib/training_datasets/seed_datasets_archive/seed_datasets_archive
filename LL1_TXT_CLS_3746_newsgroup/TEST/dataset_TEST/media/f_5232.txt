[<text>
<title>u.k. money market given 90 mln stg early help</title>
<dateline>    london, march 16 - </dateline>the bank of england said it provided the
money market with assistance worth 90 mln stg in response to an
early round of bill offers from discount houses.
    earlier, the bank estimated the shortage in the system
today at some 1.05 billion stg.
    the bank bought bills for resale to the market in equal
amounts on april 1, 2 and 3 at an interest rate of 10-7/16 pct.
 reuter
</text>]