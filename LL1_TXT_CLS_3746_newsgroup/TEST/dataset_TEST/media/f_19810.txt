[<text>
<title>british petroleum completes standard oil merger</title>
<dateline>    london, june 29 - </dateline>british petroleum company plc &lt;bp.l&gt; has
completed its merger with standard oil co of the u.s. &lt;srd&gt;,
raising its holding to 100 pct from 55 pct, bp said in a
statement.
    the acquisition was made through bp's wholly-owned
subsidiary &lt;bp america inc&gt;. bp took a 25 pct stake in standard
in 1970, raising its stake to 53 pct in 1978 and 55 pct in
1984.
    bp chairman sir peter walters will be chairman of bp
america, while robert horton is to be vice-chairman and chief
executive officer, with frank mosier as president.
    bp said further details would be released on july 21.
 reuter
</text>]