[<text>
<title>bally &lt;bly&gt; names new chief financial officer</title>
<dateline>    chicago, march 26 - </dateline>bally manufacturing corp said it has
elected paul johnson a vice president and named him chief
financial officer, replacing donald romans, who took early
retirement.
    bally also said it has appointed jerry blumenshine,
formerly a vice president, treasurer to fill the position left
vacant by johnson.
 reuter
</text>]