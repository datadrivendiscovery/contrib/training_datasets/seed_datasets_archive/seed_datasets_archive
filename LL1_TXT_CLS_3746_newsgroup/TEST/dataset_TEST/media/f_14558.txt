[<text>
<title>sun &lt;sun&gt; raises heating oil barge price</title>
<dateline>    new york, april 7 - </dateline>sun co's sun refining and marketing co
subsidiary said it raised the price it charges contract barge
customers for heating oil in new york harbor by 0.50 cent a
gallon, effective today.
    the increase brings the contract barge price to 50.50 cts a
gallon, sun said.
 reuter
</text>]