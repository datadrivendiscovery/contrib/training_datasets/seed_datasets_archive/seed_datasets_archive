[<text>
<title>newhall investment properties &lt;nip&gt; payout</title>
<dateline>    valencia, calif., april 9 -
    </dateline>shr 10 cts vs 10 cts prior qtr
    pay june one
    record april 24

 reuter
</text>]