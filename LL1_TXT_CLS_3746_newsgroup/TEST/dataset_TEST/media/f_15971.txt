[<text>
<title>national semi &lt;nsm&gt;, canon in development pact</title>
<dateline>    santa clara, calif., april 9 - </dateline>national semiconductor corp
said it and canon inc &lt;canny&gt; plan to jointly develop
integrated circuits and software products which will be
manufactured by canon.
    the first product will be a laser-beam printer for
national's series 32000 family of 32-bit microprocessor
products, the company said.
 reuter
</text>]