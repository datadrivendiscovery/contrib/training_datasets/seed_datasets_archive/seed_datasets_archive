[<text>
<title>digital &lt;dec&gt; settles suits with computer firm</title>
<dateline>    maynard, mass., march 31 - </dateline>digital equipment corp and
systems industries inc &lt;sysm&gt; said they settled all pending
litigation between the two companies.
    they said digital's 1980 patent infringement suit against
systems industries was dismissed, as was systems industries'
1986 antitrust suit against digital.
    the companies said systems industries will be licensed to
manufacture computer mass storage devices that use digital
technology.
    they also said the company will pay digital royalties in
settlement of digital's patent infringement claims.
 reuter
</text>]