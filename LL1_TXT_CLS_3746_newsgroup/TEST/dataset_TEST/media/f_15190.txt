[<text>
<title>dow jones and co inc &lt;dj&gt; 1st qtr net</title>
<dateline>    new york, april 8 -
    </dateline>shr 69 cts vs 64 cts
    net 66,719,000 vs 61,773,000
    rev 285.6 mln vs 259.7 mln
    note: earnings include after-tax gain of 29.4 mln dlrs, or
30 cts a share, versus after-tax gain of 31.4 mln dlrs, or 32
cts a share, for 1986 first quarter. earnings per share
reflects a 50 pct stock dividend in the form of a class b
common stock distribution on june 30, 1986.
   
 reuter
</text>]