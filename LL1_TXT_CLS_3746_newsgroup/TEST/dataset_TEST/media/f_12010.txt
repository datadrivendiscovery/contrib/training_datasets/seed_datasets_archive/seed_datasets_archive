[<text>
<title>freddie mac adjusts short-term discount rates</title>
<dateline>    washington, april 1 - </dateline>the federal home loan mortgage corp
adjusted the rates on its short-term discount notes as follows:
   maturity   rate       old rate  maturity
    32 days   6.00 pct   6.10 pct   1 day
  
 reuter
</text>]