[<text>
<title>phoenix steel receives offer for claymont mill</title>
<dateline>    claymont, del., march 4 - </dateline>&lt;phoenix steel corp&gt; said a group
of investors offered to buy its clayton steel plate mill for
eight mln dlrs and the assumption of a bond obligation.
   phoenix did not disclose the indentity of the investors.
    phoenix was forced to close the clayton mill last month.
    the company said the offer represents a major step in
restructuring the company.
 reuter
</text>]