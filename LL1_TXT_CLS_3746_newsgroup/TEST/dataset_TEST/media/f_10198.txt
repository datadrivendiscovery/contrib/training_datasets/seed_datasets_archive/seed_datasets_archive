[<text>
<title>acme-cleveland &lt;amt&gt; ceo to step down</title>
<dateline>    cleveland, march 26 - </dateline>b. charles ames, chairman of
acme-cleveland corp, said he will step down as chief executive
officer of the company.
    he will be replaced by david swift, who was elected to
serve as president and chief executive officer, effective april
1, the company said.
 reuter
</text>]