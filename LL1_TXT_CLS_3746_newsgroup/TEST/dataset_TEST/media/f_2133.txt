[<text>
<title>national gypsum co 4th qtr net</title>
<dateline>    dallas, march 5 -
    </dateline>net 5,521,000 vs na
    revs 358.1 mln vs 359.0 mln
    year
    net 55.3 mln vs na
    revs 1.43 billion vs 1.34 billion
    note: current year includes earnings of 49.6 mln dlrs for
the four months ended april 30, 1986. year-ago earnings not
comparable because of acquisition by aancor holdings inc on
april 29, 1986.
 reuter
</text>]