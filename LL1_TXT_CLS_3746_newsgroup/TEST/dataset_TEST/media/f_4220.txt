[<text>
<title>wiener enterprises inc &lt;wpb&gt; year jan 31 net</title>
<dateline>    harahan, la., march 12 -
    </dateline>shr 60 cts vs 85 cts
    qtly div 10 cts vs 10 cts prior
    net 1,407,000 vs 1,996,000
    sales 75.4 mln vs 58.2 mln
    note: dividend pay april 16, record april nine
 reuter
</text>]