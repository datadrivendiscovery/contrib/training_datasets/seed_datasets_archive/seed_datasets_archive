[<text>
<title>humana inc &lt;hum&gt; regular dividend</title>
<dateline>    louisville, ky., march 5 -
    </dateline>qtly div 19 cts vs 19 cts in prior qtr
    payable may one
    record april two
 reuter
</text>]