[<text>
<title>u.k. money market given 15 mln stg afternoon help</title>
<dateline>    london, march 18 - </dateline>the bank of england said it had provided
the money market with a further 15 mln stg assistance. this
brings the bank's total help so far today to 344 mln stg and
compares with the estimated shortage of around 1.3 billion stg.
    the central bank purchased bank bills outright, at the
lower dealing rates established this morning, comprising one
mln stg in band one at 9-7/8 pct and 14 mln stg in band two at
9-13/16 pct.
 reuter
</text>]