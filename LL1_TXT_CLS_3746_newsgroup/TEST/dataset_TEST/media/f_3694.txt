[<text>
<title>fed expected to add reserves</title>
<dateline>    new york, march 11 - </dateline>the federal reserve is expected to
intervene in the government securities market to add temporary
reserves via customer or system repurchase agreements,
economists said.
    most economists said the fed will inject reserves
indirectly via customer repurchases, but they added that the
fed might opt for a direct injection of reserves via overnight
system repurchases.
    federal funds opened at 6-3/8 pct and eased to 6-5/16 pct
in early trading. funds averaged 6.29 pct yesterday.
 reuter
</text>]