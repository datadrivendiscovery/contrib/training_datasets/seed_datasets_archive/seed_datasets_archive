[<text>
<title>lasmo canada to merge with onyx petroleum</title>
<dateline>    london, oct 20 - </dateline>lasmo exploration (canada) ltd, a
subsidiary of london &amp; scottish marine oil plc &lt;lsml.l&gt;, is
merging with &lt;onyx petroleum exploration co ltd&gt;, lasmo said.
    the merger is to be made by onyx buying lasmo exploration,
in a share swap. a new company, lasmo canada inc, will be
formed. lasmo will control 70 pct of the equity, worth some 76
mln canadian dlrs based on the current onyx share price.
    the new company will have proven reserves of 9.5 mln
barrels of oil and 20.9 billion cubic feet of gas, lasmo said.
 reuter
</text>]