[<text>
<title>more u.s. banks seen building loss reserves</title>
<dateline>    san francisco, june 1 - </dateline>federal deposit insurance
corporation chairman l. william seidman told a bankers' meeting
he thinks more banks will follow citicorp and chase manhattan
in building up their reserves to meet latin american debt
problems.
    seidman, speaking to an american bankers association trade
conference, said he did not think regulators would play as big
a role in encouraging banks to build up their loan-loss
reserves as the marketplace.
    "my guess is that we as regulators won't become the key
factor. i think the market system will get most of the other
banks to follow suit," seidman said.
    in the past two weeks, citicorp added three billion dlrs
and chase manhattan 1.6 billion to loan-loss reserves.
 reuter
</text>]