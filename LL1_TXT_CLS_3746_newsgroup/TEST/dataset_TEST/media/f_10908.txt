[<text>
<title>kaufman, board &lt;kb&gt; units cut quaker &lt;ksf&gt; stake</title>
<dateline>    washington, march 30 - </dateline>kaufman and board inc and its
insurance subsidiaries said they lowered their stake in quaker
state oil refining corp to 1,795,908 shares, or 6.9 pct of the
total outstanding, from 2,120,908 shares, or 8.1 pct.
    in a filing with the securities and exchange commission,
the kaufman and board group, which includes sun life group,
said it sold 325,000 quaker state common shares between feb 27
and march 23 at prices ranging from 28.00 to 31.00 dlrs each.

 reuter
</text>]