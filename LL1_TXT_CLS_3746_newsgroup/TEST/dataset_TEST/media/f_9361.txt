[<text>
<title>fed expected to add reserves in market</title>
<dateline>     new york, march 25 - </dateline>the federal reserve will probably
intervene in the government securities market to add reserves
today, economists said.
    they expected the fed will supply temporary reserves
indirectly via 1.5 to two billion dlrs of customer repurchase
agreements.
    fed funds hovered at a relatively high 6-1/4 pct this
morning after averaging 6.14 pct on tuesday.
    early this afternoon the fed also is expected to supply
reserves permanently, effective thursday, by offering to buy
all maturities of treasury bills.
 reuter
</text>]