[<text>
<title>enterra corp &lt;en&gt; sells two units</title>
<dateline>    houston, april 7 - </dateline>enterra corp said it has completed the
sale of its hale fire pump co and macomson machine co
subsidiaries to a company formed by los angeles investment firm
mcbain, rose partners for about 27 mln dlrs in cash.
    both hale and macomson make fire pumps and related
equipment.
 reuter
</text>]