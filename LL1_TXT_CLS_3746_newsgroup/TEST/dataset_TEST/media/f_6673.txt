[<text>
<title>piedmont aviation inc &lt;pie&gt; sets quarterly</title>
<dateline>    winston-salem, n.c., march 18 -
    </dateline>qtly div eight cts vs eight cts prior
    pay may 18
    record may one
 reuter
</text>]