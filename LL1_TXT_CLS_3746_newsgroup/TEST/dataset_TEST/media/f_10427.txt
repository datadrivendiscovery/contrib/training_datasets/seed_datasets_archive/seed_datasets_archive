[<text>
<title>standard bred pacers &lt;stbd&gt; yr loss</title>
<dateline>    great neck, n.y., march 27 -
    </dateline>shr loss 35 cts vs loss seven cts
    net loss 718,269 vs loss 145,216
    revs 1,394,080 vs 2,608,083
    note: full name of company is standard bred pacers and
trotters inc.
 reuter
</text>]