[<text>
<title>u.k. money market given 24 mln stg assistance</title>
<dateline>    london, march 3 - </dateline>the bank of england said it provided 24
mln stg help to the money market in the morning session.
    this compares with the bank's upward revised shortage
forecast of around 500 mln stg.
    the central bank purchased bank bills outright comprising
two mln stg in band one at 10-7/8 pct and 22 mln stg in band
two at 10-13/16 pct.
 reuter
</text>]