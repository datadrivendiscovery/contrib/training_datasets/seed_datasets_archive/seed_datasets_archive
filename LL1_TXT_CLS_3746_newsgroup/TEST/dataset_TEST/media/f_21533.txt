[<text>
<title>electricte de france issues 15 billion yen bond</title>
<dateline>    london, oct 19 - </dateline>electricite de france is issuing a 15
billion yen bond due november 1994 paying interest at 1/16 over
the six month london interbank offered rate, said ibj capital
markets ltd as lead manager.
    the bonds, which are priced at par, will be listed on the
luxembourg stock exchange. payment is set for november 20 and
they are callable in may 1989. the bonds are guaranteed by the
republic of france.
    fees are 12 basis points for management and underwriting
and 13 basis points as a selling concession. there is a three
basis point praecipuum.
 reuter
</text>]