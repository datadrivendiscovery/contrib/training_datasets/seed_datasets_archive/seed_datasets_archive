[<text>
<title>forum &lt;four&gt; ends beverly &lt;bev&gt; purchase deal</title>
<dateline>    indianapolis, march 16 - </dateline>forum group inc said it has
terminated its agreement in principle to buy eight retirement
living centers in six states from beverly enterprises due to a
failure to reach a satisfacotry definitive agreement.
 reuter
</text>]