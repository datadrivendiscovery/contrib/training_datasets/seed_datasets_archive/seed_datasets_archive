[<text>
<title>&lt;vista management inc&gt; gets bond guarantees</title>
<dateline>    hialeah, fla., march 25 - </dateline>vista managemement inc said it
has obtained notice of the availability of insurance guarantees
on 60 mln dlrs of leasing receivable bonds of its cbs leasing
subsidiary from developers insurance co, allowing the bonds to
be rated by leading credit rating agencies.
    the company said the guarantees are subject to the
placement of over one mln dlrs of reinsurance and to a pending
change of control of developers insurance.
 reuter
</text>]