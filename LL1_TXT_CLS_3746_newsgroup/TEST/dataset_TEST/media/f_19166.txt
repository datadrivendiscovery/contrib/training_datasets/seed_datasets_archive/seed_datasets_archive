[<text>
<title>&lt;microtel&gt; to lower long distance rates</title>
<dateline>    boca raton, fla., june 19 - </dateline>microtel, a fiber optic long
distance company, said it plans to lower rates for interstate
one plus dialing by 4.8 pct and laserplus wats service by 7.4
pct, effective july 15.
 reuter
</text>]