[<text>
<title>prime computer &lt;prm&gt; wins contract</title>
<dateline>    natick, mass., june 29 - </dateline>prime computer inc said it
received an order from the philadelphia board of education for
more than 8.0 mln dlrs of advanced computer equipment and
software for automating its administrative and student
accounting systems.
    the systems, designed and manufactured by prime, were
introduced in april.
 reuter
</text>]