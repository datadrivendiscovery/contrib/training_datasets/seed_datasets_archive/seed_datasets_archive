[<text>
<title>wang &lt;wan&gt; in software integration agreement</title>
<dateline>    lowell, mass., march 24 - </dateline>wang laboratories inc said it
entered into a development and marketing agreement with
sterling software's &lt;ssw&gt; answer systems inc, aimed at
integrating its data base product with answer's mainframe
database access tool.
    wang said the purpose of the projects is to offer wang vs
users direct access to information in db2, ims, and wsam files
residing on ibm (r) and ibm-compatible mainframes.
    wang said it will integrate its pace product with answer's
system's answer/db (r).
 reuter
</text>]