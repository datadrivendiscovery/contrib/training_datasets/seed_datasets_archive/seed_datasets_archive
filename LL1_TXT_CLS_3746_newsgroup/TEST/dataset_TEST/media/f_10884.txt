[<text>
<title>dune resources ltd &lt;dnlaf&gt; 4th qtr net</title>
<dateline>    oklahoma city, march 30 -
    </dateline>shr profit one ct vs loss two cts
    net profit 27,000 vs loss 69,000
    revs 295,000 vs 264,000
    year
    shr loss eight cts vs loss three cts
    net loss 262,000 vs loss 88,000
    revs 1,004,000 vs 1,248,000
 reuter
</text>]