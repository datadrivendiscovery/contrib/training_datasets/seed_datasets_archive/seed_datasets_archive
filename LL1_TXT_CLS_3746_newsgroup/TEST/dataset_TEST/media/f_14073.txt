[<text>
<title>digital transmission &lt;dtinu&gt; offers units</title>
<dateline>    new york, april 7 - </dateline>digital transmission inc is offering
400,000 units at 9.625 dlrs a unit, underwriter muller and co
inc said.
    net proceeds will be used for sales and marketing, research
and development, capital equipment and working capital.
    the underwriter has been granted an over-allotment option
to buy up to 60,000 more units. each unit consists of four
shares of class a common stock and one class a common stock
purchase warrant.
    the stock and the warrants are not detachable or separately
tradeable until six months after the offering or an earlier
date that may be determined by the underwriter.
 reuter
</text>]