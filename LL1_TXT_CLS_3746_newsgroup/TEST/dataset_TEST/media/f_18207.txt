[<text>
<title>scott paper &lt;spp&gt; sets modernization program</title>
<dateline>    philadelphia, june 2 - </dateline>scott paper co said its s.d. warren
co subsidiary will undertake a 60 mln dlr modernization to
increase capacity at its muskegon, mich., paper plant.
    the company said the plant is the nation's largest producer
of premium quality coated printing paper.
    it said the modernization is expected to be completed by
the fall of 1988.
 reuter
</text>]