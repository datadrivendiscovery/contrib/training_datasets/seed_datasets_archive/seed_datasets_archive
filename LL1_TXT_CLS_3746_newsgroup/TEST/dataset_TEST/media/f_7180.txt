[<text>
<title>guinness board seeks removal of saunders, ward</title>
<dateline>    london, march 19 - </dateline>guinness plc &lt;guin.l&gt; said its board had
agreed to put an extraordinary motion at the group's annual
meeting to remove former chairman ernest saunders and
non-executive thomas ward as directors.
    the meeting will be held on may 27.
    saunders resigned as chairman and chief executive in
january following the start of a trade department inquiry into
the group's takeover bid for &lt;distillers co plc&gt;.
    finance director olivier roux later quit from his executive
position and from the board but saunders and ward remained as
directors.
 reuter
</text>]