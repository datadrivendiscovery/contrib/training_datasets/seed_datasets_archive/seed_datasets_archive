[<text>
<title>woburn five cents savings &lt;wobs.o&gt; 1st qtr net</title>
<dateline>    woburn, mass., oct 19 -
    </dateline>shr 24 cts vs 26 cts
    net 959,000 vs 1,033,000
    assets 273.6 mln vs 236.3 mln
    deposits 183.6 mln vs 173.9 mln
    loans 133.3 mln vs 104.9 mln
    note: full name of company is woburn five cents savings
bank.
 reuter
</text>]