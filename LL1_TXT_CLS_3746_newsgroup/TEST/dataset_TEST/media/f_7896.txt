[<text>
<title>new york state electric &lt;nge&gt; ends acquisition</title>
<dateline>    binghamton, n.y., march 20 - </dateline>new york state electric and
gas corp said it has terminated its effort to acquire corning
natural gas corp.
    it cited as reasons the uncertain regulatory climate in new
york state and the depresed price of new york state electric
stock which has been caused by the delay in the nine mile point
unit two nuclear plant.
    the company had said in december that it had been
interested in acquiring corning natural gas.
 reuter
</text>]