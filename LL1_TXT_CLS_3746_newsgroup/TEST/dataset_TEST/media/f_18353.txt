[<text>
<title>coors &lt;acco.o&gt; sees lower second quarter</title>
<dateline>    golden, colo, june 2 - </dateline>adolph coors co said it expects
second quarter net income to be in the range of 45 cts to 50
cts a share compared to 82 cts a share in 1986.
    coors said earnings were hurt by start-up expenses at the
new shenandoah brewery and other marketing expenses.
    it said it expects to achieve record barrel sales in 1987,
but that second quarter beer shipments are expected to be
similar to the 4.2 mln volume of the second quarter of 1986.
    it said will spend about 90 mln dlrs in 1987 and 1988 to
expand shenandoah's operations and will increase advertising
spending by 10 pct to 200 mln dlrs in 1987.
 reuter
</text>]