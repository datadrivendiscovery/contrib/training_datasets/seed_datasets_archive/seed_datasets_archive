[<text>
<title>potlatch corp &lt;pch&gt; 1st qtr net</title>
<dateline>    san francisco, april 13 -
    </dateline>shr 63 cts vs 47 cts
    net 16.8 mln vs 12.4 mln
    sales 248.6 mln vs 233.3 mln
 reuter
</text>]