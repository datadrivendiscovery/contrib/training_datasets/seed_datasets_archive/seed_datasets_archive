[<text>
<title>amsouth bancorp &lt;aso&gt; sets exchange ratio</title>
<dateline>    birmingham, ala., april 13 - </dateline>amsouth bancorp said it will
issue about 3,166,000 shares of stock to acquire first
tuskaloosa corp.
    under a previously announced merger agreement, amsouth
offered 66 dlrs a share in amsouth stock for first tuskaloosa.
    the company said first tuskaloosa shareholders will receive
1.978825 shares of amsouth stock for each first tuskaloosa
share held when the merger is effected april 17.
    first tuskaloosa has assets of more than 425 mln dlrs.
amsouth's assets are about six billion dlrs.
 reuter
</text>]