[<text>
<title>u.k. money market given 40 mln stg late help</title>
<dateline>    london, april 2 - </dateline>the bank of england said it had provided
the money market with around 40 mln stg late assistance. this
takes the bank's total help today to some 537 mln stg and
compares with its estimate of a 700 mln stg shortage.
 reuter
</text>]