[<text>
<title>gulf applied &lt;gats&gt; to have gain from sale</title>
<dateline>    houston, march 4 - </dateline>gulf applied technologies inc said it
will report a gain of 2,900,000 dlrs or 89 cts per share on the
previously-announced sale of its pipelines and terminals
segment in first quarter results.
 reuter
</text>]