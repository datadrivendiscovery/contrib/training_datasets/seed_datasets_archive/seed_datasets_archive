[<text>
<title>no talks set on proposed japan telecom merger</title>
<dateline>    tokyo, april 14 - </dateline>no formal talks have been scheduled yet
among companies involved in a controversial proposal to merge
two groups seeking to enter japan's international
telecommunications sector, an official from one group said.
    "nothing has been firmed up yet," said an official at
&lt;international digital communications planning inc&gt; (idc), one
of the groups set up last year to study competing against
&lt;kokusai denshin denwa co ltd&gt;, which monopolises the sector.
    britain's cable and wireless plc &lt;cawl.l&gt;, which holds a 20
pct share in idc, has opposed plans to merge with rival group,
&lt;international telecom japan inc&gt;.
    under the plan, backed by the post and telecommunications
ministry, cable and wireless and u.s.-based &lt;pacific telesis
international inc&gt; would become core companies in the merged
firm, with shares equal to those of the six major japanese core
companies and seats on the board of directors.
    britain, angry over what it feels are moves to restrict
cable and wireless' role in the sector, views the issue as a
test case. the idc official declined to specify what was
holding up the talks.
    a spokesman for c. itoh and co ltd &lt;citt.t&gt;, which holds 20
pct of idc, said a meeting may be held later this week.
 reuter
</text>]