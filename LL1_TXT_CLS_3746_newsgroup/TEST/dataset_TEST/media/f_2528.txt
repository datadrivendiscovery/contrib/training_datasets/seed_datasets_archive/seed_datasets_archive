[<text>
<title>wichita &lt;wro&gt; to buy fountain oil &lt;fgas&gt;</title>
<dateline>    denver, colo., march 5 - </dateline>wichita industries inc said it
agreed to buy fountain oil and gas inc.
    wichita said it it will acquire all of the outstanding
shares of fountain in an exchange for about 11 mln newly issued
wichita common shares. wichita presently has about 3.6 mln
shares outstanding.
    the transaction calls for the issuance of 1-1/2 shares of
wichita common for each outstanding fountain share.
    wichita also said it made a number of refinancing
agreements in connection with the acquisition.
 reuter
</text>]