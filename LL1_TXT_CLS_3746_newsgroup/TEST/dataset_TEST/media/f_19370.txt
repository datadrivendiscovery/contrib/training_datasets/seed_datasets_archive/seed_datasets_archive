[<text>
<title>freeport-mcmoran oil and gas&lt;fmr&gt; distribution</title>
<dateline>    houston, june 19 -
    </dateline>monthly div 11.244 cts vs 10.309 cts prior
    pay july 10
    record june 30
    note: full name is freeport-mcmoran oil and gas royalty
trust
 reuter
</text>]