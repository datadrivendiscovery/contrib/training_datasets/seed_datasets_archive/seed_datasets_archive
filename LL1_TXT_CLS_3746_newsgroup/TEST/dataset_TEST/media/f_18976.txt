[<text>
<title>red lion inns l.p. &lt;red&gt; sets initial dividend</title>
<dateline>    vancourver, wash., june 18 - </dateline>red lion inns limited
partnership said it declared an initial cash distribution of 43
cts per unit, which is a proration of 50 cts per quarter.
    the distribution is payable august 14 to unitholders of
record june 30.
    the payout was adjusted to reflect the actual number of
days the partnership will have owned the hotels during the
calendar quarter ended june 30, 1987.
 reuter
</text>]