[<text>
<title>memory metals &lt;mrmt&gt; removes chairman</title>
<dateline>    stamford, conn., april 9 - </dateline>memory metals inc said that at a
special meeting of the board of directors it removed m. brian
o'shaugnessy as chairman, president and chief executive
officer.
    the company said it replaced o'shaugnessy as president with
stephen fischer, formerly senior vice president and chief
operating officer.
    the other two positions have not been filled yet, a
spokesman for the company said.
 reuter
</text>]