[<text>
<title>weston &lt;weston&gt; files convertible offering</title>
<dateline>    new york, march 31 - </dateline>roy f weston inc said it filed with
the securitites and exchange commission a registration
statement covering a 30 mln dlr issue of convertible
subordinated debentures due 2002.
    plans for the proceeds include capital expenditures such as
new office buildings, equipment and facilities, the company
said.  weston also anticipates using the proceeds for possible
acquisitions of related businesses as well as for general
corporate purposes.
    weston named drexel burnham lambert inc as sole manager of
the offering.
 reuter
</text>]