[<text>
<title>dekalb corp &lt;dklbb&gt; 2nd qtr feb 28 net</title>
<dateline>    dekalb, ill., april 9 -
    </dateline>shr 20 cts vs 14 cts
    net 2.4 mln vs 1.7 mln
    revs 136.7 mln vs 174.4 mln
    six mths
    sdhr 72 cts vs 82 cts
    net 8.6 mln vs 9.9 mln
    revs 212.5 mln vs 268.8 mln
 reuter
</text>]