[<text>
<title>zaire authorized to buy pl 480 rice - usda</title>
<dateline>    washington, march 24 - </dateline>zaire has been authorized to
purchase about 30,000 tonnes of u.s. rice under an existing pl
480 agreement, the u.s. agriculture department said.
    it may buy the rice, valued at 5.5 mln dlrs, between march
31 and august 31, 1987, and ship it from u.s. ports by
september 30, the department said.
    the purchase authorization covers the entire quantity of
rice provided under the agreement.
 reuter
</text>]