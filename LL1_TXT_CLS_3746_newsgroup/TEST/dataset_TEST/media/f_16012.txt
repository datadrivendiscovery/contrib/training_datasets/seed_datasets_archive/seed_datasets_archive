[<text>
<title>egypt seeking 500,000 tonnes corn - u.s. traders</title>
<dateline>    kansas city, april 9 - </dateline>egypt is expected to tender april 22
for 500,000 tonnes of corn for may through september shipments,
private export sources said.
 reuter
</text>]