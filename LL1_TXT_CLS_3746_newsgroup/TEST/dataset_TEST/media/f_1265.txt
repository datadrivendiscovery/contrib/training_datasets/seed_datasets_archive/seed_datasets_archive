[<text>
<title>del e. webb investment &lt;dwpa&gt; 4th qtr net</title>
<dateline>    phoenix, ariz., march 3 -
    </dateline>shr eight cts vs 16 cts
    net 188,000 vs 354,000
    revs 538,000 vs 594,000
    year
    shr 31 cts vs 28 cts
    net 692,000 vs 617,000
    revs 2,640,000 vs 906,000
    (del e. webb investment properties inc)
 reuter
</text>]