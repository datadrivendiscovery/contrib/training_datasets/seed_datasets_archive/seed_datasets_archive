[<text>
<title>fpl group &lt;fpl&gt; unit files to offer preferred</title>
<dateline>    miami, march 25 - </dateline>fpl group inc's florida power and light
co unit said it filed a shelf registration with the securities
and exchange commission for the future sale, through one or
more offerings, of one mln shares of serial preferred stock.
    it said that these shares, together with 150,000 shares of
preferred stock that are unissued under a previous shelf
registration, should cover the utility's preferred stock
financing needs for the next two years.
 reuter
</text>]