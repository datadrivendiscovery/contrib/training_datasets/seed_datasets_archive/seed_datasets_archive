[<text>
<title>biomet inc &lt;bmet&gt; 3rd qtr feb 28 net</title>
<dateline>    warsaw, ind., april 3 -
    </dateline>shr 18 cts vs 13 cts
    net 2,133,000 vs 1,384,000
    revs 14.1 mln vs 11.7 mln
    nine mths
    shr 49 cts vs 36 cts
    net 5,657,000 vs 3,728,000
    revs 39.7 mln vs 31.8 mln
 reuter
</text>]