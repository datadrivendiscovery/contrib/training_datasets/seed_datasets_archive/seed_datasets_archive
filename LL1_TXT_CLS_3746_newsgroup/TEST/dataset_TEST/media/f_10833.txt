[<text>
<title>regency cruists &lt;ship&gt; corrects earnings</title>
<dateline>    new york, march 30 - </dateline>regency cruises inc said its earnings
per share for the year 1986 were 36 cts per share, not the 37
cts it reported on march 11.
    the company lost 10 cts per share in 1985.
 reuter
</text>]