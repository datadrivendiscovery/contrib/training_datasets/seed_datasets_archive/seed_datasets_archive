[<text>
<title>republic savings and loan &lt;rsla&gt; 3rd qtr net</title>
<dateline>    milwaukee, wis., april 9 - 
    </dateline>shr 89 cts vs not available
    net 1,163,000 vs 466,000
    nine mths
    net 3,696,000 vs 1,624,000
    note: company converted to stock ownership effective august
1986.
    periods end march 31, 1987 and 1986 respectively.
 reuter
</text>]