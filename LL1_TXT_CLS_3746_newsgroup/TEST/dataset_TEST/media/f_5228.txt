[<text>
<title>cie bancaire in one-for-five bonus share issue</title>
<dateline>    paris, march 16 - </dateline>&lt;cie bancaire&gt;, a subsidiary of recently
privatised banking group cie financiere de paribas &lt;pari.pa&gt;,
said it is issuing 2.35 mln new 100 francs nominal shares on
the basis of one for five already held.
    the operation will begin on march 31, a spokesman said.
    cie bancaire also said it has increased its capital to 1.41
billion francs from 1.17 billion by the incorporation of 237.74
mln francs of reserves.
 reuter
</text>]