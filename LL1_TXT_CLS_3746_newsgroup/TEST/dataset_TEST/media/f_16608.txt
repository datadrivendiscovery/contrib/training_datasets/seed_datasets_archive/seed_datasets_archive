[<text>
<title>willamette industries &lt;wmtt&gt; 4th qtr net</title>
<dateline>    portland, ore., april 13 -
    </dateline>shr 90 cts vs 30 cts
    net 22.9 mln vs 7,567,000
    sales 323.0 mln vs 272.1 mln
    note: per share figures reflect april 25, 1986
five-to-three stock split. full year figures not available.
 reuter
</text>]