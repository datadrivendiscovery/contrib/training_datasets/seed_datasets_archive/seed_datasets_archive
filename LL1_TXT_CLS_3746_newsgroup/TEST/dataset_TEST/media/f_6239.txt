[<text>
<title>usda reports 350,000 tonnes corn to unknown</title>
<dateline>    washington, march 17 - </dateline>the u.s. agriculture department said
private u.s. exporters reported sales of 350,000 tonnes of corn
for delivery to unknown destinations during the 1986/87
marketing season.
    the marketing year for corn began september 1.
    this is the second day running that exporters have reported
corn sales to unknown destinations. yesterday, they reported
sales of 150,000 tonnes to unknown.
 reuter
</text>]