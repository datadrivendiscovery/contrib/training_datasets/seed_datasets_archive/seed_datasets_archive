[<text>
<title>first valley corp &lt;fivc&gt; regular dividend</title>
<dateline>    bethlehem, pa., march 17 -
    </dateline>qtly div 21 cts
    payable april 15
    record march 31
 reuter
</text>]