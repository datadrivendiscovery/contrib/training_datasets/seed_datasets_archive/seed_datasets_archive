[<text>
<title>fluor &lt;flr&gt; to assess value of gold operations</title>
<dateline>    irvine, calif., june 1 - </dateline>fluor corp said it retained two
investment bankers to assess the value of its gold operations
in light of improved world gold market conditions.
    the engineering, construction and natural resources company
said it retained shearson lehman brothers inc and s.g. warburg
and co inc to assist in the assessment.
    fluor owns 90 pct of st. joe gold corp &lt;sjg&gt;, a unit of
fluor's st. joe minerals subsidiary.
    st. joe gold explores, develops, mines and produces
precious metals in the u.s., canada and chile. it produced
281,000 ounces of gold in its most recent fiscal year.
    fluor also has gold operations in australia, spain and
uruguay.
 reuter
</text>]