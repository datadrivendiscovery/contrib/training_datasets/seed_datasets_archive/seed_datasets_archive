[<text>
<title>union valley corp &lt;uvc&gt; 4th qtr net</title>
<dateline>    whiting, n.j., march 18 -
    </dateline>shr 20 cts vs 34 cts
    net 797,000 vs 1,137,000
    rev 22.2 mln vs 18.2 mln
    avg shares 3,966,667 vs 3,366,667
    year
    shr 73 cts vs one dlr
    net 2,625,000 vs 3,371,000
    rev 69.6 mln vs 62.9 mln
    avg shares 3,583,653 vs 3,366,667
    note: 1986 includes extraordinary gain of 281,000 dlrs, or
eight cts a share.
 reuter
</text>]