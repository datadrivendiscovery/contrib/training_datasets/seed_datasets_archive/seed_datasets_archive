[<text>
<title>sl industries inc &lt;sl&gt; 2nd qtr jan 31 net</title>
<dateline>    marlton, n.j., march 5 -
    </dateline>oper shr 22 cts vs 20 cts
    oper net 1,153,000 vs 1,068,000
    revs 15.7 mln vs 15.8 mln
    six mths
    oper shr 38 cts vs 38 cts
    oper net 2,039,000 vs 2,051,000
    revs 31.4 mln vs 31.6 mln
    note: exlcudes 145,000 discontinued operations for 1986
oper net for six mths for sale of electronics division.
    oper shr for qtr and six mths 1986 adjusted for stock split
and dividend distribution in november.
 reuter
</text>]