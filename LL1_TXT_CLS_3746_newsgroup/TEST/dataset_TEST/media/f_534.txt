[<text>
<title>kapok corp &lt;kpk&gt; 1st qtr dec 31 loss</title>
<dateline>    clearwater, fla., march 2 -
    </dateline>shr loss 14 cts vs loss 21 cts
    net loss 353,000 vs loss 541,000
    revs 2,668,000 vs 2,525,000
    avg shrs 2,452,3000 vs 2,552,300
 reuter
</text>]