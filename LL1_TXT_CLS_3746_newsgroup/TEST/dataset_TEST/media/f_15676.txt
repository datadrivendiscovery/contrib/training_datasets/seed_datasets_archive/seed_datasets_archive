[<text>
<title>ec grants free market barley, maize exports</title>
<dateline>    london, april 9 - </dateline>the european commission authorised the
export of 65,000 tonnes of free market barley at today's tender
at a maximum rebate of 138.75 european currency units and
55,000 tonnes of french maize at 130 ecus, grain traders here
said.
    it rejected bids for breadmaking and feed wheat, they said.
 reuter
</text>]