[<text>
<title>final test inc &lt;fnlt&gt; 4th qtr loss</title>
<dateline>    dallas, march 20 -
    </dateline>shr loss six cts vs loss 88 cts
    net loss 128,141 vs loss 1,298,377
    sales 1,332,218 vs 385,146
    year
    shr profit six cts vs loss 1.47 dlrs
    net profit 120,571 vs loss 2,171,011
    sales 4,617,034 vs 2,959,141
 reuter
</text>]