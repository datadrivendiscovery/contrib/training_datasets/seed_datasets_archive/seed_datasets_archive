[<text>
<title>home savings bank &lt;hmsb&gt; sets initial quarterly</title>
<dateline>    new york, march 26 - </dateline>home savings bank said its board
declared an initial quarterly dividend of nine cts per share,
payable april 30, record april six.
 reuter
</text>]