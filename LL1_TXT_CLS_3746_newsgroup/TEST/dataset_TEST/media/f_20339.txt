[<text>
<title>aristech chemical corp &lt;ars&gt; 3rd qtr net</title>
<dateline>    pittsburgh, oct 20 -
    </dateline>shr 72 cts vs 56 cts
    qtly div 18 cts vs 18 cts prior
    net 18.7 mln vs 14.4 mln
    revs 239.7 mln vs 187.1 mln
    nine mths
    shr 1.87 dlrs vs 1.26 dlrs
    net 48.3 mln vs 32.5 mln
    revs 676.9 mln vs 574.8 mln
    note: dividend payable december 1 to shareholders of record
october 30.
 reuter
</text>]