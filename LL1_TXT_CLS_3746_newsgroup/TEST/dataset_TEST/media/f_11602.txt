[<text>
<title>royex makes bid for international corona</title>
<dateline>    toronto, march 31 - </dateline>&lt;royex gold mining corp&gt; said it is
making an offer for &lt;international corona resources ltd&gt; of 50
dlrs per share and 40 dlrs per warrant.
    the offer covers four mln corona common shares and all
outstanding 9.50 dlr corona share purchase warrants expiring
august 31, 1987, royex said.
    royex said that if it gains four mln corona common shares,
its interest in corona will increase to 50 pct from 38 pct. it
also said that if more than four mln shares are tendered, it
will pay for them on a pro rata basis.
    royex said the purchase price for each corona share
consists of one royex convertible retractable zero coupon
series b first preference share at 20 dlrs nominal value, one
royex convertible 6-1/2 pct series c first preference share at
20 dlrs nominal value, one five-year 7.50 dlr royex share
purchase warrant and four dlrs in cash.
    the price for each corona warrant consists of 1.75
convertible retractable zero coupon series b first preference
share at 35 dlrs nominal value and one five-year 7.50 royex
share purchase warrant.
     royex said both series of preference shares will be
convertible into royex common shares, initially on a basis of
3.33 common shares for each preference share converted.
 reuter
</text>]