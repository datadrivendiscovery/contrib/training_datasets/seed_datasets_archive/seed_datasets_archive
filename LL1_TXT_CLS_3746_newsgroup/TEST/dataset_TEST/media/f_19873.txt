[<text>
<title>first pennsylvania &lt;fba&gt; to launch new visa card</title>
<dateline>    philadelphia, june 29 - </dateline>first pennsylvania bank n.a., the
major subsidiary of first pennsylvania corp, said it will add a
variable-rate &lt;visa&gt; card to its consumer credit product line.
    the bank said the card carries a 14 pct annual interest
rate that is guaranteed through sept 30, after which time the
interest rate will increase or decrease if there is a change in
the prime rate as reflected in the wall street journal.
    there is no grace period for purchases made with card, but
first pennsylvania already offers a &lt;mastercard&gt; product with a
25-day grace period, the bank added.
 reuter
</text>]