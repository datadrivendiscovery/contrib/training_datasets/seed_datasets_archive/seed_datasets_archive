[<text>
<title>ultrasystems &lt;uls&gt; unit gets dod contracts</title>
<dateline>    irvine, calif., march 16 - </dateline>ultrasystems inc said the u.s.
defense department awarded the company's ultrasystems defense
and space inc unit several new contracts and extensions to
existing contracts having a total value of more than 15.1 mln
dlrs.
    the new awards involve software development and hardware
integration for secure office automation systems, ultrasystems
said.
 reuter
</text>]