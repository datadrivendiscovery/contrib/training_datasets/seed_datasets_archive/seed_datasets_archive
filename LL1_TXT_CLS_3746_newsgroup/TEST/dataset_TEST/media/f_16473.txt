[<text>
<title>franklin california sets monthly payout</title>
<dateline>    san mateo, calif., april 13 -
    </dateline>mthly div 6.5 cts vs 6.5 cts prior
    pay april 30
    reord april 15
    note: franklin california insured tax-free income fund.
 reuter
</text>]