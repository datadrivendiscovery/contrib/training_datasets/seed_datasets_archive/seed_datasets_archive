[<text>
<title>astrocom &lt;acom&gt; adds new networking products</title>
<dateline>    st. paul, march 20 - </dateline>astrocom corp said it added two new
products aimed at improving on-premise data network efficiency.
    its atdm/ms six-port time-division multiplexor is designed
to combine and send data from up to 18 remove terminals on a
single transmission link to the controller in the central
network.
    the other product, the squeeziplexor 3074 coaxial
multiplexor, is a low-profile version of the company's
squeeziplexor system that allows one coaxial cable to replace
up to 32 individual cables connecting terminals and printers to
the central controller.
 reuter
</text>]