[<text>
<title>slater electric inc &lt;slat&gt; 1st qtr feb 28 net</title>
<dateline>    glen cove, n.y., april 9 -
    </dateline>shr four cts vs 10 cts
    net 31,000 vs 82,000
    sales 10.9 mln vs 9,760,000
 reuter
</text>]