[<text>
<title>bankamerica &lt;bac&gt; sets new banking service</title>
<dateline>    san francisco, april 13 - </dateline>bankamerica corp said it has
introduced an electronic information and transaction service
for small businesses in california.
    the bank holding company said the service, called business
connection, allows customers to use personal computers to
transefer money between accounts, check account balances in
bank of america business checking, savings and credit card
accounts, and to call up checking account statements.
    the service also allows businesses to use electronic mail
to stop payments on checks, access commercial credit lines for
advancements and repayments,  and to pay bills.
 reuter
</text>]