[<text>
<title>metromedia buys warrant for shares of orion</title>
<dateline>    seacaucus, n.j., april 3 - </dateline>&lt;metromedia co&gt; said it
purchased from time inc's &lt;tl&gt; home box office inc a warrant to
purchase 800,000 shares of common stock of orion pictures corp
&lt;opc&gt;.
    the price for the warrant was 10 mln dlrs, metromedia said.
it added that it now owns 16.4 pct of the outstanding common
stock of orion.
 reuter
</text>]