[<text>
<title>reuters to buy i p sharp of canada</title>
<dateline>    london, march 25 - </dateline>reuters holdings plc &lt;rtrs.l&gt; said it
had agreed in principle to buy &lt;i p sharp associates ltd&gt; of
toronto for 30.4 mln stg.
    sharp is a time-sharing network and database company
specialising in finance, economics, energy and aviation. it
operates a global packet-switching network and global limits
systems for foreign exchange trading.
    sharp shareholders will be offered cash, shares or a
mixture of the two in settlement. the acquisition, which is
subject to canadian government approval, would be through
amalgamation into a specially-created company.
    reuters said it had been given options by a number of sharp
shareholders covering 67 pct of the common stock pending
completion of a reuters review of the company.
    sharp operates 38 offices in 20 countries. in 1986 it
reported revenue of 55 mln canadian dlrs with a pretax loss of
1.6 mln compared with a 1.9 mln profit in 1985.
    however, sharp said that internal accounts showed the
company was in profit in the first two months of 1987.
    end-1986 net assets totalled 11.85 mln dlrs.
    a reuters statement said the acquisition would fit
perfectly into its package for the banking and securities
industries.
 reuter
</text>]