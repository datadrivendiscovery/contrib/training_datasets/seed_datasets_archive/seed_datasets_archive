[<text>
<title>juno lighting inc &lt;juno&gt; 1st qtr feb 28 net</title>
<dateline>    des plaines, ill., march 23 -
    </dateline>shr 43 cts vs 32 cts
    net 1,991,000 vs 1,485,000
    sales 11.7 mln vs 9,479,000
 reuter
</text>]