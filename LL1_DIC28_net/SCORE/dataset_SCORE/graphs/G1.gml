graph [
  directed 1
  multigraph 1
  node [
    id 0
    label "Lydia Nolte"
    x 0.2661
    y 0.4161
    shape "0.5000"
    ellipse "ic"
    Gray "[4-475]"
    nodeID "Lydia Nolte"
  ]
  node [
    id 1
    label "David Motibe"
    x 0.3609
    y 0.445
    shape "0.5000"
    triangle "ic"
    Yellow "[281-325]"
    nodeID "David Motibe"
  ]
  node [
    id 2
    label "Rosi Koch"
    x 0.2584
    y 0.345
    shape "0.5000"
    ellipse "ic"
    Blue "[157-*]"
    nodeID "Rosi Koch"
  ]
  node [
    id 3
    label "Hubert Koch"
    x 0.2735
    y 0.3496
    shape "0.5000"
    triangle "ic"
    Gray "[159-549]"
    nodeID "Hubert Koch"
  ]
  node [
    id 4
    label "Zorro Franz Josef Pelsteiner"
    x 0.2898
    y 0.2825
    shape "0.5000"
    triangle "ic"
    Yellow "[141-383/669-670]"
    nodeID "Zorro Franz Josef Pelsteiner"
  ]
  node [
    id 5
    label "Ernst-Hugo von Salen-Priesnitz"
    x 0.1567
    y 0.4167
    shape "0.5000"
    triangle "ic"
    Gray "[123/455-597]"
    nodeID "Ernst-Hugo von Salen-Priesnitz"
  ]
  node [
    id 6
    label "Amelie von der Marwitz"
    x 0.2915
    y 0.4237
    shape "0.5000"
    ellipse "ic"
    Gray "[203-597]"
    nodeID "Amelie von der Marwitz"
  ]
  node [
    id 7
    label "Onkel Franz Franz Witt"
    x 0.3257
    y 0.4864
    shape "0.5000"
    triangle "ic"
    Blue "[89-*]"
    nodeID "Onkel Franz Franz Witt"
  ]
  node [
    id 8
    label "Gung Pahm Kien"
    x 0.3886
    y 0.3297
    shape "0.5000"
    triangle "ic"
    Blue "[4-*]"
    nodeID "Gung Pahm Kien"
  ]
  node [
    id 9
    label "Urzula Wienicki"
    x 0.4418
    y 0.4113
    shape "0.5000"
    ellipse "ic"
    Blue "[249-*]"
    nodeID "Urzula Wienicki"
  ]
  node [
    id 10
    label "Gabi Zenker-geb.Skawowski"
    x 0.3076
    y 0.3
    shape "0.5000"
    ellipse "ic"
    Blue "[1-*]"
    nodeID "Gabi Zenker-geb.Skawowski"
  ]
  node [
    id 11
    label "Phil Seegers"
    x 0.1608
    y 0.2469
    shape "0.5000"
    triangle "ic"
    Blue "[14-*]"
    nodeID "Phil Seegers"
  ]
  node [
    id 12
    label "Hans Wilhelm Huelsch"
    x 0.0891
    y 0.2154
    shape "0.5000"
    triangle "ic"
    Blue "[115-*]"
    nodeID "Hans Wilhelm Huelsch"
  ]
  node [
    id 13
    label "Hajo Scholz"
    x 0.2922
    y 0.4348
    shape "0.5000"
    triangle "ic"
    Blue "[241-*]"
    nodeID "Hajo Scholz"
  ]
  node [
    id 14
    label "Olli Klatt"
    x 0.372
    y 0.4252
    shape "0.5000"
    triangle "ic"
    Blue "[234-*]"
    nodeID "Olli Klatt"
  ]
  node [
    id 15
    label "Er-Schiller"
    x 0.2999
    y 0.6299
    shape "0.5000"
    triangle "ic"
    Blue "[301-*]"
    nodeID "Er-Schiller"
  ]
  node [
    id 16
    label "Helga Beimer Schiller"
    x 0.359
    y 0.5054
    shape "0.5000"
    ellipse "ic"
    Blue "[1-*]"
    nodeID "Helga Beimer Schiller"
  ]
  node [
    id 17
    label "Hans Beimer"
    x 0.3377
    y 0.2836
    shape "0.5000"
    triangle "ic"
    Blue "[1-*]"
    nodeID "Hans Beimer"
  ]
  node [
    id 18
    label "Anna Ziegler"
    x 0.3068
    y 0.2352
    shape "0.5000"
    ellipse "ic"
    Blue "[61-*]"
    nodeID "Anna Ziegler"
  ]
  node [
    id 19
    label "Andy Zenker"
    x 0.3052
    y 0.4016
    shape "0.5000"
    triangle "ic"
    Blue "[220-*]"
    nodeID "Andy Zenker"
  ]
  node [
    id 20
    label "Berta Griese"
    x 0.3496
    y 0.4564
    shape "0.5000"
    ellipse "ic"
    Blue "[1-*]"
    nodeID "Berta Griese"
  ]
  node [
    id 21
    label "Manoel Griese"
    x 0.3463
    y 0.3938
    shape "0.5000"
    triangle "ic"
    Yellow "[90-471]"
    nodeID "Manoel Griese"
  ]
  node [
    id 22
    label "Lisa Hoffmeister"
    x 0.3126
    y 0.3789
    shape "0.5000"
    ellipse "ic"
    Blue "[297-*]"
    nodeID "Lisa Hoffmeister"
  ]
  node [
    id 23
    label "Marion Beimer"
    x 0.4459
    y 0.4263
    shape "0.5000"
    ellipse "ic"
    Yellow "[1-*]"
    nodeID "Marion Beimer"
  ]
  node [
    id 24
    label "Benny Beimer"
    x 0.4877
    y 0.3565
    shape "0.5000"
    triangle "ic"
    Gray "[1-520]"
    nodeID "Benny Beimer"
  ]
  node [
    id 25
    label "Klaus Beimer"
    x 0.337
    y 0.4213
    shape "0.5000"
    triangle "ic"
    Blue "[1-*]"
    nodeID "Klaus Beimer"
  ]
  node [
    id 26
    label "Dr. Eva-Maria Sperling"
    x 0.3632
    y 0.4851
    shape "0.5000"
    ellipse "ic"
    Blue "[344-*]"
    nodeID "Dr. Eva-Maria Sperling"
  ]
  node [
    id 27
    label "Kurt Sperling"
    x 0.2398
    y 0.4802
    shape "0.5000"
    triangle "ic"
    Yellow "[384-548]"
    nodeID "Kurt Sperling"
  ]
  node [
    id 28
    label "Boris Ecker"
    x 0.4158
    y 0.1994
    shape "0.5000"
    triangle "ic"
    Blue "[402-*]"
    nodeID "Boris Ecker"
  ]
  node [
    id 29
    label "Valerie Ecker"
    x 0.2586
    y 0.2669
    shape "0.5000"
    ellipse "ic"
    Blue "[220-*]"
    nodeID "Valerie Ecker"
  ]
  node [
    id 30
    label "Iffi Zenker"
    x 0.2444
    y 0.4282
    shape "0.5000"
    nodeID "Iffi Zenker"
  ]
  node [
    id 31
    label "Momo Sperling"
    x 0.2114
    y 0.5635
    shape "0.5000"
    triangle "ic"
    Blue "[346-*]"
    nodeID "Momo Sperling"
  ]
  node [
    id 32
    label "Franz Schildknecht"
    x 0.7425
    y 0.4135
    shape "0.5000"
    triangle "ic"
    Gray "[2-370]"
    nodeID "Franz Schildknecht"
  ]
  node [
    id 33
    label "Henny Schildknecht"
    x 0.6241
    y 0.576
    shape "0.5000"
    ellipse "ic"
    Gray "[2-62]"
    nodeID "Henny Schildknecht"
  ]
  node [
    id 34
    label "Stefan Nossek"
    x 0.5617
    y 0.5157
    shape "0.5000"
    triangle "ic"
    Gray "[5-133]"
    nodeID "Stefan Nossek"
  ]
  node [
    id 35
    label "Vera Schildknecht"
    x 0.8574
    y 0.271
    shape "0.5000"
    ellipse "ic"
    Gray "[117-368]"
    nodeID "Vera Schildknecht"
  ]
  node [
    id 36
    label "Dr. Ahmet Dagdelen"
    x 0.4853
    y 0.4934
    shape "0.5000"
    triangle "ic"
    Blue "[624-*]"
    nodeID "Dr. Ahmet Dagdelen"
  ]
  node [
    id 37
    label "Matthias Steinbrueck"
    x 0.409
    y 0.4824
    shape "0.5000"
    triangle "ic"
    Gray "[64-507]"
    nodeID "Matthias Steinbrueck"
  ]
  node [
    id 38
    label "Egon Kling"
    x 0.5157
    y 0.479
    shape "0.5000"
    triangle "ic"
    Gray "[1-658]"
    nodeID "Egon Kling"
  ]
  node [
    id 39
    label "Else Kling"
    x 0.4059
    y 0.5063
    shape "0.5000"
    ellipse "ic"
    Blue "[1-*]"
    nodeID "Else Kling"
  ]
  node [
    id 40
    label "Paolo Varese"
    x 0.5536
    y 0.3888
    shape "0.5000"
    triangle "ic"
    Blue "[299-*]"
    nodeID "Paolo Varese"
  ]
  node [
    id 41
    label "Dani Schmitz"
    x 0.2981
    y 0.4904
    shape "0.5000"
    ellipse "ic"
    Blue "[581-*]"
    nodeID "Dani Schmitz"
  ]
  node [
    id 42
    label "Philipp Sperling"
    x 0.3299
    y 0.4469
    shape "0.5000"
    triangle "ic"
    Blue "[376-*]"
    nodeID "Philipp Sperling"
  ]
  node [
    id 43
    label "Tanja Schildknecht-Dressler"
    x 0.5683
    y 0.4784
    shape "0.5000"
    ellipse "ic"
    Blue "[2-*]"
    nodeID "Tanja Schildknecht-Dressler"
  ]
  node [
    id 44
    label "Dr. Ludwig Dressler"
    x 0.4326
    y 0.4703
    shape "0.5000"
    triangle "ic"
    Blue "[1-*]"
    nodeID "Dr. Ludwig Dressler"
  ]
  node [
    id 45
    label "Elisabeth Dressler"
    x 0.552
    y 0.5684
    shape "0.5000"
    ellipse "ic"
    Gray "[6-262]"
    nodeID "Elisabeth Dressler"
  ]
  node [
    id 46
    label "Mary Kling"
    x 0.4257
    y 0.2769
    shape "0.5000"
    ellipse "ic"
    Blue "[518-*]"
    nodeID "Mary Kling"
  ]
  node [
    id 47
    label "Olaf Kling"
    x 0.4565
    y 0.3788
    shape "0.5000"
    triangle "ic"
    Blue "[387-*]"
    nodeID "Olaf Kling"
  ]
  node [
    id 48
    label "Claudia Kling-Rantzow"
    x 0.5562
    y 0.3283
    shape "0.5000"
    ellipse "ic"
    Gray "[229-491]"
    nodeID "Claudia Kling-Rantzow"
  ]
  node [
    id 49
    label "Fausto Rossini"
    x 0.6534
    y 0.4967
    shape "0.5000"
    triangle "ic"
    Blue "[571-*]"
    nodeID "Fausto Rossini"
  ]
  node [
    id 50
    label "Sonia Besirski"
    x 0.4888
    y 0.4376
    shape "0.5000"
    ellipse "ic"
    Gray "[474-635]"
    nodeID "Sonia Besirski"
  ]
  node [
    id 51
    label "Frank Dressler"
    x 0.439
    y 0.388
    shape "0.5000"
    triangle "ic"
    Blue "[7-*]"
    nodeID "Frank Dressler"
  ]
  node [
    id 52
    label "Panaiotis Sarikakis"
    x 0.6083
    y 0.3946
    shape "0.5000"
    triangle "ic"
    Yellow "[9-559]"
    nodeID "Panaiotis Sarikakis"
  ]
  node [
    id 53
    label "Elena Sarikakis"
    x 0.6029
    y 0.2469
    shape "0.5000"
    ellipse "ic"
    Blue "[9-*]"
    nodeID "Elena Sarikakis"
  ]
  node [
    id 54
    label "Isolde Pavarotti"
    x 0.5878
    y 0.4487
    shape "0.5000"
    nodeID "Isolde Pavarotti"
  ]
  node [
    id 55
    label "Enrico Pavarotti"
    x 0.6669
    y 0.4323
    shape "0.5000"
    triangle "ic"
    Gray "[137-556]"
    nodeID "Enrico Pavarotti"
  ]
  node [
    id 56
    label "Robert Engel"
    x 0.4146
    y 0.4485
    shape "0.5000"
    triangle "ic"
    Yellow "[104-364]"
    nodeID "Robert Engel"
  ]
  node [
    id 57
    label "Kaethe Georg Eschweiler"
    x 0.4622
    y 0.609
    shape "0.5000"
    triangle "ic"
    Blue "[526-*]"
    nodeID "Kaethe Georg Eschweiler"
  ]
  node [
    id 58
    label "Carsten Floeter"
    x 0.4469
    y 0.5148
    shape "0.5000"
    triangle "ic"
    Blue "[6-*]"
    nodeID "Carsten Floeter"
  ]
  node [
    id 59
    label "Theo Klages"
    x 0.4501
    y 0.7143
    shape "0.5000"
    triangle "ic"
    Blue "[575-*]"
    nodeID "Theo Klages"
  ]
  node [
    id 60
    label "Beate Sarikakis"
    x 0.5247
    y 0.4121
    shape "0.5000"
    ellipse "ic"
    Blue "[9-*]"
    nodeID "Beate Sarikakis"
  ]
  node [
    id 61
    label "Vasily Sarikakis"
    x 0.4782
    y 0.3852
    shape "0.5000"
    triangle "ic"
    Blue "[1-*]"
    nodeID "Vasily Sarikakis"
  ]
  node [
    id 62
    label "Rolf Sattler"
    x 0.2123
    y 0.4113
    shape "0.5000"
    triangle "ic"
    Blue "[329-*]"
    nodeID "Rolf Sattler"
  ]
  node [
    id 63
    label "Bolle Guenther Bollmann"
    x 0.1957
    y 0.4112
    shape "0.5000"
    nodeID "Bolle Guenther Bollmann"
  ]
  node [
    id 64
    label "Hilmar Eggers"
    x 0.2944
    y 0.5637
    shape "0.5000"
    triangle "ic"
    Blue "[189-*]"
    nodeID "Hilmar Eggers"
  ]
  node [
    id 65
    label "Wilhelm Loesch"
    x 0.3421
    y 0.6076
    shape "0.5000"
    triangle "ic"
    Blue "[193-*]"
    nodeID "Wilhelm Loesch"
  ]
  node [
    id 66
    label "Marlene Schmitt"
    x 0.3711
    y 0.5986
    shape "0.5000"
    ellipse "ic"
    Blue "[285-*]"
    nodeID "Marlene Schmitt"
  ]
  node [
    id 67
    label "Friedemann Traube"
    x 0.3651
    y 0.1911
    shape "0.5000"
    triangle "ic"
    Blue "[611-*]"
    nodeID "Friedemann Traube"
  ]
  node [
    id 68
    label "Wanda Winicki"
    x 0.3543
    y 0.2944
    shape "0.5000"
    ellipse "ic"
    Blue "[249-*]"
    nodeID "Wanda Winicki"
  ]
  node [
    id 69
    label "Pat Wolffson"
    x 0.383
    y 0.4453
    shape "0.5000"
    ellipse "ic"
    Blue "[489-*]"
    nodeID "Pat Wolffson"
  ]
  node [
    id 70
    label "Winifred Snyder"
    x 0.2946
    y 0.709
    shape "0.5000"
    ellipse "ic"
    Blue "[616-*]"
    nodeID "Winifred Snyder"
  ]
  node [
    id 71
    label "Irina Winicki"
    x 0.4322
    y 0.6387
    shape "0.5000"
    ellipse "ic"
    Blue "[326-*]"
    nodeID "Irina Winicki"
  ]
  node [
    id 72
    label "Sophie Ziegler"
    x 0.3362
    y 0.1707
    shape "0.5000"
    ellipse "ic"
    Blue "[295-*]"
    nodeID "Sophie Ziegler"
  ]
  node [
    id 73
    label "Tom Ziegler"
    x 0.3809
    y 0.1651
    shape "0.5000"
    triangle "ic"
    Blue "[191-*]"
    nodeID "Tom Ziegler"
  ]
  node [
    id 74
    label "Sarah Ziegler"
    x 0.3431
    y 0.061
    shape "0.5000"
    ellipse "ic"
    Blue "[103-*]"
    nodeID "Sarah Ziegler"
  ]
  node [
    id 75
    label "Paula Madalena Francesca Winicki"
    x 0.5792
    y 0.2872
    shape "0.5000"
    ellipse "ic"
    Blue "[561-*]"
    nodeID "Paula Madalena Francesca Winicki"
  ]
  node [
    id 76
    label "Canan Dagdelen"
    x 0.3928
    y 0.4957
    shape "0.5000"
    ellipse "ic"
    Blue "[668-*]"
    nodeID "Canan Dagdelen"
  ]
  node [
    id 77
    label "Herr Panowski"
    x 0.354
    y 0.3528
    shape "0.5000"
    triangle "ic"
    Blue "[74-*]"
    nodeID "Herr Panowski"
  ]
  node [
    id 78
    label "Giovanna Varese"
    x 0.6977
    y 0.3004
    shape "0.5000"
    ellipse "ic"
    Blue "[579-*]"
    nodeID "Giovanna Varese"
  ]
  node [
    id 79
    label "Marcella Varese"
    x 0.6628
    y 0.2364
    shape "0.5000"
    nodeID "Marcella Varese"
  ]
  node [
    id 80
    label "Nico Zenker"
    x 0.0866
    y 0.5531
    shape "0.5000"
    triangle "ic"
    Blue "[441-*]"
    nodeID "Nico Zenker"
  ]
  node [
    id 81
    label "Gina Varese"
    x 0.6777
    y 0.2616
    shape "0.5000"
    ellipse "ic"
    Blue "[516-*]"
    nodeID "Gina Varese"
  ]
  node [
    id 82
    label "Chromo Hoyonda"
    x 0.4303
    y 0.3759
    shape "0.5000"
    ellipse "ic"
    Blue "[448-*]"
    nodeID "Chromo Hoyonda"
  ]
  node [
    id 83
    label "Alfredo"
    x 0.6503
    y 0.4254
    shape "0.5000"
    triangle "ic"
    Blue "[299-*]"
    nodeID "Alfredo"
  ]
  node [
    id 84
    label "Francesco"
    x 0.6209
    y 0.5092
    shape "0.5000"
    nodeID "Francesco"
  ]
  node [
    id 85
    label "Professor Dr. Rudolf Tenge-Wegemmann"
    x 0.4214
    y 0.5625
    shape "0.5000"
    triangle "ic"
    Blue "[8-*]"
    nodeID "Professor Dr. Rudolf Tenge-Wegemmann"
  ]
  node [
    id 86
    label "Frau Horowitz"
    x 0.4465
    y 0.5446
    shape "0.5000"
    ellipse "ic"
    Blue "[647-*]"
    nodeID "Frau Horowitz"
  ]
  node [
    id 87
    label "Harry"
    x 0.5871
    y 0.3565
    shape "0.5000"
    triangle "ic"
    Blue "[482-*]"
    nodeID "Harry"
  ]
  node [
    id 88
    label "Zeki Kurtalan"
    x 0.7187
    y 0.6898
    shape "0.5000"
    triangle "ic"
    Blue "[572-*]"
    nodeID "Zeki Kurtalan"
  ]
  node [
    id 89
    label "Hildegard Scholz"
    x 0.0995
    y 0.47
    shape "0.5000"
    ellipse "ic"
    Yellow "[316-319]"
    nodeID "Hildegard Scholz"
  ]
  node [
    id 90
    label "Ernst August Thelen"
    x 0.1698
    y 0.5627
    shape "0.5000"
    triangle "ic"
    Yellow "[553]"
    nodeID "Ernst August Thelen"
  ]
  node [
    id 91
    label "Gernot"
    x 0.1732
    y 0.4656
    shape "0.5000"
    triangle "ic"
    Yellow "[]"
    nodeID "Gernot"
  ]
  node [
    id 92
    label "Fritjof"
    x 0.3177
    y 0.5687
    shape "0.5000"
    triangle "ic"
    Yellow "[257-*]"
    nodeID "Fritjof"
  ]
  node [
    id 93
    label "Herr Huetthusen"
    x 0.4046
    y 0.1882
    shape "0.5000"
    triangle "ic"
    Yellow "[]"
    nodeID "Herr Huetthusen"
  ]
  node [
    id 94
    label "Elfriede Dabelstein"
    x 0.2738
    y 0.1654
    shape "0.5000"
    ellipse "ic"
    Yellow "[408-469]"
    nodeID "Elfriede Dabelstein"
  ]
  node [
    id 95
    label "Lothar Boedefeld"
    x 0.244
    y 0.2229
    shape "0.5000"
    triangle "ic"
    Yellow "[471-*]"
    nodeID "Lothar Boedefeld"
  ]
  node [
    id 96
    label "Gundel Koch"
    x 0.045
    y 0.2138
    shape "0.5000"
    ellipse "ic"
    Yellow "[505-*]"
    nodeID "Gundel Koch"
  ]
  node [
    id 97
    label "Dr. Otto Pelsteiner"
    x 0.2189
    y 0.2121
    shape "0.5000"
    triangle "ic"
    Yellow "[191-*]"
    nodeID "Dr. Otto Pelsteiner"
  ]
  node [
    id 98
    label "Wolf Drewitz"
    x 0.4102
    y 0.2338
    shape "0.5000"
    triangle "ic"
    Yellow "[9-56]"
    nodeID "Wolf Drewitz"
  ]
  node [
    id 99
    label "Anja Herrlinger"
    x 0.2171
    y 0.254
    shape "0.5000"
    ellipse "ic"
    Yellow "[496-504]"
    nodeID "Anja Herrlinger"
  ]
  node [
    id 100
    label "Chris Barnsteg"
    x 0.3528
    y 0.3375
    shape "0.5000"
    ellipse "ic"
    Yellow "[4-278]"
    nodeID "Chris Barnsteg"
  ]
  node [
    id 101
    label "Dr. Rard Kirch"
    x 0.2247
    y 0.5155
    shape "0.5000"
    triangle "ic"
    Yellow "[235-283]"
    nodeID "Dr. Rard Kirch"
  ]
  node [
    id 102
    label "Gloria Birnbaum"
    x 0.1587
    y 0.5906
    shape "0.5000"
    ellipse "ic"
    Yellow "[238-271]"
    nodeID "Gloria Birnbaum"
  ]
  node [
    id 103
    label "Dagmar Hoffmeister"
    x 0.2555
    y 0.3831
    shape "0.5000"
    ellipse "ic"
    Yellow "[304-397]"
    nodeID "Dagmar Hoffmeister"
  ]
  node [
    id 104
    label "Inka Fuchs"
    x 0.2362
    y 0.6841
    shape "0.5000"
    ellipse "ic"
    Yellow "[343-427]"
    nodeID "Inka Fuchs"
  ]
  node [
    id 105
    label "Leo Klamm"
    x 0.2647
    y 0.7041
    shape "0.5000"
    triangle "ic"
    Yellow "[361-*]"
    nodeID "Leo Klamm"
  ]
  node [
    id 106
    label "Karin Atter"
    x 0.3314
    y 0.2325
    shape "0.5000"
    ellipse "ic"
    Yellow "[491-508]"
    nodeID "Karin Atter"
  ]
  node [
    id 107
    label "Elvira"
    x 0.2535
    y 0.2468
    shape "0.5000"
    ellipse "ic"
    Yellow "[]"
    nodeID "Elvira"
  ]
  node [
    id 108
    label "Rita Bassermann"
    x 0.3338
    y 0.3324
    shape "0.5000"
    ellipse "ic"
    Yellow "[229-*]"
    nodeID "Rita Bassermann"
  ]
  node [
    id 109
    label "Jo Zenker"
    x 0.2263
    y 0.3092
    shape "0.5000"
    triangle "ic"
    Yellow "[220-365]"
    nodeID "Jo Zenker"
  ]
  node [
    id 110
    label "Bianca Guther"
    x 0.5865
    y 0.4213
    shape "0.5000"
    ellipse "ic"
    Yellow "[41-147]"
    nodeID "Bianca Guther"
  ]
  node [
    id 111
    label "Celin Otto Wilhelm Kern"
    x 0.9057
    y 0.2074
    shape "0.5000"
    triangle "ic"
    Yellow "[117-193]"
    nodeID "Celin Otto Wilhelm Kern"
  ]
  node [
    id 112
    label "Dominique Mourrait"
    x 0.4821
    y 0.5246
    shape "0.5000"
    ellipse "ic"
    Yellow "[162-431]"
    nodeID "Dominique Mourrait"
  ]
  node [
    id 113
    label "Christoph Bogner"
    x 0.5477
    y 0.2917
    shape "0.5000"
    triangle "ic"
    Yellow "[235-400]"
    nodeID "Christoph Bogner"
  ]
  node [
    id 114
    label "Kornelia Harnisch"
    x 0.5317
    y 0.2695
    shape "0.5000"
    ellipse "ic"
    Yellow "[159-342]"
    nodeID "Kornelia Harnisch"
  ]
  node [
    id 115
    label "Carola"
    x 0.3931
    y 0.1715
    shape "0.5000"
    ellipse "ic"
    Yellow "[358-458]"
    nodeID "Carola"
  ]
  node [
    id 116
    label "Erika"
    x 0.3495
    y 0.1643
    shape "0.5000"
    ellipse "ic"
    Yellow "[459-485]"
    nodeID "Erika"
  ]
  node [
    id 117
    label "Margot Rantzow"
    x 0.688
    y 0.2954
    shape "0.5000"
    ellipse "ic"
    Yellow "[334-492]"
    nodeID "Margot Rantzow"
  ]
  node [
    id 118
    label "Holger Graf Jakob Maria Grauvogel"
    x 0.175
    y 0.2142
    shape "0.5000"
    triangle "ic"
    Yellow "[448-458]"
    nodeID "Holger Graf Jakob Maria Grauvogel"
  ]
  node [
    id 119
    label "Timo Zenker"
    x 0.1273
    y 0.3528
    shape "0.5000"
    triangle "ic"
    Yellow "[220-310/566/665]"
    nodeID "Timo Zenker"
  ]
  node [
    id 120
    label "Simone Fitz"
    x 0.0471
    y 0.3601
    shape "0.5000"
    ellipse "ic"
    Yellow "[281-*]"
    nodeID "Simone Fitz"
  ]
  node [
    id 121
    label "Nina Winter"
    x 0.4549
    y 0.6303
    shape "0.5000"
    ellipse "ic"
    Yellow "[86-97]"
    nodeID "Nina Winter"
  ]
  node [
    id 122
    label "Petra Strauss"
    x 0.443
    y 0.5634
    shape "0.5000"
    ellipse "ic"
    Yellow "[292-344]"
    nodeID "Petra Strauss"
  ]
  node [
    id 123
    label "Corinna Marx"
    x 0.4255
    y 0.5349
    shape "0.5000"
    ellipse "ic"
    Yellow "[360-639]"
    nodeID "Corinna Marx"
  ]
  node [
    id 124
    label "Knut Magirus"
    x 0.6496
    y 0.4573
    shape "0.5000"
    triangle "ic"
    Yellow "[363-385]"
    nodeID "Knut Magirus"
  ]
  node [
    id 125
    label "Natale Pavarotti"
    x 0.6415
    y 0.4051
    shape "0.5000"
    triangle "ic"
    Yellow "[386-566]"
    nodeID "Natale Pavarotti"
  ]
  node [
    id 126
    label "Dr. Manfred Pauli"
    x 0.5931
    y 0.643
    shape "0.5000"
    triangle "ic"
    Yellow "[176-494]"
    nodeID "Dr. Manfred Pauli"
  ]
  node [
    id 127
    label "Flora"
    x 0.5027
    y 0.387
    shape "0.5000"
    ellipse "ic"
    Yellow "[232-373]"
    nodeID "Flora"
  ]
  node [
    id 128
    label "Gert Weinbauer"
    x 0.4034
    y 0.6158
    shape "0.5000"
    triangle "ic"
    Yellow "[50-105]"
    nodeID "Gert Weinbauer"
  ]
  node [
    id 129
    label "Dimitri Pallas"
    x 0.6477
    y 0.1722
    shape "0.5000"
    triangle "ic"
    Yellow "[163-279]"
    nodeID "Dimitri Pallas"
  ]
  node [
    id 130
    label "Paul Bennarsch"
    x 0.9551
    y 0.4482
    shape "0.5000"
    triangle "ic"
    Yellow "[85/97]"
    nodeID "Paul Bennarsch"
  ]
  node [
    id 131
    label "Elfie Kronmayr"
    x 0.3072
    y 0.2134
    shape "0.5000"
    ellipse "ic"
    Yellow "[1-48]"
    nodeID "Elfie Kronmayr"
  ]
  node [
    id 132
    label "Sigi Kronmayr"
    x 0.1402
    y 0.0482
    shape "0.5000"
    triangle "ic"
    Yellow "[1-48]"
    nodeID "Sigi Kronmayr"
  ]
  node [
    id 133
    label "Tante Betty Elizabeth Susan Schiller"
    x 0.3315
    y 0.5357
    shape "0.5000"
    ellipse "ic"
    Gray "[577-610]"
    nodeID "Tante Betty Elizabeth Susan Schiller"
  ]
  node [
    id 134
    label "August"
    x 0.2645
    y 0.5422
    shape "0.5000"
    triangle "ic"
    Gray "[287-?]"
    nodeID "August"
  ]
  node [
    id 135
    label "Wolf-Dieter Dabelstein"
    x 0.2992
    y 0.2174
    shape "0.5000"
    triangle "ic"
    Gray "[360-407]"
    nodeID "Wolf-Dieter Dabelstein"
  ]
  node [
    id 136
    label "Willy Jenner"
    x 0.1897
    y 0.1211
    shape "0.5000"
    triangle "ic"
    Gray "[428-433]"
    nodeID "Willy Jenner"
  ]
  node [
    id 137
    label "Jaruslav Winicki"
    x 0.5113
    y 0.2205
    shape "0.5000"
    triangle "ic"
    Gray "[249-451]"
    nodeID "Jaruslav Winicki"
  ]
  node [
    id 138
    label "Benno Zimmermann"
    x 0.4159
    y 0.1542
    shape "0.5000"
    triangle "ic"
    Gray "[13-157]"
    nodeID "Benno Zimmermann"
  ]
  node [
    id 139
    label "Karl Koch"
    x 0.1748
    y 0.3152
    shape "0.5000"
    triangle "ic"
    Gray "[246/247]"
    nodeID "Karl Koch"
  ]
  node [
    id 140
    label "Gottlieb Griese"
    x 0.4408
    y 0.6262
    shape "0.5000"
    triangle "ic"
    Gray "[6-197]"
    nodeID "Gottlieb Griese"
  ]
  node [
    id 141
    label "Friedhelm Ziegler"
    x 0.3089
    y 0.0651
    shape "0.5000"
    triangle "ic"
    Gray "[61-295]"
    nodeID "Friedhelm Ziegler"
  ]
  node [
    id 142
    label "Max Zenker-gefunden"
    x 0.2313
    y 0.3598
    shape "0.5000"
    triangle "ic"
    Gray "[76-632]"
    nodeID "Max Zenker-gefunden"
  ]
  node [
    id 143
    label "Meike Schildknecht"
    x 0.7395
    y 0.5284
    shape "0.5000"
    ellipse "ic"
    Gray "[7-84]"
    nodeID "Meike Schildknecht"
  ]
  node [
    id 144
    label "Jean-Luc Mourrait"
    x 0.6023
    y 0.5625
    shape "0.5000"
    triangle "ic"
    Gray "[162-429]"
    nodeID "Jean-Luc Mourrait"
  ]
  node [
    id 145
    label "Guenther Rantzow"
    x 0.7079
    y 0.3276
    shape "0.5000"
    triangle "ic"
    Gray "[334-466]"
    nodeID "Guenther Rantzow"
  ]
  node [
    id 146
    label "John"
    x 0.4749
    y 0.2004
    shape "0.5000"
    triangle "ic"
    Gray "[527]"
    nodeID "John"
  ]
  node [
    id 147
    label "Alexandros Kling"
    x 0.5304
    y 0.2113
    shape "0.5000"
    triangle "ic"
    Gray "[652/653]"
    nodeID "Alexandros Kling"
  ]
  node [
    id 148
    label "Dieter Rantzow"
    x 0.5575
    y 0.4327
    shape "0.5000"
    triangle "ic"
    Gray "[313-520]"
    nodeID "Dieter Rantzow"
  ]
  node [
    id 149
    label "Philo Bennarsch"
    x 0.82
    y 0.5215
    shape "0.5000"
    ellipse "ic"
    Gray "[1-116]"
    nodeID "Philo Bennarsch"
  ]
  node [
    id 150
    label "Joschi Bennarsch"
    x 0.9595
    y 0.4107
    shape "0.5000"
    triangle "ic"
    Gray "[1-51]"
    nodeID "Joschi Bennarsch"
  ]
  node [
    id 151
    label "Giancarlo"
    x 0.6207
    y 0.5253
    shape "0.5000"
    triangle "ic"
    Gray "[299-592]"
    nodeID "Giancarlo"
  ]
  node [
    id 152
    label "Hubert Panowak"
    x 0.6985
    y 0.5687
    shape "0.5000"
    triangle "ic"
    Gray "[9-10]"
    nodeID "Hubert Panowak"
  ]
  node [
    id 153
    label "Gustav Scholz"
    x 0.0933
    y 0.4439
    shape "0.5000"
    triangle "ic"
    White "[316-319]"
    nodeID "Gustav Scholz"
  ]
  node [
    id 154
    label "Paul Nolte"
    x 0.2634
    y 0.6203
    shape "0.5000"
    triangle "ic"
    White "[4-475]"
    nodeID "Paul Nolte"
  ]
  node [
    id 155
    label "Theo Nolte"
    x 0.2428
    y 0.6036
    shape "0.5000"
    triangle "ic"
    White "[4-475]"
    nodeID "Theo Nolte"
  ]
  node [
    id 156
    label "Fritz"
    x 0.1108
    y 0.435
    shape "0.5000"
    triangle "ic"
    White "[203-597]"
    nodeID "Fritz"
  ]
  node [
    id 157
    label "Eduard"
    x 0.1349
    y 0.5041
    shape "0.5000"
    nodeID "Eduard"
  ]
  node [
    id 158
    label "Egbert"
    x 0.1716
    y 0.479
    shape "0.5000"
    nodeID "Egbert"
  ]
  node [
    id 159
    label "Muetze"
    x 0.1953
    y 0.5122
    shape "0.5000"
    nodeID "Muetze"
  ]
  node [
    id 160
    label "Hannes"
    x 0.124
    y 0.5044
    shape "0.5000"
    nodeID "Hannes"
  ]
  node [
    id 161
    label "Dora Witt"
    x 0.2202
    y 0.6338
    shape "0.5000"
    ellipse "ic"
    White "[89-*]"
    nodeID "Dora Witt"
  ]
  node [
    id 162
    label "Katharina Jenner"
    x 0.1919
    y 0.2761
    shape "0.5000"
    ellipse "ic"
    White "[428-433]"
    nodeID "Katharina Jenner"
  ]
  node [
    id 163
    label "v164"
    x 0.4022
    y 0.6462
    shape "0.5000"
    triangle "ic"
    White "[249-*]"
    nodeID "v164"
  ]
  node [
    id 164
    label "Martha"
    x 0.1543
    y 0.3404
    shape "0.5000"
    ellipse "ic"
    White "[157-*]"
    nodeID "Martha"
  ]
  node [
    id 165
    label "Bruno Skabowski"
    x 0.1776
    y 0.2081
    shape "0.5000"
    triangle "ic"
    White "[157-*]"
    nodeID "Bruno Skabowski"
  ]
  node [
    id 166
    label "v165"
    x 0.6051
    y 0.707
    shape "0.5000"
    ellipse "ic"
    White "[6-197]"
    nodeID "v165"
  ]
  node [
    id 167
    label "Herr Hoffmeister"
    x 0.3992
    y 0.209
    shape "0.5000"
    triangle "ic"
    White "[304-397]"
    nodeID "Herr Hoffmeister"
  ]
  node [
    id 168
    label "Sue"
    x 0.362
    y 0.6749
    shape "0.5000"
    ellipse "ic"
    White "[301-*]"
    nodeID "Sue"
  ]
  node [
    id 169
    label "Theresa Zenker"
    x 0.1349
    y 0.3527
    shape "0.5000"
    ellipse "ic"
    White "[220-*]"
    nodeID "Theresa Zenker"
  ]
  node [
    id 170
    label "v171"
    x 0.5632
    y 0.6936
    shape "0.5000"
    ellipse "ic"
    White "[162-429]"
    nodeID "v171"
  ]
  node [
    id 171
    label "Herr Floether"
    x 0.515
    y 0.6891
    shape "0.5000"
    triangle "ic"
    White "[6-262]"
    nodeID "Herr Floether"
  ]
  node [
    id 172
    label "Der Englaender"
    x 0.6851
    y 0.4349
    shape "0.5000"
    nodeID "Der Englaender"
  ]
  node [
    id 173
    label "Inge Kling"
    x 0.5356
    y 0.1921
    shape "0.5000"
    ellipse "ic"
    White "[387-*]"
    nodeID "Inge Kling"
  ]
  node [
    id 174
    label "Natascha Rantzow"
    x 0.6722
    y 0.623
    shape "0.5000"
    ellipse "ic"
    White "[313-520]"
    nodeID "Natascha Rantzow"
  ]
  node [
    id 175
    label "Doris Rantzow"
    x 0.6067
    y 0.6804
    shape "0.5000"
    ellipse "ic"
    White "[313-520]"
    nodeID "Doris Rantzow"
  ]
  node [
    id 176
    label "v177"
    x 0.8139
    y 0.506
    shape "0.5000"
    ellipse "ic"
    White "[137-556]"
    nodeID "v177"
  ]
  node [
    id 177
    label "Ernst+Lisl Wiesenhuber"
    x 0.4673
    y 0.2033
    shape "0.5000"
    box "ic"
    Yellow "[156-*]"
    nodeID "Ernst+Lisl Wiesenhuber"
  ]
  node [
    id 178
    label "Julia von der Marwitz"
    x 0.2686
    y 0.4543
    shape "0.5000"
    box "ic"
    Green "[484-565]"
    nodeID "Julia von der Marwitz"
  ]
  node [
    id 179
    label "v180"
    x 0.1903
    y 0.4581
    shape "0.5000"
    diamond "ic"
    Black "[316-319]"
    nodeID "v180"
  ]
  node [
    id 180
    label "v181"
    x 0.2197
    y 0.4301
    shape "0.5000"
    diamond "ic"
    Black "[455-597]"
    nodeID "v181"
  ]
  node [
    id 181
    label "v182"
    x 0.208
    y 0.4062
    shape "0.5000"
    diamond "ic"
    Black "[203-597]"
    nodeID "v182"
  ]
  node [
    id 182
    label "v183"
    x 0.229
    y 0.4848
    shape "0.5000"
    diamond "ic"
    Pink "[553]"
    nodeID "v183"
  ]
  node [
    id 183
    label "v184"
    x 0.2192
    y 0.4521
    shape "0.5000"
    diamond "ic"
    Black "[203-597]"
    nodeID "v184"
  ]
  node [
    id 184
    label "v185"
    x 0.225
    y 0.4636
    shape "0.5000"
    nodeID "v185"
  ]
  node [
    id 185
    label "v186"
    x 0.2745
    y 0.5581
    shape "0.5000"
    diamond "ic"
    Black "[89-*]"
    nodeID "v186"
  ]
  node [
    id 186
    label "v187"
    x 0.2286
    y 0.1877
    shape "0.5000"
    diamond "ic"
    Black "[428-433]"
    nodeID "v187"
  ]
  node [
    id 187
    label "v188"
    x 0.4426
    y 0.3102
    shape "0.5000"
    diamond "ic"
    Black "[249-451]"
    nodeID "v188"
  ]
  node [
    id 188
    label "v189"
    x 0.2415
    y 0.286
    shape "0.5000"
    diamond "ic"
    Pink "[157-*]"
    nodeID "v189"
  ]
  node [
    id 189
    label "v190"
    x 0.3138
    y 0.3247
    shape "0.5000"
    diamond "ic"
    Black "[159-549]"
    nodeID "v190"
  ]
  node [
    id 190
    label "v191"
    x 0.0915
    y 0.2784
    shape "0.5000"
    diamond "ic"
    Black "[]"
    nodeID "v191"
  ]
  node [
    id 191
    label "v192"
    x 0.3673
    y 0.5317
    shape "0.5000"
    diamond "ic"
    Pink "[6-197]"
    nodeID "v192"
  ]
  node [
    id 192
    label "v193"
    x 0.5587
    y 0.6393
    shape "0.5000"
    diamond "ic"
    Black "[6-197]"
    nodeID "v193"
  ]
  node [
    id 193
    label "v194"
    x 0.3207
    y 0.598
    shape "0.5000"
    diamond "ic"
    Orange "[301-*]"
    nodeID "v194"
  ]
  node [
    id 194
    label "v195"
    x 0.3922
    y 0.383
    shape "0.5000"
    diamond "ic"
    Pink "[1-*]"
    nodeID "v195"
  ]
  node [
    id 195
    label "v196"
    x 0.3546
    y 0.2555
    shape "0.5000"
    diamond "ic"
    Black "[1-*]"
    nodeID "v196"
  ]
  node [
    id 196
    label "v197"
    x 0.3086
    y 0.1371
    shape "0.5000"
    diamond "ic"
    Black "[61-295]"
    nodeID "v197"
  ]
  node [
    id 197
    label "v198"
    x 0.3563
    y 0.2288
    shape "0.5000"
    diamond "ic"
    Black "[13-157]"
    nodeID "v198"
  ]
  node [
    id 198
    label "v199"
    x 0.2841
    y 0.3302
    shape "0.5000"
    diamond "ic"
    Orange "[220-*]"
    nodeID "v199"
  ]
  node [
    id 199
    label "v200"
    x 0.2218
    y 0.3527
    shape "0.5000"
    diamond "ic"
    Black "[220-*]"
    nodeID "v200"
  ]
  node [
    id 200
    label "v201"
    x 0.293
    y 0.5322
    shape "0.5000"
    diamond "ic"
    Orange "[384-548]"
    nodeID "v201"
  ]
  node [
    id 201
    label "v202"
    x 0.8057
    y 0.3431
    shape "0.5000"
    diamond "ic"
    Black "[117-368]"
    nodeID "v202"
  ]
  node [
    id 202
    label "v203"
    x 0.6684
    y 0.488
    shape "0.5000"
    diamond "ic"
    Black "[2-62]"
    nodeID "v203"
  ]
  node [
    id 203
    label "v204"
    x 0.5362
    y 0.6106
    shape "0.5000"
    diamond "ic"
    Black "[162-429]"
    nodeID "v204"
  ]
  node [
    id 204
    label "v205"
    x 0.4789
    y 0.4564
    shape "0.5000"
    diamond "ic"
    Pink "[1-658]"
    nodeID "v205"
  ]
  node [
    id 205
    label "v206"
    x 0.6255
    y 0.3548
    shape "0.5000"
    diamond "ic"
    Black "[334-466]"
    nodeID "v206"
  ]
  node [
    id 206
    label "v207"
    x 0.6246
    y 0.3168
    shape "0.5000"
    diamond "ic"
    Orange "[516-*]"
    nodeID "v207"
  ]
  node [
    id 207
    label "v208"
    x 0.2914
    y 0.1837
    shape "0.5000"
    diamond "ic"
    Orange "[402-*]"
    nodeID "v208"
  ]
  node [
    id 208
    label "v209"
    x 0.5087
    y 0.4624
    shape "0.5000"
    diamond "ic"
    Orange "[2-*]"
    nodeID "v209"
  ]
  node [
    id 209
    label "v210"
    x 0.4829
    y 0.5438
    shape "0.5000"
    diamond "ic"
    Black "[6-262]"
    nodeID "v210"
  ]
  node [
    id 210
    label "v211"
    x 0.4554
    y 0.2974
    shape "0.5000"
    diamond "ic"
    Orange "[518-*]"
    nodeID "v211"
  ]
  node [
    id 211
    label "v212"
    x 0.5011
    y 0.3187
    shape "0.5000"
    diamond "ic"
    Black "[387-491]"
    nodeID "v212"
  ]
  node [
    id 212
    label "v213"
    x 0.5979
    y 0.5739
    shape "0.5000"
    diamond "ic"
    Pink "[313-520]"
    nodeID "v213"
  ]
  node [
    id 213
    label "v214"
    x 0.4535
    y 0.4893
    shape "0.5000"
    diamond "ic"
    Pink "[86-97]"
    nodeID "v214"
  ]
  node [
    id 214
    label "v215"
    x 0.5052
    y 0.5936
    shape "0.5000"
    diamond "ic"
    Pink "[6-262]"
    nodeID "v215"
  ]
  node [
    id 215
    label "v216"
    x 0.4981
    y 0.2869
    shape "0.5000"
    diamond "ic"
    Pink "[387-*]"
    nodeID "v216"
  ]
  node [
    id 216
    label "v217"
    x 0.4527
    y 0.6267
    shape "0.5000"
    diamond "ic"
    Orange "[575-*]"
    nodeID "v217"
  ]
  node [
    id 217
    label "v218"
    x 0.5182
    y 0.4278
    shape "0.5000"
    diamond "ic"
    Pink "[9-*]"
    nodeID "v218"
  ]
  node [
    id 218
    label "v219"
    x 0.5499
    y 0.3384
    shape "0.5000"
    diamond "ic"
    Orange "[9-559]"
    nodeID "v219"
  ]
  node [
    id 219
    label "v220"
    x 0.8953
    y 0.4757
    shape "0.5000"
    diamond "ic"
    Black "[1-51]"
    nodeID "v220"
  ]
  node [
    id 220
    label "v221"
    x 0.6523
    y 0.3993
    shape "0.5000"
    diamond "ic"
    Black "[137-556]"
    nodeID "v221"
  ]
  node [
    id 221
    label "v222"
    x 0.2999
    y 0.1148
    shape "0.5000"
    diamond "ic"
    Orange "[1-48]"
    nodeID "v222"
  ]
  node [
    id 222
    label "v223"
    x 0.6439
    y 0.5001
    shape "0.5000"
    diamond "ic"
    Black "[9-10]"
    nodeID "v223"
  ]
  node [
    id 223
    label "v224"
    x 0.7449
    y 0.4772
    shape "0.5000"
    diamond "ic"
    Black "[137-556]"
    nodeID "v224"
  ]
  node [
    id 224
    label "v225"
    x 0.3012
    y 0.5184
    shape "0.5000"
    diamond "ic"
    Black "[4-475]"
    nodeID "v225"
  ]
  node [
    id 225
    label "v226"
    x 0.4585
    y 0.5165
    shape "0.5000"
    empty "ic"
    White "[249-*]"
    nodeID "v226"
  ]
  node [
    id 226
    label "v227"
    x 0.2263
    y 0.2879
    shape "0.5000"
    empty "ic"
    White "[14-*]"
    nodeID "v227"
  ]
  node [
    id 227
    label "v228"
    x 0.337
    y 0.3145
    shape "0.5000"
    empty "ic"
    White "[304-397]"
    nodeID "v228"
  ]
  node [
    id 228
    label "v229"
    x 0.5125
    y 0.3659
    shape "0.5000"
    empty "ic"
    White "[299-*]"
    nodeID "v229"
  ]
  node [
    id 229
    label "v230"
    x 0.1649
    y 0.5216
    shape "0.5000"
    empty "ic"
    White "[346-*]"
    nodeID "v230"
  ]
  node [
    id 230
    label "v231"
    x 0.5794
    y 0.47
    shape "0.5000"
    empty "ic"
    White "[6-262]"
    nodeID "v231"
  ]
  node [
    id 231
    label "v232"
    x 0.4823
    y 0.2907
    shape "0.5000"
    empty "ic"
    White "[518-*]"
    nodeID "v232"
  ]
  node [
    id 232
    label "v233"
    x 0.3498
    y 0.4141
    shape "0.5000"
    empty "ic"
    White "[297-*]"
    nodeID "v233"
  ]
  node [
    id 233
    label "v234"
    x 0.3597
    y 0.5766
    shape "0.5000"
    empty "ic"
    White "[301-*]"
    nodeID "v234"
  ]
  edge [
    source 0
    target 5
    key 0
    weight 1.0
  ]
  edge [
    source 0
    target 6
    key 0
    weight 1.0
  ]
  edge [
    source 0
    target 100
    key 0
    weight 1.0
  ]
  edge [
    source 0
    target 224
    key 0
    weight 1.0
  ]
  edge [
    source 1
    target 7
    key 0
    weight 1.0
  ]
  edge [
    source 1
    target 6
    key 0
    weight 1.0
  ]
  edge [
    source 1
    target 9
    key 0
    weight 1.0
  ]
  edge [
    source 2
    target 162
    key 0
    weight 1.0
  ]
  edge [
    source 2
    target 164
    key 0
    weight 1.0
  ]
  edge [
    source 2
    target 6
    key 0
    weight 1.0
  ]
  edge [
    source 2
    target 68
    key 0
    weight 1.0
  ]
  edge [
    source 2
    target 188
    key 0
    weight 1.0
  ]
  edge [
    source 2
    target 189
    key 0
    weight 1.0
  ]
  edge [
    source 3
    target 139
    key 0
    weight 1.0
  ]
  edge [
    source 3
    target 68
    key 0
    weight 1.0
  ]
  edge [
    source 3
    target 4
    key 0
    weight 1.0
  ]
  edge [
    source 3
    target 22
    key 0
    weight 1.0
  ]
  edge [
    source 3
    target 6
    key 0
    weight 1.0
  ]
  edge [
    source 3
    target 189
    key 0
    weight 1.0
  ]
  edge [
    source 4
    target 97
    key 0
    weight 1.0
  ]
  edge [
    source 4
    target 18
    key 0
    weight 1.0
  ]
  edge [
    source 4
    target 10
    key 0
    weight 1.0
  ]
  edge [
    source 4
    target 29
    key 0
    weight 1.0
  ]
  edge [
    source 4
    target 100
    key 0
    weight 1.0
  ]
  edge [
    source 5
    target 180
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 178
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 7
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 103
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 20
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 22
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 180
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 181
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 182
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 183
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 184
    key 0
    weight 1.0
  ]
  edge [
    source 7
    target 16
    key 0
    weight 1.0
  ]
  edge [
    source 7
    target 14
    key 0
    weight 1.0
  ]
  edge [
    source 7
    target 39
    key 0
    weight 1.0
  ]
  edge [
    source 7
    target 134
    key 0
    weight 1.0
  ]
  edge [
    source 7
    target 92
    key 0
    weight 1.0
  ]
  edge [
    source 7
    target 133
    key 0
    weight 1.0
  ]
  edge [
    source 7
    target 64
    key 0
    weight 1.0
  ]
  edge [
    source 7
    target 185
    key 0
    weight 1.0
  ]
  edge [
    source 8
    target 189
    key 0
    weight 1.0
  ]
  edge [
    source 8
    target 10
    key 0
    weight 1.0
  ]
  edge [
    source 8
    target 68
    key 0
    weight 1.0
  ]
  edge [
    source 8
    target 9
    key 0
    weight 1.0
  ]
  edge [
    source 9
    target 58
    key 0
    weight 1.0
  ]
  edge [
    source 9
    target 60
    key 0
    weight 1.0
  ]
  edge [
    source 9
    target 228
    key 0
    weight 1.0
  ]
  edge [
    source 9
    target 225
    key 0
    weight 1.0
  ]
  edge [
    source 10
    target 18
    key 0
    weight 1.0
  ]
  edge [
    source 10
    target 106
    key 0
    weight 1.0
  ]
  edge [
    source 10
    target 107
    key 0
    weight 1.0
  ]
  edge [
    source 10
    target 22
    key 0
    weight 1.0
  ]
  edge [
    source 10
    target 131
    key 0
    weight 1.0
  ]
  edge [
    source 10
    target 198
    key 0
    weight 1.0
  ]
  edge [
    source 10
    target 226
    key 0
    weight 1.0
  ]
  edge [
    source 10
    target 197
    key 0
    weight 1.0
  ]
  edge [
    source 10
    target 109
    key 0
    weight 1.0
  ]
  edge [
    source 11
    target 12
    key 0
    weight 1.0
  ]
  edge [
    source 11
    target 95
    key 0
    weight 1.0
  ]
  edge [
    source 11
    target 99
    key 0
    weight 1.0
  ]
  edge [
    source 11
    target 226
    key 0
    weight 1.0
  ]
  edge [
    source 13
    target 101
    key 0
    weight 1.0
  ]
  edge [
    source 13
    target 62
    key 0
    weight 1.0
  ]
  edge [
    source 13
    target 63
    key 0
    weight 1.0
  ]
  edge [
    source 13
    target 22
    key 0
    weight 1.0
  ]
  edge [
    source 13
    target 14
    key 0
    weight 1.0
  ]
  edge [
    source 13
    target 20
    key 0
    weight 1.0
  ]
  edge [
    source 14
    target 25
    key 0
    weight 1.0
  ]
  edge [
    source 14
    target 47
    key 0
    weight 1.0
  ]
  edge [
    source 14
    target 232
    key 0
    weight 1.0
  ]
  edge [
    source 15
    target 133
    key 0
    weight 1.0
  ]
  edge [
    source 15
    target 105
    key 0
    weight 1.0
  ]
  edge [
    source 15
    target 193
    key 0
    weight 1.0
  ]
  edge [
    source 15
    target 70
    key 0
    weight 1.0
  ]
  edge [
    source 15
    target 233
    key 0
    weight 1.0
  ]
  edge [
    source 15
    target 104
    key 0
    weight 1.0
  ]
  edge [
    source 16
    target 76
    key 0
    weight 1.0
  ]
  edge [
    source 16
    target 37
    key 0
    weight 1.0
  ]
  edge [
    source 16
    target 66
    key 0
    weight 1.0
  ]
  edge [
    source 16
    target 65
    key 0
    weight 1.0
  ]
  edge [
    source 16
    target 193
    key 0
    weight 1.0
  ]
  edge [
    source 16
    target 194
    key 0
    weight 1.0
  ]
  edge [
    source 17
    target 99
    key 0
    weight 1.0
  ]
  edge [
    source 17
    target 93
    key 0
    weight 1.0
  ]
  edge [
    source 17
    target 95
    key 0
    weight 1.0
  ]
  edge [
    source 17
    target 135
    key 0
    weight 1.0
  ]
  edge [
    source 17
    target 195
    key 0
    weight 1.0
  ]
  edge [
    source 17
    target 194
    key 0
    weight 1.0
  ]
  edge [
    source 18
    target 94
    key 0
    weight 1.0
  ]
  edge [
    source 18
    target 135
    key 0
    weight 1.0
  ]
  edge [
    source 18
    target 195
    key 0
    weight 1.0
  ]
  edge [
    source 18
    target 196
    key 0
    weight 1.0
  ]
  edge [
    source 19
    target 142
    key 0
    weight 1.0
  ]
  edge [
    source 19
    target 27
    key 0
    weight 1.0
  ]
  edge [
    source 19
    target 198
    key 0
    weight 1.0
  ]
  edge [
    source 19
    target 108
    key 0
    weight 1.0
  ]
  edge [
    source 19
    target 26
    key 0
    weight 1.0
  ]
  edge [
    source 19
    target 199
    key 0
    weight 1.0
  ]
  edge [
    source 19
    target 69
    key 0
    weight 1.0
  ]
  edge [
    source 20
    target 44
    key 0
    weight 1.0
  ]
  edge [
    source 20
    target 22
    key 0
    weight 1.0
  ]
  edge [
    source 20
    target 191
    key 0
    weight 1.0
  ]
  edge [
    source 20
    target 56
    key 0
    weight 1.0
  ]
  edge [
    source 21
    target 191
    key 0
    weight 1.0
  ]
  edge [
    source 21
    target 22
    key 0
    weight 1.0
  ]
  edge [
    source 22
    target 232
    key 0
    weight 1.0
  ]
  edge [
    source 23
    target 112
    key 0
    weight 1.0
  ]
  edge [
    source 23
    target 37
    key 0
    weight 1.0
  ]
  edge [
    source 23
    target 61
    key 0
    weight 1.0
  ]
  edge [
    source 24
    target 113
    key 0
    weight 1.0
  ]
  edge [
    source 24
    target 60
    key 0
    weight 1.0
  ]
  edge [
    source 24
    target 114
    key 0
    weight 1.0
  ]
  edge [
    source 24
    target 48
    key 0
    weight 1.0
  ]
  edge [
    source 25
    target 77
    key 0
    weight 1.0
  ]
  edge [
    source 25
    target 30
    key 0
    weight 1.0
  ]
  edge [
    source 25
    target 42
    key 0
    weight 1.0
  ]
  edge [
    source 25
    target 41
    key 0
    weight 1.0
  ]
  edge [
    source 25
    target 76
    key 0
    weight 1.0
  ]
  edge [
    source 25
    target 178
    key 0
    weight 1.0
  ]
  edge [
    source 25
    target 69
    key 0
    weight 1.0
  ]
  edge [
    source 26
    target 44
    key 0
    weight 1.0
  ]
  edge [
    source 26
    target 200
    key 0
    weight 1.0
  ]
  edge [
    source 27
    target 31
    key 0
    weight 1.0
  ]
  edge [
    source 27
    target 200
    key 0
    weight 1.0
  ]
  edge [
    source 27
    target 30
    key 0
    weight 1.0
  ]
  edge [
    source 28
    target 207
    key 0
    weight 1.0
  ]
  edge [
    source 28
    target 46
    key 0
    weight 1.0
  ]
  edge [
    source 29
    target 195
    key 0
    weight 1.0
  ]
  edge [
    source 29
    target 118
    key 0
    weight 1.0
  ]
  edge [
    source 29
    target 207
    key 0
    weight 1.0
  ]
  edge [
    source 30
    target 229
    key 0
    weight 1.0
  ]
  edge [
    source 31
    target 229
    key 0
    weight 1.0
  ]
  edge [
    source 32
    target 201
    key 0
    weight 1.0
  ]
  edge [
    source 32
    target 202
    key 0
    weight 1.0
  ]
  edge [
    source 33
    target 202
    key 0
    weight 1.0
  ]
  edge [
    source 33
    target 34
    key 0
    weight 1.0
  ]
  edge [
    source 34
    target 110
    key 0
    weight 1.0
  ]
  edge [
    source 34
    target 36
    key 0
    weight 1.0
  ]
  edge [
    source 35
    target 201
    key 0
    weight 1.0
  ]
  edge [
    source 35
    target 111
    key 0
    weight 1.0
  ]
  edge [
    source 36
    target 76
    key 0
    weight 1.0
  ]
  edge [
    source 36
    target 44
    key 0
    weight 1.0
  ]
  edge [
    source 36
    target 43
    key 0
    weight 1.0
  ]
  edge [
    source 37
    target 232
    key 0
    weight 1.0
  ]
  edge [
    source 37
    target 112
    key 0
    weight 1.0
  ]
  edge [
    source 37
    target 123
    key 0
    weight 1.0
  ]
  edge [
    source 38
    target 44
    key 0
    weight 1.0
  ]
  edge [
    source 38
    target 54
    key 0
    weight 1.0
  ]
  edge [
    source 38
    target 204
    key 0
    weight 1.0
  ]
  edge [
    source 39
    target 44
    key 0
    weight 1.0
  ]
  edge [
    source 39
    target 204
    key 0
    weight 1.0
  ]
  edge [
    source 40
    target 61
    key 0
    weight 1.0
  ]
  edge [
    source 40
    target 54
    key 0
    weight 1.0
  ]
  edge [
    source 40
    target 228
    key 0
    weight 1.0
  ]
  edge [
    source 40
    target 206
    key 0
    weight 1.0
  ]
  edge [
    source 41
    target 42
    key 0
    weight 1.0
  ]
  edge [
    source 42
    target 77
    key 0
    weight 1.0
  ]
  edge [
    source 42
    target 76
    key 0
    weight 1.0
  ]
  edge [
    source 43
    target 52
    key 0
    weight 1.0
  ]
  edge [
    source 43
    target 52
    key 1
    weight 1.0
  ]
  edge [
    source 43
    target 208
    key 0
    weight 1.0
  ]
  edge [
    source 43
    target 144
    key 0
    weight 1.0
  ]
  edge [
    source 43
    target 50
    key 0
    weight 1.0
  ]
  edge [
    source 44
    target 50
    key 0
    weight 1.0
  ]
  edge [
    source 44
    target 82
    key 0
    weight 1.0
  ]
  edge [
    source 44
    target 123
    key 0
    weight 1.0
  ]
  edge [
    source 44
    target 122
    key 0
    weight 1.0
  ]
  edge [
    source 44
    target 86
    key 0
    weight 1.0
  ]
  edge [
    source 44
    target 85
    key 0
    weight 1.0
  ]
  edge [
    source 44
    target 208
    key 0
    weight 1.0
  ]
  edge [
    source 44
    target 209
    key 0
    weight 1.0
  ]
  edge [
    source 44
    target 213
    key 0
    weight 1.0
  ]
  edge [
    source 45
    target 209
    key 0
    weight 1.0
  ]
  edge [
    source 45
    target 214
    key 0
    weight 1.0
  ]
  edge [
    source 45
    target 126
    key 0
    weight 1.0
  ]
  edge [
    source 45
    target 230
    key 0
    weight 1.0
  ]
  edge [
    source 46
    target 195
    key 0
    weight 1.0
  ]
  edge [
    source 46
    target 82
    key 0
    weight 1.0
  ]
  edge [
    source 46
    target 231
    key 0
    weight 1.0
  ]
  edge [
    source 46
    target 210
    key 0
    weight 1.0
  ]
  edge [
    source 46
    target 146
    key 0
    weight 1.0
  ]
  edge [
    source 47
    target 61
    key 0
    weight 1.0
  ]
  edge [
    source 47
    target 148
    key 0
    weight 1.0
  ]
  edge [
    source 47
    target 210
    key 0
    weight 1.0
  ]
  edge [
    source 47
    target 211
    key 0
    weight 1.0
  ]
  edge [
    source 47
    target 215
    key 0
    weight 1.0
  ]
  edge [
    source 48
    target 113
    key 0
    weight 1.0
  ]
  edge [
    source 48
    target 211
    key 0
    weight 1.0
  ]
  edge [
    source 49
    target 54
    key 0
    weight 1.0
  ]
  edge [
    source 50
    target 51
    key 0
    weight 1.0
  ]
  edge [
    source 51
    target 56
    key 0
    weight 1.0
  ]
  edge [
    source 51
    target 100
    key 0
    weight 1.0
  ]
  edge [
    source 52
    target 218
    key 0
    weight 1.0
  ]
  edge [
    source 53
    target 129
    key 0
    weight 1.0
  ]
  edge [
    source 53
    target 218
    key 0
    weight 1.0
  ]
  edge [
    source 54
    target 83
    key 0
    weight 1.0
  ]
  edge [
    source 54
    target 84
    key 0
    weight 1.0
  ]
  edge [
    source 54
    target 110
    key 0
    weight 1.0
  ]
  edge [
    source 54
    target 55
    key 0
    weight 1.0
  ]
  edge [
    source 54
    target 151
    key 0
    weight 1.0
  ]
  edge [
    source 54
    target 125
    key 0
    weight 1.0
  ]
  edge [
    source 54
    target 60
    key 0
    weight 1.0
  ]
  edge [
    source 54
    target 220
    key 0
    weight 1.0
  ]
  edge [
    source 54
    target 222
    key 0
    weight 1.0
  ]
  edge [
    source 54
    target 124
    key 0
    weight 1.0
  ]
  edge [
    source 55
    target 125
    key 0
    weight 1.0
  ]
  edge [
    source 55
    target 220
    key 0
    weight 1.0
  ]
  edge [
    source 55
    target 223
    key 0
    weight 1.0
  ]
  edge [
    source 56
    target 127
    key 0
    weight 1.0
  ]
  edge [
    source 56
    target 58
    key 0
    weight 1.0
  ]
  edge [
    source 57
    target 58
    key 0
    weight 1.0
  ]
  edge [
    source 58
    target 123
    key 0
    weight 1.0
  ]
  edge [
    source 58
    target 216
    key 0
    weight 1.0
  ]
  edge [
    source 58
    target 128
    key 0
    weight 1.0
  ]
  edge [
    source 59
    target 216
    key 0
    weight 1.0
  ]
  edge [
    source 60
    target 110
    key 0
    weight 1.0
  ]
  edge [
    source 60
    target 87
    key 0
    weight 1.0
  ]
  edge [
    source 60
    target 217
    key 0
    weight 1.0
  ]
  edge [
    source 61
    target 231
    key 0
    weight 1.0
  ]
  edge [
    source 61
    target 69
    key 0
    weight 1.0
  ]
  edge [
    source 61
    target 217
    key 0
    weight 1.0
  ]
  edge [
    source 67
    target 68
    key 0
    weight 1.0
  ]
  edge [
    source 68
    target 187
    key 0
    weight 1.0
  ]
  edge [
    source 69
    target 133
    key 0
    weight 1.0
  ]
  edge [
    source 77
    target 194
    key 0
    weight 1.0
  ]
  edge [
    source 77
    target 195
    key 0
    weight 1.0
  ]
  edge [
    source 81
    target 206
    key 0
    weight 1.0
  ]
  edge [
    source 89
    target 179
    key 0
    weight 1.0
  ]
  edge [
    source 90
    target 182
    key 0
    weight 1.0
  ]
  edge [
    source 91
    target 178
    key 0
    weight 1.0
  ]
  edge [
    source 94
    target 135
    key 0
    weight 1.0
  ]
  edge [
    source 96
    target 190
    key 0
    weight 1.0
  ]
  edge [
    source 98
    target 100
    key 0
    weight 1.0
  ]
  edge [
    source 101
    target 102
    key 0
    weight 1.0
  ]
  edge [
    source 103
    target 227
    key 0
    weight 1.0
  ]
  edge [
    source 113
    target 114
    key 0
    weight 1.0
  ]
  edge [
    source 115
    target 195
    key 0
    weight 1.0
  ]
  edge [
    source 116
    target 195
    key 0
    weight 1.0
  ]
  edge [
    source 117
    target 205
    key 0
    weight 1.0
  ]
  edge [
    source 119
    target 120
    key 0
    weight 1.0
  ]
  edge [
    source 121
    target 213
    key 0
    weight 1.0
  ]
  edge [
    source 131
    target 221
    key 0
    weight 1.0
  ]
  edge [
    source 132
    target 221
    key 0
    weight 1.0
  ]
  edge [
    source 136
    target 186
    key 0
    weight 1.0
  ]
  edge [
    source 137
    target 187
    key 0
    weight 1.0
  ]
  edge [
    source 138
    target 177
    key 0
    weight 1.0
  ]
  edge [
    source 138
    target 197
    key 0
    weight 1.0
  ]
  edge [
    source 139
    target 190
    key 0
    weight 1.0
  ]
  edge [
    source 140
    target 191
    key 0
    weight 1.0
  ]
  edge [
    source 140
    target 192
    key 0
    weight 1.0
  ]
  edge [
    source 141
    target 196
    key 0
    weight 1.0
  ]
  edge [
    source 143
    target 149
    key 0
    weight 1.0
  ]
  edge [
    source 144
    target 203
    key 0
    weight 1.0
  ]
  edge [
    source 145
    target 205
    key 0
    weight 1.0
  ]
  edge [
    source 148
    target 212
    key 0
    weight 1.0
  ]
  edge [
    source 149
    target 219
    key 0
    weight 1.0
  ]
  edge [
    source 150
    target 219
    key 0
    weight 1.0
  ]
  edge [
    source 152
    target 222
    key 0
    weight 1.0
  ]
  edge [
    source 153
    target 179
    key 0
    weight 1.0
  ]
  edge [
    source 154
    target 224
    key 0
    weight 1.0
  ]
  edge [
    source 156
    target 181
    key 0
    weight 1.0
  ]
  edge [
    source 157
    target 183
    key 0
    weight 1.0
  ]
  edge [
    source 158
    target 178
    key 0
    weight 1.0
  ]
  edge [
    source 159
    target 178
    key 0
    weight 1.0
  ]
  edge [
    source 160
    target 184
    key 0
    weight 1.0
  ]
  edge [
    source 161
    target 185
    key 0
    weight 1.0
  ]
  edge [
    source 162
    target 186
    key 0
    weight 1.0
  ]
  edge [
    source 163
    target 225
    key 0
    weight 1.0
  ]
  edge [
    source 165
    target 188
    key 0
    weight 1.0
  ]
  edge [
    source 166
    target 192
    key 0
    weight 1.0
  ]
  edge [
    source 167
    target 227
    key 0
    weight 1.0
  ]
  edge [
    source 168
    target 233
    key 0
    weight 1.0
  ]
  edge [
    source 169
    target 199
    key 0
    weight 1.0
  ]
  edge [
    source 170
    target 203
    key 0
    weight 1.0
  ]
  edge [
    source 171
    target 214
    key 0
    weight 1.0
  ]
  edge [
    source 172
    target 230
    key 0
    weight 1.0
  ]
  edge [
    source 173
    target 215
    key 0
    weight 1.0
  ]
  edge [
    source 175
    target 212
    key 0
    weight 1.0
  ]
  edge [
    source 176
    target 223
    key 0
    weight 1.0
  ]
  edge [
    source 177
    target 187
    key 0
    weight 1.0
  ]
  edge [
    source 179
    target 13
    key 0
    weight 1.0
  ]
  edge [
    source 182
    target 91
    key 0
    weight 1.0
  ]
  edge [
    source 182
    target 158
    key 0
    weight 1.0
  ]
  edge [
    source 182
    target 159
    key 0
    weight 1.0
  ]
  edge [
    source 186
    target 18
    key 0
    weight 1.0
  ]
  edge [
    source 187
    target 9
    key 0
    weight 1.0
  ]
  edge [
    source 188
    target 10
    key 0
    weight 1.0
  ]
  edge [
    source 192
    target 33
    key 0
    weight 1.0
  ]
  edge [
    source 194
    target 23
    key 0
    weight 1.0
  ]
  edge [
    source 194
    target 24
    key 0
    weight 1.0
  ]
  edge [
    source 194
    target 25
    key 0
    weight 1.0
  ]
  edge [
    source 195
    target 72
    key 0
    weight 1.0
  ]
  edge [
    source 195
    target 73
    key 0
    weight 1.0
  ]
  edge [
    source 196
    target 74
    key 0
    weight 1.0
  ]
  edge [
    source 199
    target 29
    key 0
    weight 1.0
  ]
  edge [
    source 199
    target 109
    key 0
    weight 1.0
  ]
  edge [
    source 199
    target 119
    key 0
    weight 1.0
  ]
  edge [
    source 199
    target 30
    key 0
    weight 1.0
  ]
  edge [
    source 200
    target 31
    key 0
    weight 1.0
  ]
  edge [
    source 200
    target 42
    key 0
    weight 1.0
  ]
  edge [
    source 202
    target 143
    key 0
    weight 1.0
  ]
  edge [
    source 202
    target 43
    key 0
    weight 1.0
  ]
  edge [
    source 203
    target 112
    key 0
    weight 1.0
  ]
  edge [
    source 204
    target 47
    key 0
    weight 1.0
  ]
  edge [
    source 205
    target 48
    key 0
    weight 1.0
  ]
  edge [
    source 205
    target 148
    key 0
    weight 1.0
  ]
  edge [
    source 206
    target 78
    key 0
    weight 1.0
  ]
  edge [
    source 206
    target 79
    key 0
    weight 1.0
  ]
  edge [
    source 212
    target 174
    key 0
    weight 1.0
  ]
  edge [
    source 213
    target 51
    key 0
    weight 1.0
  ]
  edge [
    source 214
    target 58
    key 0
    weight 1.0
  ]
  edge [
    source 218
    target 61
    key 0
    weight 1.0
  ]
  edge [
    source 219
    target 130
    key 0
    weight 1.0
  ]
  edge [
    source 224
    target 20
    key 0
    weight 1.0
  ]
  edge [
    source 224
    target 155
    key 0
    weight 1.0
  ]
  edge [
    source 225
    target 71
    key 0
    weight 1.0
  ]
  edge [
    source 226
    target 142
    key 0
    weight 1.0
  ]
  edge [
    source 227
    target 22
    key 0
    weight 1.0
  ]
  edge [
    source 228
    target 75
    key 0
    weight 1.0
  ]
  edge [
    source 229
    target 80
    key 0
    weight 1.0
  ]
  edge [
    source 230
    target 60
    key 0
    weight 1.0
  ]
  edge [
    source 231
    target 147
    key 0
    weight 1.0
  ]
  edge [
    source 233
    target 69
    key 0
    weight 1.0
  ]
]
