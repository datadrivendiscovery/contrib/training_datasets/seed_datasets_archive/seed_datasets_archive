graph [
  node [
    id 0
    label "e7bdba56e46dc006"
    x 0.2555
    y 0.3831
    shape "0.5000"
    ellipse "ic"
    Yellow "[304-397]"
    nodeID "e7bdba56e46dc006"
  ]
  node [
    id 1
    label "373cd8c38703a269"
    x 0.5011
    y 0.3187
    shape "0.5000"
    diamond "ic"
    Black "[387-491]"
    nodeID "373cd8c38703a269"
  ]
  node [
    id 2
    label "79cb8d064fc28a1a"
    x 0.2362
    y 0.6841
    shape "0.5000"
    ellipse "ic"
    Yellow "[343-427]"
    nodeID "79cb8d064fc28a1a"
  ]
  node [
    id 3
    label "f07bc09baf9c2bd0"
    x 0.1349
    y 0.5041
    shape "0.5000"
    nodeID "f07bc09baf9c2bd0"
  ]
  node [
    id 4
    label "ea81c14c7e0b2194"
    x 0.9551
    y 0.4482
    shape "0.5000"
    triangle "ic"
    Yellow "[85/97]"
    nodeID "ea81c14c7e0b2194"
  ]
  node [
    id 5
    label "7ea7ad97bbb89c98"
    x 0.3809
    y 0.1651
    shape "0.5000"
    triangle "ic"
    Blue "[191-*]"
    nodeID "7ea7ad97bbb89c98"
  ]
  node [
    id 6
    label "0c8615e536046940"
    x 0.5027
    y 0.387
    shape "0.5000"
    ellipse "ic"
    Yellow "[232-373]"
    nodeID "0c8615e536046940"
  ]
  node [
    id 7
    label "2b9e99ed1c3bff63"
    x 0.362
    y 0.6749
    shape "0.5000"
    ellipse "ic"
    White "[301-*]"
    nodeID "2b9e99ed1c3bff63"
  ]
  node [
    id 8
    label "399d1e49d947c00a"
    x 0.6067
    y 0.6804
    shape "0.5000"
    ellipse "ic"
    White "[313-520]"
    nodeID "399d1e49d947c00a"
  ]
  node [
    id 9
    label "5d05274a12829cee"
    x 0.2735
    y 0.3496
    shape "0.5000"
    triangle "ic"
    Gray "[159-549]"
    nodeID "5d05274a12829cee"
  ]
  node [
    id 10
    label "717e957ba791e371"
    x 0.3463
    y 0.3938
    shape "0.5000"
    triangle "ic"
    Yellow "[90-471]"
    nodeID "717e957ba791e371"
  ]
  node [
    id 11
    label "f1ec1926248e1055"
    x 0.6496
    y 0.4573
    shape "0.5000"
    triangle "ic"
    Yellow "[363-385]"
    nodeID "f1ec1926248e1055"
  ]
  node [
    id 12
    label "bf3470aded6c627d"
    x 0.6503
    y 0.4254
    shape "0.5000"
    triangle "ic"
    Blue "[299-*]"
    nodeID "bf3470aded6c627d"
  ]
  node [
    id 13
    label "2864bcd491fba2ae"
    x 0.8139
    y 0.506
    shape "0.5000"
    ellipse "ic"
    White "[137-556]"
    nodeID "2864bcd491fba2ae"
  ]
  node [
    id 14
    label "3bb991f738947f7f"
    x 0.5794
    y 0.47
    shape "0.5000"
    empty "ic"
    White "[6-262]"
    nodeID "3bb991f738947f7f"
  ]
  node [
    id 15
    label "fcae5538f8a48029"
    x 0.4418
    y 0.4113
    shape "0.5000"
    ellipse "ic"
    Blue "[249-*]"
    nodeID "fcae5538f8a48029"
  ]
  node [
    id 16
    label "488a1558ba618ac3"
    x 0.409
    y 0.4824
    shape "0.5000"
    triangle "ic"
    Gray "[64-507]"
    nodeID "488a1558ba618ac3"
  ]
  node [
    id 17
    label "a2dc368cd5462fe3"
    x 0.2197
    y 0.4301
    shape "0.5000"
    diamond "ic"
    Black "[455-597]"
    nodeID "a2dc368cd5462fe3"
  ]
  node [
    id 18
    label "3fbf8e6dd34b689d"
    x 0.2586
    y 0.2669
    shape "0.5000"
    ellipse "ic"
    Blue "[220-*]"
    nodeID "3fbf8e6dd34b689d"
  ]
  node [
    id 19
    label "be049bbd41aaaf1d"
    x 0.359
    y 0.5054
    shape "0.5000"
    ellipse "ic"
    Blue "[1-*]"
    nodeID "be049bbd41aaaf1d"
  ]
  node [
    id 20
    label "6cd8b19a56082b94"
    x 0.2263
    y 0.2879
    shape "0.5000"
    empty "ic"
    White "[14-*]"
    nodeID "6cd8b19a56082b94"
  ]
  node [
    id 21
    label "fccf06608bf11b4a"
    x 0.8057
    y 0.3431
    shape "0.5000"
    diamond "ic"
    Black "[117-368]"
    nodeID "fccf06608bf11b4a"
  ]
  node [
    id 22
    label "6c0f9f297471efbb"
    x 0.1273
    y 0.3528
    shape "0.5000"
    triangle "ic"
    Yellow "[220-310/566/665]"
    nodeID "6c0f9f297471efbb"
  ]
  node [
    id 23
    label "ba65cad114f817ed"
    x 0.7079
    y 0.3276
    shape "0.5000"
    triangle "ic"
    Gray "[334-466]"
    nodeID "ba65cad114f817ed"
  ]
  node [
    id 24
    label "27dfa647ea23f253"
    x 0.3052
    y 0.4016
    shape "0.5000"
    triangle "ic"
    Blue "[220-*]"
    nodeID "27dfa647ea23f253"
  ]
  node [
    id 25
    label "85213a2fef362995"
    x 0.337
    y 0.4213
    shape "0.5000"
    triangle "ic"
    Blue "[1-*]"
    nodeID "85213a2fef362995"
  ]
  node [
    id 26
    label "7063425adb48bd7f"
    x 0.2171
    y 0.254
    shape "0.5000"
    ellipse "ic"
    Yellow "[496-504]"
    nodeID "7063425adb48bd7f"
  ]
  node [
    id 27
    label "3b773ced8b96c79a"
    x 0.4034
    y 0.6158
    shape "0.5000"
    triangle "ic"
    Yellow "[50-105]"
    nodeID "3b773ced8b96c79a"
  ]
  node [
    id 28
    label "6194fd78293a8908"
    x 0.4214
    y 0.5625
    shape "0.5000"
    triangle "ic"
    Blue "[8-*]"
    nodeID "6194fd78293a8908"
  ]
  node [
    id 29
    label "aac8bbd225c5785c"
    x 0.383
    y 0.4453
    shape "0.5000"
    ellipse "ic"
    Blue "[489-*]"
    nodeID "aac8bbd225c5785c"
  ]
  node [
    id 30
    label "5e019a21dec4f619"
    x 0.2922
    y 0.4348
    shape "0.5000"
    triangle "ic"
    Blue "[241-*]"
    nodeID "5e019a21dec4f619"
  ]
  node [
    id 31
    label "55821423cdb566ba"
    x 0.4303
    y 0.3759
    shape "0.5000"
    ellipse "ic"
    Blue "[448-*]"
    nodeID "55821423cdb566ba"
  ]
  node [
    id 32
    label "a777f2f23580d9f0"
    x 0.1402
    y 0.0482
    shape "0.5000"
    triangle "ic"
    Yellow "[1-48]"
    nodeID "a777f2f23580d9f0"
  ]
  node [
    id 33
    label "fe3dc863b57e51b3"
    x 0.4255
    y 0.5349
    shape "0.5000"
    ellipse "ic"
    Yellow "[360-639]"
    nodeID "fe3dc863b57e51b3"
  ]
  node [
    id 34
    label "0cc68d8c1898685d"
    x 0.3992
    y 0.209
    shape "0.5000"
    triangle "ic"
    White "[304-397]"
    nodeID "0cc68d8c1898685d"
  ]
  node [
    id 35
    label "9abe75af2cb94e51"
    x 0.6209
    y 0.5092
    shape "0.5000"
    nodeID "9abe75af2cb94e51"
  ]
  node [
    id 36
    label "9d7289be90ada939"
    x 0.3495
    y 0.1643
    shape "0.5000"
    ellipse "ic"
    Yellow "[459-485]"
    nodeID "9d7289be90ada939"
  ]
  node [
    id 37
    label "db4987fc3643a120"
    x 0.5087
    y 0.4624
    shape "0.5000"
    diamond "ic"
    Orange "[2-*]"
    nodeID "db4987fc3643a120"
  ]
  node [
    id 38
    label "d9509dd8244eff73"
    x 0.4469
    y 0.5148
    shape "0.5000"
    triangle "ic"
    Blue "[6-*]"
    nodeID "d9509dd8244eff73"
  ]
  node [
    id 39
    label "cda6b749530b8ab7"
    x 0.82
    y 0.5215
    shape "0.5000"
    ellipse "ic"
    Gray "[1-116]"
    nodeID "cda6b749530b8ab7"
  ]
  node [
    id 40
    label "c4011a459260041f"
    x 0.7395
    y 0.5284
    shape "0.5000"
    ellipse "ic"
    Gray "[7-84]"
    nodeID "c4011a459260041f"
  ]
  node [
    id 41
    label "8fad6e0ea8a40419"
    x 0.2738
    y 0.1654
    shape "0.5000"
    ellipse "ic"
    Yellow "[408-469]"
    nodeID "8fad6e0ea8a40419"
  ]
  node [
    id 42
    label "f8579ba3e30d95c5"
    x 0.4465
    y 0.5446
    shape "0.5000"
    ellipse "ic"
    Blue "[647-*]"
    nodeID "f8579ba3e30d95c5"
  ]
  node [
    id 43
    label "58435d00b6e824e8"
    x 0.2634
    y 0.6203
    shape "0.5000"
    triangle "ic"
    White "[4-475]"
    nodeID "58435d00b6e824e8"
  ]
  node [
    id 44
    label "72438a9e08bb29e8"
    x 0.3089
    y 0.0651
    shape "0.5000"
    triangle "ic"
    Gray "[61-295]"
    nodeID "72438a9e08bb29e8"
  ]
  node [
    id 45
    label "65ac608e91a82caa"
    x 0.7425
    y 0.4135
    shape "0.5000"
    triangle "ic"
    Gray "[2-370]"
    nodeID "65ac608e91a82caa"
  ]
  node [
    id 46
    label "7d252e6c31be583e"
    x 0.3314
    y 0.2325
    shape "0.5000"
    ellipse "ic"
    Yellow "[491-508]"
    nodeID "7d252e6c31be583e"
  ]
  node [
    id 47
    label "d5ad3f7192210fac"
    x 0.6207
    y 0.5253
    shape "0.5000"
    triangle "ic"
    Gray "[299-592]"
    nodeID "d5ad3f7192210fac"
  ]
  node [
    id 48
    label "5f106d2c96905f6e"
    x 0.2218
    y 0.3527
    shape "0.5000"
    diamond "ic"
    Black "[220-*]"
    nodeID "5f106d2c96905f6e"
  ]
  node [
    id 49
    label "6452f2c91227df15"
    x 0.1543
    y 0.3404
    shape "0.5000"
    ellipse "ic"
    White "[157-*]"
    nodeID "6452f2c91227df15"
  ]
  node [
    id 50
    label "cc6e972f0d8d32a5"
    x 0.0915
    y 0.2784
    shape "0.5000"
    diamond "ic"
    Black "[]"
    nodeID "cc6e972f0d8d32a5"
  ]
  node [
    id 51
    label "574b7987f068c32c"
    x 0.1698
    y 0.5627
    shape "0.5000"
    triangle "ic"
    Yellow "[553]"
    nodeID "574b7987f068c32c"
  ]
  node [
    id 52
    label "c3095105428f340e"
    x 0.5979
    y 0.5739
    shape "0.5000"
    diamond "ic"
    Pink "[313-520]"
    nodeID "c3095105428f340e"
  ]
  node [
    id 53
    label "42a754b609538ec9"
    x 0.1953
    y 0.5122
    shape "0.5000"
    nodeID "42a754b609538ec9"
  ]
  node [
    id 54
    label "fd5dc78ee578ed6a"
    x 0.2415
    y 0.286
    shape "0.5000"
    diamond "ic"
    Pink "[157-*]"
    nodeID "fd5dc78ee578ed6a"
  ]
  node [
    id 55
    label "8a3ee5cd3559e35b"
    x 0.1608
    y 0.2469
    shape "0.5000"
    triangle "ic"
    Blue "[14-*]"
    nodeID "8a3ee5cd3559e35b"
  ]
  node [
    id 56
    label "be36bb5476e3a5ef"
    x 0.3431
    y 0.061
    shape "0.5000"
    ellipse "ic"
    Blue "[103-*]"
    nodeID "be36bb5476e3a5ef"
  ]
  node [
    id 57
    label "6d4d60879ebc100e"
    x 0.6684
    y 0.488
    shape "0.5000"
    diamond "ic"
    Black "[2-62]"
    nodeID "6d4d60879ebc100e"
  ]
  node [
    id 58
    label "3ffa8ddf179da82d"
    x 0.2841
    y 0.3302
    shape "0.5000"
    diamond "ic"
    Orange "[220-*]"
    nodeID "3ffa8ddf179da82d"
  ]
  node [
    id 59
    label "9f6c24080d2c145e"
    x 0.4673
    y 0.2033
    shape "0.5000"
    box "ic"
    Yellow "[156-*]"
    nodeID "9f6c24080d2c145e"
  ]
  node [
    id 60
    label "b5ff5d57fd056148"
    x 0.5683
    y 0.4784
    shape "0.5000"
    ellipse "ic"
    Blue "[2-*]"
    nodeID "b5ff5d57fd056148"
  ]
  node [
    id 61
    label "d955c9831cc32278"
    x 0.175
    y 0.2142
    shape "0.5000"
    triangle "ic"
    Yellow "[448-458]"
    nodeID "d955c9831cc32278"
  ]
  node [
    id 62
    label "1e418b35685207a7"
    x 0.2645
    y 0.5422
    shape "0.5000"
    triangle "ic"
    Gray "[287-?]"
    nodeID "1e418b35685207a7"
  ]
  node [
    id 63
    label "c649b97524c4155f"
    x 0.4426
    y 0.3102
    shape "0.5000"
    diamond "ic"
    Black "[249-451]"
    nodeID "c649b97524c4155f"
  ]
  node [
    id 64
    label "b4d8ad96fa0d51e1"
    x 0.4408
    y 0.6262
    shape "0.5000"
    triangle "ic"
    Gray "[6-197]"
    nodeID "b4d8ad96fa0d51e1"
  ]
  node [
    id 65
    label "2973bbee8c7cac8c"
    x 0.6977
    y 0.3004
    shape "0.5000"
    ellipse "ic"
    Blue "[579-*]"
    nodeID "2973bbee8c7cac8c"
  ]
  node [
    id 66
    label "a66910b9b46d2819"
    x 0.2114
    y 0.5635
    shape "0.5000"
    triangle "ic"
    Blue "[346-*]"
    nodeID "a66910b9b46d2819"
  ]
  node [
    id 67
    label "033aee198b2aca2c"
    x 0.2944
    y 0.5637
    shape "0.5000"
    triangle "ic"
    Blue "[189-*]"
    nodeID "033aee198b2aca2c"
  ]
  node [
    id 68
    label "788e2a1f206853f7"
    x 0.3651
    y 0.1911
    shape "0.5000"
    triangle "ic"
    Blue "[611-*]"
    nodeID "788e2a1f206853f7"
  ]
  node [
    id 69
    label "0de42138f8e27a25"
    x 0.2661
    y 0.4161
    shape "0.5000"
    ellipse "ic"
    Gray "[4-475]"
    nodeID "0de42138f8e27a25"
  ]
  node [
    id 70
    label "9e7f196de0542904"
    x 0.4159
    y 0.1542
    shape "0.5000"
    triangle "ic"
    Gray "[13-157]"
    nodeID "9e7f196de0542904"
  ]
  node [
    id 71
    label "4af3636d940ed0df"
    x 0.5125
    y 0.3659
    shape "0.5000"
    empty "ic"
    White "[299-*]"
    nodeID "4af3636d940ed0df"
  ]
  node [
    id 72
    label "35d1ead400fb460e"
    x 0.229
    y 0.4848
    shape "0.5000"
    diamond "ic"
    Pink "[553]"
    nodeID "35d1ead400fb460e"
  ]
  node [
    id 73
    label "05748500d49ee0d8"
    x 0.4622
    y 0.609
    shape "0.5000"
    triangle "ic"
    Blue "[526-*]"
    nodeID "05748500d49ee0d8"
  ]
  node [
    id 74
    label "75e288d595ccda32"
    x 0.2999
    y 0.1148
    shape "0.5000"
    diamond "ic"
    Orange "[1-48]"
    nodeID "75e288d595ccda32"
  ]
  node [
    id 75
    label "b4ade3d193db18e1"
    x 0.3543
    y 0.2944
    shape "0.5000"
    ellipse "ic"
    Blue "[249-*]"
    nodeID "b4ade3d193db18e1"
  ]
  node [
    id 76
    label "f54654e372daa3ae"
    x 0.4046
    y 0.1882
    shape "0.5000"
    triangle "ic"
    Yellow "[]"
    nodeID "f54654e372daa3ae"
  ]
  node [
    id 77
    label "414839f413f0300a"
    x 0.2647
    y 0.7041
    shape "0.5000"
    triangle "ic"
    Yellow "[361-*]"
    nodeID "414839f413f0300a"
  ]
  node [
    id 78
    label "b6426692c5658d6e"
    x 0.2189
    y 0.2121
    shape "0.5000"
    triangle "ic"
    Yellow "[191-*]"
    nodeID "b6426692c5658d6e"
  ]
  node [
    id 79
    label "79025b46f4031806"
    x 0.3362
    y 0.1707
    shape "0.5000"
    ellipse "ic"
    Blue "[295-*]"
    nodeID "79025b46f4031806"
  ]
  node [
    id 80
    label "92263a16e336603e"
    x 0.6051
    y 0.707
    shape "0.5000"
    ellipse "ic"
    White "[6-197]"
    nodeID "92263a16e336603e"
  ]
  node [
    id 81
    label "66bb3a21181e41a6"
    x 0.5575
    y 0.4327
    shape "0.5000"
    triangle "ic"
    Gray "[313-520]"
    nodeID "66bb3a21181e41a6"
  ]
  node [
    id 82
    label "698c7e1007d29bf8"
    x 0.4257
    y 0.2769
    shape "0.5000"
    ellipse "ic"
    Blue "[518-*]"
    nodeID "698c7e1007d29bf8"
  ]
  node [
    id 83
    label "b196f3c4c1c1108f"
    x 0.515
    y 0.6891
    shape "0.5000"
    triangle "ic"
    White "[6-262]"
    nodeID "b196f3c4c1c1108f"
  ]
  node [
    id 84
    label "68d5d2dd7447ed18"
    x 0.4158
    y 0.1994
    shape "0.5000"
    triangle "ic"
    Blue "[402-*]"
    nodeID "68d5d2dd7447ed18"
  ]
  node [
    id 85
    label "35ecbe14460e4006"
    x 0.4821
    y 0.5246
    shape "0.5000"
    ellipse "ic"
    Yellow "[162-431]"
    nodeID "35ecbe14460e4006"
  ]
  node [
    id 86
    label "25e82174e441182a"
    x 0.3632
    y 0.4851
    shape "0.5000"
    ellipse "ic"
    Blue "[344-*]"
    nodeID "25e82174e441182a"
  ]
  node [
    id 87
    label "d4f185bd673d337b"
    x 0.8953
    y 0.4757
    shape "0.5000"
    diamond "ic"
    Black "[1-51]"
    nodeID "d4f185bd673d337b"
  ]
  node [
    id 88
    label "b7a3fb15cfd663b6"
    x 0.293
    y 0.5322
    shape "0.5000"
    diamond "ic"
    Orange "[384-548]"
    nodeID "b7a3fb15cfd663b6"
  ]
  node [
    id 89
    label "1b656fbb9c004dc8"
    x 0.6722
    y 0.623
    shape "0.5000"
    ellipse "ic"
    White "[313-520]"
    nodeID "1b656fbb9c004dc8"
  ]
  node [
    id 90
    label "d6115f30c1085c46"
    x 0.9057
    y 0.2074
    shape "0.5000"
    triangle "ic"
    Yellow "[117-193]"
    nodeID "d6115f30c1085c46"
  ]
  node [
    id 91
    label "30796f3322bf00a1"
    x 0.5632
    y 0.6936
    shape "0.5000"
    ellipse "ic"
    White "[162-429]"
    nodeID "30796f3322bf00a1"
  ]
  node [
    id 92
    label "cf6a55f613056851"
    x 0.4823
    y 0.2907
    shape "0.5000"
    empty "ic"
    White "[518-*]"
    nodeID "cf6a55f613056851"
  ]
  node [
    id 93
    label "38451820b09b1629"
    x 0.3257
    y 0.4864
    shape "0.5000"
    triangle "ic"
    Blue "[89-*]"
    nodeID "38451820b09b1629"
  ]
  node [
    id 94
    label "4732c71b296129f2"
    x 0.6985
    y 0.5687
    shape "0.5000"
    triangle "ic"
    Gray "[9-10]"
    nodeID "4732c71b296129f2"
  ]
  node [
    id 95
    label "3a3687dd044f4877"
    x 0.3126
    y 0.3789
    shape "0.5000"
    ellipse "ic"
    Blue "[297-*]"
    nodeID "3a3687dd044f4877"
  ]
  node [
    id 96
    label "4c77aacc24976f05"
    x 0.5871
    y 0.3565
    shape "0.5000"
    triangle "ic"
    Blue "[482-*]"
    nodeID "4c77aacc24976f05"
  ]
  node [
    id 97
    label "7a276e57977cd203"
    x 0.6669
    y 0.4323
    shape "0.5000"
    triangle "ic"
    Gray "[137-556]"
    nodeID "7a276e57977cd203"
  ]
  node [
    id 98
    label "b4d2e67da9589371"
    x 0.3931
    y 0.1715
    shape "0.5000"
    ellipse "ic"
    Yellow "[358-458]"
    nodeID "b4d2e67da9589371"
  ]
  node [
    id 99
    label "c24114560170a192"
    x 0.5182
    y 0.4278
    shape "0.5000"
    diamond "ic"
    Pink "[9-*]"
    nodeID "c24114560170a192"
  ]
  node [
    id 100
    label "419bc6c0c811a19b"
    x 0.2535
    y 0.2468
    shape "0.5000"
    ellipse "ic"
    Yellow "[]"
    nodeID "419bc6c0c811a19b"
  ]
  node [
    id 101
    label "7fe3149a10f0be8b"
    x 0.244
    y 0.2229
    shape "0.5000"
    triangle "ic"
    Yellow "[471-*]"
    nodeID "7fe3149a10f0be8b"
  ]
  node [
    id 102
    label "da6be6cfae0e9bdc"
    x 0.3597
    y 0.5766
    shape "0.5000"
    empty "ic"
    White "[301-*]"
    nodeID "da6be6cfae0e9bdc"
  ]
  node [
    id 103
    label "f7d987d526a90395"
    x 0.4527
    y 0.6267
    shape "0.5000"
    diamond "ic"
    Orange "[575-*]"
    nodeID "f7d987d526a90395"
  ]
  node [
    id 104
    label "a5ed74537307d224"
    x 0.4501
    y 0.7143
    shape "0.5000"
    triangle "ic"
    Blue "[575-*]"
    nodeID "a5ed74537307d224"
  ]
  node [
    id 105
    label "0d416a408b81e21c"
    x 0.1716
    y 0.479
    shape "0.5000"
    nodeID "0d416a408b81e21c"
  ]
  node [
    id 106
    label "3c69d6a3227a987d"
    x 0.1649
    y 0.5216
    shape "0.5000"
    empty "ic"
    White "[346-*]"
    nodeID "3c69d6a3227a987d"
  ]
  node [
    id 107
    label "dea62a3cfbad2500"
    x 0.3138
    y 0.3247
    shape "0.5000"
    diamond "ic"
    Black "[159-549]"
    nodeID "dea62a3cfbad2500"
  ]
  node [
    id 108
    label "e5570372eb88e9fe"
    x 0.5878
    y 0.4487
    shape "0.5000"
    nodeID "e5570372eb88e9fe"
  ]
  node [
    id 109
    label "836eb0e8084184c9"
    x 0.1567
    y 0.4167
    shape "0.5000"
    triangle "ic"
    Gray "[123/455-597]"
    nodeID "836eb0e8084184c9"
  ]
  node [
    id 110
    label "b6694fed15632e49"
    x 0.1349
    y 0.3527
    shape "0.5000"
    ellipse "ic"
    White "[220-*]"
    nodeID "b6694fed15632e49"
  ]
  node [
    id 111
    label "a1a73969ba5e9aaa"
    x 0.2123
    y 0.4113
    shape "0.5000"
    triangle "ic"
    Blue "[329-*]"
    nodeID "a1a73969ba5e9aaa"
  ]
  node [
    id 112
    label "a5f41e0f356eff9b"
    x 0.3377
    y 0.2836
    shape "0.5000"
    triangle "ic"
    Blue "[1-*]"
    nodeID "a5f41e0f356eff9b"
  ]
  node [
    id 113
    label "c9c2b862cc80c3e0"
    x 0.3711
    y 0.5986
    shape "0.5000"
    ellipse "ic"
    Blue "[285-*]"
    nodeID "c9c2b862cc80c3e0"
  ]
  node [
    id 114
    label "7eb89cf7ec2b1123"
    x 0.2946
    y 0.709
    shape "0.5000"
    ellipse "ic"
    Blue "[616-*]"
    nodeID "7eb89cf7ec2b1123"
  ]
  node [
    id 115
    label "2a989525d0265699"
    x 0.3338
    y 0.3324
    shape "0.5000"
    ellipse "ic"
    Yellow "[229-*]"
    nodeID "2a989525d0265699"
  ]
  node [
    id 116
    label "a259de7561b6e42d"
    x 0.5499
    y 0.3384
    shape "0.5000"
    diamond "ic"
    Orange "[9-559]"
    nodeID "a259de7561b6e42d"
  ]
  node [
    id 117
    label "aca1a46393f573ba"
    x 0.4565
    y 0.3788
    shape "0.5000"
    triangle "ic"
    Blue "[387-*]"
    nodeID "aca1a46393f573ba"
  ]
  node [
    id 118
    label "5d4c3b850475533c"
    x 0.0471
    y 0.3601
    shape "0.5000"
    ellipse "ic"
    Yellow "[281-*]"
    nodeID "5d4c3b850475533c"
  ]
  node [
    id 119
    label "4a09d11da43f6cd6"
    x 0.7187
    y 0.6898
    shape "0.5000"
    triangle "ic"
    Blue "[572-*]"
    nodeID "4a09d11da43f6cd6"
  ]
  node [
    id 120
    label "ce0dcbd98bf5786e"
    x 0.5792
    y 0.2872
    shape "0.5000"
    ellipse "ic"
    Blue "[561-*]"
    nodeID "ce0dcbd98bf5786e"
  ]
  node [
    id 121
    label "b0eadd2d80a26bdd"
    x 0.3012
    y 0.5184
    shape "0.5000"
    diamond "ic"
    Black "[4-475]"
    nodeID "b0eadd2d80a26bdd"
  ]
  node [
    id 122
    label "e03b8d930a3176cf"
    x 0.3072
    y 0.2134
    shape "0.5000"
    ellipse "ic"
    Yellow "[1-48]"
    nodeID "e03b8d930a3176cf"
  ]
  node [
    id 123
    label "42bf69e2827fd819"
    x 0.6023
    y 0.5625
    shape "0.5000"
    triangle "ic"
    Gray "[162-429]"
    nodeID "42bf69e2827fd819"
  ]
  node [
    id 124
    label "92cd2802e26b73ea"
    x 0.3299
    y 0.4469
    shape "0.5000"
    triangle "ic"
    Blue "[376-*]"
    nodeID "92cd2802e26b73ea"
  ]
  node [
    id 125
    label "a8b43484b2bd76d7"
    x 0.5617
    y 0.5157
    shape "0.5000"
    triangle "ic"
    Gray "[5-133]"
    nodeID "a8b43484b2bd76d7"
  ]
  node [
    id 126
    label "2cc026b20adc6d43"
    x 0.4322
    y 0.6387
    shape "0.5000"
    ellipse "ic"
    Blue "[326-*]"
    nodeID "2cc026b20adc6d43"
  ]
  node [
    id 127
    label "76d4813399d9dc10"
    x 0.2898
    y 0.2825
    shape "0.5000"
    triangle "ic"
    Yellow "[141-383/669-670]"
    nodeID "76d4813399d9dc10"
  ]
  node [
    id 128
    label "db8c9c08d78c8b1c"
    x 0.2992
    y 0.2174
    shape "0.5000"
    triangle "ic"
    Gray "[360-407]"
    nodeID "db8c9c08d78c8b1c"
  ]
  node [
    id 129
    label "743e0e63d5a6e1ab"
    x 0.2914
    y 0.1837
    shape "0.5000"
    diamond "ic"
    Orange "[402-*]"
    nodeID "743e0e63d5a6e1ab"
  ]
  node [
    id 130
    label "e4915850a06b3077"
    x 0.5052
    y 0.5936
    shape "0.5000"
    diamond "ic"
    Pink "[6-262]"
    nodeID "e4915850a06b3077"
  ]
  node [
    id 131
    label "ce60d81a0ba0333e"
    x 0.1903
    y 0.4581
    shape "0.5000"
    diamond "ic"
    Black "[316-319]"
    nodeID "ce60d81a0ba0333e"
  ]
  node [
    id 132
    label "d188e40558519734"
    x 0.443
    y 0.5634
    shape "0.5000"
    ellipse "ic"
    Yellow "[292-344]"
    nodeID "d188e40558519734"
  ]
  node [
    id 133
    label "99ab7c308bcb976f"
    x 0.124
    y 0.5044
    shape "0.5000"
    nodeID "99ab7c308bcb976f"
  ]
  node [
    id 134
    label "445e0048dbb9dfae"
    x 0.225
    y 0.4636
    shape "0.5000"
    nodeID "445e0048dbb9dfae"
  ]
  node [
    id 135
    label "16ac42ba9b39e301"
    x 0.6628
    y 0.2364
    shape "0.5000"
    nodeID "16ac42ba9b39e301"
  ]
  node [
    id 136
    label "0fd3077e6cc667a1"
    x 0.688
    y 0.2954
    shape "0.5000"
    ellipse "ic"
    Yellow "[334-492]"
    nodeID "0fd3077e6cc667a1"
  ]
  node [
    id 137
    label "474568ce5e88c955"
    x 0.4102
    y 0.2338
    shape "0.5000"
    triangle "ic"
    Yellow "[9-56]"
    nodeID "474568ce5e88c955"
  ]
  node [
    id 138
    label "a114485be3078b45"
    x 0.6777
    y 0.2616
    shape "0.5000"
    ellipse "ic"
    Blue "[516-*]"
    nodeID "a114485be3078b45"
  ]
  node [
    id 139
    label "69bab46122b927f5"
    x 0.6241
    y 0.576
    shape "0.5000"
    ellipse "ic"
    Gray "[2-62]"
    nodeID "69bab46122b927f5"
  ]
  node [
    id 140
    label "6acbde086d9675ba"
    x 0.2263
    y 0.3092
    shape "0.5000"
    triangle "ic"
    Yellow "[220-365]"
    nodeID "6acbde086d9675ba"
  ]
  node [
    id 141
    label "f05805a297a55489"
    x 0.3928
    y 0.4957
    shape "0.5000"
    ellipse "ic"
    Blue "[668-*]"
    nodeID "f05805a297a55489"
  ]
  node [
    id 142
    label "37488e07b4553d64"
    x 0.045
    y 0.2138
    shape "0.5000"
    ellipse "ic"
    Yellow "[505-*]"
    nodeID "37488e07b4553d64"
  ]
  node [
    id 143
    label "25a84e16f70132df"
    x 0.6477
    y 0.1722
    shape "0.5000"
    triangle "ic"
    Yellow "[163-279]"
    nodeID "25a84e16f70132df"
  ]
  node [
    id 144
    label "c58f6b3dfe30cdc8"
    x 0.4459
    y 0.4263
    shape "0.5000"
    ellipse "ic"
    Yellow "[1-*]"
    nodeID "c58f6b3dfe30cdc8"
  ]
  node [
    id 145
    label "3bc3072932ac63dc"
    x 0.2428
    y 0.6036
    shape "0.5000"
    triangle "ic"
    White "[4-475]"
    nodeID "3bc3072932ac63dc"
  ]
  node [
    id 146
    label "e4b72e811761c5bb"
    x 0.2444
    y 0.4282
    shape "0.5000"
    nodeID "e4b72e811761c5bb"
  ]
  node [
    id 147
    label "c9dcd1cd99f721fa"
    x 0.2999
    y 0.6299
    shape "0.5000"
    triangle "ic"
    Blue "[301-*]"
    nodeID "c9dcd1cd99f721fa"
  ]
  node [
    id 148
    label "53e45c0384a6564a"
    x 0.6534
    y 0.4967
    shape "0.5000"
    triangle "ic"
    Blue "[571-*]"
    nodeID "53e45c0384a6564a"
  ]
  node [
    id 149
    label "fd90a3801ed28a33"
    x 0.4535
    y 0.4893
    shape "0.5000"
    diamond "ic"
    Pink "[86-97]"
    nodeID "fd90a3801ed28a33"
  ]
  node [
    id 150
    label "22b040382be76267"
    x 0.2313
    y 0.3598
    shape "0.5000"
    triangle "ic"
    Gray "[76-632]"
    nodeID "22b040382be76267"
  ]
  node [
    id 151
    label "4a346a7b8e09ff0f"
    x 0.4981
    y 0.2869
    shape "0.5000"
    diamond "ic"
    Pink "[387-*]"
    nodeID "4a346a7b8e09ff0f"
  ]
  node [
    id 152
    label "23c033cf3c7be65b"
    x 0.354
    y 0.3528
    shape "0.5000"
    triangle "ic"
    Blue "[74-*]"
    nodeID "23c033cf3c7be65b"
  ]
  node [
    id 153
    label "a0bb25f1aa390316"
    x 0.3528
    y 0.3375
    shape "0.5000"
    ellipse "ic"
    Yellow "[4-278]"
    nodeID "a0bb25f1aa390316"
  ]
  node [
    id 154
    label "b2a1b1ed2a1466b9"
    x 0.5317
    y 0.2695
    shape "0.5000"
    ellipse "ic"
    Yellow "[159-342]"
    nodeID "b2a1b1ed2a1466b9"
  ]
  node [
    id 155
    label "7213e2c6557b8d11"
    x 0.5562
    y 0.3283
    shape "0.5000"
    ellipse "ic"
    Gray "[229-491]"
    nodeID "7213e2c6557b8d11"
  ]
  node [
    id 156
    label "751c7235d957c6b4"
    x 0.5157
    y 0.479
    shape "0.5000"
    triangle "ic"
    Gray "[1-658]"
    nodeID "751c7235d957c6b4"
  ]
  node [
    id 157
    label "f6716c816bed970a"
    x 0.4829
    y 0.5438
    shape "0.5000"
    diamond "ic"
    Black "[6-262]"
    nodeID "f6716c816bed970a"
  ]
  node [
    id 158
    label "2207c4db6e3c7e63"
    x 0.4888
    y 0.4376
    shape "0.5000"
    ellipse "ic"
    Gray "[474-635]"
    nodeID "2207c4db6e3c7e63"
  ]
  node [
    id 159
    label "30139b730c4f5fc0"
    x 0.1108
    y 0.435
    shape "0.5000"
    triangle "ic"
    White "[203-597]"
    nodeID "30139b730c4f5fc0"
  ]
  node [
    id 160
    label "3ec951aa90a16b3a"
    x 0.5362
    y 0.6106
    shape "0.5000"
    diamond "ic"
    Black "[162-429]"
    nodeID "3ec951aa90a16b3a"
  ]
  node [
    id 161
    label "7beb65723fd3643e"
    x 0.5536
    y 0.3888
    shape "0.5000"
    triangle "ic"
    Blue "[299-*]"
    nodeID "7beb65723fd3643e"
  ]
  node [
    id 162
    label "294ca9cff4ac0456"
    x 0.6083
    y 0.3946
    shape "0.5000"
    triangle "ic"
    Yellow "[9-559]"
    nodeID "294ca9cff4ac0456"
  ]
  node [
    id 163
    label "2baa2857d39f7760"
    x 0.1776
    y 0.2081
    shape "0.5000"
    triangle "ic"
    White "[157-*]"
    nodeID "2baa2857d39f7760"
  ]
  node [
    id 164
    label "de07ea5c7c41107c"
    x 0.4853
    y 0.4934
    shape "0.5000"
    triangle "ic"
    Blue "[624-*]"
    nodeID "de07ea5c7c41107c"
  ]
  node [
    id 165
    label "41de91042e899a2d"
    x 0.5477
    y 0.2917
    shape "0.5000"
    triangle "ic"
    Yellow "[235-400]"
    nodeID "41de91042e899a2d"
  ]
  node [
    id 166
    label "4cc4a7a0c61b1857"
    x 0.208
    y 0.4062
    shape "0.5000"
    diamond "ic"
    Black "[203-597]"
    nodeID "4cc4a7a0c61b1857"
  ]
  node [
    id 167
    label "afdcd4d32943a3f4"
    x 0.5865
    y 0.4213
    shape "0.5000"
    ellipse "ic"
    Yellow "[41-147]"
    nodeID "afdcd4d32943a3f4"
  ]
  node [
    id 168
    label "8516658af2c671e4"
    x 0.4146
    y 0.4485
    shape "0.5000"
    triangle "ic"
    Yellow "[104-364]"
    nodeID "8516658af2c671e4"
  ]
  node [
    id 169
    label "641f5255276cb20a"
    x 0.3563
    y 0.2288
    shape "0.5000"
    diamond "ic"
    Black "[13-157]"
    nodeID "641f5255276cb20a"
  ]
  node [
    id 170
    label "d45ccc3f9f5b7fa2"
    x 0.8574
    y 0.271
    shape "0.5000"
    ellipse "ic"
    Gray "[117-368]"
    nodeID "d45ccc3f9f5b7fa2"
  ]
  node [
    id 171
    label "ce38dc51292c7148"
    x 0.4782
    y 0.3852
    shape "0.5000"
    triangle "ic"
    Blue "[1-*]"
    nodeID "ce38dc51292c7148"
  ]
  node [
    id 172
    label "c5aa8a547c442b08"
    x 0.5247
    y 0.4121
    shape "0.5000"
    ellipse "ic"
    Blue "[9-*]"
    nodeID "c5aa8a547c442b08"
  ]
  node [
    id 173
    label "ab50091841269cff"
    x 0.4749
    y 0.2004
    shape "0.5000"
    triangle "ic"
    Gray "[527]"
    nodeID "ab50091841269cff"
  ]
  node [
    id 174
    label "69ba52b1f5c8abad"
    x 0.5587
    y 0.6393
    shape "0.5000"
    diamond "ic"
    Black "[6-197]"
    nodeID "69ba52b1f5c8abad"
  ]
  node [
    id 175
    label "3f081dd98c1c7738"
    x 0.5113
    y 0.2205
    shape "0.5000"
    triangle "ic"
    Gray "[249-451]"
    nodeID "3f081dd98c1c7738"
  ]
  node [
    id 176
    label "dc78671ab2bad213"
    x 0.0933
    y 0.4439
    shape "0.5000"
    triangle "ic"
    White "[316-319]"
    nodeID "dc78671ab2bad213"
  ]
  node [
    id 177
    label "a5930fd1a4cae184"
    x 0.4059
    y 0.5063
    shape "0.5000"
    ellipse "ic"
    Blue "[1-*]"
    nodeID "a5930fd1a4cae184"
  ]
  node [
    id 178
    label "10fbcf7be48d58e7"
    x 0.1748
    y 0.3152
    shape "0.5000"
    triangle "ic"
    Gray "[246/247]"
    nodeID "10fbcf7be48d58e7"
  ]
  node [
    id 179
    label "91c8936651b9c593"
    x 0.3498
    y 0.4141
    shape "0.5000"
    empty "ic"
    White "[297-*]"
    nodeID "91c8936651b9c593"
  ]
  node [
    id 180
    label "a47bc2bb7f3163c7"
    x 0.2584
    y 0.345
    shape "0.5000"
    ellipse "ic"
    Blue "[157-*]"
    nodeID "a47bc2bb7f3163c7"
  ]
  node [
    id 181
    label "6b691669b2e330fa"
    x 0.3315
    y 0.5357
    shape "0.5000"
    ellipse "ic"
    Gray "[577-610]"
    nodeID "6b691669b2e330fa"
  ]
  node [
    id 182
    label "eb853100100f39f0"
    x 0.5931
    y 0.643
    shape "0.5000"
    triangle "ic"
    Yellow "[176-494]"
    nodeID "eb853100100f39f0"
  ]
  node [
    id 183
    label "79aa3134b3d3dff6"
    x 0.2398
    y 0.4802
    shape "0.5000"
    triangle "ic"
    Yellow "[384-548]"
    nodeID "79aa3134b3d3dff6"
  ]
  node [
    id 184
    label "25c5b8d262185d76"
    x 0.1732
    y 0.4656
    shape "0.5000"
    triangle "ic"
    Yellow "[]"
    nodeID "25c5b8d262185d76"
  ]
  node [
    id 185
    label "3bc222d0c74cb338"
    x 0.3177
    y 0.5687
    shape "0.5000"
    triangle "ic"
    Yellow "[257-*]"
    nodeID "3bc222d0c74cb338"
  ]
  node [
    id 186
    label "0d60cd12b3db8f45"
    x 0.1587
    y 0.5906
    shape "0.5000"
    ellipse "ic"
    Yellow "[238-271]"
    nodeID "0d60cd12b3db8f45"
  ]
  node [
    id 187
    label "ef67696fe5b237a4"
    x 0.6255
    y 0.3548
    shape "0.5000"
    diamond "ic"
    Black "[334-466]"
    nodeID "ef67696fe5b237a4"
  ]
  node [
    id 188
    label "1e60473fef79e655"
    x 0.337
    y 0.3145
    shape "0.5000"
    empty "ic"
    White "[304-397]"
    nodeID "1e60473fef79e655"
  ]
  node [
    id 189
    label "6eac0b343f4ef5e1"
    x 0.3922
    y 0.383
    shape "0.5000"
    diamond "ic"
    Pink "[1-*]"
    nodeID "6eac0b343f4ef5e1"
  ]
  node [
    id 190
    label "c67bc346b5eadfe9"
    x 0.2745
    y 0.5581
    shape "0.5000"
    diamond "ic"
    Black "[89-*]"
    nodeID "c67bc346b5eadfe9"
  ]
  node [
    id 191
    label "9560dfd6563c4ab8"
    x 0.6246
    y 0.3168
    shape "0.5000"
    diamond "ic"
    Orange "[516-*]"
    nodeID "9560dfd6563c4ab8"
  ]
  node [
    id 192
    label "4553e2fb16aab814"
    x 0.2247
    y 0.5155
    shape "0.5000"
    triangle "ic"
    Yellow "[235-283]"
    nodeID "4553e2fb16aab814"
  ]
  node [
    id 193
    label "ef834c912eb739c0"
    x 0.3673
    y 0.5317
    shape "0.5000"
    diamond "ic"
    Pink "[6-197]"
    nodeID "ef834c912eb739c0"
  ]
  node [
    id 194
    label "72e5231da04d6c3c"
    x 0.3207
    y 0.598
    shape "0.5000"
    diamond "ic"
    Orange "[301-*]"
    nodeID "72e5231da04d6c3c"
  ]
  node [
    id 195
    label "5012fbddf54e9a83"
    x 0.9595
    y 0.4107
    shape "0.5000"
    triangle "ic"
    Gray "[1-51]"
    nodeID "5012fbddf54e9a83"
  ]
  node [
    id 196
    label "0732167c8d95443b"
    x 0.2915
    y 0.4237
    shape "0.5000"
    ellipse "ic"
    Gray "[203-597]"
    nodeID "0732167c8d95443b"
  ]
  node [
    id 197
    label "23645d16c7e9aae5"
    x 0.5304
    y 0.2113
    shape "0.5000"
    triangle "ic"
    Gray "[652/653]"
    nodeID "23645d16c7e9aae5"
  ]
  node [
    id 198
    label "7efc5fecf861000d"
    x 0.0866
    y 0.5531
    shape "0.5000"
    triangle "ic"
    Blue "[441-*]"
    nodeID "7efc5fecf861000d"
  ]
  node [
    id 199
    label "27c0c49051f0ccd5"
    x 0.4326
    y 0.4703
    shape "0.5000"
    triangle "ic"
    Blue "[1-*]"
    nodeID "27c0c49051f0ccd5"
  ]
  node [
    id 200
    label "a495d4823272fa5a"
    x 0.2686
    y 0.4543
    shape "0.5000"
    box "ic"
    Green "[484-565]"
    nodeID "a495d4823272fa5a"
  ]
  node [
    id 201
    label "27cf64fd23d38e43"
    x 0.372
    y 0.4252
    shape "0.5000"
    triangle "ic"
    Blue "[234-*]"
    nodeID "27cf64fd23d38e43"
  ]
  node [
    id 202
    label "d6b14934e3498dde"
    x 0.6523
    y 0.3993
    shape "0.5000"
    diamond "ic"
    Black "[137-556]"
    nodeID "d6b14934e3498dde"
  ]
  node [
    id 203
    label "361752b1b525b8d0"
    x 0.2981
    y 0.4904
    shape "0.5000"
    ellipse "ic"
    Blue "[581-*]"
    nodeID "361752b1b525b8d0"
  ]
  node [
    id 204
    label "8fbbb098d349e005"
    x 0.2192
    y 0.4521
    shape "0.5000"
    diamond "ic"
    Black "[203-597]"
    nodeID "8fbbb098d349e005"
  ]
  node [
    id 205
    label "0d9335e5b8f0528b"
    x 0.3086
    y 0.1371
    shape "0.5000"
    diamond "ic"
    Black "[61-295]"
    nodeID "0d9335e5b8f0528b"
  ]
  node [
    id 206
    label "0dee331e584bc1f2"
    x 0.3609
    y 0.445
    shape "0.5000"
    triangle "ic"
    Yellow "[281-325]"
    nodeID "0dee331e584bc1f2"
  ]
  node [
    id 207
    label "68d6a1d74db5f3b1"
    x 0.6851
    y 0.4349
    shape "0.5000"
    nodeID "68d6a1d74db5f3b1"
  ]
  node [
    id 208
    label "b0f9d83ba141a103"
    x 0.3496
    y 0.4564
    shape "0.5000"
    ellipse "ic"
    Blue "[1-*]"
    nodeID "b0f9d83ba141a103"
  ]
  node [
    id 209
    label "543d2121b220e2d0"
    x 0.0995
    y 0.47
    shape "0.5000"
    ellipse "ic"
    Yellow "[316-319]"
    nodeID "543d2121b220e2d0"
  ]
  node [
    id 210
    label "2b3781f1330bbc0d"
    x 0.7449
    y 0.4772
    shape "0.5000"
    diamond "ic"
    Black "[137-556]"
    nodeID "2b3781f1330bbc0d"
  ]
  node [
    id 211
    label "6c8391e038a50a6d"
    x 0.2202
    y 0.6338
    shape "0.5000"
    ellipse "ic"
    White "[89-*]"
    nodeID "6c8391e038a50a6d"
  ]
  node [
    id 212
    label "fa239260f76633c2"
    x 0.5356
    y 0.1921
    shape "0.5000"
    ellipse "ic"
    White "[387-*]"
    nodeID "fa239260f76633c2"
  ]
  node [
    id 213
    label "f737928cb0d39d96"
    x 0.3886
    y 0.3297
    shape "0.5000"
    triangle "ic"
    Blue "[4-*]"
    nodeID "f737928cb0d39d96"
  ]
  node [
    id 214
    label "56c49a640fe6c7cb"
    x 0.6439
    y 0.5001
    shape "0.5000"
    diamond "ic"
    Black "[9-10]"
    nodeID "56c49a640fe6c7cb"
  ]
  node [
    id 215
    label "d46e67886fedc8f2"
    x 0.4549
    y 0.6303
    shape "0.5000"
    ellipse "ic"
    Yellow "[86-97]"
    nodeID "d46e67886fedc8f2"
  ]
  node [
    id 216
    label "5a3fd77617a2982f"
    x 0.4022
    y 0.6462
    shape "0.5000"
    triangle "ic"
    White "[249-*]"
    nodeID "5a3fd77617a2982f"
  ]
  node [
    id 217
    label "eb437256367597c6"
    x 0.3421
    y 0.6076
    shape "0.5000"
    triangle "ic"
    Blue "[193-*]"
    nodeID "eb437256367597c6"
  ]
  node [
    id 218
    label "b58a0c6c05ee063e"
    x 0.6029
    y 0.2469
    shape "0.5000"
    ellipse "ic"
    Blue "[9-*]"
    nodeID "b58a0c6c05ee063e"
  ]
  node [
    id 219
    label "8632d6a2841b9210"
    x 0.4554
    y 0.2974
    shape "0.5000"
    diamond "ic"
    Orange "[518-*]"
    nodeID "8632d6a2841b9210"
  ]
  node [
    id 220
    label "e98749201415fc34"
    x 0.4877
    y 0.3565
    shape "0.5000"
    triangle "ic"
    Gray "[1-520]"
    nodeID "e98749201415fc34"
  ]
  node [
    id 221
    label "da156e21fb535ec0"
    x 0.1957
    y 0.4112
    shape "0.5000"
    nodeID "da156e21fb535ec0"
  ]
  node [
    id 222
    label "2a237d72964ea8c8"
    x 0.2286
    y 0.1877
    shape "0.5000"
    diamond "ic"
    Black "[428-433]"
    nodeID "2a237d72964ea8c8"
  ]
  node [
    id 223
    label "585c99105274999f"
    x 0.1897
    y 0.1211
    shape "0.5000"
    triangle "ic"
    Gray "[428-433]"
    nodeID "585c99105274999f"
  ]
  node [
    id 224
    label "9165ccc367c486f8"
    x 0.3076
    y 0.3
    shape "0.5000"
    ellipse "ic"
    Blue "[1-*]"
    nodeID "9165ccc367c486f8"
  ]
  node [
    id 225
    label "f165845fa115e40b"
    x 0.3068
    y 0.2352
    shape "0.5000"
    ellipse "ic"
    Blue "[61-*]"
    nodeID "f165845fa115e40b"
  ]
  node [
    id 226
    label "9d58da585615550b"
    x 0.4789
    y 0.4564
    shape "0.5000"
    diamond "ic"
    Pink "[1-658]"
    nodeID "9d58da585615550b"
  ]
  node [
    id 227
    label "cb03ca0bd76d4a8e"
    x 0.0891
    y 0.2154
    shape "0.5000"
    triangle "ic"
    Blue "[115-*]"
    nodeID "cb03ca0bd76d4a8e"
  ]
  node [
    id 228
    label "b33ab88c7515985d"
    x 0.6415
    y 0.4051
    shape "0.5000"
    triangle "ic"
    Yellow "[386-566]"
    nodeID "b33ab88c7515985d"
  ]
  node [
    id 229
    label "09deb6499b56c1d5"
    x 0.552
    y 0.5684
    shape "0.5000"
    ellipse "ic"
    Gray "[6-262]"
    nodeID "09deb6499b56c1d5"
  ]
  node [
    id 230
    label "0089153bea5bba16"
    x 0.3546
    y 0.2555
    shape "0.5000"
    diamond "ic"
    Black "[1-*]"
    nodeID "0089153bea5bba16"
  ]
  node [
    id 231
    label "655e6fd7ba4d6176"
    x 0.439
    y 0.388
    shape "0.5000"
    triangle "ic"
    Blue "[7-*]"
    nodeID "655e6fd7ba4d6176"
  ]
  node [
    id 232
    label "a8f5afb9c38ec26b"
    x 0.1919
    y 0.2761
    shape "0.5000"
    ellipse "ic"
    White "[428-433]"
    nodeID "a8f5afb9c38ec26b"
  ]
  node [
    id 233
    label "1bd610a89af750f5"
    x 0.4585
    y 0.5165
    shape "0.5000"
    empty "ic"
    White "[249-*]"
    nodeID "1bd610a89af750f5"
  ]
  edge [
    source 0
    target 196
  ]
  edge [
    source 0
    target 188
  ]
  edge [
    source 1
    target 117
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 2
    target 147
  ]
  edge [
    source 3
    target 204
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 5
    target 230
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 9
    target 178
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 196
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 13
    target 210
  ]
  edge [
    source 14
    target 229
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 213
  ]
  edge [
    source 15
    target 38
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 71
  ]
  edge [
    source 15
    target 233
  ]
  edge [
    source 15
    target 63
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 18
    target 127
  ]
  edge [
    source 18
    target 230
  ]
  edge [
    source 18
    target 61
  ]
  edge [
    source 18
    target 129
  ]
  edge [
    source 18
    target 48
  ]
  edge [
    source 19
    target 93
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 55
  ]
  edge [
    source 20
    target 150
  ]
  edge [
    source 21
    target 45
  ]
  edge [
    source 21
    target 170
  ]
  edge [
    source 22
    target 118
  ]
  edge [
    source 22
    target 48
  ]
  edge [
    source 23
    target 187
  ]
  edge [
    source 24
    target 150
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 58
  ]
  edge [
    source 24
    target 115
  ]
  edge [
    source 24
    target 86
  ]
  edge [
    source 24
    target 48
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 25
    target 152
  ]
  edge [
    source 25
    target 146
  ]
  edge [
    source 25
    target 124
  ]
  edge [
    source 25
    target 203
  ]
  edge [
    source 25
    target 141
  ]
  edge [
    source 25
    target 200
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 189
  ]
  edge [
    source 26
    target 55
  ]
  edge [
    source 26
    target 112
  ]
  edge [
    source 27
    target 38
  ]
  edge [
    source 28
    target 199
  ]
  edge [
    source 29
    target 171
  ]
  edge [
    source 29
    target 181
  ]
  edge [
    source 29
    target 102
  ]
  edge [
    source 30
    target 192
  ]
  edge [
    source 30
    target 111
  ]
  edge [
    source 30
    target 221
  ]
  edge [
    source 30
    target 95
  ]
  edge [
    source 30
    target 201
  ]
  edge [
    source 30
    target 208
  ]
  edge [
    source 30
    target 131
  ]
  edge [
    source 31
    target 199
  ]
  edge [
    source 31
    target 82
  ]
  edge [
    source 32
    target 74
  ]
  edge [
    source 33
    target 199
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 34
    target 188
  ]
  edge [
    source 35
    target 108
  ]
  edge [
    source 36
    target 230
  ]
  edge [
    source 37
    target 60
  ]
  edge [
    source 37
    target 199
  ]
  edge [
    source 38
    target 168
  ]
  edge [
    source 38
    target 73
  ]
  edge [
    source 38
    target 103
  ]
  edge [
    source 38
    target 130
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 87
  ]
  edge [
    source 40
    target 57
  ]
  edge [
    source 41
    target 225
  ]
  edge [
    source 41
    target 128
  ]
  edge [
    source 42
    target 199
  ]
  edge [
    source 43
    target 121
  ]
  edge [
    source 44
    target 205
  ]
  edge [
    source 45
    target 57
  ]
  edge [
    source 46
    target 224
  ]
  edge [
    source 47
    target 108
  ]
  edge [
    source 48
    target 110
  ]
  edge [
    source 48
    target 140
  ]
  edge [
    source 48
    target 146
  ]
  edge [
    source 49
    target 180
  ]
  edge [
    source 50
    target 142
  ]
  edge [
    source 50
    target 178
  ]
  edge [
    source 51
    target 72
  ]
  edge [
    source 52
    target 81
  ]
  edge [
    source 52
    target 89
  ]
  edge [
    source 53
    target 200
  ]
  edge [
    source 53
    target 72
  ]
  edge [
    source 54
    target 180
  ]
  edge [
    source 54
    target 163
  ]
  edge [
    source 54
    target 224
  ]
  edge [
    source 55
    target 227
  ]
  edge [
    source 55
    target 101
  ]
  edge [
    source 56
    target 205
  ]
  edge [
    source 57
    target 139
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 58
    target 224
  ]
  edge [
    source 59
    target 70
  ]
  edge [
    source 59
    target 63
  ]
  edge [
    source 60
    target 164
  ]
  edge [
    source 60
    target 162
  ]
  edge [
    source 60
    target 123
  ]
  edge [
    source 60
    target 158
  ]
  edge [
    source 62
    target 93
  ]
  edge [
    source 63
    target 75
  ]
  edge [
    source 63
    target 175
  ]
  edge [
    source 64
    target 193
  ]
  edge [
    source 64
    target 174
  ]
  edge [
    source 65
    target 191
  ]
  edge [
    source 66
    target 183
  ]
  edge [
    source 66
    target 106
  ]
  edge [
    source 66
    target 88
  ]
  edge [
    source 67
    target 93
  ]
  edge [
    source 68
    target 75
  ]
  edge [
    source 69
    target 109
  ]
  edge [
    source 69
    target 196
  ]
  edge [
    source 69
    target 153
  ]
  edge [
    source 69
    target 121
  ]
  edge [
    source 70
    target 169
  ]
  edge [
    source 71
    target 161
  ]
  edge [
    source 71
    target 120
  ]
  edge [
    source 72
    target 196
  ]
  edge [
    source 72
    target 184
  ]
  edge [
    source 72
    target 105
  ]
  edge [
    source 74
    target 122
  ]
  edge [
    source 75
    target 180
  ]
  edge [
    source 75
    target 213
  ]
  edge [
    source 76
    target 112
  ]
  edge [
    source 77
    target 147
  ]
  edge [
    source 78
    target 127
  ]
  edge [
    source 79
    target 230
  ]
  edge [
    source 80
    target 174
  ]
  edge [
    source 81
    target 117
  ]
  edge [
    source 81
    target 187
  ]
  edge [
    source 82
    target 84
  ]
  edge [
    source 82
    target 230
  ]
  edge [
    source 82
    target 92
  ]
  edge [
    source 82
    target 219
  ]
  edge [
    source 82
    target 173
  ]
  edge [
    source 83
    target 130
  ]
  edge [
    source 84
    target 129
  ]
  edge [
    source 85
    target 144
  ]
  edge [
    source 85
    target 160
  ]
  edge [
    source 86
    target 199
  ]
  edge [
    source 86
    target 88
  ]
  edge [
    source 87
    target 195
  ]
  edge [
    source 88
    target 183
  ]
  edge [
    source 88
    target 124
  ]
  edge [
    source 90
    target 170
  ]
  edge [
    source 91
    target 160
  ]
  edge [
    source 92
    target 171
  ]
  edge [
    source 92
    target 197
  ]
  edge [
    source 93
    target 206
  ]
  edge [
    source 93
    target 196
  ]
  edge [
    source 93
    target 201
  ]
  edge [
    source 93
    target 177
  ]
  edge [
    source 93
    target 185
  ]
  edge [
    source 93
    target 181
  ]
  edge [
    source 93
    target 190
  ]
  edge [
    source 94
    target 214
  ]
  edge [
    source 95
    target 196
  ]
  edge [
    source 95
    target 224
  ]
  edge [
    source 95
    target 208
  ]
  edge [
    source 95
    target 179
  ]
  edge [
    source 95
    target 188
  ]
  edge [
    source 96
    target 172
  ]
  edge [
    source 97
    target 108
  ]
  edge [
    source 97
    target 228
  ]
  edge [
    source 97
    target 202
  ]
  edge [
    source 97
    target 210
  ]
  edge [
    source 98
    target 230
  ]
  edge [
    source 99
    target 172
  ]
  edge [
    source 99
    target 171
  ]
  edge [
    source 100
    target 224
  ]
  edge [
    source 101
    target 112
  ]
  edge [
    source 102
    target 147
  ]
  edge [
    source 103
    target 104
  ]
  edge [
    source 105
    target 200
  ]
  edge [
    source 106
    target 146
  ]
  edge [
    source 106
    target 198
  ]
  edge [
    source 107
    target 180
  ]
  edge [
    source 107
    target 213
  ]
  edge [
    source 108
    target 156
  ]
  edge [
    source 108
    target 161
  ]
  edge [
    source 108
    target 148
  ]
  edge [
    source 108
    target 167
  ]
  edge [
    source 108
    target 228
  ]
  edge [
    source 108
    target 172
  ]
  edge [
    source 108
    target 202
  ]
  edge [
    source 108
    target 214
  ]
  edge [
    source 112
    target 128
  ]
  edge [
    source 112
    target 230
  ]
  edge [
    source 112
    target 189
  ]
  edge [
    source 114
    target 147
  ]
  edge [
    source 116
    target 162
  ]
  edge [
    source 116
    target 218
  ]
  edge [
    source 116
    target 171
  ]
  edge [
    source 117
    target 201
  ]
  edge [
    source 117
    target 171
  ]
  edge [
    source 117
    target 219
  ]
  edge [
    source 117
    target 151
  ]
  edge [
    source 117
    target 226
  ]
  edge [
    source 121
    target 208
  ]
  edge [
    source 121
    target 145
  ]
  edge [
    source 122
    target 224
  ]
  edge [
    source 123
    target 160
  ]
  edge [
    source 124
    target 203
  ]
  edge [
    source 124
    target 152
  ]
  edge [
    source 124
    target 141
  ]
  edge [
    source 125
    target 139
  ]
  edge [
    source 125
    target 167
  ]
  edge [
    source 125
    target 164
  ]
  edge [
    source 126
    target 233
  ]
  edge [
    source 127
    target 225
  ]
  edge [
    source 127
    target 224
  ]
  edge [
    source 127
    target 153
  ]
  edge [
    source 128
    target 225
  ]
  edge [
    source 130
    target 229
  ]
  edge [
    source 131
    target 209
  ]
  edge [
    source 131
    target 176
  ]
  edge [
    source 132
    target 199
  ]
  edge [
    source 133
    target 134
  ]
  edge [
    source 134
    target 196
  ]
  edge [
    source 135
    target 191
  ]
  edge [
    source 136
    target 187
  ]
  edge [
    source 137
    target 153
  ]
  edge [
    source 138
    target 191
  ]
  edge [
    source 139
    target 174
  ]
  edge [
    source 140
    target 224
  ]
  edge [
    source 141
    target 164
  ]
  edge [
    source 143
    target 218
  ]
  edge [
    source 144
    target 171
  ]
  edge [
    source 144
    target 189
  ]
  edge [
    source 146
    target 183
  ]
  edge [
    source 147
    target 181
  ]
  edge [
    source 147
    target 194
  ]
  edge [
    source 149
    target 199
  ]
  edge [
    source 149
    target 215
  ]
  edge [
    source 149
    target 231
  ]
  edge [
    source 151
    target 212
  ]
  edge [
    source 152
    target 189
  ]
  edge [
    source 152
    target 230
  ]
  edge [
    source 153
    target 231
  ]
  edge [
    source 154
    target 220
  ]
  edge [
    source 154
    target 165
  ]
  edge [
    source 155
    target 220
  ]
  edge [
    source 155
    target 165
  ]
  edge [
    source 155
    target 187
  ]
  edge [
    source 156
    target 199
  ]
  edge [
    source 156
    target 226
  ]
  edge [
    source 157
    target 199
  ]
  edge [
    source 157
    target 229
  ]
  edge [
    source 158
    target 199
  ]
  edge [
    source 158
    target 231
  ]
  edge [
    source 159
    target 166
  ]
  edge [
    source 161
    target 171
  ]
  edge [
    source 161
    target 191
  ]
  edge [
    source 164
    target 199
  ]
  edge [
    source 165
    target 220
  ]
  edge [
    source 166
    target 196
  ]
  edge [
    source 167
    target 172
  ]
  edge [
    source 168
    target 208
  ]
  edge [
    source 168
    target 231
  ]
  edge [
    source 169
    target 224
  ]
  edge [
    source 172
    target 220
  ]
  edge [
    source 177
    target 199
  ]
  edge [
    source 177
    target 226
  ]
  edge [
    source 179
    target 201
  ]
  edge [
    source 180
    target 232
  ]
  edge [
    source 180
    target 196
  ]
  edge [
    source 182
    target 229
  ]
  edge [
    source 184
    target 200
  ]
  edge [
    source 186
    target 192
  ]
  edge [
    source 189
    target 220
  ]
  edge [
    source 190
    target 211
  ]
  edge [
    source 193
    target 208
  ]
  edge [
    source 196
    target 206
  ]
  edge [
    source 196
    target 200
  ]
  edge [
    source 196
    target 208
  ]
  edge [
    source 196
    target 204
  ]
  edge [
    source 199
    target 208
  ]
  edge [
    source 205
    target 225
  ]
  edge [
    source 213
    target 224
  ]
  edge [
    source 216
    target 233
  ]
  edge [
    source 222
    target 223
  ]
  edge [
    source 222
    target 232
  ]
  edge [
    source 222
    target 225
  ]
  edge [
    source 224
    target 225
  ]
  edge [
    source 225
    target 230
  ]
]
