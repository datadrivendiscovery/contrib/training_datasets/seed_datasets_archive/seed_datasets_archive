 ID: LL1_3476_HMDB_actio_recognition_dataset
 Name: HMDB dataset
 Description: Human activity recognition from video dataset. This is a reduced dataset consisting of 6 action classes: brush hair, cartwheel, catch, chew, clap, climb
 License: CC Attribution 4.0 International
 License Link: http://creativecommons.org/licenses/by/4.0/
 Source: SERRE Lab, Brown University
 Source Link: http://serre-lab.clps.brown.edu/resource/hmdb-a-large-human-motion-database/
 Citation: 
