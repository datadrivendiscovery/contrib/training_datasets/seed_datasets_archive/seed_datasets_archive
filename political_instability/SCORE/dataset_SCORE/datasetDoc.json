{
  "about": {
    "datasetID": "political_instability_dataset_SCORE",
    "datasetName": "Forecasting the presence of political instability",
    "description": "Yearly occurence of political instability from 1974 to 2003",
    "citation": "Goldsmith, B. E., Butcher, C. R., Semenovich, D., & Sowmya, A. (2013). Forecasting the onset of genocide and politicide: Annual out-of-sample forecasts on a global dataset, 1988–2003. Journal of Peace Research, 50(4), 437–452. doi: 10.1177/0022343313484167",
    "publicationDate": "06-20-2013",
    "source": "Harvard Dataverse",
    "sourceURI": "https://doi.org/10.7910/DVN/29715",
    "datasetSchemaVersion": "4.0.0",
    "datasetVersion": "1.0",
    "digest": "0a52bd8e6048b14be73b7f603a2b91e5b7190526cae04d1a24d1644c153f5b1c"
  },
  "dataResources": [
    {
      "resID": "learningData",
      "resPath": "tables/learningData.csv",
      "resType": "table",
      "resFormat": {
        "text/csv": [
          "csv"
        ]
      },
      "isCollection": false,
      "columns": [
        {
          "colIndex": 0,
          "colName": "d3mIndex",
          "colDescription": "Index",
          "colType": "integer",
          "role": [
            "index"
          ]
        },
        {
          "colIndex": 1,
          "colName": "year",
          "colDescription": "Current year for outcome variables; Ranges from 1974 through 2003, year “t”, while the corresponding “t-1” values for predictor variables are 1973 through 2002",
          "colType": "dateTime",
          "timeGranularity": {
            "value": 1,
            "unit": "years"
          },
          "role": [
            "attribute",
            "timeIndicator"
          ]
        },
        {
          "colIndex": 2,
          "colName": "ef",
          "colDescription": "EF t-1; Ethnic fractionalization",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 3,
          "colName": "instability",
          "colDescription": "Instability t-1; Political instability in year t-1; Political Instability Task Force (PITF) dataset",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 4,
          "colName": "finstability",
          "colDescription": "Instability t; Political instability in year t; PITF",
          "colType": "categorical",
          "role": [
            "suggestedTarget"
          ]
        },
        {
          "colIndex": 5,
          "colName": "ln_total_pop",
          "colDescription": "lnPopulation t-1; Total population, natural logarithm",
          "colType": "real",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 6,
          "colName": "mena",
          "colDescription": "Middle East and North African states",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 7,
          "colName": "csasia",
          "colDescription": "Central and South Asian states",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 8,
          "colName": "nac",
          "colDescription": "NeighborConflict t-1; Number of bordering states with any type (societal or interstate) internal conflict",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 9,
          "colName": "abbrev",
          "colDescription": "Country abbreviation",
          "colType": "integer",
          "role": [
            "attribute",
            "suggestedGroupingKey"
          ]
        },
        {
          "colIndex": 10,
          "colName": "assassin_dum",
          "colDescription": "Assassin t; Successful or attempted political assassination, dummy variable for any instance(s) in a given year",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 11,
          "colName": "stab_yrs",
          "colDescription": "StabYrs t-1; Time since last instability year, begin from 1948, calculated with BTSCS software",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 12,
          "colName": "prev_instability",
          "colDescription": "PrevInstability t-1; Number of previous failures (instability years), calculated with BTSCS software",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 13,
          "colName": "stab_yrs_squared",
          "colDescription": "stab_yrs squared",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 14,
          "colName": "polity_2sq_inv",
          "colDescription": "MixedRegime t-1; The polity regime-type index, ranging from -10 through +10, squared and subtracted from 100 such that it takes higher values for regimes towards the middle of the index",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 15,
          "colName": "num_igo",
          "colDescription": "num_igo t-1; Number of International Governmental Oorganization memberships",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 16,
          "colName": "pol_change_3",
          "colDescription": "Regime change over last three years, calculated as the sum of first differences [( Polity t-1 - Polity t-2 ) + ( Polity t-2 - Polity t-3 ) + ( Polity t-3 - Polity t-4 )]",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 17,
          "colName": "lagged_infant_mort",
          "colDescription": "Lagged Infant Mortality Rate, t-2",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 18,
          "colName": "lagged_assassin_dum",
          "colDescription": "Lagged Political assassination t-2",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 19,
          "colName": "state_led_discrimination",
          "colDescription": "State-led Discrimination t-1, either economic or political, against a particular group, coded as a dummy variable with state-years notincluded in the MAR dataset = 0; Minorities at Risk dataset",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 20,
          "colName": "genpold",
          "colDescription": "Genocide / Politicide dummy variable year t, including ongoing cases",
          "colType": "integer",
          "role": [
            "suggestedTarget"
          ]
        },
        {
          "colIndex": 21,
          "colName": "prev_pol_geno",
          "colDescription": "t-1, Number of previous failures (genocide/politicide onsets) since 1948, calculated with BTSCS software",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 22,
          "colName": "sq_time_since_last_gen",
          "colDescription": "t-1,Squared value of time since last genocide/politicide onset, begin 1948, calculated with BTSCS software",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 23,
          "colName": "cubed_time_since_last_gen",
          "colDescription": "t-1,cubed value of time since last genocide/politicide onset, begin 1948, calculated with BTSCS software",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 24,
          "colName": "ruling_elite",
          "colDescription": "t-1, values: 0 ethnicity not relevant, 1 ruling elite in the majority, 2 ruling elite in the minority; Ethnic Power Relations dataset",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 25,
          "colName": "fullaut",
          "colDescription": "t-1, fully authoritarian regime dummy variable, Polity values of -7 or less are coded as fully authoritarian",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 26,
          "colName": "partaut",
          "colDescription": "t-1, partially authoritarian regime dummy variable, Polity values from -6 through 1 are coded as partially authoritarian",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 27,
          "colName": "partdem",
          "colDescription": "t-1, partially democratic regime dummy variable, Polity values from 2 through 6 are coded as partially democratic",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 28,
          "colName": "hu_defense_burden",
          "colDescription": "t-1, calculated as per Colaresi and Carey (2008), total military personnel divided by natural log of total population",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 29,
          "colName": "uncon_x_hdb",
          "colDescription": "t-1, as per Colaresi and Carey (2008), unconstrained executive interacted with (multiplied by) human defense burden, we interact a dummy variable indicating an unconstrained executive (scoring 1 or 2 on the exconst component of Polity) with the number of military personnel divided by the logged population",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 30,
          "colName": "diff_uncon_x_hdb",
          "colDescription": "t-1, first difference of “exuncHDB_cc ”, (t-1) – (t-2)",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 31,
          "colName": "elec_period",
          "colDescription": "Election Period, there is an election recorded in the NELDA data for year t-2, t-1, or t",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 32,
          "colName": "ef_x_elec_period",
          "colDescription": "Ethnic fractionalization interacted with (multiplied by) Election period",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        },
        {
          "colIndex": 33,
          "colName": "forward_lag_elec_period",
          "colDescription": "Forward-lagged Election Period, i.e., there is an election recorded in the NELDA data for year t-1, t, or t+1",
          "colType": "integer",
          "role": [
            "attribute"
          ]
        }
      ],
      "columnsCount": 34
    }
  ]
}