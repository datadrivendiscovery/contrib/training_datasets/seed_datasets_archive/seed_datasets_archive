 ID: 124_214_coil20_dataset
 Name: coil-20
 Description: Image recognition dataset of 20 objects from 72 different views.
 License: open
 License Link: None
 Source: Columbia University
 Source Link: http://www.cs.columbia.edu/CAVE/software/softlib/coil-20.php
 Citation: 
