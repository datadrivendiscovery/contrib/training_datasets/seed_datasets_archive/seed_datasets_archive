 ID: LL1_crime_chicago_dataset
 Name: LL1_Chicago_Crime
 Description: Chicago Police Department
 License: City of Chicago Terms of Use
 License Link: https://www.chicago.gov/city/en/narr/foia/data_disclaimer.html
 Source: City of Chicago Data Portal
 Source Link: https://data.cityofchicago.org/Public-Safety/Crimes-2001-to-present/ijzp-q8t2
 Citation: 
