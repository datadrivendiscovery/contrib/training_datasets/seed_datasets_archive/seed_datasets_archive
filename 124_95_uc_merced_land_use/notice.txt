 ID: 124_95_uc_merced_land_use_dataset
 Name: uc_merced_land_use
 Description: Overhead image classification dataset consisting of 2100 256x256 images of 21 classes.
 License: open
 License Link: 
 Source: University of California Merced
 Source Link: http://weegee.vision.ucmerced.edu/datasets/landuse.html
 Citation: @article{Yi Yang and Shawn Newsam, "Bag-Of-Visual-Words and Spatial Extensions for Land-Use Classification," ACM SIGSPATIAL International Conference on Advances in Geographic Information Systems (ACM GIS), 2010.}
