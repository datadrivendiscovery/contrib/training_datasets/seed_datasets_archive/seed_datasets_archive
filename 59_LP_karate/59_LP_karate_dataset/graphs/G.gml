graph [
  node [
    id 0
    label "1"
    nodeID 1
  ]
  node [
    id 1
    label "32"
    nodeID 32
  ]
  node [
    id 2
    label "22"
    nodeID 22
  ]
  node [
    id 3
    label "20"
    nodeID 20
  ]
  node [
    id 4
    label "18"
    nodeID 18
  ]
  node [
    id 5
    label "14"
    nodeID 14
  ]
  node [
    id 6
    label "13"
    nodeID 13
  ]
  node [
    id 7
    label "12"
    nodeID 12
  ]
  node [
    id 8
    label "11"
    nodeID 11
  ]
  node [
    id 9
    label "9"
    nodeID 9
  ]
  node [
    id 10
    label "8"
    nodeID 8
  ]
  node [
    id 11
    label "7"
    nodeID 7
  ]
  node [
    id 12
    label "6"
    nodeID 6
  ]
  node [
    id 13
    label "5"
    nodeID 5
  ]
  node [
    id 14
    label "4"
    nodeID 4
  ]
  node [
    id 15
    label "3"
    nodeID 3
  ]
  node [
    id 16
    label "2"
    nodeID 2
  ]
  node [
    id 17
    label "31"
    nodeID 31
  ]
  node [
    id 18
    label "10"
    nodeID 10
  ]
  node [
    id 19
    label "33"
    nodeID 33
  ]
  node [
    id 20
    label "29"
    nodeID 29
  ]
  node [
    id 21
    label "28"
    nodeID 28
  ]
  node [
    id 22
    label "17"
    nodeID 17
  ]
  node [
    id 23
    label "34"
    nodeID 34
  ]
  node [
    id 24
    label "15"
    nodeID 15
  ]
  node [
    id 25
    label "16"
    nodeID 16
  ]
  node [
    id 26
    label "19"
    nodeID 19
  ]
  node [
    id 27
    label "21"
    nodeID 21
  ]
  node [
    id 28
    label "23"
    nodeID 23
  ]
  node [
    id 29
    label "24"
    nodeID 24
  ]
  node [
    id 30
    label "30"
    nodeID 30
  ]
  node [
    id 31
    label "26"
    nodeID 26
  ]
  node [
    id 32
    label "25"
    nodeID 25
  ]
  node [
    id 33
    label "27"
    nodeID 27
  ]
  edge [
    source 0
    target 2
    weight 1
  ]
  edge [
    source 0
    target 4
    weight 1
  ]
  edge [
    source 0
    target 5
    weight 1
  ]
  edge [
    source 0
    target 6
    weight 1
  ]
  edge [
    source 0
    target 7
    weight 1
  ]
  edge [
    source 0
    target 8
    weight 1
  ]
  edge [
    source 0
    target 10
    weight 1
  ]
  edge [
    source 0
    target 11
    weight 1
  ]
  edge [
    source 1
    target 31
    weight 1
  ]
  edge [
    source 1
    target 19
    weight 1
  ]
  edge [
    source 3
    target 23
    weight 1
  ]
  edge [
    source 5
    target 23
    weight 1
  ]
  edge [
    source 5
    target 15
    weight 1
  ]
  edge [
    source 8
    target 12
    weight 1
  ]
  edge [
    source 9
    target 23
    weight 1
  ]
  edge [
    source 9
    target 15
    weight 1
  ]
  edge [
    source 10
    target 15
    weight 1
  ]
  edge [
    source 10
    target 16
    weight 1
  ]
  edge [
    source 11
    target 13
    weight 1
  ]
  edge [
    source 11
    target 22
    weight 1
  ]
  edge [
    source 14
    target 16
    weight 1
  ]
  edge [
    source 15
    target 20
    weight 1
  ]
  edge [
    source 15
    target 16
    weight 1
  ]
  edge [
    source 15
    target 18
    weight 1
  ]
  edge [
    source 17
    target 23
    weight 1
  ]
  edge [
    source 19
    target 23
    weight 1
  ]
  edge [
    source 19
    target 25
    weight 1
  ]
  edge [
    source 19
    target 27
    weight 1
  ]
  edge [
    source 19
    target 28
    weight 1
  ]
  edge [
    source 19
    target 29
    weight 1
  ]
  edge [
    source 20
    target 23
    weight 1
  ]
  edge [
    source 21
    target 29
    weight 1
  ]
  edge [
    source 21
    target 32
    weight 1
  ]
  edge [
    source 23
    target 25
    weight 1
  ]
  edge [
    source 23
    target 28
    weight 1
  ]
  edge [
    source 23
    target 30
    weight 1
  ]
  edge [
    source 23
    target 24
    weight 1
  ]
  edge [
    source 23
    target 26
    weight 1
  ]
  edge [
    source 30
    target 33
    weight 1
  ]
]
