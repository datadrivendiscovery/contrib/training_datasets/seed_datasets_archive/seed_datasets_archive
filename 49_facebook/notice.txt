 ID: 49_facebook_dataset
 Name: Facebook graph data
 Description: Data for 49_facebook consists of two graphs G1 and G2 with a known mapping (both matches and mismatches) which is provided in the learningData.csv
 License: public
 License Link: None
 Source: The Max Plank Institute
 Source Link: http://socialnetworks.mpi-sws.org/datasets.html
 Citation: 
